Before your development, please check wiki and 'WARNING' label issue to get more information.

It must be pointed out that all functions or changes must be developed together with the test. You need to make sure that all the logical branches are covered by the test.

* [Install to front-end project](#install-to-front-end-project)
* [Available Scripts](#available-scripts)
  * [Build](#build)
  * [test](#test)
  * [Lint](#lint)
* [Develop Web-API](#develop-web-api)
* [Update front end project Web-API module](#develop-web-api)


## Install to front-end project
1. Before installing, you may need to confirm you have uploaded the ssh key to your gitlab account settings.  
link: http://192.168.222.60:8001/profile/keys

2. Add record to package.json dependencies field  
```
# Latest master
git+ssh://git@192.168.222.60:eab-shared-module/eab-web-api.git
# By branch
git+ssh://git@192.168.222.60:eab-shared-module/eab-web-api.git#<branch name>
# By tag
git+ssh://git@192.168.222.60:eab-shared-module/eab-web-api.git@<tag name>
```

## Available Scripts
### Build
`npm run build`

Build Web-API module to 'lib' folder.

#### Test
`npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### Lint
`npm run lint`

Run the linting utility.

#### Smart Build
`npm run smartBuild`

Build Web-API module to 'lib' folder and auto deploy to your front-end project. It need you to place the front-end project into the same folder level of your web-api.

## Develop Web-API
After the development work is completed, you must build the project before the push to incorporate the changes into the module. At the same time, you need to update the Web-API module of the front-end project.

## Update front end project Web-API module
If you are try to test the front end project with your Web-API development branch, please edit your package.json `eab-web-api` record to your branch.
1. delete package lock file(package-lock.json etc.)
2. re-install module  
`npm install eab-web-api`
3. re bundle  
`npm run reset-start`
