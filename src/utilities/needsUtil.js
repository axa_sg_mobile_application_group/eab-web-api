import * as _ from "lodash";
import {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  PSGOALS,
  EPLANNING,
  RPLANNING,
  OTHER
} from "../constants/NEEDS";
import {
  OWNER,
  SPOUSE,
  SPO,
  SON,
  DAU,
  FAT,
  MOT,
  GFA,
  GMO
} from "../constants/DEPENDANT";
import { getClientName, getSpouseProfile } from "./clientUtil";

const RELATIONSHIP_TYPE = {
  OWNER: "OWNER",
  SPOUSE: "SPOUSE",
  DEPENDANTS: "DEPENDANTS"
};

const getSeperateObj = id => {
  const seperateObjectById = {
    spAspects: {
      id: "pcHeadstart",
      items: [
        {
          title: "Planning for children's headstart - Protection",
          value: "pTotShortfall",
          checkValue: "P"
        },
        {
          title: "Planning for children's headstart - Critical Illness",
          value: "ciTotShortfall",
          checkValue: "CI"
        }
      ]
    },
    sfAspects: {
      id: "pcHeadstart",
      items: [
        {
          title: "Protection",
          value: "pTotShortfall",
          type: "protection",
          checkValue: "P"
        },
        {
          title: "Critical Illness",
          value: "ciTotShortfall",
          type: "criticalIllness",
          checkValue: "CI"
        }
      ]
    }
  };
  return seperateObjectById[id];
};

const filterAspect = ({
  relationship,
  filter,
  dependants,
  pdaDependants,
  applicant,
  cid,
  type
}) => {
  type = _.toUpper(type);
  relationship = _.toUpper(relationship);
  // is exist options
  if (relationship.indexOf(type) > -1) {
    if (type === RELATIONSHIP_TYPE.OWNER) {
      return !filter || filter.indexOf(type) > -1;
    } else if (type === RELATIONSHIP_TYPE.SPOUSE) {
      return (
        !filter ||
        (filter.indexOf(type) > -1 &&
          applicant === "joint" &&
          _.find(dependants, _d => _d.cid === cid && _d.relationship === SPO))
      );
    } else if (type === RELATIONSHIP_TYPE.DEPENDANTS) {
      // is option selected in personal data acknowledge?
      if (pdaDependants && pdaDependants.indexOf(cid) > -1) {
        const dependant = _.find(dependants, _d => _d.cid === cid);

        if (
          _.get(dependant, "relationship") === SPO &&
          applicant === "single"
        ) {
          // Make Spouse visiable when spouse is in filter of the items and the application is single
          return (
            !filter ||
            _.indexOf(_.split(filter, ","), dependant.relationship) > -1 ||
            false
          );
        } else if (
          _.get(dependant, "relationship") === SPO &&
          applicant === "joint"
        ) {
          // Filter out the SPO in dependants When the application is joint
          return false;
        }
        if (dependant) {
          return !filter || filter.indexOf(dependant.relationship) > -1;
        }
      }
    } else {
      return true;
    }
  }
  return false;
};

const returnOptionValue = ({
  profile,
  dependantProfiles,
  aspectItem,
  aspectValue,
  index,
  inCompleteFlag
}) => {
  let optionTitle = "";
  if (aspectItem.id === "psGoals") {
    optionTitle = "Planning for specific goals";
  } else if (aspectItem.id === "other") {
    optionTitle = "Other needs (e.g. mortgage, pregnancy, savings)";
  }

  return {
    // for sortable list
    id: `${aspectValue.cid}-${aspectItem.id}-${index}`,
    name: getClientName({ profile, dependantProfiles, cid: aspectValue.cid }),
    cid: aspectValue.cid,
    aid: aspectItem.id,
    title:
      aspectItem.title ||
      _.get(aspectValue, `goals[${index}].goalName`) ||
      optionTitle,
    typeOfNeeds: _.get(aspectValue, `goals[${index}].typeofNeeds`),
    type: _.get(aspectValue, `goals[${index}].typeofNeeds`),
    totShortfall: aspectValue.totShortfall,
    timeHorizon: aspectValue.timeHorizon,
    image: aspectItem.image,
    inCompleteOption: inCompleteFlag
  };
};

const handlePriorityOption = ({
  profile,
  dependantProfiles,
  aspectItem,
  aspectValue,
  seperateObj = [],
  inCompleteFlag
}) => {
  const result = [];
  const tempAspectValue = _.cloneDeep(aspectValue);
  const tempGoals = _.get(tempAspectValue, "goals");
  const tempAspectItem = _.cloneDeep(aspectItem);
  const goalNo = _.get(aspectValue, "goalNo") || 0;
  if (tempGoals) {
    _.forEach(tempGoals, (Obj, index) => {
      tempAspectValue.totShortfall = Obj.totShortfall;
      tempAspectValue.timeHorizon = Obj.timeHorizon;
      tempAspectItem.title = Obj.title;
      if (index < goalNo) {
        result.push(
          returnOptionValue({
            profile,
            dependantProfiles,
            aspectItem: tempAspectItem,
            aspectValue: tempAspectValue,
            index,
            inCompleteFlag
          })
        );
      }
    });
    return result;
  }

  if (aspectItem.id === "pcHeadstart" && !inCompleteFlag) {
    const providedFor = aspectValue.providedFor || "";
    if (providedFor.indexOf("P") > -1) {
      const pAspectValue = _.cloneDeep(aspectValue);
      const pAspectItem = _.cloneDeep(aspectItem);
      pAspectValue.totShortfall = pAspectValue.pTotShortfall;
      pAspectItem.title = "Planning for children's headstart - Protection";
      result.push(
        returnOptionValue({
          profile,
          dependantProfiles,
          aspectItem: pAspectItem,
          aspectValue: pAspectValue,
          index: 0,
          inCompleteFlag
        })
      );
    }

    if (providedFor.indexOf("C") > -1) {
      const cAspectValue = _.cloneDeep(aspectValue);
      const cAspectItem = _.cloneDeep(aspectItem);
      cAspectValue.totShortfall = cAspectValue.ciTotShortfall;
      cAspectItem.title =
        "Planning for children's headstart - Critical Illness";
      result.push(
        returnOptionValue({
          profile,
          dependantProfiles,
          aspectItem: cAspectItem,
          aspectValue: cAspectValue,
          index: 1,
          inCompleteFlag
        })
      );
    }

    if (result.length) {
      return result;
    }
  }

  // seperate option
  const sObj = _.find(seperateObj, s => s.id === aspectItem.id);
  if (sObj) {
    _.forEach(sObj.items, (obj, sIndex) => {
      const providedForArray = _.split(aspectValue.providedFor, ",");

      if (providedForArray.indexOf(obj.checkValue) > -1) {
        const sAspectValue = _.cloneDeep(aspectValue);
        const sAspectItem = _.cloneDeep(aspectItem);
        sAspectValue.totShortfall = aspectValue[obj.value];
        sAspectItem.title = obj.title;
        sAspectItem.type = obj.type;
        result.push(
          returnOptionValue({
            profile,
            dependantProfiles,
            aspectItem: sAspectItem,
            aspectValue: sAspectValue,
            index: sIndex,
            inCompleteFlag
          })
        );
      }
    });
    return result;
  }

  return returnOptionValue({
    profile,
    dependantProfiles,
    aspectItem,
    aspectValue,
    index: 0,
    inCompleteFlag
  });
};

const concatPriorityOption = ({
  profile,
  dependantProfiles,
  aspectItem,
  aspectValue,
  seperateObj,
  seperateId,
  aspect,
  relationship,
  inCompleteFlag
}) => {
  const tempOptions = handlePriorityOption({
    profile,
    dependantProfiles,
    aspectItem,
    aspectValue,
    seperateObj,
    seperateId,
    aspect,
    relationship,
    inCompleteFlag
  });
  let result = [];
  if (_.isArray(tempOptions)) {
    result = result.concat(tempOptions);
  } else {
    result.push(tempOptions);
  }

  return result;
};

const sortProfiles = (a, b) => {
  const sortArr = [SON, DAU, SPO];
  const priorityA = sortArr.indexOf(a.relationship);
  const priorityB = sortArr.indexOf(b.relationship);

  if (a.relationship === b.relationship && a.fullName > b.fullName) return 1;
  else if (a.relationship === b.relationship && a.fullName < b.fullName)
    return -1;
  if (priorityA > priorityB && priorityA !== -1 && priorityB !== -1) return -1;
  else if (priorityA < priorityB && priorityA !== -1 && priorityB !== -1)
    return 1;
  else if (priorityA > -1 && priorityB === -1) return -1;
  else if (priorityB > -1 && priorityA === -1) return 1;
  else if (a.relationship > b.relationship) return 1;
  else if (a.relationship < b.relationship) return -1;
  return 0;
};

export const getAspectOptions = () => [
  {
    id: FIPROTECTION,
    relationship: "OWNER,SPOUSE",
    filter: [OWNER, SPOUSE],
    image: "fiProtectionImg"
  },
  {
    id: CIPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [OWNER, SPOUSE],
    image: "ciProtectionImg"
  },
  {
    id: DIPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [OWNER, SPOUSE, SON, DAU],
    image: "diProtectionImg"
  },
  {
    id: PAPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [OWNER, SPOUSE, SON, DAU],
    image: "paProtectionImg"
  },
  {
    id: PCHEADSTART,
    relationship: "DEPENDANTS",
    filter: [SON, DAU],
    image: "pcHeadstartImg"
  },
  {
    id: HCPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [OWNER, SPOUSE, SPO, SON, DAU, FAT, MOT, GFA, GMO],
    image: "hcProtectionImg"
  },
  {
    id: PSGOALS,
    relationship: "OWNER,SPOUSE",
    filter: [OWNER, SPOUSE],
    image: "psGoalsImg"
  },
  {
    id: EPLANNING,
    relationship: "DEPENDANTS",
    filter: [SON, DAU],
    image: "ePlanningImg"
  },
  {
    id: RPLANNING,
    relationship: "OWNER,SPOUSE",
    filter: [OWNER, SPOUSE],
    image: "rPlanningImg"
  },
  {
    id: OTHER,
    relationship: "OWNER,SPOUSE",
    filter: [OWNER, SPOUSE],
    image: "otherAspectImg"
  }
];

export const getPriorityOptions = ({
  subType,
  fnaValue,
  profile,
  dependantProfiles,
  pda
}) => {
  const id = _.toLower(subType) === "shortfall" ? "sfAspects" : "spAspects";

  let options = [];
  const cAspectValue = fnaValue[id];
  const { dependants } = profile;
  const { applicant, dependants: pdaDependants } = pda;
  const seperateId = [PCHEADSTART];
  const seperateObj = getSeperateObj(id);
  const groupId = HCPROTECTION;

  const aspects = _.split(fnaValue.aspects, ",");
  _.forEach(aspects, aspect => {
    const aspectValue = fnaValue[aspect];
    if (aspectValue) {
      const aspectItem = _.find(
        getAspectOptions(),
        option => option.id === aspect
      );
      const ownerCid = _.get(aspectValue, "owner.cid");
      if (
        ownerCid &&
        filterAspect({
          relationship: aspectItem.relationship,
          filter: aspectItem.filter,
          dependants,
          pdaDependants,
          applicant,
          cid: ownerCid,
          type: RELATIONSHIP_TYPE.OWNER
        }) &&
        aspectValue.owner.isActive
      ) {
        options = options.concat(
          concatPriorityOption({
            profile,
            dependantProfiles,
            aspectItem,
            aspectValue: aspectValue.owner,
            seperateObj,
            seperateId,
            aspect,
            relationship: RELATIONSHIP_TYPE.OWNER
          })
        );
      }
      const spouseId = _.get(aspectValue, "spouse.cid");
      if (
        spouseId &&
        filterAspect({
          relationship: aspectItem.relationship,
          filter: aspectItem.filter,
          dependants,
          pdaDependants,
          applicant,
          cid: spouseId,
          type: RELATIONSHIP_TYPE.SPOUSE
        }) &&
        aspectValue.spouse.isActive
      ) {
        options = options.concat(
          concatPriorityOption({
            profile,
            dependantProfiles,
            aspectItem,
            aspectValue: aspectValue.spouse,
            seperateObj,
            seperateId,
            aspect,
            relationship: RELATIONSHIP_TYPE.SPOUSE
          })
        );
      }
      if (!_.isEmpty(aspectValue.dependants)) {
        _.forEach(aspectValue.dependants, de => {
          if (
            filterAspect({
              relationship: aspectItem.relationship,
              filter: aspectItem.filter,
              dependants,
              pdaDependants,
              applicant,
              cid: de.cid,
              type: RELATIONSHIP_TYPE.DEPENDANTS
            }) &&
            de.isActive
          ) {
            options = options.concat(
              concatPriorityOption({
                profile,
                dependantProfiles,
                aspectItem,
                aspectValue: de,
                seperateObj,
                seperateId,
                aspect,
                relationship: RELATIONSHIP_TYPE.OWNER
              })
            );
          }
        });
      }
    }
  });

  let orderedCid = [{ cid: profile.cid }];
  const orderedDepCid = [];
  const tempOrderedDepCid = _.cloneDeep(dependantProfiles) || [];
  _.forEach(Object.keys(tempOrderedDepCid || []), key => {
    orderedDepCid.push(tempOrderedDepCid[key]);
  });
  const orderedOptions = [];
  orderedDepCid.sort(sortProfiles);
  orderedCid = orderedCid.concat(orderedDepCid);

  if (groupId) {
    const gids = groupId.split(",");
    _.forEach(gids, gid => {
      const opts = [];
      for (let i = options.length - 1; i >= 0; i -= 1) {
        const opt = options[i];
        if (opt.aid === gid) {
          opts.push(opt);
          options.splice(i, 1);
        }
      }
      _.forEach(orderedCid, item => {
        const opt = _.find(opts, ["cid", item.cid]);
        if (_.isObject(opt)) orderedOptions.push(opt);
      });
      let cid = "";
      let name = "";
      const nameArray = [];
      if (orderedOptions.length) {
        _.forEach(orderedOptions, opt => {
          cid += (cid !== "" ? "," : "") + opt.cid;
          name += (name !== "" ? ", " : "") + opt.name;
          nameArray.push(opt.name);
        });
        options.push({
          id: gid,
          cid,
          name,
          nameArray,
          aid: gid,
          title: orderedOptions[0].title,
          image: orderedOptions[0].image
        });
      }
    });
  }
  const optionsFullLength = options.length;

  if (_.toUpper(subType) === "SHORTFALL") {
    options = options.filter(
      o => !_.isNumber(o.totShortfall) || o.totShortfall < 0
    );
  } else if (_.toUpper(subType) === "SURPLUS") {
    options = options.filter(o => o.totShortfall >= 0);
  }

  if (cAspectValue) {
    const cOptions = [];
    _.forEach(cAspectValue, c => {
      const oi = _.findIndex(
        options,
        o =>
          ((groupId && groupId.indexOf(c.aid) > -1) || o.cid === c.cid) &&
          o.aid === c.aid
      );
      if (oi !== -1) {
        cOptions.push(options[oi]);
        options.splice(oi, 1);
      }
    });

    _.forEach(options, opt => {
      cOptions.push(opt);
    });
    options = cOptions;
  }

  return [options, optionsFullLength];
};
/**
 * @description map fe data form fe(with data initial)
 * @param {object} profileData - reducer client.profile state
 * @param {object} dependantProfilesData - reducer client.dependantProfilesData
 *   state
 * @param {object} fe - reducer fna.fe state
 * @param {object} relationshipObject - relationship between the client and the
 *   editing person
 * @param {object} relationshipObject.cid - editing person cid
 * @param {object} relationshipObject.relationship - editing person cid
 * @return {object} fe data object
 * */
export const feDataGetter = ({
  profileData,
  dependantProfilesData,
  fe,
  relationshipObject
}) => {
  switch (relationshipObject.relationship) {
    case "owner": {
      const initialData = Object.assign(
        {
          cid: relationshipObject.cid,
          init: true,
          // net worth
          savAcc: 0,
          fixDeposit: 0,
          invest: 0,
          property: 0,
          car: 0,
          cpfOa: 0,
          cpfSa: 0,
          cpfMs: 0,
          srs: 0,
          otherAssetTitle: "",
          otherAsset: 0,
          mortLoan: 0,
          motLoan: 0,
          eduLoan: 0,
          otherLiabilitesTitle: "",
          otherLiabilites: 0,
          noALReason: "",
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: "",
          // cash flow
          lessMCPF: 0,
          aBonus: 0,
          personExpenses: 0,
          householdExpenses: 0,
          regSav: 0,
          otherExpenseTitle: "",
          otherExpense: 0,
          additionComment: "",
          noCFReason: "",
          // My Budget (Annual Disposable Income Committed for Regular Premium Budget)
          aRegPremBudget: 0,
          aRegPremBudgetSrc: "",
          confirmBudget: "",
          confirmBudgetReason: "",
          // My Budget (Single Premium)
          singPrem: 0,
          singPremSrc: "",
          confirmSingPremBudget: "",
          confirmSingPremBudgetReason: "",
          // My Budget - CPF(OA)
          cpfOaBudget: 0,
          // My Budget - CPF(SA)
          cpfSaBudget: 0,
          // My Budget - CPF Medisave
          cpfMsBudget: 0,
          // My Budget - SRS
          srsBudget: 0,
          // forceIncome
          forceIncome: "",
          forceIncomeReason: ""
        },
        fe.owner || {}
      );
      // net worth
      const assets =
        initialData.savAcc +
        initialData.fixDeposit +
        initialData.invest +
        initialData.property +
        initialData.car +
        initialData.cpfOa +
        initialData.cpfSa +
        initialData.cpfMs +
        initialData.srs +
        initialData.otherAsset;
      const liabilities =
        initialData.mortLoan +
        initialData.motLoan +
        initialData.eduLoan +
        initialData.otherLiabilites;
      const netWorth = assets - liabilities;
      // existing insurance portfolio
      const eiPorf =
        initialData.lifeInsProt +
        initialData.retirePol +
        initialData.ednowSavPol +
        initialData.eduFund +
        initialData.invLinkPol +
        initialData.disIncomeProt +
        initialData.ciPort +
        initialData.personAcc;
      const aInsPrem =
        initialData.pAInsPrem + initialData.sAInsPrem + initialData.oAInsPrem;
      const sInsPrem =
        initialData.caInsPrem +
        initialData.csInsPrem +
        initialData.mdInsPrem +
        initialData.fpInsPrem;
      // cash flow
      const mIncome = Number.isNaN(profileData.allowance)
        ? 0
        : profileData.allowance;
      const netMIcncome = mIncome - initialData.lessMCPF;
      const totAIncome = netMIcncome * 12 + initialData.aBonus;

      // get insurancePrem
      let insurancePrem = initialData.pAInsPrem;
      if (fe.spouse && fe.spouse.pAInsPrem) {
        insurancePrem += fe.spouse.pAInsPrem || 0;
      }
      if (fe.dependants && Array.isArray(fe.dependants)) {
        fe.dependants.forEach(dependant => {
          insurancePrem += dependant.pAInsPrem || 0;
        });
      }
      insurancePrem = Math.round((insurancePrem / 12) * 100) / 100;
      const totMExpense =
        initialData.personExpenses +
        initialData.householdExpenses +
        insurancePrem +
        initialData.regSav +
        initialData.otherExpense;
      const totAExpense = totMExpense * 12;
      const aDisposeIncome = totAIncome - totAExpense;
      const mDisposeIncome = Math.round(aDisposeIncome / 12);
      // My Budget (Annual Disposable Income Committed for Regular Premium Budget)
      // get confirmBudget, need to initial in initialData for do not cover the source data(fe)
      let { confirmBudget } = initialData;
      if (mDisposeIncome !== 0) {
        confirmBudget =
          initialData.aRegPremBudget > aDisposeIncome / 2 ? "Y" : "N";
      }
      // My Budget (Single Premium)
      // get confirmSingPremBudget, need to initial in initialData for do not cover the source data(fe)
      let { confirmSingPremBudget } = initialData;
      if (
        initialData.savAcc + initialData.fixDeposit + initialData.invest !==
        0
      ) {
        confirmSingPremBudget =
          initialData.singPrem >
          (initialData.savAcc + initialData.fixDeposit + initialData.invest) / 2
            ? "Y"
            : "N";
      }

      return Object.assign({}, initialData, {
        netWorth,
        assets,
        liabilities,
        eiPorf,
        aInsPrem,
        sInsPrem,
        mIncome,
        netMIcncome,
        totAIncome,
        insurancePrem,
        totMExpense,
        totAExpense,
        aDisposeIncome,
        mDisposeIncome,
        confirmBudget,
        confirmSingPremBudget
      });
    }
    case "SPO": {
      const initialData = Object.assign(
        {
          init: true,
          // net worth
          savAcc: 0,
          fixDeposit: 0,
          invest: 0,
          property: 0,
          car: 0,
          cpfOa: 0,
          cpfSa: 0,
          cpfMs: 0,
          srs: 0,
          otherAssetTitle: "",
          otherAsset: 0,
          mortLoan: 0,
          motLoan: 0,
          eduLoan: 0,
          otherLiabilitesTitle: "",
          otherLiabilites: 0,
          noALReason: "",
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: "",
          // cash flow
          lessMCPF: 0,
          aBonus: 0,
          personExpenses: 0,
          householdExpenses: 0,
          regSav: 0,
          otherExpenseTitle: "",
          otherExpense: 0,
          additionComment: "",
          noCFReason: ""
        },
        fe.spouse || {},
        {
          cid: relationshipObject.cid
        }
      );
      // net worth
      const assets =
        initialData.savAcc +
        initialData.fixDeposit +
        initialData.invest +
        initialData.property +
        initialData.car +
        initialData.cpfOa +
        initialData.cpfSa +
        initialData.cpfMs +
        initialData.srs +
        initialData.otherAsset;
      const liabilities =
        initialData.mortLoan +
        initialData.motLoan +
        initialData.eduLoan +
        initialData.otherLiabilites;
      const netWorth = assets - liabilities;
      // existing insurance portfolio
      const eiPorf =
        initialData.lifeInsProt +
        initialData.retirePol +
        initialData.ednowSavPol +
        initialData.eduFund +
        initialData.invLinkPol +
        initialData.disIncomeProt +
        initialData.ciPort +
        initialData.personAcc;
      const aInsPrem =
        initialData.pAInsPrem + initialData.sAInsPrem + initialData.oAInsPrem;
      const sInsPrem =
        initialData.caInsPrem +
        initialData.csInsPrem +
        initialData.mdInsPrem +
        initialData.fpInsPrem;
      // cash flow
      const mIncome = Number.isNaN(
        dependantProfilesData[relationshipObject.cid].allowance
      )
        ? 0
        : dependantProfilesData[relationshipObject.cid].allowance;
      const netMIcncome = mIncome - initialData.lessMCPF;
      const totAIncome = netMIcncome * 12 + initialData.aBonus;

      // get insurancePrem
      let insurancePrem = initialData.sAInsPrem + (fe.owner.sAInsPrem || 0);
      if (fe.dependants && Array.isArray(fe.dependants)) {
        fe.dependants.forEach(dependant => {
          insurancePrem += dependant.sAInsPrem || 0;
        });
      }
      insurancePrem = Math.round((insurancePrem / 12) * 100) / 100;

      const totMExpense =
        initialData.personExpenses +
        initialData.householdExpenses +
        insurancePrem +
        initialData.regSav +
        initialData.otherExpense;
      const totAExpense = totMExpense * 12;
      const aDisposeIncome = totAIncome - totAExpense;
      const mDisposeIncome = Math.round(aDisposeIncome / 12);

      return Object.assign({}, initialData, {
        netWorth,
        assets,
        liabilities,
        eiPorf,
        aInsPrem,
        sInsPrem,
        mIncome,
        netMIcncome,
        totAIncome,
        insurancePrem,
        totMExpense,
        totAExpense,
        aDisposeIncome,
        mDisposeIncome
      });
    }
    case "SON":
    case "DAU": {
      const dependantData = _.find(
        fe.dependants,
        opt => opt.cid === relationshipObject.cid
      );
      const initialData = Object.assign(
        {
          init: true,
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: ""
        },
        dependantData || {},
        {
          cid: relationshipObject.cid
        }
      );
      // existing insurance portfolio
      const eiPorf =
        initialData.lifeInsProt +
        initialData.retirePol +
        initialData.ednowSavPol +
        initialData.eduFund +
        initialData.invLinkPol +
        initialData.disIncomeProt +
        initialData.ciPort +
        initialData.personAcc;
      const aInsPrem =
        initialData.pAInsPrem + initialData.sAInsPrem + initialData.oAInsPrem;
      const sInsPrem =
        initialData.caInsPrem +
        initialData.csInsPrem +
        initialData.mdInsPrem +
        initialData.fpInsPrem;

      return Object.assign({}, initialData, {
        eiPorf,
        aInsPrem,
        sInsPrem
      });
    }
    default:
      throw new Error(
        `feData got unexpected relationship "${
          relationshipObject.relationship
        }".`
      );
  }
};

export const getNeedsSummaryOptions = ({
  subType,
  fnaValue,
  profile,
  dependantProfiles,
  pda
}) => {
  const id = _.toLower(subType) === "shortfall" ? "sfAspects" : "spAspects";

  const { dependants } = profile;
  const { applicant, dependants: pdaDependants } = pda;

  const aspects = fnaValue.aspects ? fnaValue.aspects.split(",") : undefined;
  const cAspectValue = fnaValue[id];

  let listNoforSurplus;
  const fnaError = {};
  // TODO update validation without dynamic form logic
  // _na.validateFNA(context, needs.template.fnaForm, changedValues, fnaError);

  const seperateId = [PCHEADSTART];
  const seperateObj = getSeperateObj(id);
  const groupId = HCPROTECTION;

  let options = [];
  let inCompleteFlag;
  _.forEach(aspects, aspect => {
    let aspectValue = fnaValue[aspect];
    const aError = fnaError[aspect];
    inCompleteFlag = false;
    // Selected the aspect but not yet fil the fna, initialize the value
    if (_.isUndefined(aspectValue) || _.isEmpty(aspectValue)) {
      aspectValue = {};
      aspectValue.owner = profile;
      aspectValue.spouse = getSpouseProfile({ profile, dependantProfiles });
      aspectValue.dependants = [];
      _.forEach(aspectValue.owner.dependants, dep => {
        if (_.isUndefined(aspectValue.spouce)) {
          aspectValue.dependants.push(dependantProfiles[dep.cid]);
        } else if (
          dep.cid !== aspectValue.spouse.cid &&
          dependantProfiles[dep.cid]
        ) {
          aspectValue.dependants.push(dependantProfiles[dep.cid]);
        }
      });
      inCompleteFlag = true;
    }

    const aspectItem = _.find(
      getAspectOptions(),
      option => option.id === aspect
    );
    const ownerCid = _.get(aspectValue, "owner.cid");
    if (
      aspectItem &&
      filterAspect({
        relationship: aspectItem.relationship,
        filter: aspectItem.filter,
        dependants,
        pdaDependants,
        applicant,
        cid: ownerCid,
        type: RELATIONSHIP_TYPE.OWNER
      }) &&
      _.get(aspectValue, "owner.isActive") &&
      aspectValue.owner &&
      _.isBoolean(_.get(aspectValue, "owner.init"))
    ) {
      inCompleteFlag = !_.isEmpty(_.get(aError, "owner.code"));

      options = options.concat(
        concatPriorityOption({
          profile,
          dependantProfiles,
          aspectItem,
          aspectValue: aspectValue.owner,
          seperateObj,
          seperateId,
          aspect,
          relationship: RELATIONSHIP_TYPE.OWNER,
          inCompleteFlag
        })
      );
    }
    const spouseId = _.get(aspectValue, "spouse.cid");
    if (
      aspectItem &&
      filterAspect({
        relationship: aspectItem.relationship,
        filter: aspectItem.filter,
        dependants,
        pdaDependants,
        applicant,
        cid: spouseId,
        type: RELATIONSHIP_TYPE.SPOUSE
      }) &&
      _.get(aspectValue, "spouse.isActive") &&
      aspectValue.spouse &&
      _.isBoolean(_.get(aspectValue, "spouse.init"))
    ) {
      inCompleteFlag = !_.isEmpty(_.get(aError, "spouse.code"));

      options = options.concat(
        concatPriorityOption({
          profile,
          dependantProfiles,
          aspectItem,
          aspectValue: aspectValue.spouse,
          seperateObj,
          seperateId,
          aspect,
          relationship: RELATIONSHIP_TYPE.SPOUSE,
          inCompleteFlag
        })
      );
    }
    if (aspectItem && !_.isEmpty(aspectValue.dependants)) {
      _.forEach(aspectValue.dependants, de => {
        if (
          filterAspect({
            relationship: aspectItem.relationship,
            filter: aspectItem.filter,
            dependants,
            pdaDependants,
            applicant,
            cid: de.cid,
            type: RELATIONSHIP_TYPE.DEPENDANTS
          }) &&
          de &&
          de.isActive &&
          _.isBoolean(de.init)
        ) {
          inCompleteFlag =
            !_.isEmpty(_.get(aError, `dependants.${de.cid}.code`)) ||
            _.get(aError, `dependants.${de.cid}.skip`);

          options = options.concat(
            concatPriorityOption({
              profile,
              dependantProfiles,
              aspectItem,
              aspectValue: de,
              seperateObj,
              seperateId,
              aspect,
              relationship: RELATIONSHIP_TYPE.OWNER,
              inCompleteFlag
            })
          );
        }
      });
    }
    // }
  });

  let orderedCid = [{ cid: profile.cid }];
  const orderedDepCid = [];
  const tempOrderedDepCid = _.cloneDeep(dependantProfiles) || [];
  _.forEach(Object.keys(tempOrderedDepCid || []), key => {
    orderedDepCid.push(tempOrderedDepCid[key]);
  });
  const orderedOptions = [];
  orderedDepCid.sort(sortProfiles);
  orderedCid = orderedCid.concat(orderedDepCid);

  if (groupId) {
    const gids = groupId.split(",");
    _.forEach(gids, gid => {
      const opts = [];
      for (let i = options.length - 1; i >= 0; i -= 1) {
        const opt = options[i];
        if (opt.aid === gid) {
          opts.push(opt);
          options.splice(i, 1);
        }
      }

      let tempOption;
      _.forEach(orderedCid, obj => {
        tempOption = _.find(opts, ["cid", obj.cid]);
        if (_.isObject(tempOption)) orderedOptions.push(tempOption);
      });

      let cid = "";
      let name = "";
      if (orderedOptions.length) {
        _.forEach(orderedOptions, opt => {
          cid += (cid !== "" ? "," : "") + opt.cid;
          name += (name !== "" ? ", " : "") + opt.name;
        });
        options.push({
          id: gid,
          cid,
          name,
          aid: gid,
          aTitle: orderedOptions[0].aTitle,
          image: orderedOptions[0].image,
          inCompleteOption: orderedOptions[0].inCompleteOption
        });
      }
    });
  }

  listNoforSurplus = options.length;
  if (_.toUpper(subType) === "SHORTFALL") {
    options = options.filter(o => o.totShortfall < 0 || o.aid === HCPROTECTION);
  } else if (_.toUpper(subType) === "SURPLUS") {
    options = options.filter(
      o => o.aid !== HCPROTECTION && (o.totShortfall >= 0 || !o.totShortfall)
    );
    listNoforSurplus -= options.length;
  }

  if (cAspectValue) {
    const cOptions = [];
    _.forEach(cAspectValue, c => {
      const oi = _.findIndex(
        options,
        o =>
          ((groupId && groupId.indexOf(c.aid) > -1) || o.cid === c.cid) &&
          o.aid === c.aid
      );
      if (oi !== -1) {
        cOptions.push(options[oi]);
        options.splice(oi, 1);
      }
    });

    _.forEach(options, opt => {
      cOptions.push(opt);
    });
    options = cOptions;
  }

  return [options, listNoforSurplus];
};
