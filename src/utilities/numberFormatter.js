import numeral from "numeral";
import * as _ from "lodash";
import * as NUMBER_FORMAT from "../constants/NUMBER_FORMAT";

const { MAX_CURRENCY_VALUE } = NUMBER_FORMAT;
/**
 * numberToCurrency
 * @description convert number into currency format
 * @param {number} value - inputted number
 * @param {number} [decimal=2] - number of decimal in returned string
 * @param {number} [isFocus=false] - number of decimal in returned string
 * @param {number} [maxValue=MAX_CURRENCY_VALUE]
 * @return {string} - string in currency format
 * */
export const numberToCurrency = ({
  value,
  decimal = 2,
  isFocus = false,
  maxValue = MAX_CURRENCY_VALUE
}) => {
  if (_.isNumber(value)) {
    const trueValue =
      Number(value) > maxValue
        ? Number(String(value).substring(0, String(maxValue).length))
        : Number(value);
    let format = "0,0";
    const dotFormat = isFocus ? "[.]" : ".";

    if (decimal > 0) {
      format += dotFormat;
      if (isFocus) {
        format += "[";
      }
      format += _.repeat("0", decimal);
      if (isFocus) {
        format += "]";
      }
    }

    return `${numeral(trueValue).format(format)}`;
  }
  return value || "";
};

/**
 * numberToCurrencyForQuotation
 * @description convert number into currency format for Quotation Page
 * @param {number} value - inputted number
 * @param {number} [decimal=2] - number of decimal in returned string
 * @param {number} [isFocus=false] - number of decimal in returned string
 * @param {number} [maxValue=MAX_CURRENCY_VALUE]
 * @return {string} - string in currency format
 * */
export const numberToCurrencyForQuotation = ({
  value,
  decimal = 2,
  isFocus = false,
  maxValue = MAX_CURRENCY_VALUE
}) => {
  if (_.isNumber(value)) {
    const trueValue =
      Number(value) > maxValue
        ? Number(String(value).substring(0, String(maxValue).length))
        : Number(value);
    let format = "0,0";
    const dotFormat = isFocus ? "[.]" : ".";

    if (decimal > 0) {
      format += dotFormat;
      if (isFocus) {
        format += "[";
      }
      format += _.repeat("0", decimal);
      if (isFocus) {
        format += "]";
      }
    }

    return `${numeral(trueValue).format(format, Math.floor)}`;
  }
  return value || "";
};

/**
 * currencyToNumber
 * @description convert currency format into number
 * @param {string} value - inputted string
 * @param {string} maxValue - maximum value allowed
 * @param {boolean} allowNegative - allow negative number
 * @return {number} - number without format
 * */
export const currencyToNumber = (
  value,
  maxValue = MAX_CURRENCY_VALUE,
  allowNegative = false
) => {
  if (_.isString(value)) {
    if (!allowNegative) value = _.replace(value, /[-]/g, "");
    const negativeValue = _.replace(value, /[-]{2,}/, "-");
    let cleanValue = _.replace(negativeValue, /[^0-9,.-]/g, "");

    const valueSplitter = cleanValue.split(".");
    if (valueSplitter.length > 1) {
      cleanValue = `${valueSplitter[0]}.${valueSplitter[1]}`;
    }

    cleanValue = cleanValue.replace(/[,]/g, "");
    if (Number(cleanValue) > maxValue) {
      cleanValue = cleanValue.substring(0, maxValue.toString().length);
    }

    return numeral(
      cleanValue.slice(-1) === "."
        ? cleanValue.slice(0, cleanValue.length - 1)
        : cleanValue
    ).value();
  }
  return value;
};
