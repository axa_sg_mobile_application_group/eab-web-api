import * as _ from "lodash";
import * as date from "./date";

export function getCurrency({ value, sign, decimals }) {
  if (!_.isNumber(value)) {
    return "-";
  }
  if (!_.isNumber(decimals)) {
    decimals = 2;
  }
  const text =
    (sign && sign.trim().length > 0 ? sign : "") +
    parseFloat(value).toFixed(decimals);
  const parts = text.split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

export function getCurrencySign({ compCode, ccy, optionsMap }) {
  const currency = _.find(
    optionsMap.ccy.currencies,
    c => c.compCode === compCode
  );
  if (currency) {
    const option = _.find(currency.options, opt => opt.value === ccy);
    if (option) {
      return option.symbol;
    }
  }
  return "S$";
}

export function getOptionTitle({ value, optionsMap, language }) {
  const { options } = optionsMap;
  let option;
  if (options instanceof Array) {
    option = _.find(options, o => o.value === value);
  } else {
    option = _.find(options, (o, key) => key === value);
  }
  return option && option.title[language];
}

export function isEmpty(value) {
  // if type == date, return false
  if (_.isDate(value)) {
    return false;
  } else if (_.isBoolean(value)) {
    return false;
  }
  // if type == number and != 0, return false
  else if (_.isNumber(value) && value !== 0) {
    return false;
  }

  if (_.isPlainObject(value) || _.isArray(value)) {
    let empty = true;
    _.forEach(value, v => {
      if (!isEmpty(v)) {
        empty = false;
      }
    });
    return empty;
  }

  return _.isEmpty(value);
}

export function calcAge(birthday) {
  const today = new Date();
  const birthDate = new Date(birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }
  return age;
}

export function fillTextVariable({ text, data, EABi18n, language, textStore }) {
  let returnText = text;

  if (data.length > 0) {
    _.forEach(data, (item, index) => {
      returnText = _.replace(
        returnText,
        `{${index + 1}}`,
        item.isPath
          ? EABi18n({ path: item.value, language, textStore })
          : item.value
      );
    });
  }

  return returnText;
}

export function getAgeByUnit(unit, targetDate, dob) {
  switch (unit) {
    case "day":
      return date.getAttainedAge(targetDate, dob).day;
    case "month":
      return date.getAttainedAge(targetDate, dob).month;
    case "year":
      return date.getAttainedAge(targetDate, dob).year;
    case "nextAge":
      return date.getAgeNextBirthday(targetDate, dob);
    case "nearestAge":
    default:
      return date.getNearestAge(targetDate, dob);
  }
}

export function replaceAll(string, search, replacement) {
  return string.split(search).join(replacement);
}

export function getEmailContent(content, params, imageCode) {
  let ret = content;
  _.each(params, (value, key) => {
    ret = replaceAll(ret, `{{${key}}}`, value);
  });
  ret = replaceAll(ret, imageCode, "");
  return ret;
}

export function getCountryWithCityList({ optionsMap, optionPath }) {
  const countryWithCityList = [];
  const optionList = _.get(optionsMap, optionPath).options;
  optionList.map(option => {
    if (option.condition && countryWithCityList.indexOf(option.condition) < 0) {
      countryWithCityList.push(option.condition);
    }
    return true;
  });
  return countryWithCityList;
}
