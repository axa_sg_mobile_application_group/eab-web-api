export default function({ planDetails, quotation }) {
  const bpDetail = planDetails[quotation.baseProductCode];

  return bpDetail && bpDetail.fundInd === "Y";
}
