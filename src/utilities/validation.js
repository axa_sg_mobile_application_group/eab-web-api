import * as _ from "lodash";
import TEXT_STORE, { LANGUAGE_TYPES } from "../locales";
import {
  TEXT_FIELD,
  TEXT_BOX,
  TEXT_SELECTION,
  TEXTSELECTION,
  TEXTFIELD,
  TEXT,
  TEXTAREA,
  CHECKBOX,
  QRADIOGROUP,
  DATEPICKER,
  PICKER
} from "../constants/FIELD_TYPES";
import PAYMENT_BANK from "../constants/PAYMENT_BANK";
import { show, setMandatory } from "./trigger";
import * as ROP_TABLE from "../constants/ROP_TABLE";
import * as DYNAMIC_VIEWS from "../constants/DYNAMIC_VIEWS";

export function validateID({ ID, IDDocumentType }) {
  function validateFunction(checkArray) {
    const IDCopy = ID;

    /* check length */
    if (IDCopy.length !== 9) {
      return false;
    }

    /* check format */
    const icArray = [];
    const supportArray = [2, 7, 6, 5, 4, 3, 2];

    for (let i = 0; i < 9; i += 1) {
      icArray[i] = IDCopy.charAt(i);
    }

    for (let i = 1; i < 8; i += 1) {
      icArray[i] = parseInt(icArray[i], 10) * supportArray[i - 1];
    }

    /* get weight */
    let weight = 0;
    for (let i = 1; i < 8; i += 1) {
      weight += icArray[i];
    }

    const offset = icArray[0] === "T" || icArray[0] === "G" ? 4 : 0;
    const temp = (offset + weight) % 11;

    const theAlpha = checkArray[temp];
    if (icArray[8] !== theAlpha) {
      return false;
    }

    /* all check pass */
    return true;
  }

  if (ID !== "") {
    let supportArray = [];
    switch (IDDocumentType) {
      case "nric": {
        if (ID[0] === "S" || ID[0] === "T") {
          supportArray = [
            "J",
            "Z",
            "I",
            "H",
            "G",
            "F",
            "E",
            "D",
            "C",
            "B",
            "A"
          ];
        }
        return validateFunction(supportArray)
          ? {
              hasError: false,
              message: {
                [LANGUAGE_TYPES.ENGLISH]: "",
                [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
              }
            }
          : {
              hasError: true,
              message: {
                [LANGUAGE_TYPES.ENGLISH]:
                  TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.604"],
                [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
              }
            };
      }
      case "fin": {
        if (ID[0] === "F" || ID[0] === "G") {
          supportArray = [
            "X",
            "W",
            "U",
            "T",
            "R",
            "Q",
            "P",
            "N",
            "M",
            "L",
            "K"
          ];
        }
        return validateFunction(supportArray)
          ? {
              hasError: false,
              message: {
                [LANGUAGE_TYPES.ENGLISH]: "",
                [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
              }
            }
          : {
              hasError: true,
              message: {
                [LANGUAGE_TYPES.ENGLISH]:
                  TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.606"],
                [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
              }
            };
      }
      default:
        break;
    }
  }
  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

export function validateEmail(email) {
  return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email)
    ? {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      }
    : {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.401"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
}

/**
 * validateText
 * @description validate TextField / TextBox
 * @param cousMessage a string with message code stroed in TEXT_STORE
 * */
export function validateText({ field, value }) {
  const { min, max } = field;
  if (_.isNumber(min)) {
    if (_.isString(value) && _.toLength(value) < min) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.407"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    } else if (_.isNumber(value) && value < min) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.405"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    }
  }

  if (_.isNumber(max)) {
    if (_.isString(value) && _.toLength(value) > max) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.406"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    } else if (_.isNumber(value) && value > max) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.404"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    }
  }

  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

/**
 * validateBankNumber
 * @description validate validateBankNumber
 * @param cousMessage a string with message code stroed in TEXT_STORE
 * */
export function validateBankNumber({ field, value }) {
  const { bankName } = field;
  let max = PAYMENT_BANK[bankName] ? PAYMENT_BANK[bankName].maxLength : 0;
  if (field.cpf) {
    max = PAYMENT_BANK[bankName] ? PAYMENT_BANK[bankName].cpfMaxLength : 0;
  }

  if (_.isNumber(max)) {
    let path = "error.302";
    switch (max) {
      case 14:
        path =
          "paymentAndSubmission.section.payment.placeholder.accountNumberLength14";
        break;
      case 13:
        path =
          "paymentAndSubmission.section.payment.placeholder.accountNumberLength13";
        break;
      case 12:
        path =
          "paymentAndSubmission.section.payment.placeholder.accountNumberLength12";
        break;
      case 9:
        path =
          "paymentAndSubmission.section.payment.placeholder.accountNumberLength9";
        break;
      default:
        path = "error.302";
    }
    if (value.toString().length === 0) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    } else if (value.toString().length !== max) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][path],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    }
  }

  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

/**
 * validateMandatory
 * @description validate mandatory for component
 * */
export function validateMandatory({ field, value }) {
  const { type, mandatory, disabled, numberAllowZero } = field;
  const fieldType = _.toUpper(type);

  if (
    !disabled &&
    mandatory &&
    [TEXT_FIELD, TEXT_BOX, TEXT_SELECTION].indexOf(fieldType) >= 0
  ) {
    if (
      (_.isString(value) && _.isEmpty(value)) ||
      _.isNull(value) ||
      (_.isNumber(value) && !numberAllowZero && value === 0) ||
      _.isNaN(value) ||
      value === undefined ||
      value === ""
    ) {
      return {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
            TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
        }
      };
    }
  }

  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

/**
 * validateMandatoryForApplicationForm
 * TODO: This function should be removed, and use validateMandatory
 * @description validate mandatory for ApplicationForm component
 * */
function validateMandatoryForApplicationForm({
  field,
  value,
  rootValues,
  path
}) {
  const { type, mandatory, disabled, numberAllowZero, id, validation } = field;
  const fieldType = _.toUpper(type);

  if ((!disabled && mandatory) || _.includes(id, "LIFESTYLE02")) {
    if (
      [
        TEXTSELECTION,
        TEXTFIELD,
        TEXT,
        TEXTAREA,
        TEXT_SELECTION,
        TEXT_FIELD,
        TEXT_BOX
      ].indexOf(fieldType) >= 0
    ) {
      if (
        (_.isString(value) && _.isEmpty(value)) ||
        _.isNull(value) ||
        (_.isNumber(value) && !numberAllowZero && value === 0) ||
        _.isNaN(value) ||
        _.isUndefined(value)
      ) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
          }
        };
      }
      if (id === "HEALTH16a" && value < 4) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.310"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.310"]
          }
        };
      }
    } else if (
      [CHECKBOX].indexOf(fieldType) >= 0 &&
      !_.includes(id, "LIFESTYLE02")
    ) {
      if (value !== "Y") {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.301"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]
          }
        };
      }
    } else if (
      [CHECKBOX].indexOf(fieldType) >= 0 &&
      _.includes(id, "LIFESTYLE02") &&
      !_.includes(id, "LIFESTYLE02a")
    ) {
      if (
        _.get(rootValues, `${path}.LIFESTYLE02a_1`) !== "Y" &&
        _.get(rootValues, `${path}.LIFESTYLE02b_1`) !== "Y" &&
        _.get(rootValues, `${path}.LIFESTYLE02c_1`) !== "Y"
      ) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: "",
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
          }
        };
      }
    } else if (
      [CHECKBOX].indexOf(fieldType) >= 0 &&
      _.includes(id, "LIFESTYLE02a")
    ) {
      if (
        _.get(rootValues, `${path}.LIFESTYLE02a_1`) !== "Y" &&
        _.get(rootValues, `${path}.LIFESTYLE02b_1`) !== "Y" &&
        _.get(rootValues, `${path}.LIFESTYLE02c_1`) !== "Y"
      ) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.301"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]
          }
        };
      }
    } else if ([QRADIOGROUP].indexOf(fieldType) >= 0) {
      if (value === "") {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.301"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]
          }
        };
      }
    } else if ([DATEPICKER].indexOf(fieldType) >= 0) {
      if (validation) {
        if (validation.type === "month_greater") {
          if (!_.isEmpty(value)) {
            const fromDate = value[validation.id];
            const toDate = value[id];
            if (toDate && fromDate) {
              if (
                new Date(fromDate.slice(0, 7)).valueOf() >
                new Date(toDate.slice(0, 7)).valueOf()
              ) {
                return {
                  hasError: true,
                  message: {
                    [LANGUAGE_TYPES.ENGLISH]:
                      TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.409"],
                    [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
                      TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE][
                        "error.409"
                      ]
                  }
                };
              }
            }
          }

          if (_.isEmpty(value[id])) {
            return {
              hasError: true,
              message: {
                [LANGUAGE_TYPES.ENGLISH]:
                  TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
                [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
                  TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
              }
            };
          }
        }
      } else if (_.isEmpty(value[id])) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
          }
        };
      }
    } else if ([PICKER].indexOf(fieldType) >= 0) {
      if (value === "") {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
          }
        };
      }
    }
  }

  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

const validateROPTableExisting = ({
  iErrorObj,
  iValues,
  iTemplate,
  baseProductCode
}) => {
  const {
    SUMASSURED,
    EXITSTING_TABLE_MAPPING,
    COMPANY,
    SHOW_EXIST_TABLE,
    SHOW_ACC_EXIST_TABLE
  } = ROP_TABLE;

  let isShow = false;

  if (baseProductCode) {
    if (baseProductCode === "BAA") {
      isShow = _.get(iValues, SHOW_ACC_EXIST_TABLE, "") === "Y";
    } else {
      isShow = _.get(iValues, SHOW_EXIST_TABLE, "") === "Y";
    }
  } else {
    isShow =
      (_.get(iValues, SHOW_EXIST_TABLE, "") === "Y" &&
        _.includes(iTemplate.presentation, SHOW_EXIST_TABLE, false)) ||
      (_.get(iValues, SHOW_ACC_EXIST_TABLE, "") === "Y" &&
        _.includes(iTemplate.presentation, SHOW_ACC_EXIST_TABLE, false));
  }

  if (!isShow) {
    _.each(COMPANY, company => {
      _.each(EXITSTING_TABLE_MAPPING[company], id => {
        if (_.get(iErrorObj, `${id}.hasError`)) {
          _.set(iErrorObj, `${id}`, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, `${id}`)) {
          _.set(iValues, `${id}`, 0);
        }
      });
    });
    return;
  }

  _.each(SUMASSURED, sumAssuredField => {
    const axaId = _.get(
      EXITSTING_TABLE_MAPPING,
      `${COMPANY.AXA}.${sumAssuredField}`,
      ""
    );
    const otherId = _.get(
      EXITSTING_TABLE_MAPPING,
      `${COMPANY.OTHER}.${sumAssuredField}`,
      ""
    );
    const totalId = _.get(
      EXITSTING_TABLE_MAPPING,
      `${COMPANY.TOTAL}.${sumAssuredField}`,
      ""
    );

    const axaValue = _.get(iValues, `${axaId}`, 0);
    const otherValue = _.get(iValues, `${otherId}`, 0);
    const totalValue = _.get(iValues, `${totalId}`, 0);

    if (axaValue + otherValue === totalValue) {
      if (
        _.get(iErrorObj, `${axaId}.hasError`) ||
        _.get(iErrorObj, `${otherId}.hasError`)
      ) {
        _.set(iErrorObj, `${axaId}`, {
          hasError: false,
          message: {}
        });
        _.set(iErrorObj, `${otherId}`, {
          hasError: false,
          message: {}
        });
      }
    } else if (
      !_.get(iErrorObj, `${axaId}.hasError`, false) ||
      !_.get(iErrorObj, `${otherId}.hasError`, false)
    ) {
      _.set(iErrorObj, `${axaId}`, {
        hasError: true,
        message: {}
      });
      _.set(iErrorObj, `${otherId}`, {
        hasError: true,
        message: {}
      });
    }
  });
};

const validateROPTablePending = ({ iErrorObj, iValues }) => {
  const {
    PENDING_LIFE_AXA,
    PENDING_CI_AXA,
    PENDING_TPD_AXA,
    PENDING_PAADB_AXA,
    PENDING_TOTALPREM_AXA,
    PENDING_LIFE_OTHER,
    PENDING_CI_OTHER,
    PENDING_TPD_OTHER,
    PENDING_PAADB_OTHER,
    PENDING_TOTALPREM_OTHER,
    SHOW_PENDING_TABLE,
    COMPANY,
    PENDING_TABLE_MAPPING
  } = ROP_TABLE;

  if (_.get(iValues, SHOW_PENDING_TABLE, "") !== "Y") {
    _.each(COMPANY, company => {
      _.each(PENDING_TABLE_MAPPING[company], id => {
        if (_.get(iErrorObj, `${id}.hasError`)) {
          _.set(iErrorObj, `${id}`, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, `${id}`)) {
          _.set(iValues, `${id}`, 0);
        }
      });
    });
    return;
  }

  const notTotalIds = [
    PENDING_LIFE_AXA,
    PENDING_CI_AXA,
    PENDING_TPD_AXA,
    PENDING_PAADB_AXA,
    PENDING_LIFE_OTHER,
    PENDING_CI_OTHER,
    PENDING_TPD_OTHER,
    PENDING_PAADB_OTHER
  ];

  const totalIds = [PENDING_TOTALPREM_AXA, PENDING_TOTALPREM_OTHER];
  let notTotalFieldsHasNonZero = false;
  let totalFieldsHasNonZero = false;

  _.each(notTotalIds, id => {
    const value = _.get(iValues, `${id}`);
    if (_.isNumber(value) && value !== 0) {
      notTotalFieldsHasNonZero = true;
      return false;
    }
    return true;
  });

  _.each(totalIds, id => {
    const value = _.get(iValues, `${id}`);
    if (_.isNumber(value) && value !== 0) {
      totalFieldsHasNonZero = true;
      return false;
    }
    return true;
  });

  if (notTotalFieldsHasNonZero) {
    _.each(notTotalIds, id => {
      _.set(iErrorObj, `${id}`, {
        hasError: false,
        message: {}
      });
    });
  } else {
    _.each(notTotalIds, id => {
      _.set(iErrorObj, `${id}`, {
        hasError: true,
        message: {}
      });
    });
  }

  if (totalFieldsHasNonZero) {
    _.each(totalIds, id => {
      _.set(iErrorObj, `${id}`, {
        hasError: false,
        message: {}
      });
    });
  } else {
    _.each(totalIds, id => {
      _.set(iErrorObj, `${id}`, {
        hasError: true,
        message: {}
      });
    });
  }
};

const validateROPTableReplace = ({ iErrorObj, iValues }) => {
  const {
    SUMASSURED,
    REPLACE_TABLE_MAPPING,
    COMPANY,
    REPLACE_POLICY_TYPE_AXA,
    REPLACE_POLICY_TYPE_OTHER,
    SHOW_REPLACE_TABLE,
    SHOW_ACC_TABLE
  } = ROP_TABLE;

  if (
    _.get(iValues, SHOW_REPLACE_TABLE, "") !== "Y" &&
    _.get(iValues, SHOW_ACC_TABLE, "") !== "Y"
  ) {
    _.each(COMPANY, company => {
      _.each(REPLACE_TABLE_MAPPING[company], id => {
        if (_.get(iErrorObj, `${id}.hasError`)) {
          _.set(iErrorObj, `${id}`, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, `${id}`)) {
          _.set(iValues, `${id}`, 0);
        }
      });
    });
    return;
  }

  let enableTypeOfPoliciesAXA = false;
  let enableTypeOfPoliciesOther = false;

  _.each(SUMASSURED, sumAssuredField => {
    const axaId = _.get(
      REPLACE_TABLE_MAPPING,
      `${COMPANY.AXA}.${sumAssuredField}`,
      ""
    );
    const otherId = _.get(
      REPLACE_TABLE_MAPPING,
      `${COMPANY.OTHER}.${sumAssuredField}`,
      ""
    );
    const totalId = _.get(
      REPLACE_TABLE_MAPPING,
      `${COMPANY.TOTAL}.${sumAssuredField}`,
      ""
    );

    const axaValue = _.get(iValues, `${axaId}`, 0);
    const otherValue = _.get(iValues, `${otherId}`, 0);
    const totalValue = _.get(iValues, `${totalId}`, 0);

    if (axaValue + otherValue === totalValue) {
      if (
        _.get(iErrorObj, `${axaId}.hasError`) ||
        _.get(iErrorObj, `${otherId}.hasError`)
      ) {
        _.set(iErrorObj, `${axaId}`, {
          hasError: false,
          message: {}
        });
        _.set(iErrorObj, `${otherId}`, {
          hasError: false,
          message: {}
        });
      }
    } else if (
      !_.get(iErrorObj, `${axaId}.hasError`, false) ||
      !_.get(iErrorObj, `${otherId}.hasError`, false)
    ) {
      _.set(iErrorObj, `${axaId}`, {
        hasError: true,
        message: {}
      });
      _.set(iErrorObj, `${otherId}`, {
        hasError: true,
        message: {}
      });
    }

    // type of policies
    if (axaValue !== 0 && !enableTypeOfPoliciesAXA) {
      enableTypeOfPoliciesAXA = true;
    }
    if (otherValue !== 0 && !enableTypeOfPoliciesOther) {
      enableTypeOfPoliciesOther = true;
    }
  });

  if (enableTypeOfPoliciesAXA) {
    if (_.isEmpty(_.get(iValues, REPLACE_POLICY_TYPE_AXA, ""))) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_AXA}`, {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.309"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
            TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.309"]
        }
      });
    } else if (_.get(iErrorObj, `${REPLACE_POLICY_TYPE_AXA}.hasError`)) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_AXA}`, {
        hasError: false,
        message: {}
      });
    }
  } else {
    _.set(iValues, `${REPLACE_POLICY_TYPE_AXA}`, "");
    if (_.get(iErrorObj, `${REPLACE_POLICY_TYPE_AXA}.hasError`)) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_AXA}`, {
        hasError: false,
        message: {}
      });
    }
  }

  if (enableTypeOfPoliciesOther) {
    if (_.isEmpty(_.get(iValues, REPLACE_POLICY_TYPE_OTHER, ""))) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_OTHER}`, {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.309"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
            TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.309"]
        }
      });
    } else if (_.get(iErrorObj, `${REPLACE_POLICY_TYPE_OTHER}.hasError`)) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_OTHER}`, {
        hasError: false,
        message: {}
      });
    }
  } else {
    _.set(iValues, `${REPLACE_POLICY_TYPE_OTHER}`, "");
    if (_.get(iErrorObj, `${REPLACE_POLICY_TYPE_OTHER}.hasError`)) {
      _.set(iErrorObj, `${REPLACE_POLICY_TYPE_OTHER}`, {
        hasError: false,
        message: {}
      });
    }
  }
};

export const validateROPTable = ({
  iErrorObj,
  iValues,
  iTemplate,
  baseProductCode
}) => {
  validateROPTableExisting({ iErrorObj, iValues, iTemplate, baseProductCode });
  validateROPTablePending({ iErrorObj, iValues });
  validateROPTableReplace({ iErrorObj, iValues });
};

function validateMandatoryForDynamicTable({ value }) {
  if (_.isUndefined(value) || _.isEmpty(value)) {
    return {
      hasError: true,
      message: {
        [LANGUAGE_TYPES.ENGLISH]:
          TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
        [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
          TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]
      }
    };
  }

  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

export function validateAssets({ rule, value }) {
  // TODO
  // keep this code
  // const assetsList = [
  //   {
  //     title: "Savings Account",
  //     value: "savAcc"
  //   },
  //   {
  //     title: "Fixed Deposits",
  //     value: "fixDeposit"
  //   },
  //   {
  //     title: "Investment(Mutual Fund/Direct investment",
  //     value: "invest"
  //   },
  //   {
  //     title: "CPF (OA)",
  //     value: "cpfOa"
  //   },
  //   {
  //     title: "CPF (SA)",
  //     value: "cpfSa"
  //   },
  //   {
  //     title: "CPF Medisave",
  //     value: "cpfMs"
  //   },
  //   {
  //     title: "SRS",
  //     value: "srs"
  //   },
  //   {
  //     title: "Please select assets",
  //     value: "all"
  //   }
  // ];
  // const targetTitle = assetsList.find(al => al.value === value[0].key).title;
  const validateArr = [];
  const assets = _.get(value, "targetData.assets", []);
  _.each(assets, (asset, indexkey) => {
    let validateValue = asset.calAsset;
    if (validateValue === "") {
      validateValue = null;
    }
    if (_.isString(validateValue)) {
      validateValue = parseInt(validateValue, 10);
    }
    let validateObj = validateMandatory({
      field: {
        type: TEXT_FIELD,
        mandatory: rule.mandatory,
        disabled: false
      },
      value: validateValue
    });
    if (!validateObj.hasError && rule.min) {
      validateObj = validateText({
        field: {
          min: rule.min
        },
        value: validateValue
      });
    }
    if (!validateObj.hasError && rule.max) {
      validateObj = validateText({
        field: {
          max: rule.max
        },
        value: asset.totalValue || validateValue
        // TODO
        // keep this code
        // cousMessage: {
        //   code: "na.analysis.assetsTotalValueError",
        //   replacement: [
        //     {
        //       text: "{title}",
        //       message: targetTitle
        //     }
        //   ]
        // }
      });
    }
    validateArr.push(
      Object.assign({}, validateObj, {
        index: indexkey
      })
    );
  });
  return validateArr;
}

export function validateNeedsAnalysis({ rule, value }) {
  if (!rule) {
    return {};
  }

  if (rule.type && !_.isEmpty(value) && value !== 0) {
    switch (rule.type) {
      case "int":
        value = _.toNumber(value);
        break;
      case "string":
        value = _.toString(value);
        break;
      default:
        break;
    }
  }
  let validateObj = {};
  if (rule.mandatory) {
    validateObj = validateMandatory({
      field: {
        type: TEXT_FIELD,
        mandatory: rule.mandatory,
        disabled: false,
        numberAllowZero: rule.numberAllowZero
      },
      value
    });
  }
  if (!validateObj.hasError && rule.min) {
    validateObj = validateText({
      field: {
        min: rule.min
      },
      value
    });
  }
  return validateObj;
}

/**
 * validateRecommendationROP
 * @description validate Replacement of Policy in Recommendation page
 * */
export function validateRecommendationROP({ field, value }) {
  const { mandatory, disabled } = field;
  const replaceLife = _.toNumber(_.get(value, "rop.replaceLife", 0));
  const replaceTpd = _.toNumber(_.get(value, "rop.replaceTpd", 0));
  const replaceCi = _.toNumber(_.get(value, "rop.replaceCi", 0));
  const replacePaAdb = _.toNumber(_.get(value, "rop.replacePaAdb", 0));

  const valid = !(
    replaceLife === 0 &&
    replaceTpd === 0 &&
    replaceCi === 0 &&
    replacePaAdb === 0
  );
  return !valid && !disabled && mandatory
    ? {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.801"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      }
    : {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
}

/**
 * validateBudgetChoice
 * @description validate Budget choice in Budget page
 * */
export function validateBudgetChoice({ field, value }) {
  const { mandatory, disabled } = field;
  const valid = value === "Y";
  return !valid && !disabled && mandatory
    ? {
        hasError: true,
        message: {
          [LANGUAGE_TYPES.ENGLISH]:
            TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      }
    : {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
}
/**
 * @description validate fe section net worth
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of net worth
 * */
export function validateNetWorth({ data, relationship }) {
  const {
    assets,
    liabilities,
    noALReason,
    otherAssetTitle,
    otherLiabilitesTitle,
    otherAsset,
    otherLiabilites
  } = data;

  // check section mandatory
  const netWorthError =
    relationship === "owner" &&
    assets === 0 &&
    liabilities === 0 &&
    noALReason === ""
      ? {
          hasError: true,
          message: "error.704"
        }
      : {
          hasError: false,
          message: ""
        };

  // check noAlReason mandatory
  const noALReasonError =
    relationship === "owner" &&
    assets === 0 &&
    liabilities === 0 &&
    noALReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherAssetTitle mandatory
  const otherAssetTitleError =
    otherAssetTitle === "" && otherAsset !== 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherAsset mandatory
  const otherAssetError =
    otherAssetTitle !== "" && otherAsset === 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherLiabilitesTitle mandatory
  const otherLiabilitesTitleError =
    otherLiabilitesTitle === "" && otherLiabilites !== 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherLiabilites mandatory
  const otherLiabilitesError =
    otherLiabilitesTitle !== "" && otherLiabilites === 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  return {
    netWorth: netWorthError,
    noALReason: noALReasonError,
    otherAssetTitleError,
    otherAsset: otherAssetError,
    otherLiabilitesTitleError,
    otherLiabilites: otherLiabilitesError
  };
}
/**
 * @description validate fe section existing insurance portfolio
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of existing insurance portfolio
 * */
export function validateEIP({ data, relationship }) {
  const { eiPorf, aInsPrem, sInsPrem, noEIReason, hospNSurg } = data;

  // check noEIReason mandatory
  const noEIReasonError =
    relationship === "owner" &&
    eiPorf === 0 &&
    hospNSurg === "" &&
    aInsPrem === 0 &&
    sInsPrem === 0 &&
    noEIReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check assured section mandatory
  const assuredError =
    eiPorf === 0 && hospNSurg === "" && (aInsPrem !== 0 || sInsPrem !== 0)
      ? {
          hasError: true,
          message: "error.714"
        }
      : {
          hasError: false,
          message: ""
        };

  // check premiums section mandatory
  const premiumsError =
    (eiPorf !== 0 || hospNSurg !== "") && aInsPrem === 0 && sInsPrem === 0
      ? {
          hasError: true,
          message: "error.715"
        }
      : {
          hasError: false,
          message: ""
        };

  return {
    noEIReason: noEIReasonError,
    assured: assuredError,
    premiums: premiumsError
  };
}
/**
 * @description validate fe section cash flow
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of cash flow
 * */
export function validateCashFlow({ data, relationship }) {
  const {
    totAIncome,
    totAExpense,
    otherExpenseTitle,
    otherExpense,
    noCFReason
  } = data;

  // check CFReason mandatory
  const noCFReasonError =
    relationship === "owner" &&
    (totAIncome === 0 || totAExpense === 0) &&
    noCFReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherExpenseTitle mandatory
  const otherExpenseTitleError =
    otherExpenseTitle === "" && otherExpense !== 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check otherExpense mandatory
  const otherExpenseError =
    otherExpenseTitle !== "" && otherExpense === 0
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  return {
    noCFReason: noCFReasonError,
    otherExpenseTitle: otherExpenseTitleError,
    otherExpense: otherExpenseError
  };
}
/**
 * @description validate fe section budget (all budgets) and force income
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @return {object} - error object of budget (all budgets) and force income
 * */
export function validateBudgetAndForceIncome(data) {
  const {
    cpfOa,
    cpfSa,
    cpfMs,
    srs,
    forceIncome,
    forceIncomeReason,
    aRegPremBudget,
    aRegPremBudgetSrc,
    confirmBudget,
    confirmBudgetReason,
    singPrem,
    singPremSrc,
    confirmSingPremBudget,
    confirmSingPremBudgetReason,
    cpfOaBudget,
    cpfSaBudget,
    cpfMsBudget,
    srsBudget
  } = data;

  // check budget section mandatory
  const budgetError =
    aRegPremBudget === 0 &&
    singPrem === 0 &&
    cpfOaBudget === 0 &&
    cpfSaBudget === 0 &&
    cpfMsBudget === 0 &&
    srsBudget === 0
      ? {
          hasError: true,
          message: "error.703"
        }
      : {
          hasError: false,
          message: ""
        };

  // check aRegPremBudgetSrc mandatory
  const aRegPremBudgetSrcError =
    aRegPremBudget !== 0 && aRegPremBudgetSrc === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check confirmBudget mandatory
  const confirmBudgetError =
    aRegPremBudget !== 0 && confirmBudget === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check confirmBudgetReason mandatory
  const confirmBudgetReasonError =
    aRegPremBudget !== 0 && confirmBudget === "Y" && confirmBudgetReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check singPremSrc mandatory
  const singPremSrcError =
    singPrem !== 0 && singPremSrc === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check confirmSingPremBudget mandatory
  const confirmSingPremBudgetError =
    singPrem !== 0 && confirmSingPremBudget === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check confirmBudgetReason mandatory
  const confirmSingPremBudgetReasonError =
    singPrem !== 0 &&
    confirmSingPremBudget === "Y" &&
    confirmSingPremBudgetReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // Check if the cpf budget is over the asset
  let cpfOaBudgetError = {};
  let cpfSaBudgetError = {};
  let cpfMsBudgetError = {};
  let srsBudgetError = {};
  if (budgetError.hasError) {
    cpfOaBudgetError = budgetError;
    cpfSaBudgetError = budgetError;
    cpfMsBudgetError = budgetError;
    srsBudgetError = budgetError;
  } else {
    cpfOaBudgetError =
      cpfOaBudget > cpfOa
        ? {
            hasError: true,
            message: "error.706"
          }
        : {
            hasError: false,
            message: ""
          };
    cpfSaBudgetError =
      cpfSaBudget > cpfSa
        ? {
            hasError: true,
            message: "error.706"
          }
        : {
            hasError: false,
            message: ""
          };
    cpfMsBudgetError =
      cpfMsBudget > cpfMs
        ? {
            hasError: true,
            message: "error.706"
          }
        : {
            hasError: false,
            message: ""
          };
    srsBudgetError =
      srsBudget > srs
        ? {
            hasError: true,
            message: "error.706"
          }
        : {
            hasError: false,
            message: ""
          };
  }

  // check forceIncome mandatory
  const forceIncomeError =
    forceIncome === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  // check forceIncomeReason mandatory
  const forceIncomeReasonError =
    forceIncome === "Y" && forceIncomeReason === ""
      ? {
          hasError: true,
          message: "error.302"
        }
      : {
          hasError: false,
          message: ""
        };

  return {
    budget: budgetError,
    aRegPremBudgetSrc: aRegPremBudgetSrcError,
    confirmBudget: confirmBudgetError,
    confirmBudgetReason: confirmBudgetReasonError,
    singPremSrc: singPremSrcError,
    confirmSingPremBudget: confirmSingPremBudgetError,
    confirmSingPremBudgetReason: confirmSingPremBudgetReasonError,
    forceIncome: forceIncomeError,
    forceIncomeReason: forceIncomeReasonError,
    cpfOaBudget: cpfOaBudgetError,
    cpfSaBudget: cpfSaBudgetError,
    cpfMsBudget: cpfMsBudgetError,
    srsBudget: srsBudgetError
  };
}

function deleteErrorAndValues({ iTemplate, iValues, iErrorObj }) {
  const id = _.get(iTemplate, "id") || _.get(iTemplate, "key");

  if (!_.isUndefined(id)) {
    if (_.includes(DYNAMIC_VIEWS.DUPLICATE_IDS, id)) {
      return;
    }
    if (iValues && _.has(iValues, id)) {
      delete iValues[id];
    }
    if (iErrorObj && _.has(iErrorObj, id)) {
      delete iErrorObj[id];
    }
  }

  if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, item => {
      deleteErrorAndValues({ iTemplate: item, iValues, iErrorObj });
    });
  }
}

export function validateApplicationFormPolicyTable() {
  // TODO: logic for validate saTable
  return {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
}

export function validateApplicationFormTemplate({
  iTemplate,
  iValues,
  iErrorObj,
  rootValues,
  path
}) {
  const { type } = iTemplate;

  const showTemplate = show({
    template: iTemplate,
    rootValues,
    valuePath: path
  });

  if (!showTemplate) {
    deleteErrorAndValues({ iTemplate, iValues, iErrorObj });
    return;
  }

  setMandatory({ template: iTemplate, rootValues, valuePath: path });

  if (
    type &&
    (_.toUpper(type) === "TABLE-BLOCK-T" ||
      _.toUpper(type) === "TABLE-BLOCK" ||
      _.toUpper(type) === "BLOCK")
  ) {
    if (_.get(iTemplate, "mandatory") && _.has(iTemplate, "items")) {
      const blockId = iTemplate.id || iTemplate.key;
      const hasError = !_.some(
        iTemplate.items,
        item => _.get(iValues, item.id) === "Y"
      );
      if (hasError) {
        iErrorObj[blockId] = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.301"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]
          }
        };
      } else if (_.get(iErrorObj, `${blockId}.hasError`)) {
        _.set(iErrorObj, `${blockId}.hasError`, false);
      }
    }
  }

  if (
    _.isString(_.get(iTemplate, "type")) &&
    _.toUpper(_.get(iTemplate, "type")) === "TABLE"
  ) {
    _.set(
      iErrorObj,
      iTemplate.id,
      validateMandatoryForDynamicTable({ value: iValues[iTemplate.id] })
    );
  } else if (_.toUpper(_.get(iTemplate, "type")) === "CHECKBOX") {
    if (!_.has(iValues, iTemplate.id)) {
      _.set(rootValues, `${path}.${iTemplate.id}`, "N");
    }
    _.set(
      iErrorObj,
      iTemplate.id,
      validateMandatoryForApplicationForm({
        field: {
          type: "CHECKBOX",
          mandatory: iTemplate.mandatory,
          disabled: !!iTemplate.disabled,
          id: iTemplate.id
        },
        value: iValues[iTemplate.id] || "",
        rootValues,
        path
      })
    );
  } else if (
    _.toUpper(_.get(iTemplate, "type")) === "PICKER" &&
    _.has(iTemplate, "value") &&
    !_.has(rootValues, `${path}.${iTemplate.id}`)
  ) {
    _.set(rootValues, `${path}.${iTemplate.id}`, _.get(iTemplate, "value"));
  } else if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, item => {
      validateApplicationFormTemplate({
        iTemplate: item,
        iValues,
        iErrorObj,
        rootValues,
        path
      });
    });
  } else if (iTemplate.id) {
    if (iTemplate.type === "saTable") {
      // TODO validation for saTable
      validateROPTable({ iErrorObj, iValues, iTemplate });
    } else {
      _.set(
        iErrorObj,
        iTemplate.id,
        validateMandatoryForApplicationForm({
          field: {
            type: TEXT_SELECTION,
            mandatory: iTemplate.mandatory,
            disabled: !!iTemplate.disabled,
            id: iTemplate.id
          },
          value: iValues[iTemplate.id] || ""
        })
      );
    }
  }
}

export function validateApplicationFormTableDialog({
  iTemplate,
  iValues,
  iErrorObj,
  rootValues,
  path
}) {
  const showTemplate = show({
    template: iTemplate,
    rootValues,
    valuePath: path
  });

  if (!showTemplate) {
    deleteErrorAndValues({ iTemplate, iValues, iErrorObj });
    return;
  }

  const { type } = iTemplate;

  setMandatory({ template: iTemplate, rootValues, valuePath: path });

  if (
    type &&
    (_.toUpper(type) === "TABLE-BLOCK-T" ||
      _.toUpper(type) === "TABLE-BLOCK" ||
      _.toUpper(type) === "BLOCK")
  ) {
    if (_.get(iTemplate, "mandatory") && _.has(iTemplate, "items")) {
      const blockId = iTemplate.id || iTemplate.key;
      const hasError = !_.some(
        iTemplate.items,
        item => _.get(iValues, item.id) === "Y"
      );
      if (hasError) {
        iErrorObj[blockId] = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.301"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]
          }
        };
      } else if (_.get(iErrorObj, `${blockId}.hasError`)) {
        _.set(iErrorObj, `${blockId}.hasError`, false);
      }
    }
  }

  if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, item => {
      validateApplicationFormTableDialog({
        iTemplate: item,
        iValues,
        iErrorObj,
        rootValues,
        path
      });
    });
  } else if (
    _.toUpper(iTemplate.type) === TEXT ||
    _.toUpper(iTemplate.type) === TEXTSELECTION ||
    _.toUpper(iTemplate.type) === TEXTFIELD ||
    _.toUpper(iTemplate.type) === TEXTAREA ||
    _.toUpper(iTemplate.type) === QRADIOGROUP ||
    _.toUpper(iTemplate.type) === DATEPICKER ||
    _.toUpper(iTemplate.type) === CHECKBOX ||
    _.toUpper(iTemplate.type) === PICKER
  ) {
    const capitalizedTypeString = _.toUpper(iTemplate.type);
    _.set(
      iErrorObj,
      iTemplate.id,
      validateMandatoryForApplicationForm({
        field: {
          type: capitalizedTypeString,
          mandatory: iTemplate.mandatory,
          disabled: !!iTemplate.disabled,
          id: iTemplate.id || "",
          validation: iTemplate.validation
        },
        value:
          capitalizedTypeString === DATEPICKER
            ? iValues
            : iValues[iTemplate.id] || ""
      })
    );
  }
}

export function validateApplicationInitPaymentMethod(value) {
  const { initPayMethod, trxStatus, trxTime, trxNo } = value;
  if (!_.isEmpty(initPayMethod)) {
    if (["crCard", "eNets", "dbsCrCardIpp"].indexOf(initPayMethod) >= 0) {
      if (!trxNo || !trxTime || trxStatus !== "Y") {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.1002"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.1002"]
          }
        };
      }
    }
    return {
      hasError: false,
      message: {
        [LANGUAGE_TYPES.ENGLISH]: "",
        [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
      }
    };
  }

  return {
    hasError: true,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"],
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
        TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.302"]
    }
  };
}
/**
 * @default indicate button should disable or not
 * @param {object} clientForm - reducer clientForm state
 * @return {boolean} disable status
 * */
export function isClientFormHasError(clientForm) {
  let hasError = false;
  const errorFields = [];
  Object.keys(clientForm).forEach(key => {
    if (_.isObjectLike(clientForm[key]) && _.has(clientForm[key], "hasError")) {
      hasError = hasError || clientForm[key].hasError;
      if (clientForm[key].hasError) {
        errorFields.push(key);
      }
    }
  });

  return { hasError, errorFields };
}
