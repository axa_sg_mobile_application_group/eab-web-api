import * as _ from "lodash";
import {
  PAPROTECTION,
  DIPROTECTION,
  OTHER,
  PSGOALS,
  FIPROTECTION,
  EPLANNING,
  CIPROTECTION,
  PCHEADSTART,
  AVGINFLATRATE,
  RETIREAGE,
  RPLANNING
} from "../constants/NEEDS";

// function getProfile(validRefValues = {}, values = {}, type) {
//   const { profile = {}, dependantProfiles = {} } = validRefValues;
//   const { cid } = values;
//   return ["dependants", "spouse"].indexOf(type) > -1
//     ? dependantProfiles[cid]
//     : profile;
// }

export function calIarRate2(iarRate) {
  return Number(Number(iarRate || 0).toFixed(2)) || 0;
}

export function calTotCoverage(mtCost = 0, lumpSum = 0) {
  return mtCost + lumpSum;
}

export function getTimeHorizon(values, dependantProfileData, id) {
  let timeHorizon;
  if (id === "ePlanning") {
    const { yrtoSupport = 0, curAgeofChild = 0 } = values;
    timeHorizon =
      yrtoSupport - curAgeofChild < 0 ? 0 : yrtoSupport - curAgeofChild;
  } else {
    timeHorizon =
      (values.retireAge || dependantProfileData.nearAge || 0) -
        dependantProfileData.nearAge || 0;
  }
  return timeHorizon;
}

export function calTotShortfall(aspect, values, type = "") {
  switch (aspect) {
    case PAPROTECTION:
      return Number(-values.pmt || 0) + Number(values.extInsurance || 0);
    case CIPROTECTION: {
      const assetsVal = _.sumBy(values.assets || [], asset => asset.usedAsset);
      return (
        Number(-values.totCoverage || 0) +
        Number(values.ciProt || 0) +
        Number(assetsVal || 0)
      );
    }
    case EPLANNING: {
      const {
        assets = [],
        timeHorizon = 0,
        maturityValue = 0,
        estTotFutureCost = 0
      } = values;
      const assetsVal = _.sumBy(
        assets,
        asset =>
          Number(asset.usedAsset) *
          (1 + Number(asset.return) / 100) ** timeHorizon
      );
      return (
        Number(-estTotFutureCost || 0) +
        Number(assetsVal || 0) +
        Number(maturityValue || 0)
      );
    }
    case FIPROTECTION: {
      const assetsVal = _.sumBy(values.assets || [], asset =>
        Number(asset.usedAsset)
      );
      return (
        Number(-values.totRequired || 0) +
        Number(values.existLifeIns || 0) +
        Number(assetsVal || 0)
      );
    }
    case DIPROTECTION:
      return (
        Number(-values.annualPmt || 0) +
        Number(values.disabilityBenefit || 0) +
        Number(values.othRegIncome || 0)
      );
    case OTHER: {
      switch (type) {
        case "goal": {
          const { needsValue = 0, extInsuDisplay = 0 } = values;
          return Number(extInsuDisplay || 0) - Number(needsValue || 0);
        }
        default:
          return values.goalNo === 1 ? values.goals[0].totShortfall : "NotShow";
      }
    }
    case PSGOALS: {
      switch (type) {
        case "goal": {
          const {
            assets = [],
            timeHorizon = 0,
            projMaturity = 0,
            compFv = 0
          } = values;

          const assetsVal = _.sumBy(
            assets,
            asset =>
              Number(asset.usedAsset) *
              (1 + Number(asset.return) / 100) ** timeHorizon
          );

          return (
            Number(-compFv || 0) +
            Number(assetsVal || 0) +
            Number(projMaturity || 0)
          );
        }
        default:
          return values.goalNo === 1 ? values.goals[0].totShortfall : "NotShow";
      }
    }
    case PCHEADSTART: {
      const totShortfallCI =
        Number(-values.sumAssuredCritical || 0) +
        Number(values.ciExtInsurance || 0);
      const totShortfallP =
        Number(-values.sumAssuredProvided || 0) +
        Number(values.extInsurance || 0);
      return { totShortfallCI, totShortfallP };
    }
    case RPLANNING: {
      const {
        assets = [],
        timeHorizon = 0,
        maturityValue = 0,
        compPv = 0
      } = values;
      const assetsVal = _.sumBy(
        assets,
        asset =>
          Number(asset.usedAsset) *
          (1 + Number(asset.return) / 100) ** timeHorizon
      );
      return (
        Number(-compPv || 0) +
        Number(assetsVal || 0) +
        Number(maturityValue || 0)
      );
    }
    default:
      return 0;
  }
}

export function calAsset(assetValue, calRate, timeHorizon) {
  if (calRate && calRate !== 0 && timeHorizon && timeHorizon !== 0) {
    return assetValue * (1 + calRate / 100) ** timeHorizon;
  }
  return assetValue;
}

export function calEstToFutureCost(values) {
  const { avgEduInflatRate = 0, timeHorizon = 0, costofEdu = 0 } = values;
  return costofEdu * (1 + avgEduInflatRate / 100) ** timeHorizon;
}

export function calTotRequired({
  finExpenses = 0,
  totLiabilities = 0,
  lumpSum = 0,
  othFundNeeds = 0
}) {
  return (
    Number(finExpenses) +
    Number(totLiabilities) +
    Number(lumpSum) +
    Number(othFundNeeds)
  );
}

export function calExitLifeIns(lifeInsProt = 0, invLinkPol = 0) {
  return Number(lifeInsProt) + Number(invLinkPol);
}

export function removeCash(data) {
  if (!data.value || typeof data.value !== "string") {
    return data;
  }
  if (data.value.indexOf("$") > -1) {
    data.value = data.value.replace("$", "");
  }
  return data;
}

export function calFv(fvRate, fvNper, fvPmt, fvPv) {
  if (fvRate === 0) {
    return -fvPv - fvPmt * fvNper || 0;
  }
  return (
    Math.round(
      ((((1 - (1 + fvRate) ** fvNper) / fvRate) * fvPmt * (1 + fvRate * 1)) /
        (1 + fvRate) ** fvNper || 0) * 10000
    ) / 10000
  );
}

// Retirement Planning

export function calCompPv(iarRate2 = 0, retireDuration = 0, firstYrPMT = 0) {
  return calFv(iarRate2 / 100, retireDuration, -firstYrPMT, 0);
}

export function calCompFv(avgInflatRate, annualInReqRetirement, timeHorizon) {
  return annualInReqRetirement * (1 + avgInflatRate / 100) ** timeHorizon;
}

export function calFirstYrPmt(values) {
  return (values.compFv || 0) - (values.othRegIncome || 0);
}

export function calRetireDurtion(values, dependantProfileData) {
  const avgAge = dependantProfileData.gender === "M" ? 80 : 85;
  return avgAge - (values.retireAge || 0);
}

export function retireAgeOnChange(data) {
  const { profile, analysisData } = data;
  if (profile && analysisData) {
    const retireAge = data.value || RETIREAGE;
    const timeHorizon =
      (retireAge || profile.nearAge || 0) - (profile.nearAge || 0);
    const firstYrPMT = calFirstYrPmt(analysisData);
    const compFv = calCompFv(
      analysisData.avgInflatRate || AVGINFLATRATE,
      analysisData.annualInReqRetirement || 0,
      timeHorizon
    );
    const retireDuration = (profile.gender === "M" ? 80 : 85) - retireAge;
    const compPv = calCompPv(analysisData.iarRate2, retireDuration, firstYrPMT);
    return { retireAge, timeHorizon, compFv, retireDuration, compPv };
  }
  return {};
}

export function inReqRetirementOnChange(data) {
  const { profile, analysisData } = data;

  if (analysisData) {
    const inReqRetirement = removeCash(data.value);
    const annualInReqRetirement = inReqRetirement * 12;
    const compFv = calCompFv(
      analysisData.avgInflatRate || AVGINFLATRATE,
      annualInReqRetirement,
      analysisData.timeHorizon
    );

    const firstYrPMT = calFirstYrPmt(analysisData);
    const iarRate2 = calIarRate2(data.iarRate);
    const retireDuration =
      (profile.gender === "M" ? 80 : 85) - (data.retireAge || RETIREAGE);
    const compPv = calCompPv(iarRate2, retireDuration, firstYrPMT);

    return {
      inReqRetirement,
      annualInReqRetirement,
      compFv,
      compPv,
      firstYrPMT,
      iarRate2
    };
  }
  return {};
}

export function othRegIncomeOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const othRegIncome = data.value;
    const firstYrPMT = (analysisData.compFv || 0) - (othRegIncome || 0);
    const compPv = calCompPv(
      analysisData.iarRate2,
      analysisData.retireDuration,
      firstYrPMT
    );
    return { othRegIncome, firstYrPMT, compPv };
  }
  return {};
}

// Critical Illness Protection

export function calLumpSum(iarRate2 = 0, requireYrIncome = 0, pmt = 0) {
  return calFv(
    Number(iarRate2) / 100,
    Number(requireYrIncome),
    Number(-pmt) * 12,
    0
  );
}

function calCiProtectionTotCoverage(lumpSum, mtCost) {
  return Number(lumpSum || 0) + Number(mtCost || 0);
}

export function ciProtectionPmtOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const pmt = removeCash(data.value);
    const annualLivingExp = Number(pmt || 0) * 12;
    const { mtCost, iarRate, requireYrIncome } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { pmt, annualLivingExp, lumpSum, totCoverage };
  }
  return {};
}

export function ciProtectionRequireYrIncomeOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const requireYrIncome = Number(data.value || 0);
    const { pmt, iarRate, mtCost } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { requireYrIncome, lumpSum, totCoverage };
  }
  return {};
}

export function ciProtectionMtCostOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const mtCost = Number(data.value || 0);
    const { pmt, iarRate, requireYrIncome } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { mtCost, totCoverage };
  }
  return {};
}

// Income Protection (fiProtection)
export function fiProtectionPmtOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const {
      iarRate,
      requireYrIncome,
      finExpenses,
      othFundNeeds,
      assets,
      feProfile
    } = analysisData;

    const pmt = removeCash(data.value);
    const annualRepIncome = Number(pmt || 0) * 12;

    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const existLifeIns = calExitLifeIns(
      feProfile.lifeInsProt,
      feProfile.invLinkPol
    );
    const totRequired = calTotRequired({
      finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum,
      othFundNeeds
    });
    const totShortfall = calTotShortfall(FIPROTECTION, {
      assets,
      totRequired,
      existLifeIns
    });

    return {
      pmt,
      annualRepIncome,
      lumpSum,
      iarRate2,
      totRequired,
      existLifeIns,
      totShortfall
    };
  }
  return {};
}

export function fiProtectionRequireYrIncomeOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const requireYrIncome = Number(data.value || 0);
    const {
      pmt,
      iarRate,
      finExpenses,
      othFundNeeds,
      assets,
      feProfile
    } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const existLifeIns = calExitLifeIns(
      feProfile.lifeInsProt,
      feProfile.invLinkPol
    );
    const totRequired = calTotRequired({
      finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum,
      othFundNeeds
    });
    const totShortfall = calTotShortfall(FIPROTECTION, {
      assets,
      totRequired,
      existLifeIns
    });

    return {
      requireYrIncome,
      lumpSum,
      iarRate2,
      totRequired,
      existLifeIns,
      totShortfall
    };
  }
  return {};
}

export function fiProtectionFinExpensesOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const finExpenses = Number(data.value || 0);
    const {
      pmt,
      iarRate,
      requireYrIncome,
      othFundNeeds,
      assets,
      feProfile
    } = analysisData;

    const annualRepIncome = Number(pmt || 0) * 12;

    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const existLifeIns = calExitLifeIns(
      feProfile.lifeInsProt,
      feProfile.invLinkPol
    );
    const totRequired = calTotRequired({
      finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum,
      othFundNeeds
    });
    const totShortfall = calTotShortfall(FIPROTECTION, {
      assets,
      totRequired,
      existLifeIns
    });

    return {
      finExpenses,
      annualRepIncome,
      lumpSum,
      iarRate2,
      totRequired,
      existLifeIns,
      totShortfall
    };
  }
  return {};
}

export function fiProtectionOthFundNeedsOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const othFundNeeds = Number(data.value || 0);
    const {
      pmt,
      iarRate,
      finExpenses,
      requireYrIncome,
      assets,
      feProfile
    } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const existLifeIns = calExitLifeIns(
      feProfile.lifeInsProt,
      feProfile.invLinkPol
    );
    const totRequired = calTotRequired({
      finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum,
      othFundNeeds
    });
    const totShortfall = calTotShortfall(FIPROTECTION, {
      assets,
      totRequired,
      existLifeIns
    });

    return { othFundNeeds, totRequired, existLifeIns, totShortfall };
  }
  return {};
}

export function fiProtectionAssetsOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const {
      pmt,
      iarRate,
      requireYrIncome,
      finExpenses,
      othFundNeeds,
      assets,
      feProfile
    } = analysisData;

    const annualRepIncome = Number(pmt || 0) * 12;

    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const existLifeIns = calExitLifeIns(
      feProfile.lifeInsProt,
      feProfile.invLinkPol
    );
    const totRequired = calTotRequired({
      finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum,
      othFundNeeds
    });
    const totShortfall = calTotShortfall(FIPROTECTION, {
      assets,
      totRequired,
      existLifeIns
    });

    return {
      pmt,
      annualRepIncome,
      lumpSum,
      iarRate2,
      totRequired,
      existLifeIns,
      totShortfall,
      assets
    };
  }
  return {};
}

// Disability Benefit
export function diProtectionPmtOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const pmt = removeCash(data.value);
    const annualPmt = Number(pmt || 0) * 12;
    const { disabilityBenefit, othRegIncome } = analysisData;
    const totShortfall = calTotShortfall(DIPROTECTION, {
      annualPmt,
      disabilityBenefit,
      othRegIncome
    });
    return { pmt, annualPmt, totShortfall };
  }
  return {};
}

export function diProtectionOthRegIncomeOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const othRegIncome = Number(data.value || 0);
    const { annualPmt, disabilityBenefit } = analysisData;
    const totShortfall = calTotShortfall(DIPROTECTION, {
      annualPmt,
      disabilityBenefit,
      othRegIncome
    });
    return { othRegIncome, totShortfall };
  }
  return {};
}

export function diProtectionRequireYrIncomeOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const requireYrIncome = Number(data.value || 0);
    const { pmt, iarRate, mtCost } = analysisData;
    const iarRate2 = calIarRate2(iarRate);
    const lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    const totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { requireYrIncome, lumpSum, totCoverage };
  }
  return {};
}

// Personal Accident Protection
export function paProtectionPmtOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const pmt = Number(data.value || 0);
    const { extInsurance } = analysisData;
    const totShortfall = calTotShortfall(PAPROTECTION, { pmt, extInsurance });
    return { pmt, totShortfall };
  }
  return {};
}

// Personal Children's headStart - CriticalIlliness
export function sumAssuredCriticalOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const sumAssuredCritical = Number(data.value || 0);
    const { ciExtInsurance } = analysisData;
    const { totShortfallCI } = calTotShortfall(PCHEADSTART, {
      sumAssuredCritical,
      ciExtInsurance
    });
    return { sumAssuredCritical, totShortfallCI };
  }
  return {};
}

// Personal Children's headStart - ProtectionProvided
export function sumAssuredProvidedOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const sumAssuredProvided = Number(data.value || 0);
    const { extInsurance } = analysisData;
    const { totShortfallP } = calTotShortfall(PCHEADSTART, {
      sumAssuredProvided,
      extInsurance
    });
    return { sumAssuredProvided, totShortfallP };
  }
  return {};
}

// Other Needs
export function otherNeedsGoalsOnChange(data) {
  const goals = data.value;
  const updatedGoals = calTotShortfall(OTHER, { goals });
  return { goals: updatedGoals };
}

// Specific Goals
export function psGoalsGoalsOnChange(data) {
  const goals = data.value;
  const updatedGoals = calTotShortfall(PSGOALS, { goals });
  return { goals: updatedGoals };
}

// Education Planning
export function ePlanningYrtoSupportOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const yrtoSupport = Number(data.value || 0);
    const { curAgeofChild } = analysisData;
    const timeHorizon =
      yrtoSupport - curAgeofChild < 0 ? 0 : yrtoSupport - curAgeofChild;
    return { yrtoSupport, curAgeofChild, timeHorizon };
  }
  return {};
}

export function ePlanningCostofEduOnChange(data) {
  const { analysisData } = data;
  if (analysisData) {
    const costofEdu = Number(data.value || 0);
    const {
      avgEduInflatRate = 5,
      timeHorizon,
      assets,
      maturityValue
    } = analysisData;
    const estTotFutureCost = calEstToFutureCost({
      avgEduInflatRate,
      timeHorizon,
      costofEdu
    });
    const totShortfall = calTotShortfall({
      assets,
      timeHorizon,
      maturityValue,
      estTotFutureCost
    });
    return { costofEdu, estTotFutureCost, totShortfall };
  }
  return {};
}

export const analysisAspect = {
  needs: [
    {
      title: "Income protection",
      value: "fiProtection",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON"],
      rule: {
        pmt: {
          type: "int",
          mandatory: true,
          min: 1
        },
        unMatchPmtReason: {
          type: "string",
          mandatory: false
        },
        assets: {
          type: "assets",
          mandatory: true,
          min: 1,
          max: "{max}"
        }
      }
    },
    {
      title: "Critical Illness protection",
      value: "ciProtection",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
      rule: {
        mtCost: {
          type: "int",
          min: 0
        },
        assets: {
          type: "assets",
          mandatory: true,
          min: 1,
          max: "{max}"
        },
        requireYrIncome: {
          mandatory: true,
          numberAllowZero: true
        },
        pmt: {
          type: "int",
          mandatory: true,
          min: 1,
          max: "{max}"
        }
      }
    },
    {
      title: "Disability benefit",
      value: "diProtection",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON", "DAU"],
      rule: {
        pmt: {
          type: "int",
          mandatory: true,
          min: 1
        },
        requireYrIncome: {
          mandatory: true,
          numberAllowZero: true
        }
      }
    },
    {
      title: "Personal accident protection",
      value: "paProtection",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON", "DAU"],
      rule: {
        pmt: {
          type: "int",
          mandatory: true,
          min: 50000
        }
      }
    },
    {
      title: "Planning for children's headstart",
      value: "pcHeadstart",
      relationshipRequired: ["SON", "DAU"],
      rule: {
        providedFor: {
          type: "string",
          mandatory: true
        },
        sumAssuredProvided: {
          type: "int",
          mandatory: true,
          min: 1
        },
        sumAssuredCritical: {
          type: "int",
          mandatory: true,
          min: 1
        }
      }
    },
    {
      title: "Hospitalisation cost protection",
      value: "hcProtection",
      relationshipRequired: [
        "OWNER",
        "SPOUSE",
        "SPO",
        "SON",
        "DAU",
        "FAT",
        "MOT",
        "GFA",
        "GMO"
      ],
      rule: {
        provideHeadstart: {
          type: "string",
          mandatory: true
        },
        typeofWard: {
          type: "string",
          mandatory: true
        }
      }
    },
    {
      title: "Retirement planning",
      value: "rPlanning",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
      rule: {
        inReqRetirement: {
          type: "int",
          mandatory: true,
          min: 1,
          max: 9999999999
        },
        unMatchPmtReason: {
          type: "string",
          mandatory: false
        }
      }
    },
    {
      title: "Education Planning",
      value: "ePlanning",
      relationshipRequired: ["SON", "DAU"],
      rule: {
        yrtoSupport: {
          type: "int",
          mandatory: true,
          min: "{min}"
        },
        costofEdu: {
          type: "int",
          mandatory: true,
          min: 1
        }
      }
    },
    {
      title: "Planning for specific goals",
      value: "psGoals",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
      rule: {
        goalNo: {
          type: "int",
          mandatory: true
        }
      }
    },
    {
      title: "Other needs (e.g. mortgage,\npregnancy, savings)",
      value: "other",
      relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
      rule: {
        goalNo: {
          type: "int",
          mandatory: true
        }
      }
    }
  ],
  assets: [
    {
      title: "Savings Account",
      value: "savAcc",
      needsRequired: [
        "fiProtection",
        "ciProtection",
        "rPlanning",
        "ePlanning",
        "psGoals"
      ]
    },
    {
      title: "Fixed Deposits",
      value: "fixDeposit",
      needsRequired: [
        "fiProtection",
        "ciProtection",
        "rPlanning",
        "ePlanning",
        "psGoals"
      ]
    },
    {
      title: "Investment(Mutual Fund/Direct investment)",
      value: "invest",
      needsRequired: [
        "fiProtection",
        "ciProtection",
        "rPlanning",
        "ePlanning",
        "psGoals"
      ]
    },
    {
      title: "CPF (OA)",
      value: "cpfOa",
      needsRequired: ["fiProtection", "rPlanning"]
    },
    {
      title: "CPF (SA)",
      value: "cpfSa",
      needsRequired: ["fiProtection", "rPlanning"]
    },
    {
      title: "CPF Medisave",
      value: "cpfMs",
      needsRequired: [""]
    },
    {
      title: "SRS",
      value: "srs",
      needsRequired: ["fiProtection", "rPlanning"]
    },
    {
      title: "Please select assets",
      value: "all",
      needsRequired: [
        "fiProtection",
        "ciProtection",
        "rPlanning",
        "ePlanning",
        "psGoals"
      ]
    }
  ]
};

export function calOtherAsset({
  key,
  selectedProduct,
  selectedProfile,
  na,
  goalIndex
}) {
  const { assets } = analysisAspect;
  const needs = assets.find(ass => ass.value === key).needsRequired;
  const selectedAspects = na.aspects.split(",");
  const otherNeeds = needs
    .filter(item => item !== selectedProduct)
    .filter(item => selectedAspects.indexOf(item) > -1);

  let total = 0;
  otherNeeds.forEach(otherNeed => {
    if (key !== "all") {
      if (
        otherNeed === "psGoals" &&
        na.psGoals &&
        na.psGoals[selectedProfile] &&
        na.psGoals[selectedProfile].goals
      ) {
        na.psGoals[selectedProfile].goals.forEach(goal => {
          if (goal.assets) {
            goal.assets.forEach(asset => {
              if (asset.key === key) {
                total += asset.usedAsset;
              }
            });
          }
        });
      } else if (
        otherNeed === "ePlanning" &&
        na.ePlanning &&
        na.ePlanning.dependants
      ) {
        na.ePlanning.dependants.forEach(depandant => {
          if (depandant.assets) {
            depandant.assets.forEach(asset => {
              if (asset.key === key) {
                total += asset.usedAsset;
              }
            });
          }
        });
      } else if (
        na[otherNeed] &&
        na[otherNeed][selectedProfile] &&
        na[otherNeed][selectedProfile].assets
      ) {
        const otherProductAssets = na[otherNeed][selectedProfile].assets;
        otherProductAssets.forEach(asset => {
          if (asset.key === key) {
            total += asset.usedAsset;
          }
        });
      }
    }
  });

  /*
  Since PsGoals has to check its own other goals assets value,
  the above forEach excluded its own selectedProduct (PsGoals),
  so have to do it outside.
  */
  if (selectedProduct === "psGoals") {
    const { goals } = na.psGoals[selectedProfile];
    goals.forEach((goal, i) => {
      if (i !== goalIndex) {
        const goalAssets = goal.assets;
        if (goalAssets) {
          goalAssets.forEach(asset => {
            if (asset.key === key) {
              total += asset.usedAsset;
            }
          });
        }
      }
    });
  }

  return total;
}
