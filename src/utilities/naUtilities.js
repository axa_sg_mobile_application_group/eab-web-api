import * as _ from "lodash";
import {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  EPLANNING,
  OTHER,
  PSGOALS,
  RPLANNING
} from "../constants/NEEDS";
import {
  calExitLifeIns,
  calIarRate2,
  calLumpSum,
  calTotRequired,
  calTotShortfall,
  calTotCoverage,
  getTimeHorizon,
  calEstToFutureCost,
  calCompFv,
  calFirstYrPmt,
  calRetireDurtion,
  calCompPv,
  calOtherAsset,
  calAsset
} from "./naAnalysis";
// =============================================================================
// variables
// =============================================================================
const fiProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  unMatchPmtReason: "",
  requireYrIncome: "",
  finExpenses: "",
  othFundNeedsName: "",
  othFundNeeds: "",
  assets: null
};
const diProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  requireYrIncome: "",
  othRegIncome: ""
};
const ciProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  requireYrIncome: "",
  mtCost: "",
  assets: null
};
const rPlanningInitialState = {
  init: true,
  isActive: false,
  retireAge: 65,
  avgInflatRate: 2.65,
  inReqRetirement: "",
  unMatchPmtReason: "",
  othRegIncome: "",
  assets: null
};
const paProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: ""
};
const pcHeadstartInitialState = {
  init: true,
  isActive: false,
  providedFor: "",
  sumAssuredCritical: "",
  sumAssuredProvided: "",
  totShortfall: "NotShow"
};
const hcProtectionInitialState = {
  init: true,
  isActive: false,
  provideHeadstart: "",
  typeofWard: ""
};
const ePlanningInitialState = {
  init: true,
  isActive: false,
  yrtoSupport: "",
  costofEdu: "",
  avgEduInflatRate: 5,
  assets: null
};
const otherInitialState = {
  init: true,
  isActive: false,
  goalNo: 1,
  goals: [
    {
      extInsuDisplay: "",
      extInsuSelField: "",
      goalName: "",
      needsValue: "",
      typeofNeeds: ""
    },
    {
      extInsuDisplay: "",
      extInsuSelField: "",
      goalName: "",
      needsValue: "",
      typeofNeeds: ""
    },
    {
      extInsuDisplay: "",
      extInsuSelField: "",
      goalName: "",
      needsValue: "",
      typeofNeeds: ""
    }
  ]
};
const psGoalsInitialState = {
  init: true,
  isActive: false,
  goalNo: 1,
  goals: [
    {
      goalName: "",
      timeHorizon: "",
      compFv: "",
      assets: null,
      projMaturity: ""
    },
    {
      goalName: "",
      timeHorizon: "",
      compFv: "",
      assets: null,
      projMaturity: ""
    },
    {
      goalName: "",
      timeHorizon: "",
      compFv: "",
      assets: null,
      projMaturity: ""
    }
  ]
};
// =============================================================================
// support functions
// =============================================================================
function dependantDataGetter({
  cid,
  protection,
  calculator,
  naData,
  dependantFeData,
  dependantProfileData,
  initialState,
  shouldInitProtection
}) {
  const protectionDependants = _.cloneDeep(protection.dependants) || [];
  const index = protectionDependants.findIndex(
    dependant => dependant.cid === cid
  );

  const dependantProtectionData = calculator({
    protection: Object.assign(
      {
        cid
      },
      initialState,
      _.get(protectionDependants, `[${index}]`, {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData,
    dependantProfileData,
    naData
  });

  if (index !== -1) {
    protectionDependants[index] = dependantProtectionData;
  } else {
    protectionDependants.push(dependantProtectionData);
  }

  return protectionDependants;
}

function fiProtectionCalculator({ protection, naData, dependantFeData }) {
  const newFiProtection = _.cloneDeep(protection);

  newFiProtection.annualRepIncome =
    Number(newFiProtection.pmt !== "" ? newFiProtection.pmt : 0) * 12;
  newFiProtection.iarRate2 = calIarRate2(Number(naData.iarRate));
  newFiProtection.lumpSum = calLumpSum(
    newFiProtection.iarRate2,
    newFiProtection.requireYrIncome,
    newFiProtection.pmt
  );
  newFiProtection.totLiabilities = dependantFeData.liabilities;
  newFiProtection.totRequired = calTotRequired({
    finExpenses: newFiProtection.finExpenses || 0,
    totLiabilities: newFiProtection.totLiabilities,
    lumpSum: newFiProtection.lumpSum,
    othFundNeeds: newFiProtection.othFundNeeds || 0
  });
  newFiProtection.existLifeIns = calExitLifeIns(
    dependantFeData.lifeInsProt,
    dependantFeData.invLinkPol
  );
  newFiProtection.assets =
    newFiProtection.assets &&
    newFiProtection.assets.map(asset => {
      const newAsset = _.cloneDeep(asset);
      newAsset.calAsset = newAsset.usedAsset;
      return newAsset;
    });
  newFiProtection.totShortfall = calTotShortfall(FIPROTECTION, {
    assets: newFiProtection.assets,
    totRequired: newFiProtection.totRequired,
    existLifeIns: newFiProtection.existLifeIns
  });

  return newFiProtection;
}

function diProtectionCalculator({ protection, dependantFeData }) {
  const newDiProtection = _.cloneDeep(protection);

  newDiProtection.annualPmt =
    Number(newDiProtection.pmt !== "" ? newDiProtection.pmt : 0) * 12;
  newDiProtection.disabilityBenefit = dependantFeData.disIncomeProt;
  newDiProtection.totShortfall = calTotShortfall(DIPROTECTION, {
    annualPmt: newDiProtection.annualPmt,
    disabilityBenefit: newDiProtection.disabilityBenefit,
    othRegIncome: newDiProtection.othRegIncome || 0
  });

  return newDiProtection;
}

function ciProtectionCalculator({ protection, dependantFeData, naData }) {
  const newCiProtection = _.cloneDeep(protection);

  newCiProtection.annualLivingExp =
    Number(newCiProtection.pmt !== "" ? newCiProtection.pmt : 0) * 12;
  newCiProtection.iarRate2 = calIarRate2(Number(naData.iarRate));
  newCiProtection.lumpSum = calLumpSum(
    newCiProtection.iarRate2,
    newCiProtection.requireYrIncome,
    newCiProtection.pmt
  );
  newCiProtection.totCoverage = calTotCoverage(
    newCiProtection.mtCost,
    newCiProtection.lumpSum
  );
  newCiProtection.ciProt = dependantFeData.ciPort || 0;
  newCiProtection.assets =
    newCiProtection.assets &&
    newCiProtection.assets.map(asset => {
      const newAsset = _.cloneDeep(asset);
      newAsset.calAsset = newAsset.usedAsset;
      return newAsset;
    });
  newCiProtection.totShortfall = calTotShortfall(CIPROTECTION, {
    assets: newCiProtection.assets,
    totCoverage: newCiProtection.totCoverage,
    ciProt: newCiProtection.ciProt
  });

  return newCiProtection;
}

function rPlanningCalculator({
  protection,
  dependantFeData,
  naData,
  dependantProfileData
}) {
  const newRPlanning = _.cloneDeep(protection);

  newRPlanning.annualInReqRetirement =
    Number(
      newRPlanning.inReqRetirement !== "" ? newRPlanning.inReqRetirement : 0
    ) * 12;

  newRPlanning.timeHorizon = getTimeHorizon(
    newRPlanning,
    dependantProfileData,
    "rPlanning"
  );
  newRPlanning.iarRate2 = calIarRate2(Number(naData.iarRate));
  newRPlanning.compFv = calCompFv(
    newRPlanning.avgInflatRate,
    newRPlanning.annualInReqRetirement,
    newRPlanning.timeHorizon
  );
  newRPlanning.firstYrPMT = calFirstYrPmt(newRPlanning);
  newRPlanning.retireDuration = calRetireDurtion(
    newRPlanning,
    dependantProfileData
  );
  newRPlanning.compPv = calCompPv(
    newRPlanning.iarRate2,
    newRPlanning.retireDuration,
    newRPlanning.firstYrPMT
  );
  newRPlanning.maturityValue = dependantFeData.retirePol;
  newRPlanning.assets =
    newRPlanning.assets &&
    newRPlanning.assets.map(asset => {
      const newAsset = _.cloneDeep(asset);
      newAsset.calAsset = calAsset(
        asset.usedAsset,
        asset.return,
        newRPlanning.timeHorizon
      );
      return newAsset;
    });
  newRPlanning.totShortfall = calTotShortfall(RPLANNING, {
    assets: newRPlanning.assets,
    timeHorizon: newRPlanning.timeHorizon,
    maturityValue: newRPlanning.maturityValue,
    compPv: newRPlanning.compPv
  });

  return newRPlanning;
}

function paProtectionCalculator({ protection, dependantFeData }) {
  const newPaProtection = _.cloneDeep(protection);

  newPaProtection.extInsurance = dependantFeData.personAcc;
  newPaProtection.totShortfall = calTotShortfall(PAPROTECTION, {
    pmt: newPaProtection.pmt,
    extInsurance: newPaProtection.extInsurance
  });

  return newPaProtection;
}

function pcHeadstartCalculator({ protection, dependantFeData }) {
  const newPcHeadstart = _.cloneDeep(protection);

  newPcHeadstart.extInsurance =
    dependantFeData.lifeInsProt + dependantFeData.invLinkPol;
  newPcHeadstart.ciExtInsurance = dependantFeData.ciPort;
  newPcHeadstart.pTotShortfall =
    newPcHeadstart.extInsurance - newPcHeadstart.sumAssuredProvided;
  newPcHeadstart.ciTotShortfall =
    newPcHeadstart.ciExtInsurance - newPcHeadstart.sumAssuredCritical;

  return newPcHeadstart;
}

function hcProtectionCalculator({ protection }) {
  return _.cloneDeep(protection);
}

function ePlanningCalculator({
  protection,
  dependantFeData,
  dependantProfileData
}) {
  const newEPlanning = _.cloneDeep(protection);

  newEPlanning.curAgeofChild = dependantProfileData.nearAge;
  newEPlanning.timeHorizon = getTimeHorizon(
    newEPlanning,
    dependantProfileData,
    "ePlanning"
  );
  newEPlanning.estTotFutureCost = calEstToFutureCost(newEPlanning);
  newEPlanning.maturityValue = dependantFeData.eduFund;
  newEPlanning.assets =
    newEPlanning.assets &&
    newEPlanning.assets.map(asset => {
      const newAsset = _.cloneDeep(asset);
      newAsset.calAsset = calAsset(
        asset.usedAsset,
        asset.return,
        newEPlanning.timeHorizon
      );
      return newAsset;
    });
  newEPlanning.totShortfall = calTotShortfall(EPLANNING, {
    assets: newEPlanning.assets,
    timeHorizon: newEPlanning.timeHorizon,
    maturityValue: newEPlanning.maturityValue,
    estTotFutureCost: newEPlanning.estTotFutureCost
  });

  return newEPlanning;
}

function otherCalculator(protection) {
  const newOther = _.cloneDeep(protection);

  newOther.goals = newOther.goals.map(goal =>
    Object.assign({}, goal, {
      totShortfall: calTotShortfall(
        OTHER,
        {
          needsValue: goal.needsValue,
          extInsuDisplay: goal.extInsuDisplay
        },
        "goal"
      ),
      cid: newOther.cid
    })
  );
  newOther.totShortfall = calTotShortfall(OTHER, {
    goalNo: newOther.goalNo,
    goals: newOther.goals
  });

  return newOther;
}

function psGoalsCalculator(protection) {
  const newPsGoals = _.cloneDeep(protection);

  newPsGoals.goals = newPsGoals.goals.map(goal =>
    Object.assign({}, goal, {
      totShortfall: calTotShortfall(
        PSGOALS,
        {
          assets:
            goal.assets &&
            goal.assets.map(asset => {
              const newAsset = _.cloneDeep(asset);
              newAsset.calAsset = calAsset(
                asset.usedAsset,
                asset.return,
                goal.timeHorizon
              );
              return newAsset;
            }),
          timeHorizon: goal.timeHorizon.toString(),
          projMaturity: goal.projMaturity,
          compFv: goal.compFv
        },
        "goal"
      ),
      assets:
        goal.assets &&
        goal.assets.map(asset => {
          const newAsset = _.cloneDeep(asset);
          newAsset.calAsset = calAsset(
            asset.usedAsset,
            asset.return,
            goal.timeHorizon
          );
          return newAsset;
        })
    })
  );
  newPsGoals.totShortfall = calTotShortfall(PSGOALS, {
    goalNo: newPsGoals.goalNo,
    goals: newPsGoals.goals
  });

  return newPsGoals;
}
// =============================================================================
// main functions
// =============================================================================
export function naDataGetter({
  naData,
  pdaData,
  profileData,
  dependantProfilesData,
  feData,
  shouldInitProtection
}) {
  // na initialize
  const newNaData = Object.assign(
    {
      rPlanning: {},
      ciProtection: {},
      fiProtection: {},
      diProtection: {},
      paProtection: {},
      pcHeadstart: {},
      hcProtection: {},
      ePlanning: {},
      other: {},
      psGoals: {}
    },
    _.cloneDeep(naData)
  );

  // owner fiProtection
  newNaData.fiProtection.owner = fiProtectionCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      fiProtectionInitialState,
      _.get(naData, "fiProtection.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    naData,
    dependantFeData: feData.owner
  });

  // owner diProtection
  newNaData.diProtection.owner = diProtectionCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      diProtectionInitialState,
      _.get(naData, "diProtection.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData: feData.owner
  });

  // owner ciProtection
  newNaData.ciProtection.owner = ciProtectionCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      ciProtectionInitialState,
      _.get(naData, "ciProtection.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData: feData.owner,
    naData: newNaData
  });

  // owner rPlanning
  newNaData.rPlanning.owner = rPlanningCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      rPlanningInitialState,
      _.get(naData, "rPlanning.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData: feData.owner,
    naData: newNaData,
    dependantProfileData: profileData
  });

  // owner paProtection
  newNaData.paProtection.owner = paProtectionCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      paProtectionInitialState,
      _.get(naData, "paProtection.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData: feData.owner
  });

  // owner hcProtection
  newNaData.hcProtection.owner = hcProtectionCalculator({
    protection: Object.assign(
      {
        cid: profileData.cid
      },
      hcProtectionInitialState,
      _.get(naData, "hcProtection.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    ),
    dependantFeData: feData.owner
  });

  // owner other
  newNaData.other.owner = otherCalculator(
    Object.assign(
      {
        cid: profileData.cid
      },
      otherInitialState,
      _.get(naData, "other.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    )
  );

  // owner psGoals
  newNaData.psGoals.owner = psGoalsCalculator(
    Object.assign(
      {
        cid: profileData.cid
      },
      psGoalsInitialState,
      _.get(naData, "psGoals.owner", {}),
      shouldInitProtection
        ? {
            init: true
          }
        : {}
    )
  );

  // spouse
  if (pdaData.applicant === "joint") {
    const spouseData = _.find(
      profileData.dependants,
      dependant => dependant.relationship === "SPO"
    );
    const spouseKey = Object.keys(dependantProfilesData).find(
      key => dependantProfilesData[key].relationship === "SPO"
    );
    const spouseProfileData = dependantProfilesData[spouseKey];

    // spouse fiProtection
    newNaData.fiProtection.spouse = fiProtectionCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        fiProtectionInitialState,
        _.get(naData, "fiProtection.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      naData,
      dependantFeData: feData.spouse
    });

    // spouse diProtection
    newNaData.diProtection.spouse = diProtectionCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        diProtectionInitialState,
        _.get(naData, "diProtection.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      dependantFeData: feData.spouse
    });

    // spouse ciProtection
    newNaData.ciProtection.spouse = ciProtectionCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        ciProtectionInitialState,
        _.get(naData, "ciProtection.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      dependantFeData: feData.spouse,
      naData: newNaData
    });

    // spouse rPlanning
    newNaData.rPlanning.spouse = rPlanningCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        rPlanningInitialState,
        _.get(naData, "rPlanning.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      dependantFeData: feData.spouse,
      naData: newNaData,
      dependantProfileData: spouseProfileData
    });

    // spouse paProtection
    newNaData.paProtection.spouse = paProtectionCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        paProtectionInitialState,
        _.get(naData, "paProtection.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      dependantFeData: feData.spouse
    });

    // spouse hcProtection
    newNaData.hcProtection.spouse = hcProtectionCalculator({
      protection: Object.assign(
        {
          cid: spouseData.cid
        },
        hcProtectionInitialState,
        _.get(naData, "hcProtection.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      ),
      dependantFeData: feData.spouse
    });

    // spouse other
    newNaData.other.spouse = otherCalculator(
      Object.assign(
        {
          cid: spouseData.cid
        },
        otherInitialState,
        _.get(naData, "other.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      )
    );

    // spouse psGoals
    newNaData.psGoals.spouse = psGoalsCalculator(
      Object.assign(
        {
          cid: spouseData.cid
        },
        psGoalsInitialState,
        _.get(naData, "psGoals.spouse", {}),
        shouldInitProtection
          ? {
              init: true
            }
          : {}
      )
    );
  }

  // dependants
  if (pdaData.dependants !== "") {
    pdaData.dependants.split(",").forEach(cid => {
      const dependantData = profileData.dependants.find(
        dependant => dependant.cid === cid
      );
      const dependantFeData = feData.dependants
        ? feData.dependants.find(dependant => dependant.cid === cid)
        : false;
      const dependantKey = Object.keys(dependantProfilesData).find(
        key => dependantProfilesData[key].cid === cid
      );
      const dependantProfileData =
        dependantKey !== -1 ? dependantProfilesData[dependantKey] : false;

      if (dependantData && dependantFeData && dependantProfileData) {
        switch (dependantData.relationship) {
          case "SON":
          case "DAU": {
            newNaData.diProtection.dependants = dependantDataGetter({
              cid,
              initialState: diProtectionInitialState,
              protection: newNaData.diProtection,
              calculator: diProtectionCalculator,
              dependantFeData,
              shouldInitProtection
            });

            newNaData.paProtection.dependants = dependantDataGetter({
              cid,
              initialState: paProtectionInitialState,
              protection: newNaData.paProtection,
              calculator: paProtectionCalculator,
              dependantFeData,
              shouldInitProtection
            });

            newNaData.pcHeadstart.dependants = dependantDataGetter({
              cid,
              initialState: pcHeadstartInitialState,
              protection: newNaData.pcHeadstart,
              calculator: pcHeadstartCalculator,
              dependantFeData,
              shouldInitProtection
            });

            newNaData.hcProtection.dependants = dependantDataGetter({
              cid,
              initialState: hcProtectionInitialState,
              protection: newNaData.hcProtection,
              calculator: hcProtectionCalculator,
              dependantFeData,
              shouldInitProtection
            });

            newNaData.ePlanning.dependants = dependantDataGetter({
              cid,
              initialState: ePlanningInitialState,
              protection: newNaData.ePlanning,
              calculator: ePlanningCalculator,
              dependantProfileData,
              dependantFeData,
              shouldInitProtection
            });
            break;
          }
          case "FAT":
          case "MOT":
          case "GFA":
          case "GMO": {
            newNaData.hcProtection.dependants = dependantDataGetter({
              cid,
              initialState: hcProtectionInitialState,
              protection: newNaData.hcProtection,
              calculator: hcProtectionCalculator,
              dependantFeData,
              shouldInitProtection
            });
            break;
          }
          default:
            throw new Error(
              `naUtilities.naDataGetter got unexpected dependant case ${
                dependantData.relationship
              }`
            );
        }
      }
    });
  }

  return newNaData;
}

export function validateFiProtection({
  fiProtectionData,
  dependantProfileData,
  selectedProfile,
  na,
  fe
}) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    unMatchPmtReason: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    },
    othFundNeedsName: {
      hasError: false,
      messagePath: ""
    },
    othFundNeeds: {
      hasError: false,
      messagePath: ""
    },
    assets: [
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      }
    ]
  };

  if (fiProtectionData.isActive) {
    // validate init
    if (fiProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (fiProtectionData.pmt === "" || fiProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (
      fiProtectionData.requireYrIncome === "" ||
      fiProtectionData.requireYrIncome === "0" ||
      fiProtectionData.requireYrIncome === 0
    ) {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate unMatchPmtReason
    if (
      // if client do not fill income field, allowance is undefined
      !Number.isNaN(parseFloat(dependantProfileData.allowance)) &&
      // if client do not fill income field, allowance is ""
      !Number.isNaN(parseFloat(fiProtectionData.pmt)) &&
      fiProtectionData.pmt > dependantProfileData.allowance &&
      fiProtectionData.unMatchPmtReason === ""
    ) {
      validation.unMatchPmtReason = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate othFundNeedsName
    // if client do not fill othFundNeeds, othFundNeeds is ""
    const realOthFundNeedsValue = parseFloat(fiProtectionData.othFundNeeds);
    if (
      !Number.isNaN(realOthFundNeedsValue) &&
      realOthFundNeedsValue !== 0 &&
      fiProtectionData.othFundNeedsName === ""
    ) {
      validation.othFundNeedsName = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validation for the first time
    if (
      Number.isNaN(realOthFundNeedsValue) &&
      fiProtectionData.othFundNeedsName !== ""
    ) {
      validation.othFundNeeds = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate othFundNeeds
    if (
      fiProtectionData.othFundNeedsName !== "" &&
      fiProtectionData.othFundNeeds === 0
    ) {
      validation.othFundNeeds = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (fiProtectionData.assets) {
      fiProtectionData.assets.forEach((asset, index) => {
        const otherAssetsValue = calOtherAsset({
          key: asset.key,
          selectedProduct: "fiProtection",
          selectedProfile,
          na
        });

        if (
          fe[selectedProfile][asset.key] <
          asset.usedAsset + otherAssetsValue
        ) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (
          asset.key === "all" ||
          (asset.key !== "all" &&
            (asset.usedAsset === 0 || asset.usedAsset === null))
        ) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

export function validateDiProtection(diProtectionData) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    }
  };

  if (diProtectionData.isActive) {
    // validate init
    if (diProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (diProtectionData.pmt === "" || diProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (diProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

export function validateCiProtection({
  ciProtectionData,
  selectedProfile,
  na,
  fe
}) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    },
    mtCost: {
      hasError: false,
      messagePath: ""
    },
    assets: [
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        }
      }
    ]
  };

  if (ciProtectionData.isActive) {
    // validate init
    if (ciProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (ciProtectionData.pmt === "" || ciProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (ciProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate mtCost
    if (ciProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (ciProtectionData.assets) {
      ciProtectionData.assets.forEach((asset, index) => {
        const otherAssetsValue = calOtherAsset({
          key: asset.key,
          selectedProduct: "ciProtection",
          selectedProfile,
          na
        });
        if (
          fe[selectedProfile][asset.key] <
          asset.usedAsset + otherAssetsValue
        ) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (
          asset.key === "all" ||
          (asset.key !== "all" &&
            (asset.usedAsset === 0 || asset.usedAsset === null))
        ) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

export function validateRPlanning({
  rPlanningData,
  dependantProfileData,
  selectedProfile,
  na,
  fe
}) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    retireAge: {
      hasError: false,
      messagePath: ""
    },
    inReqRetirement: {
      hasError: false,
      messagePath: ""
    },
    unMatchPmtReason: {
      hasError: false,
      messagePath: ""
    },
    othRegIncome: {
      hasError: false,
      messagePath: ""
    },
    assets: [
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }
    ]
  };

  if (rPlanningData.isActive) {
    // validate init
    if (rPlanningData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate inReqRetirement
    if (
      rPlanningData.inReqRetirement === "" ||
      rPlanningData.inReqRetirement === 0
    ) {
      validation.inReqRetirement = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    if (
      // if client do not fill income field, allowance is undefined
      !Number.isNaN(parseFloat(dependantProfileData.allowance)) &&
      // if client do not fill income field, allowance is ""
      !Number.isNaN(parseFloat(rPlanningData.inReqRetirement)) &&
      rPlanningData.inReqRetirement > dependantProfileData.allowance &&
      rPlanningData.unMatchPmtReason === ""
    ) {
      validation.unMatchPmtReason = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (rPlanningData.assets) {
      rPlanningData.assets.forEach((asset, index) => {
        const otherAssetsValue = calOtherAsset({
          key: asset.key,
          selectedProduct: "rPlanning",
          selectedProfile,
          na
        });
        if (
          fe[selectedProfile][asset.key] <
          asset.usedAsset + otherAssetsValue
        ) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (
          asset.key === "all" ||
          (asset.key !== "all" &&
            (asset.usedAsset === 0 || asset.usedAsset === null))
        ) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }

        if ("key" in asset && !("return" in asset)) {
          validation.assets[index].return = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

export function validatePaProtection(paProtectionData) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    }
  };

  if (paProtectionData.isActive) {
    // validate init
    if (paProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (paProtectionData.pmt === "" || paProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    } else if (paProtectionData.pmt < 50000) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.708"
      };
    }
  }

  return validation;
}

export function validatePcHeadstart(pcHeadstartData) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    providedFor: {
      hasError: false,
      messagePath: ""
    },
    sumAssuredCritical: {
      hasError: false,
      messagePath: ""
    },
    sumAssuredProvided: {
      hasError: false,
      messagePath: ""
    }
  };

  if (pcHeadstartData.isActive) {
    // validate init
    if (pcHeadstartData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate providedFor
    if (pcHeadstartData.providedFor === "") {
      validation.providedFor = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate sumAssuredCritical
    if (
      pcHeadstartData.providedFor.indexOf("CI") !== -1 &&
      (pcHeadstartData.sumAssuredCritical === "" ||
        pcHeadstartData.sumAssuredCritical === 0)
    ) {
      validation.sumAssuredCritical = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate sumAssuredProvided
    if (
      pcHeadstartData.providedFor.indexOf("P") !== -1 &&
      (pcHeadstartData.sumAssuredProvided === "" ||
        pcHeadstartData.sumAssuredProvided === 0)
    ) {
      validation.sumAssuredProvided = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

export function validateHcProtection(hcProtectionData) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    provideHeadstart: {
      hasError: false,
      messagePath: ""
    },
    typeofWard: {
      hasError: false,
      messagePath: ""
    }
  };

  if (hcProtectionData.isActive) {
    // validate init
    if (hcProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate provideHeadstart
    if (
      hcProtectionData.provideHeadstart === "" ||
      (hcProtectionData.isActive && !hcProtectionData.provideHeadstart)
    ) {
      validation.provideHeadstart = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate typeofWard
    if (
      hcProtectionData.typeofWard === "" ||
      (hcProtectionData.isActive && !hcProtectionData.typeofWard)
    ) {
      validation.typeofWard = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

export function validateEPlanning({ ePlanningData, na, fe }) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    yrtoSupport: {
      hasError: false,
      messagePath: ""
    },
    costofEdu: {
      hasError: false,
      messagePath: ""
    },
    assets: [
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }
    ]
  };

  if (ePlanningData.isActive) {
    // validate init
    if (ePlanningData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate yrtoSupport
    if (ePlanningData.yrtoSupport === "" || ePlanningData.yrtoSupport === 0) {
      validation.yrtoSupport = {
        hasError: true,
        messagePath: "error.302"
      };
    } else if (ePlanningData.yrtoSupport < ePlanningData.curAgeofChild) {
      validation.yrtoSupport = {
        hasError: true,
        messagePath: "error.405"
      };
    }

    // validate costofEdu
    if (ePlanningData.costofEdu === "" || ePlanningData.costofEdu === 0) {
      validation.costofEdu = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (ePlanningData.assets) {
      ePlanningData.assets.forEach((asset, index) => {
        const otherAssetsValue = calOtherAsset({
          key: asset.key,
          selectedProduct: "ePlanning",
          selectedProfile: "owner",
          na
        });
        if (fe.owner[asset.key] < asset.usedAsset + otherAssetsValue) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (
          asset.key === "all" ||
          (asset.key !== "all" &&
            (asset.usedAsset === 0 || asset.usedAsset === null))
        ) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }

        if ("key" in asset && !("return" in asset)) {
          validation.assets[index].return = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

export function validateOther(otherData) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    goalNo: {
      hasError: false,
      messagePath: ""
    },
    goals: [
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        needsValue: {
          hasError: false,
          messagePath: ""
        },
        typeofNeeds: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        needsValue: {
          hasError: false,
          messagePath: ""
        },
        typeofNeeds: {
          hasError: false,
          messagePath: ""
        }
      },
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        needsValue: {
          hasError: false,
          messagePath: ""
        },
        typeofNeeds: {
          hasError: false,
          messagePath: ""
        }
      }
    ]
  };

  if (otherData.isActive) {
    // validate init
    if (otherData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate goalNo
    if (otherData.goalNo === "" || otherData.goalNo === 0) {
      validation.goalNo = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate goals
    for (let goalIndex = 0; goalIndex < otherData.goalNo; goalIndex += 1) {
      // validate goals[].goalName
      if (otherData.goals[goalIndex].goalName === "") {
        validation.goals[goalIndex].goalName = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].needsValue
      if (
        otherData.goals[goalIndex].needsValue === "" ||
        otherData.goals[goalIndex].needsValue === 0 ||
        otherData.goals[goalIndex].needsValue === null
      ) {
        validation.goals[goalIndex].needsValue = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].typeofNeeds
      if (otherData.goals[goalIndex].typeofNeeds === "") {
        validation.goals[goalIndex].typeofNeeds = {
          hasError: true,
          messagePath: "error.302"
        };
      }
    }
  }

  return validation;
}

export function validatePsGoals({ psGoalsData, selectedProfile, na, fe }) {
  const validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    goalNo: {
      hasError: false,
      messagePath: ""
    },
    goals: [
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        timeHorizon: {
          hasError: false,
          messagePath: ""
        },
        compFv: {
          hasError: false,
          messagePath: ""
        },
        assets: [
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          }
        ]
      },
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        timeHorizon: {
          hasError: false,
          messagePath: ""
        },
        compFv: {
          hasError: false,
          messagePath: ""
        },
        assets: [
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          }
        ]
      },
      {
        goalName: {
          hasError: false,
          messagePath: ""
        },
        timeHorizon: {
          hasError: false,
          messagePath: ""
        },
        compFv: {
          hasError: false,
          messagePath: ""
        },
        assets: [
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          },
          {
            usedAsset: {
              hasError: false,
              messagePath: ""
            },
            key: {
              hasError: false,
              messagePath: ""
            },
            return: {
              hasError: false,
              messagePath: ""
            }
          }
        ]
      }
    ]
  };

  if (psGoalsData.isActive) {
    // validate init
    if (psGoalsData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate goalNo
    if (psGoalsData.goalNo === "" || psGoalsData.goalNo === 0) {
      validation.goalNo = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate goals
    for (let goalIndex = 0; goalIndex < psGoalsData.goalNo; goalIndex += 1) {
      // validate goals[].goalName
      if (psGoalsData.goals[goalIndex].goalName === "") {
        validation.goals[goalIndex].goalName = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].timeHorizon
      if (psGoalsData.goals[goalIndex].timeHorizon === "") {
        validation.goals[goalIndex].timeHorizon = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].compFv
      if (
        psGoalsData.goals[goalIndex].compFv === "" ||
        psGoalsData.goals[goalIndex].compFv === 0
      ) {
        validation.goals[goalIndex].compFv = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate assets
      if (
        psGoalsData.goals &&
        psGoalsData.goals[goalIndex] &&
        psGoalsData.goals[goalIndex].assets
      ) {
        psGoalsData.goals[goalIndex].assets.forEach((asset, index) => {
          const otherAssetsValue = calOtherAsset({
            key: asset.key,
            selectedProduct: "psGoals",
            selectedProfile,
            na,
            goalIndex
          });
          if (
            fe[selectedProfile][asset.key] <
            asset.usedAsset + otherAssetsValue
          ) {
            if (asset.usedAsset === 0 || asset.usedAsset === null) {
              validation.goals[goalIndex].assets[index].usedAsset = {
                hasError: true,
                messagePath: ""
              };
            } else {
              validation.goals[goalIndex].assets[index].usedAsset = {
                hasError: true,
                messagePath: "error.702"
              };
            }
          }

          if (
            asset.key === "all" ||
            (asset.key !== "all" &&
              (asset.usedAsset === 0 || asset.usedAsset === null))
          ) {
            validation.goals[goalIndex].assets[index].key = {
              hasError: true,
              messagePath: ""
            };
          }

          if ("key" in asset && !("return" in asset)) {
            validation.goals[goalIndex].assets[index].return = {
              hasError: true,
              messagePath: ""
            };
          }
        });
      }
    }
  }

  return validation;
}
