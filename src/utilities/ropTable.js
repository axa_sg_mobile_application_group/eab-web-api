import * as _ from "lodash";
import * as ROP_TABLE from "../constants/ROP_TABLE";

const { REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER } = ROP_TABLE;

export function calculateRopTable({ rootValues, path, id, pathValue }) {
  if (!_.isString(id)) {
    return;
  }

  if ([REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER].indexOf(id) > -1) {
    _.set(rootValues, `${path}.${id}`, pathValue);
  } else if (_.isNumber(pathValue)) {
    if (pathValue < 0) {
      return;
    }

    const values = _.get(rootValues, path, {});
    let relatedId;
    let relatedTotalId;

    if (id.indexOf("AXA") > -1) {
      relatedId = _.replace(id, "AXA", "Other");
      relatedTotalId = _.replace(id, "AXA", "");
    } else if (id.indexOf("Other") > -1) {
      relatedId = _.replace(id, "Other", "AXA");
      relatedTotalId = _.replace(id, "Other", "");
    }

    if (id.indexOf("exist") > -1 || id.indexOf("replace") > -1) {
      const relatedTotalValue = _.get(values, relatedTotalId, 0);
      if (pathValue > relatedTotalValue) {
        return;
      }
      _.set(values, id, pathValue);
      if (_.isNumber(relatedTotalValue)) {
        _.set(values, relatedId, relatedTotalValue - pathValue);
      }
    } else if (id.indexOf("pending") > -1) {
      const relatedValue = _.get(values, relatedId, 0);
      _.set(values, id, pathValue);
      if (_.isNumber(relatedValue)) {
        _.set(values, relatedTotalId, relatedValue + pathValue);
      }
    }

    _.set(rootValues, path, values);
  }
}

export default {};
