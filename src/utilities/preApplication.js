import * as _ from "lodash";
import getProductId from "./getProductId";

export const prepareProductList = applicationsList => {
  const productList = [];
  _.forEach(applicationsList, item => {
    const {
      plans = [],
      baseProductCode,
      baseProductName,
      iCid,
      iCids,
      pCid,
      version
    } = item;
    const covName = baseProductName || _.get(plans, "[0].covName");
    const id = _.get(plans, "[0].productId");
    const pIndex = _.findIndex(
      productList,
      pro => pro.covCode === baseProductCode
    );
    if (pIndex < 0) {
      if (_.isArray(iCids) && iCids.length > 0) {
        _.forEach(iCids, cid => {
          const product = {
            id,
            covCode: baseProductCode,
            covName,
            iCid: cid,
            pCid,
            ccy: "",
            attachmentId: "thumbnail3",
            version
          };
          product.id = id || getProductId(product);
          productList.push(product);
        });
      } else {
        const product = {
          id,
          covCode: baseProductCode,
          covName,
          iCid,
          pCid,
          ccy: "",
          attachmentId: "thumbnail3",
          version
        };
        product.id = id || getProductId(product);
        productList.push(product);
      }
    }
  });
  return productList;
};

export const getValidBundleId = profile =>
  _.get(_.find(_.get(profile, "bundle"), bundle => bundle.isValid), "id", "");
