import * as _ from "lodash";

/**
 * @description use to search client
 * @param {array} contactList - full list of clients
 * @param {string} contactList[].fullName - client full name. use to reorder
 *   list too.
 * @param {string} contactList[].idCardNo - client id Card No
 * @param {string} contactList[].mobileNo - client mobile No
 * @param {string} contactList[].email - client email
 * @param {boolean} isViewAll - should return all client
 * @param {string} searchText - search key word
 * @return {array} search result
 * */
export default function({ contactList, isViewAll, searchText }) {
  const returnArray = [];

  contactList.forEach(client => {
    const { idCardNo = "", fullName = "", mobileNo = "", email = "" } = client;

    if (
      isViewAll ||
      (searchText.length > 1 &&
        (_.toUpper(idCardNo).indexOf(_.toUpper(searchText)) > -1 ||
          _.toUpper(fullName).indexOf(_.toUpper(searchText)) > -1 ||
          _.toUpper(mobileNo).indexOf(_.toUpper(searchText)) > -1 ||
          _.toUpper(email).indexOf(_.toUpper(searchText)) > -1))
    ) {
      returnArray.push(client);
    }
  });

  return _.orderBy(returnArray, [item => _.toUpper(item.fullName)], "asc");
}
