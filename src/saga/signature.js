import { call, put, select } from "redux-saga/effects";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  CONFIG,
  CLIENT,
  SIGNATURE,
  APPLICATION
} from "../constants/REDUCER_TYPES";
import EAPP from "../constants/EAPP";

export function* getSignature(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { docId, successCallback, failCallback } = action;
    const newCurrentStep = EAPP.tabStep[SIGNATURE];
    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "getSignatureStatusFromCb",
        docId
      }
    });

    if (response.success && response.application) {
      let signingTabIdx = 0;
      let allSigned = false;
      response.application.attachments.some(att => {
        if (att.isSigned) {
          if (signingTabIdx + 1 < response.application.tabCount) {
            signingTabIdx += 1;
          } else if (signingTabIdx + 1 === response.application.tabCount) {
            signingTabIdx = -1;
            allSigned = true;
          }
          return false;
        }
        return true;
      });
      const {
        tabCount,
        isFaChannel,
        attachments,
        agentSignFields,
        clientSignFields
      } = response.application;

      const newSignature = {
        isFaChannel,
        // TODO: update selectedPdfIdx after auto switch is completed
        // selectedPdfIdx: signingTabIdx < 0 ? tabCount - 1 : signingTabIdx,
        selectedPdfIdx: -1,
        signingTabIdx,
        isSigningProcess: [],
        numOfTabs: tabCount,
        attachments,
        attUrls: [],
        agentSignFields,
        clientSignFields
      };

      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
        newCurrentStep
      });

      if (
        ![
          _.get(response.crossAge, "insuredStatus"),
          _.get(response.crossAge, "proposerStatus")
        ].some(s => s === EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED)
      ) {
        yield put({
          type: ACTION_TYPES[APPLICATION].UPDATE_CROSS_AGE,
          newCrossAge: response.crossAge
        });
        yield put({
          type: ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
          newIsAlertDelayed: newCurrentStep === EAPP.tabStep[SIGNATURE],
          newIsCrossAgeAlertShow: true
        });
      }

      yield put({
        type: ACTION_TYPES[SIGNATURE].UPDATE_SIGNATURE,
        newSignature
      });

      yield put({
        type: ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF,
        newSelectedPdfIdx: allSigned ? 0 : signingTabIdx
      });
      if (_.isFunction(successCallback)) {
        successCallback();
      }
    } else if (_.isFunction(failCallback)) {
      failCallback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveSignedPdf(action) {
  try {
    const { docId, attId, pdfData, isShield, callback } = action;
    let apiOptions = {};
    const currentStep = yield select(
      state => state[APPLICATION].component.currentStep
    );
    const signature = yield select(state => state[SIGNATURE].signature);
    const { signingTabIdx, numOfTabs, attachments } = signature;

    const callServer = yield select(state => state[CONFIG].callServer);

    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "saveSignedPdf",
        docId,
        attId,
        pdfData
      }
    });

    if (isShield) {
      apiOptions = {
        url: "shieldApplication",
        data: {
          action: "getSignatureUpdatedPdfString",
          sdwebDocid: `${docId}_${attId}`,
          tabIdx: signingTabIdx
        }
      };
    } else {
      apiOptions = {
        url: "application",
        data: {
          action: "getUpdatedAttachmentUrl",
          signDocId: `${docId}_${attId}`,
          tabIdx: signingTabIdx
        }
      };
    }
    const getResponse = yield call(callServer, apiOptions);

    if (response.success && getResponse.success) {
      let newSelectedPdfIdx = -1;
      const newSigningTabIdx =
        signingTabIdx !== numOfTabs - 1 ? signingTabIdx + 1 : -1;
      const newAttachments = _.cloneDeep(attachments);
      newAttachments[signingTabIdx].isSigned = true;

      getResponse.newPdfStrings.forEach(item => {
        newAttachments[item.index].pdfStr = item.pdfStr;
      });
      newAttachments.some((value, index) => {
        if (value.isSigned) {
          return false;
        }
        newSelectedPdfIdx = index;
        return true;
      });
      let newCompletedStep = -1;
      if (isShield) {
        newCompletedStep = response.application.appCompletedStep;
      } else {
        newCompletedStep =
          newAttachments.findIndex(attachment => !attachment.isSigned) < 0
            ? 1
            : 0;
      }

      if (newSelectedPdfIdx !== -1) {
        yield put({
          type: ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF,
          newSelectedPdfIdx
        });
      }

      yield put({
        type: ACTION_TYPES[SIGNATURE].UPDATE_SIGNED_PDF,
        newSigningTabIdx,
        newAttachments
      });

      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
        newCurrentStep: currentStep,
        newCompletedStep
      });

      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newTemplate: getResponse.appFormTemplate,
        newApplication: isShield
          ? response.application
          : getResponse.application
      });
    }
    if (_.isFunction(callback)) {
      const cid = yield select(state => state[CLIENT].profile.cid);
      yield put({
        type: ACTION_TYPES[CLIENT].GET_PROFILE,
        cid
      });
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* initSignatureShield(action) {
  try {
    const { signature, application, crossAge } = action;
    const newCurrentStep = EAPP.tabStep[SIGNATURE];
    // yield put({
    //   type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
    //   newCurrentStep
    // });

    let allSigned = false;
    let signingTabIdx = 0;
    signature.pdfStr.some((att, index) => {
      if (signature.isSigned[index]) {
        if (signingTabIdx + 1 < signature.numOfTabs) {
          signingTabIdx += 1;
        } else if (signingTabIdx + 1 === signature.numOfTabs) {
          signingTabIdx = -1;
          allSigned = true;
        }
        return false;
      }
      return true;
    });

    const newSignature = {
      isFaChannel: signature.isFaChannel,
      // TODO: update selectedPdfIdx after auto switch is completed
      // selectedPdfIdx: signingTabIdx < 0 ? tabCount - 1 : signingTabIdx,
      selectedPdfIdx: -1,
      signingTabIdx,
      isSigningProcess: [],
      numOfTabs: signature.numOfTabs,
      attachments: signature.pdfStr.map((value, index) => ({
        isSigned: signature.isSigned[index],
        pdfStr: value,
        docid: signature.signDocConfig.ids[index]
      })),
      agentSignFields: signature.agentSignFields,
      clientSignFields: signature.clientSignFields
    };

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_COMPLETED_STEP,
      newCompletedStep: allSigned ? 1 : 0
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
      newApplication: application
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_CROSS_AGE,
      newCrossAge: crossAge
    });
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
      newIsAlertDelayed: newCurrentStep === EAPP.tabStep[SIGNATURE],
      newIsCrossAgeAlertShow: true
    });

    yield put({
      type: ACTION_TYPES[SIGNATURE].UPDATE_SIGNATURE,
      newSignature
    });

    yield put({
      type: ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF,
      newSelectedPdfIdx: allSigned ? 0 : signingTabIdx
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getSignatureShield() {
  try {
    const signature = yield select(state => state[SIGNATURE].signature);

    yield put({
      type: ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF,
      newSelectedPdfIdx: signature.signingTabIdx
    });
  } catch (e) {
    /* TODO need error handling */
  }
}
