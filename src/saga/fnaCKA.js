import * as _ from "lodash";
import moment from "moment";
import { put, select } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { FNA, FNA_CKA, CLIENT } from "../constants/REDUCER_TYPES";
import {
  INVESTLINKEDPLANS,
  PARTICIPATINGPLANS
} from "../constants/PRODUCT_TYPES";

function isPassCKA(ckaData) {
  const course = ckaData.course || "";
  const collectiveInvestment = ckaData.collectiveInvestment || "";
  const insuranceInvestment = ckaData.insuranceInvestment || "";
  const profession = ckaData.profession || "";

  const result =
    (course !== "notApplicable" ? 1 : 0) +
      (collectiveInvestment === "Y" ? 1 : 0) +
      (insuranceInvestment === "Y" ? 1 : 0) +
      (profession !== "notApplicable" ? 1 : 0) >=
    1
      ? "Y"
      : "N";

  return result;
}

function* updateCKAisValid() {
  const ckaData = yield select(state => state[FNA].na.ckaSection.owner);
  const prodType = yield select(state => state[FNA].na.productType.prodType);
  const raSection = yield select(state => state[FNA].na.raSection);
  const prodTypeArr = prodType.split(",");
  let isCompleted = true;
  let ckaIsValid = false;

  if (
    ckaData.isCourseError.hasError === false &&
    ckaData.isInstitutionError.hasError === false &&
    ckaData.isStudyPeriodEndYearError.hasError === false &&
    ckaData.isCollectiveInvestmentError.hasError === false &&
    ckaData.isTransactionTypeError.hasError === false &&
    ckaData.isInsuranceInvestmentError.hasError === false &&
    ckaData.isInsuranceTypeError.hasError === false &&
    ckaData.isProfessionError.hasError === false &&
    ckaData.isYearsError.hasError === false
  ) {
    ckaIsValid = true;
  }

  if (
    (prodTypeArr.indexOf(INVESTLINKEDPLANS) > -1 &&
      (!ckaIsValid || !raSection.isValid)) ||
    (prodTypeArr.indexOf(PARTICIPATINGPLANS) > -1 && !raSection.isValid)
  ) {
    isCompleted = false;
  }

  yield put({
    type: ACTION_TYPES[FNA_CKA].UPDATE_CKA_IS_VALID,
    isValid: ckaIsValid
  });

  yield put({
    type: ACTION_TYPES[FNA].UPDATE_IS_COMPLETED,
    isCompleted
  });
}

export function* updateCKAinit(action) {
  try {
    const ckaData = yield select(state => state[FNA].na.ckaSection.owner);

    if (ckaData.init) {
      yield put({
        type: ACTION_TYPES[FNA_CKA].UPDATE_CKA_OWNER_INIT,
        newInit: false
      });

      yield put({
        type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
        newNaIsChanged: true
      });
    }
    if (action && _.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* initCKA(action) {
  try {
    const ckaSection = yield select(state => state[FNA].na.ckaSection);
    const ckaData = ckaSection.owner;
    const dob = yield select(state => state[CLIENT].profile.dob);
    const birthYear = moment(dob).format("YYYY");

    yield put({
      type: ACTION_TYPES[FNA_CKA].INITIAL_VALIDATE,
      ckaData,
      birthYear
    });

    const newCkaData = yield select(state => state[FNA].na.ckaSection.owner);

    let noError = true;

    if (
      newCkaData.isCourseError.hasError === true ||
      newCkaData.isInstitutionError.hasError === true ||
      newCkaData.isStudyPeriodEndYearError.hasError === true ||
      newCkaData.isCollectiveInvestmentError.hasError === true ||
      newCkaData.isTransactionTypeError.hasError === true ||
      newCkaData.isInsuranceInvestmentError.hasError === true ||
      newCkaData.isInsuranceTypeError.hasError === true ||
      newCkaData.isProfessionError.hasError === true ||
      newCkaData.isYearsError.hasError === true
    ) {
      noError = false;
    }

    // if it already completed but it shows incomplete,
    // it won't trigger the save action.
    // So, set NA is changed, in order to trigger the save action.
    if (noError && !ckaSection.isValid) {
      yield put({
        type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
        newNaIsChanged: true
      });
    }

    yield updateCKAisValid();
    yield updateCKAinit();

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* courseSaga() {
  try {
    const ckaData = yield select(state => state[FNA].na.ckaSection.owner);
    const newCourse = yield select(
      state => state[FNA].na.ckaSection.owner.course
    );
    const dob = yield select(state => state[CLIENT].profile.dob);
    const birthYear = moment(dob).format("YYYY");

    if (newCourse !== "notApplicable" && newCourse !== "") {
      yield put({
        type: ACTION_TYPES[FNA_CKA].VALIDATE_INSTITUTION,
        newInstitution: ckaData.institution
      });

      yield put({
        type: ACTION_TYPES[FNA_CKA].VALIDATE_STUDYPERIODENDYEAR,
        newStudyPeriodEndYear: ckaData.studyPeriodEndYear,
        birthYear
      });
    }

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_COURSE,
      newCourse
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield put({
      type: ACTION_TYPES[FNA_CKA].UPDATE_PASS_CKA,
      newPassCka: isPassCKA(ckaData)
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* institutionSaga() {
  try {
    const newInstitution = yield select(
      state => state[FNA].na.ckaSection.owner.institution
    );
    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_INSTITUTION,
      newInstitution
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* studyPeriodEndYearSaga() {
  try {
    const newStudyPeriodEndYear = yield select(
      state => state[FNA].na.ckaSection.owner.studyPeriodEndYear
    );
    const dob = yield select(state => state[CLIENT].profile.dob);
    const birthYear = moment(dob).format("YYYY");

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_STUDYPERIODENDYEAR,
      newStudyPeriodEndYear,
      birthYear
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* collectiveInvestmentSaga() {
  try {
    const ckaData = yield select(state => state[FNA].na.ckaSection.owner);
    const newCollectiveInvestment = yield select(
      state => state[FNA].na.ckaSection.owner.collectiveInvestment
    );

    if (newCollectiveInvestment === "Y") {
      yield put({
        type: ACTION_TYPES[FNA_CKA].VALIDATE_TRANSACTIONTYPE,
        newTransactionType: ckaData.transactionType
      });
    }

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_COLLECTIVEINVESTMENT,
      newCollectiveInvestment
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield put({
      type: ACTION_TYPES[FNA_CKA].UPDATE_PASS_CKA,
      newPassCka: isPassCKA(ckaData)
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* transactionTypeSaga() {
  try {
    const newTransactionType = yield select(
      state => state[FNA].na.ckaSection.owner.transactionType
    );

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_TRANSACTIONTYPE,
      newTransactionType
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* insuranceInvestmentSaga() {
  try {
    const ckaData = yield select(state => state[FNA].na.ckaSection.owner);
    const newInsuranceInvestment = yield select(
      state => state[FNA].na.ckaSection.owner.insuranceInvestment
    );

    if (newInsuranceInvestment === "Y") {
      yield put({
        type: ACTION_TYPES[FNA_CKA].VALIDATE_INSURANCETYPE,
        newInsuranceType: ckaData.insuranceType
      });
    }

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_INSURANCEINVESTMENT,
      newInsuranceInvestment
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield put({
      type: ACTION_TYPES[FNA_CKA].UPDATE_PASS_CKA,
      newPassCka: isPassCKA(ckaData)
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* insuranceTypeSaga() {
  try {
    const newInsuranceType = yield select(
      state => state[FNA].na.ckaSection.owner.insuranceType
    );

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_INSURANCETYPE,
      newInsuranceType
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* professionSaga() {
  try {
    const ckaData = yield select(state => state[FNA].na.ckaSection.owner);
    const newProfession = yield select(
      state => state[FNA].na.ckaSection.owner.profession
    );

    if (newProfession !== "notApplicable" && newProfession !== "") {
      yield put({
        type: ACTION_TYPES[FNA_CKA].VALIDATE_YEARS,
        newYears: ckaData.years
      });
    }

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_PROFESSION,
      newProfession
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield put({
      type: ACTION_TYPES[FNA_CKA].UPDATE_PASS_CKA,
      newPassCka: isPassCKA(ckaData)
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
export function* yearsSaga() {
  try {
    const newYears = yield select(
      state => state[FNA].na.ckaSection.owner.years
    );

    yield put({
      type: ACTION_TYPES[FNA_CKA].VALIDATE_YEARS,
      newYears
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield updateCKAisValid();
  } catch (e) {
    // TODO
  }
}
