import * as _ from "lodash";
import { call, put, select } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  CLIENT,
  CLIENT_FORM,
  CONFIG,
  OPTIONS_MAP,
  FNA,
  AGENT,
  PRE_APPLICATION
} from "../constants/REDUCER_TYPES";
import { calcAge } from "../utilities/common";
import {
  formMapTrustedIndividuel,
  formMapProfile,
  profileMapForm
} from "../utilities/dataMapping";
import { optionCondition } from "../utilities/trigger";
import { isClientFormHasError } from "../utilities/validation";
// =============================================================================
// variables
// =============================================================================
const occupationCheckList = [
  "O674",
  "O675",
  "O1450",
  "O1132",
  "O1321",
  "O1322"
];
const occupationCheckListWithOutStudent = ["O674", "O675", "O1450", "O1132"];
const employStatusCheckListWithOutStudent = ["hh", "hw", "rt", "ue"];
// =============================================================================
// saga function
// =============================================================================

export function* multiClientFormValidation() {
  try {
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const hasErrorList = yield select(
      state => state[PRE_APPLICATION].multiClientProfile.hasErrorList
    );
    const newHasErrorList = _.cloneDeep(hasErrorList);
    const { hasError } = isClientFormHasError(clientForm);

    const hasErrorIndex = hasErrorList.findIndex(
      clientHasError => clientHasError.cid === clientForm.profileBackUp.cid
    );
    if (hasErrorIndex >= 0) {
      newHasErrorList[hasErrorIndex].hasError = hasError;
      yield put({
        type:
          ACTION_TYPES[PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR,
        newHasErrorList
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* availableInsuredProfileValidation() {
  try {
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const hasErrorList = yield select(
      state => state[CLIENT].availableInsuredProfile.hasErrorList
    );
    const newHasErrorList = _.cloneDeep(hasErrorList);
    const { hasError } = isClientFormHasError(clientForm);

    const hasErrorIndex = hasErrorList.findIndex(
      clientHasError => clientHasError.cid === clientForm.profileBackUp.cid
    );
    if (hasErrorIndex >= 0) {
      newHasErrorList[hasErrorIndex].hasError = hasError;
      yield put({
        type: ACTION_TYPES[CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
        newHasErrorList
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getProfileData(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);

    const { cid } = action;

    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });

    const profileResponse = yield call(callServer, {
      url: `client`,
      data: {
        action: "getProfile",
        docId: cid
      }
    });

    if (profileResponse.success) {
      const profile = profileMapForm(_.cloneDeep(profileResponse.profile));
      profile.type = ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT;

      yield put(profile);

      yield put({
        type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
        newIsFnaInvalid: !!profileResponse.showFnaInvalidFlag
      });
    }

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* unlinkRelationship(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const client = yield select(state => state[CLIENT]);
    const clientForm = yield select(state => state[CLIENT_FORM]);

    const saveClientAPI = yield call(callServer, {
      url: "client",
      data: {
        action: "unlinkRelationship",
        cid: client.profile.cid,
        fid: clientForm.profileBackUp.cid,
        confirm: true
      }
    });

    if (saveClientAPI.success) {
      yield put({
        type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
        newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
      });

      yield call(action.callback);

      yield put({
        type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
      });
    } else {
      // TODO
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* deleteClient(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const cid = yield select(state => state[CLIENT_FORM].profileBackUp.cid);

    const saveClientAPI = yield call(callServer, {
      url: "client",
      data: {
        action: "deleteProfile",
        cid,
        confirm: true
      }
    });

    if (saveClientAPI.success) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
      });

      yield call(action.callback);
    } else {
      // TODO
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateRelationship() {
  try {
    const relationshipData = yield select(
      state => state[CLIENT_FORM].relationship
    );
    const genderData = yield select(state => state[CLIENT_FORM].gender);
    const profileData = yield select(state => state[CLIENT].profile);
    const configData = yield select(state => state[CLIENT_FORM].config);
    const isPDA = yield select(state => state[CLIENT_FORM].config.isPDA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isFamilyMemberData = yield select(
      state => state[CLIENT_FORM].config.isFamilyMember
    );
    const newCid = yield select(state => state[CLIENT_FORM].profileBackUp.cid);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_RELATIONSHIP,
      relationshipData,
      genderData,
      profileData,
      configData,
      isFamilyMemberData,
      isPDA,
      newCid
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveClient(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const clientProfile = yield select(state => state[CLIENT].profile);
    const { confirm, callback } = action;

    /* get action */
    let actionName = "";
    if (clientForm.config.isFamilyMember) {
      actionName = "saveFamilyMember";
    } else if (clientForm.config.isCreate) {
      actionName = "addProfile";
    } else {
      actionName = "saveProfile";
    }

    /* map redux data to server structure data */
    // let profile = formMapProfile(clientForm);
    let profile = Object.assign(
      {},
      formMapProfile(clientForm),
      clientForm.profileBackUp
    );
    if (actionName !== "addProfile") {
      profile = Object.assign({}, profile, {
        agentCode: yield select(state => state[AGENT].agentCode),
        agentId: yield select(state => state[AGENT].agentCode)
      });
    }

    const saveClientAPI = yield call(callServer, {
      url: "client",
      data: {
        action: actionName,
        profile,
        photo: clientForm.photo,
        fid: clientForm.config.isFamilyMember ? profile.cid : undefined,
        cid: clientForm.config.isFamilyMember ? clientProfile.cid : undefined,
        tiPhoto: clientForm.config.isFamilyMember ? undefined : "",
        confirm
      }
    });

    if (saveClientAPI.success) {
      if (saveClientAPI.code) {
        yield call(callback, saveClientAPI.code);
      } else if (
        clientForm.config.isCreate &&
        !clientForm.config.isFamilyMember
      ) {
        yield put({
          type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
          profileData: saveClientAPI.profile
        });

        yield put({
          type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
          callback
        });
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
          newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
        });
        yield put({
          type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
        });
      } else {
        yield put({
          type: ACTION_TYPES[CLIENT].GET_PROFILE,
          cid: clientProfile.cid,
          callback
        });
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
          newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
        });
        yield put({
          type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
        });
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveTrustedIndividual(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const cid = yield select(state => state[CLIENT].profile.cid);
    const currentPage = yield select(state => state[FNA].currentPage);

    // get tiPhoto
    let tiPhoto;
    if (clientForm.photo.uri) {
      const base64Data = clientForm.photo.uri.replace(
        "data:image/jpeg;base64,",
        ""
      );

      tiPhoto = {
        type: "data:image/jpeg;base64,",
        value: base64Data
      };
    }

    const response = yield call(callServer, {
      url: "client",
      data: {
        action: "saveTrustedIndividual",
        cid,
        tiPhoto,
        tiInfo: formMapTrustedIndividuel(clientForm),
        confirm: action.confirm
      }
    });

    if (response.success) {
      if (response.code) {
        action.alert(response.code);
      } else {
        yield put({
          type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
          profileData: response.profile
        });
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
          newIsFnaInvalid: !!response.showFnaInvalidFlag
        });
        action.callback();

        if (currentPage !== "PDA") {
          yield put({
            type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
          });
        }
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveClientEmail(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const clientProfile = yield select(state => state[CLIENT].profile);

    const profile = Object.assign({}, clientProfile, {
      email: clientForm.email
    });

    const saveClientAPI = yield call(callServer, {
      url: "client",
      data: {
        action: "saveProfile",
        profile,
        photo: clientProfile.photo,
        tiPhoto: "",
        confirm: true
      }
    });

    if (saveClientAPI.success) {
      yield put({
        type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
        profileData: saveClientAPI.profile
      });
    }

    if (_.isFunction(action.callback)) {
      yield call(action.callback);
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* initialValidateSaga(action) {
  try {
    const client = yield select(state => state[CLIENT]);
    const clientForm = yield select(state => state[CLIENT_FORM]);
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE,
      configData: clientForm.config,
      isPDA: clientForm.config.isPDA,
      isFNA: clientForm.config.isFNA,
      isAPP: clientForm.config.isAPP,
      relationshipData: clientForm.relationship,
      genderData: clientForm.gender,
      profileData: client.profile,
      incomeData: clientForm.income,
      maritalData: clientForm.maritalStatus,
      educationData: clientForm.education,
      isFamilyMemberData: clientForm.config.isFamilyMember,
      isProposerMissingData: clientForm.config.isProposerMissing,
      relationshipOtherData: clientForm.relationshipOther,
      givenNameData: clientForm.givenName,
      nameData: clientForm.givenName,
      titleData: clientForm.title,
      isCreateData: clientForm.config.isCreate,
      singaporePRStatusData: clientForm.singaporePRStatus,
      nationalityData: clientForm.nationality,
      cityOfResidenceData: clientForm.cityOfResidence,
      countryOfResidenceData: clientForm.countryOfResidence,
      optionsMapData,
      isApplicationData: clientForm.config.isApplication,
      otherCityOfResidenceData: clientForm.otherCityOfResidence,
      IDData: clientForm.ID,
      IDDocumentTypeData: clientForm.IDDocumentType,
      IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
      employStatusData: clientForm.employStatus,
      employStatusOtherData: clientForm.employStatusOther,
      industryData: clientForm.industry,
      smokingStatusData: clientForm.smokingStatus,
      occupationData: clientForm.occupation,
      birthdayData: clientForm.birthday,
      occupationOtherData: clientForm.occupationOther,
      languageData: clientForm.language,
      languageOtherData: clientForm.languageOther,
      emailData: clientForm.email,
      clientFormCid: clientForm.profileBackUp.cid,
      prefixAData: clientForm.prefixA,
      mobileNoAData: clientForm.mobileNoA,
      typeOfPassData: clientForm.typeOfPass,
      typeOfPassOtherData: clientForm.typeOfPassOther
    });

    yield call(action.callback);
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* passSaga() {
  try {
    const singaporePRStatus = yield select(
      state => state[CLIENT_FORM].singaporePRStatus
    );

    if (singaporePRStatus === "Y") {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
        newTypeOfPass: ""
      });

      yield put({
        type: ACTION_TYPES[CLIENT_FORM].PASS_EXPIRY_DATE_ON_CHANGE,
        newPassExpiryDate: NaN
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* relationShipOtherSaga() {
  try {
    const relationshipData = yield select(
      state => state[CLIENT_FORM].relationship
    );
    const relationshipOtherData = yield select(
      state => state[CLIENT_FORM].relationshipOther
    );

    if (relationshipData !== "OTH" && relationshipOtherData !== "") {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE,
        newRelationshipOther: ""
      });
    } else {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].VALIDATE_RELATIONSHIP_OTHER,
        relationshipOtherData,
        relationshipData
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateGivenName() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const givenNameData = yield select(state => state[CLIENT_FORM].givenName);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_GIVEN_NAME,
      givenNameData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateSmoking() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const isInsuredMissingData = yield select(
      state => state[CLIENT_FORM].config.isInsuredMissing
    );
    const smokingStatusData = yield select(
      state => state[CLIENT_FORM].smokingStatus
    );
    const isShieldQuotation = yield select(
      state => state[CLIENT_FORM].config.isShieldQuotation
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_SMOKING_STATUS,
      smokingStatusData,
      isProposerMissingData: isProposerMissingData || isInsuredMissingData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
    if (isShieldQuotation) {
      yield availableInsuredProfileValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIndustry() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const isInsuredMissingData = yield select(
      state => state[CLIENT_FORM].config.isInsuredMissing
    );
    const industryData = yield select(state => state[CLIENT_FORM].industry);
    const isShieldQuotation = yield select(
      state => state[CLIENT_FORM].config.isShieldQuotation
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_INDUSTRY,
      industryData,
      isProposerMissingData: isProposerMissingData || isInsuredMissingData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
    if (isShieldQuotation) {
      yield availableInsuredProfileValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* genderSaga() {
  try {
    const relationship = yield select(state => state[CLIENT_FORM].relationship);
    const clientGender = yield select(state => state[CLIENT].profile.gender);

    let newGender = null;

    if (["GFA", "SON", "FAT", "BRO", "GSO"].indexOf(relationship) > -1) {
      newGender = "M";
    } else if (["GMO", "MOT", "DAU", "GDA", "SIS"].indexOf(relationship) > -1) {
      newGender = "F";
    } else if (relationship === "SPO") {
      if (clientGender === "M") {
        newGender = "F";
      } else {
        newGender = "M";
      }
    }

    if (newGender !== null) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].GENDER_ON_CHANGE,
        newGender
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateGender() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const genderData = yield select(state => state[CLIENT_FORM].gender);
    const configData = yield select(state => state[CLIENT_FORM].config);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_GENDER,
      genderData,
      configData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateEmployStatus() {
  try {
    const genderData = yield select(state => state[CLIENT_FORM].gender);
    const employStatusData = yield select(
      state => state[CLIENT_FORM].employStatus
    );
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_EMPLOY_STATUS,
      employStatusData,
      genderData,
      isFNA
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateEmployStatusOther() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const employStatusData = yield select(
      state => state[CLIENT_FORM].employStatus
    );
    const employStatusOtherData = yield select(
      state => state[CLIENT_FORM].employStatusOther
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_EMPLOY_STATUS_OTHER,
      employStatusData,
      employStatusOtherData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* singaporePRStatusValidation() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const singaporePRStatusData = yield select(
      state => state[CLIENT_FORM].singaporePRStatus
    );
    const nationalityData = yield select(
      state => state[CLIENT_FORM].nationality
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_SINGAPORE_PR_STATUS,
      singaporePRStatusData,
      nationalityData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIDDocumentTypeOther() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const IDDocumentTypeOtherData = yield select(
      state => state[CLIENT_FORM].IDDocumentTypeOther
    );
    const IDDocumentTypeData = yield select(
      state => state[CLIENT_FORM].IDDocumentType
    );
    if (IDDocumentTypeData !== "other" && IDDocumentTypeOtherData !== "") {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE,
        newIDDocTypeOther: ""
      });
    } else {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE_OTHER,
        IDDocumentTypeOtherData,
        IDDocumentTypeData
      });
    }

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIDDocumentType() {
  try {
    const configData = yield select(state => state[CLIENT_FORM].config);
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isPDA = yield select(state => state[CLIENT_FORM].config.isPDA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const IDDocumentTypeData = yield select(
      state => state[CLIENT_FORM].IDDocumentType
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE,
      IDDocumentTypeData,
      configData,
      isFNA,
      isPDA,
      isAPP
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* maritalStatusSaga() {
  try {
    const relationshipData = yield select(
      state => state[CLIENT_FORM].relationship
    );
    const newMaritalStatus = yield select(
      state => state[CLIENT_FORM].maritalStatus
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].MARITAL_STATUS_ON_CHANGE,
      newMaritalStatus,
      relationshipData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateMaritalStatus(action) {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const maritalData = yield select(state => state[CLIENT_FORM].maritalStatus);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_MARITAL_STATUS,
      maritalData,
      isFNA,
      isAPP
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }

    if (_.isFunction(action.callback)) {
      yield put(action.callback);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* autoFillName() {
  try {
    const givenName = yield select(state => state[CLIENT_FORM].givenName);
    const surname = yield select(state => state[CLIENT_FORM].surname);
    const nameOrder = yield select(state => state[CLIENT_FORM].nameOrder);

    let newName = "";
    if (nameOrder === "L") {
      newName = `${surname} ${givenName}`;
    } else if (nameOrder === "F") {
      newName = `${givenName} ${surname}`;
    }

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].NAME_ON_CHANGE,
      newName
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* nameSaga() {
  try {
    const nameData = yield select(state => state[CLIENT_FORM].name);
    const givenNameData = yield select(state => state[CLIENT_FORM].givenName);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_NAME,
      nameData,
      givenNameData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateTitle() {
  try {
    const genderData = yield select(state => state[CLIENT_FORM].gender);
    const titleData = yield select(state => state[CLIENT_FORM].title);
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isTrustedIndividual = yield select(
      state => state[CLIENT_FORM].config.isTrustedIndividual
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_TITLE,
      titleData,
      genderData,
      isFNA,
      isTrustedIndividual
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* employStatusSaga(action) {
  try {
    if (!action["@@redux-saga/SAGA_ACTION"]) {
      const birthday = yield select(state => state[CLIENT_FORM].birthday);
      const age = calcAge(birthday);
      const occupation = yield select(state => state[CLIENT_FORM].occupation);
      const { occupationCurrently } = action;
      let newEmployStatus = null;

      if (
        age < 18 &&
        action.type === ACTION_TYPES[CLIENT_FORM].BIRTHDAY_ON_CHANGE
      ) {
        newEmployStatus = "sd";
      }

      switch (true) {
        case ["O1321", "O1322"].indexOf(occupation) > -1: {
          newEmployStatus = "sd";
          break;
        }
        case occupation === "O674": {
          newEmployStatus = "hh";
          break;
        }
        case occupation === "O675": {
          newEmployStatus = "hw";
          break;
        }
        case occupation === "O1450": {
          newEmployStatus = "ue";
          break;
        }
        case occupation === "O1132": {
          newEmployStatus = "rt";
          break;
        }
        case occupationCheckList.indexOf(occupationCurrently) > -1 &&
          occupationCheckList.indexOf(occupation) === -1: {
          newEmployStatus = "";
          break;
        }
        default:
          break;
      }

      if (newEmployStatus !== null) {
        yield put({
          type: ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
          newEmployStatus
        });
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* employStatusOtherSaga(action) {
  try {
    if (!action["@@redux-saga/SAGA_ACTION"]) {
      const employStatus = yield select(
        state => state[CLIENT_FORM].employStatus
      );
      const employStatusOther = yield select(
        state => state[CLIENT_FORM].employStatusOther
      );

      if (employStatus.indexOf("ot") === -1 && employStatusOther !== "") {
        yield put({
          type: ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE,
          newEmployStatusOther: ""
        });
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* occupationSaga(action) {
  try {
    if (!action["@@redux-saga/SAGA_ACTION"]) {
      const optionsMap = yield select(state => state[OPTIONS_MAP]);
      const employStatus = yield select(
        state => state[CLIENT_FORM].employStatus
      );
      const occupation = yield select(state => state[CLIENT_FORM].occupation);
      const birthday = yield select(state => state[CLIENT_FORM].birthday);
      const age = calcAge(birthday);

      let newOccupation = null;
      let isChangeOccupation = false;

      /* employ status rule */
      switch (employStatus) {
        case "hh": {
          newOccupation = "O674";
          isChangeOccupation = true;
          break;
        }
        case "hw": {
          newOccupation = "O675";
          isChangeOccupation = true;
          break;
        }
        case "ue": {
          newOccupation = "O1450";
          isChangeOccupation = true;
          break;
        }
        case "rt": {
          newOccupation = "O1132";
          isChangeOccupation = true;
          break;
        }
        case "ot":
        case "se":
        case "pt":
        case "ft": {
          if (occupationCheckListWithOutStudent.indexOf(occupation) > -1) {
            newOccupation = "";
            isChangeOccupation = true;
          }
          break;
        }
        case "sd": {
          if (birthday === "") {
            newOccupation = "";
            isChangeOccupation = true;
          }
          break;
        }
        default:
          break;
      }

      if (!isChangeOccupation) {
        /* age and employ status rule */
        if (age < 6) {
          newOccupation = "O246";
        } else if (age >= 6 && age < 18 && employStatus === "sd") {
          newOccupation = "O1322";
        } else if (age >= 18 && employStatus === "sd") {
          newOccupation = "O1321";
        }

        /* industry rule */
        if (action.newIndustry) {
          const occupationOpt = _.find(
            _.get(optionsMap, "occupation.options"),
            opt => opt.value === occupation
          );

          const occupationOptIndustry = _.get(occupationOpt, "condition", "");
          newOccupation =
            occupationOptIndustry === action.newIndustry ? null : "";
        }
      }

      if (newOccupation !== null) {
        yield put({
          type: ACTION_TYPES[CLIENT_FORM].OCCUPATION_ON_CHANGE,
          newOccupation
        });
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateOccupation() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const isInsuredMissingData = yield select(
      state => state[CLIENT_FORM].config.isInsuredMissing
    );
    const occupationData = yield select(state => state[CLIENT_FORM].occupation);
    const birthdayData = yield select(state => state[CLIENT_FORM].birthday);
    const isShieldQuotation = yield select(
      state => state[CLIENT_FORM].config.isShieldQuotation
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_OCCUPATION,
      occupationData,
      birthdayData,
      isProposerMissingData: isProposerMissingData || isInsuredMissingData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
    if (isShieldQuotation) {
      yield availableInsuredProfileValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* languageOtherSaga(action) {
  try {
    const language = yield select(state => state[CLIENT_FORM].language);
    const languageOther = yield select(
      state => state[CLIENT_FORM].languageOther
    );
    if (
      !action["@@redux-saga/SAGA_ACTION"] &&
      language.indexOf("other") === -1 &&
      languageOther !== ""
    ) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE,
        newLanguageOther: ""
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* occupationOtherSaga() {
  try {
    const occupationData = yield select(state => state[CLIENT_FORM].occupation);
    const occupationOtherData = yield select(
      state => state[CLIENT_FORM].occupationOther
    );

    if (occupationData !== "O921" && occupationOtherData !== "") {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].OCCUPATION_OTHER_ON_CHANGE,
        newOccupationOther: ""
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* industrySaga(action) {
  try {
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const occupation = yield select(state => state[CLIENT_FORM].occupation);
    const employStatus = yield select(state => state[CLIENT_FORM].employStatus);
    const rule =
      employStatus !== "ft" ||
      employStatus !== "pt" ||
      employStatus !== "se" ||
      employStatus !== "ot";
    let newIndustry = null;

    if (rule && !action["@@redux-saga/SAGA_ACTION"]) {
      const occupationOpt = _.find(
        _.get(optionsMap, "occupation.options"),
        opt => opt.value === occupation
      );

      newIndustry = _.get(occupationOpt, "condition", "");
    }

    if (newIndustry !== null) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].INDUSTRY_ON_CHANGE,
        newIndustry
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* nameOfEmployBusinessSchoolSaga(action) {
  try {
    const occupation = yield select(state => state[CLIENT_FORM].occupation);
    const { occupationCurrently } = action;
    let newNameOfEmployBusinessSchool = yield select(
      state => state[CLIENT_FORM].nameOfEmployBusinessSchool
    );
    const employStatusData = yield select(
      state => state[CLIENT_FORM].employStatus
    );
    const birthdayData = yield select(state => state[CLIENT_FORM].birthday);
    const ageBelowSix = calcAge(birthdayData) < 6;

    /* rule of occupation change */
    if (
      (!action["@@redux-saga/SAGA_ACTION"] &&
        occupationCheckList.indexOf(occupation) > -1 &&
        occupation.indexOf(occupationCurrently) === -1 &&
        action.type !== "clientForm/BIRTHDAY_ON_CHANGE") ||
      occupation === ""
    ) {
      newNameOfEmployBusinessSchool = "";
    }

    /* rule of employStatus change */
    if (employStatusCheckListWithOutStudent.indexOf(employStatusData) === -1) {
      newNameOfEmployBusinessSchool =
        newNameOfEmployBusinessSchool === "N/A"
          ? ""
          : newNameOfEmployBusinessSchool;
    }

    if (ageBelowSix) {
      newNameOfEmployBusinessSchool = "N/A";
    }

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
      newNameOfEmployBusinessSchool,
      employStatusData,
      birthdayData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* countryOfEmployBusinessSchoolSaga(action) {
  try {
    const occupation = yield select(state => state[CLIENT_FORM].occupation);
    const { occupationCurrently } = action;

    let newCountryOfEmployBusinessSchool = yield select(
      state => state[CLIENT_FORM].countryOfEmployBusinessSchool
    );
    const employStatusData = yield select(
      state => state[CLIENT_FORM].employStatus
    );
    const birthdayData = yield select(state => state[CLIENT_FORM].birthday);
    const ageBelowSix = calcAge(birthdayData) < 6;

    /* rule of occupation change */
    if (employStatusCheckListWithOutStudent.indexOf(employStatusData) === -1) {
      newCountryOfEmployBusinessSchool =
        newCountryOfEmployBusinessSchool === "na"
          ? "R2"
          : newCountryOfEmployBusinessSchool;
    } else if (
      !action["@@redux-saga/SAGA_ACTION"] &&
      occupationCheckList.indexOf(occupation) > -1 &&
      occupation.indexOf(occupationCurrently) === -1
    ) {
      newCountryOfEmployBusinessSchool =
        newCountryOfEmployBusinessSchool === "na"
          ? "R2"
          : newCountryOfEmployBusinessSchool;
    }

    if (ageBelowSix) {
      newCountryOfEmployBusinessSchool = "na";
    }

    const type =
      ACTION_TYPES[CLIENT_FORM].COUNTRY_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE;

    yield put({
      type,
      newCountryOfEmployBusinessSchool,
      employStatusData,
      birthdayData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateTypeOfPass() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const typeOfPassData = yield select(state => state[CLIENT_FORM].typeOfPass);
    const nationalityData = yield select(
      state => state[CLIENT_FORM].nationality
    );
    const singaporePRStatusData = yield select(
      state => state[CLIENT_FORM].singaporePRStatus
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_TYPE_OF_PASS,
      typeOfPassData,
      nationalityData,
      singaporePRStatusData,
      isAPP
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateTypeOfPassOther() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const typeOfPassData = yield select(state => state[CLIENT_FORM].typeOfPass);
    const typeOfPassOtherData = yield select(
      state => state[CLIENT_FORM].typeOfPassOther
    );
    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_TYPE_OF_PASS_OTHER,
      typeOfPassData,
      typeOfPassOtherData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* TypeOfPassOtherSaga(action) {
  try {
    const typeOfPassData = yield select(state => state[CLIENT_FORM].typeOfPass);
    const typeOfPassOtherData = yield select(
      state => state[CLIENT_FORM].typeOfPassOther
    );
    if (
      !action["@@redux-saga/SAGA_ACTION"] &&
      typeOfPassData.indexOf("o") === -1 &&
      typeOfPassOtherData !== ""
    ) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE,
        newTypeOfPassOther: ""
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* singaporePRStatusSaga() {
  try {
    const nationality = yield select(state => state[CLIENT_FORM].nationality);
    const singaporePRStatus = yield select(
      state => state[CLIENT_FORM].singaporePRStatus
    );

    if (
      !!nationality &&
      (nationality === "N1" || nationality === "N2") &&
      singaporePRStatus !== ""
    ) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE,
        newSingaporePRStatus: ""
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateNationality() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const isInsuredMissingData = yield select(
      state => state[CLIENT_FORM].config.isInsuredMissing
    );
    const nationalityData = yield select(
      state => state[CLIENT_FORM].nationality
    );
    const isShieldQuotation = yield select(
      state => state[CLIENT_FORM].config.isShieldQuotation
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_NATIONALITY,
      nationalityData,
      isProposerMissingData: isProposerMissingData || isInsuredMissingData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
    if (isShieldQuotation) {
      yield availableInsuredProfileValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* countrySaga() {
  try {
    const newCountry = yield select(state => state[CLIENT_FORM].country);
    const countryOfResidenceData = yield select(
      state => state[CLIENT_FORM].countryOfResidence
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].COUNTRY_ON_CHANGE,
      newCountry,
      countryOfResidenceData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* postalCodeSaga(action) {
  try {
    const countryOfResidence = yield select(
      state => state[CLIENT_FORM].countryOfResidence
    );
    if (countryOfResidence === "R2") {
      const postalCode = yield select(state => state[CLIENT_FORM].postal);
      const callServer = yield select(state => state[CONFIG].callServer);
      const covPostalCode =
        postalCode && postalCode.length === 6
          ? Number(_.toString(postalCode).substr(0, 2))
          : Number(`0${_.toString(postalCode).substr(0, 1)}`);
      let resultFilename;
      const mappingTable = {
        0: "_00_19",
        19: "_20_29",
        29: "_30_39",
        39: "_40_49",
        49: "_50_59",
        59: "_60_69",
        69: "_70_79",
        79: "_80_89"
      };
      let keyNumber;
      _.forEach(Object.keys(mappingTable).sort((a, b) => b - a), obj => {
        keyNumber = Number(obj);
        if (covPostalCode > keyNumber && !resultFilename) {
          resultFilename = mappingTable[obj];
        }
      });

      if (resultFilename) {
        const resp = yield call(callServer, {
          url: `client`,
          data: {
            action: "getAddressByPostalCode",
            pC: postalCode,
            fileName: resultFilename
          }
        });

        if (resp.success) {
          yield put({
            type: ACTION_TYPES[CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
            newBlockHouse: resp.BLDGNO
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].STREET_ROAD_ON_CHANGE,
            newStreetRoad: resp.STREETNAME
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].UNIT_ON_CHANGE,
            newUnit: ""
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
            newBuildingEstate: resp.BLDGNAME
          });
        } else {
          /* To notify the user in alert function */
          action.alertFunction();
          yield put({
            type: ACTION_TYPES[CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
            newBlockHouse: ""
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].STREET_ROAD_ON_CHANGE,
            newStreetRoad: ""
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].UNIT_ON_CHANGE,
            newUnit: ""
          });

          yield put({
            type: ACTION_TYPES[CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
            newBuildingEstate: ""
          });
        }
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* isCityOfResidenceErrorSaga() {
  try {
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);
    const cityOfResidenceData = yield select(
      state => state[CLIENT_FORM].cityOfResidence
    );
    const countryOfResidenceData = yield select(
      state => state[CLIENT_FORM].countryOfResidence
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_CITY_OF_RESIDENCE,
      cityOfResidenceData,
      countryOfResidenceData,
      optionsMapData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* cityOfResidenceSaga() {
  try {
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const cityOfResidence = yield select(
      state => state[CLIENT_FORM].cityOfResidence
    );

    let newCityOfResidence = null;

    if (
      !optionCondition({
        value: cityOfResidence,
        options: "city",
        optionsMap,
        showIfNoValue: false
      }) &&
      cityOfResidence !== ""
    ) {
      newCityOfResidence = "";
    }

    if (newCityOfResidence !== null) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE,
        newCityOfResidence
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateOtherCityOfResidence() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);
    const isApplicationData = yield select(
      state => state[CLIENT_FORM].config.isApplication
    );
    const cityOfResidenceData = yield select(
      state => state[CLIENT_FORM].cityOfResidence
    );
    const otherCityOfResidenceData = yield select(
      state => state[CLIENT_FORM].otherCityOfResidence
    );

    if (
      !_.find(
        _.get(optionsMap, "city.options"),
        opt =>
          opt.value === cityOfResidenceData &&
          (opt.value === "T110" ||
            opt.value === "T120" ||
            opt.value === "T180" ||
            opt.value === "T205" ||
            opt.value === "T207" ||
            opt.value === "T208" ||
            opt.value === "T209" ||
            opt.value === "T224")
      ) &&
      otherCityOfResidenceData !== ""
    ) {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE,
        newOtherCityOfResidence: ""
      });
    } else {
      yield put({
        type: ACTION_TYPES[CLIENT_FORM].VALIDATE_OTHER_CITY_OF_RESIDENCE,
        optionsMapData,
        isApplicationData,
        cityOfResidenceData,
        otherCityOfResidenceData
      });
    }

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateID() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const IDData = yield select(state => state[CLIENT_FORM].ID);
    const IDDocumentTypeData = yield select(
      state => state[CLIENT_FORM].IDDocumentType
    );
    const configData = yield select(state => state[CLIENT_FORM].config);
    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_ID,
      IDData,
      IDDocumentTypeData,
      configData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateBirthday() {
  try {
    const birthdayData = yield select(state => state[CLIENT_FORM].birthday);

    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const isInsuredMissingData = yield select(
      state => state[CLIENT_FORM].config.isInsuredMissing
    );

    const isFamilyMemberData = yield select(
      state => state[CLIENT_FORM].config.isFamilyMember
    );
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isPDA = yield select(state => state[CLIENT_FORM].config.isPDA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isShieldQuotation = yield select(
      state => state[CLIENT_FORM].config.isShieldQuotation
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_BIRTHDAY,
      birthdayData,
      isProposerMissingData: isProposerMissingData || isInsuredMissingData,
      isFamilyMemberData,
      isFNA,
      isPDA,
      isAPP
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
    if (isShieldQuotation) {
      yield availableInsuredProfileValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* cityStateSaga() {
  try {
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const cityOfResidence = yield select(
      state => state[CLIENT_FORM].cityOfResidence
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].CITY_STATE_ON_CHANGE,
      newCityState: _.find(
        _.get(optionsMap, "city.options"),
        opt => opt.value === cityOfResidence
      ).value
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* mobileNoASaga() {
  try {
    const newMobileNoA = yield select(state => state[CLIENT_FORM].mobileNoA);
    const prefixAData = yield select(state => state[CLIENT_FORM].prefixA);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
      newMobileNoA,
      prefixAData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* mobileNoBSaga() {
  try {
    const newMobileNoB = yield select(state => state[CLIENT_FORM].mobileNoB);
    const prefixBData = yield select(state => state[CLIENT_FORM].prefixB);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_B_ON_CHANGE,
      newMobileNoB,
      prefixBData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateLanguageOther() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const languageData = yield select(state => state[CLIENT_FORM].language);
    const languageOtherData = yield select(
      state => state[CLIENT_FORM].languageOther
    );

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_LANGUAGE_OTHER,
      languageData,
      languageOtherData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* documentTypeSaga() {
  try {
    const singaporePRStatusData = yield select(
      state => state[CLIENT_FORM].singaporePRStatus
    );
    const nationalityData = yield select(
      state => state[CLIENT_FORM].nationality
    );

    let newIDDocType = null;
    if (
      nationalityData === "N1" ||
      nationalityData === "N2" ||
      singaporePRStatusData === "Y"
    ) {
      newIDDocType = "nric";
    } else {
      newIDDocType = yield select(state => state[CLIENT_FORM].IDDocumentType);
    }

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
      newIDDocType
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIncome() {
  try {
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const incomeData = yield select(state => state[CLIENT_FORM].income);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_INCOME,
      incomeData,
      isFNA,
      isAPP
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateEducation() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const educationData = yield select(state => state[CLIENT_FORM].education);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_EDUCATION,
      educationData,
      isFNA
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateMobileNoA() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const configData = yield select(state => state[CLIENT_FORM].config);
    const mobileNoAData = yield select(state => state[CLIENT_FORM].mobileNoA);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_MOBILE_A_NO,
      mobileNoAData,
      configData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validatePrefixA() {
  try {
    const isFNA = yield select(state => state[CLIENT_FORM].config.isFNA);
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const prefixAData = yield select(state => state[CLIENT_FORM].prefixA);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_PREFIX_A,
      prefixAData,
      isFNA
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateEmail() {
  try {
    const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
    const isProposerMissingData = yield select(
      state => state[CLIENT_FORM].config.isProposerMissing
    );
    const emailData = yield select(state => state[CLIENT_FORM].email);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_EMAIL,
      emailData,
      isAPP,
      isProposerMissingData
    });

    if (isAPP) {
      yield multiClientFormValidation();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateLanguage() {
  const isAPP = yield select(state => state[CLIENT_FORM].config.isAPP);
  const languageData = yield select(state => state[CLIENT_FORM].language);
  const configData = yield select(state => state[CLIENT_FORM].config);

  yield put({
    type: ACTION_TYPES[CLIENT_FORM].VALIDATE_LANGUAGE,
    languageData,
    configData
  });

  if (isAPP) {
    yield multiClientFormValidation();
  }
}

export function* updateClientProfileAndValidate(action) {
  try {
    const client = yield select(state => state[CLIENT]);
    const { cid, profile, configData } = action;
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);

    const loadClientData = Object.assign(
      {},
      profileMapForm(_.cloneDeep(profile)),
      {
        type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT
      }
    );
    yield put(loadClientData);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
      configData
    });

    const clientForm = yield select(state => state[CLIENT_FORM]);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE,
      configData: clientForm.config,
      isPDA: clientForm.config.isPDA,
      isFNA: clientForm.config.isFNA,
      isAPP: clientForm.config.isAPP,
      isShieldQuotation: clientForm.config.isShieldQuotation,
      relationshipData: clientForm.relationship,
      genderData: clientForm.gender,
      profileData: client.profile,
      incomeData: clientForm.income,
      maritalData: clientForm.maritalStatus,
      educationData: clientForm.education,
      isFamilyMemberData: clientForm.config.isFamilyMember,
      isProposerMissingData: clientForm.config.isProposerMissing,
      isInsuredMissingData: clientForm.config.isInsuredMissing,
      relationshipOtherData: clientForm.relationshipOther,
      givenNameData: clientForm.givenName,
      nameData: clientForm.givenName,
      titleData: clientForm.title,
      isCreateData: clientForm.config.isCreate,
      singaporePRStatusData: clientForm.singaporePRStatus,
      nationalityData: clientForm.nationality,
      cityOfResidenceData: clientForm.cityOfResidence,
      countryOfResidenceData: clientForm.countryOfResidence,
      optionsMapData,
      isApplicationData: clientForm.config.isApplication,
      otherCityOfResidenceData: clientForm.otherCityOfResidence,
      IDData: clientForm.ID,
      IDDocumentTypeData: clientForm.IDDocumentType,
      IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
      employStatusData: clientForm.employStatus,
      employStatusOtherData: clientForm.employStatusOther,
      industryData: clientForm.industry,
      occupationData: clientForm.occupation,
      birthdayData: clientForm.birthday,
      smokingStatusData: clientForm.smokingStatus,
      occupationOtherData: clientForm.occupationOther,
      languageData: clientForm.language,
      languageOtherData: clientForm.languageOther,
      emailData: clientForm.email,
      clientFormCid: clientForm.profileBackUp.cid,
      prefixAData: clientForm.prefixA,
      mobileNoAData: clientForm.mobileNoA,
      typeOfPassData: clientForm.typeOfPass,
      typeOfPassOtherData: clientForm.typeOfPassOther
    });

    const validatedClientForm = yield select(state => state[CLIENT_FORM]);
    const hasErrorList = yield select(
      state => state[CLIENT].availableInsuredProfile.hasErrorList
    );
    const newHasErrorList = _.cloneDeep(hasErrorList);

    const { hasError, errorFields } = isClientFormHasError(validatedClientForm);
    const clientHasErrorIndex = newHasErrorList.findIndex(
      clientHasError => clientHasError.cid === cid
    );
    if (clientHasErrorIndex >= 0) {
      newHasErrorList[clientHasErrorIndex].hasError = hasError;
    } else if (hasError) {
      newHasErrorList.push({ cid, hasError, errorFields });
    }

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
      newHasErrorList
    });

    yield call(action.callback);
  } catch (e) {
    // TODO need error handling
  }
}

export function* loadClientProfileAndInitValidate(action) {
  try {
    const client = yield select(state => state[CLIENT]);
    const { cid, profile, configData } = action;
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);

    const loadClientData = Object.assign(
      {},
      profileMapForm(_.cloneDeep(profile)),
      {
        type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT
      }
    );
    yield put(loadClientData);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
      configData
    });

    const clientForm = yield select(state => state[CLIENT_FORM]);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE,
      configData: clientForm.config,
      isPDA: clientForm.config.isPDA,
      isFNA: clientForm.config.isFNA,
      isAPP: clientForm.config.isAPP,
      isShieldQuotation: clientForm.config.isShieldQuotation,
      relationshipData: clientForm.relationship,
      genderData: clientForm.gender,
      profileData: client.profile,
      incomeData: clientForm.income,
      maritalData: clientForm.maritalStatus,
      educationData: clientForm.education,
      isFamilyMemberData: clientForm.config.isFamilyMember,
      isProposerMissingData: clientForm.config.isProposerMissing,
      isInsuredMissingData: clientForm.config.isInsuredMissing,
      relationshipOtherData: clientForm.relationshipOther,
      givenNameData: clientForm.givenName,
      nameData: clientForm.givenName,
      titleData: clientForm.title,
      isCreateData: clientForm.config.isCreate,
      singaporePRStatusData: clientForm.singaporePRStatus,
      nationalityData: clientForm.nationality,
      cityOfResidenceData: clientForm.cityOfResidence,
      countryOfResidenceData: clientForm.countryOfResidence,
      optionsMapData,
      isApplicationData: clientForm.config.isApplication,
      otherCityOfResidenceData: clientForm.otherCityOfResidence,
      IDData: clientForm.ID,
      IDDocumentTypeData: clientForm.IDDocumentType,
      IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
      employStatusData: clientForm.employStatus,
      employStatusOtherData: clientForm.employStatusOther,
      industryData: clientForm.industry,
      occupationData: clientForm.occupation,
      birthdayData: clientForm.birthday,
      smokingStatusData: clientForm.smokingStatus,
      occupationOtherData: clientForm.occupationOther,
      languageData: clientForm.language,
      languageOtherData: clientForm.languageOther,
      emailData: clientForm.email,
      clientFormCid: clientForm.profileBackUp.cid,
      prefixAData: clientForm.prefixA,
      mobileNoAData: clientForm.mobileNoA,
      typeOfPassData: clientForm.typeOfPass,
      typeOfPassOtherData: clientForm.typeOfPassOther
    });

    const validatedClientForm = yield select(state => state[CLIENT_FORM]);
    const hasErrorList = yield select(
      state => state[CLIENT].availableInsuredProfile.hasErrorList
    );
    const newHasErrorList = _.cloneDeep(hasErrorList);

    const { hasError, errorFields } = isClientFormHasError(validatedClientForm);
    const clientHasErrorIndex = newHasErrorList.findIndex(
      clientHasError => clientHasError.cid === cid
    );

    if (clientHasErrorIndex >= 0) {
      newHasErrorList[clientHasErrorIndex].hasError = hasError;
    } else if (hasError) {
      newHasErrorList.push({ cid, hasError, errorFields });
    }

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
      newHasErrorList
    });

    yield call(action.callback);
  } catch (e) {
    // TODO need error handling
  }
}

export function* checkInsuredList(action) {
  try {
    const { pCid, iCids, isInsuredMissing } = action;
    const dependantProfiles = yield select(
      state => state[CLIENT].dependantProfiles
    );

    // check insured

    for (let i = 0; i < iCids.length; i += 1) {
      const iCid = iCids[i];
      if (pCid !== iCid) {
        const dependantProfile = dependantProfiles[iCid];

        if (dependantProfile) {
          yield loadClientProfileAndInitValidate({
            cid: iCid,
            profile: dependantProfile,
            configData: {
              isCreate: false,
              isFamilyMember: false,
              isProposerMissing: isInsuredMissing,
              isFNA: false,
              isAPP: false,
              isShieldQuotation: true
            }
          });
        }
      }
    }

    yield call(action.callback);
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* removeCompleteProfileFromList(action) {
  try {
    const hasErrorList = yield select(
      state => state[CLIENT].availableInsuredProfile.hasErrorList
    );
    let newHasErrorList = _.cloneDeep(hasErrorList);

    _.forEach(newHasErrorList, (newHasErrorListItem, newHasErrorListIndex) => {
      if (!newHasErrorListItem.hasError) {
        newHasErrorList = _.remove(newHasErrorList[newHasErrorListIndex]);
      }
    });

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
      newHasErrorList
    });

    yield call(action.callback);
  } catch (e) {
    /* TODO need error handling */
  }
}
