import * as _ from "lodash";
import { select, put, call } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { TEXT_BOX, TEXT_SELECTION } from "../constants/FIELD_TYPES";
import * as TOLERANCE_LEVEL from "../constants/TOLERANCE_LEVEL";
import {
  CONFIG,
  PRE_APPLICATION,
  RECOMMENDATION,
  CLIENT
} from "../constants/REDUCER_TYPES";
import { LANGUAGE_TYPES } from "../locales";
import * as validation from "../utilities/validation";

/**
 * Support functions
 */

const prepareErrorObject = (values, error) => {
  const defaultError = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };

  _.forEach(values, (value, key) => {
    if (key !== "extra") {
      if (_.isObject(value)) {
        error[key] = {};
        prepareErrorObject(value, error[key]);
      } else {
        error[key] = defaultError;
      }
    }
  });
};

const checkDataHasError = error => {
  let hasError = false;

  _.forEach(error, err => {
    if (_.isObject(err)) {
      if (_.has(err, "hasError")) {
        hasError = hasError || err.hasError;
      } else {
        hasError = hasError || checkDataHasError(err);
      }
    }
  });
  return hasError;
};

const validateRecommendationNormalData = ({
  value,
  error,
  isChosen,
  disabled
}) => {
  const newError = _.cloneDeep(error);
  const hasRop = _.get(value, "rop.choiceQ1") === "Y";

  _.set(
    newError,
    "reason",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "reason")
    })
  );
  _.set(
    newError,
    "benefit",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "benefit")
    })
  );
  _.set(
    newError,
    "limitation",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "limitation")
    })
  );
  if (isChosen) {
    _.set(
      newError,
      "rop.choiceQ1",
      validation.validateMandatory({
        field: {
          type: TEXT_SELECTION,
          mandatory: true,
          disabled
        },
        value: _.get(value, "rop.choiceQ1")
      })
    );

    _.set(
      newError,
      "rop.choiceQ1Sub1",
      validation.validateMandatory({
        field: {
          type: TEXT_SELECTION,
          mandatory: hasRop,
          disabled
        },
        value: _.get(value, "rop.choiceQ1Sub1")
      })
    );
    _.set(
      newError,
      "rop.choiceQ1Sub2",
      validation.validateMandatory({
        field: {
          type: TEXT_SELECTION,
          mandatory: hasRop,
          disabled
        },
        value: _.get(value, "rop.choiceQ1Sub2")
      })
    );
    _.set(
      newError,
      "rop.choiceQ1Sub3",
      validation.validateMandatory({
        field: { type: TEXT_BOX, mandatory: hasRop, disabled },
        value: _.get(value, "rop.choiceQ1Sub3")
      })
    );
    _.set(
      newError,
      "rop.replaceLife",
      validation.validateRecommendationROP({
        field: { mandatory: hasRop, disabled },
        value
      })
    );
  }

  return newError;
};

const validateRecommendationShieldData = ({
  value,
  error,
  isChosen,
  disabled
}) => {
  // prepare new state
  const newError = _.cloneDeep(error);
  const hasRop =
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_0") === "Y" ||
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_1") === "Y" ||
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_2") === "Y" ||
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_3") === "Y" ||
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_4") === "Y" ||
    _.get(value, "rop_shield.ropBlock.shieldRopAnswer_5") === "Y";

  _.set(
    newError,
    "reason",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "reason")
    })
  );
  _.set(
    newError,
    "benefit",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "benefit")
    })
  );
  _.set(
    newError,
    "limitation",
    validation.validateMandatory({
      field: { type: TEXT_BOX, mandatory: true, disabled },
      value: _.get(value, "limitation")
    })
  );
  if (isChosen) {
    const shieldRopAnswers = _.values(
      _.get(value, "rop_shield.ropBlock.iCidRopAnswerMap")
    );
    _.forEach(shieldRopAnswers, answer => {
      _.set(
        newError,
        `rop_shield.ropBlock.${answer}`,
        validation.validateMandatory({
          field: {
            type: TEXT_SELECTION,
            mandatory: true,
            disabled
          },
          value: _.get(value, `rop_shield.ropBlock.${answer}`)
        })
      );
    });

    _.set(
      newError,
      "rop_shield.ropBlock.ropQ2",
      validation.validateMandatory({
        field: {
          type: TEXT_SELECTION,
          mandatory: hasRop,
          disabled
        },
        value: _.get(value, "rop_shield.ropBlock.ropQ2")
      })
    );
    _.set(
      newError,
      "rop_shield.ropBlock.ropQ3",
      validation.validateMandatory({
        field: {
          type: TEXT_SELECTION,
          mandatory: hasRop,
          disabled
        },
        value: _.get(value, "rop_shield.ropBlock.ropQ3")
      })
    );
    _.set(
      newError,
      "rop_shield.ropBlock.ropQ1sub3",
      validation.validateMandatory({
        field: { type: TEXT_BOX, mandatory: hasRop, disabled },
        value: _.get(value, "rop_shield.ropBlock.ropQ1sub3")
      })
    );
  }
  return newError;
};

const validateBudgetData = ({ value, error, disabled }) => {
  const newError = _.cloneDeep(error);

  const showBudgetLess =
    _.get(value, "spCompare") > TOLERANCE_LEVEL.POSITIVE ||
    _.get(value, "rpCompare") > TOLERANCE_LEVEL.POSITIVE ||
    _.get(value, "cpfOaCompare") > TOLERANCE_LEVEL.POSITIVE ||
    _.get(value, "cpfSaCompare") > TOLERANCE_LEVEL.POSITIVE ||
    _.get(value, "srsCompare") > TOLERANCE_LEVEL.POSITIVE ||
    _.get(value, "cpfMsCompare") > TOLERANCE_LEVEL.POSITIVE;

  const showBudgetMore =
    _.get(value, "spCompare") < TOLERANCE_LEVEL.NEGATIVE ||
    _.get(value, "rpCompare") < TOLERANCE_LEVEL.NEGATIVE ||
    _.get(value, "cpfOaCompare") < TOLERANCE_LEVEL.NEGATIVE ||
    _.get(value, "cpfSaCompare") < TOLERANCE_LEVEL.NEGATIVE ||
    _.get(value, "srsCompare") < TOLERANCE_LEVEL.NEGATIVE ||
    _.get(value, "cpfMsCompare") < TOLERANCE_LEVEL.NEGATIVE;

  if (showBudgetMore) {
    _.set(
      newError,
      "budgetMoreChoice",
      validation.validateBudgetChoice({
        field: {
          type: TEXT_SELECTION,
          mandatory: true,
          disabled
        },
        value: _.get(value, "budgetMoreChoice")
      })
    );
    _.set(
      newError,
      "budgetMoreReason",
      validation.validateMandatory({
        field: { type: TEXT_BOX, mandatory: true, disabled },
        value: _.get(value, "budgetMoreReason")
      })
    );
  }

  if (showBudgetLess) {
    _.set(
      newError,
      "budgetLessChoice",
      validation.validateBudgetChoice({
        field: {
          type: TEXT_SELECTION,
          mandatory: true,
          disabled
        },
        value: _.get(value, "budgetLessChoice")
      })
    );
    _.set(
      newError,
      "budgetLessReason",
      validation.validateMandatory({
        field: { type: TEXT_BOX, mandatory: true, disabled },
        value: _.get(value, "budgetLessReason")
      })
    );
  }

  return newError;
};

/**
 * Saga functions
 */
export function* getRecommendation() {
  try {
    const choiceList = yield select(
      state => state[PRE_APPLICATION].recommendation.choiceList
    );

    const profile = yield select(state => state[CLIENT].profile);

    const callServer = yield select(state => state[CONFIG].callServer);

    const resp = yield call(callServer, {
      url: "clientChoice",
      data: {
        method: "post",
        action: "clientChoice",
        quotCheckedList: choiceList,
        clientId: profile.cid
      }
    });

    let completedStep = -1;
    if (resp.isRecommendationCompleted) {
      completedStep += 1;
      if (resp.isBudgetCompleted) {
        completedStep += 1;
        if (resp.isAcceptanceCompleted || resp.clientChoiceStep > 1) {
          completedStep += 1;
        }
      }
    }

    // prepare error object for validation
    const rError = {};
    const bError = {};
    _.forEach(resp.recommendation, (value, key) => {
      if (["chosenList", "notChosenList"].indexOf(key) < 0) {
        rError[key] = {};
        prepareErrorObject(value, rError[key]);
      }
    });
    prepareErrorObject(resp.budget, bError);

    const error = {
      recommendation: rError,
      budget: bError,
      acceptance: {}
    };

    // prepare component object
    const component = {
      completedStep,
      currentStep: resp.clientChoiceStep,
      completedSection: {
        recommendation: {},
        budget: resp.isBudgetCompleted
      },
      showDetails: false,
      disabled: resp.isReadOnly
    };
    _.forEach(resp.recommendation, (value, key) => {
      if (["chosenList", "notChosenList"].indexOf(key) < 0) {
        component.completedSection.recommendation[key] =
          resp.isRecommendationCompleted;
      }
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION,
      recommendation: resp.recommendation,
      budget: resp.budget,
      acceptance: resp.acceptance,
      error,
      component
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveRecommendation(action) {
  try {
    const profile = yield select(state => state[CLIENT].profile);
    const recommendation = yield select(
      state => state[RECOMMENDATION].recommendation
    );
    const budget = yield select(state => state[RECOMMENDATION].budget);
    const acceptance = yield select(state => state[RECOMMENDATION].acceptance);
    const component = yield select(state => state[RECOMMENDATION].component);
    const callServer = yield select(state => state[CONFIG].callServer);

    const toSaveRecommendation = _.cloneDeep(recommendation);
    _.forEach(toSaveRecommendation, (value, key) => {
      if (["chosenList", "notChosenList"].indexOf(key) < 0) {
        _.set(
          value,
          "extra.isCompleted",
          component.completedSection.recommendation[key]
        );
      }
    });

    const resp = yield call(callServer, {
      url: "clientChoice",
      data: {
        method: "post",
        action: "updateQuotationClientChoice",
        clientId: profile.cid,
        values: {
          recommendation: toSaveRecommendation,
          budget,
          acceptance
        },
        stepperIndex: action.step,
        isSaveOnly: action.isSaveOnly
      }
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION,
      recommendation: !_.isEmpty(resp.recommendation)
        ? resp.recommendation
        : recommendation,
      budget: !_.isEmpty(resp.budget) ? resp.budget : budget,
      acceptance: !_.isEmpty(resp.acceptance) ? resp.acceptance : acceptance
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* closeRecommendation() {
  try {
    // get state
    const component = yield select(state => state[RECOMMENDATION].component);

    yield* saveRecommendation({
      step: component.currentStep,
      isSaveOnly: true
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION
    });

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_RECOMMENDATION_COMPLETED,
      completed: component.completedStep === 2
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateRecommendationStep(action) {
  try {
    // get state
    const component = yield select(state => state[RECOMMENDATION].component);

    const isSaveOnly =
      component.completedStep > 1 ||
      action.newCurrentStep <= component.currentStep;

    const newComponent = _.cloneDeep(component);
    if (newComponent.completedStep === 1 && action.newCurrentStep > 1) {
      newComponent.completedStep = action.newCurrentStep;
    }
    newComponent.currentStep = action.newCurrentStep;

    yield* saveRecommendation({
      step: component.currentStep,
      isSaveOnly
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: newComponent
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateRecommendation(action) {
  try {
    const { init } = action;

    // get state
    const recommendation = yield select(
      state => state[RECOMMENDATION].recommendation
    );
    const error = yield select(state => state[RECOMMENDATION].error);
    const component = yield select(state => state[RECOMMENDATION].component);
    const { disabled } = component;

    // prepare new state
    const newError = _.cloneDeep(error);
    const newComponent = _.cloneDeep(component);

    _.forEach(recommendation, (value, quotId) => {
      if (["chosenList", "notChosenList"].indexOf(quotId) < 0) {
        let isCompleted = false;
        const isChosen = recommendation.chosenList.indexOf(quotId) >= 0;
        const errorByQuotId = _.get(error, `recommendation[${quotId}]`, {});
        const newErrorByQuotId = _.get(value, "extra.isShield")
          ? validateRecommendationShieldData({
              value,
              error: errorByQuotId,
              isChosen,
              disabled
            })
          : validateRecommendationNormalData({
              value,
              error: errorByQuotId,
              isChosen,
              disabled
            });
        newError.recommendation[quotId] = newErrorByQuotId;
        const hasError = checkDataHasError(newErrorByQuotId);
        newComponent.completedSection.recommendation[quotId] = !hasError;
        if (component.completedStep <= 0) {
          isCompleted =
            _
              .values(newComponent.completedSection.recommendation)
              .indexOf(false) < 0;
          newComponent.completedStep = isCompleted ? 0 : -1;
        }
      }
    });

    if (init) {
      newComponent.selectedQuotId = _.get(recommendation, "chosenList[0]", "");
    }

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
      error: newError
    });
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: newComponent
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateRecommendationData(action) {
  try {
    const { quotationId, newValue } = action;

    // get state
    const recommendation = yield select(
      state => state[RECOMMENDATION].recommendation
    );
    const error = yield select(state => state[RECOMMENDATION].error);
    const component = yield select(state => state[RECOMMENDATION].component);
    const isChosen = recommendation.chosenList.indexOf(quotationId) >= 0;
    const { disabled } = component;
    const errorByQuotId = _.get(error, `recommendation[${quotationId}]`, {});

    // prepare new state
    const newRecommendation = _.cloneDeep(recommendation);
    const newError = _.cloneDeep(error);
    const newComponent = _.cloneDeep(component);
    let newErrorByQuotId = {};
    let isCompleted = false;

    // perform validation
    if (_.get(recommendation, `[${quotationId}].extra.isShield`)) {
      newErrorByQuotId = validateRecommendationShieldData({
        value: newValue,
        error: errorByQuotId,
        isChosen,
        disabled
      });
    } else {
      newErrorByQuotId = validateRecommendationNormalData({
        value: newValue,
        error: errorByQuotId,
        isChosen,
        disabled
      });
    }

    newRecommendation[quotationId] = newValue;
    newError.recommendation[quotationId] = newErrorByQuotId;

    const hasError = checkDataHasError(newErrorByQuotId);
    newComponent.completedSection.recommendation[quotationId] = !hasError;
    isCompleted =
      _.values(newComponent.completedSection.recommendation).indexOf(false) < 0;

    if (isCompleted) {
      if (component.completedStep <= 0) {
        newComponent.completedStep = 0;
      }
    } else {
      newComponent.completedStep = -1;
    }

    // dispatch
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DATA,
      recommendation: newRecommendation
    });
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
      error: newError
    });
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: newComponent
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateBudget() {
  try {
    // get state
    const budget = yield select(state => state[RECOMMENDATION].budget);
    const error = yield select(state => state[RECOMMENDATION].error);
    const component = yield select(state => state[RECOMMENDATION].component);
    const { disabled } = component;

    // prepare new state
    const newError = _.cloneDeep(error);
    const newComponent = _.cloneDeep(component);
    const newBudgetError = validateBudgetData({
      value: budget,
      error: _.get(error, "budget", {}),
      disabled
    });

    newError.budget = newBudgetError;
    const hasError = checkDataHasError(newBudgetError);

    newComponent.completedSection.budget = !hasError;

    if (component.completedStep >= 0 && component.completedStep <= 1) {
      newComponent.completedStep = newComponent.completedSection.budget ? 1 : 0;
    }

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_BUDGET_ERROR,
      error: newError
    });
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: newComponent
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateBudgetData(action) {
  try {
    const { newValue } = action;

    // get state
    const error = yield select(state => state[RECOMMENDATION].error);
    const component = yield select(state => state[RECOMMENDATION].component);
    const { disabled } = component;

    // prepare new value
    const newError = _.cloneDeep(error);
    const newComponent = _.cloneDeep(component);
    const newBudgetError = validateBudgetData({
      value: newValue,
      error: _.get(error, "budget", {}),
      disabled
    });

    newError.budget = newBudgetError;
    const hasError = checkDataHasError(newBudgetError);

    newComponent.completedSection.budget = !hasError;

    if (component.completedStep >= 0 && component.completedStep <= 1) {
      newComponent.completedStep = newComponent.completedSection.budget ? 1 : 0;
    }

    // dispatch
    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_BUDGET_DATA,
      budget: newValue
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
      error: newError
    });

    yield put({
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: newComponent
    });
  } catch (e) {
    /* TODO need error handling */
  }
}
