import * as _ from "lodash";
import { select, put, call } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  CONFIG,
  OPTIONS_MAP,
  CLIENT,
  CLIENT_FORM,
  FNA,
  PRE_APPLICATION,
  AGENT,
  LOGIN
} from "../constants/REDUCER_TYPES";
import * as PDF_TYPE from "../constants/PDF_TYPE";
import { ERROR, OTHER } from "../constants/DIALOG_TYPES";
import { getEmailContent } from "../utilities/common";
import { profileMapForm } from "../utilities/dataMapping";
import { isClientFormHasError } from "../utilities/validation";
import {
  prepareProductList,
  getValidBundleId
} from "../utilities/preApplication";

/**
 * support functions
 */

const getPILabelByQuotType = quotType => {
  const result = {};
  if (quotType === "SHIELD") {
    result.label = "Policy Illustration";
    result.fileName = "policy_illustration.pdf";
  } else {
    result.label = "Policy Illustration Document(s)";
    result.fileName = "policy_illustration_document(s).pdf";
  }
  return result;
};

/**
 * Saga functions
 */
export function* getApplications(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const { bundleId } = action;

    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "getAppListView",
        profile,
        bundleId
      }
    });
    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST,
      productList: prepareProductList(resp.applicationsList),
      applicationsList: resp.applicationsList,
      pdaMembers: resp.pdaMembers,
      agentChannel: resp.agentChannel,
      recommendation: {
        completed: resp.clientChoiceFinished,
        choiceList: resp.quotCheckedList
      },
      newProductInvalidList: resp.productInvalidList
    });

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECTED_BUNDLE_ID,
      selectedBundleId: bundleId || getValidBundleId(profile)
    });
  } catch (e) {
    /* TODO need error handling */
  } finally {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}

export function* deleteApplications(action) {
  try {
    const { callback } = action;
    const callServer = yield select(state => state[CONFIG].callServer);
    const applicationsList = yield select(
      state => state[PRE_APPLICATION].applicationsList
    );
    const selectedIdList = yield select(
      state => state[PRE_APPLICATION].component.selectedIdList
    );

    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "deleteApplications",
        deleteItemsSelectedArray: selectedIdList,
        applicationsList
      }
    });

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].DELETE_APPLICATION_LIST,
      productList: prepareProductList(resp.result),
      applicationsList: resp.result,
      recommendation: {
        completed: false,
        choiceList: resp.quotCheckedList
      }
    });

    if (_.isFunction(callback)) {
      yield call(callback);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateUiSelectedList(action) {
  try {
    const selectedIdList = yield select(
      state => state[PRE_APPLICATION].component.selectedIdList
    );

    const { id, isSelected } = action.changeData;

    const newSelectedIdList = _.cloneDeep(selectedIdList);
    const selectedIdIndex = _.findIndex(
      selectedIdList,
      selectedId => selectedId === id
    );
    if (isSelected && selectedIdIndex < 0) {
      newSelectedIdList.push(id);
    } else if (!isSelected && selectedIdIndex >= 0) {
      newSelectedIdList.splice(selectedIdIndex, 1);
    }

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECTED_LIST,
      selectedIdList: newSelectedIdList
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateRecommendChoiceList(action) {
  try {
    const choiceList = yield select(
      state => state[PRE_APPLICATION].recommendation.choiceList
    );
    let completed = yield select(
      state => state[PRE_APPLICATION].recommendation.completed
    );

    const { id, checked } = action.choiceData;

    const newChoiceList = _.cloneDeep(choiceList);
    const choiceIndex = _.findIndex(newChoiceList, choice => choice.id === id);
    if (choiceIndex >= 0) {
      const hasChange = choiceList[choiceIndex].checked !== checked;
      if (hasChange) {
        completed = false;
      }
      newChoiceList[choiceIndex].checked = checked;
    }

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_RECOMMENDATION_CHOICE_LIST,
      choiceList: newChoiceList,
      completed
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* openEmailDialog(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const agent = yield select(state => state[AGENT]);
    const preApplication = yield select(state => state[PRE_APPLICATION]);
    const selectedApps = preApplication.component.selectedIdList;
    const appList = preApplication.applicationsList;

    const proposalsMappingData = {
      bi: PDF_TYPE.BI,
      prodSummary: PDF_TYPE.PROD_SUMMARY,
      FPXProdSummary: PDF_TYPE.FPX_PROD_SUMMARY,
      shield_product_summary: PDF_TYPE.SHIELD_PRODUCT_SUMMARY,
      phs: PDF_TYPE.PHS,
      fib: PDF_TYPE.FIB,
      fna: PDF_TYPE.FNA
    };

    const getAttKeysResp = yield call(callServer, {
      url: `application`,
      data: {
        action: "getAppSumEmailAttKeys"
      }
    });

    const getEmailResp = yield call(callServer, {
      url: `application`,
      data: {
        action: "getEmailTemplate"
      }
    });

    if (getAttKeysResp && getEmailResp && getEmailResp.success) {
      const {
        agentContent,
        clientContent,
        agentSubject,
        clientSubject,
        embedImgs
      } = getEmailResp;

      const params = {
        agentName: agent.name,
        agentContactNum: agent.mobile,
        clientName: profile.fullName
      };

      const emails = [];

      const { attKeysMap } = getAttKeysResp;

      const selectedAttachments = {};
      const quotIds = {};

      selectedApps.forEach(appKey => {
        const { baseProductCode, baseProductName, quotationDocId, id } = _.find(
          appList,
          app => app.id === appKey
        );
        const attKeysReMap = {};
        attKeysMap.forEach(item => {
          if (item.productCode.includes(baseProductCode)) {
            item.attKeys.forEach(attKey => {
              attKeysReMap[attKey.id] = true;
            });
          }
        });
        selectedAttachments[appKey] = attKeysReMap;
        quotIds[appKey] = quotationDocId || id;

        const appListViewObj = _.find(appList, obj => obj.id === appKey);
        const attachments = [];
        const attachmentsDetail = [];
        Object.keys(selectedAttachments[appKey]).forEach(attKey => {
          if (selectedAttachments[appKey][attKey] === true) {
            const title =
              attKey === "bi"
                ? _.get(getPILabelByQuotType(appListViewObj.quotType), "label")
                : proposalsMappingData[attKey];

            attachments.push(attKey);
            attachmentsDetail.push({
              id: attKey,
              title
            });
          }
        });
        emails.push({
          id: `agent_${appKey}`,
          agentCode: agent.agentCode,
          dob: "",
          receiverType: "agent",
          to: [agent.email],
          title: agentSubject,
          content: getEmailContent(
            agentContent,
            params,
            '<img src="cid:axaLogo">'
          ),
          attachmentIds: attachments,
          attachmentsDetail,
          embedImgs,
          quotId: quotIds[appKey],
          appId: appKey,
          baseProductName: baseProductName.en
        });
        emails.push({
          id: `client_${appKey}`,
          agentCode: "",
          dob: profile.dob,
          receiverType: "client",
          to: [profile.email],
          title: clientSubject,
          content: getEmailContent(
            clientContent,
            params,
            '<img src="cid:axaLogo">'
          ),
          attachmentIds: attachments.slice(),
          attachmentsDetail,
          embedImgs,
          quotId: quotIds[appKey],
          appId: appKey,
          baseProductName: baseProductName.en
        });
      });

      const options = _.map(emails, (email, index) =>
        email.attachmentsDetail.map(attachment => ({
          key: attachment.id,
          id: email.id,
          index,
          text: attachment.title,
          isSelected: true
        }))
      );

      yield put({
        type: ACTION_TYPES[PRE_APPLICATION].OPEN_EMAIL,
        attKeysMap: getAttKeysResp.success ? attKeysMap : [],
        emails,
        options
      });
    }

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* sendEmail(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const cid = yield select(state => state[CLIENT].profile.cid);
    const emails = yield select(
      state => state[PRE_APPLICATION].preApplicationEmail.emails
    );
    const options = yield select(
      state => state[PRE_APPLICATION].preApplicationEmail.options
    );
    const authToken = yield select(state => state[LOGIN].authToken);
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);
    const newEmails = _.cloneDeep(emails);

    _.forEach(newEmails, (email, index) => {
      newEmails[index].content += "<img src='cid:axaLogo'>";

      const checkboxOptions = options.find(
        cbOptions => cbOptions.length > 0 && cbOptions[0].id === email.id
      );
      _.forEach(checkboxOptions, option => {
        if (!option.isSelected) {
          const idIndex = email.attachmentIds.findIndex(
            id => id === option.key
          );
          if (idIndex >= 0) {
            newEmails[index].attachmentIds.splice(idIndex, 1);
          }
        }
      });
    });
    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "sendApplicationEmail",
        emails: newEmails,
        cid,
        authToken,
        webServiceUrl
      }
    });

    if (response && response.success && _.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO: need error handling
  }
}

export function* loadClientProfileAndInitValidate(action) {
  try {
    const { cid, profile, configData, callback } = action;
    const profileData = yield select(state => state[CLIENT].profile);
    const optionsMapData = yield select(state => state[OPTIONS_MAP]);

    const loadClientData = Object.assign(
      {},
      profileMapForm(_.cloneDeep(profile)),
      {
        type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT
      }
    );
    yield put(loadClientData);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
      configData
    });

    const clientForm = yield select(state => state[CLIENT_FORM]);

    yield put({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE,
      configData: clientForm.config,
      isPDA: clientForm.config.isPDA,
      isFNA: clientForm.config.isFNA,
      isAPP: clientForm.config.isAPP,
      relationshipData: clientForm.relationship,
      genderData: clientForm.gender,
      profileData,
      incomeData: clientForm.income,
      maritalData: clientForm.maritalStatus,
      educationData: clientForm.education,
      isFamilyMemberData: clientForm.config.isFamilyMember,
      isProposerMissingData: clientForm.config.isProposerMissing,
      isInsuredMissingData: clientForm.config.isInsuredMissing,
      relationshipOtherData: clientForm.relationshipOther,
      givenNameData: clientForm.givenName,
      nameData: clientForm.givenName,
      titleData: clientForm.title,
      isCreateData: clientForm.config.isCreate,
      singaporePRStatusData: clientForm.singaporePRStatus,
      nationalityData: clientForm.nationality,
      cityOfResidenceData: clientForm.cityOfResidence,
      countryOfResidenceData: clientForm.countryOfResidence,
      optionsMapData,
      isApplicationData: clientForm.config.isApplication,
      otherCityOfResidenceData: clientForm.otherCityOfResidence,
      IDData: clientForm.ID,
      IDDocumentTypeData: clientForm.IDDocumentType,
      IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
      employStatusData: clientForm.employStatus,
      employStatusOtherData: clientForm.employStatusOther,
      industryData: clientForm.industry,
      occupationData: clientForm.occupation,
      birthdayData: clientForm.birthday,
      smokingStatusData: clientForm.smokingStatus,
      occupationOtherData: clientForm.occupationOther,
      languageData: clientForm.language,
      languageOtherData: clientForm.languageOther,
      emailData: clientForm.email,
      clientFormCid: clientForm.profileBackUp.cid,
      prefixAData: clientForm.prefixA,
      mobileNoAData: clientForm.mobileNoA,
      typeOfPassData: clientForm.typeOfPass,
      typeOfPassOtherData: clientForm.typeOfPassOther
    });

    const validatedClientForm = yield select(state => state[CLIENT_FORM]);
    const hasErrorList = yield select(
      state => state[PRE_APPLICATION].multiClientProfile.hasErrorList
    );
    const newHasErrorList = _.cloneDeep(hasErrorList);

    const { hasError, errorFields } = isClientFormHasError(validatedClientForm);
    const clientHasErrorIndex = newHasErrorList.findIndex(
      clientHasError => clientHasError.cid === cid
    );
    if (clientHasErrorIndex >= 0) {
      newHasErrorList[clientHasErrorIndex].hasError = hasError;
    } else if (hasError) {
      newHasErrorList.push({ cid, hasError, errorFields });
    }

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR,
      newHasErrorList
    });

    if (_.isFunction(callback)) {
      yield call(callback);
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* checkBeforeApply(action) {
  try {
    const { pCid, iCids, callback } = action;
    const agentChannel = yield select(
      state => state[PRE_APPLICATION].agentChannel
    );
    const profile = yield select(state => state[CLIENT].profile);
    const dependantProfiles = yield select(
      state => state[CLIENT].dependantProfiles
    );
    const isFnaInvalid = yield select(state => state[FNA].isFnaInvalid);
    let dialogType = "";

    if (isFnaInvalid) {
      dialogType = ERROR;
    } else {
      // check proposer
      yield loadClientProfileAndInitValidate({
        cid: pCid,
        profile,
        configData: {
          isCreate: false,
          isFamilyMember: false,
          isProposerMissing: true,
          isInsuredMissing: false,
          isFNA: agentChannel !== "FA",
          isAPP: true
        }
      });

      yield put({
        type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
      });

      // check insured

      for (let i = 0; i < iCids.length; i += 1) {
        const iCid = iCids[i];
        if (pCid !== iCid) {
          const dependantProfile = dependantProfiles[iCid];

          if (dependantProfile) {
            yield loadClientProfileAndInitValidate({
              cid: iCid,
              profile: dependantProfile,
              configData: {
                isCreate: false,
                isFamilyMember: false,
                isProposerMissing: false,
                isInsuredMissing: true,
                isFNA: false,
                isAPP: true
              }
            });

            yield put({
              type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
            });
          }
        }
      }

      const hasErrorList = yield select(
        state => state[PRE_APPLICATION].multiClientProfile.hasErrorList
      );
      const showProfileDialog = hasErrorList.some(
        clientHasError => clientHasError.hasError
      );

      if (showProfileDialog) {
        dialogType = OTHER;
      }
    }

    if (_.isFunction(callback)) {
      yield call(callback, dialogType);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* confirmApplicationPaid(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const selectedBundleId = yield select(
      state => state[PRE_APPLICATION].component.selectedBundleId
    );

    const { isShield, applicationId } = action;
    const response = isShield
      ? yield call(callServer, {
          url: `shieldApplication`,
          data: {
            action: "confirmAppPaid",
            appId: applicationId
          }
        })
      : yield call(callServer, {
          url: `application`,
          data: {
            action: "confirmAppPaid",
            appId: applicationId
          }
        });

    if (response && response.success) {
      yield put({
        type: ACTION_TYPES[PRE_APPLICATION].SAGA_GET_APPLICATIONS,
        bundleId: selectedBundleId
      });
    }
  } catch (e) {
    // TODO: need error handling
  }
}
