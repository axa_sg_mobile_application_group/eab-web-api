import { call, put, select } from "redux-saga/effects";
import * as _ from "lodash";
import * as validation from "../utilities/validation";
import { checkPaymentHasError } from "../utilities/application";
import { TEXT_FIELD } from "../constants/FIELD_TYPES";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import EAPP from "../constants/EAPP";
import {
  CONFIG,
  PAYMENT_AND_SUBMISSION,
  APPLICATION,
  PAYMENT,
  LOGIN
} from "../constants/REDUCER_TYPES";
import * as supportingDocument from "./supportingDocument";

const {
  INITPAYMETHOD,
  TELETRANSFTER,
  TTREMITTINGBANK,
  TTDOR,
  TTREMARKS,
  SRSBLOCK
} = EAPP.fieldId[PAYMENT];

const { SRS } = EAPP.fieldValue[PAYMENT];

export const triggerExtraValidation = ({ dataObj, errorObj }) => {
  const fieldTriggerList = [
    {
      id: INITPAYMETHOD,
      value: TELETRANSFTER,
      extraCheckPathList: [TTREMITTINGBANK, TTDOR, TTREMARKS]
    }
  ];
  fieldTriggerList.forEach(fieldTrigger => {
    const { extraCheckPathList, id, value } = fieldTrigger;
    extraCheckPathList.forEach(path => {
      _.set(
        errorObj,
        path,
        validation.validateMandatory({
          field: {
            type: TEXT_FIELD,
            mandatory: dataObj[id] === value,
            disabled: false
          },
          value: _.get(dataObj, path)
        })
      );
    });
  });
};

export const srsBlockValidation = ({ dataObj, errorObj }) => {
  const srsBlock = dataObj[SRSBLOCK];
  const errorObjClear = dataGroup => {
    dataGroup.forEach(key => {
      if (errorObj[key]) {
        errorObj[key].hasError = false;
      }
    });
  };
  const groupList = {
    dateGroup: ["srsFundReqDate", "srsFundProcessDate"],
    bankGroup: ["srsBankNamePicker", "srsInvmAcctNo"],
    declareGroup: ["srsAcctDeclare"]
  };

  Object.keys(groupList).forEach(group => {
    groupList[group].forEach(key => {
      if (errorObj[key]) {
        errorObj[key].hasError = errorObj[key].message.ENGLISH !== "";
      }
    });
  });

  if (srsBlock.srsIsHaveSrsAcct) {
    if (srsBlock.srsIsHaveSrsAcct === "Y") {
      if (srsBlock.srsFundReqDate !== "Y") {
        // Include "N" and ""
        errorObjClear(groupList.dateGroup);
      }
      errorObjClear(groupList.declareGroup);
    } else if (srsBlock.srsIsHaveSrsAcct === "N") {
      errorObjClear(groupList.bankGroup);
      errorObjClear(groupList.dateGroup);
    }
  } else {
    errorObj.srsIsHaveSrsAcct = validation.validateMandatory({
      field: {
        type: TEXT_FIELD,
        mandatory: true,
        disabled: false
      },
      value: ""
    });
  }
};

export const initSRSBlock = values => {
  const srsBlockKey = [
    "srsIsHaveSrsAcct",
    "srsBankNamePicker",
    "srsInvmAcctNo",
    "srsFundReqDate",
    "srsFundProcessDate",
    "srsAcctDeclare"
  ];
  srsBlockKey.forEach(key => {
    if (
      !_.isInteger(values[SRSBLOCK][key]) &&
      (values[SRSBLOCK][key] || "").toString().length === 0
    ) {
      values[SRSBLOCK][key] = "";
    }
  });
};

export const initCPFBlock = (values, blockName) => {
  const blockKey = [
    `${blockName}IsHaveCpfAcct`,
    `${blockName}BankNamePicker`,
    `${blockName}InvmAcctNo`,
    `${blockName}FundReqDate`,
    `${blockName}FundProcessDate`,
    `${blockName}AcctDeclare`
  ];
  blockKey.forEach(key => {
    if (
      !_.isInteger(values[`${blockName}Block`][key]) &&
      (values[`${blockName}Block`][key] || "").toString().length === 0
    ) {
      values[`${blockName}Block`][key] = "";
    }
  });
};

export const cpfBlockValidation = ({ dataObj, blockName, errorObj }) => {
  const block = dataObj[`${blockName}Block`];
  const errorObjClear = dataGroup => {
    dataGroup.forEach(key => {
      if (errorObj[key]) {
        errorObj[key].hasError = false;
      }
    });
  };
  const groupList = {
    dateGroup: [`${blockName}FundReqDate`, `${blockName}FundProcessDate`],
    bankGroup: [`${blockName}BankNamePicker`, `${blockName}InvmAcctNo`],
    declareGroup: [`${blockName}AcctDeclare`]
  };

  Object.keys(groupList).forEach(group => {
    groupList[group].forEach(key => {
      if (errorObj[key]) {
        errorObj[key].hasError = errorObj[key].message.ENGLISH !== "";
      }
    });
  });

  if (blockName === "cpfissa") {
    if (block[`${blockName}FundReqDate`] !== "Y") {
      // Include "N" and ""
      errorObjClear(groupList.dateGroup);
    }
  } else if (block[`${blockName}IsHaveCpfAcct`]) {
    if (block[`${blockName}IsHaveCpfAcct`] === "Y") {
      if (block[`${blockName}FundReqDate`] !== "Y") {
        // Include "N" and ""
        errorObjClear(groupList.dateGroup);
      }
      errorObjClear(groupList.declareGroup);
    } else if (block[`${blockName}IsHaveCpfAcct`] === "N") {
      errorObjClear(groupList.bankGroup);
      errorObjClear(groupList.dateGroup);
    }
  } else {
    errorObj[`${blockName}IsHaveCpfAcct`] = validation.validateMandatory({
      field: {
        type: TEXT_FIELD,
        mandatory: true,
        disabled: false
      },
      value: ""
    });
  }
};

export const initValidatePayment = ({ dataObj, errorObj }) => {
  const validateList = [TTREMITTINGBANK, TTDOR, TTREMARKS];
  if (!dataObj.initPayMethod) {
    dataObj.initPayMethod = "";
  }

  Object.entries(dataObj).forEach(([key, value]) => {
    if (key === INITPAYMETHOD) {
      _.set(
        errorObj,
        key,
        validation.validateApplicationInitPaymentMethod(dataObj)
      );
    } else if (validateList.indexOf(key) !== -1) {
      _.set(
        errorObj,
        key,
        validation.validateMandatory({
          field: { type: TEXT_FIELD, mandatory: true, disabled: false },
          value
        })
      );
    } else if (key === SRSBLOCK) {
      Object.keys(value).forEach(srsKey => {
        if (srsKey !== "idCardNo") {
          if (srsKey === "srsInvmAcctNo") {
            _.set(
              errorObj,
              srsKey,
              validation.validateBankNumber({
                field: {
                  type: TEXT_FIELD,
                  mandatory: true,
                  disabled: false,
                  bankName: value.srsBankNamePicker
                },
                value: value[srsKey]
              })
            );
          } else {
            _.set(
              errorObj,
              srsKey,
              validation.validateMandatory({
                field: { type: TEXT_FIELD, mandatory: true, disabled: false },
                value: value[srsKey]
              })
            );
          }
        }
      });
    } else if (key === "cpfisoaBlock") {
      Object.keys(value).forEach(cpfKey => {
        if (cpfKey !== "idCardNo") {
          if (cpfKey === `${dataObj.initPayMethod}InvmAcctNo`) {
            _.set(
              errorObj,
              cpfKey,
              validation.validateBankNumber({
                field: {
                  type: TEXT_FIELD,
                  mandatory: true,
                  disabled: false,
                  bankName: value[`${dataObj.initPayMethod}BankNamePicker`],
                  cpf: true
                },
                value: value[cpfKey]
              })
            );
          } else {
            _.set(
              errorObj,
              cpfKey,
              validation.validateMandatory({
                field: { type: TEXT_FIELD, mandatory: true, disabled: false },
                value: value[cpfKey]
              })
            );
          }
        }
      });
    } else if (key === "cpfissaBlock") {
      Object.keys(value).forEach(cpfKey => {
        if (
          cpfKey === `${dataObj.initPayMethod}FundReqDate` &&
          value[`${dataObj.initPayMethod}FundReqDate`] === "Y"
        ) {
          _.set(
            errorObj,
            `${dataObj.initPayMethod}FundProcessDate`,
            validation.validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: value[`${dataObj.initPayMethod}FundProcessDate`]
            })
          );
        }
      });
    }
  });

  if (dataObj.initPayMethod === SRS) {
    srsBlockValidation({
      dataObj,
      errorObj
    });
  } else if (
    dataObj.initPayMethod === "cpfissa" ||
    dataObj.initPayMethod === "cpfisoa"
  ) {
    cpfBlockValidation({
      dataObj,
      blockName: dataObj.initPayMethod,
      errorObj
    });
  }
  triggerExtraValidation({ dataObj, errorObj });
};

export function* getSubmission(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { docId } = action;
    const submissionResponse = yield call(callServer, {
      url: `application`,
      data: {
        action: "getSubmissionTemplateValues",
        docId
      }
    });
    if (submissionResponse.success) {
      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
        newSubmission: submissionResponse
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getPayment(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const errorObj = yield select(state => state[PAYMENT_AND_SUBMISSION].error);
    const { docId, callback } = action;
    const paymentResponse = yield call(callServer, {
      url: `application`,
      data: {
        action: "getPaymentTemplateValues",
        docId
      }
    });
    if (paymentResponse.success) {
      const newApplication = yield call(callServer, {
        url: "application",
        data: {
          method: "post",
          action: "goApplication",
          id: action.applicationId
        }
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newApplication
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_STATUS,
        applicationStatus: {
          isMandDocsAllUploaded: paymentResponse.isMandDocsAllUploaded,
          isSubmitted: paymentResponse.isSubmitted,
          success: paymentResponse.success
        }
      });

      if (paymentResponse.values.initPayMethod === SRS) {
        initSRSBlock(paymentResponse.values);
      }

      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
        newPayment: paymentResponse
      });

      initValidatePayment({ dataObj: paymentResponse.values, errorObj });
      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
        errorObj
      });

      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
        newCurrentStep: 2,
        newCompletedStep: checkPaymentHasError({ application, error: errorObj })
          ? 1
          : 2
      });

      yield call(supportingDocument.getSupportingDocument, {
        appId: application.id,
        appStatus: EAPP.appStatus.APPLYING
      });
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePaymentMethods() {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const callServer = yield select(state => state[CONFIG].callServer);
    const errorObj = yield select(state => state[PAYMENT_AND_SUBMISSION].error);
    const payment = yield select(
      state => state[PAYMENT_AND_SUBMISSION].payment
    );
    if (payment.values[INITPAYMETHOD] === TELETRANSFTER) {
      if (
        !payment.values[TTREMITTINGBANK] &&
        payment.values[TTREMITTINGBANK] !== 0
      ) {
        payment.values[TTREMITTINGBANK] = "";
      } else if (!payment.values[TTDOR] && payment.values[TTDOR] !== 0) {
        payment.values[TTDOR] = "";
      } else if (
        !payment.values[TTREMARKS] &&
        payment.values[TTREMARKS] !== 0
      ) {
        payment.values[TTREMARKS] = "";
      }
    } else if (payment.values[INITPAYMETHOD] === SRS) {
      initSRSBlock(payment.values);

      srsBlockValidation({
        dataObj: payment.values,
        errorObj
      });
      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
        errorObj
      });
    } else if (
      payment.values[INITPAYMETHOD] === "cpfissa" ||
      payment.values[INITPAYMETHOD] === "cpfisoa"
    ) {
      initCPFBlock(payment.values, payment.values[INITPAYMETHOD]);

      cpfBlockValidation({
        dataObj: payment.values,
        blockName: payment.values[INITPAYMETHOD],
        errorObj
      });
      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
        errorObj
      });
    } else {
      // if payment method !== "teleTransfter", then clear all values under teleTransfter option
      payment.values[TTREMITTINGBANK] = "";
      payment.values[TTDOR] = "";
      payment.values[TTREMARKS] = "";
    }
    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "updatePaymentMethods",
        docId: application.id,
        values: payment.values,
        stepperIndex: 2,
        updateStepper: true
      }
    });
    if (response.success) {
      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newApplication: response.application
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
        newCurrentStep: response.application.appStep,
        newCompletedStep: response.application.appStep
      });
      yield call(supportingDocument.getSupportingDocument, {
        appId: application.id,
        appStatus: EAPP.appStatus.APPLYING
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePaymentMethodsShield() {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const callServer = yield select(state => state[CONFIG].callServer);
    const errorObj = yield select(state => state[PAYMENT_AND_SUBMISSION].error);
    const payment = yield select(
      state => state[PAYMENT_AND_SUBMISSION].payment
    );
    let update = true;
    if (
      payment[INITPAYMETHOD] === TELETRANSFTER &&
      (payment[TTREMITTINGBANK] === "" ||
        payment[TTDOR] === "" ||
        payment[TTREMARKS] === "")
    ) {
      update = false;
    }
    if (update) {
      const response = yield call(callServer, {
        url: "shieldApplication",
        data: {
          method: "post",
          action: "savePaymentMethods",
          appId: application.id,
          changedValues: payment
        }
      });
      if (response.success) {
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
          newPayment: response.application.payment
        });
        initValidatePayment({
          dataObj: response.application.payment,
          errorObj
        });
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION]
              .VALIDATE_PAYMENT_AND_SUBMISSION,
          errorObj
        });
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePayment(action) {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const payment = yield select(
      state => state[PAYMENT_AND_SUBMISSION].payment
    );
    const errorObj = yield select(state => state[PAYMENT_AND_SUBMISSION].error);
    const newErrorObj = _.cloneDeep(errorObj);
    const { path, newValue, validateObj, isShield, callback } = action;
    const newPayment = _.cloneDeep(payment);
    _.set(newPayment, path, newValue);
    yield put({
      type: ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
      newPayment
    });
    if (validateObj) {
      if (validateObj.mandatory) {
        if (path.includes("srsInvmAcctNo")) {
          _.set(
            newErrorObj,
            validateObj.field,
            validation.validateBankNumber({
              field: {
                type: TEXT_FIELD,
                mandatory: true,
                disabled: false,
                bankName: validateObj.bankName
              },
              value: validateObj.value
            })
          );
        } else if (
          path.includes("cpfissaInvmAcctNo") ||
          path.includes("cpfisoaInvmAcctNo")
        ) {
          _.set(
            newErrorObj,
            validateObj.field,
            validation.validateBankNumber({
              field: {
                type: TEXT_FIELD,
                mandatory: true,
                disabled: false,
                bankName: validateObj.bankName,
                cpf: true
              },
              value: validateObj.value
            })
          );
        } else {
          _.set(
            newErrorObj,
            validateObj.field,
            validation.validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: validateObj.value
            })
          );
        }

        if (!isShield && payment.values.initPayMethod === SRS) {
          srsBlockValidation({
            dataObj: newPayment.values,
            errorObj: newErrorObj
          });
        } else if (
          !isShield &&
          (payment.values.initPayMethod === "cpfissa" ||
            payment.values.initPayMethod === "cpfisoa")
        ) {
          cpfBlockValidation({
            dataObj: newPayment.values,
            blockName: payment.values.initPayMethod,
            errorObj: newErrorObj
          });
        } else {
          triggerExtraValidation({
            dataObj: isShield ? newPayment : newPayment.values,
            errorObj: newErrorObj
          });
        }
      }

      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
        newCurrentStep: 2,
        newCompletedStep: checkPaymentHasError({ application, error: errorObj })
          ? 1
          : 2
      });
    }
    _.set(
      newErrorObj,
      "initPayMethod",
      validation.validateApplicationInitPaymentMethod(
        isShield ? newPayment : newPayment.values
      )
    );
    yield put({
      type:
        ACTION_TYPES[PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
      errorObj: Object.assign({}, newErrorObj)
    });
    if (isShield) {
      yield call(updatePaymentMethodsShield);
    } else {
      yield call(updatePaymentMethods);
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* submitPaymentAndSubmission(action) {
  try {
    const currentApplication = yield select(
      state => state[APPLICATION].application
    );
    const callServer = yield select(state => state[CONFIG].callServer);
    const {
      application,
      policyDocument,
      appPdf,
      bundle,
      isShield,
      callback
    } = action;
    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "applicationSubmit",
        isShield,
        application,
        policyDocument,
        appPdf,
        bundle
      }
    });
    if (response.success) {
      if (isShield) {
        yield put({
          type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
          newApplication: application.find(
            value => value.id === currentApplication.id
          )
        });
      } else {
        yield call(getSubmission, { docId: application.id });
      }
      if (_.isFunction(callback)) {
        callback();
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveSubmissionShield() {
  try {
    const application = yield select(state => state[APPLICATION]);
    const callServer = yield select(state => state[CONFIG].callServer);
    const response = yield call(callServer, {
      url: "shieldApplication",
      data: {
        method: "post",
        action: "saveSubmissionValues",
        appId: application.application.id,
        changedValues: application
      }
    });
    if (response.success) {
      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newApplication: response.application
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}
/**
 * @description this function just used for trigger EASE API to update
 *   application payment status. all change should get by data sync, so just
 *   call it and DO NOT handle anything.
 * */
export function* refreshPaymentStatus(action) {
  const { callback, error } = action;
  try {
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);
    const authToken = yield select(state => state[LOGIN].authToken);
    const callServer = yield select(state => state[CONFIG].callServer);
    const docId = yield select(state => state[APPLICATION].application.id);

    const response = yield call(callServer, {
      url: `/payment/status`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${authToken}`
      },
      data: {
        docId
      },
      webServiceUrl
    });

    if (response.data.status === "success" && response.data.result.isSuccess) {
      if (_.isFunction(callback)) {
        callback();
      }
    } else if (_.isFunction(error)) {
      error("Can not get payment status");
    }
  } catch (e) {
    if (_.isFunction(error)) {
      error("Get payment status fail");
    }
  }
}

export function* beforeUpdatePaymentStatus(action) {
  const { newPayment } = action;
  if (newPayment) {
    const application = yield select(state => state[APPLICATION].application);
    const { quotation } = application;
    if (quotation && quotation.quotType === "SHIELD") {
      yield put({
        type: ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS,
        newStatus: newPayment
      });
    } else {
      yield put({
        type: ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS,
        newStatus: newPayment.values
      });
    }
  }
}

export function* getPaymentURL(action) {
  try {
    const { callback, alertFunction, prepareDataSync } = action;
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);
    const authToken = yield select(state => state[LOGIN].authToken);
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const payment = yield select(
      state => state[PAYMENT_AND_SUBMISSION].payment
    );
    const { id: docId, quotation } = application;
    const isShield = quotation.quotType === "SHIELD";
    const values = isShield ? payment : payment.values;

    const response = yield call(callServer, {
      url: "application",
      data: {
        action: "getPaymentUrl",
        docId,
        values,
        isShield,
        authToken,
        webServiceUrl
      }
    });

    if (
      response &&
      response.success &&
      response.url &&
      response.url !== "undefined"
    ) {
      callback(response.url);
    } else {
      switch (response.error.response.status) {
        case 409:
          // already have payment data
          prepareDataSync();
          break;
        default:
          alertFunction();
          break;
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

// TODO
function dataSync({
  api,
  environment,
  agentCode,
  loginId,
  authToken,
  isLeftMenu,
  isBackground,
  sessionCookieSyncGatewaySession,
  sessionCookieExpiryDate,
  sessionCookiePath,
  syncDocumentIds
}) {
  return new Promise(resolve => {
    api(
      environment,
      agentCode,
      loginId,
      `Bearer ${authToken}`,
      isLeftMenu,
      isBackground,
      sessionCookieSyncGatewaySession,
      sessionCookieExpiryDate,
      sessionCookiePath,
      syncDocumentIds,
      response => {
        console.log("dataSync --> call back function");
        console.log(response);
        resolve(true);
      }
    );
  });
}

export function* eappDataSync(action) {
  try {
    const authToken = yield select(state => state.login.authToken);
    const agentCode = yield select(state => state.agent.agentCode);
    const loginId = yield select(state => state.login.profileId);
    const environment = yield select(state => state.login.environment);
    const syncDocumentIds = yield select(state => state.login.syncDocumentIds);
    const { api, callback } = action;
    const isLeftMenu = false;
    const isBackground = false;
    const sessionCookieSyncGatewaySession = yield select(
      state => state.login.sessionCookieSyncGatewaySession
    );
    const sessionCookieExpiryDate = yield select(
      state => state.login.sessionCookieExpiryDate
    );
    const sessionCookiePath = yield select(
      state => state.login.sessionCookiePath
    );
    const dataSyncPromise = yield call(dataSync, {
      api,
      environment,
      agentCode,
      loginId,
      authToken,
      isLeftMenu,
      isBackground,
      sessionCookieSyncGatewaySession,
      sessionCookieExpiryDate,
      sessionCookiePath,
      syncDocumentIds
    });
    Promise.all([dataSyncPromise]).then(dataSyncStatus => {
      if (_.isFunction(callback)) {
        callback(dataSyncStatus);
      }
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* callApiSubmitApplication(action) {
  try {
    const { docId, lang, callback, api } = action;
    const authToken = yield select(state => state.login.authToken);
    const environment = yield select(state => state.login.environment);
    // keep
    // const authToken =
    // "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vbWFhbS1kZXYuYXhhLmNvbS9kZXYvandrcz9raWQ9NjBEREE4Rjc2OUMxRTA0NTZBNkZDNjc0QUQ3MDI3RDM5ODc5MkQwRiIsImtpZCI6IjYwRERBOEY3NjlDMUUwNDU2QTZGQzY3NEFENzAyN0QzOTg3OTJEMEYiLCJ4NXUiOiJodHRwczovL21hYW0tZGV2LmF4YS5jb20vZGV2L3g1MDk_eDV0PTYwRERBOEY3NjlDMUUwNDU2QTZGQzY3NEFENzAyN0QzOTg3OTJEMEYiLCJ4NXQiOiI2MEREQThGNzY5QzFFMDQ1NkE2RkM2NzRBRDcwMjdEMzk4NzkyRDBGIn0.ew0KCSJzdWIiOiB7InZhbHVlIjogIkFHMDEzODdTRlNNIn0sDQoJImF1ZCI6ICI0M2RkNzViYy0zY2FlLTQyODEtOTA2Yi1mZTA0MDRkZGFiNzUiLA0KCSJleHAiOiAxNTM4MDM5NjY0LA0KCSJpYXQiOiAxNTM4MDM2MDY0LA0KCSJuYmYiOiAxNTM4MDM2MDU0LA0KCSJpc3MiOiAiaHR0cHM6Ly9tYWFtLWRldi5heGEuY29tIiwNCgkiYW1yIjogIiIsDQoJImN1c3RvbURhdGEiOiB7InVpZCI6IkFHMDEzODdTRlNNIiwicmVzb3VyY2Vfb3duZXIiOiJBRzAxMzg3U0ZTTSIsIm1haWwiOiJhZzAxMzg3c2ZzbUB0ZXN0LmNvbSIsImdpdmVuTmFtZSI6IkFORyIsInNuIjoiQ1lOVEhJQSJ9LA0KCSJzY29wZSI6ICJheGEtc2ctbGlmZWFwaSIsDQoJIm5vbmNlIjogIjhhMzQzNWNlLWM3NzYtNDBjMS1hODBjLWYwODMzZjJmOGY1MiINCn0.IFXAD9yasEKAl8WSVLLL77-4QzlilnI5Ts0KPKTLwWI_IweZWrk_U0UtY1Tq1eDyYgKIDCksPywD1gVbdWMsah5Rnk7zH5sz_tmndEpM9KDqnbNE365mWKDuZMjheUEtwU1LtMWldTXG275INAr5hf7SPV4tmF5E5Fpav1U7S-hpBBp-RwNgwxYgxymPom4vpeJ6_ymMYs_4EZr4VsMtZfh_-783SLXthKDBfbk5zOU_y5TWoiWm_rIPDbdLHmSY-jUKnnZpmK9Oe2smTyUOqY1TNQ8RGFBlnJIwnU1z8kYzJNAUN19ogbEfYS4fH7hSPRqzqX-wb_pW9D2XW4lgvl9fjL7rcGTajScP8jhh2NXmhVtc6C85uX1XGGNGxUZLUXEwEO4mYWRn6lyP5T05lFMIjGSQLILO14a2Ow_Rh9nMYT55y9iVMDcG_AwQJg6_3qKWyyWRrZkFpBiqRI5O4KhzFcXzIHcrg7KZMfMCuccuM2OBVe12PJvYHSxsld0KmPPikaVtHxFKWCoNMryVC45ouJBB4_tE8FBBnhRaDHKfY-F37jQ00Z64pJOOO7D5erbFlgZp_3g3YeB8YsR6ShUw9haV_lm3iZSSPhX881FnTVe8fKaCvBo0HLuIchqaLt14z_QU9XW0eE8GzLPT2i7uNe3S5Vk1BhN7HGozMvM";
    // const getPaymentStatusResponse = yield call(api, authToken, docId);
    const getPaymentStatusResponse = api(authToken, docId, environment, lang);
    Promise.all([getPaymentStatusResponse]).then(response => {
      if (_.isFunction(callback)) {
        callback(response);
      }
    });
  } catch (e) {
    /* TODO need error handling */
  }
}
