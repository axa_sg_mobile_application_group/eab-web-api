import { call, put, select } from "redux-saga/effects";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  CLIENT,
  CONFIG,
  PRODUCTS,
  QUOTATION,
  OPTIONS_MAP
} from "../constants/REDUCER_TYPES";

export function* getDependants(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const cid = yield select(state => state[CLIENT].profile.cid);
    const isQuickQuote = yield select(state => state[QUOTATION].isQuickQuote);

    const response = yield call(callServer, {
      url: `product`,
      data: {
        action: "getDependantList",
        cid,
        quickQuote: isQuickQuote
      }
    });

    yield put({
      type: ACTION_TYPES[PRODUCTS].UPDATE_DEPENDANTS,
      dependantsData: response.dependants
    });

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getProductList(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const currency = yield select(state => state[PRODUCTS].currency);
    const isQuickQuote = yield select(state => state[QUOTATION].isQuickQuote);
    const insuredCid = yield select(state => state[PRODUCTS].insuredCid);
    const cid = yield select(state => state[CLIENT].profile.cid);
    const optionsMap = yield select(state => state[OPTIONS_MAP]);

    const response = yield call(callServer, {
      url: `product`,
      data: {
        action: "queryProducts",
        insuredCid: insuredCid || cid,
        proposerCid: insuredCid ? cid : null,
        quickQuote: isQuickQuote,
        params: {
          ccy: currency
        },
        optionsMap
      }
    });

    yield put({
      type: ACTION_TYPES[PRODUCTS].UPDATE_PRODUCT_LIST,
      productListData: response.prodCategories || [],
      errorMessage: response.error || response.errorMsg || ""
    });

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* productTabBarSaga(action) {
  try {
    yield put({
      type: ACTION_TYPES[PRODUCTS].GET_DEPENDANTS,
      callback: action.callback
    });

    yield put({
      type: ACTION_TYPES[PRODUCTS].GET_PRODUCT_LIST
    });
  } catch (e) {
    // TODO need error handling
  }
}
