import * as _ from "lodash";
import { call, put, select } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { ERROR, CONFIRMATION } from "../constants/DIALOG_TYPES";
import { getEmailContent } from "../utilities/common";
import {
  AGENT,
  CLIENT,
  CONFIG,
  PRODUCTS,
  PROPOSAL,
  QUOTATION,
  LOGIN,
  OPTIONS_MAP
} from "../constants/REDUCER_TYPES";
import fundChecker from "../utilities/fundChecker";

export function* getQuotation(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const insuredCid = yield select(state => state[PRODUCTS].insuredCid);
    const isQuickQuote = yield select(state => state[QUOTATION].isQuickQuote);

    // TODO update params, confirm
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        method: "get",
        action: "initQuotation",
        productId: action.productID,
        iCid: !_.isEmpty(insuredCid) ? insuredCid : profile.cid,
        pCid: profile.cid,
        quickQuote: isQuickQuote,
        params: {
          ccy: null
        },
        confirm: action.confirm
      }
    });

    if (resp.success) {
      switch (true) {
        case !!resp.errorMsg:
          action.alert(resp.errorMsg);
          break;
        case !!resp.requireConfirm:
          action.confirmRequest("products.confirm.invalidateApplications");
          break;
        case !!(resp.quotation && resp.planDetails):
          yield put({
            type: ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

          yield put({
            type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
            profileData: resp.profile
          });

          if (
            resp.quotation.baseProductCode === "PNP" ||
            resp.quotation.baseProductCode === "PNP2" ||
            resp.quotation.baseProductCode === "PNPP" ||
            resp.quotation.baseProductCode === "PNPP2"
          ) {
            yield put({
              type: ACTION_TYPES[QUOTATION].QUOTE,
              quotationData: resp.quotation
            });
          }

          action.openQuotationFunction();
          break;
        default:
          throw new Error("saga 'getQuotation' got unexpected response");
      }
    } else {
      throw new Error("saga 'getQuotation' request fail");
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getFunds(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const quotation = _.cloneDeep(
      yield select(state => state[QUOTATION].quotation)
    );
    const {
      isReQuote = false,
      isClone = false,
      updateObject,
      callback
    } = action;

    const getFundsAPI = yield call(callServer, {
      url: `quot`,
      data: {
        action: "getFundDetails",
        productId: quotation.baseProductId,
        invOpt: quotation.fund ? quotation.fund.invOpt : action.invOpt,
        paymentMethod:
          quotation.policyOptions && quotation.policyOptions.paymentMethod
      }
    });

    yield put({
      type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS,
      newAvailableFunds: getFundsAPI.funds
    });

    if (isReQuote) {
      yield put(
        Object.assign({}, updateObject, {
          type: ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED
        })
      );
    }

    if (isClone) {
      yield put(
        Object.assign({}, updateObject, {
          type: ACTION_TYPES[PROPOSAL].CLONE_COMPLETED
        })
      );
    }

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* allocFunds(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const quotation = yield select(state => state[QUOTATION].quotation);
    const {
      invOpt,
      portfolio,
      fundAllocs,
      topUpAllocs,
      hasTopUpAlloc,
      adjustedModel,
      callback
    } = action;

    const allocFundsAPI = yield call(callServer, {
      url: `quot`,
      data: {
        action: "allocFunds",
        quotation: _.cloneDeep(quotation),
        invOpt,
        portfolio,
        fundAllocs,
        hasTopUpAlloc,
        topUpAllocs,
        adjustedModel
      }
    });

    yield put({
      type: ACTION_TYPES[QUOTATION].ALLOC_FUNDS_COMPLETED,
      newQuotation: allocFundsAPI.quotation,
      newQuotationErrors: allocFundsAPI.errors
    });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* updateIsQuickQuote(action) {
  try {
    yield put({
      type: ACTION_TYPES[PRODUCTS].UPDATE_INSURED_CID,
      insuredCidData: null
    });

    yield put({
      type: ACTION_TYPES[PRODUCTS].GET_DEPENDANTS,
      callback: () => {}
    });

    yield put({
      type: ACTION_TYPES[PRODUCTS].GET_PRODUCT_LIST,
      callback: action.callback ? action.callback : () => {}
    });
  } catch (e) {
    // TODO need error handling
  }
}

export function* queryQuickQuotes(action) {
  try {
    const pCid = yield select(state => state[CLIENT].profile.cid);
    const callServer = yield select(state => state[CONFIG].callServer);

    const response = yield call(callServer, {
      url: `quot`,
      data: {
        action: "queryQuickQuotes",
        pCid
      }
    });

    yield put({
      type: ACTION_TYPES[QUOTATION].UPDATE_QUICK_QUOTES,
      quickQuotesData: response.quickQuotes
    });

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* deleteQuickQuotes(action) {
  try {
    const pCid = yield select(state => state[CLIENT].profile.cid);
    const callServer = yield select(state => state[CONFIG].callServer);
    const { quotIds } = action;

    const response = yield call(callServer, {
      url: `quot`,
      data: {
        action: "deleteQuickQuotes",
        pCid,
        quotIds
      }
    });

    yield put({
      type: ACTION_TYPES[QUOTATION].UPDATE_QUICK_QUOTES,
      quickQuotesData: response.quickQuotes
    });

    action.callback();
  } catch (e) {
    // TODO need error handling
  }
}

export function* createProposal(action) {
  try {
    const { callback, warningCallback, errorCallback } = action;
    const quotation = yield select(state => state[QUOTATION].quotation);
    const callServer = yield select(state => state[CONFIG].callServer);
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        method: "post",
        action: "genProposal",
        quotation,
        optionsMap
      }
    });

    if (resp.success) {
      if (resp.errorMsg) {
        errorCallback(resp.errorMsg);
      } else {
        yield put({
          type: ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL,
          quotation: resp.quotation,
          pdfData: resp.proposal,
          illustrateData: resp.illustrateData,
          planDetails: resp.planDetails,
          canRequote: _.get(resp, "funcPerms.requote", false),
          canRequoteInvalid: _.get(resp, "funcPerms.requoteInvalid", false),
          canClone: _.get(resp, "funcPerms.clone", false),
          canEmail: true
        });

        if (resp.warningMsg && action.warningCallback) {
          warningCallback(resp.warningMsg);
        } else if (_.isFunction(callback)) {
          callback();
        }
      }
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* updateRiders(action) {
  try {
    const { covCodes } = action;
    const quotation = _.cloneDeep(
      yield select(state => state[QUOTATION].quotation)
    );
    const callServer = yield select(state => state[CONFIG].callServer);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "updateRiderList",
        quotation,
        newRiderList: covCodes
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[QUOTATION].UPDATE_RIDERS_COMPLETED,
        newQuotation: resp.quotation,
        newInputConfigs: resp.inputConfigs,
        newQuotationWarnings: resp.quotWarnings,
        newQuotationErrors: resp.errors
      });
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* updateClient() {
  try {
    const quotation = yield select(state => state[QUOTATION].quotation);
    const callServer = yield select(state => state[CONFIG].callServer);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "updateClients",
        quotation
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_INSUREDS,
        newAvailableInsureds: resp.availableInsureds
      });
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* updateShieldRiders(action) {
  try {
    const { covCodes, cid, alert } = action;
    const quotation = _.cloneDeep(
      yield select(state => state[QUOTATION].quotation)
    );
    const callServer = yield select(state => state[CONFIG].callServer);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "updateShieldRiderList",
        quotation,
        newRiderList: covCodes,
        cid
      }
    });

    if (resp.success) {
      if (resp.errorMsg) {
        alert(resp.errorMsg);
      } else {
        yield put({
          type: ACTION_TYPES[QUOTATION].UPDATE_INSURED_QUOTATION,
          newQuotation: resp.quotation,
          newInputConfigs: resp.inputConfigs,
          newQuotationErrors: resp.errors
        });
      }
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* quote(action) {
  try {
    const { quotationData } = action;
    const callServer = yield select(state => state[CONFIG].callServer);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "quote",
        quotation: _.cloneDeep(quotationData)
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[QUOTATION].QUOTE_COMPLETED,
        newQuotation: resp.quotation,
        newInputConfigs: resp.inputConfigs,
        newQuotationWarnings: resp.quotWarnings,
        newQuotationErrors: resp.errors
      });
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* resetQuot(action) {
  try {
    const {
      quotation,
      keepConfigs,
      keepPolicyOptions,
      keepPlans,
      keepFunds,
      callback
    } = action;
    const callServer = yield select(state => state[CONFIG].callServer);
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "resetQuot",
        quotation: _.cloneDeep(quotation),
        keepConfigs,
        keepPolicyOptions,
        keepPlans,
        keepFunds
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[QUOTATION].RESET_QUOTE_COMPLETED,
        newQuotation: resp.quotation,
        newInputConfigs: resp.inputConfigs,
        newQuotationWarnings: resp.quotWarnings,
        newQuotationErrors: resp.errors
      });

      if (_.isFunction(callback)) {
        yield call(callback);
      }
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* viewProposal(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { readonly, quotationId, callback } = action;
    const optionsMap = yield select(state => state[OPTIONS_MAP]);
    const resp = yield call(callServer, {
      url: "quot",
      data: {
        method: "post",
        action: "getProposal",
        quotId: quotationId,
        readonly,
        optionsMap
      }
    });

    yield put({
      type: ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL,
      quotation: resp.quotation,
      pdfData: resp.proposal,
      illustrateData: resp.illustrateData,
      planDetails: resp.planDetails,
      canRequote: _.get(resp, "funcPerms.requote", false),
      canRequoteInvalid: _.get(resp, "funcPerms.requoteInvalid", false),
      canClone: _.get(resp, "funcPerms.clone", false),
      canEmail: !readonly
    });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* requote(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { quotationId, callback } = action;

    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        method: "post",
        action: "requote",
        quotId: quotationId
      }
    });

    if (resp.success) {
      if (
        fundChecker({
          quotation: resp.quotation,
          planDetails: resp.planDetails
        })
      ) {
        // update quotation for get fund
        yield put({
          type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
          newQuotation: {
            baseProductId: resp.quotation.baseProductId,
            policyOptions: resp.quotation.policyOptions
          }
        });

        yield put({
          type: ACTION_TYPES[QUOTATION].GET_FUNDS,
          isReQuote: true,
          invOpt: resp.quotation.fund
            ? resp.quotation.fund.invOpt
            : "mixedAssets",
          updateObject: {
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newAvailableInsureds: resp.availableInsureds
          },
          callback
        });
      } else {
        yield put({
          type: ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED,
          newQuotation: resp.quotation,
          newPlanDetails: resp.planDetails,
          newInputConfigs: resp.inputConfigs,
          newQuotationWarnings: resp.quotWarnings,
          newQuotationErrors: resp.errors,
          newAvailableInsureds: resp.availableInsureds
        });

        if (_.isFunction(callback)) {
          callback();
        }
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* requoteInvalid(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { quotationId, confirm, callback } = action;
    let dialogType = "";

    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        method: "post",
        action: "requoteInvalid",
        quotId: quotationId,
        confirm
      }
    });

    if (resp.success) {
      if (resp.requireConfirm) {
        dialogType = CONFIRMATION;
      } else if (!resp.allowRequote) {
        dialogType = ERROR;
      } else {
        yield put({
          type: ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED,
          newQuotation: resp.quotation,
          newPlanDetails: resp.planDetails,
          newInputConfigs: resp.inputConfigs,
          newQuotationWarnings: resp.quotWarnings,
          newQuotationErrors: resp.errors,
          newAvailableInsureds: resp.availableInsureds
        });
      }

      if (_.isFunction(callback)) {
        yield call(callback, dialogType);
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* clone(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { quotationId, callback } = action;

    // TODO: update confirm
    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        method: "post",
        action: "cloneQuotation",
        quotId: quotationId,
        confirm: true
      }
    });

    if (resp.success) {
      if (resp.allowClone) {
        if (
          fundChecker({
            quotation: resp.quotation,
            planDetails: resp.planDetails
          })
        ) {
          // update quotation for get fund
          yield put({
            type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
            newQuotation: {
              baseProductId: resp.quotation.baseProductId,
              policyOptions: resp.quotation.policyOptions
            }
          });
          yield put({
            type: ACTION_TYPES[QUOTATION].GET_FUNDS,
            isClone: true,
            invOpt: resp.quotation.fund
              ? resp.quotation.fund.invOpt
              : "mixedAssets",
            updateObject: {
              newQuotation: resp.quotation,
              newPlanDetails: resp.planDetails,
              newInputConfigs: resp.inputConfigs,
              newQuotationWarnings: resp.quotWarnings,
              newAvailableInsureds: resp.availableInsureds
            },
            callback: () => {
              callback(resp.allowClone);
            }
          });
        } else {
          yield put({
            type: ACTION_TYPES[PROPOSAL].CLONE_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

          if (
            resp.quotation.baseProductCode === "PNP" ||
            resp.quotation.baseProductCode === "PNP2" ||
            resp.quotation.baseProductCode === "PNPP" ||
            resp.quotation.baseProductCode === "PNPP2"
          ) {
            yield put({
              type: ACTION_TYPES[QUOTATION].QUOTE,
              quotationData: resp.quotation
            });
          }
        }
      }
      if (_.isFunction(callback)) {
        callback(resp.allowClone);
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* selectInsuredQuotation(action) {
  try {
    const { cid, covClass } = action;
    const callServer = yield select(state => state[CONFIG].callServer);
    const quotation = yield select(state => state[QUOTATION].quotation);

    const resp = yield call(callServer, {
      url: `quot`,
      data: {
        action: "selectInsured",
        quotation,
        cid,
        covClass
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[QUOTATION].UPDATE_INSURED_QUOTATION,
        newQuotation: resp.quotation,
        newInputConfigs: resp.inputConfigs,
        newQuotationErrors: resp.errors
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* openEmailDialog(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const agent = yield select(state => state[AGENT]);
    const proposal = yield select(state => state[PROPOSAL]);
    const isQuickQuote = yield select(state => state[QUOTATION].isQuickQuote);
    const reports = proposal.pdfData;
    const response = yield call(callServer, {
      url: `quot`,
      data: {
        action: "getEmailTemplate",
        standalone: isQuickQuote
      }
    });

    if (response && response.success) {
      const {
        agentContent,
        clientContent,
        agentSubject,
        clientSubject,
        embedImgs
      } = response;

      const params = {
        agentName: agent.name,
        agentContactNum: agent.mobile,
        clientName: profile.fullName
      };

      const attachments = _.map(proposal.pdfData, pdf => pdf.id);
      const emails = [
        {
          id: "agent",
          isQQ: isQuickQuote,
          agentCode: agent.agentCode,
          dob: "",
          to: [agent.email],
          title: agentSubject,
          content: getEmailContent(
            agentContent,
            params,
            '<img src="cid:axa_logo.png" width="72" height="72">'
          ),
          attachmentIds: attachments,
          embedImgs
        },
        {
          id: "client",
          isQQ: isQuickQuote,
          agentCode: "",
          dob: profile.dob,
          to: [profile.email],
          title: clientSubject,
          content: getEmailContent(
            clientContent,
            params,
            '<img src="cid:axa_logo.png" width="72" height="72">'
          ),
          attachmentIds: attachments,
          embedImgs
        }
      ];

      yield put({
        type: ACTION_TYPES[PROPOSAL].OPEN_EMAIL,
        emails,
        reports
      });

      if (_.isFunction(action.callback)) {
        action.callback();
      }
    }
  } catch (e) {
    // TODO: need error handling
  }
}

export function* sendEmail(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const emails = yield select(state => state[PROPOSAL].proposalEmail.emails);
    const reportOptions = yield select(
      state => state[PROPOSAL].proposalEmail.reportOptions
    );
    const quotation = yield select(state => state[PROPOSAL].quotation);
    const authToken = yield select(state => state[LOGIN].authToken);
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);

    let newEmail = _.cloneDeep(emails);

    newEmail.forEach(item => {
      item.attachmentIds = [];
    });

    Object.keys(reportOptions).forEach(user => {
      Object.keys(reportOptions[user]).forEach(report => {
        if (reportOptions[user][report] === true) {
          const index = newEmail.findIndex(x => x.id === user);
          newEmail[index].attachmentIds.push(report);
        }
      });
    });

    newEmail = newEmail.filter(item => item.attachmentIds.length !== 0);

    _.forEach(newEmail, email => {
      email.content += "<img src='cid:axa_logo.png'>";
    });
    const response = yield call(callServer, {
      url: `quot`,
      data: {
        action: "emailProposal",
        emails: newEmail,
        quotId: quotation.id,
        authToken,
        webServiceUrl
      }
    });

    if (response && response.success) {
      if (_.isFunction(action.callback)) {
        action.callback();
      }
    }
  } catch (e) {
    // TODO: need error handling
  }
}
