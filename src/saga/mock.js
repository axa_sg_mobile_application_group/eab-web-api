export default {
  success: true,
  warningMsg: {
    msgCode: 0,
    showSignExpiryWarning: false
  },
  stepper: {
    sections: {
      items: [
        {
          id: "stepApp",
          type: "section",
          title: "Application",
          detailSeq: 1,
          items: [
            {
              id: "app",
              type: "shieldAppForm"
            }
          ]
        },
        {
          id: "stepSign",
          type: "section",
          title: "Signature",
          detailSeq: 2,
          items: [
            {
              id: "sign",
              type: "shieldSignature"
            }
          ]
        },
        {
          id: "stepPay",
          type: "section",
          title: "Payment",
          detailSeq: 3,
          items: [
            {
              id: "pay",
              type: "shieldPayment"
            }
          ]
        },
        {
          id: "stepSubmit",
          type: "section",
          title: "Submit",
          detailSeq: 4,
          items: [
            {
              id: "submit",
              type: "shieldSubmission"
            }
          ]
        }
      ]
    },
    index: {
      current: 2,
      completed: 1,
      active: 2
    },
    enableNextStep: false
  },
  template: {
    items: [
      {
        items: [
          {
            allowEdit: false,
            fullWidth: true,
            generateTotalRow: true,
            id: "premiumDetails",
            items: [
              {
                id: "laName",
                planDetailLayout: true,
                title: "Life Assured Name",
                type: "TEXT"
              },
              {
                disableTdStyle: true,
                items: [
                  {
                    id: "planCover",
                    type: "image"
                  },
                  {
                    id: "covName",
                    type: "TEXT"
                  }
                ],
                presentation: "planCover@covName",
                title: "Plan Name",
                type: "HBOX"
              },
              {
                id: "policyNumber",
                planDetailLayout: true,
                title: "Proposal Number",
                type: "TEXT"
              },
              {
                calTotal: true,
                decimal: 2,
                id: "medisave",
                planDetailLayout: true,
                subTitle: "(incl. Medishield Life)",
                subType: "currency",
                title: "CPF Portion",
                trigger: {
                  id: "medisave",
                  replacement: "-",
                  type: "showIfNotEqual",
                  value: "0"
                },
                type: "TEXT"
              },
              {
                calTotal: true,
                decimal: 2,
                id: "cashPortion",
                planDetailLayout: true,
                subType: "currency",
                title: "Cash Portion",
                trigger: {
                  id: "cashPortion",
                  replacement: "-",
                  type: "showIfNotEqual",
                  value: "0"
                },
                type: "TEXT"
              },
              {
                disableRowTrigger: {
                  id: "payFrequency",
                  type: "disableIfEqual",
                  value: "M"
                },
                editable: true,
                id: "subseqPayMethod",
                planDetailLayout: true,
                tableCellTitle: "GIRO",
                tableTrigger: {
                  id: "cashPortion",
                  replacement: "-",
                  type: "showIfNotEqual",
                  value: "0"
                },
                title: "Subsequent Premium Payment Method",
                type: "CHECKBOX"
              }
            ],
            noBottomBorder: true,
            planDetailLayout: true,
            totalIndex: 2,
            totalRowAlignLeft: true,
            type: "table",
            zebraRow: true
          }
        ],
        title: "Premium Details",
        type: "BLOCK"
      },
      {
        content:
          "<span style='padding: 0px; line-height: 2em'>*The CPF account no. used for initial payment will be used for all subsequent payments for AXA shield.</span>",
        noMargin: true,
        style: {
          color: "#ec4d33"
        },
        type: "PARAGRAPH"
      },
      {
        content:
          "<span style='padding: 0px; line-height: 2em'>*Any premium in excess of the withdrawal limits on Medisave will be payable via the selected subsequent payment method.</span>",
        noMargin: true,
        style: {
          color: "#ec4d33"
        },
        type: "PARAGRAPH"
      },
      {
        items: [
          {
            bold: true,
            id: "cpfPortionMedisaveAccDetails",
            padding12TopBottom: true,
            subType: "titleOnly",
            title: "CPF Portion (Medisave Account Details)",
            type: "TEXTONLY"
          },
          {
            allowEdit: false,
            fullWidth: true,
            id: "cpfMedisaveAccDetails",
            items: [
              {
                id: "cpfAccHolderName",
                planDetailLayout: true,
                postfix: "(Proposer)",
                title: "CPF account Holder Name",
                type: "TEXT"
              },
              {
                id: "cpfAccNo",
                planDetailLayout: true,
                title: "CPF account no.",
                type: "Text"
              }
            ],
            noBottomBorder: true,
            planDetailLayout: true,
            type: "table",
            zebraRow: true
          },
          {
            bold: true,
            id: "cashPortionTitle",
            padding12Top: true,
            subType: "titleOnly",
            title: "Cash Portion",
            trigger: {
              id: "totCashPortion",
              type: "showIfNotEqual",
              value: "0"
            },
            type: "TEXTONLY"
          },
          {
            items: [
              {
                items: [
                  {
                    disableTrigger: {
                      id: "trxStatus",
                      type: "trueIfEqual",
                      value: "Y"
                    },
                    hiddenField: {
                      type: "skip"
                    },
                    id: "initPayMethod",
                    mandatory: true,
                    options: [
                      {
                        title: "Credit Card",
                        value: "crCard"
                      },
                      {},
                      {
                        title: "AXS/SAM",
                        value: "axasam"
                      },
                      {
                        title: "Cheque/Cashier Order",
                        value: "chequeCashierOrder"
                      },
                      {
                        title: "Cash",
                        value: "cash"
                      },
                      {
                        title: "Telegraphic Transfer",
                        value: "teleTransfter"
                      }
                    ],
                    subType: "PAYMENT",
                    title: "Initial Payment Methods",
                    trigger: [
                      {
                        id: "trxStatus",
                        type: "showIfExist",
                        value: ",Y,N,C"
                      }
                    ],
                    type: "PICKER"
                  },
                  {
                    disableTrigger: {
                      id: "trxStatusRemark",
                      type: "trueIfEqual",
                      value: "I"
                    },
                    hiddenField: {
                      type: "skip"
                    },
                    id: "initPayMethod",
                    mandatory: true,
                    options: [
                      {
                        condition: "crCard",
                        title: "Credit Card",
                        value: "crCard"
                      },
                      {},
                      {
                        title: "AXS/SAM",
                        value: "axasam"
                      }
                    ],
                    subType: "PAYMENT",
                    title: "Initial Payment Methods",
                    trigger: [
                      {
                        id: "trxStatus",
                        stop: true,
                        type: "showIfExist",
                        value: "I,O"
                      },
                      {
                        id: "trxMethod",
                        type: "optionCondition"
                      }
                    ],
                    type: "PICKER"
                  },
                  {
                    disabled: false,
                    id: "initPayMethodButton",
                    isShield: true,
                    title: "Pay",
                    trigger: [
                      {
                        id: "initPayMethod",
                        type: "showIfExist",
                        value: "crCard,eNets"
                      }
                    ],
                    type: "PAYMENTBUTTON",
                    value: "initPayMethod"
                  }
                ],
                type: "12Box"
              },
              {
                items: [
                  {
                    highLight: true,
                    id: "trxStatusRemark",
                    options: [
                      {
                        title: "Transaction in progress...",
                        value: "I"
                      },
                      {
                        title: "Transaction Successful",
                        value: "Y"
                      },
                      {
                        title: "Transaction Failed",
                        value: "N"
                      },
                      {
                        title: "Transaction Cancelled",
                        value: "C"
                      },
                      {
                        condition: "crCard",
                        title:
                          "Oops, there was no response from the Credit Card server for this transaction. Please check the payment status after sometime. Alternatively, you may choose the AXS/SAM option to submit the case.",
                        value: "O"
                      },
                      {
                        condition: "eNets",
                        title:
                          "Oops, there was no response from the eNets server for this transaction. Please check the payment status after sometime. Alternatively, you may choose the AXS/SAM option to submit the case.",
                        value: "O"
                      },
                      {
                        condition: "crCard",
                        title:
                          "Oops, you may have accidentally closed the credit card gateway window and/or there was no response from the Credit Card server for this transaction. Please check the payment status after sometime. Alternatively, you may choose the AXS/SAM option to submit the case",
                        value: "E"
                      },
                      {
                        condition: "eNets",
                        title:
                          "Oops, you may have accidentally closed the eNet payment window and/or there was no response from the eNets server for this transaction. Please check the payment status after sometime. Alternatively, you may choose the AXS/SAM option to submit the case",
                        value: "E"
                      }
                    ],
                    subType: "multiline",
                    title: "Payment Status",
                    trigger: [
                      {
                        id: "trxMethod",
                        type: "optionCondition"
                      }
                    ],
                    type: "ReadOnly"
                  }
                ],
                presentation: "Column1",
                trigger: [
                  {
                    id: "initPayMethod",
                    type: "showIfExist",
                    value: "crCard,eNets"
                  },
                  {
                    id: "trxStatusRemark",
                    type: "showIfNotEqual",
                    value: ""
                  }
                ],
                triggerType: "and",
                type: "HBOX"
              },
              {
                items: [
                  {
                    highLight: true,
                    id: "trxTime",
                    presentation: "mmm dd, yyyy hh:MM TT",
                    subType: "DATEWITHTIME",
                    title: "Transaction Time",
                    type: "READONLY"
                  },
                  {
                    highLight: true,
                    id: "trxNo",
                    title: "Transaction Reference No",
                    type: "READONLY"
                  }
                ],
                trigger: [
                  {
                    id: "initPayMethod",
                    type: "showIfExist",
                    value: "crCard,eNets"
                  },
                  {
                    id: "trxStatusRemark",
                    type: "showIfNotEqual",
                    value: ""
                  }
                ],
                triggerType: "and",
                type: "HBOX"
              },
              {
                items: [
                  {
                    items: [
                      {
                        items: [
                          {
                            id: "axsOnline",
                            subType: "URL",
                            title: "AXS online",
                            type: "BUTTON",
                            value: "http://www.axs.com.sg"
                          },
                          {
                            id: "samOnline",
                            style: {
                              marginLeft: "-50px"
                            },
                            subType: "URL",
                            title: "SAM online",
                            type: "BUTTON",
                            value: "https://www.mysam.sg"
                          }
                        ],
                        type: "HBOX"
                      }
                    ],
                    type: "HBOX"
                  }
                ],
                trigger: {
                  id: "initPayMethod",
                  type: "showIfEqual",
                  value: "axasam"
                },
                type: "BLOCK"
              },
              {
                items: [
                  {
                    id: "ttRemittingBank",
                    mandatory: true,
                    max: 30,
                    title: "Remitting Bank",
                    type: "TEXT"
                  },
                  {
                    id: "ttDOR",
                    mandatory: true,
                    title: "Date of Remittance",
                    type: "DATEPICKER"
                  },
                  {
                    id: "ttRemarks",
                    mandatory: true,
                    max: 100,
                    title: "Remarks",
                    type: "TEXT"
                  }
                ],
                trigger: {
                  id: "initPayMethod",
                  type: "showIfEqual",
                  value: "teleTransfter"
                },
                type: "BLOCK"
              }
            ],
            trigger: {
              id: "totCashPortion",
              type: "showIfNotEqual",
              value: "0"
            },
            type: "BLOCK"
          },
          {
            content:
              "Please setup e-GIRO for DBS/POSB Accounts, or you can submit the original GIRO form to AXA Insurance Pte Ltd",
            style: {
              color: "#ec4d33"
            },
            title: "",
            trigger: {
              id: "premiumDetails/subseqPayMethod",
              type: "showIfExistinArray",
              value: "Y"
            },
            type: "PARAGRAPH"
          }
        ],
        title: "Initial Payment Method",
        type: "BLOCK"
      }
    ],
    type: "BLOCK"
  },
  application: {
    agent: {
      agentCode: "084620",
      company: "AXA Insurance Pte Ltd",
      dealerGroup: "AGENCY",
      email: "eposapi.axa.sg@eabsystems.com",
      mobile: "00000000",
      name: "John Huynh",
      tel: "0"
    },
    agentChannelType: "N",
    agentCode: "084620",
    appCompletedStep: 1,
    appStep: 2,
    applicationForm: {
      values: {
        appFormTemplate: {
          main: "appform_report_shield_main",
          properties: {
            dollarSignForAllCcy: false
          },
          template: [
            "appform_report_common_shield",
            "appform_report_personal_details_shield",
            "appform_report_plan_details_shield",
            "appform_report_rop_insurability_shield",
            "appform_report_declaration_ph_shield",
            "appform_report_declaration_la_shield"
          ]
        },
        checkedMenu: [
          "menu_person",
          "menu_residency",
          "menu_insure",
          "menu_declaration"
        ],
        completedMenus: [
          "menu_residency",
          "menu_plan",
          "menu_person",
          "menu_insure",
          "menu_declaration"
        ],
        insured: [
          {
            declaration: {
              BANKRUPTCY01: "N",
              FCD_01: "1",
              FCD_02: "N"
            },
            extra: {
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              isCompleted: true,
              isPdfGenerated: true,
              isPhSameAsLa: "Y",
              premium: 2560.7
            },
            groupedPlanDetails: {
              awl: 300,
              axaShield: 283,
              basicPlanList: [
                {
                  cashPortion: 0,
                  covClass: "A",
                  covCode: "ASIM",
                  covName: {
                    en: "AXA Shield (Plan A)",
                    "zh-Hant": "AXA Shield (Plan A)"
                  },
                  covNameDesc: {
                    en: "AXA Shield",
                    "zh-Hant": "AXA Shield"
                  },
                  cpfPortion: 283,
                  medisave: 593,
                  payFreq: "A",
                  payFreqDesc: "Annual",
                  paymentMethod: "CPF",
                  paymentMethodDesc: "CPF",
                  planCode: "ASIMSA",
                  planType: "Health Plan",
                  planTypeDesc: "Plan A",
                  premium: 593,
                  productId: "08_product_ASIM_3",
                  productLine: "HP",
                  sumInsured: " - ",
                  tax: {
                    yearTax: 38.8
                  },
                  version: 3
                }
              ],
              cashPortion: 0,
              ccy: "SGD",
              covClass: "A",
              cpfPortion: 283,
              medisave: 593,
              medishieldLife: 310,
              paymentMode: "A",
              planTypeName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              premium: 0,
              riderPlanList: [],
              totYearPrem: 0,
              totalRiderPremium: 0
            },
            insurability: {
              HEALTH13: "N",
              HEALTH_SHIELD_01: "N",
              HEALTH_SHIELD_02: "N",
              HEALTH_SHIELD_03: "N",
              HEALTH_SHIELD_04: "N",
              HEALTH_SHIELD_05: "N",
              HEALTH_SHIELD_06: "N",
              HEALTH_SHIELD_07: "N",
              HEALTH_SHIELD_08: "N",
              HEALTH_SHIELD_09: "N",
              HEALTH_SHIELD_10: "N",
              HEALTH_SHIELD_11: "N",
              HEALTH_SHIELD_12: "N",
              HEALTH_SHIELD_13: "N",
              HEALTH_SHIELD_14: "N",
              HEALTH_SHIELD_15: "N",
              HEALTH_SHIELD_16: "N",
              HEALTH_SHIELD_17: "N",
              HEALTH_SHIELD_18: "N",
              HEALTH_SHIELD_19: "N",
              HEALTH_SHIELD_20: "N",
              HEALTH_SHIELD_21: "N",
              HEALTH_SHIELD_22: "N",
              HEALTH_SHIELD_23: "N",
              HW01: 1,
              HW02: 123,
              HW03: "N",
              INS03: "N",
              INS04: "N",
              INS05: "N",
              LIFESTYLE01: "N",
              LIFESTYLE02: "N",
              insAllNoInd: "Y"
            },
            personalInfo: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentId: "084620",
              allowance: 1000000,
              bundle: [
                {
                  id: "FN001002-00009",
                  isValid: true
                }
              ],
              cid: "CP001002-00008",
              dependants: [
                {
                  cid: "CP001002-00001",
                  relationship: "SPO",
                  relationshipOther: null
                }
              ],
              dob: "1987-12-20",
              education: "below",
              email: "",
              employStatus: "hw",
              firstName: "222",
              fnaRecordIdArray: "",
              fullName: "222",
              gender: "F",
              hanyuPinyinName: "",
              hasUT: "N",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I8",
              initial: "2",
              isSmoker: "N",
              isValid: true,
              language: "en",
              lastName: "",
              lastUpdateDate: "2018-09-15T12:12:39.271Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "61988440",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 31,
              occupation: "O2",
              organization: "N/A",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "",
              referrals: "",
              relationship: "SPO",
              residenceCountry: "R2",
              title: "Dr",
              type: "cust",
              unitNum: ""
            },
            policies: {
              ROP_01: "N"
            }
          },
          {
            declaration: {
              BANKRUPTCY01: "N",
              FCD_01: "1",
              FCD_02: "N"
            },
            extra: {
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              isCompleted: true,
              isPdfGenerated: true,
              isPhSameAsLa: "Y",
              premium: 2560.7
            },
            groupedPlanDetails: {
              awl: 300,
              axaShield: 283,
              basicPlanList: [
                {
                  cashPortion: 0,
                  covClass: "A",
                  covCode: "ASIM",
                  covName: {
                    en: "AXA Shield (Plan A)",
                    "zh-Hant": "AXA Shield (Plan A)"
                  },
                  covNameDesc: {
                    en: "AXA Shield",
                    "zh-Hant": "AXA Shield"
                  },
                  cpfPortion: 283,
                  medisave: 593,
                  payFreq: "A",
                  payFreqDesc: "Annual",
                  paymentMethod: "CPF",
                  paymentMethodDesc: "CPF",
                  planCode: "ASIMSA",
                  planType: "Health Plan",
                  planTypeDesc: "Plan A",
                  premium: 593,
                  productId: "08_product_ASIM_3",
                  productLine: "HP",
                  sumInsured: " - ",
                  tax: {
                    yearTax: 38.8
                  },
                  version: 3
                }
              ],
              cashPortion: 0,
              ccy: "SGD",
              covClass: "A",
              cpfPortion: 283,
              medisave: 593,
              medishieldLife: 310,
              paymentMode: "A",
              planTypeName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              premium: 0,
              riderPlanList: [],
              totYearPrem: 0,
              totalRiderPremium: 0
            },
            insurability: {
              HEALTH_SHIELD_01: "N",
              HEALTH_SHIELD_02: "N",
              HEALTH_SHIELD_03: "N",
              HEALTH_SHIELD_04: "N",
              HEALTH_SHIELD_05: "N",
              HEALTH_SHIELD_06: "N",
              HEALTH_SHIELD_07: "N",
              HEALTH_SHIELD_08: "N",
              HEALTH_SHIELD_09: "N",
              HEALTH_SHIELD_10: "N",
              HEALTH_SHIELD_11: "N",
              HEALTH_SHIELD_12: "N",
              HEALTH_SHIELD_13: "N",
              HEALTH_SHIELD_14: "N",
              HEALTH_SHIELD_15: "N",
              HEALTH_SHIELD_16: "N",
              HEALTH_SHIELD_17: "N",
              HEALTH_SHIELD_18: "N",
              HEALTH_SHIELD_19: "N",
              HW01: 1,
              HW02: 1,
              HW03: "N",
              INS03: "N",
              INS04: "N",
              INS05: "N",
              LIFESTYLE01: "N",
              LIFESTYLE02: "N",
              insAllNoInd: "Y"
            },
            personalInfo: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentCode: "084620",
              agentId: "084620",
              allowance: 12312,
              bundle: [
                {
                  id: "FN001002-00233",
                  isValid: true
                }
              ],
              cid: "CP001002-00081",
              compCode: "01",
              dealerGroup: "AGENCY",
              dependants: [
                {
                  cid: "CP001002-00001",
                  relationship: "SON",
                  relationshipOther: null
                }
              ],
              dob: "1988-09-15",
              education: "",
              email: "",
              employStatus: "",
              firstName: "1207father",
              fnaRecordIdArray: "",
              fullName: "1207father",
              gender: "M",
              hanyuPinyinName: "",
              hasUT: "N",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I8",
              initial: "1",
              isSmoker: "N",
              isValid: true,
              lastName: "",
              lastUpdateDate: "2018-09-15T12:16:21.109Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 30,
              occupation: "O2",
              organization: "12",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "",
              referrals: "",
              relationship: "FAT",
              residenceCountry: "R2",
              title: "Dr",
              type: "cust",
              unitNum: ""
            },
            policies: {
              ROP_01: "N"
            }
          },
          {
            declaration: {
              BANKRUPTCY01: "N",
              FCD_01: "1",
              FCD_02: "N"
            },
            extra: {
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              isCompleted: true,
              isPdfGenerated: true,
              isPhSameAsLa: "Y",
              premium: 2560.7
            },
            groupedPlanDetails: {
              awl: 300,
              axaShield: 62,
              basicPlanList: [
                {
                  cashPortion: 0,
                  covClass: "C",
                  covCode: "ASIM",
                  covName: {
                    en: "AXA Shield (Standard Plan)",
                    "zh-Hant": "AXA Shield (Standard Plan)"
                  },
                  covNameDesc: {
                    en: "AXA Shield",
                    "zh-Hant": "AXA Shield"
                  },
                  cpfPortion: 62,
                  medisave: 372,
                  payFreq: "A",
                  payFreqDesc: "Annual",
                  paymentMethod: "CPF",
                  paymentMethodDesc: "CPF",
                  planCode: "ASIMSC",
                  planType: "Health Plan",
                  planTypeDesc: "Standard Plan",
                  premium: 372,
                  productId: "08_product_ASIM_3",
                  productLine: "HP",
                  sumInsured: " - ",
                  tax: {
                    yearTax: 24.34
                  },
                  version: 3
                }
              ],
              cashPortion: 0,
              ccy: "SGD",
              covClass: "C",
              cpfPortion: 62,
              medisave: 372,
              medishieldLife: 310,
              paymentMode: "A",
              planTypeName: {
                en: "AXA Shield (Standard Plan)",
                "zh-Hant": "AXA Shield (Standard Plan)"
              },
              premium: 0,
              riderPlanList: [],
              totYearPrem: 0,
              totalRiderPremium: 0
            },
            insurability: {
              HEALTH13: "N",
              HEALTH_SHIELD_01: "N",
              HEALTH_SHIELD_02: "N",
              HEALTH_SHIELD_03: "N",
              HEALTH_SHIELD_04: "N",
              HEALTH_SHIELD_05: "N",
              HEALTH_SHIELD_06: "N",
              HEALTH_SHIELD_07: "N",
              HEALTH_SHIELD_08: "N",
              HEALTH_SHIELD_09: "N",
              HEALTH_SHIELD_10: "N",
              HEALTH_SHIELD_11: "N",
              HEALTH_SHIELD_12: "N",
              HEALTH_SHIELD_13: "N",
              HEALTH_SHIELD_14: "N",
              HEALTH_SHIELD_15: "N",
              HEALTH_SHIELD_16: "N",
              HEALTH_SHIELD_17: "N",
              HEALTH_SHIELD_18: "N",
              HEALTH_SHIELD_19: "N",
              HEALTH_SHIELD_20: "N",
              HEALTH_SHIELD_21: "N",
              HEALTH_SHIELD_22: "N",
              HEALTH_SHIELD_23: "N",
              HW01: 1,
              HW02: 1,
              HW03: "N",
              INS03: "N",
              INS04: "N",
              INS05: "N",
              LIFESTYLE01: "N",
              LIFESTYLE02: "N",
              insAllNoInd: "Y"
            },
            personalInfo: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentCode: "084620",
              agentId: "084620",
              allowance: 123123,
              bundle: [
                {
                  id: "FN001002-00234",
                  isValid: true
                }
              ],
              cid: "CP001002-00082",
              compCode: "01",
              dealerGroup: "AGENCY",
              dependants: [
                {
                  cid: "CP001002-00001",
                  relationship: "SON",
                  relationshipOther: null
                }
              ],
              dob: "1988-09-15",
              education: "",
              email: "",
              employStatus: "",
              firstName: "1207mother",
              fnaRecordIdArray: "",
              fullName: "1207mother",
              gender: "F",
              hanyuPinyinName: "",
              hasUT: "N",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I8",
              initial: "1",
              isSmoker: "N",
              isValid: true,
              lastName: "",
              lastUpdateDate: "2018-09-15T12:16:36.631Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 30,
              occupation: "O2",
              organization: "123",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "",
              referrals: "",
              relationship: "MOT",
              residenceCountry: "R2",
              title: "Dr",
              type: "cust",
              unitNum: ""
            },
            policies: {
              ROP_01: "N"
            }
          },
          {
            declaration: {
              BANKRUPTCY01: "N",
              FCD_01: "1",
              FCD_02: "N"
            },
            extra: {
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              isCompleted: true,
              isPdfGenerated: true,
              isPhSameAsLa: "Y",
              premium: 2560.7
            },
            groupedPlanDetails: {
              awl: 300,
              axaShield: 62,
              basicPlanList: [
                {
                  cashPortion: 0,
                  covClass: "C",
                  covCode: "ASIM",
                  covName: {
                    en: "AXA Shield (Standard Plan)",
                    "zh-Hant": "AXA Shield (Standard Plan)"
                  },
                  covNameDesc: {
                    en: "AXA Shield",
                    "zh-Hant": "AXA Shield"
                  },
                  cpfPortion: 62,
                  medisave: 372,
                  payFreq: "A",
                  payFreqDesc: "Annual",
                  paymentMethod: "CPF",
                  paymentMethodDesc: "CPF",
                  planCode: "ASIMSC",
                  planType: "Health Plan",
                  planTypeDesc: "Standard Plan",
                  premium: 372,
                  productId: "08_product_ASIM_3",
                  productLine: "HP",
                  sumInsured: " - ",
                  tax: {
                    yearTax: 24.34
                  },
                  version: 3
                }
              ],
              cashPortion: 0,
              ccy: "SGD",
              covClass: "C",
              cpfPortion: 62,
              medisave: 372,
              medishieldLife: 310,
              paymentMode: "A",
              planTypeName: {
                en: "AXA Shield (Standard Plan)",
                "zh-Hant": "AXA Shield (Standard Plan)"
              },
              premium: 0,
              riderPlanList: [],
              totYearPrem: 0,
              totalRiderPremium: 0
            },
            insurability: {
              HEALTH_SHIELD_01: "N",
              HEALTH_SHIELD_02: "N",
              HEALTH_SHIELD_03: "N",
              HEALTH_SHIELD_04: "N",
              HEALTH_SHIELD_05: "N",
              HEALTH_SHIELD_06: "N",
              HEALTH_SHIELD_07: "N",
              HEALTH_SHIELD_08: "N",
              HEALTH_SHIELD_09: "N",
              HEALTH_SHIELD_10: "N",
              HEALTH_SHIELD_11: "N",
              HEALTH_SHIELD_12: "N",
              HEALTH_SHIELD_13: "N",
              HEALTH_SHIELD_14: "N",
              HEALTH_SHIELD_15: "N",
              HEALTH_SHIELD_16: "N",
              HEALTH_SHIELD_17: "N",
              HEALTH_SHIELD_18: "N",
              HEALTH_SHIELD_19: "N",
              HW01: 1,
              HW02: 1,
              HW03: "N",
              INS03: "N",
              INS04: "N",
              INS05: "N",
              LIFESTYLE01: "N",
              LIFESTYLE02: "N",
              insAllNoInd: "Y"
            },
            personalInfo: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentCode: "084620",
              agentId: "084620",
              allowance: 123123,
              bundle: [
                {
                  id: "FN001002-00235",
                  isValid: true
                }
              ],
              cid: "CP001002-00083",
              compCode: "01",
              dealerGroup: "AGENCY",
              dependants: [
                {
                  cid: "CP001002-00001",
                  relationship: "FAT",
                  relationshipOther: null
                }
              ],
              dob: "1988-09-15",
              education: "",
              email: "",
              employStatus: "",
              firstName: "1207son",
              fnaRecordIdArray: "",
              fullName: "1207son",
              gender: "M",
              hanyuPinyinName: "",
              hasUT: "N",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I8",
              initial: "1",
              isSmoker: "N",
              isValid: true,
              lastName: "",
              lastUpdateDate: "2018-09-15T12:16:52.026Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 30,
              occupation: "O2",
              organization: "123",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "",
              referrals: "",
              relationship: "SON",
              residenceCountry: "R2",
              title: "Dr",
              type: "cust",
              unitNum: ""
            },
            policies: {
              ROP_01: "N"
            }
          }
        ],
        menus: [
          "menu_person",
          "menu_residency",
          "menu_insure",
          "menu_plan",
          "menu_declaration"
        ],
        proposer: {
          declaration: {
            BANKRUPTCY01: "N",
            FCD_01: "2",
            FCD_02: "N",
            ROADSHOW01: "N",
            trustedIndividuals: {}
          },
          extra: {
            ccy: "SGD",
            channel: "AGENCY",
            channelName: "AGENCY",
            hasTrustedIndividual: null,
            isCompleted: true,
            isPdfGenerated: true,
            isPhSameAsLa: "Y",
            premium: 2560.7
          },
          groupedPlanDetails: {
            awl: 300,
            axaShield: 283,
            basicPlanList: [
              {
                cashPortion: 0,
                covClass: "A",
                covCode: "ASIM",
                covName: {
                  en: "AXA Shield (Plan A)",
                  "zh-Hant": "AXA Shield (Plan A)"
                },
                covNameDesc: {
                  en: "AXA Shield",
                  "zh-Hant": "AXA Shield"
                },
                cpfPortion: 283,
                medisave: 593,
                payFreq: "A",
                payFreqDesc: "Annual",
                paymentMethod: "CPF",
                paymentMethodDesc: "CPF",
                planCode: "ASIMSA",
                planType: "Health Plan",
                planTypeDesc: "Plan A",
                premium: 593,
                productId: "08_product_ASIM_3",
                productLine: "HP",
                sumInsured: " - ",
                tax: {
                  yearTax: 38.8
                },
                version: 3
              }
            ],
            cashPortion: 0,
            ccy: "SGD",
            covClass: "A",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310,
            paymentMode: "A",
            planTypeName: {
              en: "AXA Shield (Plan A)",
              "zh-Hant": "AXA Shield (Plan A)"
            },
            premium: 0,
            riderPlanList: [
              {
                covClass: "A",
                covCode: "ASP",
                covName: {
                  en: "AXA Basic Care (Plan A)",
                  "zh-Hant": "AXA Basic Care (Plan A)"
                },
                covNameDesc: {
                  en: "AXA Basic Care",
                  "zh-Hant": "AXA Basic Care"
                },
                payFreq: "M",
                payFreqDesc: "Monthly",
                paymentMethod: "CASH",
                paymentMethodDesc: "Cash",
                planCode: "ASPA",
                planType: "Health Plan",
                planTypeDesc: "Plan A",
                premium: 37.7,
                productId: "08_product_ASP_3",
                productLine: "HP",
                sumInsured: " - ",
                tax: {
                  monthTax: 2.47,
                  yearTax: 29.64
                },
                version: 3,
                yearPrem: 452.4
              }
            ],
            totYearPrem: 0,
            totalRiderPremium: 37.7
          },
          insurability: {
            HEALTH_SHIELD_01: "N",
            HEALTH_SHIELD_02: "N",
            HEALTH_SHIELD_03: "N",
            HEALTH_SHIELD_04: "N",
            HEALTH_SHIELD_05: "N",
            HEALTH_SHIELD_06: "N",
            HEALTH_SHIELD_07: "N",
            HEALTH_SHIELD_08: "N",
            HEALTH_SHIELD_09: "N",
            HEALTH_SHIELD_10: "N",
            HEALTH_SHIELD_11: "N",
            HEALTH_SHIELD_12: "N",
            HEALTH_SHIELD_13: "N",
            HEALTH_SHIELD_14: "N",
            HEALTH_SHIELD_15: "N",
            HEALTH_SHIELD_16: "N",
            HEALTH_SHIELD_17: "N",
            HEALTH_SHIELD_18: "N",
            HEALTH_SHIELD_19: "N",
            HW01: 1.8,
            HW02: 100,
            HW03: "N",
            INS03: "N",
            INS04: "N",
            INS05: "N",
            LIFESTYLE01: "N",
            LIFESTYLE02: "N",
            insAllNoInd: "Y"
          },
          personalInfo: {
            addrBlock: "2",
            addrCity: "",
            addrCountry: "R2",
            addrEstate: "",
            addrStreet: "2",
            age: 30,
            agentId: "084620",
            allowance: 1000000,
            applicationCount: 1,
            bundle: [
              {
                id: "FN001002-00001",
                isValid: false
              },
              {
                id: "FN001002-00008",
                isValid: false
              },
              {
                id: "FN001002-00229",
                isValid: true
              }
            ],
            cid: "CP001002-00001",
            dependants: [
              {
                cid: "CP001002-00008",
                relationship: "SPO"
              },
              {
                cid: "CP001002-00081",
                relationship: "FAT"
              },
              {
                cid: "CP001002-00082",
                relationship: "MOT"
              },
              {
                cid: "CP001002-00083",
                relationship: "SON"
              },
              {
                cid: "CP001002-00084",
                relationship: "BRO"
              }
            ],
            dob: "1987-12-07",
            education: "below",
            email: "kelvin.wong@eabsystems.com",
            employStatus: "ft",
            firstName: "1207",
            fnaRecordIdArray: "",
            fullName: "1207",
            gender: "M",
            hanyuPinyinName: "",
            hasUT: "N",
            haveSignDoc: true,
            idCardNo: "S1234567D",
            idDocType: "nric",
            industry: "I1",
            initial: "1",
            isSameAddr: "Y",
            isSmoker: "N",
            isValid: true,
            language: "en",
            lastName: "",
            lastUpdateDate: "2018-09-15T12:15:58.744Z",
            marital: "M",
            mobileCountryCode: "+65",
            mobileNo: "61988440",
            nameOrder: "L",
            nationality: "N1",
            nearAge: 31,
            occupation: "O1",
            organization: "2",
            organizationCountry: "R2",
            othName: "",
            otherMobileCountryCode: "+65",
            otherNo: "",
            photo: "",
            postalCode: "2",
            prStatus: "Y",
            referrals: "",
            residenceCountry: "R2",
            title: "Mr",
            type: "cust",
            unitNum: ""
          },
          policies: {
            ROP_01: "N",
            ROP_DECLARATION_01: "N",
            ROP_DECLARATION_02: "N"
          },
          residency: {}
        },
        undefined: {}
      }
    },
    applicationInforceDate: 0,
    applicationSignedDate: "2018-09-17T03:52:43.617Z",
    applicationStartedDate: "2018-09-15T12:16:55.769Z",
    applicationSubmittedDate: 0,
    biSignedDate: "2018-09-17T03:51:01.049Z",
    bundleId: "FN001002-00229",
    childIds: [
      "NB001002-00262",
      "NB001002-00263",
      "NB001002-00264",
      "NB001002-00265",
      "NB001002-00266",
      "NB001002-00267"
    ],
    compCode: "01",
    createDate: "2018-09-15T12:16:55.769Z",
    dealerGroup: "AGENCY",
    iCidMapping: {
      "CP001002-00001": [
        {
          applicationId: "NB001002-00262",
          covCode: "ASIM",
          policyNumber: "301-4648640"
        },
        {
          applicationId: "NB001002-00263",
          covCode: "ASP",
          policyNumber: "301-4648657"
        }
      ],
      "CP001002-00008": [
        {
          applicationId: "NB001002-00264",
          covCode: "ASIM",
          policyNumber: "301-4648665"
        }
      ],
      "CP001002-00081": [
        {
          applicationId: "NB001002-00265",
          covCode: "ASIM",
          policyNumber: "301-4648673"
        }
      ],
      "CP001002-00082": [
        {
          applicationId: "NB001002-00266",
          covCode: "ASIM",
          policyNumber: "301-4648681"
        }
      ],
      "CP001002-00083": [
        {
          applicationId: "NB001002-00267",
          covCode: "ASIM",
          policyNumber: "301-4648699"
        }
      ]
    },
    iCids: [
      "CP001002-00008",
      "CP001002-00081",
      "CP001002-00082",
      "CP001002-00083"
    ],
    id: "SA001002-00262",
    isAppFormInsuredSigned: [true, true, true, true],
    isAppFormProposerSigned: true,
    isCrossAge: false,
    isFailSubmit: false,
    isFullySigned: true,
    isInitialPaymentCompleted: false,
    isMandDocsAllUploaded: false,
    isPolicyNumberGenerated: true,
    isProposalSigned: true,
    isStartSignature: true,
    isSubmittedStatus: false,
    isValid: true,
    lastUpdateDate: "2018-09-17T06:02:19.438Z",
    pCid: "CP001002-00001",
    payment: {
      cpfMedisaveAccDetails: [
        {
          cpfAccHolderName: "1207",
          cpfAccNo: "S1234567D"
        }
      ],
      initPayMethod: "",
      isCompleted: false,
      premiumDetails: [
        {
          applicationId: "NB001002-00262",
          cashPortion: 0,
          cid: "CP001002-00001",
          covName: {
            en: "AXA Shield (Plan A)",
            "zh-Hant": "AXA Shield (Plan A)"
          },
          cpfPortion: 283,
          laName: "1207",
          medisave: 593,
          payFrequency: "A",
          policyNumber: "301-4648640",
          subseqPayMethod: "N"
        },
        {
          applicationId: "NB001002-00263",
          cashPortion: 75.4,
          cid: "CP001002-00001",
          covName: {
            en: "AXA Basic Care (Plan A)",
            "zh-Hant": "AXA Basic Care (Plan A)"
          },
          cpfPortion: 0,
          laName: "1207",
          medisave: 0,
          payFrequency: "M",
          policyNumber: "301-4648657",
          subseqPayMethod: "Y"
        },
        {
          applicationId: "NB001002-00264",
          cashPortion: 0,
          cid: "CP001002-00008",
          covName: {
            en: "AXA Shield (Plan A)",
            "zh-Hant": "AXA Shield (Plan A)"
          },
          cpfPortion: 283,
          laName: "222",
          medisave: 593,
          payFrequency: "A",
          policyNumber: "301-4648665",
          subseqPayMethod: "N"
        },
        {
          applicationId: "NB001002-00265",
          cashPortion: 0,
          cid: "CP001002-00081",
          covName: {
            en: "AXA Shield (Plan A)",
            "zh-Hant": "AXA Shield (Plan A)"
          },
          cpfPortion: 283,
          laName: "1207father",
          medisave: 593,
          payFrequency: "A",
          policyNumber: "301-4648673",
          subseqPayMethod: "N"
        },
        {
          applicationId: "NB001002-00266",
          cashPortion: 0,
          cid: "CP001002-00082",
          covName: {
            en: "AXA Shield (Standard Plan)",
            "zh-Hant": "AXA Shield (Standard Plan)"
          },
          cpfPortion: 62,
          laName: "1207mother",
          medisave: 372,
          payFrequency: "A",
          policyNumber: "301-4648681",
          subseqPayMethod: "N"
        },
        {
          applicationId: "NB001002-00267",
          cashPortion: 0,
          cid: "CP001002-00083",
          covName: {
            en: "AXA Shield (Standard Plan)",
            "zh-Hant": "AXA Shield (Standard Plan)"
          },
          cpfPortion: 62,
          laName: "1207son",
          medisave: 372,
          payFrequency: "A",
          policyNumber: "301-4648699",
          subseqPayMethod: "N"
        }
      ],
      totCPFPortion: 973,
      totCashPortion: 75.4,
      totMedisave: 2523
    },
    quotation: {
      agent: {
        agentCode: "084620",
        company: "AXA Insurance Pte Ltd",
        dealerGroup: "AGENCY",
        email: "eposapi.axa.sg@eabsystems.com",
        mobile: "00000000",
        name: "John Huynh",
        tel: "0"
      },
      agentCode: "084620",
      baseProductCode: "ASIM",
      baseProductId: "08_product_ASIM_3",
      baseProductName: {
        en: "AXA Shield",
        "zh-Hant": "AXA Shield"
      },
      budgetRules: ["RP"],
      bundleId: "FN001002-00229",
      clientChoice: {
        isClientChoiceSelected: true,
        recommendation: {
          benefit:
            "1207\nAXA Shield Plan A with AXA Basic Care: 1) Reimburses eligible expenses in private hospital standard rooms 2) 365 days of post-hospitalisation coverage 3) Covers Deductibles and Co-Insurance.  You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.\n\n222, 1207father\nAXA Shield Plan A: 1) Reimburses eligible expenses in private hospital standard rooms 2) 365 days of post-hospitalisation coverage.  You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.\n\n1207mother, 1207son\nAXA Shield Standard Plan: 1) Reimburses eligible expenses in class B1  wards and below in restructured (public) hospitals.  You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.",
          limitation:
            "1207\nAXA Shield Plan A with AXA Basic Care: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated under the additional benefits of your AXA Shield Policy). 5) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy.\n\nThis plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date.\n\nYou have bought the Basic Care Rider (no copay).\nIn line with the Ministry of Health guidelines issued on 8 Mar 2018, you will have to transit to a rider with co-payment of 5% (or more) from 1 April 2021 upon policy renewal. You also have the option to transit to a rider with co-payment earlier when it is available from 1 April 2019 upon policy renewal.\n\n222, 1207father\nAXA Shield Plan A: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover Deductibles and Co-Insurance 5) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated under the additional benefits of your AXA Shield Policy). 6) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy.\n\nThis plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date. \n\n1207mother, 1207son\nAXA Shield Stardard Plan: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover Deductibles and Co-Insurance 5) Does not cover medical treatment or Hospitalization outside of Singapore 6) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy.\n\nThis plan does not cover Treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth.",
          reason: "123",
          rop: {
            choiceQ1: "",
            choiceQ1Sub1: "",
            choiceQ1Sub2: "",
            choiceQ1Sub3: "",
            existCi: 0,
            existLife: 0,
            existPaAdb: 0,
            existTotalPrem: 0,
            existTpd: 0,
            replaceCi: 0,
            replaceLife: 0,
            replacePaAdb: 0,
            replaceTotalPrem: 0,
            replaceTpd: 0
          },
          rop_shield: {
            ropBlock: {
              iCidRopAnswerMap: {
                "CP001002-00001": "shieldRopAnswer_0",
                "CP001002-00008": "shieldRopAnswer_1",
                "CP001002-00081": "shieldRopAnswer_2",
                "CP001002-00082": "shieldRopAnswer_3",
                "CP001002-00083": "shieldRopAnswer_4"
              },
              ropQ1sub3: "",
              ropQ2: "",
              ropQ3: "",
              shieldRopAnswer_0: "N",
              shieldRopAnswer_1: "N",
              shieldRopAnswer_2: "N",
              shieldRopAnswer_3: "N",
              shieldRopAnswer_4: "N",
              shieldRopAnswer_5: ""
            }
          }
        }
      },
      compCode: "01",
      createDate: "2018-09-15T12:11:56.077Z",
      dealerGroup: "AGENCY",
      extraFlags: {},
      id: "QU001002-00337",
      insureds: {
        "CP001002-00001": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "eposapi.axa.sg@eabsystems.com",
            mobile: "00000000",
            name: "John Huynh",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001002-00001",
          iDob: "1987-12-07",
          iEmail: "kelvin.wong@eabsystems.com",
          iFirstName: "1207",
          iFullName: "1207",
          iGender: "M",
          iLastName: "",
          iOccupation: "O1",
          iOccupationClass: "4",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001002-00001",
          pDob: "1987-12-07",
          pEmail: "kelvin.wong@eabsystems.com",
          pFirstName: "1207",
          pFullName: "1207",
          pGender: "M",
          pLastName: "",
          pOccupation: "O1",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "N",
          paymentMode: "A",
          plans: [
            {
              covClass: "A",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSA",
              premium: 593,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 38.8
              },
              version: 3
            },
            {
              covClass: "A",
              covCode: "ASP",
              covName: {
                en: "AXA Basic Care (Plan A)",
                "zh-Hant": "AXA Basic Care (Plan A)"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASPA",
              premium: 37.7,
              productId: "08_product_ASP_3",
              productLine: "HP",
              tax: {
                monthTax: 2.47,
                yearTax: 29.64
              },
              version: 3,
              yearPrem: 452.4
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 283,
            cashPortion: 0,
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 283,
            cashPortion: "-",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "Y",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 37.7,
          totRiderYearPrem: 452.4,
          totYearCashPortion: 452.4,
          totYearPrem: 0,
          type: "quotation"
        },
        "CP001002-00008": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "eposapi.axa.sg@eabsystems.com",
            mobile: "00000000",
            name: "John Huynh",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001002-00008",
          iDob: "1987-12-20",
          iEmail: "",
          iFirstName: "222",
          iFullName: "222",
          iGender: "F",
          iLastName: "",
          iOccupation: "O2",
          iOccupationClass: "DCL",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001002-00001",
          pDob: "1987-12-07",
          pEmail: "kelvin.wong@eabsystems.com",
          pFirstName: "1207",
          pFullName: "1207",
          pGender: "M",
          pLastName: "",
          pOccupation: "O1",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "N",
          paymentMode: "A",
          plans: [
            {
              covClass: "A",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSA",
              premium: 593,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 38.8
              },
              version: 3
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 283,
            cashPortion: 0,
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 283,
            cashPortion: "-",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "N",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 0,
          totRiderYearPrem: 0,
          totYearCashPortion: 0,
          totYearPrem: 0,
          type: "quotation"
        },
        "CP001002-00081": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "eposapi.axa.sg@eabsystems.com",
            mobile: "00000000",
            name: "John Huynh",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001002-00081",
          iDob: "1988-09-15",
          iEmail: "",
          iFirstName: "1207father",
          iFullName: "1207father",
          iGender: "M",
          iLastName: "",
          iOccupation: "O2",
          iOccupationClass: "DCL",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001002-00001",
          pDob: "1987-12-07",
          pEmail: "kelvin.wong@eabsystems.com",
          pFirstName: "1207",
          pFullName: "1207",
          pGender: "M",
          pLastName: "",
          pOccupation: "O1",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "N",
          paymentMode: "A",
          plans: [
            {
              covClass: "A",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSA",
              premium: 593,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 38.8
              },
              version: 3
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 283,
            cashPortion: 0,
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 283,
            cashPortion: "-",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "N",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 0,
          totRiderYearPrem: 0,
          totYearCashPortion: 0,
          totYearPrem: 0,
          type: "quotation"
        },
        "CP001002-00082": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "eposapi.axa.sg@eabsystems.com",
            mobile: "00000000",
            name: "John Huynh",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001002-00082",
          iDob: "1988-09-15",
          iEmail: "",
          iFirstName: "1207mother",
          iFullName: "1207mother",
          iGender: "F",
          iLastName: "",
          iOccupation: "O2",
          iOccupationClass: "DCL",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001002-00001",
          pDob: "1987-12-07",
          pEmail: "kelvin.wong@eabsystems.com",
          pFirstName: "1207",
          pFullName: "1207",
          pGender: "M",
          pLastName: "",
          pOccupation: "O1",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "N",
          paymentMode: "A",
          plans: [
            {
              covClass: "C",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Standard Plan)",
                "zh-Hant": "AXA Shield (Standard Plan)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSC",
              premium: 372,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 24.34
              },
              version: 3
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 62,
            cashPortion: 0,
            cpfPortion: 62,
            medisave: 372,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 62,
            cashPortion: "-",
            cpfPortion: 62,
            medisave: 372,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "N",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 0,
          totRiderYearPrem: 0,
          totYearCashPortion: 0,
          totYearPrem: 0,
          type: "quotation"
        },
        "CP001002-00083": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "eposapi.axa.sg@eabsystems.com",
            mobile: "00000000",
            name: "John Huynh",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001002-00083",
          iDob: "1988-09-15",
          iEmail: "",
          iFirstName: "1207son",
          iFullName: "1207son",
          iGender: "M",
          iLastName: "",
          iOccupation: "O2",
          iOccupationClass: "DCL",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001002-00001",
          pDob: "1987-12-07",
          pEmail: "kelvin.wong@eabsystems.com",
          pFirstName: "1207",
          pFullName: "1207",
          pGender: "M",
          pLastName: "",
          pOccupation: "O1",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "N",
          paymentMode: "A",
          plans: [
            {
              covClass: "C",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Standard Plan)",
                "zh-Hant": "AXA Shield (Standard Plan)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSC",
              premium: 372,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 24.34
              },
              version: 3
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 62,
            cashPortion: 0,
            cpfPortion: 62,
            medisave: 372,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 62,
            cashPortion: "-",
            cpfPortion: 62,
            medisave: 372,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "N",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 0,
          totRiderYearPrem: 0,
          totYearCashPortion: 0,
          totYearPrem: 0,
          type: "quotation"
        }
      },
      isBackDate: "N",
      lastUpdateDate: "2018-09-15T12:14:47.895Z",
      pAge: 31,
      pCid: "CP001002-00001",
      pDob: "1987-12-07",
      pEmail: "kelvin.wong@eabsystems.com",
      pFirstName: "1207",
      pFullName: "1207",
      pGender: "M",
      pLastName: "",
      pOccupation: "O1",
      pOccupationClass: "4",
      pResidence: "R2",
      pSmoke: "N",
      premium: 2560.7,
      productLine: "HP",
      productVersion: "2.0",
      quotType: "SHIELD",
      riskCommenDate: "2018-09-15",
      totCashPortion: 37.7,
      totMedisave: 2523,
      totPremium: 2560.7,
      totYearCashPortion: 452.4,
      totYearPrem: 2975.4,
      type: "quotation"
    },
    quotationDocId: "QU001002-00337",
    signExpiryShown: false,
    submission: {},
    supportDocuments: {
      values: {
        "CP001002-00001": {
          mandDocs: {
            iNric: [],
            iReentryPermit: []
          },
          optDoc: {
            iDischargeSummary: [],
            iJuvenileQuestions: [],
            iMedicalReport: [],
            iOtherIllnessQues: []
          }
        },
        "CP001002-00008": {
          mandDocs: {
            iNric: [],
            iReentryPermit: []
          },
          optDoc: {
            iDischargeSummary: [],
            iJuvenileQuestions: [],
            iMedicalReport: [],
            iOtherIllnessQues: []
          }
        },
        "CP001002-00081": {
          mandDocs: {
            iNric: [],
            iReentryPermit: []
          },
          optDoc: {
            iDischargeSummary: [],
            iJuvenileQuestions: [],
            iMedicalReport: [],
            iOtherIllnessQues: []
          }
        },
        "CP001002-00082": {
          mandDocs: {
            iNric: [],
            iReentryPermit: []
          },
          optDoc: {
            iDischargeSummary: [],
            iJuvenileQuestions: [],
            iMedicalReport: [],
            iOtherIllnessQues: []
          }
        },
        "CP001002-00083": {
          mandDocs: {
            iNric: [],
            iReentryPermit: []
          },
          optDoc: {
            iDischargeSummary: [],
            iJuvenileQuestions: [],
            iMedicalReport: [],
            iOtherIllnessQues: []
          }
        },
        policyForm: {
          mandDocs: {
            axasam: [],
            cAck: [],
            cash: [],
            chequeCashierOrder: [],
            healthDeclaration: [],
            pAddrProof: [],
            pNric: [],
            pPass: [],
            pPassport: [],
            pPassportWStamp: [],
            teleTransfer: [],
            thirdPartyAddrProof: [],
            thirdPartyID: []
          },
          optDoc: {
            axasam: [],
            cash: [],
            chequeCashierOrder: [],
            pChinaVisit: [],
            teleTransfer: []
          },
          sysDocs: {
            appPdf: {
              fileSize: "",
              fileType: "application/pdf",
              id: "appPdf",
              title: "e-App (1207)"
            },
            "appPdfCP001002-00008": {
              fileSize: "",
              fileType: "application/pdf",
              id: "appPdfCP001002-00008",
              title: "e-App (222)"
            },
            "appPdfCP001002-00081": {
              fileSize: "",
              fileType: "application/pdf",
              id: "appPdfCP001002-00081",
              title: "e-App (1207father)"
            },
            "appPdfCP001002-00082": {
              fileSize: "",
              fileType: "application/pdf",
              id: "appPdfCP001002-00082",
              title: "e-App (1207mother)"
            },
            "appPdfCP001002-00083": {
              fileSize: "",
              fileType: "application/pdf",
              id: "appPdfCP001002-00083",
              title: "e-App (1207son)"
            },
            eapproval_supervisor_pdf: {
              fileSize: "",
              fileType: "application/pdf",
              id: "eapproval_supervisor_pdf",
              title: "Supervisor Validation"
            },
            faFirm_Comment: {
              fileSize: "",
              fileType: "application/pdf",
              id: "faFirm_Comment",
              title: "Comments by FA Firm Admin"
            },
            fnaReport: {
              fileSize: "",
              fileType: "application/pdf",
              id: "fnaReport",
              title: "e-FNA"
            },
            proposal: {
              fileSize: "",
              fileType: "application/pdf",
              id: "proposal",
              title: "Product Summary"
            }
          }
        },
        proposer: {
          mandDocs: {},
          optDoc: {
            pDischargeSummary: [],
            pMedicalReport: [],
            pOtherIllnessQues: []
          }
        }
      },
      viewedList: {
        "084620": {
          appPdf: false,
          "appPdfCP001002-00008": false,
          "appPdfCP001002-00081": false,
          "appPdfCP001002-00082": false,
          "appPdfCP001002-00083": false,
          fnaReport: false,
          proposal: false
        }
      }
    },
    type: "masterApplication"
  },
  signature: {}
};
