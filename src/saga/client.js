import { call, put, select } from "redux-saga/effects";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { CLIENT, FNA, CONFIG } from "../constants/REDUCER_TYPES";

import * as invalidation from "./invalidation";

export function* getProfile(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { cid } = action;

    const profileResponse = yield call(callServer, {
      url: `client`,
      data: {
        action: "getProfile",
        docId: cid
      }
    });
    const dependantProfilesResponse = yield call(callServer, {
      url: `client`,
      data: {
        action: "getAllDependantsProfile",
        cid
      }
    });

    const signExpiryResponse = yield call(invalidation.signatureExpiry, {
      cid
    });

    if (signExpiryResponse.success) {
      // TODO
    }

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_CLIENT_AND_DEPENDANTS,
      profileData: profileResponse.profile,
      dependantProfilesData: dependantProfilesResponse.dependantProfiles
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID,
      newIsFnaInvalid: !!profileResponse.showFnaInvalidFlag
    });

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO
  }
}

export function* getContactList(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);

    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const response = yield call(callServer, {
      url: `client`,
      data: {
        action: "getContactList"
      }
    });

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_CONTACT_LIST,
      contactListData: response.contactList
    });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO
  }
}

export function* updateProfile(action) {
  try {
    const { profileList } = action;
    const profile = yield select(state => state[CLIENT].profile);
    const dependantProfiles = yield select(
      state => state[CLIENT].dependantProfiles
    );

    let newProfile = _.cloneDeep(profile);
    const newDependantProfiles = _.cloneDeep(dependantProfiles);

    _.forEach(profileList, p => {
      const cid = _.get(p, "cid");
      if (cid) {
        if (cid === newProfile.cid) {
          newProfile = p;
        } else if (!_.isEmpty(dependantProfiles) && dependantProfiles[cid]) {
          newDependantProfiles[cid] = p;
        }
      }
    });

    yield put({
      type: ACTION_TYPES[CLIENT].UPDATE_CLIENT_AND_DEPENDANTS,
      profileData: newProfile,
      dependantProfilesData: newDependantProfiles
    });
  } catch (e) {
    // TODO
  }
}
