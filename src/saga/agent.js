import { call, select, put } from "redux-saga/effects";
import * as _ from "lodash";
import { CONFIG, AGENT } from "../constants/REDUCER_TYPES";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* updateAgentProfileAvatar(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { imageType, base64, callback } = action;

    yield call(callServer, {
      url: `agent`,
      data: {
        action: "updateProfilePic",
        type: imageType,
        data: base64
      }
    });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    // TODO
  }
}

export function* assignEligProductsToAgent(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const agent = yield select(state => state[AGENT]);
    const { callback } = action;
    const response = yield call(callServer, {
      url: `agent`,
      data: {
        action: "assignEligProductsToAgent"
      }
    });

    if (response.success) {
      const newAgent = _.cloneDeep(agent);
      newAgent.eligProducts = response.agent.eligProducts;
      yield put({
        type: ACTION_TYPES[AGENT].UPDATE_AGENT_DATA,
        data: newAgent
      });
    }

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    // TODO
  }
}
