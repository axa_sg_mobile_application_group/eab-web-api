import { select, put, call } from "redux-saga/effects";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import SECTION_KEYS from "../constants/SECTION_KEYS";
import EAPP from "../constants/EAPP";
import { DIALOG_NEW_RECORD } from "../constants/DYNAMIC_VIEWS";
import {
  CONFIG,
  PRE_APPLICATION,
  APPLICATION,
  PAYMENT_AND_SUBMISSION,
  SIGNATURE,
  PAYMENT,
  SUBMISSION,
  FNA,
  CLIENT
} from "../constants/REDUCER_TYPES";
import { TEXT_SELECTION, TEXT_FIELD } from "../constants/FIELD_TYPES";
import * as validation from "../utilities/validation";
import {
  initValidateApplicationFormError,
  calculateRopTable,
  updateFieldValue
} from "../utilities/dynamicApplicationForm";

import {
  initApplicationFormPersonalDetailsValue,
  initValidateApplicationPersonalDetailsForm,
  validateApplicationFormPersonalDetails,
  checkApplicationFormHasError,
  checkSignatureHasError,
  updateApplicationFormCompletedMenus,
  updateProfileList
} from "../utilities/application";
import * as paymentAndSubmission from "./paymentAndSubmission";
import * as signature from "./signature";
import * as client from "./client";

const {
  SWITCHMENU,
  SWITCHTAB,
  OPENSUPPORTINGDOCUMENT,
  CLOSEAPPLICATION,
  NEXTPAGE,
  GOFNA
} = EAPP.action;
const { PLAN_DETAILS } = SECTION_KEYS[APPLICATION];

export function* applyApplicationFormNormal(action) {
  try {
    yield put({
      type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
    });
    const callServer = yield select(state => state[CONFIG].callServer);
    const applicationFormError = {};
    const { callback } = action;

    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "apply",
        id: action.quotationId
      }
    });

    const newApplication = initApplicationFormPersonalDetailsValue({
      application: resp.application,
      template: resp.template
    });
    yield put({
      type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
      newTemplate: resp.template,
      newApplication
    });
    // init validate state when enters the application form
    initValidateApplicationPersonalDetailsForm({
      application: newApplication,
      errorObj: applicationFormError
    });
    applicationFormError.applicationForm = {};
    initValidateApplicationFormError({
      template: resp.template,
      rootValues: newApplication,
      errorObj: applicationFormError
    });
    // update completedMenus
    _.forEach(_.get(newApplication, "applicationForm.values.menus"), menu => {
      if (menu !== PLAN_DETAILS) {
        updateApplicationFormCompletedMenus({
          template: resp.template,
          application: newApplication,
          error: applicationFormError,
          sectionKey: menu,
          isInit: true
        });
      }
    });
    yield put({
      type:
        ACTION_TYPES[APPLICATION]
          .INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
      errorObj: applicationFormError
    });
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
      newCurrentStep: resp.application.appStep
    });
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* continueApplicationForm(action) {
  try {
    if (action.cleanData) {
      yield put({
        type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
      });
    }
    const callServer = yield select(state => state[CONFIG].callServer);
    const applicationFormError = {};
    const { data, currentStep, callback } = action;
    let resp = data;
    if (!resp) {
      resp = yield call(callServer, {
        url: "application",
        data: {
          method: "post",
          action: "goApplication",
          id: action.applicationId
        }
      });
    }
    const newCurrentStep = currentStep || resp.application.appStep;
    let newCompletedStep = -1;
    const newApplication = initApplicationFormPersonalDetailsValue({
      application: resp.application,
      template: resp.template
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
      newTemplate: resp.template,
      newApplication
    });
    // init validate state when enters the application form
    initValidateApplicationPersonalDetailsForm({
      application: newApplication,
      errorObj: applicationFormError
    });
    applicationFormError.applicationForm = {};
    initValidateApplicationFormError({
      template: resp.template,
      rootValues: newApplication,
      errorObj: applicationFormError
    });

    // update completedMenus
    _.forEach(_.get(newApplication, "applicationForm.values.menus"), menu => {
      if (menu !== PLAN_DETAILS) {
        updateApplicationFormCompletedMenus({
          template: resp.template,
          application: newApplication,
          error: applicationFormError,
          sectionKey: menu,
          isInit: true
        });
      }
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: applicationFormError
      })
    ) {
      newCompletedStep += 1;
      if (!checkSignatureHasError(newApplication)) {
        newCompletedStep += 1;
      }
    }

    yield put({
      type:
        ACTION_TYPES[APPLICATION]
          .INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
      errorObj: applicationFormError
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
      newCurrentStep,
      newCompletedStep
    });

    if (
      [
        _.get(resp.crossAge, "insuredStatus"),
        _.get(resp.crossAge, "proposerStatus")
      ].some(s => s === EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED)
    ) {
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_CROSS_AGE,
        newCrossAge: resp.crossAge
      });

      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
        newIsAlertDelayed: newCurrentStep === EAPP.tabStep[SIGNATURE],
        newIsCrossAgeAlertShow: true
      });
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
      newIsAlertDelayed: newCurrentStep === EAPP.tabStep[SIGNATURE],
      newIsSignatureExpiryAlertShow: !!resp.showSignExpiryWarning
    });

    if (_.isFunction(callback)) {
      callback({
        currentStep: newCurrentStep
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateApplicationFormPersonalDetails(action) {
  try {
    const { newApplication, validateObj, triggerValidate, callback } = action;
    const template = yield select(state => state[APPLICATION].template);
    const error = yield select(state => state[APPLICATION].error);
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const selectedSectionKey = yield select(
      state => state[APPLICATION].component.selectedSectionKey
    );

    const newError = _.cloneDeep(error);
    let newCompletedStep = completedStep;

    validateApplicationFormPersonalDetails({
      validateObj,
      errorObj: newError
    });

    // trigger other validation
    _.forEach(triggerValidate, validate => {
      validateApplicationFormPersonalDetails({
        validateObj: validate,
        errorObj: newError
      });
    });

    // update completedMenus
    updateApplicationFormCompletedMenus({
      template,
      application: newApplication,
      error: newError,
      sectionKey: selectedSectionKey
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: newError
      })
    ) {
      newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
    } else {
      newCompletedStep = -1;
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
      newApplication
    });

    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newError,
      newCompletedStep
    });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePersonalDetailsAddress(action) {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const callServer = yield select(state => state[CONFIG].callServer);
    const applicationFormError = yield select(
      state => state[APPLICATION].error
    );
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const { fileName, postalCode, fieldType, targetProfile, callback } = action;
    const newApplication = _.cloneDeep(application);
    const newApplicationFormError = _.cloneDeep(applicationFormError);
    const response = yield call(callServer, {
      url: "client",
      data: {
        method: "post",
        action: "getAddressByPostalCode",
        pC: postalCode,
        fileName
      }
    });

    let list;
    if (fieldType === "mailingAddress") {
      list = {
        valueUpdateList: [
          { fieldKey: "mPostalCode", responsekey: "postalCode" },
          { fieldKey: "mAddrBlock", responsekey: "BLDGNO" },
          { fieldKey: "mAddrStreet", responsekey: "STREETNAME" },
          { fieldKey: "mAddrEstate", responsekey: "BLDGNAME" }
        ],
        errorUpdateList: [
          { fieldKey: "mPostalCode", responsekey: "postalCode" },
          { fieldKey: "mAddrBlock", responsekey: "BLDGNO" },
          { fieldKey: "mAddrStreet", responsekey: "STREETNAME" }
        ]
      };
    } else {
      list = {
        valueUpdateList: [
          { fieldKey: "postalCode", responsekey: "postalCode" },
          { fieldKey: "addrBlock", responsekey: "BLDGNO" },
          { fieldKey: "addrStreet", responsekey: "STREETNAME" },
          { fieldKey: "addrEstate", responsekey: "BLDGNAME" }
        ],
        errorUpdateList: [
          { fieldKey: "postalCode", responsekey: "postalCode" },
          { fieldKey: "addrBlock", responsekey: "BLDGNO" },
          { fieldKey: "addrStreet", responsekey: "STREETNAME" }
        ]
      };
    }

    _.each(list.valueUpdateList, value => {
      const newValue =
        value.responsekey === "postalCode"
          ? postalCode
          : response[value.responsekey] || "";
      _.set(
        newApplication,
        `applicationForm.values.${targetProfile}.personalInfo.${
          value.fieldKey
        }`,
        newValue
      );
    });

    _.each(list.errorUpdateList, value => {
      const newValue =
        value.responsekey === "postalCode"
          ? postalCode
          : response[value.responsekey] || "";
      _.set(
        newApplicationFormError,
        `application.${targetProfile}.personalInfo.${value.fieldKey}`,
        validation.validateMandatory({
          field: {
            type: TEXT_FIELD,
            mandatory: true,
            disabled: false
          },
          value: newValue
        })
      );
    });
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
      newApplication
    });
    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newApplicationFormError,
      newCompletedStep: completedStep
    });

    if (!response.success && _.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveApplicationForm(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const newApplication = _.cloneDeep(application);

    const { clientId, extra, genPDF, policyNumber, callback } = action;

    const response = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "saveForm",
        applicationId: application.id,
        clientId: clientId === "" ? null : application.pCid,
        values: application.applicationForm.values,
        policyNumber,
        genPDF,
        extra
      }
    });
    if (response.success) {
      if (response.policyNumber) {
        newApplication.policyNumber = policyNumber;
        yield put({
          type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
          newApplication
        });
      }

      if (response.updIds) {
        const profileList = updateProfileList({
          updIds: response.updIds,
          application: newApplication
        });

        if (profileList.length > 0) {
          yield client.updateProfile({ profileList });
        }
      }
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
  return null;
}

export function* saveApplicationFormBefore(action) {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const policyNumber = yield select(
      state => state[APPLICATION].application.policyNumber
    );

    // keep
    // const selectedSectionKey = yield select(
    //   state => state[APPLICATION].component.selectedSectionKey
    // );
    const { currentStep, nextStep, actionType, callback } = action;
    const { genPDF } = action;
    let clientId =
      currentStep === EAPP.tabStep[APPLICATION] ? application.pCid : "";
    const extra = {};
    let exit = false;
    switch (actionType) {
      case SWITCHMENU:
        // TODO trigger save form only while personal form switch to others
        clientId = application.pCid;
        break;
      case SWITCHTAB:
        if (
          currentStep === EAPP.tabStep[APPLICATION] &&
          nextStep === EAPP.tabStep[SIGNATURE]
        ) {
          extra.formCompleted = true;
        } else if (currentStep === EAPP.tabStep[PAYMENT]) {
          yield call(paymentAndSubmission.updatePaymentMethods, {
            docId: application.id
          });
        } else {
          exit = true;
        }
        break;
      case OPENSUPPORTINGDOCUMENT:
        if (
          currentStep === EAPP.tabStep[SIGNATURE] ||
          currentStep === EAPP.tabStep[PAYMENT]
        ) {
          exit = true;
        }
        break;
      case CLOSEAPPLICATION:
        if (currentStep === EAPP.tabStep[APPLICATION]) {
          extra.resetAppStep = false;
        } else {
          exit = true;
        }
        break;
      case NEXTPAGE:
        if (currentStep === EAPP.tabStep[APPLICATION]) {
          extra.formCompleted = true;
        } else if (currentStep === EAPP.tabStep[SIGNATURE]) {
          exit = true;
        } else if (currentStep === EAPP.tabStep[PAYMENT]) {
          yield call(paymentAndSubmission.updatePaymentMethods, {
            docId: application.id
          });
        }
        break;
      case GOFNA:
        clientId = application.pCid;
        break;
      default:
        break;
    }
    // TODO: Need to retest this scenario if use this code: click into Insurability page, then click to Personal Details page, crashes.
    // if (selectedSectionKey === SECTION_KEYS[APPLICATION].INSURABILITY) {
    //   // TODO if (Declaration) && (Insurability Info) do not exist; true
    //   extra.toGetPolicyNumber = true;
    // }
    if (policyNumber) {
      extra.toGetPolicyNumber = true;
    }
    if (!exit) {
      yield call(saveApplicationForm, {
        extra,
        clientId,
        genPDF,
        policyNumber,
        callback
      });
    }

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* closeDialogAndGetApplicationList(action) {
  try {
    yield put({
      type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
    });

    yield put({
      type: ACTION_TYPES[PRE_APPLICATION].SAGA_GET_APPLICATIONS
    });

    if (_.isFunction(action.callback)) {
      yield call(action.callback);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* redirectToFNA(action) {
  try {
    const { page, callback } = action;
    yield put({
      type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
    });
    yield put({
      type: ACTION_TYPES[FNA].REDIRECT_TO,
      newPage: page
    });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateEappStep(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);

    // keep
    // const signature = yield select(state => state[SIGNATURE].signature);
    const currentStep = yield select(
      state => state[APPLICATION].component.currentStep
    );
    const { nextStep, isShield, callback } = action;
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
      newCurrentStep: nextStep
    });

    if (
      !isShield &&
      currentStep === EAPP.tabStep[SIGNATURE] &&
      nextStep === EAPP.tabStep[PAYMENT] &&
      application.isFullySigned
    ) {
      const resp = yield call(callServer, {
        url: "application",
        data: {
          method: "post",
          action: "updateAppStep",
          docId: application.id,
          stepperIndex: currentStep
        }
      });
      if (resp.success) {
        yield put({
          type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
          newCurrentStep: nextStep
        });
      }
    }

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
  return null;
}

// getApplication the application object
export function* getApplication(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { docId } = action;

    const getApplicationResponse = yield call(callServer, {
      url: `application`,
      data: {
        action: "goApplication",
        id: docId
      }
    });
    let getApplicationResult = {};
    if (getApplicationResponse.success) {
      getApplicationResult = {
        hasError: false,
        result: getApplicationResponse
      };
    } else {
      getApplicationResult = {
        hasError: true,
        errorMsg: "Cannot get the application"
      };
      // yield put({
      //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
      //   errorMsg: getApplicationResponse.id
      // });
    }
    action.callback(getApplicationResult);
  } catch (e) {
    /* TODO need error handling */
    const errorMsg = e;
    const getApplicationResult = {
      hasError: true,
      errorMsg: `Catch Error${errorMsg}`
    };
    action.callback(getApplicationResult);
  }
}

// validatePayment: Checks are performed before data sync when payment button is pressed.
export function* validatePayment(action) {
  console.log("[Saga]-[validatePayment]-[Start]");
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { docId } = action;

    const validatePaymentResponse = yield call(callServer, {
      url: `application`,
      data: {
        action: "goApplication",
        id: docId
      }
    });
    console.log(`[Saga]-[validatePaymentResponse]-${validatePaymentResponse}`);
    console.log(validatePaymentResponse);
    let isPaymentAllowed = false;
    let errorMsg = "";
    let validatePaymentResult = {};
    if (validatePaymentResponse.success) {
      if (
        validatePaymentResponse.application.type === "application" ||
        validatePaymentResponse.application.type === "masterApplication"
      ) {
        if (validatePaymentResponse.application.payment) {
          const initPayMethodCheck =
            validatePaymentResponse.application.payment.initPayMethod;
          const trxStatusCheck =
            validatePaymentResponse.application.payment.trxStatus;
          const trxTimeCheck =
            validatePaymentResponse.application.payment.trxTime;
          if (
            (initPayMethodCheck === "crCard" ||
              initPayMethodCheck === "eNets" ||
              initPayMethodCheck === "dbsCrCardlpp") &&
            trxStatusCheck === "Y" &&
            trxTimeCheck > 0
          ) {
            isPaymentAllowed = false;
            errorMsg = "Payment is performed already";
          } else if (initPayMethodCheck && initPayMethodCheck.length > 0) {
            isPaymentAllowed = false;
            errorMsg = "Payment is performed already";
          } else {
            // Can perform payment la
            isPaymentAllowed = true;
          }
        } else {
          // Error: no payment object in the application object
          isPaymentAllowed = false;
          errorMsg = "No payment object in the application object";
        }
      } else {
        // Error: document type is not application or masterApplication
        isPaymentAllowed = false;
        errorMsg = "document type is not application or masterApplication";
      }
    } else {
      isPaymentAllowed = false;
      errorMsg = "Fail to get Application Object";
      // yield put({
      //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
      //   errorMsg
      // });
    }
    // ToDO: In order to testing, set isPaymentAllowed = true;
    isPaymentAllowed = true;
    if (isPaymentAllowed) {
      // can do payment
      validatePaymentResult = {
        hasError: false,
        result: validatePaymentResponse
      };
    } else {
      // payment is not allowed to do
      validatePaymentResult = {
        hasError: true,
        errorMsg
      };
    }
    // callback
    action.callback(validatePaymentResult);
  } catch (e) {
    /* TODO need error handling */
    const errorMsg = e;
    const validatePaymentResult = {
      hasError: true,
      errorMsg: `Catch Error${errorMsg}`
    };
    action.callback(validatePaymentResult);
  }
}

// validateSubmission: Checks are performed before data sync when submission button is pressed.
export function* validateSubmission(action) {
  console.log("[Saga]-[validateSubmission]-[Start]");
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { docId } = action;

    const validateSubmissionResponse = yield call(callServer, {
      url: `application`,
      data: {
        action: "goApplication",
        id: docId
      }
    });
    console.log(
      `[Saga]-[validateSubmissionResponse]-${validateSubmissionResponse}`
    );
    console.log(validateSubmissionResponse);
    let isSubmissionAllowed = false;
    let errorMsg = "";
    let validateSubmissionResult = {};
    if (validateSubmissionResponse.success) {
      const isSubmittedStatusCheck =
        validateSubmissionResponse.application.isSubmittedStatus;
      const isMandDocsAllUploadedCheck =
        validateSubmissionResponse.application.isMandDocsAllUploaded;
      if (isSubmittedStatusCheck) {
        isSubmissionAllowed = false;
        errorMsg = "Submission is performed.";
      } else if (isMandDocsAllUploadedCheck) {
        isSubmissionAllowed = true;
      } else {
        isSubmissionAllowed = false;
        errorMsg =
          "Mand. Documents are not all uploaded, please upload ALL before submission";
      }
    } else {
      isSubmissionAllowed = false;
      errorMsg = "Fail to get Application Object";
      // yield put({
      //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
      //   errorMsg: validateSubmissionResult.id
      // });
    }
    // ToDO: In order to testing, set isSubmissionAllowed = true;
    isSubmissionAllowed = true;
    if (isSubmissionAllowed) {
      // can do payment
      validateSubmissionResult = {
        hasError: false,
        result: validateSubmissionResponse
      };
    } else {
      // payment is not allowed to do
      validateSubmissionResult = {
        hasError: true,
        errorMsg
      };
    }
    // callback
    action.callback(validateSubmissionResult);
  } catch (e) {
    /* TODO need error handling */
    const errorMsg = e;
    const validateSubmissionResult = {
      hasError: true,
      errorMsg: `Catch Error${errorMsg}`
    };
    action.callback(validateSubmissionResult);
  }
}

export function* updateROPTable(action) {
  try {
    const { path, pathValue, id } = action;
    const error = yield select(state => state[APPLICATION].error);
    const template = yield select(state => state[APPLICATION].template);
    const application = yield select(state => state[APPLICATION].application);
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const selectedSectionKey = yield select(
      state => state[APPLICATION].component.selectedSectionKey
    );
    const newError = _.cloneDeep(error);
    const newApplication = _.cloneDeep(application);
    let newCompletedStep = completedStep;

    // calculate column
    calculateRopTable({
      rootValues: newApplication,
      path,
      id,
      pathValue
    });
    // Can't get iTemplate from here, so need isAcc.
    validation.validateROPTable({
      iErrorObj: _.get(newError, path, {}),
      iValues: _.get(newApplication, path, {}),
      baseProductCode: _.get(newApplication, "quotation.baseProductCode", "")
    });

    // update completedMenus
    updateApplicationFormCompletedMenus({
      template,
      application: newApplication,
      error: newError,
      sectionKey: selectedSectionKey
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: newError
      })
    ) {
      newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
    } else {
      newCompletedStep = -1;
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
      newApplication
    });

    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newError,
      newCompletedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateDynamicApplicationFormAddress(action) {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const callServer = yield select(state => state[CONFIG].callServer);
    const applicationFormError = yield select(
      state => state[APPLICATION].error
    );
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const { fileName, postalCode, path, callback } = action;
    const newApplication = _.cloneDeep(application);
    const newApplicationFormError = _.cloneDeep(applicationFormError);
    const response = yield call(callServer, {
      url: "client",
      data: {
        method: "post",
        action: "getAddressByPostalCode",
        pC: postalCode,
        fileName
      }
    });

    if (_.isFunction(callback)) {
      callback({ success: !!response.success });
    }

    let list = "";
    list = [
      { fieldKey: "FUND_SRC15", responsekey: "BLDGNO", mandatory: true },
      { fieldKey: "FUND_SRC16", responsekey: "STREETNAME", mandatory: true },
      { fieldKey: "FUND_SRC18", responsekey: "BLDGNAME", mandatory: false }
    ];
    list.forEach(value => {
      _.set(
        newApplication,
        `${path}.${value.fieldKey}`,
        response[value.responsekey] || ""
      );
      _.set(
        newApplicationFormError,
        `${path}.${value.fieldKey}`,
        validation.validateMandatory({
          field: {
            type: TEXT_FIELD,
            mandatory: value.mandatory,
            disabled: false
          },
          value: response[value.responsekey] || ""
        })
      );
    });
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
      newApplication
    });
    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newApplicationFormError,
      newCompletedStep: completedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateApplicationFormDynamicQuestions(action) {
  try {
    const { id, iTemplate, path, pathValue } = action;
    const error = yield select(state => state[APPLICATION].error);
    const application = yield select(state => state[APPLICATION].application);
    const template = yield select(state => state[APPLICATION].template);
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const selectedSectionKey = yield select(
      state => state[APPLICATION].component.selectedSectionKey
    );
    const itemPath = `${path}.${id}`;

    const newError = _.cloneDeep(error);
    const newApplication = _.cloneDeep(application);
    let newCompletedStep = completedStep;

    // update values
    updateFieldValue({
      iTemplate,
      rootValues: newApplication,
      path,
      id,
      pathValue
    });

    // update error
    // TODOL update type based on template type
    _.set(
      newError,
      itemPath,
      validation.validateMandatory({
        field: { type: TEXT_SELECTION, mandatory: true, disabled: false },
        value: pathValue
      })
    );

    // update influenced values and errors by trigger
    initValidateApplicationFormError({
      template,
      rootValues: newApplication,
      errorObj: newError
    });

    // update completedMenus
    updateApplicationFormCompletedMenus({
      template,
      application: newApplication,
      error: newError,
      sectionKey: selectedSectionKey
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: newError
      })
    ) {
      newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
    } else {
      newCompletedStep = -1;
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
      newApplication
    });

    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newError,
      newCompletedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveApplicationFormTableRecord(action) {
  try {
    const { path, dataId, values, recordIndex } = action;
    const template = yield select(state => state[APPLICATION].template);
    const application = yield select(state => state[APPLICATION].application);
    const error = yield select(state => state[APPLICATION].error);
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const selectedSectionKey = yield select(
      state => state[APPLICATION].component.selectedSectionKey
    );

    const newApplication = _.cloneDeep(application);
    const newError = _.cloneDeep(error);
    const dataPath = `${path}.${dataId}`;
    const dataValues = _.get(newApplication, dataPath, []);
    let newCompletedStep = completedStep;

    if (recordIndex === DIALOG_NEW_RECORD) {
      dataValues.push(values);
    } else if (!_.isUndefined(dataValues[recordIndex])) {
      dataValues[recordIndex] = values;
    }
    _.set(newApplication, dataPath, dataValues);

    // update influenced values and errors by trigger
    initValidateApplicationFormError({
      template,
      rootValues: newApplication,
      errorObj: newError
    });

    // update completedMenus
    updateApplicationFormCompletedMenus({
      template,
      application: newApplication,
      error: newError,
      sectionKey: selectedSectionKey
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: newError
      })
    ) {
      newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
    } else {
      newCompletedStep = -1;
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
      newApplication
    });

    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newError,
      newCompletedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* deleteApplicationFormTableRecord(action) {
  try {
    const { path, dataId, recordIndex } = action;
    const template = yield select(state => state[APPLICATION].template);
    const application = yield select(state => state[APPLICATION].application);
    const error = yield select(state => state[APPLICATION].error);
    const completedStep = yield select(
      state => state[APPLICATION].component.completedStep
    );
    const selectedSectionKey = yield select(
      state => state[APPLICATION].component.selectedSectionKey
    );

    const newApplication = _.cloneDeep(application);
    const newError = _.cloneDeep(error);
    const dataPath = `${path}.${dataId}`;
    const dataValues = _.get(newApplication, dataPath, []);
    const dataError = _.get(newError, dataPath, []);
    let newCompletedStep = completedStep;

    _.remove(dataValues, (v, index) => index === recordIndex);
    _.remove(dataError, (v, index) => index === recordIndex);
    _.set(newApplication, dataPath, dataValues);
    _.set(newError, dataPath, dataError);

    // update influenced values and errors by trigger
    initValidateApplicationFormError({
      template,
      rootValues: newApplication,
      errorObj: newError
    });

    // update completedMenus
    updateApplicationFormCompletedMenus({
      template,
      application: newApplication,
      error: newError,
      sectionKey: selectedSectionKey
    });

    // update stepper
    if (
      !checkApplicationFormHasError({
        application: newApplication,
        error: newError
      })
    ) {
      newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
    } else {
      newCompletedStep = -1;
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
      newApplication
    });

    // init validate state when input data into application form
    yield put({
      type: ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM,
      errorObj: newError,
      newCompletedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function reRenderPage(action) {
  try {
    const { dispatch, currentPage, applicationId, quotType } = action;
    switch (currentPage) {
      case "ApplicationForm":
        dispatch({
          type: ACTION_TYPES[APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
          applicationId,
          quotType
        });
        break;
      case "Signatures":
        break;
      case "PaymentAndSubmisstion":
        dispatch({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT_AND_SUBMISSION
        });
        break;
      default:
        break;
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateApplicationBackDate(action) {
  try {
    const { callback } = action;
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);

    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "updateBIBackDateTrue",
        appId: application.id
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newApplication: resp.application
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_BACKDATE,
        newAttachments: resp.attachments
      });
    }

    if (_.isFunction(callback)) {
      yield call(callback);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* invalidateApplication(action) {
  try {
    const { callback } = action;

    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);

    const isShield = application.quotation.quotType === "SHIELD";

    const resp = isShield
      ? yield call(callServer, {
          url: "shieldApplication",
          data: {
            method: "post",
            action: "invalidateApplication",
            appId: application.id
          }
        })
      : yield call(callServer, {
          url: "application",
          data: {
            method: "post",
            action: "invalidateApplication",
            appId: application.id
          }
        });

    if (resp.success) {
      // TODO
    }

    if (_.isFunction(callback)) {
      yield call(callback);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateShownSignatureExpiryAlert() {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);

    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "setSignExpiryShown",
        appId: application.id
      }
    });

    if (resp.success) {
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
        newIsAlertDelayed: false,
        newIsSignatureExpiryAlertShow: false
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateSelectedSectionKey(action) {
  try {
    const { newSelectedSectionKey, template } = action;
    const application = yield select(state => state[APPLICATION].application);
    const currTemplate = yield select(state => state[APPLICATION].template);
    const error = yield select(state => state[APPLICATION].error);

    const newError = _.cloneDeep(error);
    const newApplication = _.cloneDeep(application);

    // update completedMenus
    _.forEach(_.get(application, "applicationForm.values.menus"), menu => {
      if (menu !== PLAN_DETAILS) {
        updateApplicationFormCompletedMenus({
          template: template || currTemplate,
          application,
          error: newError,
          sectionKey: menu,
          isInit: true
        });
      }
    });

    const checkedMenu = _.get(
      newApplication,
      "applicationForm.values.checkedMenu",
      []
    );
    if (!checkedMenu.includes(newSelectedSectionKey)) {
      checkedMenu.push(newSelectedSectionKey);
    }

    yield put({
      type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
      newApplication
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SELECTED_SECTION_KEY,
      newSelectedSectionKey
    });
  } catch (e) {
    // TODO
  }
}

// =============================================================================
// For Shield Below
// =============================================================================

function* initApplicationFormShield(action) {
  let newCompletedStep = -1;
  const { template, application } = action;
  const currTemplate = yield select(state => state[APPLICATION].template);
  const error = yield select(state => state[APPLICATION].error);

  const newError = _.cloneDeep(error);
  const newApplication = initApplicationFormPersonalDetailsValue({
    application,
    template: template || currTemplate
  });
  yield put({
    type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
    newTemplate: template,
    newApplication
  });
  // init validate state when enters the application form
  initValidateApplicationPersonalDetailsForm({
    application: newApplication,
    errorObj: newError
  });
  newError.applicationForm = {};
  initValidateApplicationFormError({
    template: template || currTemplate,
    rootValues: newApplication,
    errorObj: newError
  });

  // update stepper
  if (
    !checkApplicationFormHasError({
      application: newApplication,
      error: newError
    })
  ) {
    newCompletedStep += 1;
    if (!checkSignatureHasError(newApplication)) {
      newCompletedStep += 1;
    }
  }

  yield put({
    type:
      ACTION_TYPES[APPLICATION].INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
    errorObj: newError
  });

  yield put({
    type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
    newCurrentStep: EAPP.tabStep[APPLICATION],
    newCompletedStep
  });
}

function* initEappStoreForShield(eappData) {
  try {
    const currentStep = eappData.stepper.index.current;
    const paymentErrorObj = yield select(
      state => state[PAYMENT_AND_SUBMISSION].error
    );
    switch (currentStep) {
      case EAPP.tabStep[APPLICATION]:
        yield initApplicationFormShield(eappData);
        break;
      case EAPP.tabStep[SIGNATURE]:
        yield signature.initSignatureShield(eappData);
        break;
      case EAPP.tabStep[PAYMENT]:
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // this function need to run before BEFORE_UPDATE_PAYMENT_STATUS
        yield put({
          type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
          newApplication: eappData.application
        });
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].BEFORE_UPDATE_PAYMENT_STATUS,
          newPayment: eappData.application.payment
        });
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
          newPayment: eappData.application.payment
        });
        paymentAndSubmission.initValidatePayment({
          dataObj: eappData.application.payment,
          errorObj: paymentErrorObj
        });
        yield put({
          type: ACTION_TYPES[APPLICATION].UPDATE_COMPLETED_STEP,
          newCompletedStep: Object.values(paymentErrorObj).find(value => {
            if (value.hasError) {
              return true;
            }
            return false;
          })
            ? 2
            : 3
        });
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION]
              .VALIDATE_PAYMENT_AND_SUBMISSION,
          errorObj: paymentErrorObj
        });
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
          newTemplate: eappData.template
        });
        break;
      case EAPP.tabStep[SUBMISSION]:
        yield put({
          type:
            ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
          newSubmission: eappData.application.submission
        });
        yield put({
          type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
          newApplication: eappData.application
        });
        break;
      default:
        break;
    }
    if (
      _.get(eappData.crossAge, "status") ===
      EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED
    ) {
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_CROSS_AGE,
        newCrossAge: eappData.crossAge
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
        newIsAlertDelayed: currentStep === EAPP.tabStep[SIGNATURE],
        newIsCrossAgeAlertShow: true
      });
    }
    let newIsSignatureExpiryAlertShow = false;
    if (eappData.warningMsg && eappData.warningMsg.showSignExpiryWarning) {
      newIsSignatureExpiryAlertShow = eappData.warningMsg.showSignExpiryWarning;
    }
    yield put({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
      newIsAlertDelayed: currentStep === EAPP.tabStep[SIGNATURE],
      newIsSignatureExpiryAlertShow
    });

    // yield put({
    //   type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
    //   newCurrentStep: currentStep
    // });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* applyApplicationFormShield(action) {
  try {
    yield put({
      type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
    });
    const callServer = yield select(state => state[CONFIG].callServer);
    const { quotationId, callback } = action;
    const response = yield call(callServer, {
      url: "shieldApplication",
      data: {
        method: "post",
        action: "applyAppForm",
        quotId: quotationId
      }
    });
    if (response.success) {
      yield call(initEappStoreForShield, response);
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* continueApplicationFormShield(action) {
  try {
    if (action.cleanData) {
      yield put({
        type: ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
      });
    }
    const callServer = yield select(state => state[CONFIG].callServer);
    const { applicationId, callback } = action;
    const response = yield call(callServer, {
      url: "shieldApplication",
      data: {
        method: "post",
        action: "continueApplication",
        appId: applicationId
      }
    });
    if (response.success) {
      yield call(initEappStoreForShield, response);
    }
    if (_.isFunction(callback)) {
      callback({
        currentStep: _.get(response, "stepper.index.current", 0)
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* changeTabShield(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const { currentStep, nextStep, callback } = action;
    let changedValues = "";
    switch (currentStep) {
      case EAPP.tabStep[APPLICATION]:
        changedValues = yield select(
          state => state[APPLICATION].application.applicationForm.values
        );
        break;
      case EAPP.tabStep[SIGNATURE]:
        changedValues = yield select(
          state => state[APPLICATION].application.applicationForm.values
        );
        break;
      case EAPP.tabStep[PAYMENT]:
        changedValues = yield select(
          state => state[PAYMENT_AND_SUBMISSION].payment
        );
        break;
      case EAPP.tabStep[SUBMISSION]:
        changedValues = yield select(state => state[APPLICATION]);
        break;
      default:
        break;
    }
    const response = yield call(callServer, {
      url: "shieldApplication",
      data: {
        method: "post",
        action: "goNextStep",
        appId: application.id,
        currIndex: currentStep,
        nextIndex: nextStep,
        changedValues
      }
    });
    if (response.success) {
      _.set(response, "stepper.index.current", nextStep);
      yield call(initEappStoreForShield, response);
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveApplicationFormShieldBefore(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const shieldPolicyNumber = yield select(
      state => state[APPLICATION].component.shieldPolicyNumber
    );
    const { currentStep, actionType, callback } = action;
    const changedValues = application.applicationForm.values;
    let exit = true;
    let handleResponse = true;
    switch (actionType) {
      case SWITCHMENU:
        exit = false;
        break;
      case OPENSUPPORTINGDOCUMENT:
        if (currentStep === EAPP.tabStep[APPLICATION]) {
          exit = false;
          handleResponse = false;
        }
        break;
      case CLOSEAPPLICATION:
        if (
          currentStep === EAPP.tabStep[PAYMENT] ||
          currentStep === EAPP.tabStep[SUBMISSION]
        ) {
          yield call(paymentAndSubmission.updatePaymentMethodsShield);
          yield call(paymentAndSubmission.saveSubmissionShield);
        } else if (currentStep === EAPP.tabStep[APPLICATION]) {
          exit = false;
        }
        break;
      default:
        break;
    }
    if (!exit) {
      const response = yield call(callServer, {
        url: "shieldApplication",
        data: {
          method: "post",
          action: "saveAppForm",
          appId: application.id,
          policyNumber: shieldPolicyNumber,
          changedValues
        }
      });
      if (response.success && handleResponse) {
        _.set(response, "stepper.index.current", currentStep);
        yield call(initEappStoreForShield, response);

        if (response.updIds) {
          const profileList = updateProfileList({
            updIds: response.updIds,
            application
          });

          if (profileList.length > 0) {
            yield client.updateProfile({ profileList });
          }
        }
      }
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePolicyNumber(action) {
  try {
    const application = yield select(state => state[APPLICATION].application);
    const { policyNumber, isShield, callback } = action;
    if (isShield) {
      const newiCidMapping = _.cloneDeep(application.iCidMapping);
      let policyNumberIndex = 0;
      Object.values(newiCidMapping).forEach(profile => {
        profile.forEach(value => {
          value.policyNumber = policyNumber[policyNumberIndex];
          policyNumberIndex += 1;
        });
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_SHIELD_POLICY_NUMBER,
        newShieldPolicyNumber: policyNumber
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
        newApplication: Object.assign({}, application, {
          iCidMapping: newiCidMapping
        })
      });
      yield call(saveApplicationFormShieldBefore, {
        currentStep: 0,
        actionType: SWITCHMENU
      });
    } else {
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_POLICY_NUMBER,
        newPolicuNumnber: policyNumber[0]
      });
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* applyApplicationForm(action) {
  const { isShield } = action;
  if (isShield) {
    yield applyApplicationFormShield(action);
  } else {
    yield applyApplicationFormNormal(action);
  }
}

export function* getApplicationsAfterSubmission(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const application = yield select(state => state[APPLICATION].application);
    const { callback } = action;
    const resp = yield call(callServer, {
      url: "application",
      data: {
        method: "post",
        action: "goApplication",
        id: application.id
      }
    });

    yield put({
      type: ACTION_TYPES[APPLICATION].GET_APPLICATION,
      newApplication: resp.application
    });

    yield call(paymentAndSubmission.getSubmission, { docId: application.id });

    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* genFNA(action) {
  try {
    const cid = yield select(state => state[CLIENT].profile.cid);
    const callServer = yield select(state => state[CONFIG].callServer);
    const { callback } = action;

    yield call(callServer, {
      url: `application`,
      data: {
        action: "genFNA",
        cid
      }
    });
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}
