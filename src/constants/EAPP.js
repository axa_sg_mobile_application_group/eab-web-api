import { APPLICATION, SIGNATURE, PAYMENT, SUBMISSION } from "./REDUCER_TYPES";

export default {
  appStatus: {
    APPLYING: "APPLYING",
    SUBMITTED: "SUBMITTED",
    INVALIDATED: "INVALIDATED",
    INVALIDATED_SIGNED: "INVALIDATED_SIGNED",
    APP_FORM_SUBMISSION: "appFormSubmission"
  },
  tabStep: {
    [APPLICATION]: 0,
    [SIGNATURE]: 1,
    [PAYMENT]: 2,
    [SUBMISSION]: 3
  },
  reportName: {
    FNA: "fnaReport",
    PROPOSAL: "proposal",
    APPPDF: "appPdf"
  },
  fieldId: {
    [PAYMENT]: {
      INITPAYMETHOD: "initPayMethod",
      TELETRANSFTER: "teleTransfter",
      TTREMITTINGBANK: "ttRemittingBank",
      TTDOR: "ttDOR",
      TTREMARKS: "ttRemarks",
      PROPOSALNO: "proposalNo",
      POLICYCCY: "policyCcy",
      INITBASICPREM: "initBasicPrem",
      INITTOTALPREM: "initTotalPrem",
      BACKDATINGPREM: "backdatingPrem",
      TOPUPPREM: "topupPrem",
      INITRSPPREM: "initRspPrem",
      SUBSEQPAYMETHOD: "subseqPayMethod",
      SRSBLOCK: "srsBlock"
    }
  },
  fieldValue: {
    [PAYMENT]: {
      CASH: "cash",
      AXASAM: "axasam",
      CHEQUECASHIERORDER: "chequeCashierOrder",
      CREDIT_CARD: "crCard",
      E_NETS: "eNets",
      DBSCRCARDIPP: "dbsCrCardIpp",
      SRS: "srs"
    }
  },
  riderName: {
    BASIC_CARE_A: "AXA Basic Care (Plan A)",
    GENERAL_CARE_A: "AXA General Care (Plan A)",
    HOME_CARE: "AXA Home Care",
    BASIC_CARE_B: "AXA Basic Care (Plan B)",
    GENERAL_CARE_B: "AXA General Care (Plan B)",
    BASIC_CARE_STANDARD: "AXA Basic Care (Standard Plan)"
  },
  action: {
    SWITCHMENU: "switchMenu",
    SWITCHTAB: "switchTab",
    OPENSUPPORTINGDOCUMENT: "openSupportingDocument",
    CLOSEAPPLICATION: "closeApplication",
    NEXTPAGE: "nextPage",
    GOFNA: "goFNA"
  },
  CROSSAGE_STATUS: {
    NO_STATUS: 0,
    WILL_CROSSAGE: 100,
    WILL_CROSSAGE_NO_ACTION: 150,
    CROSSED_AGE_SIGNED: 200,
    CROSSED_AGE_NO_ACTION: 250,
    CROSSED_AGE_NOTSIGNED: 300
  },
  dataSyncStep: {
    IS_ONLINE_CHECKING_CONFLICT: "IS_ONLINE_CHECKING_CONFLICT",
    IS_AFTER_SUBMIT: "IS_AFTER_SUBMIT",
    IS_BEFORE_SUBMIT: "IS_BEFORE_SUBMIT"
  },
  paymentButton: {
    IS_GETTING_PAYMENT_URL: "IS_GETTING_PAYMENT_URL",
    IS_GETTING_PAYMENT_STATUS: "IS_GETTING_PAYMENT_STATUS",
    IS_SHIELD_DATA_SYNC: "IS_SHIELD_DATA_SYNC",
    IS_CHECKING_PAYMENT_CONFLICT: "IS_CHECKING_PAYMENT_CONFLICT",
    IS_ONLINE_CHECKING_CONFLICT: "IS_ONLINE_CHECKING_CONFLICT"
  }
};
