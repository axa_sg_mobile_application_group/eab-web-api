export const AGENT = "agent";

export const CLIENT_PROFILE = "cust";
export const BUNDLE = "bundle";
export const FNA_PDA = "pda";
export const FNA_FE = "fe";
export const FNA_NA = "na";
export const QUOTATION = "quotation";
export const APPLICATION_NORMAL = "application";
export const APPLICATION_MASTER = "masterApplication";

export const PRODUCT = "product";
export const FUND = "fund";
export const PDF_TEMPLATE = "pdfTemplate";
