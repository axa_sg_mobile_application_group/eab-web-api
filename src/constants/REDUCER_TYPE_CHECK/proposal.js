import PropTypes from "prop-types";

export const proposal = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      tabLabel: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      allowSave: PropTypes.bool.isRequired,
      fileName: PropTypes.string.isRequired,
      docId: PropTypes.string,
      attachmentId: PropTypes.string,
      data: PropTypes.string
    })
  ).isRequired
};

// TODO update data inside array
export const illustrateData = PropTypes.objectOf(
  PropTypes.arrayOf(PropTypes.shape({}))
);

export const canEmail = PropTypes.bool;
export const canRequote = PropTypes.bool;
export const canClone = PropTypes.bool;

export const proposalEmail = {
  isRequired: PropTypes.array.isRequired
};
