import PropTypes from "prop-types";

export const pda = {
  isRequired: PropTypes.shape({
    agentCode: PropTypes.string,
    applicant: PropTypes.string,
    compCode: PropTypes.string,
    completed: PropTypes.bool,
    dealerGroup: PropTypes.string,
    dependants: PropTypes.string,
    id: PropTypes.string,
    isCompleted: PropTypes.bool,
    lastStepIndex: PropTypes.number,
    lastUpd: PropTypes.number,
    notes: PropTypes.string,
    ownerConsentMethod: PropTypes.string,
    trustedIndividual: PropTypes.string,
    type: PropTypes.number,
    undefined: PropTypes.string
  })
};

export const dependantProfiles = {
  isRequired: PropTypes.shape({
    agentCode: PropTypes.string.isRequired,
    aspects: PropTypes.string.isRequired,
    ciProtection: PropTypes.oneOfType([PropTypes.object]),
    // TODO
    ckaSection: PropTypes.oneOfType([PropTypes.object]),
    compCode: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
    dealerGroup: PropTypes.string.isRequired,
    diProtection: PropTypes.oneOfType([PropTypes.object]),
    ePlanning: PropTypes.oneOfType([PropTypes.object]),
    fiProtection: PropTypes.oneOfType([PropTypes.object]),
    haveFnaCompleted: PropTypes.bool.isRequired,
    hcProtection: PropTypes.oneOfType([PropTypes.object]),
    iarRate: PropTypes.string,
    iarRateNote: PropTypes.string,
    id: PropTypes.string.isRequired,
    isCompleted: PropTypes.bool.isRequired,
    lastStepIndex: PropTypes.number.isRequired,
    lastUpd: PropTypes.number,
    needsImgDesc: PropTypes.string,
    other: PropTypes.oneOfType([PropTypes.object]),
    paProtection: PropTypes.oneOfType([PropTypes.object]),
    pcHeadstart: PropTypes.oneOfType([PropTypes.object]),
    productType: PropTypes.oneOfType([PropTypes.object]),
    psGoals: PropTypes.oneOfType([PropTypes.object]),
    rPlanning: PropTypes.oneOfType([PropTypes.object]),
    raSection: PropTypes.oneOfType([PropTypes.object]),
    selectedMenuId: PropTypes.string.isRequired,
    sfAspects: PropTypes.array,
    sfFailNotes: PropTypes.string,
    spAspects: PropTypes.array,
    spNotes: PropTypes.string,
    type: PropTypes.string
  })
};

export const fe = {
  isRequired: PropTypes.shape({
    agentCode: PropTypes.string,
    compCode: PropTypes.string,
    completed: PropTypes.bool,
    dealerGroup: PropTypes.string,
    id: PropTypes.string,
    isCompleted: PropTypes.bool,
    lastUpd: PropTypes.number,
    owner: PropTypes.oneOfType([PropTypes.object]),
    spouse: PropTypes.oneOfType([PropTypes.object]),
    dependants: PropTypes.arrayOf([PropTypes.object])
  })
};

export const feErrors = {
  isRequired: PropTypes.objectOf(
    PropTypes.objectOf(
      PropTypes.shape({
        hasError: PropTypes.bool.isRequired,
        message: PropTypes.string.isRequired
      })
    ).isRequired
  ).isRequired
};

export const FNAReport = {
  isRequired: PropTypes.string.isRequired
};

export const FNAReportEmail = {
  isRequired: PropTypes.array.isRequired
};

export const na = {
  isRequired: PropTypes.shape({
    agentCode: PropTypes.string,
    aspects: PropTypes.string,
    ciProtection: PropTypes.oneOfType([PropTypes.object]),
    ckaSection: PropTypes.oneOfType([PropTypes.object]),
    compCode: PropTypes.string,
    completed: PropTypes.bool,
    dealerGroup: PropTypes.string,
    diProtection: PropTypes.oneOfType([PropTypes.object]),
    ePlanning: PropTypes.oneOfType([PropTypes.object]),
    fiProtection: PropTypes.oneOfType([PropTypes.object]),
    haveFnaCompleted: PropTypes.bool,
    hcProtection: PropTypes.oneOfType([PropTypes.object]),
    iarRate: PropTypes.string,
    iarRateNote: PropTypes.string,
    id: PropTypes.string,
    isCompleted: PropTypes.bool,
    lastStepIndex: PropTypes.number,
    lastUpd: PropTypes.number,
    needsImgDesc: PropTypes.string,
    other: PropTypes.oneOfType([PropTypes.object]),
    paProtection: PropTypes.oneOfType([PropTypes.object]),
    pcHeadstart: PropTypes.oneOfType([PropTypes.object]),
    productType: PropTypes.oneOfType([PropTypes.object]),
    psGoals: PropTypes.oneOfType([PropTypes.object]),
    rPlanning: PropTypes.oneOfType([PropTypes.object]),
    raSection: PropTypes.oneOfType([PropTypes.object]),
    selectedMenuId: PropTypes.string,
    sfAspects: PropTypes.array,
    sfFailNotes: PropTypes.string,
    spAspects: PropTypes.array,
    spNotes: PropTypes.string,
    type: PropTypes.string,
    completedStep: PropTypes.number
  })
};

export const analysis = {
  isRequired: PropTypes.shape({
    // TODO
  })
};

export const analysisData = {
  isRequired: PropTypes.shape({
    // TODO
  })
};

export const analysisRule = {
  isRequired: PropTypes.shape({
    // TODO
  })
};

export const analysisContainerFunc = {
  isRequired: PropTypes.shape({
    // TODO
  })
};

export const ra = {
  isRequired: PropTypes.shape({
    // TODO
  })
};

export const isFNA = {
  isRequired: PropTypes.bool
};

export const nameOrder = {
  isRequired: PropTypes.string.isRequired
};
