import PropTypes from "prop-types";
import * as common from "./common";

const applicationFormErrorTypes = PropTypes.objectOf(
  PropTypes.objectOf(common.error)
);

export const error = {
  isRequired: PropTypes.objectOf(
    PropTypes.objectOf(
      PropTypes.shape({
        proposer: PropTypes.objectOf(applicationFormErrorTypes),
        insured: PropTypes.arrayOf(
          PropTypes.objectOf(applicationFormErrorTypes)
        )
      })
    )
  )
};

export const planDetails = {
  availableFunds: {
    isRequired: PropTypes.oneOfType([PropTypes.array])
  },
  applicationPlanDetailsData: {
    isRequired: PropTypes.oneOfType([PropTypes.object])
  }
};

// TODO:
export const template = {
  isRequired: {}
};

// TODO:
export const rootValues = {
  isRequired: {}
};

// TODO:
export const dialogValues = {};

export const crossAge = PropTypes.shape({
  crossedAge: PropTypes.bool.isRequired,
  allowBackdate: PropTypes.bool.isRequired,
  proposerStatus: PropTypes.number.isRequired,
  insuredStatus: PropTypes.number.isRequired,
  crossedAgeCid: PropTypes.arrayOf(PropTypes.string).isRequired,
  status: PropTypes.number.isRequired
});
