import PropTypes from "prop-types";

// TODO
export const SupportingDocumentData = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};

export const error = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};

export const screenProps = {
  isRequired: PropTypes.shape({
    isShield: PropTypes.bool
  })
};

export const application = {
  isRequired: PropTypes.shape({
    applicationId: PropTypes.string,
    bundleId: PropTypes.string
  })
};
