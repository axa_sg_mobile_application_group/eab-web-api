import PropTypes from "prop-types";
import { error } from "./common";

export const recommendation = {
  value: PropTypes.shape({
    chosenList: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    notChosenList: PropTypes.arrayOf(PropTypes.string).isRequired
  }),
  valueByQuotId: PropTypes.shape({
    benefit: PropTypes.string.isRequired,
    limitation: PropTypes.string.isRequired,
    reason: PropTypes.string.isRequired,
    rop: PropTypes.shape({
      choiceQ1: PropTypes.string.isRequired,
      choiceQ1Sub1: PropTypes.string.isRequired,
      choiceQ1Sub2: PropTypes.string.isRequired,
      choiceQ1Sub3: PropTypes.string.isRequired,
      existCi: PropTypes.number.isRequired,
      existLife: PropTypes.number.isRequired,
      existPaAdb: PropTypes.number.isRequired,
      existTotalPrem: PropTypes.number.isRequired,
      existTpd: PropTypes.number.isRequired,
      replaceCi: PropTypes.number,
      replaceLife: PropTypes.number,
      replacePaAdb: PropTypes.number,
      replaceTotalPrem: PropTypes.number,
      replaceTpd: PropTypes.number
    }).isRequired,
    rop_shield: PropTypes.shape({
      ropBlock: PropTypes.shape({
        iCidRopAnswerMap: PropTypes.objectOf(PropTypes.string).isRequired,
        ropQ1sub3: PropTypes.string.isRequired,
        ropQ2: PropTypes.string.isRequired,
        ropQ3: PropTypes.string.isRequired,
        shieldRopAnswer_0: PropTypes.string.isRequired,
        shieldRopAnswer_1: PropTypes.string.isRequired,
        shieldRopAnswer_2: PropTypes.string.isRequired,
        shieldRopAnswer_3: PropTypes.string.isRequired,
        shieldRopAnswer_4: PropTypes.string.isRequired,
        shieldRopAnswer_5: PropTypes.string.isRequired
      }).isRequired
    }).isRequired
  }),
  error: PropTypes.shape({
    benefit: error.isRequired,
    limitation: error.isRequired,
    reason: error.isRequired,
    rop: PropTypes.shape({
      choiceQ1: error.isRequired,
      choiceQ1Sub1: error.isRequired,
      choiceQ1Sub2: error.isRequired,
      choiceQ1Sub3: error.isRequired,
      existCi: error.isRequired,
      existLife: error.isRequired,
      existPaAdb: error.isRequired,
      existTotalPrem: error.isRequired,
      existTpd: error.isRequired,
      replaceCi: error.isRequired,
      replaceLife: error.isRequired,
      replacePaAdb: error.isRequired,
      replaceTotalPrem: error.isRequired,
      replaceTpd: error.isRequired
    }).isRequired,
    rop_shield: PropTypes.shape({
      ropBlock: PropTypes.shape({
        iCidRopAnswerMap: PropTypes.objectOf(error).isRequired,
        ropQ1sub3: error.isRequired,
        ropQ2: error.isRequired,
        ropQ3: error.isRequired,
        shieldRopAnswer_0: error.isRequired,
        shieldRopAnswer_1: error.isRequired,
        shieldRopAnswer_2: error.isRequired,
        shieldRopAnswer_3: error.isRequired,
        shieldRopAnswer_4: error.isRequired,
        shieldRopAnswer_5: error.isRequired
      }).isRequired
    }).isRequired
  })
};

export const budget = {
  value: PropTypes.shape({
    spBudget: PropTypes.number.isRequired,
    spTotalPremium: PropTypes.number.isRequired,
    spCompare: PropTypes.number.isRequired,
    spCompareResult: PropTypes.string.isRequired,
    rpBudget: PropTypes.number.isRequired,
    rpTotalPremium: PropTypes.number.isRequired,
    rpCompare: PropTypes.number.isRequired,
    rpCompareResult: PropTypes.string.isRequired,
    cpfOaBudget: PropTypes.number.isRequired,
    cpfOaTotalPremium: PropTypes.number.isRequired,
    cpfOaCompare: PropTypes.number.isRequired,
    cpfOaCompareResult: PropTypes.string.isRequired,
    cpfSaBudget: PropTypes.number.isRequired,
    cpfSaTotalPremium: PropTypes.number.isRequired,
    cpfSaCompare: PropTypes.number.isRequired,
    cpfSaCompareResult: PropTypes.string.isRequired,
    srsBudget: PropTypes.number.isRequired,
    srsTotalPremium: PropTypes.number.isRequired,
    srsCompare: PropTypes.number.isRequired,
    srsCompareResult: PropTypes.string.isRequired,
    cpfMsBudget: PropTypes.number.isRequired,
    cpfMsTotalPremium: PropTypes.number.isRequired,
    cpfMsCompare: PropTypes.number.isRequired,
    cpfMsCompareResult: PropTypes.string.isRequired,
    budgetMoreChoice: PropTypes.string.isRequired,
    budgetMoreReason: PropTypes.string.isRequired,
    budgetLessChoice: PropTypes.string.isRequired,
    budgetLessReason: PropTypes.string.isRequired
  }),
  error: PropTypes.shape({
    spBudget: error.isRequired,
    spTotalPremium: error.isRequired,
    spCompare: error.isRequired,
    spCompareResult: error.isRequired,
    rpBudget: error.isRequired,
    rpTotalPremium: error.isRequired,
    rpCompare: error.isRequired,
    rpCompareResult: error.isRequired,
    cpfOaBudget: error.isRequired,
    cpfOaTotalPremium: error.isRequired,
    cpfOaCompare: error.isRequired,
    cpfOaCompareResult: error.isRequired,
    cpfSaBudget: error.isRequired,
    cpfSaTotalPremium: error.isRequired,
    cpfSaCompare: error.isRequired,
    cpfSaCompareResult: error.isRequired,
    srsBudget: error.isRequired,
    srsTotalPremium: error.isRequired,
    srsCompare: error.isRequired,
    srsCompareResult: error.isRequired,
    cpfMsBudget: error.isRequired,
    cpfMsTotalPremium: error.isRequired,
    cpfMsCompare: error.isRequired,
    cpfMsCompareResult: error.isRequired,
    budgetMoreChoice: error.isRequired,
    budgetMoreReason: error.isRequired,
    budgetLessChoice: error.isRequired,
    budgetLessReason: error.isRequired
  })
};

export const acceptance = {
  value: PropTypes.arrayOf(
    PropTypes.shape({
      quotationDocId: PropTypes.string.isRequired,
      proposerAndLifeAssuredName: PropTypes.string.isRequired,
      basicPlanName: PropTypes.string.isRequired,
      ridersName: PropTypes.arrayOf(
        PropTypes.shape({
          en: PropTypes.string.isRequired,
          "zh-Hant": PropTypes.string.isRequired
        })
      ).isRequired
    })
  )
};
