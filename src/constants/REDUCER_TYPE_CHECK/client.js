import PropTypes from "prop-types";

export const profile = {
  isRequired: PropTypes.shape({
    addrBlock: PropTypes.string,
    addrCity: PropTypes.string,
    addrCountry: PropTypes.string,
    addrEstate: PropTypes.string,
    addrStreet: PropTypes.string,
    age: PropTypes.number,
    agentId: PropTypes.string,
    allowance: PropTypes.number,
    applicationCount: PropTypes.number,
    branchInfo: PropTypes.shape({
      bankRefId: PropTypes.string,
      branch: PropTypes.string
    }),
    bundle: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        isValid: PropTypes.bool.isRequired
      })
    ),
    cid: PropTypes.string,
    compCode: PropTypes.string,
    dealerGroup: PropTypes.string,
    dependants: PropTypes.arrayOf(
      PropTypes.shape({
        cid: PropTypes.string.isRequired,
        relationship: PropTypes.string.isRequired,
        relationshipOther: PropTypes.string
      })
    ),
    dob: PropTypes.string,
    education: PropTypes.string,
    email: PropTypes.string,
    employStatus: PropTypes.string,
    firstName: PropTypes.string,
    fnaRecordIdArray: PropTypes.string,
    fullName: PropTypes.string,
    gender: PropTypes.string,
    hanyuPinyinName: PropTypes.string,
    haveSignDoc: PropTypes.bool,
    idCardNo: PropTypes.string,
    idDocType: PropTypes.string,
    idDocTypeOther: PropTypes.string,
    industry: PropTypes.string,
    initial: PropTypes.string,
    isSmoker: PropTypes.string,
    isValid: PropTypes.bool,
    language: PropTypes.string,
    lastName: PropTypes.string,
    lastUpdateDate: PropTypes.string,
    marital: PropTypes.string,
    mobileCountryCode: PropTypes.string,
    mobileNo: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    nameOrder: PropTypes.string,
    nationality: PropTypes.string,
    nearAge: PropTypes.number,
    occupation: PropTypes.string,
    occupationOther: PropTypes.string,
    organization: PropTypes.string,
    organizationCountry: PropTypes.string,
    othName: PropTypes.string,
    otherMobileCountryCode: PropTypes.string,
    otherNo: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    otherResidenceCity: PropTypes.string,
    pass: PropTypes.string,
    passExpDate: PropTypes.number,
    photo: PropTypes.object,
    postalCode: PropTypes.string,
    prStatus: PropTypes.string,
    referrals: PropTypes.string,
    residenceCity: PropTypes.string,
    residenceCountry: PropTypes.string,
    title: PropTypes.string,
    trustedIndividuals: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
      idCardNo: PropTypes.string.isRequired,
      idDocType: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      mobileCountryCode: PropTypes.string.isRequired,
      mobileNo: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.object
      ]),
      nameOrder: PropTypes.string.isRequired,
      relationship: PropTypes.string.isRequired,
      tiPhoto: PropTypes.string.isRequired
    }),
    type: PropTypes.string,
    unitNum: PropTypes.string
  })
};

export const contactList = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      applicationCount: PropTypes.number.isRequired,
      email: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      idCardNo: PropTypes.string.isRequired,
      idDocType: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      mobileNo: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      nameOrder: PropTypes.string.isRequired,
      photo: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
        PropTypes.string
      ]).isRequired
    })
  )
};

export const dependantProfiles = {
  isRequired: PropTypes.objectOf(profile.isRequired).isRequired
};
