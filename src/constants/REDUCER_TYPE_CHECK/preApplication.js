import PropTypes from "prop-types";

export const ApplicationItem = {
  value: {
    isRequired: PropTypes.shape({
      // Common - from View
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      isValid: PropTypes.bool,
      bundleId: PropTypes.string,
      baseProductCode: PropTypes.string,
      baseProductName: PropTypes.object,
      iName: PropTypes.string,
      pName: PropTypes.string,
      ccy: PropTypes.string,
      paymentMode: PropTypes.string,
      lastUpdateDate: PropTypes.string,
      productLine: PropTypes.string,
      plans: PropTypes.array,
      iCid: PropTypes.string,
      pCid: PropTypes.string,
      iCids: PropTypes.array,
      quotType: PropTypes.string,
      // Common - from core-api
      paymentMethod: PropTypes.string,
      // Quotation - from View
      insureds: PropTypes.object,
      policyOptions: PropTypes.object,
      policyOptionsDesc: PropTypes.object,
      clientChoice: PropTypes.object,
      totCpfPortion: PropTypes.number,
      totCashPortion: PropTypes.number,
      totMedisave: PropTypes.number,
      totPremium: PropTypes.number,
      // Application - from View
      policyNumber: PropTypes.string,
      quotation: PropTypes.object,
      iCidMapping: PropTypes.object,
      isInitialPaymentCompleted: PropTypes.bool,
      isStartSignature: PropTypes.bool,
      isFullySigned: PropTypes.bool,
      isMandDocsAllUploaded: PropTypes.bool,
      // Application - from core-api
      approvalStatus: PropTypes.string
    })
  }
};

export const preApplicationEmail = {
  isRequired: PropTypes.array.isRequired
};
