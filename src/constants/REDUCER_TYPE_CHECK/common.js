import PropTypes from "prop-types";
import { LANGUAGE_TYPES } from "../../locales";

export const error = PropTypes.shape({
  hasError: PropTypes.bool.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.shape({
      [LANGUAGE_TYPES.ENGLISH]: PropTypes.string,
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: PropTypes.string
    }),
    PropTypes.string
  ]).isRequired
});

export default {};
