export const PENDING_LIFE = "pendingLife";
export const PENDING_CI = "pendingCi";
export const PENDING_TPD = "pendingTpd";
export const PENDING_PAADB = "pendingPaAdb";
export const PENDING_TOTALPREM = "pendingTotalPrem";

export const EXIST_LIFE = "existLife";
export const EXIST_CI = "existCi";
export const EXIST_TPD = "existTpd";
export const EXIST_PAADB = "existPaAdb";
export const EXIST_TOTALPREM = "existTotalPrem";

export const REPLACE_LIFE = "replaceLife";
export const REPLACE_CI = "replaceCi";
export const REPLACE_TPD = "replaceTpd";
export const REPLACE_PAADB = "replacePaAdb";
export const REPLACE_TOTALPREM = "replaceTotalPrem";

export const PENDING_LIFE_AXA = "pendingLifeAXA";
export const PENDING_CI_AXA = "pendingCiAXA";
export const PENDING_TPD_AXA = "pendingTpdAXA";
export const PENDING_PAADB_AXA = "pendingPaAdbAXA";
export const PENDING_TOTALPREM_AXA = "pendingTotalPremAXA";

export const EXIST_LIFE_AXA = "existLifeAXA";
export const EXIST_CI_AXA = "existCiAXA";
export const EXIST_TPD_AXA = "existTpdAXA";
export const EXIST_PAADB_AXA = "existPaAdbAXA";
export const EXIST_TOTALPREM_AXA = "existTotalPremAXA";

export const REPLACE_LIFE_AXA = "replaceLifeAXA";
export const REPLACE_CI_AXA = "replaceCiAXA";
export const REPLACE_TPD_AXA = "replaceTpdAXA";
export const REPLACE_PAADB_AXA = "replacePaAdbAXA";
export const REPLACE_TOTALPREM_AXA = "replaceTotalPremAXA";

export const PENDING_LIFE_OTHER = "pendingLifeOther";
export const PENDING_CI_OTHER = "pendingCiOther";
export const PENDING_TPD_OTHER = "pendingTpdOther";
export const PENDING_PAADB_OTHER = "pendingPaAdbOther";
export const PENDING_TOTALPREM_OTHER = "pendingTotalPremOther";

export const EXIST_LIFE_OTHER = "existLifeOther";
export const EXIST_CI_OTHER = "existCiOther";
export const EXIST_TPD_OTHER = "existTpdOther";
export const EXIST_PAADB_OTHER = "existPaAdbOther";
export const EXIST_TOTALPREM_OTHER = "existTotalPremOther";

export const REPLACE_LIFE_OTHER = "replaceLifeOther";
export const REPLACE_CI_OTHER = "replaceCiOther";
export const REPLACE_TPD_OTHER = "replaceTpdOther";
export const REPLACE_PAADB_OTHER = "replacePaAdbOther";
export const REPLACE_TOTALPREM_OTHER = "replaceTotalPremOther";

export const REPLACE_POLICY_TYPE_AXA = "replacePolTypeAXA";
export const REPLACE_POLICY_TYPE_OTHER = "replacePolTypeOther";

export const SHOW_EXIST_TABLE = "havExtPlans";
export const SHOW_ACC_EXIST_TABLE = "havAccPlans";
export const SHOW_PENDING_TABLE = "havPndinApp";
export const SHOW_REPLACE_TABLE = "isProslReplace";

export const PERIOD = {
  EXISTING: "exist",
  PENDING: "pending",
  REPLACE: "replace"
};

export const SUMASSURED = {
  LIFE: "LIFE",
  TPD: "TPD",
  CI: "CI",
  PAADB: "PAADB",
  TOTALPREM: "TOTALPREM"
};

export const COMPANY = {
  AXA: "AXA",
  OTHER: "OTHER",
  TOTAL: "TOTAL"
};

export const EXITSTING_TABLE_MAPPING = {
  AXA: {
    LIFE: EXIST_LIFE_AXA,
    TPD: EXIST_TPD_AXA,
    CI: EXIST_CI_AXA,
    PAADB: EXIST_PAADB_AXA,
    TOTALPREM: EXIST_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: EXIST_LIFE_OTHER,
    TPD: EXIST_TPD_OTHER,
    CI: EXIST_CI_OTHER,
    PAADB: EXIST_PAADB_OTHER,
    TOTALPREM: EXIST_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: EXIST_LIFE,
    TPD: EXIST_TPD,
    CI: EXIST_CI,
    PAADB: EXIST_PAADB,
    TOTALPREM: EXIST_TOTALPREM
  }
};

export const PENDING_TABLE_MAPPING = {
  AXA: {
    LIFE: PENDING_LIFE_AXA,
    TPD: PENDING_TPD_AXA,
    CI: PENDING_CI_AXA,
    PAADB: PENDING_PAADB_AXA,
    TOTALPREM: PENDING_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: PENDING_LIFE_OTHER,
    TPD: PENDING_TPD_OTHER,
    CI: PENDING_CI_OTHER,
    PAADB: PENDING_PAADB_OTHER,
    TOTALPREM: PENDING_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: PENDING_LIFE,
    TPD: PENDING_TPD,
    CI: PENDING_CI,
    PAADB: PENDING_PAADB,
    TOTALPREM: PENDING_TOTALPREM
  }
};

export const REPLACE_TABLE_MAPPING = {
  AXA: {
    LIFE: REPLACE_LIFE_AXA,
    TPD: REPLACE_TPD_AXA,
    CI: REPLACE_CI_AXA,
    PAADB: REPLACE_PAADB_AXA,
    TOTALPREM: REPLACE_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: REPLACE_LIFE_OTHER,
    TPD: REPLACE_TPD_OTHER,
    CI: REPLACE_CI_OTHER,
    PAADB: REPLACE_PAADB_OTHER,
    TOTALPREM: REPLACE_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: REPLACE_LIFE,
    TPD: REPLACE_TPD,
    CI: REPLACE_CI,
    PAADB: REPLACE_PAADB,
    TOTALPREM: REPLACE_TOTALPREM
  }
};
