export default {
  APPLICATION_LIST: {
    ALL: "ALL",
    PROPOSED: "PROPOSED",
    APPLYING: "APPLYING",
    SUBMITTED: "SUBMITTED",
    INVALIDATED: "INVALIDATED",
    INVALIDATED_SIGNED: "INVALIDATED_SIGNED"
  }
};
