export const CONFIRMATION = "CONFIRMATION";

// User can continue after clicking
export const WARNING = "WARNING";

// User cannot continue after clicking
export const ERROR = "ERROR";

export const OTHER = "OTHER";
