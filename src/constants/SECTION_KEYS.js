import { APPLICATION } from "./REDUCER_TYPES";

export default {
  [APPLICATION]: {
    PERSONAL_DETAILS: "menu_person",
    RESIDENCY: "menu_residency",
    INSURABILITY: "menu_insure",
    PLAN_DETAILS: "menu_plan",
    DECLARATION: "menu_declaration"
  }
};
