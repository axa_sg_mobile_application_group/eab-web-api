import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { SUPPORTING_DOCUMENT } from "../constants/REDUCER_TYPES";

/**
 * supportingDocumentData
 * @description store the SUPPORTING_DOCUMENT data retrieved from core-api and web/app UI
 * @default { chosenList: [], notChosenList: [] }
 * */
const supportingDocument = (state = {}, { type, newSupportingDocument }) => {
  switch (type) {
    case ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA:
      return Object.assign({}, state, newSupportingDocument);
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

const pendingSubmitList = (state = [], { type, newPendingSubmitList }) => {
  switch (type) {
    case ACTION_TYPES[SUPPORTING_DOCUMENT]
      .UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST:
      return newPendingSubmitList;
    default:
      return state;
  }
};

const viewedListState = {
  appPdf: false,
  fnaReport: false,
  proposal: false
};

/**
 * error
 * @description TODO dont know what is this, but nothing important so far
 * */
const viewedList = (state = viewedListState, { type, newViewedList }) => {
  switch (type) {
    case ACTION_TYPES[SUPPORTING_DOCUMENT]
      .UPDATE_SUPPORTING_DOCUMENT_VIEWED_LIST_DATA:
      return Object.assign({}, state, newViewedList);
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * error
 * @description store the error data retrieved from core-api and web/app UI
 * */
const error = (state = {}, { type, newError }) => {
  switch (type) {
    case ACTION_TYPES[SUPPORTING_DOCUMENT].VALIDATE_SUPPORTING_DOCUMENT:
      return Object.assign({}, state, newError);
    default:
      return state;
  }
};

export default combineReducers({
  pendingSubmitList,
  supportingDocument,
  error,
  viewedList
});
