import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PROPOSAL, QUOTATION } from "../constants/REDUCER_TYPES";

/**
 * quotation
 * @description quotation main state
 * @default {}
 * */
const quotation = (state = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.quotation);
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * pdfData
 * @description pdfData main state
 * @default []
 * */
const pdfData = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return action.pdfData;
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * illustrateData
 * @description illustrateData main state
 * @default {}
 * */
const illustrateData = (state = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.illustrateData);
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * planDetails
 * @description planDetails main state
 * @default {}
 * */
const planDetails = (state = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.planDetails);
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * canRequote
 * @description canRequote main state
 * @default false
 * */
const canRequote = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return action.canRequote;
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canRequoteInvalid
 * @description canRequoteInvalid main state
 * @default false
 * */
const canRequoteInvalid = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return action.canRequoteInvalid;
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canClone
 * @description canClone main state
 * @default false
 * */
const canClone = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return action.canClone;
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canEmail
 * @description canEmail main state
 * @default false
 * */
const canEmail = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[PROPOSAL].OPEN_PROPOSAL:
      return action.canEmail;
    case ACTION_TYPES[PROPOSAL].CLOSE_PROPOSAL:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * proposalEmail
 * @description proposalEmail main state
 * @default {}
 */
const proposalEmail = (
  state = {},
  { type, emails, tab, option, reports, report, isSelected }
) => {
  switch (type) {
    case ACTION_TYPES[PROPOSAL].OPEN_EMAIL: {
      const mappedReport = reports.reduce((map, obj) => {
        map[obj.id] = true;
        return map;
      }, {});
      return Object.assign({}, state, {
        emails,
        reportOptions: {
          agent: mappedReport,
          client: mappedReport
        },
        tab: "agent"
      });
    }

    case ACTION_TYPES[QUOTATION].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        reportOptions: Object.assign({}, state.reportOptions, {
          [tab]: Object.assign({}, state.reportOptions[tab], {
            [report]: !isSelected
          })
        })
      });

    case ACTION_TYPES[QUOTATION].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab: option
      });

    default:
      return state;
  }
};

export default combineReducers({
  quotation,
  pdfData,
  illustrateData,
  planDetails,
  canRequote,
  canRequoteInvalid,
  canClone,
  canEmail,
  proposalEmail
});
