import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PRODUCTS } from "../constants/REDUCER_TYPES";
import reducer from "./products";

describe("client reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual({
        errorMsg: "",
        currency: "ALL",
        insuredCid: null,
        dependants: [],
        productList: []
      });
  });

  it("should handle CLEAN_CLIENT_DATA", () => {
    // TODO: use fake data instead of undefined
    expect(reducer(undefined, {
      type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
    }))
      .toEqual({
        errorMsg: "",
        currency: "ALL",
        insuredCid: null,
        dependants: [],
        productList: []
      });
  });
});

it("should handle UPDATE_CURRENCY", () => {
  expect(reducer(
    {},
    {
      type: ACTION_TYPES[PRODUCTS].UPDATE_CURRENCY,
      currencyData: "SGD"
    }
  )).toEqual({
    errorMsg: "",
    currency: "SGD",
    insuredCid: null,
    dependants: [],
    productList: []
  });
});

it("should handle UPDATE_INSURED_CID", () => {
  expect(reducer(
    {},
    {
      type: ACTION_TYPES[PRODUCTS].UPDATE_INSURED_CID,
      insuredCidData: "CP001003-00008"
    }
  )).toEqual({
    errorMsg: "",
    currency: "ALL",
    insuredCid: "CP001003-00008",
    dependants: [],
    productList: []
  });
});

it("should handle UPDATE_DEPENDANTS", () => {
  expect(reducer(
    {},
    {
      type: ACTION_TYPES[PRODUCTS].UPDATE_DEPENDANTS,
      dependantsData: [
        { cid: "CP001003-00008", fullName: "Alex 4 Son" },
        { cid: "CP001003-00009", fullName: "Alex 4 Mother" },
        { cid: "CP001003-00028", fullName: "Alex 4 Son xPR" },
        { cid: "CP001003-00010", fullName: "Alex 4 Father" }
      ]
    }
  )).toEqual({
    currency: "ALL",
    insuredCid: null,
    errorMsg: "",
    dependants: [
      { cid: "CP001003-00008", fullName: "Alex 4 Son" },
      { cid: "CP001003-00009", fullName: "Alex 4 Mother" },
      { cid: "CP001003-00028", fullName: "Alex 4 Son xPR" },
      { cid: "CP001003-00010", fullName: "Alex 4 Father" }
    ],
    productList: []
  });
});

it("should handle UPDATE_PRODUCT_LIST", () => {
  expect(reducer(
    {},
    {
      type: ACTION_TYPES[PRODUCTS].UPDATE_PRODUCT_LIST,
      productListData: [
        {
          title: { en: "Accident Plan" },
          seq: 4,
          products: [
            {
              compCode: "08",
              covCode: "BAA",
              covName: { en: "AXA Band Aid", "zh-Hant": "AXA Band Aid" },
              ctyGroup: ["*"],
              currencies: [{ ccy: ["SGD"], country: "*" }],
              effDate: "2017-08-01 00:00:00",
              entryAge: [
                {
                  country: "*",
                  imaxAge: 70,
                  imaxAgeUnit: "nearestAge",
                  iminAge: 15,
                  iminAgeUnit: "day",
                  omaxAge: 70,
                  omaxAgeUnit: "nearestAge",
                  ominAge: 18,
                  ominAgeUnit: "year",
                  sameAsOwner: ""
                }
              ],
              expDate: "2018-08-25 12:00:40",
              genderInd: "*",
              illustrationInd: "",
              insuredAgeDesc: { en: "15 (Attained) ~ 70 (Age Nearest Birthday)" },
              keyRisk: "",
              payModeDesc: { en: "Annual, Semi-Annual, Quarterly and Monthly" },
              polTermDesc: { en: "To Age 75" },
              premTermDesc: { en: "To Age 75" },
              prodFeature: {
                en:
                  "Provides protection against Accidental Death and Accidental Permanent Disablement in the event the Life Assured sustains a bodily Injury caused by an Accident."
              },
              productCategory: "sgResidentOnly",
              productLine: "AP",
              quotForm: "",
              scrOrderSeq: 0,
              smokeInd: "*",
              tnc: "",
              version: 2
            }
          ]
        },
        {
          title: { en: "Health Plan" },
          seq: 5,
          products: [
            {
              compCode: "08",
              covCode: "ASIM",
              covName: { en: "AXA Shield", "zh-Hant": "AXA Shield" },
              ctyGroup: ["*"],
              currencies: [{ ccy: ["SGD"], country: "*" }],
              effDate: "2017-12-01 00:00:00",
              entryAge: [
                {
                  country: "*",
                  imaxAge: 100,
                  imaxAgeUnit: "0",
                  iminAge: 0,
                  iminAgeUnit: "0",
                  omaxAge: 99,
                  omaxAgeUnit: "nextAge",
                  ominAge: 19,
                  ominAgeUnit: "nextAge",
                  sameAsOwner: ""
                }
              ],
              expDate: "2018-12-11 18:45:52",
              genderInd: "*",
              illustrationInd: "",
              insuredAgeDesc: {},
              keyRisk: "",
              payModeDesc: {},
              polTermDesc: {},
              premTermDesc: {},
              prodFeature: {},
              productCategory: "sgResidentOnly",
              productLine: "HP",
              quotForm: "",
              scrOrderSeq: 0,
              smokeInd: "*",
              tnc: "",
              version: 1
            }
          ]
        }
      ],
      errorMessage: "",
    }
  )).toEqual({
    currency: "ALL",
    insuredCid: null,
    dependants: [],
    errorMsg: "",
    productList: [
      {
        title: { en: "Accident Plan" },
        seq: 4,
        products: [
          {
            compCode: "08",
            covCode: "BAA",
            covName: { en: "AXA Band Aid", "zh-Hant": "AXA Band Aid" },
            ctyGroup: ["*"],
            currencies: [{ ccy: ["SGD"], country: "*" }],
            effDate: "2017-08-01 00:00:00",
            entryAge: [
              {
                country: "*",
                imaxAge: 70,
                imaxAgeUnit: "nearestAge",
                iminAge: 15,
                iminAgeUnit: "day",
                omaxAge: 70,
                omaxAgeUnit: "nearestAge",
                ominAge: 18,
                ominAgeUnit: "year",
                sameAsOwner: ""
              }
            ],
            expDate: "2018-08-25 12:00:40",
            genderInd: "*",
            illustrationInd: "",
            insuredAgeDesc: { en: "15 (Attained) ~ 70 (Age Nearest Birthday)" },
            keyRisk: "",
            payModeDesc: { en: "Annual, Semi-Annual, Quarterly and Monthly" },
            polTermDesc: { en: "To Age 75" },
            premTermDesc: { en: "To Age 75" },
            prodFeature: {
              en:
                "Provides protection against Accidental Death and Accidental Permanent Disablement in the event the Life Assured sustains a bodily Injury caused by an Accident."
            },
            productCategory: "sgResidentOnly",
            productLine: "AP",
            quotForm: "",
            scrOrderSeq: 0,
            smokeInd: "*",
            tnc: "",
            version: 2
          }
        ]
      },
      {
        title: { en: "Health Plan" },
        seq: 5,
        products: [
          {
            compCode: "08",
            covCode: "ASIM",
            covName: { en: "AXA Shield", "zh-Hant": "AXA Shield" },
            ctyGroup: ["*"],
            currencies: [{ ccy: ["SGD"], country: "*" }],
            effDate: "2017-12-01 00:00:00",
            entryAge: [
              {
                country: "*",
                imaxAge: 100,
                imaxAgeUnit: "0",
                iminAge: 0,
                iminAgeUnit: "0",
                omaxAge: 99,
                omaxAgeUnit: "nextAge",
                ominAge: 19,
                ominAgeUnit: "nextAge",
                sameAsOwner: ""
              }
            ],
            expDate: "2018-12-11 18:45:52",
            genderInd: "*",
            illustrationInd: "",
            insuredAgeDesc: {},
            keyRisk: "",
            payModeDesc: {},
            polTermDesc: {},
            premTermDesc: {},
            prodFeature: {},
            productCategory: "sgResidentOnly",
            productLine: "HP",
            quotForm: "",
            scrOrderSeq: 0,
            smokeInd: "*",
            tnc: "",
            version: 1
          }
        ]
      }
    ]
  });
});
