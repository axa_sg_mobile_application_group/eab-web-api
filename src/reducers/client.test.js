import ACTION_TYPES from "../constants/ACTION_TYPES";
import { CLIENT } from "../constants/REDUCER_TYPES";
import reducer from "./client";

describe("client reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      availableInsuredProfile: { hasErrorList: [] },
      profile: {},
      dependantProfiles: {},
      contactList: []
    });
  });

  it("should handle CLEAN_CLIENT_DATA", () => {
    const profileData = {
      addrBlock: "aaaaabc",
      addrCity: "T120",
      addrCountry: "R54",
      addrEstate: "",
      addrStreet: "sssssasdfsfaaba",
      age: 30,
      agentId: "008008",
      allowance: 123123,
      applicationCount: 0,
      branchInfo: {},
      bundle: [
        {
          id: "FN001003-00046",
          isValid: false
        },
        {
          id: "FN001003-00048",
          isValid: false
        },
        {
          id: "FN001003-00049",
          isValid: false
        },
        {
          id: "FN001003-00054",
          isValid: false
        },
        {
          id: "FN001003-00055",
          isValid: false
        },
        {
          id: "FN001003-00056",
          isValid: false
        },
        {
          id: "FN001003-00067",
          isValid: false
        },
        {
          id: "FN001003-00068",
          isValid: false
        },
        {
          id: "FN001003-00069",
          isValid: false
        },
        {
          id: "FN001003-00070",
          isValid: false
        },
        {
          id: "FN001003-00098",
          isValid: false
        },
        {
          id: "FN001003-00099",
          isValid: false
        },
        {
          id: "FN001003-00100",
          isValid: false
        },
        {
          id: "FN001003-00101",
          isValid: false
        },
        {
          id: "FN001003-00102",
          isValid: false
        },
        {
          id: "FN001003-00130",
          isValid: false
        },
        {
          id: "FN001003-00131",
          isValid: false
        },
        {
          id: "FN001003-00132",
          isValid: false
        },
        {
          id: "FN001003-00133",
          isValid: false
        },
        {
          id: "FN001003-00135",
          isValid: false
        },
        {
          id: "FN001003-00137",
          isValid: false
        },
        {
          id: "FN001003-00138",
          isValid: false
        },
        {
          id: "FN001003-00255",
          isValid: false
        },
        {
          id: "FN001003-00274",
          isValid: false
        },
        {
          id: "FN001003-00275",
          isValid: false
        },
        {
          id: "FN001003-00276",
          isValid: true
        }
      ],
      cid: "CP001003-00006",
      dependants: [
        {
          cid: "CP001003-00007",
          relationship: "SPO",
          relationshipOther: null
        },
        {
          cid: "CP001003-00008",
          relationship: "SON"
        },
        {
          cid: "CP001003-00009",
          relationship: "MOT"
        },
        {
          cid: "CP001003-00010",
          relationship: "FAT"
        },
        {
          cid: "CP001003-00028",
          relationship: "SON"
        }
      ],
      dob: "1987-11-24",
      education: "below",
      email: "alex.tang@eabsystems.com",
      employStatus: "ft",
      firstName: "Alex 4",
      fnaRecordIdArray: "",
      fullName: "Alex 4",
      gender: "M",
      hanyuPinyinName: "",
      haveSignDoc: true,
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I12",
      initial: "A",
      isSmoker: "Y",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-05-10T04:19:52.290Z",
      marital: "M",
      mobileCountryCode: "+65",
      mobileNo: "123123",
      nameOrder: "L",
      nationality: "N6",
      nearAge: 31,
      occupation: "O921",
      occupationOther: "bbbbbb",
      organization: "aaaa",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      otherResidenceCity: "",
      pass: "wp",
      passExpDate: 1525926035374,
      photo: "",
      postalCode: "1233466789",
      prStatus: "N",
      referrals: "",
      residenceCity: "T120",
      residenceCountry: "R54",
      title: "Mr",
      trustedIndividuals: {
        firstName: "Alex bun 5 Fa Daug",
        fullName: "Alex bun 5 Fa Daug",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileCountryCode: "+65",
        mobileNo: "12123123",
        nameOrder: "L",
        relationship: "SON",
        tiPhoto: ""
      },
      type: "cust",
      unitNum: ""
    };

    const dependantProfilesData = {
      "CP001003-00009": {
        addrBlock: "",
        addrCity: "",
        addrCountry: "R2",
        addrEstate: "",
        addrStreet: "",
        age: 31,
        agentId: "008008",
        allowance: 1,
        bundle: [
          {
            id: "FN001003-00053",
            isValid: true
          }
        ],
        cid: "CP001003-00009",
        dependants: [
          {
            cid: "CP001003-00006",
            relationship: "SON",
            relationshipOther: null
          }
        ],
        dob: "1986-12-08",
        education: "above",
        email: "a@b.com",
        employStatus: "ft",
        firstName: "Alex 4 Mother",
        fnaRecordIdArray: "",
        fullName: "Alex 4 Mother",
        gender: "F",
        hanyuPinyinName: "",
        idCardNo: "S1234567D",
        idDocType: "nric",
        industry: "I1",
        initial: "A",
        isSmoker: "N",
        isValid: true,
        language: "en",
        lastName: "",
        lastUpdateDate: "2018-04-25T03:01:16.630Z",
        marital: "M",
        mobileCountryCode: "+65",
        mobileNo: "123123",
        nameOrder: "L",
        nationality: "N1",
        nearAge: 31,
        occupation: "O1",
        organization: "d",
        organizationCountry: "R2",
        othName: "",
        otherMobileCountryCode: "+65",
        otherNo: "",
        photo: "",
        postalCode: "",
        referrals: "",
        residenceCountry: "R2",
        title: "Mrs",
        type: "cust",
        unitNum: "",
        relationship: "MOT"
      },
      "CP001003-00010": {
        addrBlock: "a",
        addrCity: "",
        addrCountry: "R2",
        addrEstate: "",
        addrStreet: "f",
        age: 42,
        agentId: "008008",
        allowance: 1,
        applicationCount: 2,
        branchInfo: {},
        bundle: [
          {
            id: "FN001003-00057",
            isValid: true
          }
        ],
        cid: "CP001003-00010",
        dependants: [
          {
            cid: "CP001003-00006",
            relationship: "SON",
            relationshipOther: null
          }
        ],
        dob: "1976-03-19",
        education: "above",
        email: "a@v.com",
        employStatus: "ft",
        firstName: "Alex 4 Father",
        fnaRecordIdArray: "",
        fullName: "Alex 4 Father",
        gender: "M",
        hanyuPinyinName: "",
        haveSignDoc: false,
        idCardNo: "S1234567D",
        idDocType: "nric",
        industry: "I1",
        initial: "A",
        isSmoker: "Y",
        isValid: true,
        language: "zh",
        lastName: "",
        lastUpdateDate: "2018-04-25T01:59:09.419Z",
        marital: "S",
        mobileCountryCode: "+65",
        mobileNo: "12123123",
        nameOrder: "L",
        nationality: "N1",
        nearAge: 42,
        occupation: "O1",
        organization: "aaaaaa",
        organizationCountry: "R2",
        othName: "",
        otherMobileCountryCode: "+65",
        otherNo: "",
        photo: "",
        postalCode: "111",
        referrals: "",
        residenceCountry: "R2",
        title: "Mr",
        type: "cust",
        unitNum: "",
        relationship: "FAT"
      },
      "CP001003-00028": {
        addrBlock: "",
        addrCity: "",
        addrCountry: "R2",
        addrEstate: "",
        addrStreet: "",
        age: 2,
        agentCode: "008008",
        agentId: "008008",
        allowance: 1,
        bundle: [
          {
            id: "FN001003-00111",
            isValid: true
          }
        ],
        cid: "CP001003-00028",
        compCode: "01",
        dealerGroup: "AGENCY",
        dependants: [
          {
            cid: "CP001003-00006",
            relationship: "FAT",
            relationshipOther: null
          }
        ],
        dob: "2016-01-16",
        education: "",
        email: "",
        employStatus: "",
        firstName: "Alex 4 Son xPR",
        fnaRecordIdArray: "",
        fullName: "Alex 4 Son xPR",
        gender: "M",
        hanyuPinyinName: "",
        idCardNo: "S1234567D",
        idDocType: "nric",
        industry: "I1",
        initial: "A",
        isSmoker: "N",
        isValid: true,
        language: "zh",
        lastName: "",
        lastUpdateDate: "2018-04-25T01:59:09.419Z",
        marital: "S",
        mobileCountryCode: "+65",
        mobileNo: "",
        nameOrder: "L",
        nationality: "N3",
        nearAge: 2,
        occupation: "O1",
        organization: "a",
        organizationCountry: "R2",
        othName: "",
        otherMobileCountryCode: "+65",
        otherNo: "",
        photo: "",
        postalCode: "",
        prStatus: "Y",
        referrals: "",
        residenceCountry: "R2",
        title: "Mr",
        type: "cust",
        unitNum: "",
        relationship: "SON"
      },
      "CP001003-00008": {
        addrBlock: "same block h",
        addrCity: "asdfasdfsad",
        addrCountry: "R2",
        addrEstate: "asdfvasdf",
        addrStreet: "asdfsdfas",
        age: 3,
        agentId: "008008",
        allowance: 1,
        bundle: [
          {
            id: "FN001003-00052",
            isValid: true
          }
        ],
        cid: "CP001003-00008",
        dependants: [
          {
            cid: "CP001003-00006",
            relationship: "FAT",
            relationshipOther: null
          }
        ],
        dob: "2014-12-30",
        education: "below",
        email: "ma@b.co",
        employStatus: "sd",
        firstName: "Alex 4 Son",
        fnaRecordIdArray: "",
        fullName: "Alex 4 Son",
        gender: "M",
        hanyuPinyinName: "",
        idCardNo: "S1234567D",
        idDocType: "nric",
        industry: "I1",
        initial: "A",
        isSmoker: "N",
        isValid: true,
        language: "en",
        lastName: "",
        lastUpdateDate: "2018-05-10T04:21:37.765Z",
        marital: "S",
        mobileCountryCode: "+65",
        mobileNo: "123123",
        nameOrder: "L",
        nationality: "N3",
        nearAge: 3,
        occupation: "O1",
        organization: "bbbbbbbb",
        organizationCountry: "R2",
        othName: "",
        otherMobileCountryCode: "+65",
        otherNo: "",
        pass: "ep",
        passExpDate: 1525925562329,
        photo: "",
        postalCode: "9000",
        prStatus: "N",
        referrals: "",
        residenceCountry: "R2",
        title: "Mr",
        type: "cust",
        unitNum: "",
        relationship: "SON"
      },
      "CP001003-00007": {
        addrBlock: "aaaaa",
        addrCity: "",
        addrCountry: "R2",
        addrEstate: "",
        addrStreet: "sssss",
        age: 31,
        agentId: "008008",
        allowance: 1111,
        applicationCount: 0,
        bundle: [
          {
            id: "FN001003-00047",
            isValid: false
          },
          {
            id: "FN001003-00266",
            isValid: false
          },
          {
            id: "FN001003-00268",
            isValid: false
          },
          {
            id: "FN001003-00270",
            isValid: false
          },
          {
            id: "FN001003-00272",
            isValid: false
          },
          {
            id: "FN001003-00273",
            isValid: false
          },
          {
            id: "FN001003-00277",
            isValid: true
          }
        ],
        cid: "CP001003-00007",
        dependants: [
          {
            cid: "CP001003-00006",
            relationship: "SPO",
            relationshipOther: null
          }
        ],
        dob: "1986-11-22",
        education: "below",
        email: "a@b.com",
        employStatus: "ft",
        firstName: "Alex 4 SP",
        fnaRecordIdArray: "",
        fullName: "Alex 4 SP",
        gender: "F",
        hanyuPinyinName: "",
        haveSignDoc: true,
        idCardNo: "S1234567D",
        idDocType: "nric",
        industry: "I12",
        initial: "A",
        isSmoker: "Y",
        isValid: true,
        language: "zh",
        lastName: "",
        lastUpdateDate: "2018-05-03T03:10:54.624Z",
        marital: "M",
        mobileCountryCode: "+65",
        mobileNo: "12312123",
        nameOrder: "L",
        nationality: "N3",
        nearAge: 32,
        occupation: "O921",
        occupationOther: "aaaaaaa",
        organization: "bbbbc",
        organizationCountry: "R2",
        othName: "",
        otherMobileCountryCode: "+65",
        otherNo: "",
        photo: "",
        postalCode: "1233333",
        prStatus: "Y",
        referrals: "",
        residenceCountry: "R2",
        title: "Mrs",
        trustedIndividuals: {
          firstName: "Alex 4 Father",
          fullName: "Alex 4 Father",
          idCardNo: "S1234567D",
          idDocType: "nric",
          lastName: "",
          mobileCountryCode: "+65",
          mobileNo: "12123123",
          nameOrder: "L",
          relationship: "FAT",
          tiPhoto: ""
        },
        type: "cust",
        unitNum: "Unit 1233",
        relationship: "SPO"
      }
    };

    const contactListData = [
      {
        applicationCount: 0,
        email: "alex.tang@eabsystems.com",
        firstName: "Alex 4",
        fullName: "Alex 4",
        id: "CP001003-00006",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Alex 4 SP",
        fullName: "Alex 4 SP",
        id: "CP001003-00007",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "12312123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "ma@b.co",
        firstName: "Alex 4 Son",
        fullName: "Alex 4 Son",
        id: "CP001003-00008",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Alex 4 Mother",
        fullName: "Alex 4 Mother",
        id: "CP001003-00009",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 2,
        email: "a@v.com",
        firstName: "Alex 4 Father",
        fullName: "Alex 4 Father",
        id: "CP001003-00010",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "12123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.ocm",
        firstName: "Alex TI",
        fullName: "Alex TI",
        id: "CP001003-00011",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "111111",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "",
        firstName: "Alex TI Son",
        fullName: "Alex TI Son",
        id: "CP001003-00012",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "alex.tang@eabsystems.com",
        firstName: "Alex Shield",
        fullName: "Alex Shield",
        id: "CP001003-00013",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "12312312",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "aaa@b.com",
        firstName: "Alex Shield SP",
        fullName: "Alex Shield SP",
        id: "CP001003-00014",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "1231231",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Alex Shield Son",
        fullName: "Alex Shield Son",
        id: "CP001003-00015",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "12312312",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "s@v.com",
        firstName: "Alex Shield Father",
        fullName: "Alex Shield Father",
        id: "CP001003-00016",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 1,
        email: "s@b.com",
        firstName: "Alex Shield Mother",
        fullName: "Alex Shield Mother",
        id: "CP001003-00017",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "1231231",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "",
        firstName: "Alex Shield Grandma",
        fullName: "Alex Shield Grandma",
        id: "CP001003-00018",
        idCardNo: "",
        idDocType: "nric",
        lastName: "Alex",
        mobileNo: "",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@n.om",
        firstName: "Alex Shield Daughter",
        fullName: "Alex Shield Daughter",
        id: "CP001003-00020",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "1123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Alex Shield Grandfather",
        fullName: "Alex Shield Grandfather",
        id: "CP001003-00021",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "13123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 1,
        email: "alex.tang@eabsystems.com",
        firstName: "Test Shield",
        fullName: "Test Shield",
        id: "CP001003-00022",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "1231231",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Test Shield Son",
        fullName: "Test Shield Son",
        id: "CP001003-00023",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "1231231",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Test Shield Daughter",
        fullName: "Test Shield Daughter",
        id: "CP001003-00024",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 1,
        email: "alex.tang@eabsystems.com",
        firstName: "Test Shield SP",
        fullName: "Test Shield SP",
        id: "CP001003-00025",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.com",
        firstName: "Test Shield Son 2",
        fullName: "Test Shield Son 2",
        id: "CP001003-00026",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileNo: "123123",
        nameOrder: "L",
        photo: ""
      },
      {
        applicationCount: 0,
        email: "a@b.cc",
        firstName: "Alex Shield GrandMother long ",
        fullName: "long long long name Alex Shiel",
        id: "CP001003-00027",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "long long long name",
        mobileNo: "7668768",
        nameOrder: "L",
        photo: ""
      }
    ];

    expect(
      reducer(
        {
          availableInsuredProfile: { hasErrorList: [] },
          profile: profileData,
          dependantProfiles: dependantProfilesData,
          contactList: contactListData
        },
        {
          type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
        }
      )
    ).toEqual({
      availableInsuredProfile: { hasErrorList: [] },
      profile: {},
      dependantProfiles: {},
      contactList: []
    });
  });
});

it("should handle CLEAN_CLIENT_DATA", () => {
  const profile = {
    addrBlock: "aaaaabc",
    addrCity: "T120",
    addrCountry: "R54",
    addrEstate: "",
    addrStreet: "sssssasdfsfaaba",
    age: 30,
    agentId: "008008",
    allowance: 123123,
    applicationCount: 0,
    branchInfo: {},
    bundle: [
      {
        id: "FN001003-00046",
        isValid: false
      },
      {
        id: "FN001003-00048",
        isValid: false
      },
      {
        id: "FN001003-00049",
        isValid: false
      },
      {
        id: "FN001003-00054",
        isValid: false
      },
      {
        id: "FN001003-00055",
        isValid: false
      },
      {
        id: "FN001003-00056",
        isValid: false
      },
      {
        id: "FN001003-00067",
        isValid: false
      },
      {
        id: "FN001003-00068",
        isValid: false
      },
      {
        id: "FN001003-00069",
        isValid: false
      },
      {
        id: "FN001003-00070",
        isValid: false
      },
      {
        id: "FN001003-00098",
        isValid: false
      },
      {
        id: "FN001003-00099",
        isValid: false
      },
      {
        id: "FN001003-00100",
        isValid: false
      },
      {
        id: "FN001003-00101",
        isValid: false
      },
      {
        id: "FN001003-00102",
        isValid: false
      },
      {
        id: "FN001003-00130",
        isValid: false
      },
      {
        id: "FN001003-00131",
        isValid: false
      },
      {
        id: "FN001003-00132",
        isValid: false
      },
      {
        id: "FN001003-00133",
        isValid: false
      },
      {
        id: "FN001003-00135",
        isValid: false
      },
      {
        id: "FN001003-00137",
        isValid: false
      },
      {
        id: "FN001003-00138",
        isValid: false
      },
      {
        id: "FN001003-00255",
        isValid: false
      },
      {
        id: "FN001003-00274",
        isValid: false
      },
      {
        id: "FN001003-00275",
        isValid: false
      },
      {
        id: "FN001003-00276",
        isValid: true
      }
    ],
    cid: "CP001003-00006",
    dependants: [
      {
        cid: "CP001003-00007",
        relationship: "SPO",
        relationshipOther: null
      },
      {
        cid: "CP001003-00008",
        relationship: "SON"
      },
      {
        cid: "CP001003-00009",
        relationship: "MOT"
      },
      {
        cid: "CP001003-00010",
        relationship: "FAT"
      },
      {
        cid: "CP001003-00028",
        relationship: "SON"
      }
    ],
    dob: "1987-11-24",
    education: "below",
    email: "alex.tang@eabsystems.com",
    employStatus: "ft",
    firstName: "Alex 4",
    fnaRecordIdArray: "",
    fullName: "Alex 4",
    gender: "M",
    hanyuPinyinName: "",
    haveSignDoc: true,
    idCardNo: "S1234567D",
    idDocType: "nric",
    industry: "I12",
    initial: "A",
    isSmoker: "Y",
    isValid: true,
    language: "zh",
    lastName: "",
    lastUpdateDate: "2018-05-10T04:19:52.290Z",
    marital: "M",
    mobileCountryCode: "+65",
    mobileNo: "123123",
    nameOrder: "L",
    nationality: "N6",
    nearAge: 31,
    occupation: "O921",
    occupationOther: "bbbbbb",
    organization: "aaaa",
    organizationCountry: "R2",
    othName: "",
    otherMobileCountryCode: "+65",
    otherNo: "",
    otherResidenceCity: "",
    pass: "wp",
    passExpDate: 1525926035374,
    photo: "",
    postalCode: "1233466789",
    prStatus: "N",
    referrals: "",
    residenceCity: "T120",
    residenceCountry: "R54",
    title: "Mr",
    trustedIndividuals: {
      firstName: "Alex bun 5 Fa Daug",
      fullName: "Alex bun 5 Fa Daug",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileCountryCode: "+65",
      mobileNo: "12123123",
      nameOrder: "L",
      relationship: "SON",
      tiPhoto: ""
    },
    type: "cust",
    unitNum: ""
  };
  const dependantProfiles = {
    "CP001003-00009": {
      addrBlock: "",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "",
      age: 31,
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00053",
          isValid: true
        }
      ],
      cid: "CP001003-00009",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SON",
          relationshipOther: null
        }
      ],
      dob: "1986-12-08",
      education: "above",
      email: "a@b.com",
      employStatus: "ft",
      firstName: "Alex 4 Mother",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Mother",
      gender: "F",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "en",
      lastName: "",
      lastUpdateDate: "2018-04-25T03:01:16.630Z",
      marital: "M",
      mobileCountryCode: "+65",
      mobileNo: "123123",
      nameOrder: "L",
      nationality: "N1",
      nearAge: 31,
      occupation: "O1",
      organization: "d",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "",
      referrals: "",
      residenceCountry: "R2",
      title: "Mrs",
      type: "cust",
      unitNum: "",
      relationship: "MOT"
    },
    "CP001003-00010": {
      addrBlock: "a",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "f",
      age: 42,
      agentId: "008008",
      allowance: 1,
      applicationCount: 2,
      branchInfo: {},
      bundle: [
        {
          id: "FN001003-00057",
          isValid: true
        }
      ],
      cid: "CP001003-00010",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SON",
          relationshipOther: null
        }
      ],
      dob: "1976-03-19",
      education: "above",
      email: "a@v.com",
      employStatus: "ft",
      firstName: "Alex 4 Father",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Father",
      gender: "M",
      hanyuPinyinName: "",
      haveSignDoc: false,
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "Y",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-04-25T01:59:09.419Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "12123123",
      nameOrder: "L",
      nationality: "N1",
      nearAge: 42,
      occupation: "O1",
      organization: "aaaaaa",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "111",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "FAT"
    },
    "CP001003-00028": {
      addrBlock: "",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "",
      age: 2,
      agentCode: "008008",
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00111",
          isValid: true
        }
      ],
      cid: "CP001003-00028",
      compCode: "01",
      dealerGroup: "AGENCY",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "FAT",
          relationshipOther: null
        }
      ],
      dob: "2016-01-16",
      education: "",
      email: "",
      employStatus: "",
      firstName: "Alex 4 Son xPR",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Son xPR",
      gender: "M",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-04-25T01:59:09.419Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 2,
      occupation: "O1",
      organization: "a",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "",
      prStatus: "Y",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "SON"
    },
    "CP001003-00008": {
      addrBlock: "same block h",
      addrCity: "asdfasdfsad",
      addrCountry: "R2",
      addrEstate: "asdfvasdf",
      addrStreet: "asdfsdfas",
      age: 3,
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00052",
          isValid: true
        }
      ],
      cid: "CP001003-00008",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "FAT",
          relationshipOther: null
        }
      ],
      dob: "2014-12-30",
      education: "below",
      email: "ma@b.co",
      employStatus: "sd",
      firstName: "Alex 4 Son",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Son",
      gender: "M",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "en",
      lastName: "",
      lastUpdateDate: "2018-05-10T04:21:37.765Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "123123",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 3,
      occupation: "O1",
      organization: "bbbbbbbb",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      pass: "ep",
      passExpDate: 1525925562329,
      photo: "",
      postalCode: "9000",
      prStatus: "N",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "SON"
    },
    "CP001003-00007": {
      addrBlock: "aaaaa",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "sssss",
      age: 31,
      agentId: "008008",
      allowance: 1111,
      applicationCount: 0,
      bundle: [
        {
          id: "FN001003-00047",
          isValid: false
        },
        {
          id: "FN001003-00266",
          isValid: false
        },
        {
          id: "FN001003-00268",
          isValid: false
        },
        {
          id: "FN001003-00270",
          isValid: false
        },
        {
          id: "FN001003-00272",
          isValid: false
        },
        {
          id: "FN001003-00273",
          isValid: false
        },
        {
          id: "FN001003-00277",
          isValid: true
        }
      ],
      cid: "CP001003-00007",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SPO",
          relationshipOther: null
        }
      ],
      dob: "1986-11-22",
      education: "below",
      email: "a@b.com",
      employStatus: "ft",
      firstName: "Alex 4 SP",
      fnaRecordIdArray: "",
      fullName: "Alex 4 SP",
      gender: "F",
      hanyuPinyinName: "",
      haveSignDoc: true,
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I12",
      initial: "A",
      isSmoker: "Y",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-05-03T03:10:54.624Z",
      marital: "M",
      mobileCountryCode: "+65",
      mobileNo: "12312123",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 32,
      occupation: "O921",
      occupationOther: "aaaaaaa",
      organization: "bbbbc",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "1233333",
      prStatus: "Y",
      referrals: "",
      residenceCountry: "R2",
      title: "Mrs",
      trustedIndividuals: {
        firstName: "Alex 4 Father",
        fullName: "Alex 4 Father",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileCountryCode: "+65",
        mobileNo: "12123123",
        nameOrder: "L",
        relationship: "FAT",
        tiPhoto: ""
      },
      type: "cust",
      unitNum: "Unit 1233",
      relationship: "SPO"
    }
  };
  const contactList = [
    {
      applicationCount: 0,
      email: "alex.tang@eabsystems.com",
      firstName: "Alex 4",
      fullName: "Alex 4",
      id: "CP001003-00006",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex 4 SP",
      fullName: "Alex 4 SP",
      id: "CP001003-00007",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "ma@b.co",
      firstName: "Alex 4 Son",
      fullName: "Alex 4 Son",
      id: "CP001003-00008",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex 4 Mother",
      fullName: "Alex 4 Mother",
      id: "CP001003-00009",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 2,
      email: "a@v.com",
      firstName: "Alex 4 Father",
      fullName: "Alex 4 Father",
      id: "CP001003-00010",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.ocm",
      firstName: "Alex TI",
      fullName: "Alex TI",
      id: "CP001003-00011",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "111111",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "",
      firstName: "Alex TI Son",
      fullName: "Alex TI Son",
      id: "CP001003-00012",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "alex.tang@eabsystems.com",
      firstName: "Alex Shield",
      fullName: "Alex Shield",
      id: "CP001003-00013",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312312",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "aaa@b.com",
      firstName: "Alex Shield SP",
      fullName: "Alex Shield SP",
      id: "CP001003-00014",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex Shield Son",
      fullName: "Alex Shield Son",
      id: "CP001003-00015",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312312",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "s@v.com",
      firstName: "Alex Shield Father",
      fullName: "Alex Shield Father",
      id: "CP001003-00016",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "s@b.com",
      firstName: "Alex Shield Mother",
      fullName: "Alex Shield Mother",
      id: "CP001003-00017",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "",
      firstName: "Alex Shield Grandma",
      fullName: "Alex Shield Grandma",
      id: "CP001003-00018",
      idCardNo: "",
      idDocType: "nric",
      lastName: "Alex",
      mobileNo: "",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@n.om",
      firstName: "Alex Shield Daughter",
      fullName: "Alex Shield Daughter",
      id: "CP001003-00020",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex Shield Grandfather",
      fullName: "Alex Shield Grandfather",
      id: "CP001003-00021",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "13123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "alex.tang@eabsystems.com",
      firstName: "Test Shield",
      fullName: "Test Shield",
      id: "CP001003-00022",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Son",
      fullName: "Test Shield Son",
      id: "CP001003-00023",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Daughter",
      fullName: "Test Shield Daughter",
      id: "CP001003-00024",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "alex.tang@eabsystems.com",
      firstName: "Test Shield SP",
      fullName: "Test Shield SP",
      id: "CP001003-00025",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Son 2",
      fullName: "Test Shield Son 2",
      id: "CP001003-00026",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.cc",
      firstName: "Alex Shield GrandMother long ",
      fullName: "long long long name Alex Shiel",
      id: "CP001003-00027",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "long long long name",
      mobileNo: "7668768",
      nameOrder: "L",
      photo: ""
    }
  ];
  const availableInsuredProfile = {
    hasErrorList: [
      {
        cid: "CP001003-00027",
        errorFields: ["isIndustryError", "isOccupationError"],
        hasError: true
      },
      {
        cid: "CP201007-00002",
        errorFields: ["isIndustryError", "isOccupationError"],
        hasError: true
      }
    ]
  };

  expect(
    reducer(
      {
        availableInsuredProfile,
        profile,
        dependantProfiles,
        contactList
      },
      {
        type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
      }
    )
  ).toEqual({
    availableInsuredProfile: { hasErrorList: [] },
    profile: {},
    dependantProfiles: {},
    contactList: []
  });
});

it("should handle UPDATE_PROFILE", () => {
  const profileData = {
    addrBlock: "aaaaabc",
    addrCity: "T120",
    addrCountry: "R54",
    addrEstate: "",
    addrStreet: "sssssasdfsfaaba",
    age: 30,
    agentId: "008008",
    allowance: 123123,
    applicationCount: 0,
    branchInfo: {},
    bundle: [
      {
        id: "FN001003-00046",
        isValid: false
      },
      {
        id: "FN001003-00048",
        isValid: false
      },
      {
        id: "FN001003-00049",
        isValid: false
      },
      {
        id: "FN001003-00054",
        isValid: false
      },
      {
        id: "FN001003-00055",
        isValid: false
      },
      {
        id: "FN001003-00056",
        isValid: false
      },
      {
        id: "FN001003-00067",
        isValid: false
      },
      {
        id: "FN001003-00068",
        isValid: false
      },
      {
        id: "FN001003-00069",
        isValid: false
      },
      {
        id: "FN001003-00070",
        isValid: false
      },
      {
        id: "FN001003-00098",
        isValid: false
      },
      {
        id: "FN001003-00099",
        isValid: false
      },
      {
        id: "FN001003-00100",
        isValid: false
      },
      {
        id: "FN001003-00101",
        isValid: false
      },
      {
        id: "FN001003-00102",
        isValid: false
      },
      {
        id: "FN001003-00130",
        isValid: false
      },
      {
        id: "FN001003-00131",
        isValid: false
      },
      {
        id: "FN001003-00132",
        isValid: false
      },
      {
        id: "FN001003-00133",
        isValid: false
      },
      {
        id: "FN001003-00135",
        isValid: false
      },
      {
        id: "FN001003-00137",
        isValid: false
      },
      {
        id: "FN001003-00138",
        isValid: false
      },
      {
        id: "FN001003-00255",
        isValid: false
      },
      {
        id: "FN001003-00274",
        isValid: false
      },
      {
        id: "FN001003-00275",
        isValid: false
      },
      {
        id: "FN001003-00276",
        isValid: true
      }
    ],
    cid: "CP001003-00006",
    dependants: [
      {
        cid: "CP001003-00007",
        relationship: "SPO",
        relationshipOther: null
      },
      {
        cid: "CP001003-00008",
        relationship: "SON"
      },
      {
        cid: "CP001003-00009",
        relationship: "MOT"
      },
      {
        cid: "CP001003-00010",
        relationship: "FAT"
      },
      {
        cid: "CP001003-00028",
        relationship: "SON"
      }
    ],
    dob: "1987-11-24",
    education: "below",
    email: "alex.tang@eabsystems.com",
    employStatus: "ft",
    firstName: "Alex 4",
    fnaRecordIdArray: "",
    fullName: "Alex 4",
    gender: "M",
    hanyuPinyinName: "",
    haveSignDoc: true,
    idCardNo: "S1234567D",
    idDocType: "nric",
    industry: "I12",
    initial: "A",
    isSmoker: "Y",
    isValid: true,
    language: "zh",
    lastName: "",
    lastUpdateDate: "2018-05-10T04:19:52.290Z",
    marital: "M",
    mobileCountryCode: "+65",
    mobileNo: "123123",
    nameOrder: "L",
    nationality: "N6",
    nearAge: 31,
    occupation: "O921",
    occupationOther: "bbbbbb",
    organization: "aaaa",
    organizationCountry: "R2",
    othName: "",
    otherMobileCountryCode: "+65",
    otherNo: "",
    otherResidenceCity: "",
    pass: "wp",
    passExpDate: 1525926035374,
    photo: "",
    postalCode: "1233466789",
    prStatus: "N",
    referrals: "",
    residenceCity: "T120",
    residenceCountry: "R54",
    title: "Mr",
    trustedIndividuals: {
      firstName: "Alex bun 5 Fa Daug",
      fullName: "Alex bun 5 Fa Daug",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileCountryCode: "+65",
      mobileNo: "12123123",
      nameOrder: "L",
      relationship: "SON",
      tiPhoto: ""
    },
    type: "cust",
    unitNum: ""
  };

  expect(
    reducer(
      {},
      {
        type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
        profileData
      }
    )
  ).toEqual({
    availableInsuredProfile: { hasErrorList: [] },
    profile: profileData,
    dependantProfiles: {},
    contactList: []
  });
});

it("should handle UPDATE_DEPENDANT_PROFILES", () => {
  const dependantProfilesData = {
    "CP001003-00009": {
      addrBlock: "",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "",
      age: 31,
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00053",
          isValid: true
        }
      ],
      cid: "CP001003-00009",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SON",
          relationshipOther: null
        }
      ],
      dob: "1986-12-08",
      education: "above",
      email: "a@b.com",
      employStatus: "ft",
      firstName: "Alex 4 Mother",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Mother",
      gender: "F",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "en",
      lastName: "",
      lastUpdateDate: "2018-04-25T03:01:16.630Z",
      marital: "M",
      mobileCountryCode: "+65",
      mobileNo: "123123",
      nameOrder: "L",
      nationality: "N1",
      nearAge: 31,
      occupation: "O1",
      organization: "d",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "",
      referrals: "",
      residenceCountry: "R2",
      title: "Mrs",
      type: "cust",
      unitNum: "",
      relationship: "MOT"
    },
    "CP001003-00010": {
      addrBlock: "a",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "f",
      age: 42,
      agentId: "008008",
      allowance: 1,
      applicationCount: 2,
      branchInfo: {},
      bundle: [
        {
          id: "FN001003-00057",
          isValid: true
        }
      ],
      cid: "CP001003-00010",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SON",
          relationshipOther: null
        }
      ],
      dob: "1976-03-19",
      education: "above",
      email: "a@v.com",
      employStatus: "ft",
      firstName: "Alex 4 Father",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Father",
      gender: "M",
      hanyuPinyinName: "",
      haveSignDoc: false,
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "Y",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-04-25T01:59:09.419Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "12123123",
      nameOrder: "L",
      nationality: "N1",
      nearAge: 42,
      occupation: "O1",
      organization: "aaaaaa",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "111",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "FAT"
    },
    "CP001003-00028": {
      addrBlock: "",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "",
      age: 2,
      agentCode: "008008",
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00111",
          isValid: true
        }
      ],
      cid: "CP001003-00028",
      compCode: "01",
      dealerGroup: "AGENCY",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "FAT",
          relationshipOther: null
        }
      ],
      dob: "2016-01-16",
      education: "",
      email: "",
      employStatus: "",
      firstName: "Alex 4 Son xPR",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Son xPR",
      gender: "M",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-04-25T01:59:09.419Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 2,
      occupation: "O1",
      organization: "a",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "",
      prStatus: "Y",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "SON"
    },
    "CP001003-00008": {
      addrBlock: "same block h",
      addrCity: "asdfasdfsad",
      addrCountry: "R2",
      addrEstate: "asdfvasdf",
      addrStreet: "asdfsdfas",
      age: 3,
      agentId: "008008",
      allowance: 1,
      bundle: [
        {
          id: "FN001003-00052",
          isValid: true
        }
      ],
      cid: "CP001003-00008",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "FAT",
          relationshipOther: null
        }
      ],
      dob: "2014-12-30",
      education: "below",
      email: "ma@b.co",
      employStatus: "sd",
      firstName: "Alex 4 Son",
      fnaRecordIdArray: "",
      fullName: "Alex 4 Son",
      gender: "M",
      hanyuPinyinName: "",
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I1",
      initial: "A",
      isSmoker: "N",
      isValid: true,
      language: "en",
      lastName: "",
      lastUpdateDate: "2018-05-10T04:21:37.765Z",
      marital: "S",
      mobileCountryCode: "+65",
      mobileNo: "123123",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 3,
      occupation: "O1",
      organization: "bbbbbbbb",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      pass: "ep",
      passExpDate: 1525925562329,
      photo: "",
      postalCode: "9000",
      prStatus: "N",
      referrals: "",
      residenceCountry: "R2",
      title: "Mr",
      type: "cust",
      unitNum: "",
      relationship: "SON"
    },
    "CP001003-00007": {
      addrBlock: "aaaaa",
      addrCity: "",
      addrCountry: "R2",
      addrEstate: "",
      addrStreet: "sssss",
      age: 31,
      agentId: "008008",
      allowance: 1111,
      applicationCount: 0,
      bundle: [
        {
          id: "FN001003-00047",
          isValid: false
        },
        {
          id: "FN001003-00266",
          isValid: false
        },
        {
          id: "FN001003-00268",
          isValid: false
        },
        {
          id: "FN001003-00270",
          isValid: false
        },
        {
          id: "FN001003-00272",
          isValid: false
        },
        {
          id: "FN001003-00273",
          isValid: false
        },
        {
          id: "FN001003-00277",
          isValid: true
        }
      ],
      cid: "CP001003-00007",
      dependants: [
        {
          cid: "CP001003-00006",
          relationship: "SPO",
          relationshipOther: null
        }
      ],
      dob: "1986-11-22",
      education: "below",
      email: "a@b.com",
      employStatus: "ft",
      firstName: "Alex 4 SP",
      fnaRecordIdArray: "",
      fullName: "Alex 4 SP",
      gender: "F",
      hanyuPinyinName: "",
      haveSignDoc: true,
      idCardNo: "S1234567D",
      idDocType: "nric",
      industry: "I12",
      initial: "A",
      isSmoker: "Y",
      isValid: true,
      language: "zh",
      lastName: "",
      lastUpdateDate: "2018-05-03T03:10:54.624Z",
      marital: "M",
      mobileCountryCode: "+65",
      mobileNo: "12312123",
      nameOrder: "L",
      nationality: "N3",
      nearAge: 32,
      occupation: "O921",
      occupationOther: "aaaaaaa",
      organization: "bbbbc",
      organizationCountry: "R2",
      othName: "",
      otherMobileCountryCode: "+65",
      otherNo: "",
      photo: "",
      postalCode: "1233333",
      prStatus: "Y",
      referrals: "",
      residenceCountry: "R2",
      title: "Mrs",
      trustedIndividuals: {
        firstName: "Alex 4 Father",
        fullName: "Alex 4 Father",
        idCardNo: "S1234567D",
        idDocType: "nric",
        lastName: "",
        mobileCountryCode: "+65",
        mobileNo: "12123123",
        nameOrder: "L",
        relationship: "FAT",
        tiPhoto: ""
      },
      type: "cust",
      unitNum: "Unit 1233",
      relationship: "SPO"
    }
  };

  expect(
    reducer(
      {},
      {
        type: ACTION_TYPES[CLIENT].UPDATE_DEPENDANT_PROFILES,
        dependantProfilesData
      }
    )
  ).toEqual({
    availableInsuredProfile: { hasErrorList: [] },
    profile: {},
    dependantProfiles: dependantProfilesData,
    contactList: []
  });
});

it("should handle UPDATE_CONTACT_LIST", () => {
  const contactListData = [
    {
      applicationCount: 0,
      email: "alex.tang@eabsystems.com",
      firstName: "Alex 4",
      fullName: "Alex 4",
      id: "CP001003-00006",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex 4 SP",
      fullName: "Alex 4 SP",
      id: "CP001003-00007",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "ma@b.co",
      firstName: "Alex 4 Son",
      fullName: "Alex 4 Son",
      id: "CP001003-00008",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex 4 Mother",
      fullName: "Alex 4 Mother",
      id: "CP001003-00009",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 2,
      email: "a@v.com",
      firstName: "Alex 4 Father",
      fullName: "Alex 4 Father",
      id: "CP001003-00010",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.ocm",
      firstName: "Alex TI",
      fullName: "Alex TI",
      id: "CP001003-00011",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "111111",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "",
      firstName: "Alex TI Son",
      fullName: "Alex TI Son",
      id: "CP001003-00012",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "alex.tang@eabsystems.com",
      firstName: "Alex Shield",
      fullName: "Alex Shield",
      id: "CP001003-00013",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312312",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "aaa@b.com",
      firstName: "Alex Shield SP",
      fullName: "Alex Shield SP",
      id: "CP001003-00014",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex Shield Son",
      fullName: "Alex Shield Son",
      id: "CP001003-00015",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "12312312",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "s@v.com",
      firstName: "Alex Shield Father",
      fullName: "Alex Shield Father",
      id: "CP001003-00016",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "s@b.com",
      firstName: "Alex Shield Mother",
      fullName: "Alex Shield Mother",
      id: "CP001003-00017",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "",
      firstName: "Alex Shield Grandma",
      fullName: "Alex Shield Grandma",
      id: "CP001003-00018",
      idCardNo: "",
      idDocType: "nric",
      lastName: "Alex",
      mobileNo: "",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@n.om",
      firstName: "Alex Shield Daughter",
      fullName: "Alex Shield Daughter",
      id: "CP001003-00020",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Alex Shield Grandfather",
      fullName: "Alex Shield Grandfather",
      id: "CP001003-00021",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "13123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "alex.tang@eabsystems.com",
      firstName: "Test Shield",
      fullName: "Test Shield",
      id: "CP001003-00022",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Son",
      fullName: "Test Shield Son",
      id: "CP001003-00023",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "1231231",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Daughter",
      fullName: "Test Shield Daughter",
      id: "CP001003-00024",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 1,
      email: "alex.tang@eabsystems.com",
      firstName: "Test Shield SP",
      fullName: "Test Shield SP",
      id: "CP001003-00025",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.com",
      firstName: "Test Shield Son 2",
      fullName: "Test Shield Son 2",
      id: "CP001003-00026",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "",
      mobileNo: "123123",
      nameOrder: "L",
      photo: ""
    },
    {
      applicationCount: 0,
      email: "a@b.cc",
      firstName: "Alex Shield GrandMother long ",
      fullName: "long long long name Alex Shiel",
      id: "CP001003-00027",
      idCardNo: "S1234567D",
      idDocType: "nric",
      lastName: "long long long name",
      mobileNo: "7668768",
      nameOrder: "L",
      photo: ""
    }
  ];

  expect(
    reducer(
      {},
      {
        type: ACTION_TYPES[CLIENT].UPDATE_CONTACT_LIST,
        contactListData
      }
    )
  ).toEqual({
    availableInsuredProfile: { hasErrorList: [] },
    profile: {},
    dependantProfiles: {},
    contactList: contactListData
  });
});
