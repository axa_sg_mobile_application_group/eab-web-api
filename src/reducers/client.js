import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { CLIENT, FNA } from "../constants/REDUCER_TYPES";

/**
 * profile
 * @default {}
 * */
const profile = (state = {}, { type, profileData }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT].UPDATE_CLIENT_AND_DEPENDANTS:
    case ACTION_TYPES[CLIENT].UPDATE_PROFILE:
      return profileData;
    case ACTION_TYPES[FNA].SAVE_FNA_COMPLETE:
      return profileData !== undefined ? profileData : state;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * dependantProfiles
 * @default {}
 * */
const dependantProfiles = (
  // TODO remove mock data when core is ready
  state = {},
  { type, dependantProfilesData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT].UPDATE_CLIENT_AND_DEPENDANTS:
    case ACTION_TYPES[CLIENT].UPDATE_DEPENDANT_PROFILES:
      return dependantProfilesData;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * contactList
 * @default []
 * */
const contactList = (state = [], { type, contactListData }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT].UPDATE_CONTACT_LIST:
      return contactListData;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

const initAvailableInsuredProfile = {
  hasErrorList: []
};

const availableInsuredProfile = (
  state = initAvailableInsuredProfile,
  { type, newHasErrorList }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR:
      return Object.assign({}, state, { hasErrorList: newHasErrorList });
    case ACTION_TYPES[CLIENT].CLEAN_AVAILABLE_INSURED_PROFILE:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initAvailableInsuredProfile;
    default:
      return state;
  }
};

export default combineReducers({
  profile,
  dependantProfiles,
  contactList,
  availableInsuredProfile
});
