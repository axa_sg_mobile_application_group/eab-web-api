import faker from "faker";
import * as _ from "lodash";
import reducer from "./recommendation";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { RECOMMENDATION } from "../constants/REDUCER_TYPES";


const randomRecommendationDataNormal = () => {
  const normalProductChoice1 = faker.random.arrayElement(["Y", "N"]);

  return {
    extra: {
      proposerAndLifeAssuredName: faker.name.firstName() + " / " + faker.name.lastName(),
      basicPlanName: faker.random.arrayElement(["AXA Band Aid (RP)", "SavvySaver (RP)"]),
      ridersName: [faker.random.arrayElement(["Rider 1", "Rider 2"])],
      recommendBenefitDefault: faker.random.words(5),
      recommendLimitDefault: faker.random.words(5),
      paymentMode: faker.random.arrayElement(["A", "S", "Q", "M", "L"]),
      isShield: false,
      lastUpdateDate: faker.date.past(0)
    },
    benefit: faker.random.words(5),
    limitation: faker.random.words(5),
    reason: faker.random.words(5),
    rop: {
      choiceQ1: normalProductChoice1,
      choiceQ1Sub1: normalProductChoice1 === "Y" ? faker.random.arrayElement(["Y", "N"]) : "",
      choiceQ1Sub2: normalProductChoice1 === "Y" ? faker.random.arrayElement(["Y", "N"]) : "",
      choiceQ1Sub3: faker.random.words(5),
      existCi: faker.random.number(9999999999),
      existLife: faker.random.number(9999999999),
      existPaAdb: faker.random.number(9999999999),
      existTotalPrem: faker.random.number(9999999999),
      existTpd: faker.random.number(9999999999),
      replaceCi: faker.random.number(9999999999),
      replaceLife: faker.random.number(9999999999),
      replacePaAdb: faker.random.number(9999999999),
      replaceTotalPrem: faker.random.number(9999999999),
      replaceTpd: faker.random.number(9999999999)
    },
    rop_shield: {
      ropBlock: {
        iCidRopAnswerMap: {},
        ropQ1sub3: "",
        ropQ2: "",
        ropQ3: "",
        shieldRopAnswer_0: "",
        shieldRopAnswer_1: "",
        shieldRopAnswer_2: "",
        shieldRopAnswer_3: "",
        shieldRopAnswer_4: "",
        shieldRopAnswer_5: ""
      }
    }
  };
}

const randomRecommendationDataShield = () => {
  const shieldProductChoice0 = faker.random.arrayElement(["Y", "N"]);
  const shieldProductChoice1 = faker.random.arrayElement(["Y", "N"]);
  const shieldProductChoice2 = faker.random.arrayElement(["Y", "N"]);
  const shieldProductChoice3 = faker.random.arrayElement(["Y", "N"]);
  const shieldProductChoice4 = faker.random.arrayElement(["Y", "N"]);
  const shieldProductChoice5 = faker.random.arrayElement(["Y", "N"]);
  const shieldHasRop = shieldProductChoice0 === "Y" || shieldProductChoice1 === "Y" || shieldProductChoice2 === "Y" || 
    shieldProductChoice3 === "Y" || shieldProductChoice4 === "Y" || shieldProductChoice5 === "Y";

  return {
    extra: {
      proposerAndLifeAssuredName: "",
      basicPlanName: "AXA Shield Plan (RP)",
      ridersName: [],
      shieldInsuredPlans: [
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        },
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        },
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        },
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        },
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        },
        {
          shieldInsured: faker.name.firstName() + ":",
          shieldPlan:
            "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
        }
      ],
      shieldInsuredAndPlanGroupList: [
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        },
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        },
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        },
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        },
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        },
        {
          insuredNameList: [faker.name.firstName()],
          planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
          planNameList: [
            "AXA Shield (Plan A)",
            "AXA Basic Care (Plan A)",
            "AXA General Care (Plan A)",
            "AXA Home Care"
          ]
        }
      ],
      recommendBenefitDefault: faker.random.words(5),
      recommendLimitDefault: faker.random.words(5),
      paymentMode: faker.random.arrayElement(["A", "S", "Q", "M"]),
      isShield: true,
      lastUpdateDate: faker.date.past(0)
    },
    benefit: faker.random.words(5),
    limitation: faker.random.words(5),
    reason: faker.random.words(5),
    rop: {
      choiceQ1: "",
      choiceQ1Sub1: "",
      choiceQ1Sub2: "",
      choiceQ1Sub3: "",
      existCi: 0,
      existLife: 0,
      existPaAdb: 0,
      existTotalPrem: 0,
      existTpd: 0,
      replaceCi: 0,
      replaceLife: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      replaceTpd: 0
    },
    rop_shield: {
      ropBlock: {
        iCidRopAnswerMap: {
          "CP001001-00001": "shieldRopAnswer_0",
          "CP001001-00002": "shieldRopAnswer_1",
          "CP001001-00003": "shieldRopAnswer_2",
          "CP001001-00004": "shieldRopAnswer_3",
          "CP001001-00005": "shieldRopAnswer_4",
          "CP001001-00006": "shieldRopAnswer_5"
        },
        ropQ1sub3: faker.random.words(5),
        ropQ2: shieldHasRop === "Y" ? faker.random.arrayElement(["Y", "N"]) : "",
        ropQ3: shieldHasRop === "Y" ? faker.random.arrayElement(["Y", "N"]) : "",
        shieldRopAnswer_0: shieldProductChoice0,
        shieldRopAnswer_1: shieldProductChoice1,
        shieldRopAnswer_2: shieldProductChoice2,
        shieldRopAnswer_3: shieldProductChoice3,
        shieldRopAnswer_4: shieldProductChoice4,
        shieldRopAnswer_5: shieldProductChoice5
      }
    }
  };
}

const randomBudgetData = () => {
  const budget = {
    spBudget: faker.random.number(9999999999),
    spTotalPremium: faker.random.number(9999999999),
    spCompare: 0,
    spCompareResult: "", 
    rpBudget: faker.random.number(9999999999),
    rpTotalPremium: faker.random.number(9999999999),
    rpCompare: 0,
    rpCompareResult: "",
    cpfOaBudget: faker.random.number(9999999999),
    cpfOaTotalPremium: faker.random.number(9999999999),
    cpfOaCompare: 0,
    cpfOaCompareResult: "",
    cpfSaBudget: faker.random.number(9999999999),
    cpfSaTotalPremium: faker.random.number(9999999999),
    cpfSaCompare: 0,
    cpfSaCompareResult: "",
    srsBudget: faker.random.number(9999999999),
    srsTotalPremium: faker.random.number(9999999999),
    srsCompare: 0,
    srsCompareResult: "",
    cpfMsBudget: faker.random.number(9999999999),
    cpfMsTotalPremium: faker.random.number(9999999999),
    cpfMsCompare: 0,
    cpfMsCompareResult: "",
    budgetMoreChoice: faker.random.arrayElement(["Y", "N"]),
    budgetMoreReason: faker.random.words(300),
    budgetLessChoice: faker.random.arrayElement(["Y", "N"]),
    budgetLessReason: faker.random.words(300),
  };
  budget.spCompare = budget.spBudget - budget.spTotalPremium;
  budget.rpCompare = budget.rpBudget - budget.rpTotalPremium;
  budget.cpfOaCompare = budget.cpfOaBudget - budget.cpfOaTotalPremium;
  budget.cpfSaCompare = budget.cpfSaBudget - budget.cpfSaTotalPremium;
  budget.srsCompare = budget.srsBudget - budget.srsTotalPremium;
  budget.cpfMsCompare = budget.cpfMsBudget - budget.cpfMsTotalPremium;

  budget.spCompareResult = budget.spCompare > 9.99 ? "Underutilized" : (budget.spCompare < -9.99 ?  "Exceeded" : "Aligned");
  budget.rpCompareResult = budget.rpCompare > 9.99 ? "Underutilized" : (budget.rpCompare < -9.99 ?  "Exceeded" : "Aligned");
  budget.cpfOaCompareResult = budget.cpfOaCompare > 9.99 ? "Underutilized" : (budget.cpfOaCompare < -9.99 ?  "Exceeded" : "Aligned");
  budget.cpfSaCompareResult = budget.cpfSaCompare > 9.99 ? "Underutilized" : (budget.cpfSaCompare < -9.99 ?  "Exceeded" : "Aligned");
  budget.srsCompareResult = budget.srsCompare > 9.99 ? "Underutilized" : (budget.srsCompare < -9.99 ?  "Exceeded" : "Aligned");
  budget.cpfMsCompareResult = budget.cpfMsCompare > 9.99 ? "Underutilized" : (budget.cpfMsCompare < -9.99 ?  "Exceeded" : "Aligned");

  return budget;
}

const randomAcceptanceData = () => {
  return {
    proposerAndLifeAssuredName: faker.name.firstName() + " / " + faker.name.lastName(),
    basicPlanName: faker.random.arrayElement(["AXA Band Aid (RP)", "SavvySaver (RP)"]),
    ridersName: [faker.random.arrayElement(["Rider 1", "Rider 2"])],
  }
}

const randomAcceptanceDataList = (count) => {
  const list = [];
  for (let i = 0; i < count; i ++) {
    list.push(randomAcceptanceData());
  }
  return list;
}

describe("config reducer", () => {
  it("should test the initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual({
        recommendation: {
          chosenList: [],
          notChosenList: []
        },
        budget: {},
        acceptance: [],
        error: {
          recommendation: {},
          budget: {},
          acceptance: {}
        },
        component: {
          completedStep: -1,
          currentStep: 0,
          completedSection: { recommendation: {}, budget: false },
          selectedQuotId: "",
          showDetails: false,
          disabled: false
        }
      });
  });

  it("should test CLEAN_CLIENT_DATA", () => {
    // TODO: use fake data instead of undefined
    expect(reducer(undefined, {
      type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
    }))
      .toEqual({
        recommendation: {
          chosenList: [],
          notChosenList: []
        },
        budget: {},
        acceptance: [],
        error: {
          recommendation: {},
          budget: {},
          acceptance: {}
        },
        component: {
          completedStep: -1,
          currentStep: 0,
          completedSection: { recommendation: {}, budget: false },
          selectedQuotId: "",
          showDetails: false,
          disabled: false
        }
      });
  });


  it("should test CLOSE_RECOMMENDATION", () => {
    expect(reducer(undefined, {
      type: ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION
    }))
    .toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test GET_RECOMMENDATION", () => {
    const chosenList = [faker.random.arrayElement(["QU001002-11111", "QU003004-22222","QU005006-33333","QU007008-44444"])];
    const notChosenList = [faker.random.arrayElement(["QU001002-55555", "QU003004-66666","QU005006-77777","QU007008-88888"])];
    
    const normalProduct1 = randomRecommendationDataNormal();
    const normalProduct2 = randomRecommendationDataNormal();
    const shieldProduct1 = randomRecommendationDataShield();
    const shieldProduct2 = randomRecommendationDataShield();
    const recommendation = {
      chosenList,
      notChosenList
    };
    recommendation[chosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});
    recommendation[notChosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});

    const budget = randomBudgetData();
    const acceptance = randomAcceptanceDataList(chosenList.length);
    const component = {
      completedStep: -1,
      currentStep: 0,
      completedSection: { recommendation: {}, budget: false },
      selectedQuotId: "",
      showDetails: false,
      disabled: false
    }

    expect(reducer(undefined, {
      type: ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION,
      recommendation,
      budget,
      acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component
    }))
    .toEqual({
      recommendation,
      budget,
      acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component
    });
  });

  it("should test UPDATE_RECOMMENDATION", () => {
    const chosenList = [faker.random.arrayElement(["QU001002-11111", "QU003004-22222","QU005006-33333","QU007008-44444"])];
    const notChosenList = [faker.random.arrayElement(["QU001002-55555", "QU003004-66666","QU005006-77777","QU007008-88888"])];
    
    const normalProduct1 = randomRecommendationDataNormal();
    const normalProduct2 = randomRecommendationDataNormal();
    const shieldProduct1 = randomRecommendationDataShield();
    const shieldProduct2 = randomRecommendationDataShield();
    const recommendation = {
      chosenList,
      notChosenList
    };
    recommendation[chosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});
    recommendation[notChosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});

    const budget = randomBudgetData();
    const acceptance = randomAcceptanceDataList(chosenList.length);

    expect(reducer({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION,
      recommendation,
      budget,
      acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }))
    .toEqual({
      recommendation,
      budget,
      acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_DATA", () => {
    const chosenList = [faker.random.arrayElement(["QU001002-11111", "QU003004-22222","QU005006-33333","QU007008-44444"])];
    const notChosenList = [faker.random.arrayElement(["QU001002-55555", "QU003004-66666","QU005006-77777","QU007008-88888"])];
    
    const normalProduct1 = randomRecommendationDataNormal();
    const normalProduct2 = randomRecommendationDataNormal();
    const shieldProduct1 = randomRecommendationDataShield();
    const shieldProduct2 = randomRecommendationDataShield();
    const recommendation = {
      chosenList,
      notChosenList
    };
    recommendation[chosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});
    recommendation[notChosenList[0]] = faker.random.objectElement({normalProduct1, normalProduct2, shieldProduct1, shieldProduct2});

    expect(reducer({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DATA,
      recommendation
    }))
    .toEqual({
      recommendation,
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_SELECTED_QUOT", () => {
    const newSelectedQuotId = faker.random.arrayElement(["QU003004-22222","QU005006-33333","QU007008-44444"]);

    expect(reducer(undefined, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT,
      newSelectedQuotId
    }))
    .toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: newSelectedQuotId,
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_COMPONENT", () => {
    const currentStep = faker.random.number(2);
    const completedStep = currentStep - faker.random.number(1);
    const component = {
      completedStep,
      currentStep,
      completedSection: { 
        recommendation: {
          "QU003004-22222": faker.random.boolean(),
          "QU005006-33333": faker.random.boolean(),
          "QU007008-44444": faker.random.boolean()
        }, 
        budget: faker.random.boolean() 
      },
      selectedQuotId: faker.random.arrayElement(["QU003004-22222","QU005006-33333","QU007008-44444"]),
      showDetails: faker.random.boolean(),
      disabled: faker.random.boolean()
    };

    expect(reducer({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component
    }))
    .toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component
    });
  });

  it("should test UPDATE_RECOMMENDATION_DETAILS", () => {
    const currentStep = faker.random.number(2);
    const completedStep = currentStep - faker.random.number(1);
    const component = {
      completedStep,
      currentStep,
      completedSection: { 
        recommendation: {
          "QU003004-22222": faker.random.boolean(),
          "QU005006-33333": faker.random.boolean(),
          "QU007008-44444": faker.random.boolean()
        }, 
        budget: faker.random.boolean() 
      },
      selectedQuotId: faker.random.arrayElement(["QU003004-22222","QU005006-33333","QU007008-44444"]),
      showDetails: faker.random.boolean(),
      disabled: faker.random.boolean()
    };

    const expectedComponent = _.cloneDeep(component);
    expectedComponent.showDetails = !component.showDetails;
    
    expect(reducer({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component
    }, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS,
      newShowDetails: !component.showDetails
    }))
    .toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: expectedComponent
    });
  });

  // TODO after complet budget component
  it("should test UPDATE_BUDGET_DATA", () => {
    expect(reducer(undefined, {
      type: ACTION_TYPES[RECOMMENDATION].UPDATE_BUDGET_DATA
    }))
    .toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });
})
