import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { QUOTATION } from "../constants/REDUCER_TYPES";
import reducer from "./quotation";

const initialState = {
  isQuickQuote: false,
  quickQuotes: [],
  quotation: {},
  planDetails: {},
  inputConfigs: {},
  quotationErrors: {},
  quotWarnings: [],
  availableInsureds: [],
  availableFunds: []
};

describe("quotation reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it("should handle CLEAN_QUOTATION", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.isQuickQuote = true;

    expect(reducer(
      {
        isQuickQuote: true,
        quickQuotes: [
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-09T06:00:03.369Z",
            iFullName: "Alex",
            id: "QU001020-00026",
            lastUpdateDate: "2018-03-09T06:01:15.779Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-08T10:43:37.948Z",
            iFullName: "Alex",
            id: "QU001020-00025",
            lastUpdateDate: "2018-03-08T10:44:56.970Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: { en: "SavvySaver", "zh-Hant": "SavvySaver" },
            createDate: "2018-03-06T10:33:46.494Z",
            iFullName: "Alex",
            id: "QU001020-00024",
            lastUpdateDate: "2018-03-06T10:34:02.082Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          }
        ],
        quotation: {
          type: "quotation",
          baseProductCode: "SAV",
          baseProductId: "08_product_SAV_1",
          baseProductName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          productLine: "WLE",
          budgetRules: ["RP"],
          compCode: "01",
          agentCode: "011011",
          dealerGroup: "AGENCY",
          agent: {
            agentCode: "011011",
            name: "Hockin Tsui",
            dealerGroup: "AGENCY",
            company: "AXA Insurance Pte Ltd",
            tel: "0",
            mobile: "00000000",
            email: "hokyin.tsui@eabsystems.com"
          },
          pCid: "CP001001-00001",
          pFullName: "Arya Stark",
          pFirstName: "Arya",
          pLastName: "Stark",
          pGender: "F",
          pDob: "1996-12-07",
          pAge: 21,
          pResidence: "R2",
          pSmoke: "N",
          pEmail: "aryastark@test.com",
          pOccupation: "O1527",
          pOccupationClass: "4",
          sameAs: "Y",
          ccy: "SGD",
          policyOptions: {},
          iCid: "CP001001-00001",
          iFullName: "Arya Stark",
          iFirstName: "Arya",
          iLastName: "Stark",
          iGender: "F",
          iDob: "1996-12-07",
          iAge: 21,
          iResidence: "R2",
          iSmoke: "N",
          iEmail: "aryastark@test.com",
          iOccupation: "O1527",
          iOccupationClass: "4",
          quickQuote: true,
          fund: null,
          extraFlags: {
            nationalityResidence: {
              iCategory: "A1",
              iRejected: "N",
              pCategory: "A1",
              pRejected: "N"
            }
          },
          isBackDate: "N",
          riskCommenDate: "2018-05-07",
          createDate: "2018-05-07T01:52:52.023Z",
          lastUpdateDate: "2018-05-07T01:52:52.023Z",
          plans: [
            {
              covCode: "SAV",
              covName: {
                en: "SavvySaver",
                "zh-Hant": "SavvySaver"
              },
              productLine: "WLE",
              version: 1,
              saViewInd: "Y"
            },
            {
              covCode: "PET",
              covName: {
                en: "PremiumEraser Total",
                "zh-Hant": "PremiumEraser Total"
              },
              version: 1,
              productLine: "WLE",
              saViewInd: "N",
              sumInsured: null,
              premRate: 0,
              yearPrem: null,
              halfYearPrem: null,
              quarterPrem: null,
              monthPrem: null
            }
          ],
          policyOptionsDesc: {},
          premium: 0,
          totYearPrem: null,
          totHalfyearPrem: null,
          totQuarterPrem: null,
          totMonthPrem: null
        },
        planDetails: {
          SAV: {
            allowBackdate: "Y",
            benlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "M"
                  }
                ]
              }
            ],
            brochures: 6849732,
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description",
              "zh-Hant": "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            coexist: [
              {
                ageFr: 0,
                ageTo: 99,
                country: "*",
                dealerGroup: "*",
                groupA: ["PPERA"],
                groupB: ["PPEPS"],
                shouldCoExist: "N"
              }
            ],
            compCode: "08",
            corpInd: "N",
            covCode: "SAV",
            covName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            },
            create: "Super Admin  on 11 Dec 2017 12:30:31",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2014-01-01 00:00:00",
            entryAge: [
              {
                country: "*",
                imaxAge: 60,
                imaxAgeUnit: "nearestAge",
                iminAge: 1,
                iminAgeUnit: "month",
                omaxAge: 60,
                omaxAgeUnit: "nearestAge",
                ominAge: 18,
                ominAgeUnit: "year",
                sameAsOwner: "N"
              }
            ],
            expDate: "2018-08-08 00:00:00",
            genderInd: "*",
            illustrationColumns: [
              {
                id: "totalPremPaid",
                isRelatedToProjectionRate: "N",
                title: {
                  en: "Premium Paid",
                  "zh-Hant": "Premium Paid"
                }
              },
              {
                id: "totalDB",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Death Benefit",
                  "zh-Hant": "Death Benefit"
                }
              },
              {
                id: "totalSV",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Surrender Value",
                  "zh-Hant": "Surrender Value"
                }
              }
            ],
            insuredAgeDesc: {
              en: "Age"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            licence: [""],
            nfInd: "N",
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Payment Mode"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "eNets",
              "teleTransfter",
              "crCard",
              "cash",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder"
            ],
            planCodeMapping: {
              planCode: ["15SS", "18SS", "21SS", "24SS"],
              policyTerm: ["15", "18", "21", "24"]
            },
            planInd: "B",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "Policy Team Desc",
              "zh-Hant": ""
            },
            polTermInd: "Y",
            policyOptions: [],
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "Y",
            premMatSameAsPolMat: "Y",
            premStructurs: [],
            premTermDesc: {
              en: "Prem Term Desc"
            },
            premTermInd: "N",
            premTermRule: "BT",
            premType: "R",
            premlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: "",
                    min: 1143,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 582.93,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 297.18,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 100.01,
                    payMode: "M"
                  }
                ]
              }
            ],
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 2251160,
            productCategory: "cv",
            productLine: "WLE",
            projection: [
              {
                rates: {
                  CRB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0325,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 100,
                title: {
                  en: "3.25"
                }
              },
              {
                rates: {
                  CRB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0475,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 200,
                title: {
                  en: "4.75"
                }
              }
            ],
            ref_no: "SavvySaver",
            releaseDate: "2018-03-07 14:52:01",
            remarks: {
              en: ""
            },
            reportTemplate: [
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_INTRO",
                pdfType: "COVER_PAGE",
                seq: 200
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_MAIN_BI",
                pdfType: "BNFT_ILL",
                seq: 300
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_SUPP_BI",
                pdfType: "BNFT_ILL",
                seq: 400
              },
              {
                covCode: "PPERA",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PPEPS",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_ACK"
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_PROD_SUMMARY"
              }
            ],
            riderList: [
              {
                condition: [
                  {
                    autoAttach: "N",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPERA"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPEPS"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PET"
              }
            ],
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 1
              }
            ],
            saInputInd: "Y",
            saScale: "",
            saSecDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            saViewInd: "Y",
            smokeInd: "*",
            status: "A",
            supportIllustration: "Y",
            task_desc: "SavvySaver",
            thumbnail3: 44107,
            thumbnail4: 0,
            type: "product",
            upd: "JOHN.TAM  on 07 Mar 2018 11:54:22",
            version: 1,
            wholeLifeInd: "N"
          },
          PPERA: {
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPERA",
            covName: {
              en: "Smart Payer PremiumEraser",
              "zh-Hant": "Smart Payer PremiumEraser"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "agency",
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-23 00:00:00",
            entryAge: [],
            expDate: "2018-05-24 10:58:56",
            genderInd: "*",
            insuredAgeDesc: {
              en: "11"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features",
              "zh-Hant": "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: ["p1"],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PE", "18PE", "21PE", "24PE", "PE65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "pol maturity"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "PM"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "111"
            },
            prod_summary: 525508,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPERA",
            releaseDate: "2018-03-07 14:52:05",
            remarks: {
              en: "Remarks",
              "zh-Hant": "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPERA",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PPEPS: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPEPS",
            covName: {
              en: "Smart Payer PremiumEraser Plus",
              "zh-Hant": "Smart Payer PremiumEraser Plus"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-17 12:09:03",
            entryAge: [],
            expDate: "2018-05-17 12:13:25",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PEC", "18PEC", "21PEC", "24PEC", "PEC65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "PM"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "sadas"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features English"
            },
            prod_summary: 852600,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPEPS",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: "",
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPEP",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PET: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PET",
            covName: {
              en: "PremiumEraser Total",
              "zh-Hant": "PremiumEraser Total"
            },
            create: "johntest22  on 17 May 2017 16:07:49",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "SYNERGY",
              "FUSION",
              "SINGPOST",
              "DIRECT"
            ],
            effDate: "2016-05-17 15:58:52",
            entryAge: [],
            expDate: "2018-05-17 15:59:39",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "axasam",
              "dbsCrCardIpp",
              "eNets",
              "chequeCashierOrder",
              "teleTransfter",
              "cash"
            ],
            planCodeMapping: {
              planCode: [
                "5WOP",
                "6WOP",
                "7WOP",
                "8WOP",
                "9WOP",
                "10WOP",
                "11WOP",
                "12WOP",
                "13WOP",
                "14WOP",
                "15WOP",
                "16WOP",
                "17WOP",
                "18WOP",
                "19WOP",
                "20WOP",
                "21WOP",
                "22WOP",
                "23WOP",
                "24WOP"
              ],
              policyTerm: [
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24"
              ]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polTermDesc: {
              en: "Policy Team Description"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "Y",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "Y",
            premTermDesc: {
              en: "Premium Term Description"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 1211664,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PET",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "Description",
            type: "product",
            upd: "SYSTEM  on 20 Jul 2017 17:29:39",
            version: 1,
            wholeLifeInd: "N"
          }
        },
        inputConfigs: {
          SAV: {
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: true,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: true,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 1
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premlim: [],
            benlim: [],
            riderList: [
              {
                covCode: "PET",
                autoAttach: "Y",
                compulsory: "N",
                saRate: "",
                saRule: "BP"
              }
            ]
          },
          PET: {
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: false,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: false,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            }
          }
        },
        quotationErrors: {
          mandatoryErrors: {},
          otherErrors: [],
          planErrors: {
            SAV: [
              {
                covCode: "SAV",
                msgPara: ["$1,143.00", 0],
                code: "js.err.invalid_premlim_min"
              }
            ]
          },
          policyOptionErrors: {}
        },
        quotWarnings: [
          "Application will be subject to underwriting review based on Life Assured's residency and/or nationality"
        ],
        availableInsureds: [
          {
            profile: {
              addrBlock: "123",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "321",
              age: 21,
              agentId: "011011",
              allowance: 1233455,
              applicationCount: 1,
              bundle: [
                {
                  id: "FN001001-00001",
                  isValid: false
                },
                {
                  id: "FN001001-00007",
                  isValid: false
                },
                {
                  id: "FN001001-00008",
                  isValid: false
                },
                {
                  id: "FN001001-00009",
                  isValid: false
                },
                {
                  id: "FN001001-00010",
                  isValid: false
                },
                {
                  id: "FN001001-00011",
                  isValid: false
                },
                {
                  id: "FN001001-00012",
                  isValid: true
                }
              ],
              cid: "CP001001-00001",
              dependants: [
                {
                  cid: "CP001001-00002",
                  relationship: "FAT",
                  relationshipOther: null
                }
              ],
              dob: "1996-12-07",
              education: "above",
              email: "aryastark@test.com",
              employStatus: "ft",
              firstName: "Arya",
              fnaRecordIdArray: "",
              fullName: "Arya Stark",
              gender: "F",
              hanyuPinyinName: "",
              haveSignDoc: true,
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I2",
              initial: "SA",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2017-12-18T07:29:53.831Z",
              marital: "S",
              mobileCountryCode: "+65",
              mobileNo: "223344",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 21,
              occupation: "O1527",
              organization: "123123",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1505181974183,
              postalCode: "123",
              referrals: "",
              residenceCountry: "R2",
              title: "Ms",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          },
          {
            profile: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentId: "011011",
              allowance: 123332,
              bundle: [
                {
                  id: "FN001001-00002",
                  isValid: true
                }
              ],
              cid: "CP001001-00002",
              dependants: [
                {
                  cid: "CP001001-00001",
                  relationship: "DAU"
                }
              ],
              dob: "1987-12-13",
              education: "above",
              email: "",
              employStatus: "ft",
              firstName: "Eddard",
              fnaRecordIdArray: "",
              fullName: "Eddard Stark",
              gender: "M",
              hanyuPinyinName: "",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I3",
              initial: "SE",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2018-01-03T09:17:43.954Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "12365489",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 30,
              occupation: "O99",
              organization: "",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1500460588805,
              postalCode: "",
              referrals: "",
              residenceCountry: "R2",
              title: "Mr",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          }
        ],
        availableFunds: {
          type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS,
          newAvailableFunds: [
            {
              "assetClass": "mixed_assets_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ABFD",
              "fundName": {
                "en": "AXA Asian Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ZAGF",
              "fundName": {
                "en": "AXA Asian Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "mixed_assets_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "AINC",
              "fundName": {
                "en": "AXA Asian Income Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_greater_china",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "CHGF",
              "fundName": {
                "en": "AXA China Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "mixed_assets_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "DSAF",
              "fundName": {
                "en": "AXA Dynamic Strategies Aggressive Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ZFFA",
              "fundName": {
                "en": "AXA Fortress Fund A (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "PLAN",
              "fundName": {
                "en": "AXA Global Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 3,
              "version": 1
            }, {
              "assetClass": "bond_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GRDN",
              "fundName": {
                "en": "AXA Global Defensive Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "equity_emerging_markets",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GEME",
              "fundName": {
                "en": "AXA Global Emerging Markets Equity (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GEQB",
              "fundName": {
                "en": "AXA Global Equity Blend (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SEEK",
              "fundName": {
                "en": "AXA Global Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ENTP",
              "fundName": {
                "en": "AXA Global High Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "HVST",
              "fundName": {
                "en": "AXA Global Secure Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 2,
              "version": 1
            }, {
              "assetClass": "equity_healthcare",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "HEAL",
              "fundName": {
                "en": "AXA Health Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_india",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "INDF",
              "fundName": {
                "en": "AXA India Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "PAEQ",
              "fundName": {
                "en": "AXA Pacific Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_global_shariah",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SGEF",
              "fundName": {
                "en": "AXA Shariah Global Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "bond_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ASGD",
              "fundName": {
                "en": "AXA Short Duration Bond Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "mixed_assets_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SBFD",
              "fundName": {
                "en": "AXA Singapore Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "bond_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SBON",
              "fundName": {
                "en": "AXA Singapore Bond Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "equity_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SGEQ",
              "fundName": {
                "en": "AXA Singapore Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_asean",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SEAS",
              "fundName": {
                "en": "AXA South East Asia Special Situations (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }
          ]
        }
      },
      {
        type: ACTION_TYPES[QUOTATION].CLEAN_QUOTATION
      }
    )).toEqual(expectData);
  });

  it("should handle CLEAN_CLIENT_DATA", () => {
    expect(reducer(
      {
        isQuickQuote: true,
        quickQuotes: [
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-09T06:00:03.369Z",
            iFullName: "Alex",
            id: "QU001020-00026",
            lastUpdateDate: "2018-03-09T06:01:15.779Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-08T10:43:37.948Z",
            iFullName: "Alex",
            id: "QU001020-00025",
            lastUpdateDate: "2018-03-08T10:44:56.970Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: { en: "SavvySaver", "zh-Hant": "SavvySaver" },
            createDate: "2018-03-06T10:33:46.494Z",
            iFullName: "Alex",
            id: "QU001020-00024",
            lastUpdateDate: "2018-03-06T10:34:02.082Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          }
        ],
        quotation: {
          type: "quotation",
          baseProductCode: "SAV",
          baseProductId: "08_product_SAV_1",
          baseProductName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          productLine: "WLE",
          budgetRules: ["RP"],
          compCode: "01",
          agentCode: "011011",
          dealerGroup: "AGENCY",
          agent: {
            agentCode: "011011",
            name: "Hockin Tsui",
            dealerGroup: "AGENCY",
            company: "AXA Insurance Pte Ltd",
            tel: "0",
            mobile: "00000000",
            email: "hokyin.tsui@eabsystems.com"
          },
          pCid: "CP001001-00001",
          pFullName: "Arya Stark",
          pFirstName: "Arya",
          pLastName: "Stark",
          pGender: "F",
          pDob: "1996-12-07",
          pAge: 21,
          pResidence: "R2",
          pSmoke: "N",
          pEmail: "aryastark@test.com",
          pOccupation: "O1527",
          pOccupationClass: "4",
          sameAs: "Y",
          ccy: "SGD",
          policyOptions: {},
          iCid: "CP001001-00001",
          iFullName: "Arya Stark",
          iFirstName: "Arya",
          iLastName: "Stark",
          iGender: "F",
          iDob: "1996-12-07",
          iAge: 21,
          iResidence: "R2",
          iSmoke: "N",
          iEmail: "aryastark@test.com",
          iOccupation: "O1527",
          iOccupationClass: "4",
          quickQuote: true,
          fund: null,
          extraFlags: {
            nationalityResidence: {
              iCategory: "A1",
              iRejected: "N",
              pCategory: "A1",
              pRejected: "N"
            }
          },
          isBackDate: "N",
          riskCommenDate: "2018-05-07",
          createDate: "2018-05-07T01:52:52.023Z",
          lastUpdateDate: "2018-05-07T01:52:52.023Z",
          plans: [
            {
              covCode: "SAV",
              covName: {
                en: "SavvySaver",
                "zh-Hant": "SavvySaver"
              },
              productLine: "WLE",
              version: 1,
              saViewInd: "Y"
            },
            {
              covCode: "PET",
              covName: {
                en: "PremiumEraser Total",
                "zh-Hant": "PremiumEraser Total"
              },
              version: 1,
              productLine: "WLE",
              saViewInd: "N",
              sumInsured: null,
              premRate: 0,
              yearPrem: null,
              halfYearPrem: null,
              quarterPrem: null,
              monthPrem: null
            }
          ],
          policyOptionsDesc: {},
          premium: 0,
          totYearPrem: null,
          totHalfyearPrem: null,
          totQuarterPrem: null,
          totMonthPrem: null
        },
        planDetails: {
          SAV: {
            allowBackdate: "Y",
            benlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "M"
                  }
                ]
              }
            ],
            brochures: 6849732,
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description",
              "zh-Hant": "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            coexist: [
              {
                ageFr: 0,
                ageTo: 99,
                country: "*",
                dealerGroup: "*",
                groupA: ["PPERA"],
                groupB: ["PPEPS"],
                shouldCoExist: "N"
              }
            ],
            compCode: "08",
            corpInd: "N",
            covCode: "SAV",
            covName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            },
            create: "Super Admin  on 11 Dec 2017 12:30:31",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2014-01-01 00:00:00",
            entryAge: [
              {
                country: "*",
                imaxAge: 60,
                imaxAgeUnit: "nearestAge",
                iminAge: 1,
                iminAgeUnit: "month",
                omaxAge: 60,
                omaxAgeUnit: "nearestAge",
                ominAge: 18,
                ominAgeUnit: "year",
                sameAsOwner: "N"
              }
            ],
            expDate: "2018-08-08 00:00:00",
            genderInd: "*",
            illustrationColumns: [
              {
                id: "totalPremPaid",
                isRelatedToProjectionRate: "N",
                title: {
                  en: "Premium Paid",
                  "zh-Hant": "Premium Paid"
                }
              },
              {
                id: "totalDB",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Death Benefit",
                  "zh-Hant": "Death Benefit"
                }
              },
              {
                id: "totalSV",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Surrender Value",
                  "zh-Hant": "Surrender Value"
                }
              }
            ],
            insuredAgeDesc: {
              en: "Age"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            licence: [""],
            nfInd: "N",
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Payment Mode"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "eNets",
              "teleTransfter",
              "crCard",
              "cash",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder"
            ],
            planCodeMapping: {
              planCode: ["15SS", "18SS", "21SS", "24SS"],
              policyTerm: ["15", "18", "21", "24"]
            },
            planInd: "B",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "Policy Team Desc",
              "zh-Hant": ""
            },
            polTermInd: "Y",
            policyOptions: [],
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "Y",
            premMatSameAsPolMat: "Y",
            premStructurs: [],
            premTermDesc: {
              en: "Prem Term Desc"
            },
            premTermInd: "N",
            premTermRule: "BT",
            premType: "R",
            premlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: "",
                    min: 1143,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 582.93,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 297.18,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 100.01,
                    payMode: "M"
                  }
                ]
              }
            ],
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 2251160,
            productCategory: "cv",
            productLine: "WLE",
            projection: [
              {
                rates: {
                  CRB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0325,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 100,
                title: {
                  en: "3.25"
                }
              },
              {
                rates: {
                  CRB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0475,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 200,
                title: {
                  en: "4.75"
                }
              }
            ],
            ref_no: "SavvySaver",
            releaseDate: "2018-03-07 14:52:01",
            remarks: {
              en: ""
            },
            reportTemplate: [
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_INTRO",
                pdfType: "COVER_PAGE",
                seq: 200
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_MAIN_BI",
                pdfType: "BNFT_ILL",
                seq: 300
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_SUPP_BI",
                pdfType: "BNFT_ILL",
                seq: 400
              },
              {
                covCode: "PPERA",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PPEPS",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_ACK"
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_PROD_SUMMARY"
              }
            ],
            riderList: [
              {
                condition: [
                  {
                    autoAttach: "N",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPERA"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPEPS"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PET"
              }
            ],
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 1
              }
            ],
            saInputInd: "Y",
            saScale: "",
            saSecDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            saViewInd: "Y",
            smokeInd: "*",
            status: "A",
            supportIllustration: "Y",
            task_desc: "SavvySaver",
            thumbnail3: 44107,
            thumbnail4: 0,
            type: "product",
            upd: "JOHN.TAM  on 07 Mar 2018 11:54:22",
            version: 1,
            wholeLifeInd: "N"
          },
          PPERA: {
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPERA",
            covName: {
              en: "Smart Payer PremiumEraser",
              "zh-Hant": "Smart Payer PremiumEraser"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "agency",
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-23 00:00:00",
            entryAge: [],
            expDate: "2018-05-24 10:58:56",
            genderInd: "*",
            insuredAgeDesc: {
              en: "11"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features",
              "zh-Hant": "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: ["p1"],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PE", "18PE", "21PE", "24PE", "PE65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "pol maturity"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "PM"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "111"
            },
            prod_summary: 525508,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPERA",
            releaseDate: "2018-03-07 14:52:05",
            remarks: {
              en: "Remarks",
              "zh-Hant": "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPERA",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PPEPS: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPEPS",
            covName: {
              en: "Smart Payer PremiumEraser Plus",
              "zh-Hant": "Smart Payer PremiumEraser Plus"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-17 12:09:03",
            entryAge: [],
            expDate: "2018-05-17 12:13:25",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PEC", "18PEC", "21PEC", "24PEC", "PEC65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "PM"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "sadas"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features English"
            },
            prod_summary: 852600,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPEPS",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: "",
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPEP",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PET: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PET",
            covName: {
              en: "PremiumEraser Total",
              "zh-Hant": "PremiumEraser Total"
            },
            create: "johntest22  on 17 May 2017 16:07:49",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "SYNERGY",
              "FUSION",
              "SINGPOST",
              "DIRECT"
            ],
            effDate: "2016-05-17 15:58:52",
            entryAge: [],
            expDate: "2018-05-17 15:59:39",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "axasam",
              "dbsCrCardIpp",
              "eNets",
              "chequeCashierOrder",
              "teleTransfter",
              "cash"
            ],
            planCodeMapping: {
              planCode: [
                "5WOP",
                "6WOP",
                "7WOP",
                "8WOP",
                "9WOP",
                "10WOP",
                "11WOP",
                "12WOP",
                "13WOP",
                "14WOP",
                "15WOP",
                "16WOP",
                "17WOP",
                "18WOP",
                "19WOP",
                "20WOP",
                "21WOP",
                "22WOP",
                "23WOP",
                "24WOP"
              ],
              policyTerm: [
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24"
              ]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polTermDesc: {
              en: "Policy Team Description"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "Y",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "Y",
            premTermDesc: {
              en: "Premium Term Description"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 1211664,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PET",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "Description",
            type: "product",
            upd: "SYSTEM  on 20 Jul 2017 17:29:39",
            version: 1,
            wholeLifeInd: "N"
          }
        },
        inputConfigs: {
          SAV: {
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: true,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: true,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 1
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premlim: [],
            benlim: [],
            riderList: [
              {
                covCode: "PET",
                autoAttach: "Y",
                compulsory: "N",
                saRate: "",
                saRule: "BP"
              }
            ]
          },
          PET: {
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: false,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: false,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            }
          }
        },
        quotationErrors: {
          mandatoryErrors: {},
          otherErrors: [],
          planErrors: {
            SAV: [
              {
                covCode: "SAV",
                msgPara: ["$1,143.00", 0],
                code: "js.err.invalid_premlim_min"
              }
            ]
          },
          policyOptionErrors: {}
        },
        quotWarnings: [
          "Application will be subject to underwriting review based on Life Assured's residency and/or nationality"
        ],
        availableInsureds: [
          {
            profile: {
              addrBlock: "123",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "321",
              age: 21,
              agentId: "011011",
              allowance: 1233455,
              applicationCount: 1,
              bundle: [
                {
                  id: "FN001001-00001",
                  isValid: false
                },
                {
                  id: "FN001001-00007",
                  isValid: false
                },
                {
                  id: "FN001001-00008",
                  isValid: false
                },
                {
                  id: "FN001001-00009",
                  isValid: false
                },
                {
                  id: "FN001001-00010",
                  isValid: false
                },
                {
                  id: "FN001001-00011",
                  isValid: false
                },
                {
                  id: "FN001001-00012",
                  isValid: true
                }
              ],
              cid: "CP001001-00001",
              dependants: [
                {
                  cid: "CP001001-00002",
                  relationship: "FAT",
                  relationshipOther: null
                }
              ],
              dob: "1996-12-07",
              education: "above",
              email: "aryastark@test.com",
              employStatus: "ft",
              firstName: "Arya",
              fnaRecordIdArray: "",
              fullName: "Arya Stark",
              gender: "F",
              hanyuPinyinName: "",
              haveSignDoc: true,
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I2",
              initial: "SA",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2017-12-18T07:29:53.831Z",
              marital: "S",
              mobileCountryCode: "+65",
              mobileNo: "223344",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 21,
              occupation: "O1527",
              organization: "123123",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1505181974183,
              postalCode: "123",
              referrals: "",
              residenceCountry: "R2",
              title: "Ms",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          },
          {
            profile: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentId: "011011",
              allowance: 123332,
              bundle: [
                {
                  id: "FN001001-00002",
                  isValid: true
                }
              ],
              cid: "CP001001-00002",
              dependants: [
                {
                  cid: "CP001001-00001",
                  relationship: "DAU"
                }
              ],
              dob: "1987-12-13",
              education: "above",
              email: "",
              employStatus: "ft",
              firstName: "Eddard",
              fnaRecordIdArray: "",
              fullName: "Eddard Stark",
              gender: "M",
              hanyuPinyinName: "",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I3",
              initial: "SE",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2018-01-03T09:17:43.954Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "12365489",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 30,
              occupation: "O99",
              organization: "",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1500460588805,
              postalCode: "",
              referrals: "",
              residenceCountry: "R2",
              title: "Mr",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          }
        ],
        availableFunds: {
          type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS,
          newAvailableFunds: [
            {
              "assetClass": "mixed_assets_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ABFD",
              "fundName": {
                "en": "AXA Asian Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ZAGF",
              "fundName": {
                "en": "AXA Asian Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "mixed_assets_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "AINC",
              "fundName": {
                "en": "AXA Asian Income Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_greater_china",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "CHGF",
              "fundName": {
                "en": "AXA China Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "mixed_assets_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "DSAF",
              "fundName": {
                "en": "AXA Dynamic Strategies Aggressive Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ZFFA",
              "fundName": {
                "en": "AXA Fortress Fund A (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "PLAN",
              "fundName": {
                "en": "AXA Global Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 3,
              "version": 1
            }, {
              "assetClass": "bond_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GRDN",
              "fundName": {
                "en": "AXA Global Defensive Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "equity_emerging_markets",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GEME",
              "fundName": {
                "en": "AXA Global Emerging Markets Equity (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "GEQB",
              "fundName": {
                "en": "AXA Global Equity Blend (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SEEK",
              "fundName": {
                "en": "AXA Global Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ENTP",
              "fundName": {
                "en": "AXA Global High Growth Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "asset_allocation_global",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "HVST",
              "fundName": {
                "en": "AXA Global Secure Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 2,
              "version": 1
            }, {
              "assetClass": "equity_healthcare",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "HEAL",
              "fundName": {
                "en": "AXA Health Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "equity_india",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "INDF",
              "fundName": {
                "en": "AXA India Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_asia_pacific_ex_japan",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "PAEQ",
              "fundName": {
                "en": "AXA Pacific Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_global_shariah",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SGEF",
              "fundName": {
                "en": "AXA Shariah Global Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "bond_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "ASGD",
              "fundName": {
                "en": "AXA Short Duration Bond Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "mixed_assets_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SBFD",
              "fundName": {
                "en": "AXA Singapore Balanced Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "Y",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 4,
              "version": 1
            }, {
              "assetClass": "bond_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SBON",
              "fundName": {
                "en": "AXA Singapore Bond Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
              "riskRating": 1,
              "version": 1
            }, {
              "assetClass": "equity_singapore",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SGEQ",
              "fundName": {
                "en": "AXA Singapore Equity Fund (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }, {
              "assetClass": "equity_asean",
              "ccy": "SGD",
              "compCode": "08",
              "fundCode": "SEAS",
              "fundName": {
                "en": "AXA South East Asia Special Situations (SGD)",
                "zh-Hant": ""
              },
              "isMixedAsset": "N",
              "paymentMethod": ["cash", "srs", "cpfisoa"],
              "riskRating": 5,
              "version": 1
            }
          ]
        }
      },
      {
        type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
      }
    )).toEqual(initialState);
  });

  it("should handle UPDATE_IS_QUICK_QUOTE", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.isQuickQuote = true;

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_IS_QUICK_QUOTE,
        isQuickQuoteData: true
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_QUICK_QUOTES", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.quickQuotes = [
      {
        covName: {
          en: "AXA Wealth Treasure",
          "zh-Hant": "AXA Wealth Treasure"
        },
        createDate: "2018-03-09T06:00:03.369Z",
        iFullName: "Alex",
        id: "QU001020-00026",
        lastUpdateDate: "2018-03-09T06:01:15.779Z",
        pCid: "CP001020-00003",
        pFullName: "Alex",
        type: "quotation"
      },
      {
        covName: {
          en: "AXA Wealth Treasure",
          "zh-Hant": "AXA Wealth Treasure"
        },
        createDate: "2018-03-08T10:43:37.948Z",
        iFullName: "Alex",
        id: "QU001020-00025",
        lastUpdateDate: "2018-03-08T10:44:56.970Z",
        pCid: "CP001020-00003",
        pFullName: "Alex",
        type: "quotation"
      },
      {
        covName: { en: "SavvySaver", "zh-Hant": "SavvySaver" },
        createDate: "2018-03-06T10:33:46.494Z",
        iFullName: "Alex",
        id: "QU001020-00024",
        lastUpdateDate: "2018-03-06T10:34:02.082Z",
        pCid: "CP001020-00003",
        pFullName: "Alex",
        type: "quotation"
      }
    ];

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_QUICK_QUOTES,
        quickQuotesData: [
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-09T06:00:03.369Z",
            iFullName: "Alex",
            id: "QU001020-00026",
            lastUpdateDate: "2018-03-09T06:01:15.779Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: {
              en: "AXA Wealth Treasure",
              "zh-Hant": "AXA Wealth Treasure"
            },
            createDate: "2018-03-08T10:43:37.948Z",
            iFullName: "Alex",
            id: "QU001020-00025",
            lastUpdateDate: "2018-03-08T10:44:56.970Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          },
          {
            covName: { en: "SavvySaver", "zh-Hant": "SavvySaver" },
            createDate: "2018-03-06T10:33:46.494Z",
            iFullName: "Alex",
            id: "QU001020-00024",
            lastUpdateDate: "2018-03-06T10:34:02.082Z",
            pCid: "CP001020-00003",
            pFullName: "Alex",
            type: "quotation"
          }
        ]
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_QUOTATION", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.quotation = {
      type: "quotation",
      baseProductCode: "SAV",
      baseProductId: "08_product_SAV_1",
      baseProductName: {
        en: "SavvySaver",
        "zh-Hant": "SavvySaver"
      },
      productLine: "WLE",
      budgetRules: ["RP"],
      compCode: "01",
      agentCode: "011011",
      dealerGroup: "AGENCY",
      agent: {
        agentCode: "011011",
        name: "Hockin Tsui",
        dealerGroup: "AGENCY",
        company: "AXA Insurance Pte Ltd",
        tel: "0",
        mobile: "00000000",
        email: "hokyin.tsui@eabsystems.com"
      },
      pCid: "CP001001-00001",
      pFullName: "Arya Stark",
      pFirstName: "Arya",
      pLastName: "Stark",
      pGender: "F",
      pDob: "1996-12-07",
      pAge: 21,
      pResidence: "R2",
      pSmoke: "N",
      pEmail: "aryastark@test.com",
      pOccupation: "O1527",
      pOccupationClass: "4",
      sameAs: "Y",
      ccy: "SGD",
      policyOptions: {},
      iCid: "CP001001-00001",
      iFullName: "Arya Stark",
      iFirstName: "Arya",
      iLastName: "Stark",
      iGender: "F",
      iDob: "1996-12-07",
      iAge: 21,
      iResidence: "R2",
      iSmoke: "N",
      iEmail: "aryastark@test.com",
      iOccupation: "O1527",
      iOccupationClass: "4",
      quickQuote: true,
      fund: null,
      extraFlags: {
        nationalityResidence: {
          iCategory: "A1",
          iRejected: "N",
          pCategory: "A1",
          pRejected: "N"
        }
      },
      isBackDate: "N",
      riskCommenDate: "2018-05-07",
      createDate: "2018-05-07T01:52:52.023Z",
      lastUpdateDate: "2018-05-07T01:52:52.023Z",
      plans: [
        {
          covCode: "SAV",
          covName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          productLine: "WLE",
          version: 1,
          saViewInd: "Y"
        },
        {
          covCode: "PET",
          covName: {
            en: "PremiumEraser Total",
            "zh-Hant": "PremiumEraser Total"
          },
          version: 1,
          productLine: "WLE",
          saViewInd: "N",
          sumInsured: null,
          premRate: 0,
          yearPrem: null,
          halfYearPrem: null,
          quarterPrem: null,
          monthPrem: null
        }
      ],
      policyOptionsDesc: {},
      premium: 0,
      totYearPrem: null,
      totHalfyearPrem: null,
      totQuarterPrem: null,
      totMonthPrem: null
    };

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
        newQuotation: {
          type: "quotation",
          baseProductCode: "SAV",
          baseProductId: "08_product_SAV_1",
          baseProductName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          productLine: "WLE",
          budgetRules: ["RP"],
          compCode: "01",
          agentCode: "011011",
          dealerGroup: "AGENCY",
          agent: {
            agentCode: "011011",
            name: "Hockin Tsui",
            dealerGroup: "AGENCY",
            company: "AXA Insurance Pte Ltd",
            tel: "0",
            mobile: "00000000",
            email: "hokyin.tsui@eabsystems.com"
          },
          pCid: "CP001001-00001",
          pFullName: "Arya Stark",
          pFirstName: "Arya",
          pLastName: "Stark",
          pGender: "F",
          pDob: "1996-12-07",
          pAge: 21,
          pResidence: "R2",
          pSmoke: "N",
          pEmail: "aryastark@test.com",
          pOccupation: "O1527",
          pOccupationClass: "4",
          sameAs: "Y",
          ccy: "SGD",
          policyOptions: {},
          iCid: "CP001001-00001",
          iFullName: "Arya Stark",
          iFirstName: "Arya",
          iLastName: "Stark",
          iGender: "F",
          iDob: "1996-12-07",
          iAge: 21,
          iResidence: "R2",
          iSmoke: "N",
          iEmail: "aryastark@test.com",
          iOccupation: "O1527",
          iOccupationClass: "4",
          quickQuote: true,
          fund: null,
          extraFlags: {
            nationalityResidence: {
              iCategory: "A1",
              iRejected: "N",
              pCategory: "A1",
              pRejected: "N"
            }
          },
          isBackDate: "N",
          riskCommenDate: "2018-05-07",
          createDate: "2018-05-07T01:52:52.023Z",
          lastUpdateDate: "2018-05-07T01:52:52.023Z",
          plans: [
            {
              covCode: "SAV",
              covName: {
                en: "SavvySaver",
                "zh-Hant": "SavvySaver"
              },
              productLine: "WLE",
              version: 1,
              saViewInd: "Y"
            },
            {
              covCode: "PET",
              covName: {
                en: "PremiumEraser Total",
                "zh-Hant": "PremiumEraser Total"
              },
              version: 1,
              productLine: "WLE",
              saViewInd: "N",
              sumInsured: null,
              premRate: 0,
              yearPrem: null,
              halfYearPrem: null,
              quarterPrem: null,
              monthPrem: null
            }
          ],
          policyOptionsDesc: {},
          premium: 0,
          totYearPrem: null,
          totHalfyearPrem: null,
          totQuarterPrem: null,
          totMonthPrem: null
        }
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_PLAN_DETAILS", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.planDetails = {
      SAV: {
        allowBackdate: "Y",
        benlim: [
          {
            ageFr: "0",
            ageTo: "99",
            class: "*",
            country: "*",
            limits: [
              {
                ccy: "SGD",
                max: 0,
                min: 0,
                payMode: "A"
              },
              {
                ccy: "SGD",
                max: 0,
                min: 0,
                payMode: "S"
              },
              {
                ccy: "SGD",
                max: 0,
                min: 0,
                payMode: "Q"
              },
              {
                ccy: "SGD",
                max: 0,
                min: 0,
                payMode: "M"
              }
            ]
          }
        ],
        brochures: 6849732,
        budgetRules: ["RP"],
        ccyDesc: {
          en: "Currency Description",
          "zh-Hant": "Currency Description"
        },
        classInd: "N",
        classSecDesc: {
          en: "Class Description"
        },
        coexist: [
          {
            ageFr: 0,
            ageTo: 99,
            country: "*",
            dealerGroup: "*",
            groupA: ["PPERA"],
            groupB: ["PPEPS"],
            shouldCoExist: "N"
          }
        ],
        compCode: "08",
        corpInd: "N",
        covCode: "SAV",
        covName: {
          en: "SavvySaver",
          "zh-Hant": "SavvySaver"
        },
        create: "Super Admin  on 11 Dec 2017 12:30:31",
        ctyGroup: ["*"],
        currencies: [
          {
            ccy: ["SGD"],
            country: "*"
          }
        ],
        dealerGroup: [
          "AGENCY",
          "BROKER",
          "FUSION",
          "SYNERGY",
          "DIRECT",
          "SINGPOST"
        ],
        effDate: "2014-01-01 00:00:00",
        entryAge: [
          {
            country: "*",
            imaxAge: 60,
            imaxAgeUnit: "nearestAge",
            iminAge: 1,
            iminAgeUnit: "month",
            omaxAge: 60,
            omaxAgeUnit: "nearestAge",
            ominAge: 18,
            ominAgeUnit: "year",
            sameAsOwner: "N"
          }
        ],
        expDate: "2018-08-08 00:00:00",
        genderInd: "*",
        illustrationColumns: [
          {
            id: "totalPremPaid",
            isRelatedToProjectionRate: "N",
            title: {
              en: "Premium Paid",
              "zh-Hant": "Premium Paid"
            }
          },
          {
            id: "totalDB",
            isRelatedToProjectionRate: "Y",
            title: {
              en: "Death Benefit",
              "zh-Hant": "Death Benefit"
            }
          },
          {
            id: "totalSV",
            isRelatedToProjectionRate: "Y",
            title: {
              en: "Surrender Value",
              "zh-Hant": "Surrender Value"
            }
          }
        ],
        insuredAgeDesc: {
          en: "Age"
        },
        jointLives: "N",
        keyProdFeatures: {
          en: "Key Product Features"
        },
        licence: [""],
        nfInd: "N",
        payMethodsDesc: {
          en: "Description"
        },
        payModeDesc: {
          en: "Payment Mode"
        },
        payModes: [
          {
            covMonth: 1,
            default: "N",
            factor: 1,
            mode: "A",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.51,
            mode: "S",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.26,
            mode: "Q",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.0875,
            mode: "M",
            operator: "M"
          }
        ],
        paymentMethods: [
          "eNets",
          "teleTransfter",
          "crCard",
          "cash",
          "dbsCrCardIpp",
          "axasam",
          "chequeCashierOrder"
        ],
        planCodeMapping: {
          planCode: ["15SS", "18SS", "21SS", "24SS"],
          policyTerm: ["15", "18", "21", "24"]
        },
        planInd: "B",
        polMaturities: [
          {
            country: "*",
            default: 0,
            interval: 3,
            maxAge: 99,
            maxTerm: 24,
            minTerm: 15,
            ownerMaxAge: 99,
            sameAsOwner: "Y"
          }
        ],
        polTermDesc: {
          en: "Policy Team Desc",
          "zh-Hant": ""
        },
        polTermInd: "Y",
        policyOptions: [],
        premInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        premInputInd: "Y",
        premMatSameAsPolMat: "Y",
        premStructurs: [],
        premTermDesc: {
          en: "Prem Term Desc"
        },
        premTermInd: "N",
        premTermRule: "BT",
        premType: "R",
        premlim: [
          {
            ageFr: "0",
            ageTo: "99",
            class: "*",
            country: "*",
            limits: [
              {
                ccy: "SGD",
                max: "",
                min: 1143,
                payMode: "A"
              },
              {
                ccy: "SGD",
                max: "",
                min: 582.93,
                payMode: "S"
              },
              {
                ccy: "SGD",
                max: "",
                min: 297.18,
                payMode: "Q"
              },
              {
                ccy: "SGD",
                max: "",
                min: 100.01,
                payMode: "M"
              }
            ]
          }
        ],
        prodFeature: {
          en: "Product Features"
        },
        prod_summary: 2251160,
        productCategory: "cv",
        productLine: "WLE",
        projection: [
          {
            rates: {
              CRB_15: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              CRB_18: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              CRB_21: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0
              ],
              CRB_24: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054
              ],
              CashBack: [
                0.03,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_15": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                2.2,
                2.2,
                2.2,
                2.2,
                2.2,
                2.9,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_18": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                2.2,
                2.2,
                2.2,
                2.2,
                2.2,
                2.65,
                2.65,
                2.75,
                3.4,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_21": [
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                2.3,
                2.3,
                2.3,
                2.3,
                2.3,
                2.85,
                2.85,
                2.95,
                3.05,
                3.15,
                3.4,
                3.6,
                0,
                0,
                0
              ],
              "Death TB_24": [
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                2.5,
                2.5,
                2.5,
                2.5,
                2.5,
                3.05,
                3.05,
                3.15,
                3.25,
                3.35,
                3.6,
                3.7,
                3.8,
                3.9,
                4.1
              ],
              "Mat TB_15": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1.45,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Mat TB_18": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1.95,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Mat TB_21": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                2,
                0,
                0,
                0
              ],
              "Mat TB_24": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                2.65
              ],
              ProjectInvestReturn: [
                0.0325,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_15: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_18: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_21: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0,
                0,
                0
              ],
              RB_24: [
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054,
                0.0054
              ],
              "Surr TB_15": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.2,
                1.2,
                1.2,
                1.2,
                1.2,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Surr TB_18": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.6,
                1.6,
                1.6,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Surr TB_21": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                0,
                0,
                0,
                0
              ],
              "Surr TB_24": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                0
              ]
            },
            seq: 100,
            title: {
              en: "3.25"
            }
          },
          {
            rates: {
              CRB_15: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              CRB_18: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              CRB_21: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0
              ],
              CRB_24: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089
              ],
              CashBack: [
                0.03,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_15": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                2.2,
                2.2,
                2.2,
                2.2,
                2.2,
                2.9,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_18": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                2.2,
                2.2,
                2.2,
                2.2,
                2.2,
                2.65,
                2.65,
                2.75,
                3.4,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Death TB_21": [
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                2.3,
                2.3,
                2.3,
                2.3,
                2.3,
                2.85,
                2.85,
                2.95,
                3.05,
                3.15,
                3.4,
                3.6,
                0,
                0,
                0
              ],
              "Death TB_24": [
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                2.5,
                2.5,
                2.5,
                2.5,
                2.5,
                3.05,
                3.05,
                3.15,
                3.25,
                3.35,
                3.6,
                3.7,
                3.8,
                3.9,
                4.1
              ],
              "Mat TB_15": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1.45,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Mat TB_18": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1.95,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Mat TB_21": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                2,
                0,
                0,
                0
              ],
              "Mat TB_24": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                2.65
              ],
              ProjectInvestReturn: [
                0.0475,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_15: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_18: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              RB_21: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0,
                0,
                0
              ],
              RB_24: [
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089,
                0.0089
              ],
              "Surr TB_15": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.2,
                1.2,
                1.2,
                1.2,
                1.2,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Surr TB_18": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.6,
                1.6,
                1.6,
                0,
                0,
                0,
                0,
                0,
                0,
                0
              ],
              "Surr TB_21": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                0,
                0,
                0,
                0
              ],
              "Surr TB_24": [
                0,
                0,
                0,
                0,
                0,
                0.9,
                0.9,
                0.9,
                0.9,
                1.4,
                1.4,
                1.4,
                1.4,
                1.4,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                1.8,
                0
              ]
            },
            seq: 200,
            title: {
              en: "4.75"
            }
          }
        ],
        ref_no: "SavvySaver",
        releaseDate: "2018-03-07 14:52:01",
        remarks: {
          en: ""
        },
        reportTemplate: [
          {
            covCode: "0",
            desc: "",
            pdfCode: "SAV_INTRO",
            pdfType: "COVER_PAGE",
            seq: 200
          },
          {
            covCode: "0",
            desc: "",
            pdfCode: "SAV_MAIN_BI",
            pdfType: "BNFT_ILL",
            seq: 300
          },
          {
            covCode: "0",
            desc: "",
            pdfCode: "SAV_SUPP_BI",
            pdfType: "BNFT_ILL",
            seq: 400
          },
          {
            covCode: "PPERA",
            desc: "",
            pdfCode: "SAV_SUPP_TDC"
          },
          {
            covCode: "PPEPS",
            desc: "",
            pdfCode: "SAV_SUPP_TDC"
          },
          {
            covCode: "PET",
            desc: "",
            pdfCode: "SAV_SUPP_TDC"
          },
          {
            covCode: "PET",
            desc: "",
            pdfCode: "SAV_ACK"
          },
          {
            covCode: "0",
            desc: "",
            pdfCode: "SAV_PROD_SUMMARY"
          }
        ],
        riderList: [
          {
            condition: [
              {
                autoAttach: "N",
                compulsory: "N",
                country: "*",
                dealerGroup: "*",
                endAge: 60,
                saRate: "",
                saRule: "BP",
                staAge: 18
              }
            ],
            covCode: "PPERA"
          },
          {
            condition: [
              {
                autoAttach: "Y",
                compulsory: "N",
                country: "*",
                dealerGroup: "*",
                endAge: 60,
                saRate: "",
                saRule: "BP",
                staAge: 18
              }
            ],
            covCode: "PPEPS"
          },
          {
            condition: [
              {
                autoAttach: "Y",
                compulsory: "N",
                country: "*",
                dealerGroup: "*",
                endAge: 60,
                saRate: "",
                saRule: "BP",
                staAge: 18
              }
            ],
            covCode: "PET"
          }
        ],
        saDesc: {
          en: "Description"
        },
        saInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 1
          }
        ],
        saInputInd: "Y",
        saScale: "",
        saSecDesc: {
          en: "Description",
          "zh-Hant": ""
        },
        saViewInd: "Y",
        smokeInd: "*",
        status: "A",
        supportIllustration: "Y",
        task_desc: "SavvySaver",
        thumbnail3: 44107,
        thumbnail4: 0,
        type: "product",
        upd: "JOHN.TAM  on 07 Mar 2018 11:54:22",
        version: 1,
        wholeLifeInd: "N"
      },
      PPERA: {
        budgetRules: ["RP"],
        ccyDesc: {
          en: "Currency Description"
        },
        classInd: "N",
        classSecDesc: {
          en: "Class Description"
        },
        compCode: "08",
        corpInd: "N",
        covCode: "PPERA",
        covName: {
          en: "Smart Payer PremiumEraser",
          "zh-Hant": "Smart Payer PremiumEraser"
        },
        ctyGroup: ["*"],
        currencies: [
          {
            ccy: ["SGD"],
            country: "*"
          }
        ],
        dealerGroup: [
          "agency",
          "AGENCY",
          "BROKER",
          "FUSION",
          "SYNERGY",
          "DIRECT",
          "SINGPOST"
        ],
        effDate: "2017-05-23 00:00:00",
        entryAge: [],
        expDate: "2018-05-24 10:58:56",
        genderInd: "*",
        insuredAgeDesc: {
          en: "11"
        },
        jointLives: "N",
        keyProdFeatures: {
          en: "Key Product Features",
          "zh-Hant": "Key Product Features"
        },
        levelPrem: {
          en: "Premium Description"
        },
        licence: ["p1"],
        payMethodsDesc: {
          en: "Description"
        },
        payModeDesc: {
          en: "Description"
        },
        payModes: [
          {
            covMonth: 1,
            default: "N",
            factor: 1,
            mode: "A",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.51,
            mode: "S",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.26,
            mode: "Q",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.0875,
            mode: "M",
            operator: "M"
          }
        ],
        paymentMethods: [
          "crCard",
          "eNets",
          "dbsCrCardIpp",
          "axasam",
          "chequeCashierOrder",
          "cash",
          "teleTransfter"
        ],
        planCodeMapping: {
          planCode: ["15PE", "18PE", "21PE", "24PE", "PE65"],
          policyTerm: ["15", "18", "21", "24", "Until Age 65"]
        },
        planInd: "R",
        polMatSameAsPremMat: "N",
        polMaturities: [
          {
            country: "*",
            default: 0,
            interval: 3,
            maxAge: 99,
            maxTerm: 24,
            minTerm: 15,
            ownerMaxAge: 99,
            sameAsOwner: "Y"
          }
        ],
        polTermDesc: {
          en: "pol maturity"
        },
        polTermInd: "N",
        polTermRule: "BBT",
        policyMatSameAsBasic: "N",
        premInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        premInputInd: "N",
        premMatSameAsBasic: "N",
        premMatSameAsPolMat: "N",
        premMaturities: [
          {
            country: "*",
            default: 0,
            interval: 3,
            maxAge: 99,
            maxTerm: 24,
            minTerm: 15,
            ownerMaxAge: 99,
            sameAsOwner: "Y"
          }
        ],
        premTermDesc: {
          en: "PM"
        },
        premTermInd: "N",
        premTermRule: "BT",
        prodFeature: {
          en: "111"
        },
        prod_summary: 525508,
        productCategory: "cv",
        productLine: "WLE",
        ref_no: "PPERA",
        releaseDate: "2018-03-07 14:52:05",
        remarks: {
          en: "Remarks",
          "zh-Hant": "Remarks"
        },
        saDesc: {
          en: "Description"
        },
        saInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        saInputInd: "N",
        saRule: "na",
        saScale: 0,
        saSecDesc: {
          en: "Description"
        },
        saViewInd: "N",
        smokeInd: "*",
        status: "A",
        task_desc: "PPERA",
        type: "product",
        version: 1,
        wholeLifeInd: "N"
      },
      PPEPS: {
        allowBackdate: "N",
        budgetRules: ["RP"],
        ccyDesc: {
          en: "Currency Description"
        },
        classInd: "N",
        classSecDesc: {
          en: "Class Description"
        },
        compCode: "08",
        corpInd: "N",
        covCode: "PPEPS",
        covName: {
          en: "Smart Payer PremiumEraser Plus",
          "zh-Hant": "Smart Payer PremiumEraser Plus"
        },
        ctyGroup: ["*"],
        currencies: [
          {
            ccy: ["SGD"],
            country: "*"
          }
        ],
        dealerGroup: [
          "BROKER",
          "AGENCY",
          "FUSION",
          "SYNERGY",
          "DIRECT",
          "SINGPOST"
        ],
        effDate: "2017-05-17 12:09:03",
        entryAge: [],
        expDate: "2018-05-17 12:13:25",
        genderInd: "*",
        insuredAgeDesc: {
          en: "Age Description"
        },
        jointLives: "N",
        keyProdFeatures: {
          en: "Key Product Features"
        },
        levelPrem: {
          en: "Premium Description"
        },
        licence: [""],
        payMethodsDesc: {
          en: "Description"
        },
        payModeDesc: {
          en: "Description"
        },
        payModes: [
          {
            covMonth: 1,
            default: "N",
            factor: 1,
            mode: "A",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.51,
            mode: "S",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.26,
            mode: "Q",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.0875,
            mode: "M",
            operator: "M"
          }
        ],
        paymentMethods: [
          "crCard",
          "eNets",
          "dbsCrCardIpp",
          "axasam",
          "chequeCashierOrder",
          "cash",
          "teleTransfter"
        ],
        planCodeMapping: {
          planCode: ["15PEC", "18PEC", "21PEC", "24PEC", "PEC65"],
          policyTerm: ["15", "18", "21", "24", "Until Age 65"]
        },
        planInd: "R",
        polMatSameAsPremMat: "N",
        polMaturities: [
          {
            country: "*",
            default: 0,
            interval: 3,
            maxAge: 99,
            maxTerm: 24,
            minTerm: 15,
            ownerMaxAge: 99,
            sameAsOwner: "Y"
          }
        ],
        polTermDesc: {
          en: "PM"
        },
        polTermInd: "N",
        polTermRule: "BBT",
        policyMatSameAsBasic: "N",
        premInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        premInputInd: "N",
        premMatSameAsBasic: "N",
        premMatSameAsPolMat: "N",
        premMaturities: [
          {
            country: "*",
            default: 0,
            interval: 3,
            maxAge: 99,
            maxTerm: 24,
            minTerm: 15,
            ownerMaxAge: 99,
            sameAsOwner: "Y"
          }
        ],
        premTermDesc: {
          en: "sadas"
        },
        premTermInd: "N",
        premTermRule: "BT",
        prodFeature: {
          en: "Product Features English"
        },
        prod_summary: 852600,
        productCategory: "cv",
        productLine: "WLE",
        ref_no: "PPEPS",
        releaseDate: "2018-03-07 14:52:04",
        remarks: {
          en: "Remarks"
        },
        saDesc: {
          en: "Description"
        },
        saInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        saInputInd: "N",
        saRule: "na",
        saScale: "",
        saSecDesc: {
          en: "Description"
        },
        saViewInd: "N",
        smokeInd: "*",
        status: "A",
        task_desc: "PPEP",
        type: "product",
        version: 1,
        wholeLifeInd: "N"
      },
      PET: {
        allowBackdate: "N",
        budgetRules: ["RP"],
        ccyDesc: {
          en: "Currency Description"
        },
        classInd: "N",
        classSecDesc: {
          en: "Class Description"
        },
        compCode: "08",
        corpInd: "N",
        covCode: "PET",
        covName: {
          en: "PremiumEraser Total",
          "zh-Hant": "PremiumEraser Total"
        },
        create: "johntest22  on 17 May 2017 16:07:49",
        ctyGroup: ["*"],
        currencies: [
          {
            ccy: ["SGD"],
            country: "*"
          }
        ],
        dealerGroup: [
          "BROKER",
          "AGENCY",
          "SYNERGY",
          "FUSION",
          "SINGPOST",
          "DIRECT"
        ],
        effDate: "2016-05-17 15:58:52",
        entryAge: [],
        expDate: "2018-05-17 15:59:39",
        genderInd: "*",
        insuredAgeDesc: {
          en: "Age Description"
        },
        jointLives: "N",
        keyProdFeatures: {
          en: "Key Product Features"
        },
        levelPrem: {
          en: "Premium Description"
        },
        licence: [""],
        payMethodsDesc: {
          en: "Description",
          "zh-Hant": ""
        },
        payModeDesc: {
          en: "Description"
        },
        payModes: [
          {
            covMonth: 1,
            default: "N",
            factor: 1,
            mode: "A",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.51,
            mode: "S",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.26,
            mode: "Q",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "",
            factor: 0.0875,
            mode: "M",
            operator: "M"
          }
        ],
        paymentMethods: [
          "crCard",
          "axasam",
          "dbsCrCardIpp",
          "eNets",
          "chequeCashierOrder",
          "teleTransfter",
          "cash"
        ],
        planCodeMapping: {
          planCode: [
            "5WOP",
            "6WOP",
            "7WOP",
            "8WOP",
            "9WOP",
            "10WOP",
            "11WOP",
            "12WOP",
            "13WOP",
            "14WOP",
            "15WOP",
            "16WOP",
            "17WOP",
            "18WOP",
            "19WOP",
            "20WOP",
            "21WOP",
            "22WOP",
            "23WOP",
            "24WOP"
          ],
          policyTerm: [
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"
          ]
        },
        planInd: "R",
        polMatSameAsPremMat: "N",
        polTermDesc: {
          en: "Policy Team Description"
        },
        polTermInd: "N",
        polTermRule: "BBT",
        policyMatSameAsBasic: "Y",
        premInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        premInputInd: "N",
        premMatSameAsBasic: "N",
        premMatSameAsPolMat: "Y",
        premTermDesc: {
          en: "Premium Term Description"
        },
        premTermInd: "N",
        premTermRule: "BT",
        prodFeature: {
          en: "Product Features"
        },
        prod_summary: 1211664,
        productCategory: "cv",
        productLine: "WLE",
        ref_no: "PET",
        releaseDate: "2018-03-07 14:52:04",
        remarks: {
          en: "Remarks"
        },
        saDesc: {
          en: "Description"
        },
        saInput: [
          {
            ccy: "SGD",
            decimal: 2,
            factor: 0
          }
        ],
        saInputInd: "N",
        saRule: "na",
        saScale: 0,
        saSecDesc: {
          en: "Description"
        },
        saViewInd: "N",
        smokeInd: "*",
        status: "A",
        task_desc: "Description",
        type: "product",
        upd: "SYSTEM  on 20 Jul 2017 17:29:39",
        version: 1,
        wholeLifeInd: "N"
      }
    };

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_PLAN_DETAILS,
        newPlanDetails: {
          SAV: {
            allowBackdate: "Y",
            benlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: 0,
                    min: 0,
                    payMode: "M"
                  }
                ]
              }
            ],
            brochures: 6849732,
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description",
              "zh-Hant": "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            coexist: [
              {
                ageFr: 0,
                ageTo: 99,
                country: "*",
                dealerGroup: "*",
                groupA: ["PPERA"],
                groupB: ["PPEPS"],
                shouldCoExist: "N"
              }
            ],
            compCode: "08",
            corpInd: "N",
            covCode: "SAV",
            covName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            },
            create: "Super Admin  on 11 Dec 2017 12:30:31",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2014-01-01 00:00:00",
            entryAge: [
              {
                country: "*",
                imaxAge: 60,
                imaxAgeUnit: "nearestAge",
                iminAge: 1,
                iminAgeUnit: "month",
                omaxAge: 60,
                omaxAgeUnit: "nearestAge",
                ominAge: 18,
                ominAgeUnit: "year",
                sameAsOwner: "N"
              }
            ],
            expDate: "2018-08-08 00:00:00",
            genderInd: "*",
            illustrationColumns: [
              {
                id: "totalPremPaid",
                isRelatedToProjectionRate: "N",
                title: {
                  en: "Premium Paid",
                  "zh-Hant": "Premium Paid"
                }
              },
              {
                id: "totalDB",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Death Benefit",
                  "zh-Hant": "Death Benefit"
                }
              },
              {
                id: "totalSV",
                isRelatedToProjectionRate: "Y",
                title: {
                  en: "Surrender Value",
                  "zh-Hant": "Surrender Value"
                }
              }
            ],
            insuredAgeDesc: {
              en: "Age"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            licence: [""],
            nfInd: "N",
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Payment Mode"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "eNets",
              "teleTransfter",
              "crCard",
              "cash",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder"
            ],
            planCodeMapping: {
              planCode: ["15SS", "18SS", "21SS", "24SS"],
              policyTerm: ["15", "18", "21", "24"]
            },
            planInd: "B",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "Policy Team Desc",
              "zh-Hant": ""
            },
            polTermInd: "Y",
            policyOptions: [],
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "Y",
            premMatSameAsPolMat: "Y",
            premStructurs: [],
            premTermDesc: {
              en: "Prem Term Desc"
            },
            premTermInd: "N",
            premTermRule: "BT",
            premType: "R",
            premlim: [
              {
                ageFr: "0",
                ageTo: "99",
                class: "*",
                country: "*",
                limits: [
                  {
                    ccy: "SGD",
                    max: "",
                    min: 1143,
                    payMode: "A"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 582.93,
                    payMode: "S"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 297.18,
                    payMode: "Q"
                  },
                  {
                    ccy: "SGD",
                    max: "",
                    min: 100.01,
                    payMode: "M"
                  }
                ]
              }
            ],
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 2251160,
            productCategory: "cv",
            productLine: "WLE",
            projection: [
              {
                rates: {
                  CRB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0325,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054,
                    0.0054
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 100,
                title: {
                  en: "3.25"
                }
              },
              {
                rates: {
                  CRB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  CRB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  CRB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  CashBack: [
                    0.03,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.9,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.2,
                    2.65,
                    2.65,
                    2.75,
                    3.4,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Death TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.3,
                    2.85,
                    2.85,
                    2.95,
                    3.05,
                    3.15,
                    3.4,
                    3.6,
                    0,
                    0,
                    0
                  ],
                  "Death TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    1,
                    1,
                    1,
                    1,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    2.5,
                    3.05,
                    3.05,
                    3.15,
                    3.25,
                    3.35,
                    3.6,
                    3.7,
                    3.8,
                    3.9,
                    4.1
                  ],
                  "Mat TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.45,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    1.95,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2,
                    0,
                    0,
                    0
                  ],
                  "Mat TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    2.65
                  ],
                  ProjectInvestReturn: [
                    0.0475,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_15: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_18: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  RB_21: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0,
                    0,
                    0
                  ],
                  RB_24: [
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089,
                    0.0089
                  ],
                  "Surr TB_15": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    1.2,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_18": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.6,
                    1.6,
                    1.6,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_21": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0,
                    0,
                    0,
                    0
                  ],
                  "Surr TB_24": [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0.9,
                    0.9,
                    0.9,
                    0.9,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.4,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    1.8,
                    0
                  ]
                },
                seq: 200,
                title: {
                  en: "4.75"
                }
              }
            ],
            ref_no: "SavvySaver",
            releaseDate: "2018-03-07 14:52:01",
            remarks: {
              en: ""
            },
            reportTemplate: [
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_INTRO",
                pdfType: "COVER_PAGE",
                seq: 200
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_MAIN_BI",
                pdfType: "BNFT_ILL",
                seq: 300
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_SUPP_BI",
                pdfType: "BNFT_ILL",
                seq: 400
              },
              {
                covCode: "PPERA",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PPEPS",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_SUPP_TDC"
              },
              {
                covCode: "PET",
                desc: "",
                pdfCode: "SAV_ACK"
              },
              {
                covCode: "0",
                desc: "",
                pdfCode: "SAV_PROD_SUMMARY"
              }
            ],
            riderList: [
              {
                condition: [
                  {
                    autoAttach: "N",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPERA"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PPEPS"
              },
              {
                condition: [
                  {
                    autoAttach: "Y",
                    compulsory: "N",
                    country: "*",
                    dealerGroup: "*",
                    endAge: 60,
                    saRate: "",
                    saRule: "BP",
                    staAge: 18
                  }
                ],
                covCode: "PET"
              }
            ],
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 1
              }
            ],
            saInputInd: "Y",
            saScale: "",
            saSecDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            saViewInd: "Y",
            smokeInd: "*",
            status: "A",
            supportIllustration: "Y",
            task_desc: "SavvySaver",
            thumbnail3: 44107,
            thumbnail4: 0,
            type: "product",
            upd: "JOHN.TAM  on 07 Mar 2018 11:54:22",
            version: 1,
            wholeLifeInd: "N"
          },
          PPERA: {
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPERA",
            covName: {
              en: "Smart Payer PremiumEraser",
              "zh-Hant": "Smart Payer PremiumEraser"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "agency",
              "AGENCY",
              "BROKER",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-23 00:00:00",
            entryAge: [],
            expDate: "2018-05-24 10:58:56",
            genderInd: "*",
            insuredAgeDesc: {
              en: "11"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features",
              "zh-Hant": "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: ["p1"],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PE", "18PE", "21PE", "24PE", "PE65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "pol maturity"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "PM"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "111"
            },
            prod_summary: 525508,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPERA",
            releaseDate: "2018-03-07 14:52:05",
            remarks: {
              en: "Remarks",
              "zh-Hant": "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPERA",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PPEPS: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PPEPS",
            covName: {
              en: "Smart Payer PremiumEraser Plus",
              "zh-Hant": "Smart Payer PremiumEraser Plus"
            },
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "FUSION",
              "SYNERGY",
              "DIRECT",
              "SINGPOST"
            ],
            effDate: "2017-05-17 12:09:03",
            entryAge: [],
            expDate: "2018-05-17 12:13:25",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description"
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "eNets",
              "dbsCrCardIpp",
              "axasam",
              "chequeCashierOrder",
              "cash",
              "teleTransfter"
            ],
            planCodeMapping: {
              planCode: ["15PEC", "18PEC", "21PEC", "24PEC", "PEC65"],
              policyTerm: ["15", "18", "21", "24", "Until Age 65"]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            polTermDesc: {
              en: "PM"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "N",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "N",
            premMaturities: [
              {
                country: "*",
                default: 0,
                interval: 3,
                maxAge: 99,
                maxTerm: 24,
                minTerm: 15,
                ownerMaxAge: 99,
                sameAsOwner: "Y"
              }
            ],
            premTermDesc: {
              en: "sadas"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features English"
            },
            prod_summary: 852600,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PPEPS",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: "",
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "PPEP",
            type: "product",
            version: 1,
            wholeLifeInd: "N"
          },
          PET: {
            allowBackdate: "N",
            budgetRules: ["RP"],
            ccyDesc: {
              en: "Currency Description"
            },
            classInd: "N",
            classSecDesc: {
              en: "Class Description"
            },
            compCode: "08",
            corpInd: "N",
            covCode: "PET",
            covName: {
              en: "PremiumEraser Total",
              "zh-Hant": "PremiumEraser Total"
            },
            create: "johntest22  on 17 May 2017 16:07:49",
            ctyGroup: ["*"],
            currencies: [
              {
                ccy: ["SGD"],
                country: "*"
              }
            ],
            dealerGroup: [
              "BROKER",
              "AGENCY",
              "SYNERGY",
              "FUSION",
              "SINGPOST",
              "DIRECT"
            ],
            effDate: "2016-05-17 15:58:52",
            entryAge: [],
            expDate: "2018-05-17 15:59:39",
            genderInd: "*",
            insuredAgeDesc: {
              en: "Age Description"
            },
            jointLives: "N",
            keyProdFeatures: {
              en: "Key Product Features"
            },
            levelPrem: {
              en: "Premium Description"
            },
            licence: [""],
            payMethodsDesc: {
              en: "Description",
              "zh-Hant": ""
            },
            payModeDesc: {
              en: "Description"
            },
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            paymentMethods: [
              "crCard",
              "axasam",
              "dbsCrCardIpp",
              "eNets",
              "chequeCashierOrder",
              "teleTransfter",
              "cash"
            ],
            planCodeMapping: {
              planCode: [
                "5WOP",
                "6WOP",
                "7WOP",
                "8WOP",
                "9WOP",
                "10WOP",
                "11WOP",
                "12WOP",
                "13WOP",
                "14WOP",
                "15WOP",
                "16WOP",
                "17WOP",
                "18WOP",
                "19WOP",
                "20WOP",
                "21WOP",
                "22WOP",
                "23WOP",
                "24WOP"
              ],
              policyTerm: [
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24"
              ]
            },
            planInd: "R",
            polMatSameAsPremMat: "N",
            polTermDesc: {
              en: "Policy Team Description"
            },
            polTermInd: "N",
            polTermRule: "BBT",
            policyMatSameAsBasic: "Y",
            premInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            premInputInd: "N",
            premMatSameAsBasic: "N",
            premMatSameAsPolMat: "Y",
            premTermDesc: {
              en: "Premium Term Description"
            },
            premTermInd: "N",
            premTermRule: "BT",
            prodFeature: {
              en: "Product Features"
            },
            prod_summary: 1211664,
            productCategory: "cv",
            productLine: "WLE",
            ref_no: "PET",
            releaseDate: "2018-03-07 14:52:04",
            remarks: {
              en: "Remarks"
            },
            saDesc: {
              en: "Description"
            },
            saInput: [
              {
                ccy: "SGD",
                decimal: 2,
                factor: 0
              }
            ],
            saInputInd: "N",
            saRule: "na",
            saScale: 0,
            saSecDesc: {
              en: "Description"
            },
            saViewInd: "N",
            smokeInd: "*",
            status: "A",
            task_desc: "Description",
            type: "product",
            upd: "SYSTEM  on 20 Jul 2017 17:29:39",
            version: 1,
            wholeLifeInd: "N"
          }
        }
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_INPUT_CONFIGS", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.inputConfigs = {
      SAV: {
        payModes: [
          {
            covMonth: 1,
            default: "N",
            factor: 1,
            mode: "A",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.51,
            mode: "S",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.26,
            mode: "Q",
            operator: "M"
          },
          {
            covMonth: 1,
            default: "N",
            factor: 0.0875,
            mode: "M",
            operator: "M"
          }
        ],
        policyOptions: [],
        canEditClassType: false,
        canEditPolicyTerm: true,
        policyTermList: [
          {
            value: "15",
            title: "15 Years",
            default: false
          },
          {
            value: "18",
            title: "18 Years",
            default: false
          },
          {
            value: "21",
            title: "21 Years",
            default: false
          },
          {
            value: "24",
            title: "24 Years",
            default: false
          }
        ],
        canEditPremTerm: false,
        premTermList: [
          {
            value: "15",
            title: "15 Years",
            default: false
          },
          {
            value: "18",
            title: "18 Years",
            default: false
          },
          {
            value: "21",
            title: "21 Years",
            default: false
          },
          {
            value: "24",
            title: "24 Years",
            default: false
          }
        ],
        canEditSumAssured: false,
        canEditPremium: false,
        canViewSumAssured: true,
        canViewOthSa: false,
        canEditOthSa: false,
        saInput: {
          ccy: "SGD",
          decimal: 2,
          factor: 1
        },
        premInput: {
          ccy: "SGD",
          decimal: 2,
          factor: 0
        },
        premlim: [],
        benlim: [],
        riderList: [
          {
            covCode: "PET",
            autoAttach: "Y",
            compulsory: "N",
            saRate: "",
            saRule: "BP"
          }
        ]
      },
      PET: {
        policyOptions: [],
        canEditClassType: false,
        canEditPolicyTerm: false,
        policyTermList: [
          {
            value: "15",
            title: "15 Years",
            default: false
          },
          {
            value: "18",
            title: "18 Years",
            default: false
          },
          {
            value: "21",
            title: "21 Years",
            default: false
          },
          {
            value: "24",
            title: "24 Years",
            default: false
          }
        ],
        canEditPremTerm: false,
        premTermList: [
          {
            value: "15",
            title: "15 Years",
            default: false
          },
          {
            value: "18",
            title: "18 Years",
            default: false
          },
          {
            value: "21",
            title: "21 Years",
            default: false
          },
          {
            value: "24",
            title: "24 Years",
            default: false
          }
        ],
        canEditSumAssured: false,
        canEditPremium: false,
        canViewSumAssured: false,
        canViewOthSa: false,
        canEditOthSa: false,
        saInput: {
          ccy: "SGD",
          decimal: 2,
          factor: 0
        },
        premInput: {
          ccy: "SGD",
          decimal: 2,
          factor: 0
        }
      }
    };

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_INPUT_CONFIGS,
        newInputConfigs: {
          SAV: {
            payModes: [
              {
                covMonth: 1,
                default: "N",
                factor: 1,
                mode: "A",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.51,
                mode: "S",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.26,
                mode: "Q",
                operator: "M"
              },
              {
                covMonth: 1,
                default: "N",
                factor: 0.0875,
                mode: "M",
                operator: "M"
              }
            ],
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: true,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: true,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 1
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premlim: [],
            benlim: [],
            riderList: [
              {
                covCode: "PET",
                autoAttach: "Y",
                compulsory: "N",
                saRate: "",
                saRule: "BP"
              }
            ]
          },
          PET: {
            policyOptions: [],
            canEditClassType: false,
            canEditPolicyTerm: false,
            policyTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditPremTerm: false,
            premTermList: [
              {
                value: "15",
                title: "15 Years",
                default: false
              },
              {
                value: "18",
                title: "18 Years",
                default: false
              },
              {
                value: "21",
                title: "21 Years",
                default: false
              },
              {
                value: "24",
                title: "24 Years",
                default: false
              }
            ],
            canEditSumAssured: false,
            canEditPremium: false,
            canViewSumAssured: false,
            canViewOthSa: false,
            canEditOthSa: false,
            saInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            },
            premInput: {
              ccy: "SGD",
              decimal: 2,
              factor: 0
            }
          }
        }
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_QUOTATION_ERRORS", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.quotationErrors = {
      mandatoryErrors: {},
      otherErrors: [],
      planErrors: {
        SAV: [
          {
            covCode: "SAV",
            msgPara: ["$1,143.00", 0],
            code: "js.err.invalid_premlim_min"
          }
        ]
      },
      policyOptionErrors: {}
    };

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_ERRORS,
        newQuotationErrors: {
          mandatoryErrors: {},
          otherErrors: [],
          planErrors: {
            SAV: [
              {
                covCode: "SAV",
                msgPara: ["$1,143.00", 0],
                code: "js.err.invalid_premlim_min"
              }
            ]
          },
          policyOptionErrors: {}
        }
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_QUOTATION_WARNING", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.quotWarnings = [
      "Application will be subject to underwriting review based on Life Assured's residency and/or nationality"
    ];

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_WARNINGS,
        newQuotationWarnings: [
          "Application will be subject to underwriting review based on Life Assured's residency and/or nationality"
        ]
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_AVAILABLE_INSUREDS", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.availableInsureds = [
      {
        profile: {
          addrBlock: "123",
          addrCity: "",
          addrCountry: "R2",
          addrEstate: "",
          addrStreet: "321",
          age: 21,
          agentId: "011011",
          allowance: 1233455,
          applicationCount: 1,
          bundle: [
            {
              id: "FN001001-00001",
              isValid: false
            },
            {
              id: "FN001001-00007",
              isValid: false
            },
            {
              id: "FN001001-00008",
              isValid: false
            },
            {
              id: "FN001001-00009",
              isValid: false
            },
            {
              id: "FN001001-00010",
              isValid: false
            },
            {
              id: "FN001001-00011",
              isValid: false
            },
            {
              id: "FN001001-00012",
              isValid: true
            }
          ],
          cid: "CP001001-00001",
          dependants: [
            {
              cid: "CP001001-00002",
              relationship: "FAT",
              relationshipOther: null
            }
          ],
          dob: "1996-12-07",
          education: "above",
          email: "aryastark@test.com",
          employStatus: "ft",
          firstName: "Arya",
          fnaRecordIdArray: "",
          fullName: "Arya Stark",
          gender: "F",
          hanyuPinyinName: "",
          haveSignDoc: true,
          idCardNo: "S1234567D",
          idDocType: "nric",
          industry: "I2",
          initial: "SA",
          isSmoker: "N",
          language: "en",
          lastName: "Stark",
          lastUpdateDate: "2017-12-18T07:29:53.831Z",
          marital: "S",
          mobileCountryCode: "+65",
          mobileNo: "223344",
          nameOrder: "F",
          nationality: "N1",
          nearAge: 21,
          occupation: "O1527",
          organization: "123123",
          organizationCountry: "R2",
          othName: "",
          otherMobileCountryCode: "+65",
          otherNo: "",
          photo: 1505181974183,
          postalCode: "123",
          referrals: "",
          residenceCountry: "R2",
          title: "Ms",
          type: "cust",
          unitNum: ""
        },
        valid: true,
        eligible: true
      },
      {
        profile: {
          addrBlock: "",
          addrCity: "",
          addrCountry: "R2",
          addrEstate: "",
          addrStreet: "",
          age: 30,
          agentId: "011011",
          allowance: 123332,
          bundle: [
            {
              id: "FN001001-00002",
              isValid: true
            }
          ],
          cid: "CP001001-00002",
          dependants: [
            {
              cid: "CP001001-00001",
              relationship: "DAU"
            }
          ],
          dob: "1987-12-13",
          education: "above",
          email: "",
          employStatus: "ft",
          firstName: "Eddard",
          fnaRecordIdArray: "",
          fullName: "Eddard Stark",
          gender: "M",
          hanyuPinyinName: "",
          idCardNo: "S1234567D",
          idDocType: "nric",
          industry: "I3",
          initial: "SE",
          isSmoker: "N",
          language: "en",
          lastName: "Stark",
          lastUpdateDate: "2018-01-03T09:17:43.954Z",
          marital: "M",
          mobileCountryCode: "+65",
          mobileNo: "12365489",
          nameOrder: "F",
          nationality: "N1",
          nearAge: 30,
          occupation: "O99",
          organization: "",
          organizationCountry: "R2",
          othName: "",
          otherMobileCountryCode: "+65",
          otherNo: "",
          photo: 1500460588805,
          postalCode: "",
          referrals: "",
          residenceCountry: "R2",
          title: "Mr",
          type: "cust",
          unitNum: ""
        },
        valid: true,
        eligible: true
      }
    ];

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_INSUREDS,
        newAvailableInsureds: [
          {
            profile: {
              addrBlock: "123",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "321",
              age: 21,
              agentId: "011011",
              allowance: 1233455,
              applicationCount: 1,
              bundle: [
                {
                  id: "FN001001-00001",
                  isValid: false
                },
                {
                  id: "FN001001-00007",
                  isValid: false
                },
                {
                  id: "FN001001-00008",
                  isValid: false
                },
                {
                  id: "FN001001-00009",
                  isValid: false
                },
                {
                  id: "FN001001-00010",
                  isValid: false
                },
                {
                  id: "FN001001-00011",
                  isValid: false
                },
                {
                  id: "FN001001-00012",
                  isValid: true
                }
              ],
              cid: "CP001001-00001",
              dependants: [
                {
                  cid: "CP001001-00002",
                  relationship: "FAT",
                  relationshipOther: null
                }
              ],
              dob: "1996-12-07",
              education: "above",
              email: "aryastark@test.com",
              employStatus: "ft",
              firstName: "Arya",
              fnaRecordIdArray: "",
              fullName: "Arya Stark",
              gender: "F",
              hanyuPinyinName: "",
              haveSignDoc: true,
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I2",
              initial: "SA",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2017-12-18T07:29:53.831Z",
              marital: "S",
              mobileCountryCode: "+65",
              mobileNo: "223344",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 21,
              occupation: "O1527",
              organization: "123123",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1505181974183,
              postalCode: "123",
              referrals: "",
              residenceCountry: "R2",
              title: "Ms",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          },
          {
            profile: {
              addrBlock: "",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "",
              age: 30,
              agentId: "011011",
              allowance: 123332,
              bundle: [
                {
                  id: "FN001001-00002",
                  isValid: true
                }
              ],
              cid: "CP001001-00002",
              dependants: [
                {
                  cid: "CP001001-00001",
                  relationship: "DAU"
                }
              ],
              dob: "1987-12-13",
              education: "above",
              email: "",
              employStatus: "ft",
              firstName: "Eddard",
              fnaRecordIdArray: "",
              fullName: "Eddard Stark",
              gender: "M",
              hanyuPinyinName: "",
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I3",
              initial: "SE",
              isSmoker: "N",
              language: "en",
              lastName: "Stark",
              lastUpdateDate: "2018-01-03T09:17:43.954Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "12365489",
              nameOrder: "F",
              nationality: "N1",
              nearAge: 30,
              occupation: "O99",
              organization: "",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: 1500460588805,
              postalCode: "",
              referrals: "",
              residenceCountry: "R2",
              title: "Mr",
              type: "cust",
              unitNum: ""
            },
            valid: true,
            eligible: true
          }
        ]
      }
    )).toEqual(expectData);
  });

  it("should handle UPDATE_AVAILABLE_FUNDS", () => {
    let expectData = _.cloneDeep(initialState);
    expectData.availableFunds = [
      {
        "assetClass": "mixed_assets_asia_pacific_ex_japan",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "ABFD",
        "fundName": {
          "en": "AXA Asian Balanced Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "equity_asia_pacific_ex_japan",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "ZAGF",
        "fundName": {
          "en": "AXA Asian Growth Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "mixed_assets_asia_pacific_ex_japan",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "AINC",
        "fundName": {
          "en": "AXA Asian Income Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "equity_greater_china",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "CHGF",
        "fundName": {
          "en": "AXA China Growth Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "mixed_assets_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "DSAF",
        "fundName": {
          "en": "AXA Dynamic Strategies Aggressive Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "equity_singapore",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "ZFFA",
        "fundName": {
          "en": "AXA Fortress Fund A (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "asset_allocation_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "PLAN",
        "fundName": {
          "en": "AXA Global Balanced Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 3,
        "version": 1
      }, {
        "assetClass": "bond_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "GRDN",
        "fundName": {
          "en": "AXA Global Defensive Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 1,
        "version": 1
      }, {
        "assetClass": "equity_emerging_markets",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "GEME",
        "fundName": {
          "en": "AXA Global Emerging Markets Equity (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "equity_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "GEQB",
        "fundName": {
          "en": "AXA Global Equity Blend (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "asset_allocation_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SEEK",
        "fundName": {
          "en": "AXA Global Growth Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "equity_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "ENTP",
        "fundName": {
          "en": "AXA Global High Growth Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "asset_allocation_global",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "HVST",
        "fundName": {
          "en": "AXA Global Secure Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 2,
        "version": 1
      }, {
        "assetClass": "equity_healthcare",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "HEAL",
        "fundName": {
          "en": "AXA Health Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "equity_india",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "INDF",
        "fundName": {
          "en": "AXA India Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "equity_asia_pacific_ex_japan",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "PAEQ",
        "fundName": {
          "en": "AXA Pacific Equity Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "equity_global_shariah",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SGEF",
        "fundName": {
          "en": "AXA Shariah Global Equity Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "bond_singapore",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "ASGD",
        "fundName": {
          "en": "AXA Short Duration Bond Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 1,
        "version": 1
      }, {
        "assetClass": "mixed_assets_singapore",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SBFD",
        "fundName": {
          "en": "AXA Singapore Balanced Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "Y",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 4,
        "version": 1
      }, {
        "assetClass": "bond_singapore",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SBON",
        "fundName": {
          "en": "AXA Singapore Bond Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
        "riskRating": 1,
        "version": 1
      }, {
        "assetClass": "equity_singapore",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SGEQ",
        "fundName": {
          "en": "AXA Singapore Equity Fund (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }, {
        "assetClass": "equity_asean",
        "ccy": "SGD",
        "compCode": "08",
        "fundCode": "SEAS",
        "fundName": {
          "en": "AXA South East Asia Special Situations (SGD)",
          "zh-Hant": ""
        },
        "isMixedAsset": "N",
        "paymentMethod": ["cash", "srs", "cpfisoa"],
        "riskRating": 5,
        "version": 1
      }
    ];

    expect(reducer(
      {},
      {
        type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS,
        newAvailableFunds: [
          {
            "assetClass": "mixed_assets_asia_pacific_ex_japan",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "ABFD",
            "fundName": {
              "en": "AXA Asian Balanced Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "equity_asia_pacific_ex_japan",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "ZAGF",
            "fundName": {
              "en": "AXA Asian Growth Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "mixed_assets_asia_pacific_ex_japan",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "AINC",
            "fundName": {
              "en": "AXA Asian Income Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "equity_greater_china",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "CHGF",
            "fundName": {
              "en": "AXA China Growth Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "mixed_assets_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "DSAF",
            "fundName": {
              "en": "AXA Dynamic Strategies Aggressive Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "equity_singapore",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "ZFFA",
            "fundName": {
              "en": "AXA Fortress Fund A (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "asset_allocation_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "PLAN",
            "fundName": {
              "en": "AXA Global Balanced Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 3,
            "version": 1
          }, {
            "assetClass": "bond_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "GRDN",
            "fundName": {
              "en": "AXA Global Defensive Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 1,
            "version": 1
          }, {
            "assetClass": "equity_emerging_markets",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "GEME",
            "fundName": {
              "en": "AXA Global Emerging Markets Equity (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "equity_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "GEQB",
            "fundName": {
              "en": "AXA Global Equity Blend (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "asset_allocation_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SEEK",
            "fundName": {
              "en": "AXA Global Growth Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "equity_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "ENTP",
            "fundName": {
              "en": "AXA Global High Growth Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "asset_allocation_global",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "HVST",
            "fundName": {
              "en": "AXA Global Secure Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 2,
            "version": 1
          }, {
            "assetClass": "equity_healthcare",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "HEAL",
            "fundName": {
              "en": "AXA Health Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "equity_india",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "INDF",
            "fundName": {
              "en": "AXA India Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "equity_asia_pacific_ex_japan",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "PAEQ",
            "fundName": {
              "en": "AXA Pacific Equity Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "equity_global_shariah",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SGEF",
            "fundName": {
              "en": "AXA Shariah Global Equity Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "bond_singapore",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "ASGD",
            "fundName": {
              "en": "AXA Short Duration Bond Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 1,
            "version": 1
          }, {
            "assetClass": "mixed_assets_singapore",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SBFD",
            "fundName": {
              "en": "AXA Singapore Balanced Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "Y",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 4,
            "version": 1
          }, {
            "assetClass": "bond_singapore",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SBON",
            "fundName": {
              "en": "AXA Singapore Bond Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa", "cpfissa"],
            "riskRating": 1,
            "version": 1
          }, {
            "assetClass": "equity_singapore",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SGEQ",
            "fundName": {
              "en": "AXA Singapore Equity Fund (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }, {
            "assetClass": "equity_asean",
            "ccy": "SGD",
            "compCode": "08",
            "fundCode": "SEAS",
            "fundName": {
              "en": "AXA South East Asia Special Situations (SGD)",
              "zh-Hant": ""
            },
            "isMixedAsset": "N",
            "paymentMethod": ["cash", "srs", "cpfisoa"],
            "riskRating": 5,
            "version": 1
          }
        ]
      }
    )).toEqual(expectData);
  });
});
