import * as _ from "lodash";
import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { TEXT_FIELD } from "../constants/FIELD_TYPES";
import { CLIENT_FORM, PRE_APPLICATION } from "../constants/REDUCER_TYPES";
import { MAX_CURRENCY_VALUE } from "../constants/NUMBER_FORMAT";
import TEXT_STORE, { LANGUAGE_TYPES } from "../locales";
import { calcAge } from "../utilities/common";
import { optionCondition } from "../utilities/trigger";
import {
  validateEmail,
  validateID,
  validateMandatory
} from "../utilities/validation";
// =============================================================================
// initial states
// =============================================================================
const configInitialState = {
  isCreate: false,
  isFamilyMember: false,
  isApplication: false,
  isProposerMissing: false,
  isFromProfile: false,
  isFromProduct: false,
  isFNA: false,
  isPDA: false,
  isTrustedIndividual: false
};
const errorInitialState = {
  hasError: false,
  message: {
    [LANGUAGE_TYPES.ENGLISH]: "",
    [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
  }
};
// =============================================================================
// reducers
// =============================================================================
/**
 * config
 * @description clientForm config. trigger function or change behavior
 * @requires configInitialState
 * @default configInitialState
 * */
const config = (state = configInitialState, { type, configData }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM:
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG:
      return Object.assign({}, configInitialState, configData);
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return configInitialState;
    default:
      return state;
  }
};
/**
 * relationship
 * @description client form dialog relationship field
 * @default ""
 * */
const relationship = (state = "", { type, newRelationship }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      state = newRelationship;
      return state;
    case ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_ON_CHANGE:
      state = newRelationship;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isRelationshipError
 * @requires errorInitialState
 * @reducer clientForm.isFamilyMember
 * @reducer clientForm.relationship
 * @reducer clientForm.gender
 * @reducer client.profile
 * @rule if reducer isFamilyMember, isPDA, isTrustedIndividual is true,
 *   relationship is mandatory field
 * @rule if isTrustedIndividual is false, reducer relationship should match
 *   reducer gender state
 * @rule if isTrustedIndividual is false, only allow one spouse
 * @default Object.assign({ cid: "" }, errorInitialState)
 * */
const isRelationshipError = (
  state = Object.assign({ cid: "" }, errorInitialState),
  { type, relationshipData, genderData, profileData, configData, newCid }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return Object.assign({}, { cid: newCid });
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_RELATIONSHIP: {
      /* check mandatory */
      const checkMandatory = validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory:
            configData.isFamilyMember ||
            configData.isPDA ||
            configData.isTrustedIndividual,
          disabled: false
        },
        value: relationshipData
      });

      if (checkMandatory.hasError) {
        return Object.assign(
          {
            cid: newCid || state.cid
          },
          checkMandatory
        );
      }

      /* check gender rule */
      if (!configData.isTrustedIndividual) {
        const maleRule =
          ["GFA", "SON", "FAT", "BRO", "GSO"].indexOf(relationshipData) > -1 &&
          genderData === "F";
        const femaleRule =
          ["GMO", "MOT", "DAU", "GDA", "SIS"].indexOf(relationshipData) > -1 &&
          genderData === "M";

        if (
          maleRule ||
          femaleRule ||
          (relationshipData === "SPO" && genderData === profileData.gender)
        ) {
          return {
            hasError: true,
            message: {
              [LANGUAGE_TYPES.ENGLISH]:
                TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.601"],
              [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
                TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.601"]
            },
            cid: newCid || state.cid
          };
        }
      }

      /* check spouse rule */
      if (!configData.isTrustedIndividual) {
        let isSpouseInvalid =
          relationshipData === "SPO" && profileData.marital !== "M";
        if (isSpouseInvalid) {
          return {
            hasError: true,
            message: {
              [LANGUAGE_TYPES.ENGLISH]:
                TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.607"],
              [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
                TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.607"]
            },
            cid: newCid || state.cid
          };
        }

        const spouseRule = dependant =>
          dependant.relationship === "SPO" &&
          relationshipData === "SPO" &&
          (newCid ? newCid !== dependant.cid : state.cid !== dependant.cid) &&
          !configData.isAPP;

        if (profileData.dependants && !configData.isPDA) {
          profileData.dependants.forEach(dependant => {
            isSpouseInvalid = isSpouseInvalid || spouseRule(dependant);
          });
        }

        if (isSpouseInvalid) {
          return {
            hasError: true,
            message: {
              [LANGUAGE_TYPES.ENGLISH]:
                TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.602"],
              [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
                TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.602"]
            },
            cid: newCid || state.cid
          };
        }
      }

      /* relationship is valid */
      return errorInitialState;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return Object.assign({ cid: "" }, errorInitialState);
    default:
      return state;
  }
};
/**
 * relationshipOther
 * @description client form dialog please specify field
 * @reducer clientForm.relationship
 * @rule if relationship !== "OTH", clean
 * @default ""
 * */
const relationshipOther = (state = "", { type, newRelationshipOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      state = newRelationshipOther;
      return state;
    case ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE:
      state = newRelationshipOther;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isRelationshipOtherError
 * @reducer clientForm.relationship
 * @reducer clientForm.relationshipOther
 * @rule if reducer relationship is OTH(other), please specify field is
 *   mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isRelationshipOtherError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, relationshipOtherData, relationshipData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_RELATIONSHIP_OTHER: {
      const mandatory = relationshipData === "OTH";

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: relationshipOtherData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * givenName
 * @description client form dialog given name field
 * @rule up to 30 characters
 * @default ""
 * */
const givenName = (state = "", { type, newGivenName }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newGivenName;
    case ACTION_TYPES[CLIENT_FORM].GIVEN_NAME_ON_CHANGE:
      state = newGivenName.substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isGivenNameError
 * @reducer clientForm.givenName
 * @rule given name is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isGivenNameError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, givenNameData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_GIVEN_NAME:
      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: givenNameData
      });
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * surname
 * @description client form dialog surname name field
 * @rule up to 30 characters
 * @default ""
 * */
const surname = (state = "", { type, newSurname }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newSurname;
    case ACTION_TYPES[CLIENT_FORM].SURNAME_ON_CHANGE:
      state = newSurname.substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * otherName
 * @description client form dialog other name field
 * @rule up to 30 characters
 * @default ""
 * */
const otherName = (state = "", { type, newOtherName }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newOtherName;
    case ACTION_TYPES[CLIENT_FORM].OTHER_NAME_ON_CHANGE:
      state = newOtherName.substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * hanYuPinYinName
 * @description client form dialog hanyu pinyin name field
 * @rule up to 30 characters
 * @default ""
 * */
const hanYuPinYinName = (state = "", { type, newHanYuPinYinName }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newHanYuPinYinName;
    case ACTION_TYPES[CLIENT_FORM].HAN_YU_PIN_YIN_ON_CHANGE:
      state = newHanYuPinYinName.substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * name
 * @description client form dialog name field
 * @rule up to 30 characters
 * @rule automatically populate by new name, new first name and name order
 * @default ""
 * */
const name = (state = "", { type, newName }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newName;
    case ACTION_TYPES[CLIENT_FORM].NAME_ON_CHANGE:
      state = newName.trim().substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isNameError
 * @requires errorInitialState
 * @reducer clientForm.givenName
 * @rule mandatory field
 * @rule should fill given name first
 * @default errorInitialState
 * */
const isNameError = (
  state = errorInitialState,
  { type, nameData, givenNameData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_NAME: {
      let error = errorInitialState;

      error = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: nameData
      });

      if (!error.hasError && nameData !== "" && !givenNameData) {
        error = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.603"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.603"]
          }
        };
      }

      return error;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * nameOrder
 * @description client form dialog name order field
 * @default "L"
 * */
const nameOrder = (state = "L", { type, newNameOrder }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newNameOrder;
    case ACTION_TYPES[CLIENT_FORM].NAME_ORDER_ON_CHANGE:
      return newNameOrder;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "L";
    default:
      return state;
  }
};
/**
 * title
 * @description client form title order field
 * @default ""
 * */
const title = (state = "", { type, newTitle }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newTitle;
    case ACTION_TYPES[CLIENT_FORM].TITLE_ON_CHANGE:
      return newTitle;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isTitleError
 * @description client form dialog title field validation status
 * @reducer clientForm.title
 * @reducer clientForm.gender
 * @rule if reducer title is Mr and gender is filled, reducer gender must be M.
 *   if reducer title is Ms, Mrs or Mdm and gender is filled, reducer gender
 *   must be M
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isTitleError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, titleData, genderData, isFNA, isTrustedIndividual }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_TITLE: {
      const ruleA =
        titleData === "Mr" && genderData !== "M" && genderData !== "";
      const ruleB =
        (titleData === "Ms" || titleData === "Mrs" || titleData === "Mdm") &&
        genderData !== "F" &&
        genderData !== "";

      if ((ruleA || ruleB) && !isTrustedIndividual) {
        state = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.601"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.601"]
          }
        };
        return state;
      }

      const mandatory = isFNA;
      if (mandatory) {
        return validateMandatory({
          field: { type: TEXT_FIELD, mandatory, disabled: false },
          value: titleData
        });
      }

      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * gender
 * @description client form gender field
 * @reducer clientForm.relationship
 * @reducer client.profile
 * @rule if relationship is one of GFA(grand father), SON(son), FAT(father),
 *   BRO(brother) or GSO(grand son), gender is M(male)
 * @rule if relationship is one of GMO(grand mother), MOT(mother),
 *   DAU(daughter), GDA(grand daughter) or SIS(sister), gender is F(female)
 * @rule if relationship is SPO(Spouse) and client.profile.gender is F(female),
 *   gender is M(male)
 * @rule if relationship is SPO(Spouse) and client.profile.gender is M(male),
 *   gender is F(female)
 * @default ""
 * */
const gender = (state = "", { type, newGender }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newGender;
    case ACTION_TYPES[CLIENT_FORM].GENDER_ON_CHANGE:
      return newGender;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isGenderError
 * @description client form dialog gender field validation status
 * @reducer clientForm.gender
 * @rule if form is for proposer or family member, gender is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isGenderError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, genderData, configData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_GENDER: {
      const mandatory =
        (configData.isCreate && !configData.isTrustedIndividual) ||
        configData.isFamilyMember;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: genderData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * birthday
 * @description client form gender field
 * @default ""
 * */
const birthday = (state = "", { type, newBirthday }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newBirthday;
    case ACTION_TYPES[CLIENT_FORM].BIRTHDAY_ON_CHANGE:
      return newBirthday;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isBirthdayError
 * */
const isBirthdayError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  {
    type,
    birthdayData,
    isProposerMissingData,
    isFamilyMemberData,
    isFNA,
    isPDA,
    isAPP
  }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_BIRTHDAY: {
      const mandatory =
        isProposerMissingData || isFamilyMemberData || isFNA || isPDA || isAPP;
      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: birthdayData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * nationality
 * @description client form nationality field
 * @default ""
 * */
const nationality = (state = "", { type, newNationality }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newNationality;
    case ACTION_TYPES[CLIENT_FORM].NATIONALITY_ON_CHANGE:
      return newNationality;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * singaporePRStatus
 * @description client form singapore PR status field
 * @reducer clientForm.nationality
 * @rule if nationality have value and === "N1" or "N2", singaporePRStatus clean
 * @default ""
 * */
const singaporePRStatus = (state = "", { type, newSingaporePRStatus }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newSingaporePRStatus;
    case ACTION_TYPES[CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE:
      return newSingaporePRStatus;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isSingaporePRStatusError
 * @description client form dialog singaporePRStatus field validation status
 * @reducer clientForm.nationality
 * @reducer clientForm.singaporePRStatus
 * @rule if reducer nationality is not N1(Singaporean) or N2(unknown),
 *   singaporePRStatus is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isSingaporePRStatusError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, singaporePRStatusData, nationalityData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_SINGAPORE_PR_STATUS: {
      const mandatory =
        !!nationalityData &&
        nationalityData !== "N1" &&
        nationalityData !== "N2";

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: singaporePRStatusData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * countryOfResidence
 * @description client form country of residence field
 * @default ""
 * */
const countryOfResidence = (state = "R2", { type, newCountryOfResidence }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newCountryOfResidence;
    case ACTION_TYPES[CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE:
      return newCountryOfResidence;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "R2";
    default:
      return state;
  }
};
/**
 * cityOfResidence
 * @description client form city of residence field
 * @reducer clientForm.countryOfResidence
 * @rule if reducer countryOfResidence value do not match some value,
 *   cityOfResidence will be set to ""
 * @default ""
 * */
const cityOfResidence = (state = "", { type, newCityOfResidence }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newCityOfResidence;
    case ACTION_TYPES[CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE:
      return newCityOfResidence;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isCityOfResidenceError
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.countryOfResidence
 * @reducer optionsMap
 * @rule if reducer countryOfResidence value match some value, cityOfResidence
 *   is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isCityOfResidenceError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, cityOfResidenceData, countryOfResidenceData, optionsMapData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_CITY_OF_RESIDENCE: {
      const mandatory = optionCondition({
        value: countryOfResidenceData,
        options: "city",
        optionsMap: optionsMapData,
        showIfNoValue: false
      });

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: cityOfResidenceData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * otherCityOfResidence
 * @reducer clientForm.cityOfResidence
 * @rule if cityOfResidence do not match some value, clean
 * @default ""
 * */
const otherCityOfResidence = (
  state = "",
  { type, newOtherCityOfResidence }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newOtherCityOfResidence;
    case ACTION_TYPES[CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE:
      state = newOtherCityOfResidence.substring(0, 50);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isOtherCityOfResidenceError
 * @reducer clientForm.isApplication
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.otherCityOfResidence
 * @reducer optionsMap
 * @rule if isApplication is true and cityOfResidence is other option,
 *   otherCityOfResidence is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isOtherCityOfResidenceError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, cityOfResidenceData, otherCityOfResidenceData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_OTHER_CITY_OF_RESIDENCE: {
      const otherCityOfResidenceList = [
        "T110",
        "T120",
        "T205",
        "T207",
        "T208",
        "T209",
        "T224"
      ];
      const rule = _.includes(otherCityOfResidenceList, cityOfResidenceData);

      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory: rule,
          disabled: false
        },
        value: otherCityOfResidenceData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * IDDocumentType
 * @description client form dialog ID document type field
 * @reducer clientForm.nationality
 * @reducer clientForm.prStatus
 * @rule if reducer nationality is N1(Singaporean) or N2. Or reducer
 *   singaporePRStatus is Y, value will be lock to nric
 * @default ""
 * */
const IDDocumentType = (
  state = "",
  { type, newIDDocType, nationalityData, singaporePRStatusData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newIDDocType;
    case ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE:
      state =
        nationalityData === "N1" ||
        nationalityData === "N2" ||
        singaporePRStatusData === "Y"
          ? "nric"
          : newIDDocType;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDDocumentTypeError
 * @requires errorInitialState
 * @reducer clientForm.IDDocumentType
 * @reducer clientForm.config
 * @rule if config.isFNA or config.isTrustedIndividual is true, IDDocumentType
 *   is mandatory
 * @default errorInitialState
 * */
const isIDDocumentTypeError = (
  state = errorInitialState,
  { type, IDDocumentTypeData, configData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE: {
      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory:
            configData.isFNA ||
            configData.isTrustedIndividual ||
            configData.isPDA ||
            configData.isAPP,
          disabled: false
        },
        value: IDDocumentTypeData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * IDDocumentTypeOther
 * @reducer clientForm.IDDocumentType
 * @rule up to 25 characters
 * @rule if IDDocumentType !== "other", clean
 * @default ""
 * */
const IDDocumentTypeOther = (state = "", { type, newIDDocTypeOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newIDDocTypeOther;
    case ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE:
      state = newIDDocTypeOther.substring(0, 25);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDDocumentTypeOtherError
 * @reducer clientForm.IDDocumentType
 * @reducer clientForm.IDDocumentTypeOther
 * @rule if IDDocumentType is other(other), IDDocumentTypeOther is mandatory
 *   field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
       [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isIDDocumentTypeOtherError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, IDDocumentTypeOtherData, IDDocumentTypeData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE_OTHER: {
      const mandatory = IDDocumentTypeData === "other";

      state = validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: IDDocumentTypeOtherData
      });

      return state;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * ID
 * @description client form dialog ID field
 * @rule up to 20 characters
 * @rule must be uppercase
 * @default ""
 * */
const ID = (state = "", { type, newID }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newID;
    case ACTION_TYPES[CLIENT_FORM].ID_ON_CHANGE:
      state = newID.substring(0, 20).toUpperCase();
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDError
 * @description client form dialog ID field validation status
 * @requires errorInitialState
 * @reducer clientForm.config
 * @reducer clientForm.ID
 * @reducer clientForm.IDType
 * @rule if reducer ID or reducer IDType update, update value by result of the
 *   ID format validation with ID type
 * @rule if config.isFNA or config.isTrustedIndividual is true, ID is mandatory
 * @default errorInitialState
 * */
const isIDError = (
  state = errorInitialState,
  { type, IDData, IDDocumentTypeData, configData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_ID: {
      let error = errorInitialState;

      if (
        configData.isFNA ||
        configData.isTrustedIndividual ||
        configData.isAPP
      ) {
        error = validateMandatory({
          field: {
            type: TEXT_FIELD,
            mandatory: true,
            disabled: false
          },
          value: IDData
        });
      }

      if (!error.hasError && IDData !== "") {
        error = validateID({
          ID: IDData,
          IDDocumentType: IDDocumentTypeData
        });
      }
      return error;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * smokingStatus
 * @description client form dialog smoking status field
 * @default ""
 * */
const smokingStatus = (state = "", { type, newSmokingStatus }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newSmokingStatus;
    case ACTION_TYPES[CLIENT_FORM].SMOKING_STATUS_ON_CHANGE:
      state = newSmokingStatus;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isSmokingError
 * */
const isSmokingError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, smokingStatusData, isProposerMissingData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_SMOKING_STATUS: {
      const mandatory = isProposerMissingData;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: smokingStatusData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * maritalStatus
 * @description client form dialog marital status field
 * @rule if reducer relationship is "SPO"(spouse), value will be lock to "M"
 * @default ""
 * */
const maritalStatus = (
  state = "",
  { type, newMaritalStatus, relationshipData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newMaritalStatus;
    case ACTION_TYPES[CLIENT_FORM].MARITAL_STATUS_ON_CHANGE:
      return relationshipData === "SPO" ? "M" : newMaritalStatus;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isMaritalStatusError
 * */
const isMaritalStatusError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, maritalData, isFNA, isAPP }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_MARITAL_STATUS: {
      const mandatory = isFNA || isAPP;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: maritalData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};

/**
 * education
 * @default ""
 * */
const education = (state = "", { type, newEducation }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newEducation;
    case ACTION_TYPES[CLIENT_FORM].EDUCATION_ON_CHANGE:
      state = newEducation;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isEducationError
 * */
const isEducationError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, educationData, isFNA }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_EDUCATION: {
      const mandatory = isFNA;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: educationData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};

/**
 * employStatus
 * @description client form dialog employ status field
 * @reducer clientForm.birthday
 * @reducer clientForm.occupation
 * @rule if occupation change to O674(Househusband), change employStatus to
 *   hh(Househusband)
 * @rule if occupation change to O675(Housewife), change employStatus to
 *   hw(Housewife)
 * @rule if occupation change to O1450(Unemployed), change employStatus to
 *   ue(Unemployed)
 * @rule if occupation change to O1132(Retiree / Pensioner), change
 *   employStatus to rt(Retired)
 * @rule if occupation change to O1321(Student (Age 18 and above)) or
 *   O1322(Student (Below age 18)) or age < 18, change employStatus to
 *   sd(Student)
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1321(Student (Age 18 and above)) or O1322(Student
 *   (Below age 18)) to out of these value, change employStatus to ""
 * @default ""
 * */
const employStatus = (state = "", { type, newEmployStatus }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newEmployStatus;
    case ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE:
      state = newEmployStatus;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmployStatusError
 * @description client form dialog ID field validation status
 * @reducer clientForm.employStatus
 * @reducer clientForm.gender
 * @rule if reducer gender is F(female), reducer employStatus can not be
 *   hh(house husband)
 * @rule if reducer gender is M(male), reducer employStatus can not be hw(house
 *   wife)
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
       [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isEmployStatusError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, employStatusData, genderData, isFNA }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_EMPLOY_STATUS: {
      let error = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      const mandatory = isFNA;
      if (mandatory) {
        error = validateMandatory({
          field: { type: TEXT_FIELD, mandatory, disabled: false },
          value: employStatusData
        });
      }

      if (!error.hasError && employStatusData === "hh" && genderData === "F") {
        error = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.605"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.605"]
          }
        };
      }

      if (!error.hasError && employStatusData === "hw" && genderData === "M") {
        error = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.605"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.605"]
          }
        };
      }

      return error;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};
/**
 * employStatusOther
 * @default ""
 * */
const employStatusOther = (state = "", { type, newEmployStatusOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newEmployStatusOther;
    case ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE:
      state = newEmployStatusOther;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmployStatusOtherError
 * @reducer clientForm.employStatus
 * @reducer clientForm.employStatusOther
 * @rule other rule - if reducer employStatus is ot(other), other field is
 *   mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isEmployStatusOtherError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, employStatusData, employStatusOtherData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_EMPLOY_STATUS_OTHER: {
      // other rule
      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory: employStatusData === "ot",
          disabled: false
        },
        value: employStatusOtherData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * industry
 * @reducer clientForm.occupation
 * @reducer optionsMap
 * @rule if occupation change, auto fill industry by occupation condition
 * @default ""
 * */
const industry = (state = "", { type, newIndustry }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newIndustry;
    case ACTION_TYPES[CLIENT_FORM].INDUSTRY_ON_CHANGE:
      state = newIndustry;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIndustryError
 * @reducer clientForm.industry
 * @reducer clientForm.employStatus
 * @rule if reducer employStatus is sd(student) and moduleTypes is APPLICATION,
 *   industry field is mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isIndustryError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  {
    type,
    industryData,
    employStatusData,
    isApplicationData,
    isProposerMissingData
  }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_INDUSTRY: {
      const mandatory =
        (isApplicationData && employStatusData === "sd") ||
        isProposerMissingData;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: industryData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * isIndustryError
 * */
const isNationalityError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, nationalityData, isProposerMissingData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_NATIONALITY: {
      const mandatory = isProposerMissingData;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: nationalityData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * occupation
 * @reducer clientForm.age
 * @reducer clientForm.employStatus
 * @reducer clientForm.industry
 * @rule if employ status is hh(house husband), occupation is O674(house
 *   husband)
 * @rule if employ status is hw(house wife), occupation is O675(house wife)
 * @rule if employ status is ue(unemployed), occupation is O1450(unemployed)
 * @rule if employ status is ue(retired), occupation is
 *   O1132(retiree/pensioner)
 * @rule if age < 6, occupation is O246(child/juvenile/infant)
 * @rule if age < 18, occupation is O1322(student(below age 18))
 * @rule if age >= 18 and employStatus is sd(student), occupation is
 *   O1321(student(age 18 and above))
 * @rule if industry change(not trigger by saga), clean occupation state
 * @default ""
 * */
const occupation = (state = "", { type, newOccupation }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newOccupation;
    case ACTION_TYPES[CLIENT_FORM].OCCUPATION_ON_CHANGE:
      state = newOccupation;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isOccupationError
 * @reducer clientForm.occupation
 * @reducer clientForm.birthday
 * @rule when age < 18, 01321(student above 18) cannot be selected
 * @rule when age >= 18, Student (Below age 18) cannot be selected, when age <
 *   6, should be select Child / Juvenile / Infant, not student (Below age 18)
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isOccupationError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, occupationData, birthdayData, isProposerMissingData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_OCCUPATION: {
      const mandatory = isProposerMissingData;
      if (mandatory) {
        return validateMandatory({
          field: { type: TEXT_FIELD, mandatory, disabled: false },
          value: occupationData
        });
      }

      const age = calcAge(birthdayData);

      const ruleA = occupationData === "O1321" && age && age < 18;
      const ruleB = occupationData === "O1322" && age && (age < 6 || age >= 18);

      if (ruleA || ruleB) {
        return {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.713"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.713"]
          }
        };
      }

      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * occupationOther
 * @reducer clientForm.occupation
 * @rule up to 50 characters
 * @rule if occupation !== O921, clean occupationOther data
 * @default ""
 * */
const occupationOther = (state = "", { type, newOccupationOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newOccupationOther;
    case ACTION_TYPES[CLIENT_FORM].OCCUPATION_OTHER_ON_CHANGE:
      return newOccupationOther.substring(0, 50);
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * language
 * @rule up to 30 characters
 * @default ""
 * */
const language = (state = "", { type, newLanguage }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newLanguage;
    case ACTION_TYPES[CLIENT_FORM].LANGUAGE_ON_CHANGE:
      state = newLanguage.substring(0, 30);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isLanguageOtherError
 * @requires errorInitialState
 * @reducer clientForm.language
 * @reducer clientForm.config
 * @rule if config.isFNA is true, language field is mandatory
 * @default errorInitialState
 * */
const isLanguageError = (
  state = errorInitialState,
  { type, languageData, configData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_LANGUAGE: {
      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory: configData.isFNA,
          disabled: false
        },
        value: languageData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * languageOther
 * @default ""
 * */
const languageOther = (state = "", { type, newLanguageOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newLanguageOther;
    case ACTION_TYPES[CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE:
      state = newLanguageOther;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isLanguageOtherError
 * @reducer clientForm.language
 * @reducer clientForm.languageOther
 * @rule if reducer language is other(other), languageOther field is mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isLanguageOtherError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, languageData, languageOtherData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_LANGUAGE_OTHER: {
      const mandatory = languageData.indexOf("other") !== -1;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: languageOtherData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * nameOfEmployBusinessSchool
 * @reducer clientForm.occupation
 * @reducer clientForm.employStatus
 * @reducer clientForm.birthday
 * @rule if employStatus is one of hh(house husband, hw(house wife),
 *   rt(retired), ue(unemployed), value will be lock to N/A
 * @rule if age < 6, value will be lock to N/A
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1132(Retiree / Pensioner), O1321(Student(Age 18 and
 *   above)) or O1322(Student (Below age 18)) to out of these value,
 *   change nameOfEmployBusinessSchool to ""
 * @default ""
 * */
const nameOfEmployBusinessSchool = (
  state = "",
  { type, newNameOfEmployBusinessSchool, employStatusData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newNameOfEmployBusinessSchool;
    case ACTION_TYPES[CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE: {
      const ruleA = ["hh", "hw", "rt", "ue"].indexOf(employStatusData) > -1;
      if (ruleA) {
        state = "N/A";
      } else {
        state = newNameOfEmployBusinessSchool;
      }
      return state;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * countryOfEmployBusinessSchool
 * @reducer clientForm.occupation
 * @reducer clientForm.employStatus
 * @reducer clientForm.birthday
 * @rule if employStatus is one of hh(house husband, hw(house wife),
 *   rt(retired), ue(unemployed), value will be lock to na
 * @rule if age < 6, value will be lock to na
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1132(Retiree / Pensioner), O1321(Student(Age 18 and
 *   above)) or O1322(Student (Below age 18)) to out of these value,
 *   change countryOfEmployBusinessSchool to R2(singapore)
 * @default ""
 * */
const countryOfEmployBusinessSchool = (
  state = "R2",
  { type, newCountryOfEmployBusinessSchool, employStatusData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newCountryOfEmployBusinessSchool;
    case ACTION_TYPES[CLIENT_FORM]
      .COUNTRY_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE: {
      const ruleA = ["hh", "hw", "rt", "ue"].indexOf(employStatusData) > -1;

      if (ruleA) {
        state = "na";
      } else {
        state = newCountryOfEmployBusinessSchool;
      }

      return state;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "R2";
      return state;
    default:
      return state;
  }
};
/**
 * typeOfPass
 * @description client form dialog type of pass field
 * @default ""
 * */
const typeOfPass = (state = "", { type, newTypeOfPass }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newTypeOfPass;
    case ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE:
      state = newTypeOfPass;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isTypeOfPassError
 * @reducer clientForm.typeOfPass
 * @rule if typeOfPass is not o(other),typeOfPassOther is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isTypeOfPassError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, typeOfPassData, nationalityData, singaporePRStatusData, isAPP }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_TYPE_OF_PASS: {
      const mandatory = isAPP;

      const rule =
        nationalityData &&
        nationality !== "N1" &&
        nationality !== "N2" &&
        singaporePRStatusData &&
        singaporePRStatusData !== "Y";

      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory: mandatory && rule,
          disabled: false
        },
        value: typeOfPassData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};

/**
 * typeOfPassOther
 * @rule up to 24 characters
 * @default ""
 * */
const typeOfPassOther = (state = "", { type, newTypeOfPassOther }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
    case ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE:
      state = newTypeOfPassOther.substring(0, 24);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isTypeOfPassOtherError
 * @reducer clientForm.typeOfPass
 * @rule if typeOfPass is o(other),typeOfPassOther is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isTypeOfPassOtherError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, typeOfPassData, typeOfPassOtherData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_TYPE_OF_PASS_OTHER: {
      const mandatory = typeOfPassData === "o";

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: typeOfPassOtherData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * passExpiryDate
 * @description client form dialog pass expiry date field
 * @rule state should be timestamp format
 * @default NaN
 * */
const passExpiryDate = (state = NaN, { type, newPassExpiryDate }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
    case ACTION_TYPES[CLIENT_FORM].PASS_EXPIRY_DATE_ON_CHANGE:
      return newPassExpiryDate;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = NaN;
      return state;
    default:
      return state;
  }
};
/**
 * income
 * @default NaN
 * */
const income = (state = NaN, { type, newIncome }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newIncome;
    case ACTION_TYPES[CLIENT_FORM].INCOME_ON_CHANGE: {
      /* covert to number type */
      const numberValue = parseInt(newIncome, 10);

      /* check is it covert to a valid number */
      if (!Number.isNaN(numberValue) && numberValue >= 0) {
        return numberValue >= MAX_CURRENCY_VALUE
          ? Number(
              String(numberValue).substring(
                0,
                String(MAX_CURRENCY_VALUE).length
              )
            )
          : numberValue;
      }
      return NaN;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return NaN;
    default:
      return state;
  }
};

/**
 * isIncomeError
 * */
const isIncomeError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, incomeData, isFNA, isAPP }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_INCOME: {
      const mandatory = isFNA || isAPP;

      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory,
          disabled: false,
          numberAllowZero: true
        },
        value: incomeData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};

/**
 * prefixA
 * @default "+65"
 * */
const prefixA = (state = "+65", { type, newPrefixA }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newPrefixA;
    case ACTION_TYPES[CLIENT_FORM].PREFIX_A_ON_CHANGE:
      state = newPrefixA;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "+65";
      return state;
    default:
      return state;
  }
};

/**
 * isPrefixAError
 * */
const isPrefixAError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, prefixAData, isFNA }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_PREFIX_A: {
      const mandatory = isFNA;

      return validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: prefixAData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
      return state;
    default:
      return state;
  }
};

/**
 * mobileNoA
 * @reducer clientForm.prefixA
 * @rule add prefixA before the data
 * @rule The prefixA + mobileNoA number up to 15 characters, if not, delete the
 *   mobile number character
 * @default ""
 * */
const mobileNoA = (state = "", { type, newMobileNoA, prefixAData }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newMobileNoA;
    case ACTION_TYPES[CLIENT_FORM].MOBILE_NO_A_ON_CHANGE: {
      const maxLength = 15 - prefixAData.length;
      state = newMobileNoA.replace(/[^0-9]/g, "").substring(0, maxLength);
      return state;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isMobileNoAError
 * @requires errorInitialState
 * @reducer clientForm.mobileNoA
 * @reducer clientForm.config
 * @rule if config.isFNA or config.isTrustedIndividual is true, mobileNoA is
 *   mandatory
 * @default errorInitialState
 * */
const isMobileNoAError = (
  state = errorInitialState,
  { type, mobileNoAData, configData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_MOBILE_A_NO: {
      return validateMandatory({
        field: {
          type: TEXT_FIELD,
          mandatory: configData.isFNA || configData.isTrustedIndividual,
          disabled: false
        },
        value: mobileNoAData
      });
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};

/**
 * prefixB
 * @default "+65"
 * */
const prefixB = (state = "+65", { type, newPrefixB }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newPrefixB;
    case ACTION_TYPES[CLIENT_FORM].PREFIX_B_ON_CHANGE:
      state = newPrefixB;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "+65";
      return state;
    default:
      return state;
  }
};
/**
 * mobileNoB
 * @reducer clientForm.prefixB
 * @rule add prefixB before the data
 * @rule The prefixB + mobileNoB number up to 15 characters, if not, delete the
 *   mobile number character
 * @default ""
 * */
const mobileNoB = (state = "", { type, newMobileNoB, prefixBData }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newMobileNoB;
    case ACTION_TYPES[CLIENT_FORM].MOBILE_NO_B_ON_CHANGE: {
      const maxLength = 15 - prefixBData.length;
      state = newMobileNoB.replace(/[^0-9]/g, "").substring(0, maxLength);
      return state;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * email
 * @rule up to 50 characters
 * @default ""
 * */
const email = (state = "", { type, newEmail }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newEmail;
    case ACTION_TYPES[CLIENT_FORM].EMAIL_ON_CHANGE:
      state = newEmail.substring(0, 50);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmailError
 * @rule if email !=="", reducer email should match email format
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
const isEmailError = (
  state = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  },
  { type, emailData, isAPP, isProposerMissingData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].INITIAL_VALIDATE:
    case ACTION_TYPES[CLIENT_FORM].VALIDATE_EMAIL: {
      const mandatory = isAPP && isProposerMissingData;

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory, disabled: false },
        value: emailData
      });

      if (mandatory && checkMandatory.hasError) {
        return checkMandatory;
      }

      return emailData !== ""
        ? validateEmail(emailData)
        : {
            hasError: false,
            message: {
              [LANGUAGE_TYPES.ENGLISH]: "",
              [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
            }
          };
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: {
          [LANGUAGE_TYPES.ENGLISH]: "",
          [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
        }
      };
    default:
      return state;
  }
};
/**
 * country
 * @description client form country field
 * @rule clientForm.countryOfResidence
 * @rule if countryOfResidence !== "", state will be lock to countryOfResidence
 *   state
 * @default ""
 * */
const country = (
  state = "R2",
  { type, newCountry, countryOfResidenceData }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newCountry;
    case ACTION_TYPES[CLIENT_FORM].COUNTRY_ON_CHANGE:
      return countryOfResidenceData || newCountry;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "R2";
    default:
      return state;
  }
};
/**
 * cityState
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.otherCityOfResidence
 * @reducer if cityOfResidence is not other option, auto fill from
 *   cityOfResidence state
 * @reducer if otherCityOfResidence is not "", auto fill from
 *   otherCityOfResidence state
 * @default ""
 * */
const cityState = (state = "", { type, newCityState }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newCityState;
    case ACTION_TYPES[CLIENT_FORM].CITY_STATE_ON_CHANGE:
      return newCityState;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * postal
 * @rule up to 12 characters
 * @default ""
 * */
const postal = (state = "", { type, newPostal }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newPostal;
    case ACTION_TYPES[CLIENT_FORM].POSTAL_ON_CHANGE:
      state = newPostal.substring(0, 10);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * blockHouse
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 12 characters
 * @default ""
 * */
const blockHouse = (state = "", { type, newBlockHouse }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newBlockHouse;
    case ACTION_TYPES[CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE:
      state = newBlockHouse.substring(0, 12);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * streetRoad
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 24 characters
 * @default ""
 * */
const streetRoad = (state = "", { type, newStreetRoad }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newStreetRoad;
    case ACTION_TYPES[CLIENT_FORM].STREET_ROAD_ON_CHANGE:
      state = newStreetRoad.substring(0, 24);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * unit
 * @rule up to 13 characters
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule this field have prefix "Unit"
 * @default ""
 * */
const unit = (state = "", { type, newUnit, prefix }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newUnit;
    case ACTION_TYPES[CLIENT_FORM].UNIT_ON_CHANGE:
      prefix = _.isEmpty(prefix) ? "" : `${prefix} `;
      return state === ""
        ? (prefix + newUnit).substring(0, 13)
        : newUnit.substring(0, 13);
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * buildingEstate
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 40 characters
 * @default ""
 * */
const buildingEstate = (state = "", { type, newBuildingEstate }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      return newBuildingEstate;
    case ACTION_TYPES[CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE:
      state = newBuildingEstate.substring(0, 40);
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * photo
 * @description client form dialog photo/avatar
 * @default ""
 * */
const photo = (state = {}, { type, newPhoto }) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT:
      state = newPhoto;
      return state;
    case ACTION_TYPES[CLIENT_FORM].PHOTO_ON_CHANGE:
      state = newPhoto;
      return state;
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {};
      return state;
    default:
      return state;
  }
};
/**
 * @description for some data will not be edit by clientForm, but contain in the
 * clientProfile data structure
 * @default {}
 * */
const profileBackUp = (
  state = {},
  {
    type,
    newHaveSignDoc,
    newBundle,
    newTrustedIndividuals,
    newCid,
    newDependants
  }
) => {
  switch (type) {
    case ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT: {
      const object = {
        cid: newCid,
        dependants: newDependants
      };

      if (newBundle.length > 0) {
        object.bundle = newBundle;
      }

      if (newTrustedIndividuals) {
        object.trustedIndividuals = newTrustedIndividuals;
      }

      if (newHaveSignDoc) {
        object.haveSignDoc = newHaveSignDoc;
      }

      return object;
    }
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {};
    default:
      return state;
  }
};

export default combineReducers({
  config,
  relationship,
  isRelationshipError,
  relationshipOther,
  isRelationshipOtherError,
  givenName,
  isGivenNameError,
  surname,
  otherName,
  hanYuPinYinName,
  name,
  isNameError,
  nameOrder,
  title,
  isTitleError,
  gender,
  isGenderError,
  birthday,
  nationality,
  singaporePRStatus,
  isSingaporePRStatusError,
  countryOfResidence,
  cityOfResidence,
  isCityOfResidenceError,
  otherCityOfResidence,
  isOtherCityOfResidenceError,
  IDDocumentType,
  isIDDocumentTypeError,
  IDDocumentTypeOther,
  isIDDocumentTypeOtherError,
  ID,
  isIDError,
  smokingStatus,
  maritalStatus,
  language,
  isLanguageError,
  languageOther,
  isLanguageOtherError,
  education,
  isEducationError,
  employStatus,
  isEmployStatusError,
  employStatusOther,
  isEmployStatusOtherError,
  industry,
  isIndustryError,
  occupation,
  isOccupationError,
  occupationOther,
  nameOfEmployBusinessSchool,
  countryOfEmployBusinessSchool,
  typeOfPass,
  isTypeOfPassError,
  typeOfPassOther,
  isTypeOfPassOtherError,
  passExpiryDate,
  income,
  prefixA,
  mobileNoA,
  prefixB,
  mobileNoB,
  email,
  isEmailError,
  country,
  cityState,
  postal,
  blockHouse,
  streetRoad,
  unit,
  buildingEstate,
  photo,
  isBirthdayError,
  isSmokingError,
  isNationalityError,
  isMaritalStatusError,
  isIncomeError,
  isMobileNoAError,
  isPrefixAError,
  profileBackUp
});
