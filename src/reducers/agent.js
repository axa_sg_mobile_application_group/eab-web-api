import ACTION_TYPES from "../constants/ACTION_TYPES";
import { AGENT } from "../constants/REDUCER_TYPES";

/**
 * profile
 * @default {}
 * */
export default (state = {}, { type, data }) => {
  switch (type) {
    case ACTION_TYPES[AGENT].UPDATE_AGENT_DATA:
      return data;
    default:
      return state;
  }
};
