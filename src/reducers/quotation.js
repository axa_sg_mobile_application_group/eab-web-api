import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { QUOTATION, PROPOSAL } from "../constants/REDUCER_TYPES";

/**
 * isQuickQuote
 * @description indicates whether quotation is quick quote or not
 * @default false
 * */
const isQuickQuote = (state = false, { type, isQuickQuoteData }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_IS_QUICK_QUOTE:
      return isQuickQuoteData;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * quickQuotes
 * @description quick quotes list
 * @default []
 * */
const quickQuotes = (state = [], { type, quickQuotesData }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_QUICK_QUOTES:
      return quickQuotesData;
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * quotation
 * @description quotation main state
 * @default {}
 * */
const quotation = (state = {}, { type, newQuotation }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_QUOTATION:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[QUOTATION].ALLOC_FUNDS_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_RIDERS_COMPLETED:
    case ACTION_TYPES[QUOTATION].QUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].RESET_QUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_INSURED_QUOTATION:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newQuotation);
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * planDetails
 * @description quotation plan details
 * @default {}
 * */
const planDetails = (state = {}, { type, newPlanDetails }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_PLAN_DETAILS:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      state = newPlanDetails;
      return state;
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * inputConfigs
 * @description quotation input config
 * @default {}
 * */
const inputConfigs = (state = {}, { type, newInputConfigs }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_INPUT_CONFIGS:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_RIDERS_COMPLETED:
    case ACTION_TYPES[QUOTATION].QUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].RESET_QUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_INSURED_QUOTATION:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newInputConfigs);
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * quotationErrors
 * @description store the quotation errors
 * @default {}
 * */
const quotationErrors = (state = {}, { type, newQuotationErrors }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_ERRORS:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[QUOTATION].ALLOC_FUNDS_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_RIDERS_COMPLETED:
    case ACTION_TYPES[QUOTATION].QUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_INSURED_QUOTATION:
    case ACTION_TYPES[QUOTATION].RESET_QUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newQuotationErrors);
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * quotWarnings
 * @description store the quotation warnings
 * @default []
 * */
const quotWarnings = (state = [], { type, newQuotationWarnings }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_WARNINGS:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[QUOTATION].UPDATE_RIDERS_COMPLETED:
    case ACTION_TYPES[QUOTATION].QUOTE_COMPLETED:
    case ACTION_TYPES[QUOTATION].RESET_QUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      state = newQuotationWarnings;
      return state;
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

/**
 * availableInsureds
 * @description quotation available insureds
 * @default []
 * */
const availableInsureds = (state = [], { type, newAvailableInsureds }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_INSUREDS:
    case ACTION_TYPES[QUOTATION].GET_QUOTATION_COMPLETED:
    case ACTION_TYPES[PROPOSAL].REQUOTE_COMPLETED:
    case ACTION_TYPES[PROPOSAL].CLONE_COMPLETED:
      state = newAvailableInsureds;
      return state;
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

/**
 * availableFunds
 * @description quotation available funds
 * @default []
 * */
const availableFunds = (state = [], { type, newAvailableFunds }) => {
  switch (type) {
    case ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS:
      return newAvailableFunds ? newAvailableFunds.slice(0) : [];
    case ACTION_TYPES[QUOTATION].CLEAN_QUOTATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

export default combineReducers({
  isQuickQuote,
  quickQuotes,
  quotation,
  planDetails,
  inputConfigs,
  quotationErrors,
  quotWarnings,
  availableInsureds,
  availableFunds
});
