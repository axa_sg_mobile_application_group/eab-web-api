import optionsMap from "../assets/cache/optionsMap";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { OPTIONS_MAP } from "../constants/REDUCER_TYPES";
import reducer from "./optionsMap";

describe("optionsMap reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual(optionsMap);
  });
});

it("should handle UPDATE_OPTIONS_MAP", () => {
  expect(reducer(
    {},
    {
      type: ACTION_TYPES[OPTIONS_MAP].UPDATE_OPTIONS_MAP,
      optionsMapData: {
        ccy: {
          type: "layout",
          currencies: [
            {
              compCode: "01",
              default: "ALL",
              options: [
                {
                  value: "ALL",
                  title: {
                    en: "All Currencies",
                    "zh-hant": "æ‰€æœ‰è²¨å¹£"
                  },
                  seq: 1
                },
                {
                  value: "SGD",
                  title: {
                    en: "SGD",
                    "zh-hant": "SGD"
                  },
                  exRate: 1,
                  symbol: "S$",
                  seq: 2
                },
                {
                  value: "USD",
                  title: {
                    en: "USD",
                    "zh-hant": "USD"
                  },
                  exRate: 0.718548,
                  symbol: "US$",
                  seq: 3
                },
                {
                  value: "GBP",
                  title: {
                    en: "GBP",
                    "zh-hant": "GBP"
                  },
                  exRate: 0.563038,
                  symbol: "Â£",
                  seq: 4
                },
                {
                  value: "EUR",
                  title: {
                    en: "EUR",
                    "zh-hant": "EUR"
                  },
                  exRate: 0.645927,
                  symbol: "â‚¬",
                  seq: 5
                },
                {
                  value: "AUD",
                  title: {
                    en: "AUD",
                    "zh-hant": "AUD"
                  },
                  exRate: 0.941408,
                  symbol: "A$",
                  seq: 6
                }
              ]
            }
          ]
        }
      }
    }
  )).toEqual({
    ccy: {
      type: "layout",
      currencies: [
        {
          compCode: "01",
          default: "ALL",
          options: [
            {
              value: "ALL",
              title: {
                en: "All Currencies",
                "zh-hant": "æ‰€æœ‰è²¨å¹£"
              },
              seq: 1
            },
            {
              value: "SGD",
              title: {
                en: "SGD",
                "zh-hant": "SGD"
              },
              exRate: 1,
              symbol: "S$",
              seq: 2
            },
            {
              value: "USD",
              title: {
                en: "USD",
                "zh-hant": "USD"
              },
              exRate: 0.718548,
              symbol: "US$",
              seq: 3
            },
            {
              value: "GBP",
              title: {
                en: "GBP",
                "zh-hant": "GBP"
              },
              exRate: 0.563038,
              symbol: "Â£",
              seq: 4
            },
            {
              value: "EUR",
              title: {
                en: "EUR",
                "zh-hant": "EUR"
              },
              exRate: 0.645927,
              symbol: "â‚¬",
              seq: 5
            },
            {
              value: "AUD",
              title: {
                en: "AUD",
                "zh-hant": "AUD"
              },
              exRate: 0.941408,
              symbol: "A$",
              seq: 6
            }
          ]
        }
      ]
    }
  });
});
