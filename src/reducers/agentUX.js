import ACTION_TYPES from "../constants/ACTION_TYPES";
import { AGENTUX } from "../constants/REDUCER_TYPES";

/**
 * profile
 * @default {}
 * */
export default (state = {}, { type, data }) => {
  switch (type) {
    case ACTION_TYPES[AGENTUX].UPDATE_UX_AGENT_DATA:
      return data;
    default:
      return state;
  }
};
