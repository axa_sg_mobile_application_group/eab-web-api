import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PRODUCTS, QUOTATION, CLIENT_FORM } from "../constants/REDUCER_TYPES";

/**
 * currency
 * @description affect product list filter
 * @default ALL
 * */
const currency = (state = "ALL", { type, currencyData }) => {
  switch (type) {
    case ACTION_TYPES[PRODUCTS].UPDATE_CURRENCY:
      return currencyData;
    case ACTION_TYPES[CLIENT_FORM].UPDATE_PRODUCT_LIST:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return "ALL";
    default:
      return state;
  }
};

/**
 * insuredCid
 * @description product list target client. affect product list filter
 * @default ""
 * */
const insuredCid = (state = null, { type, insuredCidData }) => {
  switch (type) {
    case ACTION_TYPES[PRODUCTS].UPDATE_INSURED_CID:
      return insuredCidData;
    case ACTION_TYPES[QUOTATION].UPDATE_PRODUCT_LIST:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return null;
    default:
      return state;
  }
};

/**
 * dependants
 * @description dependants list. Unlike client.profile.dependants, so can not
 *   reuse client.profile.dependants.
 * @default []
 * */
const dependants = (state = [], { type, dependantsData }) => {
  switch (type) {
    case ACTION_TYPES[PRODUCTS].UPDATE_DEPENDANTS:
      /* do not need to assign a new array, because the data is from server/core-api response */
      return dependantsData;
    case ACTION_TYPES[CLIENT_FORM].UNLINK_CLIENT:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * productList
 * @default []
 * */
const productList = (state = [], { type, productListData }) => {
  switch (type) {
    case ACTION_TYPES[PRODUCTS].UPDATE_PRODUCT_LIST:
      /* do not need to assign a new array, because the data is from server/core-api response */
      return productListData;
    case ACTION_TYPES[CLIENT_FORM].UNLINK_CLIENT:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * error message handle
 */
const errorMsg = (state = "", { type, errorMessage }) => {
  switch (type) {
    case ACTION_TYPES[PRODUCTS].UPDATE_PRODUCT_LIST:
      return errorMessage;
    case ACTION_TYPES[CLIENT_FORM].UNLINK_CLIENT:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

export default combineReducers({
  currency,
  insuredCid,
  dependants,
  productList,
  errorMsg
});
