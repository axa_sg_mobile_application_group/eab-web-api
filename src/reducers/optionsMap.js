import optionsMap from "../assets/cache/optionsMap";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { OPTIONS_MAP } from "../constants/REDUCER_TYPES";

export default (
  // TODO remove mock data when core is ready
  state = optionsMap,
  { type, optionsMapData }
) => {
  switch (type) {
    case ACTION_TYPES[OPTIONS_MAP].UPDATE_OPTIONS_MAP:
      return optionsMapData;
    default:
      return state;
  }
};
