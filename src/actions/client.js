import ACTION_TYPES from "../constants/ACTION_TYPES";
import { CLIENT } from "../constants/REDUCER_TYPES";

/**
 * getProfile
 * @description get profile
 * @param {function} dispatch - redux dispatch function
 * @param {string} cid - client ID
 * @param {function} callback - callback of this dispatch
 * */
export const getProfile = ({ dispatch, cid, callback }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT].GET_PROFILE,
    cid,
    callback
  });
};

/**
 * updateProfile
 * @description update Profile
 * @param {function} dispatch - redux dispatch function
 * @param {object} newProfile - new profile object
 * */
export const updateProfile = ({ dispatch, newProfile }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT].UPDATE_PROFILE,
    newProfile
  });
};
