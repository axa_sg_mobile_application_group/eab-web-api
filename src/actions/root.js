import ACTION_TYPES from "../constants/ACTION_TYPES";

/**
 * cleanClientData
 * @description update quotation
 * @reducer client
 * @reducer appSummary
 * @reducer quotation
 * @param {function} dispatch - redux dispatch function
 * */
export const cleanClientData = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
  });
};

export const save = () => ({});
