import ACTION_TYPES from "../constants/ACTION_TYPES";
import { APPLICATION } from "../constants/REDUCER_TYPES";

/**
 * actions
 */

/**
 * continueApplicationForm
 * @description continue application form
 * @reducer application
 * @param {function} dispatch - redux dispatch function
 * @param {string} [applicationId] - application id to continue
 * @param {string} [quotType] - "SHIELD" or "" (NON-SHIELD)
 * */
export const continueApplicationForm = ({
  dispatch,
  applicationId,
  quotType,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
    applicationId,
    quotType,
    callback
  });
};

/**
 * updateUiViewPlanDetails
 * @description update UI to show / hide plan details
 * @reducer application.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} isPlanDetailsView - show plan details flag
 * */
export const updateUiViewPlanDetails = ({ dispatch, isPlanDetailsView }) => {
  dispatch({
    type: ACTION_TYPES[APPLICATION].UPDATE_UI_VIEW_PLAN_DETAILS,
    isPlanDetailsView
  });
};
