import ACTION_TYPES from "../constants/ACTION_TYPES";
import { CLIENT_FORM } from "../constants/REDUCER_TYPES";

/**
 * cancelCreateClient
 * @description clean all client form data
 * @reducer clientForm
 * @param {function} dispatch - redux dispatch function
 * */
export const cancelCreateClient = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
  });
};
/**
 * givenNameOnChange
 * @description change givenName reducer state
 * @reducer clientForm.givenName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newGivenName - new reducer value
 * */
export const givenNameOnChange = ({ dispatch, newGivenName }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
    newGivenName
  });
};
/**
 * surnameOnChange
 * @description change surname reducer state
 * @reducer clientForm.surname
 * @param {function} dispatch - redux dispatch function
 * @param {string} newSurname - new reducer value
 * */
export const surnameOnChange = ({ dispatch, newSurname }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].SURNAME_ON_CHANGE,
    newSurname
  });
};
/**
 * otherNameOnChange
 * @description change otherName reducer state
 * @reducer clientForm.otherName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newOtherName - new reducer value
 * */
export const otherNameOnChange = ({ dispatch, newOtherName }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].OTHER_NAME_ON_CHANGE,
    newOtherName
  });
};
/**
 * hanYuPinYinNameOnChange
 * @description change hanYuPinYinName reducer state
 * @reducer clientForm.hanYuPinYinName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newHanYuPinYinName - new reducer value
 * */
export const hanYuPinYinNameOnChange = ({ dispatch, newHanYuPinYinName }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].HAN_YU_PIN_YIN_ON_CHANGE,
    newHanYuPinYinName
  });
};
/**
 * nameOnChange
 * @description change name reducer state
 * @reducer clientForm.name
 * @param {function} dispatch - redux dispatch function
 * @param {string} newName - new reducer value
 * */
export const nameOnChange = ({ dispatch, newName }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].NAME_ON_CHANGE,
    newName
  });
};
/**
 * IDOnChange
 * @description change ID reducer state
 * @reducer clientForm.ID
 * @param {function} dispatch - redux dispatch function
 * @param {string} newID - new reducer value
 * */
export const IDOnChange = ({ dispatch, newID }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].ID_ON_CHANGE,
    newID
  });
};
/**
 * validateID
 * @description ID validation
 * @reducer clientForm.ID
 * @reducer clientForm.IDType
 * @reducer clientForm.isIDError
 * @param {function} dispatch - redux dispatch function
 * @param {string} IDData - ID value
 * @param {string} IDTypeData - ID format
 * */
export const validateID = ({ dispatch, IDData, IDTypeData }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].VALIDATE_ID,
    IDData,
    IDTypeData
  });
};
/**
 * languageOnChange
 * @description change language reducer state
 * @reducer clientForm.language
 * @param {function} dispatch - redux dispatch function
 * @param {string} newLanguage - new reducer value
 * */
export const languageOnChange = ({ dispatch, newLanguage }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].LANGUAGE_ON_CHANGE,
    newLanguage
  });
};

/**
 * languageOnChange
 * @description change languageOther reducer state
 * @reducer clientForm.languageOther
 * @param {function} dispatch - redux dispatch function
 * @param {string} newLanguage - new reducer value
 * */
export const languageOtherOnChange = ({ dispatch, newLanguageOther }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].LANGUAGE_ON_CHANGE,
    newLanguageOther
  });
};

/**
 * nameOfEmployBusinessSchoolOnChange
 * @description change nameOfEmployBusinessSchool reducer state
 * @reducer clientForm.nameOfEmployBusinessSchool
 * @param {function} dispatch - redux dispatch function
 * @param {string} newNameOfEmployBusinessSchool - new reducer value
 * */
export const nameOfEmployBusinessSchoolOnChange = ({
  dispatch,
  newNameOfEmployBusinessSchool
}) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
    newNameOfEmployBusinessSchool
  });
};

/**
 * employStatusOtherOnChange
 * @description change employStatusOtherOnChange reducer state
 * @reducer clientForm.employStatusOtherOnChange
 * @param {function} dispatch - redux dispatch function
 * @param {string} employStatusOtherOnChange - new reducer value
 * * @param {string} employStatusOnChange - new reducer value
 * */
export const employStatusOtherOnChange = ({
  dispatch,
  newEmployStatus,
  newEmployStatusOther
}) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE,
    newEmployStatus,
    newEmployStatusOther
  });
};

/**
 * incomeOnChange
 * @description change incomeOnChange reducer state
 * @reducer clientForm.incomeOnChange
 * @param {function} dispatch - redux dispatch function
 * @param {string} newIncome - new reducer value
 * */
export const incomeOnChange = ({ dispatch, newIncome }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].INCOME_ON_CHANGE,
    newIncome
  });
};
/**
 * prefixAOnchange
 * @description change prefixA reducer state
 * @reducer clientForm.prefixA
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPrefixA - new reducer value
 * */
export const prefixAOnchange = ({ dispatch, newPrefixA }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].PREFIX_A_ON_CHANGE,
    newPrefixA
  });
};
/**
 * mobileNoAOnChange
 * @description change mobileNoA reducer state
 * @reducer clientForm.mobileNoA
 * @reducer clientForm.prefixA
 * @param {function} dispatch - redux dispatch function
 * @param {string} newMobileNoA - new reducer value
 * @param {string} prefix - mobile number prefix
 * */
export const mobileNoAOnChange = ({ dispatch, newMobileNoA, prefix }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
    newMobileNoA,
    prefix
  });
};
/**
 * prefixBOnChange
 * @description change prefixB reducer state
 * @reducer clientForm.prefixB
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPrefixB - new reducer value
 * */
export const prefixBOnChange = ({ dispatch, newPrefixB }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].PREFIX_B_ON_CHANGE,
    newPrefixB
  });
};
/**
 * mobileNoBOnChange
 * @description change mobileNoB reducer state
 * @reducer clientForm.mobileNoB
 * @reducer clientForm.prefixB
 * @param {function} dispatch - redux dispatch function
 * @param {string} newMobileNoB - new reducer value
 * @param {string} prefix - mobile number prefix
 * */
export const mobileNoBOnChange = ({ dispatch, newMobileNoB, prefix }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_B_ON_CHANGE,
    newMobileNoB,
    prefix
  });
};
/**
 * emailOnChange
 * @description change email reducer state
 * @reducer clientForm.email
 * @param {function} dispatch - redux dispatch function
 * @param {string} newEmail - new reducer value
 * */
export const emailOnChange = ({ dispatch, newEmail }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].EMAIL_ON_CHANGE,
    newEmail
  });
};
/**
 * validateEmail
 * @description email validation
 * @reducer clientForm.email
 * @reducer clientForm.isEmailError
 * @param {function} dispatch - redux dispatch function
 * @param {string} emailData - email value
 * */
export const validateEmail = ({ dispatch, emailData }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].VALIDATE_EMAIL,
    emailData
  });
};
/**
 * cityStateOnChange
 * @description change cityState reducer state
 * @reducer clientForm.cityState
 * @param {function} dispatch - redux dispatch function
 * @param {string} newCityState - new reducer value
 * */
export const cityStateOnChange = ({ dispatch, newCityState }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].CITY_STATE_ON_CHANGE,
    newCityState
  });
};
/**
 * postalOnChange
 * @description change postal reducer state
 * @reducer clientForm.postal
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPostal - new reducer value
 * */
export const postalOnChange = ({ dispatch, newPostal }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].POSTAL_ON_CHANGE,
    newPostal
  });
};
/**
 * blockHouseOnChange
 * @description change blockHouse reducer state
 * @reducer clientForm.blockHouse
 * @param {function} dispatch - redux dispatch function
 * @param {string} newBlockHouse - new reducer value
 * */
export const blockHouseOnChange = ({ dispatch, newBlockHouse }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
    newBlockHouse
  });
};
/**
 * streetRoadOnChange
 * @description change streetRoad reducer state
 * @reducer clientForm.streetRoad
 * @param {function} dispatch - redux dispatch function
 * @param {string} newStreetRoad - new reducer value
 * */
export const streetRoadOnChange = ({ dispatch, newStreetRoad }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].STREET_ROAD_ON_CHANGE,
    newStreetRoad
  });
};
/**
 * unitOnChange
 * @description change unit reducer state
 * @reducer clientForm.unit
 * @param {function} dispatch - redux dispatch function
 * @param {string} newUnit - new reducer value
 * */
export const unitOnChange = ({ dispatch, newUnit }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].UNIT_ON_CHANGE,
    newUnit
  });
};
/**
 * buildingEstateOnChange
 * @description change buildingEstate reducer state
 * @reducer clientForm.buildingEstate
 * @param {function} dispatch - redux dispatch function
 * @param {string} newBuildingEstate - new reducer value
 * */
export const buildingEstateOnChange = ({ dispatch, newBuildingEstate }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
    newBuildingEstate
  });
};
/**
 * photoOnChange
 * @description change photo reducer state
 * @reducer clientForm.photo
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPhoto - new reducer value
 * */
export const photoOnChange = ({ dispatch, newPhoto }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].PHOTO_ON_CHANGE,
    newPhoto
  });
};

/**
 * typeOfPassOtherOnChange
 * @description change typeOfPassOther reducer state
 * @reducer clientForm.typeOfPassOther
 * @param {function} dispatch - redux dispatch function
 * @param {string} newtypeOthPassOther - new reducer value
 * */
export const typeOthPassOtherOnChange = ({ dispatch, newtypeOfPassOther }) => {
  dispatch({
    type: ACTION_TYPES[CLIENT_FORM].PHOTO_ON_CHANGE,
    newtypeOfPassOther
  });
};
