export const pda = {
  agentCode: "020020",
  applicant: "single",
  compCode: "01",
  completed: true,
  dealerGroup: "AGENCY",
  dependants: "CP001001-00002,CP001001-00004",
  id: "FN001006-00001-PDA",
  isCompleted: false,
  lastStepIndex: -1,
  lastUpd: 1528275781599,
  notes: null,
  ownerConsentMethod: "phone,text",
  trustedIndividual: null,
  type: "pda",
  undefined: null
};

export const pda1 = {
  agentCode: "020020",
  applicant: "single",
  compCode: "01",
  completed: true,
  dealerGroup: "AGENCY",
  dependants: "",
  id: "FN001006-00001-PDA",
  isCompleted: true,
  lastStepIndex: -1,
  lastUpd: 1528275781599,
  notes: null,
  ownerConsentMethod: "phone,text",
  trustedIndividual: null,
  type: "pda",
  undefined: null
};

export const na = {
  agentCode: "085216",
  aspects:
    "fiProtection,ciProtection,diProtection,paProtection,other,psGoals,rPlanning,hcProtection",
  ciProtection: {
    isValid: false,
    owner: {
      annualLivingExp: 12,
      assets: null,
      ciProt: 0,
      cid: "CP001001-00001",
      header: {
        en: "CRITICAL ILLNESS PROTECTION"
      },
      iarRate2: 0.5,
      init: true,
      isActive: true,
      lumpSum: 11.999999999999744,
      mtCost: "",
      pmt: 1,
      requireYrIncome: "1",
      totCoverage: "11.999999999999744",
      totShortfall: -11.999999999999744,
      undefined: null
    }
  },
  ckaSection: {
    isValid: false
  },
  compCode: "01",
  completed: true,
  dealerGroup: "SINGPOST",
  diProtection: {
    isValid: false,
    owner: {
      annualPmt: 12,
      cid: "CP001001-00001",
      disabilityBenefit: 0,
      header: {
        en: "Disability Benefit"
      },
      init: true,
      isActive: true,
      othRegIncome: "",
      pmt: 1,
      requireYrIncome: "1",
      totShortfall: -12,
      undefined: null
    }
  },
  ePlanning: {},
  fiProtection: {
    isValid: true,
    owner: {
      annualRepIncome: 11,
      assets: [
        {
          calAsset: 11,
          key: "savAcc",
          usedAsset: 11
        }
      ],
      cid: "CP001001-00001",
      existLifeIns: 0,
      finExpenses: "",
      header: {
        en: "INCOME PROTECTION"
      },
      iarRate2: 0.5,
      init: false,
      isActive: true,
      lumpSum: 11.999999999999744,
      othFundNeeds: "",
      othFundNeedsName: "",
      pmt: 1,
      requireYrIncome: "1",
      totLiabilities: 0,
      totRequired: 11.999999999999744,
      totShortfall: -11.999999999999744,
      undefined: null
    },
    spouse: {
      annualRepIncome: 12,
      assets: [
        {
          calAsset: 22,
          key: "savAcc",
          usedAsset: 22
        }
      ],
      cid: "CP001001-00002",
      existLifeIns: 0,
      finExpenses: "",
      header: {
        en: "INCOME PROTECTION"
      },
      iarRate2: 0.5,
      init: false,
      isActive: true,
      lumpSum: 22.999999999999744,
      othFundNeeds: "",
      othFundNeedsName: "",
      pmt: 2,
      requireYrIncome: "2",
      totLiabilities: 0,
      totRequired: 22.999999999999744,
      totShortfall: -22.999999999999744,
      undefined: null
    },
    dependants: [
      {
        cid: "CP001001-00003",
        annualRepIncome: 13,
        assets: [
          {
            calAsset: 33,
            key: "savAcc",
            usedAsset: 33
          }
        ],
        existLifeIns: 0,
        finExpenses: "",
        header: {
          en: "INCOME PROTECTION"
        },
        iarRate2: 0.5,
        init: false,
        isActive: true,
        lumpSum: 33.999999999999744,
        othFundNeeds: "",
        othFundNeedsName: "",
        pmt: 3,
        requireYrIncome: "3",
        totLiabilities: 0,
        totRequired: 33.999999999999744,
        totShortfall: -33.999999999999744,
        undefined: null
      }
    ]
  },
  haveFnaCompleted: true,
  hcProtection: {
    isValid: false,
    owner: {
      cid: "CP001001-00001",
      header: {
        en: "Hospitalisation cost protection"
      },
      init: true,
      isActive: true,
      provideHeadstart: "PUBLIC",
      typeofWard: "A"
    }
  },
  iarRate: "0.50",
  iarRateNote: null,
  id: "FN001048-00001-NA",
  isCompleted: false,
  lastStepIndex: 2,
  lastUpd: 1533126092857,
  needsImgDesc: null,
  other: {
    isValid: false,
    owner: {
      cid: "CP001001-00001",
      goalNo: 1,
      goals: [
        {
          cid: "CP001001-00001",
          extInsuDisplay: "",
          extInsuSelField: "",
          goalName: "1",
          needsValue: 1,
          totShortfall: -1,
          typeofNeeds: "SAVINGS"
        }
      ],
      header: {
        en: "OTHER NEEDS (E.G. MORTGAGE, PREGNANCY, SAVINGS)"
      },
      init: true,
      isActive: true,
      totShortfall: -1,
      undefined: null
    }
  },
  paProtection: {
    isValid: false,
    owner: {
      cid: "CP001001-00001",
      extInsurance: 0,
      header: {
        en: "Personal accident protection"
      },
      init: true,
      isActive: true,
      pmt: 50000,
      totShortfall: -50000,
      undefined: null
    }
  },
  pcHeadstart: {},
  productType: {
    isValid: true,
    lastProdType: ",nonParticipatingPlan,accidentHealthPlans",
    prodType: ",nonParticipatingPlan,accidentHealthPlans"
  },
  psGoals: {
    isValid: false,
    owner: {
      cid: "CP001001-00001",
      goalNo: 1,
      goals: [
        {
          assets: null,
          cid: "CP001001-00001",
          compFv: 1,
          goalName: "1",
          projMaturity: "",
          timeHorizon: "1",
          totShortfall: -1
        }
      ],
      header: {
        en: "PLANNING FOR SPECIFIC GOALS"
      },
      init: true,
      isActive: true,
      totShortfall: -1,
      undefined: null
    }
  },
  rPlanning: {
    isValid: false,
    owner: {
      annualInReqRetirement: 12,
      assets: null,
      avgInflatRate: 2.65,
      cid: "CP001001-00001",
      compFv: 49.26797883736504,
      compPv: 713.8293478624764,
      firstYrPMT: 49.26797883736504,
      header: {
        en: "Retirement Planning"
      },
      iarRate2: 0.5,
      inReqRetirement: 1,
      init: true,
      isActive: true,
      maturityValue: 0,
      othRegIncome: "",
      retireAge: 65,
      retireDuration: 15,
      timeHorizon: 54,
      totShortfall: -713.8293478624764,
      undefined: null
    }
  },
  raSection: {
    isValid: false
  },
  selectedMenuId: "fiProtection",
  sfAspects: [
    {
      aid: "fiProtection",
      cid: "CP001001-00001",
      index: 1,
      title: "Income protection"
    },
    {
      aid: "ciProtection",
      cid: "CP001001-00001",
      index: 1,
      title: "Critical illness protection"
    },
    {
      aid: "diProtection",
      cid: "CP001001-00001",
      index: 1,
      title: "Disability benefit"
    },
    {
      aid: "paProtection",
      cid: "CP001001-00001",
      index: 1,
      title: "Personal accident protection"
    },
    {
      aid: "hcProtection",
      cid: "CP001001-00001",
      index: null,
      title: "Hospitalisation cost protection"
    },
    {
      aid: "rPlanning",
      cid: "CP001001-00001",
      index: 1,
      title: "Retirement planning"
    },
    {
      aid: "psGoals",
      cid: "CP001001-00001",
      index: 1,
      title: "1"
    },
    {
      aid: "other",
      cid: "CP001001-00001",
      index: 1,
      title: "1",
      type: "SAVINGS"
    }
  ],
  sfFailNotes: null,
  spAspects: [],
  spNotes: null,
  type: "na",
  undefined: null
};

export const fe = {
  agentCode: "046046",
  compCode: "01",
  completed: true,
  dealerGroup: "AGENCY",
  id: "FN001064-00001-FE",
  isCompleted: true,
  lastUpd: 1533542343207,
  owner: {
    aBonus: 0,
    aDisposeIncome: 119999.04,
    aInsPrem: 1,
    aRegPremBudget: 1,
    aRegPremBudgetSrc: "1",
    additionComment: "",
    assets: 1,
    caInsPrem: 0,
    car: 5,
    ciPort: 0,
    cid: "CP001001-00001",
    confirmBudget: "N",
    cpfMs: 8,
    cpfMsBudget: 0,
    cpfOa: 6,
    cpfOaBudget: 0,
    cpfSa: 7,
    cpfSaBudget: 0,
    csInsPrem: 0,
    disIncomeProt: 0,
    ednowSavPol: 0,
    eduFund: 0,
    eduLoan: 12,
    eiPorf: 1,
    fixDeposit: 2,
    forceIncome: "N",
    forceIncomeReason: "",
    fpInsPrem: 0,
    hospNSurg: "",
    householdExpenses: 0,
    init: false,
    insurancePrem: 0.08,
    invLinkPol: 0,
    invest: 3,
    isActive: false,
    lessMCPF: 0,
    liabilities: 0,
    lifeInsProt: 1,
    mDisposeIncome: 10000,
    mIncome: 10000,
    mdInsPrem: 0,
    mortLoan: 10,
    motLoan: 11,
    netMIcncome: 10000,
    netWorth: 1,
    noALReason: "",
    noCFReason: "",
    noEIReason: "",
    oAInsPrem: 0,
    otherAsset: 9,
    otherAssetTitle: "testingOtherAsset",
    otherExpense: 0,
    otherExpenseTitle: "",
    otherLiabilites: 13,
    otherLiabilitesTitle: "testingLiabilitesAsset",
    pAInsPrem: 1,
    personAcc: 0,
    personExpenses: 0,
    property: 4,
    regSav: 0,
    retirePol: 0,
    sAInsPrem: 0,
    sInsPrem: 0,
    savAcc: 1,
    singPrem: 0,
    srs: 9,
    srsBudget: 0,
    totAExpense: 0.96,
    totAIncome: 120000,
    totMExpense: 0.08
  },
  spouse: {
    aBonus: 0,
    aDisposeIncome: 119999.04,
    aInsPrem: 1,
    aRegPremBudget: 1,
    aRegPremBudgetSrc: "1",
    additionComment: "",
    assets: 1,
    caInsPrem: 0,
    car: 0,
    ciPort: 0,
    cid: "CP001001-00002",
    confirmBudget: "N",
    cpfMs: 0,
    cpfMsBudget: 0,
    cpfOa: 0,
    cpfOaBudget: 0,
    cpfSa: 0,
    cpfSaBudget: 0,
    csInsPrem: 0,
    disIncomeProt: 0,
    ednowSavPol: 0,
    eduFund: 0,
    eduLoan: 0,
    eiPorf: 1,
    fixDeposit: 0,
    forceIncome: "N",
    forceIncomeReason: "",
    fpInsPrem: 0,
    hospNSurg: "",
    householdExpenses: 0,
    init: false,
    insurancePrem: 0.08,
    invLinkPol: 0,
    invest: 0,
    isActive: false,
    lessMCPF: 0,
    liabilities: 0,
    lifeInsProt: 1,
    mDisposeIncome: 10000,
    mIncome: 10000,
    mdInsPrem: 0,
    mortLoan: 0,
    motLoan: 0,
    netMIcncome: 10000,
    netWorth: 1,
    noALReason: "",
    noCFReason: "",
    noEIReason: "",
    oAInsPrem: 0,
    otherAsset: 0,
    otherAssetTitle: "",
    otherExpense: 0,
    otherExpenseTitle: "",
    otherLiabilites: 0,
    otherLiabilitesTitle: "",
    pAInsPrem: 1,
    personAcc: 0,
    personExpenses: 0,
    property: 0,
    regSav: 0,
    retirePol: 0,
    sAInsPrem: 0,
    sInsPrem: 0,
    savAcc: 1,
    singPrem: 0,
    srs: 0,
    srsBudget: 0,
    totAExpense: 0.96,
    totAIncome: 120000,
    totMExpense: 0.08
  },
  dependants: [
    {
      aInsPrem: 1,
      caInsPrem: 0,
      ciPort: 0,
      cid: "CP001001-00003",
      csInsPrem: 0,
      disIncomeProt: 0,
      ednowSavPol: 0,
      eduFund: 0,
      eiPorf: 1,
      fpInsPrem: 0,
      hospNSurg: "",
      init: false,
      invLinkPol: 0,
      isActive: false,
      lifeInsProt: 1,
      mdInsPrem: 0,
      oAInsPrem: 0,
      pAInsPrem: 1,
      personAcc: 0,
      retirePol: 0,
      sAInsPrem: 0,
      sInsPrem: 0
    }
  ]
};
