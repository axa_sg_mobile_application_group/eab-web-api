export const FAKE_APPLICATIONS_LIST = {
  "FN001003-00276": [
    {
      baseProductCode: "SAV",
      baseProductName: {
        en: "SavvySaver",
        "zh-Hant": "SavvySaver"
      },
      bundleId: "FN001003-00276",
      ccy: "SGD",
      clientChoice: {
        isClientChoiceSelected: false,
        recommendation: {
          benefit:
            "SavvySaver is a regular premium, participating anticipated endowment policy. It provides you Death and Terminal Illness coverage, yearly Guaranteed Cash Payouts from the end of the second Policy year and a Guaranteed Maturity benefit plus bonuses (if any), payable to you in a lump sum upon Policy maturity. SavvySaver terminates upon full payout of either your TI benefit, Death benefit, Policy maturity amount or any other reasons as stated in Product Summary.\n\nFree-Look Period: Upon receiving your policy, you have 14 days to review if it suits your needs. If you wish to cancel it, give us a written request and we will refund the premiums paid for this Policy less any medical expenses incurred in processing your application. If Policy is received by post, your 14-day Fee Look Period starts 7 days from the date of posting.\n\nPremiumEraser Total Rider will waive future Premiums upon Total and Permanent Disability before the Policy Anniversary nearest to your 70th year birthday or you are diagnosed with one of the 36 Critical Illness, while this Additional Benefit is in force.",
          limitation:
            "The Sum Assured is a notional value, used solely for the calculation of Guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death or Terminal Illness. For the actual payout in the event of death and terminal illness please refer to the product summary.\nGuaranteed Cash Payouts could  be deposited with us at a non-guaranteed interest rate. You can withdraw partial OR all of the re-deposited amounts with earned interest, subject to the minimum withdrawal amount equal to 1 Guaranteed Cash Payout payment . The bonus rates used for the benefit illustration are non-guaranteed, the actual benefit payable may vary according to the future performance of the Participating Fund. There are certain conditions under which no benefits will be payable under this Policy. These are stated as exclusions in this Policy. Refer to your Product Summary for the exclusions, terms & conditions for payable Benefits.\nBuying a life insurance is a long-term commitment. An early termination of this Policy usually involves high costs and the surrender value may be less than the total Premiums paid.\n\nPremiumEraser Total Rider terminates upon its coverage expiry date of this additional benefit OR when Policy is assigned OR when you no longer suffers from TPD  or any other event which results in the termination of this Additional Benefit as set out in the Policy.\nThe premium rates for this Additional Benefit are not guaranteed. The rates may be adjusted based on future experience. Premiums are payable throughout the term of this Additional Benefit.\nPlease refer to product summary for terms and conditions and exclusions.",
          reason: "asdfasdf",
          rop: {
            choiceQ1Sub1: "",
            choiceQ1Sub2: "",
            choiceQ1Sub3: "",
            existCi: 0,
            existLife: 3333,
            existPaAdb: 0,
            existTotalPrem: 44,
            existTpd: 0,
            replaceCi: 0,
            replaceLife: 0,
            replacePaAdb: 0,
            replaceTotalPrem: 0,
            replaceTpd: 0
          },
          rop_shield: {
            ropBlock: {
              iCidRopAnswerMap: {},
              ropQ1sub3: "",
              ropQ2: "",
              ropQ3: "",
              shieldRopAnswer_0: "",
              shieldRopAnswer_1: "",
              shieldRopAnswer_2: "",
              shieldRopAnswer_3: "",
              shieldRopAnswer_4: "",
              shieldRopAnswer_5: ""
            }
          }
        }
      },
      fnaAns: "",
      iCid: "CP001003-00006",
      iCids: [],
      iName: "Alex 4",
      id: "QU001003-01349",
      insureds: {},
      lastUpdateDate: "2018-05-14T07:15:16.964Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "premium",
          covCode: "SAV",
          covName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          halfYearPrem: 583.84,
          monthPrem: 100.17,
          planCode: "15SS",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 108,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 1144.8,
          productLine: "WLE",
          quarterPrem: 297.64,
          saViewInd: "Y",
          sumInsured: 10600,
          version: 1,
          yearPrem: 1144.8
        },
        {
          covCode: "PET",
          covName: {
            en: "PremiumEraser Total",
            "zh-Hant": "PremiumEraser Total"
          },
          halfYearPrem: 10.91,
          monthPrem: 1.87,
          planCode: "15WOP",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 1.87,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 21.4,
          productLine: "WLE",
          quarterPrem: 5.56,
          saViewInd: "N",
          sumInsured: 1144.8,
          version: 1,
          yearPrem: 21.4
        }
      ],
      policyOptions: {},
      policyOptionsDesc: {},
      productLine: "WLE",
      quotType: "",
      statusChangeDate: "2018-05-14T07:15:16.964Z",
      totPremium: 1166.2,
      type: "quotation",
      appStatus: "INVALIDATED",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "BAA",
      baseProductName: {
        en: "AXA Band Aid",
        "zh-Hant": "AXA Band Aid"
      },
      bundleId: "FN001003-00276",
      ccy: "SGD",
      clientChoice: {
        isClientChoiceSelected: true,
        recommendation: {
          benefit:
            "AXA Band Aid is a comprehensive Accident & Health policy that provides cover up to age 75. It includes 5 core benefits which are payable when death OR injury occurs within 12 months from date of Accident. Please refer to Product Summary for the benefits and terms and conditions.\n\nThe Permanent Disablement Schedule is split into 4 categories with different Scope of Coverage and payout amount based on a percentage of your Sum Assured. \n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.",
          limitation:
            "There are certain conditions under which no benefits will be payable under the Policy:\n1) Any pre-existing condition 2) Acts of war, terrorism, riot, strike or civil commotion, if the Life Assured collaborated, participated or provoked such act directly or indirectly. 3) Engaging in illegal or criminal acts 4) Hazardous pursuits including but not limited to sky diving, diving, motorcar and motorcycle racing, deep sea fishing, deep sea diving, scuba diving, skiing, mountaineering and parachuting. Please refer to the policy contract for the full list of exclusions. \nAXA Band Aid premiums rates are based on your Occupation Class and rates may be adjusted based on future experience. Policy is not guaranteed renewable. We must receive in writing, immediately or as soon as possible, information on any change in the Life Assuredâ€™s occupation, country of residence, or pursuits. We have the right to vary the terms and conditions of this Policy and/or adjust the Premiums payable. If there has been a failure to notify us as required in this clause, We may, in the event of a claim being made under this Policy, repudiate the claim or adjust the Benefits payable at Our discretion.\n\nThe maximum payable Accidental-related benefits is S$3.5 million inclusive of all other policies issued by AXA and other insurance companies, in respect of the same Life Assured. Refer to your Product Summary for definition of Accident, Schedule of Permanent Disablement Benefits with scope of coverage, the exclusions, terms & conditions for Benefits to be payable.",
          reason: "aa",
          rop: {
            choiceQ1: "N",
            choiceQ1Sub1: "",
            choiceQ1Sub2: "",
            choiceQ1Sub3: "",
            existCi: 0,
            existLife: 1111,
            existPaAdb: 0,
            existTotalPrem: 222,
            existTpd: 0,
            replaceCi: 0,
            replaceLife: 0,
            replacePaAdb: 0,
            replaceTotalPrem: 0,
            replaceTpd: 0
          },
          rop_shield: {
            ropBlock: {
              iCidRopAnswerMap: {},
              ropQ1sub3: "",
              ropQ2: "",
              ropQ3: "",
              shieldRopAnswer_0: "",
              shieldRopAnswer_1: "",
              shieldRopAnswer_2: "",
              shieldRopAnswer_3: "",
              shieldRopAnswer_4: "",
              shieldRopAnswer_5: ""
            }
          }
        }
      },
      fnaAns: "",
      iCid: "CP001003-00008",
      iCids: [],
      iName: "Alex 4 Son",
      id: "QU001003-01359",
      insureds: {},
      lastUpdateDate: "2018-06-05T09:43:47.459Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "sumAssured",
          covClass: "4",
          covCode: "BAA",
          covName: {
            en: "AXA Band Aid",
            "zh-Hant": "AXA Band Aid"
          },
          halfYearPrem: 35.95,
          monthPrem: 6.16,
          planCode: "PAR4",
          polTermDesc: "To Age 75",
          policyTerm: "999",
          premRate: 1.23375,
          premTerm: "999",
          premTermDesc: "To Age 75",
          premium: 75.44,
          productLine: "AP",
          quarterPrem: 18.33,
          saViewInd: "Y",
          sumInsured: 50000,
          tax: {
            halfYearTax: 2.52,
            monthTax: 0.43,
            quarterTax: 1.28,
            yearTax: 4.94
          },
          version: 1,
          yearPrem: 70.5
        }
      ],
      policyOptions: {},
      policyOptionsDesc: {},
      productLine: "AP",
      quotType: "",
      statusChangeDate: "2018-06-05T09:43:47.459Z",
      totPremium: 75.44,
      type: "quotation",
      appStatus: "INVALIDATED",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "ASIM",
      baseProductName: {
        en: "AXA Shield Plan",
        "zh-Hant": "AXA Shield"
      },
      bundleId: "FN001003-00276",
      fnaAns: "",
      iCids: ["CP001003-00006"],
      id: "QU001003-01365",
      insureds: {
        "CP001003-00006": {
          agent: {
            agentCode: "008008",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "alex.tang@eabsystems.com",
            mobile: "12345678",
            name: "Alex Tang",
            tel: "0"
          },
          agentCode: "008008",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_1",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001003-00006",
          iDob: "1987-11-24",
          iEmail: "alex.tang@eabsystems.com",
          iFirstName: "Alex 4",
          iFullName: "Alex 4",
          iGender: "M",
          iLastName: "",
          iOccupation: "O921",
          iOccupationClass: "4",
          iResidence: "R2",
          iSmoke: "Y",
          pAge: 31,
          pCid: "CP001003-00006",
          pDob: "1987-11-24",
          pEmail: "alex.tang@eabsystems.com",
          pFirstName: "Alex 4",
          pFullName: "Alex 4",
          pGender: "M",
          pLastName: "",
          pOccupation: "O921",
          pOccupationClass: "4",
          pResidence: "R2",
          pSmoke: "Y",
          paymentMode: "A",
          plans: [
            {
              covClass: "A",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSA",
              premium: 593,
              productLine: "HP",
              tax: {
                yearTax: 38.8
              },
              version: 1
            },
            {
              covClass: "A",
              covCode: "ASP",
              covName: {
                en: "AXA Basic Care (Plan A)",
                "zh-Hant": "AXA Basic Care (Plan A)"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASPA",
              premium: 28.2,
              productLine: "HP",
              tax: {
                monthTax: 1.84,
                yearTax: 22.08
              },
              version: 1,
              yearPrem: 338.4
            },
            {
              covClass: "A",
              covCode: "ASG",
              covName: {
                en: "AXA General Care (Plan A)",
                "zh-Hant": "AXA General Care (Plan A)"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASGC",
              premium: 11.9,
              productLine: "HP",
              tax: {
                monthTax: 0.78,
                yearTax: 9.36
              },
              version: 1,
              yearPrem: 142.8
            },
            {
              covClass: "D",
              covCode: "ASH",
              covName: {
                en: "AXA Home Care",
                "zh-Hant": "AXA Home Care"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASHCD",
              premium: 7.7,
              productLine: "HP",
              tax: {
                monthTax: 0.5,
                yearTax: 6
              },
              version: 1,
              yearPrem: 92.4
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 283,
            cashPortion: 0,
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 283,
            cashPortion: "-",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          sameAs: "Y",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 47.8,
          totRiderYearPrem: 573.6,
          totYearCashPortion: 573.6,
          totYearPrem: 0,
          type: "quotation"
        }
      },
      lastUpdateDate: "2018-06-15T06:10:26.983Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "",
      productLine: "HP",
      quotType: "SHIELD",
      statusChangeDate: "2018-06-15T06:10:26.983Z",
      totCashPortion: 47.8,
      totMedisave: 593,
      totPremium: 640.8,
      type: "quotation",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "SAV",
      baseProductName: {
        en: "SavvySaver",
        "zh-Hant": "SavvySaver"
      },
      bundleId: "FN001003-00276",
      ccy: "SGD",
      fnaAns: "",
      iCid: "CP001003-00006",
      iCids: [],
      iName: "Alex 4",
      id: "QU001003-01366",
      insureds: {},
      lastUpdateDate: "2018-06-15T06:14:27.876Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "S",
      plans: [
        {
          calcBy: "premium",
          covCode: "SAV",
          covName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          halfYearPrem: 1145.66,
          monthPrem: 196.56,
          planCode: "15SS",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 108,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 1145.66,
          productLine: "WLE",
          quarterPrem: 584.06,
          saViewInd: "Y",
          sumInsured: 20800,
          version: 1,
          yearPrem: 2246.4
        },
        {
          covCode: "PET",
          covName: {
            en: "PremiumEraser Total",
            "zh-Hant": "PremiumEraser Total"
          },
          halfYearPrem: 24.51,
          monthPrem: 4.2,
          planCode: "15WOP",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 2.14,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 24.51,
          productLine: "WLE",
          quarterPrem: 12.49,
          saViewInd: "N",
          sumInsured: 2246.4,
          version: 1,
          yearPrem: 48.07
        }
      ],
      policyOptions: {},
      policyOptionsDesc: {},
      productLine: "WLE",
      quotType: "",
      statusChangeDate: "2018-06-15T06:14:27.876Z",
      totPremium: 1170.17,
      type: "quotation",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "BAA",
      baseProductName: {
        en: "AXA Band Aid",
        "zh-Hant": "AXA Band Aid"
      },
      bundleId: "FN001003-00276",
      ccy: "SGD",
      fnaAns: "",
      iCid: "CP001003-00008",
      iCids: [],
      iName: "Alex 4 Son",
      id: "QU001003-01367",
      insureds: {},
      lastUpdateDate: "2018-06-15T06:14:52.217Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "sumAssured",
          covClass: "4",
          covCode: "BAA",
          covName: {
            en: "AXA Band Aid",
            "zh-Hant": "AXA Band Aid"
          },
          halfYearPrem: 35.95,
          monthPrem: 6.16,
          planCode: "PAR4",
          polTermDesc: "To Age 75",
          policyTerm: "999",
          premRate: 1.23375,
          premTerm: "999",
          premTermDesc: "To Age 75",
          premium: 75.44,
          productLine: "AP",
          quarterPrem: 18.33,
          saViewInd: "Y",
          sumInsured: 50000,
          tax: {
            halfYearTax: 2.52,
            monthTax: 0.43,
            quarterTax: 1.28,
            yearTax: 4.94
          },
          version: 1,
          yearPrem: 70.5
        }
      ],
      policyOptions: {},
      policyOptionsDesc: {},
      productLine: "AP",
      quotType: "",
      statusChangeDate: "2018-06-15T06:14:52.217Z",
      totPremium: 75.44,
      type: "quotation",
      paymentMethod: "RP"
    }
  ],
  "FN001003-00255": [
    {
      baseProductCode: "RHP",
      baseProductName: {
        en: "AXA Retire Happy Plus",
        "zh-Hant": "AXA RETIRE HAPPY PLUS"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      fnaAns: "",
      iCid: "CP001003-00006",
      iCids: [],
      iName: "Alex 4",
      id: "QU001003-01056",
      insureds: {},
      lastUpdateDate: "2018-03-29T06:59:11.566Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "premium",
          covCode: "RHP",
          covName: {
            en: "AXA Retire Happy Plus",
            "zh-Hant": "AXA RETIRE HAPPY PLUS"
          },
          halfYearPrem: 1251.67,
          monthPrem: 214.74,
          planCode: "05SL50A",
          polTermDesc: "35 Years",
          policyTerm: 35,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 2500,
          productLine: "WLE",
          quarterPrem: 638.1,
          retirementIncome: 1161.45,
          saViewInd: "N",
          sumAssured: 26700,
          version: 1,
          yearPrem: 2454.26
        },
        {
          covCode: "TPD",
          covName: {
            en: "TPD Multiplier Rider",
            "zh-Hant": "Total and Permanent Disability Benefit"
          },
          halfYearPrem: 25.6,
          monthPrem: 4.39,
          planCode: "05TL50A",
          polTermDesc: "20 Years",
          policyTerm: 20,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 50.2,
          productLine: "WLE",
          quarterPrem: 13.05,
          sumAssured: 26700,
          version: 1,
          yearPrem: 50.2
        }
      ],
      policyOptions: {
        paymentMethod: "payMethodCash",
        payoutTerm: "15",
        payoutType: "Level",
        retirementAge: "50"
      },
      policyOptionsDesc: {
        paymentMethod: {
          en: "Cash"
        },
        payoutTerm: {
          en: "15 Years"
        },
        payoutType: {
          en: "Level"
        },
        retirementAge: {
          en: "Age 50"
        }
      },
      productLine: "WLE",
      quotType: "",
      statusChangeDate: "2018-03-29T06:59:11.566Z",
      totPremium: 2550.2,
      type: "quotation",
      appStatus: "INVALIDATED",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "IND",
      baseProductName: {
        en: "INSPIREâ„¢ Duo",
        "zh-Hant": "INSPIREâ„¢ Duo"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      fnaAns: "",
      iCid: "CP001003-00006",
      iCids: [],
      iName: "Alex 4",
      id: "QU001003-01169",
      insureds: {},
      lastUpdateDate: "2018-04-17T04:28:17.067Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "L",
      plans: [
        {
          calcBy: "premium",
          covCode: "IND",
          covName: {
            en: "INSPIREâ„¢ Duo",
            "zh-Hant": "INSPIREâ„¢ Duo"
          },
          paymentMode: "L",
          planCode: "INC60A1",
          polTermDesc: "To Age 99",
          policyTerm: "999",
          premRate: 1.01,
          premTerm: "1",
          premTermDesc: "Single Premium",
          premium: 5000,
          productLine: "IL",
          sumInsured: 5050,
          version: 1,
          yearPrem: 5000
        }
      ],
      policyOptions: {
        paymentMethod: "cash",
        rspAmount: null,
        rspPayFreq: null,
        rspSalesCharges: "5%",
        singlePremSalesCharge: "5"
      },
      policyOptionsDesc: {
        paymentMethod: {
          en: "Cash"
        },
        rspAmount: "-",
        rspPayFreq: "-",
        rspSalesCharges: "5%",
        singlePremSalesCharge: {
          en: "5%"
        }
      },
      productLine: "IL",
      quotType: "",
      statusChangeDate: "2018-04-17T04:28:17.067Z",
      totPremium: 5000,
      type: "quotation",
      appStatus: "INVALIDATED",
      paymentMethod: "Cash"
    },
    {
      baseProductCode: "RHP",
      baseProductName: {
        en: "AXA Retire Happy Plus",
        "zh-Hant": "AXA RETIRE HAPPY PLUS"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      clientChoice: {
        isClientChoiceSelected: true,
        recommendation: {
          benefit:
            "Retire Happy Plus (Level) is a participating endowment policy. It will pay out guaranteed and non-guaranteed annual cash benefits. This annual cash benefit is payable for 15 years starting from the policy anniversary nearest to the Selected Retirement Age. Upon Death or Terminal Illness, the amount payable is (a) higher of : (i) 101% of the Total Premiums paid (including the premiums of the TPD Multiplier rider, but excluding the premiums of optional supplementary benefit(s), if any) less any Retirement Income payouts already paid; or (ii) Gauranteed Cash Surrender Values; plus (b) Deposited guaranteed Retirement Income with non-guaranteed interest (if any); less outstanding indebtness. If you suffer from Total & Permanent Disability (TPD) before the Selected Retirement Age (SRA), an additional lump sum of 5 times the  yearly Guaranteed Retirement Income will be paid.\n\nThe free-Look Period allows you to review your policy. If you decide that this policy does not suit your needs, you may return it to us within 14 days from the date you received it and we will refund the premiums paid for this Policy. If Policy is received by post,  it is considered to have been received by you in 7 days from the date of posting. If any claim(s) was made during the free look period, the free look period will no longer be applicable.",
          limitation:
            "Retire Happy Plus (Level) provides limited financial coverage against Death and Terminal Illness (TI). Retirement Income will be paid out unless you choose to deposit them with AXA to earn interest at a non-guaranteed interest rate. You can withdraw partial OR all of the deposited Retirement Income with earned interest, subject to minimum withdrawal amount equivalent to the first Retirement Income payout.\n\nBonus rates used for the benefit illustration are non-guaranteed, the actual payable bonuses may vary based on future performance of the Participating Fund. Refer to the Product Summary for the exclusions, terms & conditions for payable Benefits.",
          reason: "asdf",
          rop: {
            choiceQ1: "N",
            choiceQ1Sub1: "",
            choiceQ1Sub2: "",
            choiceQ1Sub3: "",
            existCi: 0,
            existLife: 3333,
            existPaAdb: 0,
            existTotalPrem: 44,
            existTpd: 0,
            replaceCi: 0,
            replaceLife: 0,
            replacePaAdb: 0,
            replaceTotalPrem: 0,
            replaceTpd: 0
          },
          rop_shield: {
            ropBlock: {
              iCidRopAnswerMap: {},
              ropQ1sub3: "",
              ropQ2: "",
              ropQ3: "",
              shieldRopAnswer_0: "",
              shieldRopAnswer_1: "",
              shieldRopAnswer_2: "",
              shieldRopAnswer_3: "",
              shieldRopAnswer_4: "",
              shieldRopAnswer_5: ""
            }
          }
        }
      },
      fnaAns: "",
      iCid: "CP001003-00006",
      iCids: [],
      iName: "Alex 4",
      id: "QU001003-01250",
      insureds: {},
      lastUpdateDate: "2018-04-24T03:35:18.052Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "premium",
          covCode: "RHP",
          covName: {
            en: "AXA Retire Happy Plus",
            "zh-Hant": "AXA RETIRE HAPPY PLUS"
          },
          gteedAnnualRetireIncome: 11597.1,
          halfYearPrem: 12497.99,
          monthPrem: 2144.26,
          planCode: "05SL50A",
          polTermDesc: "35 Years",
          policyTerm: 35,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 24505.87,
          productLine: "WLE",
          quarterPrem: 6371.52,
          retirementIncome: 11597.1,
          saViewInd: "N",
          sumAssured: 266600,
          version: 1,
          yearPrem: 24505.87
        },
        {
          annualPremium: 501.21,
          covCode: "TPD",
          covName: {
            en: "Total and Permanent Disability Multiplier Rider",
            "zh-Hant": "Total and Permanent Disability Benefit"
          },
          gteedAnnualRetireIncome: 57985.5,
          halfYearPrem: 255.61,
          monthPrem: 43.85,
          planCode: "05TL50A",
          polTermDesc: "20 Years",
          policyTerm: 20,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 501.21,
          productLine: "WLE",
          quarterPrem: 130.31,
          sumAssured: 266600,
          sumAssuredNonTPD: 25007.08,
          version: 1,
          yearPrem: 501.21
        }
      ],
      policyOptions: {
        paymentMethod: "cash",
        payoutTerm: "15",
        payoutType: "Level",
        retirementAge: "50"
      },
      policyOptionsDesc: {
        paymentMethod: {
          en: "Cash"
        },
        payoutTerm: {
          en: "15 Years"
        },
        payoutType: {
          en: "Level"
        },
        retirementAge: {
          en: "Age 50"
        }
      },
      productLine: "WLE",
      quotType: "",
      statusChangeDate: "2018-04-24T03:35:18.052Z",
      totPremium: 25007.079999999998,
      type: "quotation",
      appStatus: "INVALIDATED",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "SAV",
      baseProductName: {
        en: "SavvySaver",
        "zh-Hant": "SavvySaver"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      iCid: "CP001003-00006",
      iCidMapping: {},
      iCids: [],
      iName: "Alex 4",
      id: "NB001003-01992",
      isFullySigned: false,
      isMandDocsAllUploaded: false,
      isStartSignature: false,
      isSubQuotation: false,
      isValid: true,
      lastUpdateDate: "2018-04-23T09:12:12.419Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "A",
      plans: [
        {
          calcBy: "premium",
          covCode: "SAV",
          covName: {
            en: "SavvySaver",
            "zh-Hant": "SavvySaver"
          },
          halfYearPrem: 583.84,
          monthPrem: 100.17,
          planCode: "15SS",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 108,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 1144.8,
          productLine: "WLE",
          quarterPrem: 297.64,
          saViewInd: "Y",
          sumInsured: 10600,
          version: 1,
          yearPrem: 1144.8
        },
        {
          covCode: "PET",
          covName: {
            en: "PremiumEraser Total",
            "zh-Hant": "PremiumEraser Total"
          },
          halfYearPrem: 10.91,
          monthPrem: 1.87,
          planCode: "15WOP",
          polTermDesc: "15 Years",
          policyTerm: "15",
          premRate: 1.87,
          premTerm: "15",
          premTermDesc: "15 Years",
          premium: 21.4,
          productLine: "WLE",
          quarterPrem: 5.56,
          saViewInd: "N",
          sumInsured: 1144.8,
          version: 1,
          yearPrem: 21.4
        }
      ],
      policyNumber: "101-4576928",
      productLine: "WLE",
      quotType: "",
      quotation: {
        agent: {
          agentCode: "008008",
          company: "AXA Insurance Pte Ltd",
          dealerGroup: "AGENCY",
          email: "alex.tang@eabsystems.com",
          mobile: "12345678",
          name: "Alex Tang",
          tel: "0"
        },
        agentCode: "008008",
        baseProductCode: "SAV",
        baseProductId: "08_product_SAV_1",
        baseProductName: {
          en: "SavvySaver",
          "zh-Hant": "SavvySaver"
        },
        budgetRules: ["RP"],
        bundleId: "FN001003-00255",
        ccy: "SGD",
        clientChoice: {
          isClientChoiceSelected: true,
          recommendation: {
            benefit:
              "SavvySaver is a regular premium, participating anticipated endowment policy. It provides you Death and Terminal Illness coverage, yearly Guaranteed Cash Payouts from the end of the second Policy year and a Guaranteed Maturity benefit plus bonuses (if any), payable to you in a lump sum upon Policy maturity. SavvySaver terminates upon full payout of either your TI benefit, Death benefit, Policy maturity amount or any other reasons as stated in Product Summary.\n\nFree-Look Period: Upon receiving your policy, you have 14 days to review if it suits your needs. If you wish to cancel it, give us a written request and we will refund the premiums paid for this Policy less any medical expenses incurred in processing your application. If Policy is received by post, your 14-day Fee Look Period starts 7 days from the date of posting.\n\nPremiumEraser Total Rider will waive future Premiums upon Total and Permanent Disability before the Policy Anniversary nearest to your 70th year birthday or you are diagnosed with one of the 36 Critical Illness, while this Additional Benefit is in force.",
            limitation:
              "The Sum Assured is a notional value, used solely for the calculation of Guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death or Terminal Illness. For the actual payout in the event of death and terminal illness please refer to the product summary.\nGuaranteed Cash Payouts could  be deposited with us at a non-guaranteed interest rate. You can withdraw partial OR all of the re-deposited amounts with earned interest, subject to the minimum withdrawal amount equal to 1 Guaranteed Cash Payout payment . The bonus rates used for the benefit illustration are non-guaranteed, the actual benefit payable may vary according to the future performance of the Participating Fund. There are certain conditions under which no benefits will be payable under this Policy. These are stated as exclusions in this Policy. Refer to your Product Summary for the exclusions, terms & conditions for payable Benefits.\nBuying a life insurance is a long-term commitment. An early termination of this Policy usually involves high costs and the surrender value may be less than the total Premiums paid.\n\nPremiumEraser Total Rider terminates upon its coverage expiry date of this additional benefit OR when Policy is assigned OR when you no longer suffers from TPD  or any other event which results in the termination of this Additional Benefit as set out in the Policy.\nThe premium rates for this Additional Benefit are not guaranteed. The rates may be adjusted based on future experience. Premiums are payable throughout the term of this Additional Benefit.\nPlease refer to product summary for terms and conditions and exclusions.",
            reason: "asdfasdf",
            rop: {
              choiceQ1: "Y",
              choiceQ1Sub1: "Y",
              choiceQ1Sub2: "Y",
              choiceQ1Sub3: "a",
              existCi: 0,
              existLife: 3333,
              existPaAdb: 0,
              existTotalPrem: 44,
              existTpd: 0,
              replaceCi: 0,
              replaceLife: 1111,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              replaceTpd: 0
            },
            rop_shield: {
              ropBlock: {
                iCidRopAnswerMap: {},
                ropQ1sub3: "",
                ropQ2: "",
                ropQ3: "",
                shieldRopAnswer_0: "",
                shieldRopAnswer_1: "",
                shieldRopAnswer_2: "",
                shieldRopAnswer_3: "",
                shieldRopAnswer_4: "",
                shieldRopAnswer_5: ""
              }
            }
          }
        },
        compCode: "01",
        createDate: "2018-04-23T09:11:27.277Z",
        dealerGroup: "AGENCY",
        extraFlags: {
          fna: {
            riskProfile: 5
          },
          nationalityResidence: {
            iCategory: "B1",
            iRejected: "N",
            pCategory: "B1",
            pRejected: "N"
          }
        },
        fund: null,
        iAge: 30,
        iCid: "CP001003-00006",
        iDob: "1987-11-24",
        iEmail: "alex.tang@eabsystems.com",
        iFirstName: "Alex 4",
        iFullName: "Alex 4",
        iGender: "M",
        iLastName: "",
        iOccupation: "O921",
        iOccupationClass: "4",
        iResidence: "R2",
        iSmoke: "Y",
        id: "QU001003-01249",
        isBackDate: "N",
        lastUpdateDate: "2018-04-23T09:11:38.403Z",
        pAge: 30,
        pCid: "CP001003-00006",
        pDob: "1987-11-24",
        pEmail: "alex.tang@eabsystems.com",
        pFirstName: "Alex 4",
        pFullName: "Alex 4",
        pGender: "M",
        pLastName: "",
        pOccupation: "O921",
        pOccupationClass: "4",
        pResidence: "R2",
        pSmoke: "Y",
        paymentMode: "A",
        plans: [
          {
            calcBy: "premium",
            covCode: "SAV",
            covName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            },
            halfYearPrem: 583.84,
            monthPrem: 100.17,
            planCode: "15SS",
            polTermDesc: "15 Years",
            policyTerm: "15",
            premRate: 108,
            premTerm: "15",
            premTermDesc: "15 Years",
            premium: 1144.8,
            productLine: "WLE",
            quarterPrem: 297.64,
            saViewInd: "Y",
            sumInsured: 10600,
            version: 1,
            yearPrem: 1144.8
          },
          {
            covCode: "PET",
            covName: {
              en: "PremiumEraser Total",
              "zh-Hant": "PremiumEraser Total"
            },
            halfYearPrem: 10.91,
            monthPrem: 1.87,
            planCode: "15WOP",
            polTermDesc: "15 Years",
            policyTerm: "15",
            premRate: 1.87,
            premTerm: "15",
            premTermDesc: "15 Years",
            premium: 21.4,
            productLine: "WLE",
            quarterPrem: 5.56,
            saViewInd: "N",
            sumInsured: 1144.8,
            version: 1,
            yearPrem: 21.4
          }
        ],
        policyOptions: {},
        policyOptionsDesc: {},
        premTerm: "15",
        premium: 1166.2,
        productLine: "WLE",
        riskCommenDate: "2018-04-23",
        sameAs: "Y",
        sumInsured: 11744.8,
        totHalfyearPrem: 594.75,
        totMonthPrem: 102.04,
        totQuarterPrem: 303.2,
        totYearPrem: 1166.2,
        type: "quotation"
      },
      quotationDocId: "QU001003-01249",
      totPremium: 1166.2,
      type: "application",
      appStatus: "INVALIDATED",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "ESP",
      baseProductName: {
        en: "AXA Early Saver Plus",
        "zh-Hant": "AXA Early Saver 2"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      iCid: "CP001003-00008",
      iCidMapping: {},
      iCids: [],
      iName: "Alex 4 Son",
      id: "NB001003-02029",
      isFullySigned: true,
      isMandDocsAllUploaded: false,
      isStartSignature: true,
      isSubQuotation: false,
      isValid: true,
      lastUpdateDate: "2018-05-09T09:31:27.153Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "Q",
      plans: [
        {
          calcBy: "premium",
          covCode: "ESP",
          covName: {
            en: "AXA Early Saver Plus",
            "zh-Hant": "AXA Early Saver 2"
          },
          halfYearPrem: 38453.72,
          monthPrem: 6597.45,
          newPlanName: "AXA Early Saver Plus 5 PAY",
          planCode: "05AES24",
          polTermDesc: "24 Years",
          policyTerm: 24,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 19603.86,
          productLine: "WLE",
          quarterPrem: 19603.86,
          saViewInd: "Y",
          sumInsured: 529900,
          version: 1,
          yearPrem: 75399.47
        },
        {
          covCode: "ESR",
          covName: {
            en: "Enhanced Benefit Rider",
            "zh-Hant": "Enhanced Benefit Rider"
          },
          halfYearPrem: 778.31,
          monthPrem: 133.53,
          newPlanName: "Enhanced Benefit Rider 5 PAY",
          planCode: "05ESR24",
          polTermDesc: "24 Years",
          policyTerm: 24,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 396.78,
          productLine: "WLE",
          quarterPrem: 396.78,
          saPostfix: {},
          saViewInd: "Y",
          sumInsured: 529900,
          version: 1,
          yearPrem: 1526.11
        },
        {
          covCode: "PPU",
          covName: {
            en: "Payer PremiumEraser (DTPDCIUN)",
            "zh-Hant": "Payer PremiumEraser(DTPDCIUN)"
          },
          halfYearPrem: 867.02,
          monthPrem: 148.75,
          newPlanName: "Payer PremiumEraser (DTPDCIUN)",
          planCode: "05PPUN",
          polTermDesc: "5 Years",
          policyTerm: "5_YR",
          policyTermYr: 5,
          premTerm: "5_YR",
          premTermDesc: "5 Years",
          premTermYr: 5,
          premium: 442.01,
          productLine: "CIT",
          quarterPrem: 442.01,
          version: 1,
          yearPrem: 1700.05
        }
      ],
      policyNumber: "101-4669301",
      productLine: "WLE",
      quotType: "",
      quotation: {
        agent: {
          agentCode: "008008",
          company: "AXA Insurance Pte Ltd",
          dealerGroup: "AGENCY",
          email: "alex.tang@eabsystems.com",
          mobile: "12345678",
          name: "Alex Tang",
          tel: "0"
        },
        agentCode: "008008",
        baseProductCode: "ESP",
        baseProductId: "08_product_ESP_1",
        baseProductName: {
          en: "AXA Early Saver Plus",
          "zh-Hant": "AXA Early Saver 2"
        },
        budgetRules: ["RP"],
        bundleId: "FN001003-00255",
        ccy: "SGD",
        clientChoice: {
          isClientChoiceSelected: true,
          recommendation: {
            benefit:
              "AXA Early Saver is a participating endowment policy. It covers the Life Assured for Death and Terminal Illness (TI). It has an embedded non-participating Enhanced Benefit rider which provides coverage for Total & Permanent Disability (TPD) benefit, Accidental Death benefit and Outpatient Medical benefit. AXA Early Saver provides non-guaranteed Maturity Benefits & surrender value. It also provides a Guaranteed Cash Payout.\n\nUpon the Life Assured's Death, TI or TPD, the Sum Assured payable is a) the higher of 101% of total Premium Paid (including the TPD premium but excluding other supplementary riders premiums) less any total Guaranteed Cash Payouts received & any outstanding indebtedness, if any, OR the Total Surrender Value; b) plus Deposited Guaranteed Cash Payouts with accumulated non-guaranteed interest, if any. The TPD benefit is an advanced lump sum of the Death benefit, payable if TPD is diagnosed on or before Policy Anniversary prior to Life Assured's 70th birthday while the Policy is in force.\n\nThe free-Look Period allows you to review your policy. If you decide that this policy does not suit your needs, you may return it to us within 14 days from the date you received it and we will refund the premiums paid for this Policy less any medical expenses incurred in processing your application. If Policy is received by post, it is considered to have been received by you 7 days from the date of posting.\n\nPayer PremiumEraser (DTPDUN) is a non-participating rider that waives the premiums on Your Policy on the occurrence of the following events:\n\na) Involuntary Loss of Income: We will waive six (6) months of Premiums from the next Premium due date if you, working under Full-Time Employment, suffer an Involuntary Loss of Income (ILOI) before age 50. \n\nILOI refers to either of the following happening to you:\n\nâ€¢Your employer where you are under Full-Time Employment has terminated such Full-Time Employment due to Redundancy or Retrenchment; or\n\nâ€¢ You suffer from Short Term Disability for a continuous period of at least three (3) months, and is unable to be engaged under Full-Time Employment.\n\nIn both instances, you must not be receiving any income from other employment, whether full-time or part-time.\n\nb) Death and Total and Permanent Disability (TPD): We will waive the future Premiums of Your Policy if (i) death occured; or (ii) You suffer from Total and Permanent Disability (TPD) before the Policy Anniversary nearest to Your 65th year birthday.",
            limitation:
              "AXA Early Saver provides limited financial coverage for Death, Terminal Illness (TI) and Total & Permanent Disability (TPD). The Sum Assured is a notional value, used solely for the calculation of guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death, TI or TPD. The actual payable Bonuses may be higher or lower than projected values, based on future performance of the Participating Fund.\n\nOnce a claim has been admitted for Terminal Illness Benefit, we will not admit a claim for Death, Accidental Death, TPD Benefit or Outpatient Medical Benefit.  Refer to the Product Summary for the exclusions, terms & conditions for Benefits to be payable.\n\nPayer PremiumEraser (DTPDUN) rider will automatically terminate once a successful Death, TPD & ILOI claim has been paid. Upon a claim, this rider will terminate. Please refer to product summary for terms and conditions and exclusions.\n\nYou will only be eligible to claim for ILOI if the Policy has been in force for at least three (3) months from the Commencement Date or the last Reinstatement Date. After the end of the ILOI waiver period, Premium payment of Your Policy will resume. Premium rate is not guaranteed.",
            reason: "aaaa",
            rop: {
              choiceQ1: "N",
              choiceQ1Sub1: "",
              choiceQ1Sub2: "",
              choiceQ1Sub3: "",
              existCi: 0,
              existLife: 1111,
              existPaAdb: 0,
              existTotalPrem: 222,
              existTpd: 0,
              replaceCi: 0,
              replaceLife: 0,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              replaceTpd: 0
            },
            rop_shield: {
              ropBlock: {
                iCidRopAnswerMap: {},
                ropQ1sub3: "",
                ropQ2: "",
                ropQ3: "",
                shieldRopAnswer_0: "",
                shieldRopAnswer_1: "",
                shieldRopAnswer_2: "",
                shieldRopAnswer_3: "",
                shieldRopAnswer_4: "",
                shieldRopAnswer_5: ""
              }
            }
          }
        },
        compCode: "01",
        createDate: "2018-05-07T10:39:57.974Z",
        dealerGroup: "AGENCY",
        extraFlags: {
          fna: {
            riskProfile: 5
          },
          nationalityResidence: {
            iCategory: "A1",
            iRejected: "N",
            pCategory: "A3",
            pRejected: "N"
          },
          previousInputPremium: 20000,
          previousPremium: 19603.86,
          riders: {}
        },
        fund: null,
        iAge: 3,
        iCid: "CP001003-00008",
        iDob: "2014-12-30",
        iEmail: "ma@b.co",
        iFirstName: "Alex 4 Son",
        iFullName: "Alex 4 Son",
        iGender: "M",
        iLastName: "",
        iOccupation: "O1",
        iOccupationClass: "4",
        iResidence: "R2",
        iSmoke: "N",
        id: "QU001003-01319",
        isBackDate: "N",
        lastUpdateDate: "2018-05-09T09:28:14.133Z",
        pAge: 30,
        pCid: "CP001003-00006",
        pDob: "1987-11-24",
        pEmail: "alex.tang@eabsystems.com",
        pFirstName: "Alex 4",
        pFullName: "Alex 4",
        pGender: "M",
        pLastName: "",
        pOccupation: "O921",
        pOccupationClass: "4",
        pResidence: "R54",
        pResidenceCity: "T120",
        pSmoke: "Y",
        paymentMode: "Q",
        plans: [
          {
            calcBy: "premium",
            covCode: "ESP",
            covName: {
              en: "AXA Early Saver Plus",
              "zh-Hant": "AXA Early Saver 2"
            },
            halfYearPrem: 38453.72,
            monthPrem: 6597.45,
            newPlanName: "AXA Early Saver Plus 5 PAY",
            planCode: "05AES24",
            polTermDesc: "24 Years",
            policyTerm: 24,
            premTerm: 5,
            premTermDesc: "5 Years",
            premium: 19603.86,
            productLine: "WLE",
            quarterPrem: 19603.86,
            saViewInd: "Y",
            sumInsured: 529900,
            version: 1,
            yearPrem: 75399.47
          },
          {
            covCode: "ESR",
            covName: {
              en: "Enhanced Benefit Rider",
              "zh-Hant": "Enhanced Benefit Rider"
            },
            halfYearPrem: 778.31,
            monthPrem: 133.53,
            newPlanName: "Enhanced Benefit Rider 5 PAY",
            planCode: "05ESR24",
            polTermDesc: "24 Years",
            policyTerm: 24,
            premTerm: 5,
            premTermDesc: "5 Years",
            premium: 396.78,
            productLine: "WLE",
            quarterPrem: 396.78,
            saPostfix: {},
            saViewInd: "Y",
            sumInsured: 529900,
            version: 1,
            yearPrem: 1526.11
          },
          {
            covCode: "PPU",
            covName: {
              en: "Payer PremiumEraser (DTPDCIUN)",
              "zh-Hant": "Payer PremiumEraser(DTPDCIUN)"
            },
            halfYearPrem: 867.02,
            monthPrem: 148.75,
            newPlanName: "Payer PremiumEraser (DTPDCIUN)",
            planCode: "05PPUN",
            polTermDesc: "5 Years",
            policyTerm: "5_YR",
            policyTermYr: 5,
            premTerm: "5_YR",
            premTermDesc: "5 Years",
            premTermYr: 5,
            premium: 442.01,
            productLine: "CIT",
            quarterPrem: 442.01,
            version: 1,
            yearPrem: 1700.05
          }
        ],
        policyOptions: {
          guaranteedCashPayoutType: "paidOut"
        },
        policyOptionsDesc: {
          guaranteedCashPayoutType: {
            en: "Paid Out"
          }
        },
        premTerm: 5,
        premium: 20442.65,
        productLine: "WLE",
        riskCommenDate: "2018-05-09",
        sameAs: "N",
        sumInsured: 1059800,
        totHalfyearPrem: 40099.05,
        totMonthPrem: 6879.73,
        totQuarterPrem: 20442.65,
        totYearPrem: 78625.63,
        type: "quotation"
      },
      quotationDocId: "QU001003-01319",
      totPremium: 20442.65,
      type: "application",
      appStatus: "INVALIDATED_SIGNED",
      approvalStatus: "",
      paymentMethod: "RP"
    },
    {
      baseProductCode: "ESP",
      baseProductName: {
        en: "AXA Early Saver Plus",
        "zh-Hant": "AXA Early Saver 2"
      },
      bundleId: "FN001003-00255",
      ccy: "SGD",
      iCid: "CP001003-00006",
      iCidMapping: {},
      iCids: [],
      iName: "Alex 4",
      id: "NB001003-02030",
      isFullySigned: true,
      isInitialPaymentCompleted: false,
      isMandDocsAllUploaded: true,
      isStartSignature: true,
      isSubQuotation: false,
      isValid: true,
      lastUpdateDate: "2018-05-09T09:30:26.891Z",
      pCid: "CP001003-00006",
      pName: "Alex 4",
      paymentMode: "S",
      plans: [
        {
          calcBy: "premium",
          covCode: "ESP",
          covName: {
            en: "AXA Early Saver Plus",
            "zh-Hant": "AXA Early Saver 2"
          },
          halfYearPrem: 21788.53,
          monthPrem: 3738.22,
          newPlanName: "AXA Early Saver Plus 5 PAY",
          planCode: "05AES21",
          polTermDesc: "21 Years",
          policyTerm: 21,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 21788.53,
          productLine: "WLE",
          quarterPrem: 11107.88,
          saViewInd: "Y",
          sumInsured: 283100,
          version: 1,
          yearPrem: 42722.62
        },
        {
          covCode: "ESR",
          covName: {
            en: "Enhanced Benefit Rider",
            "zh-Hant": "Enhanced Benefit Rider"
          },
          halfYearPrem: 440.36,
          monthPrem: 75.55,
          newPlanName: "Enhanced Benefit Rider 5 PAY",
          planCode: "05ESR21",
          polTermDesc: "21 Years",
          policyTerm: 21,
          premTerm: 5,
          premTermDesc: "5 Years",
          premium: 440.36,
          productLine: "WLE",
          quarterPrem: 224.49,
          saPostfix: {},
          saViewInd: "Y",
          sumInsured: 283100,
          version: 1,
          yearPrem: 863.45
        },
        {
          covCode: "UNBN",
          covName: {
            en: "Premium Waiver (UN)",
            "zh-Hant": "Premium Waiver (UN)"
          },
          halfYearPrem: 162.26,
          monthPrem: 27.83,
          newPlanName: "Premium Waiver (UN)",
          planCode: "05UNBN",
          polTermDesc: "5 Years",
          policyTerm: "5_YR",
          policyTermYr: 5,
          premTerm: "5_YR",
          premTermDesc: "5 Years",
          premTermYr: 5,
          premium: 162.26,
          productLine: "CIT",
          quarterPrem: 82.72,
          version: 1,
          yearPrem: 318.17
        }
      ],
      policyNumber: "101-4668766",
      productLine: "WLE",
      quotType: "",
      quotation: {
        agent: {
          agentCode: "008008",
          company: "AXA Insurance Pte Ltd",
          dealerGroup: "AGENCY",
          email: "alex.tang@eabsystems.com",
          mobile: "12345678",
          name: "Alex Tang",
          tel: "0"
        },
        agentCode: "008008",
        baseProductCode: "ESP",
        baseProductId: "08_product_ESP_1",
        baseProductName: {
          en: "AXA Early Saver Plus",
          "zh-Hant": "AXA Early Saver 2"
        },
        budgetRules: ["RP"],
        bundleId: "FN001003-00255",
        ccy: "SGD",
        clientChoice: {
          isClientChoiceSelected: true,
          recommendation: {
            benefit:
              "AXA Early Saver is a participating endowment policy. It covers Death and Terminal Illness (TI). It has an embedded non-participating Enhanced Benefit rider which provides coverage for Total & Permanent Disability (TPD) benefit, Accidental Death benefit and Outpatient Medical benefit. AXA Early Saver provides non-guaranteed Maturity Benefits & surrender value. It also provides a Guaranteed Cash Payout.\n\nUpon Death, TI or TPD, the Sum Assured payable is a) the higher of 101% of total Premium Paid (including the TPD premium but excluding other supplementary riders premiums) less any total Guaranteed Cash Payouts received & any outstanding indebtedness, if any, OR the Total Surrender Value; b) plus Deposited Guaranteed Cash Payouts with accumulated non-guaranteed interest, if any. The TPD benefit is an advanced lump sum of the Death benefit, payable if TPD is diagnosed on or before Policy Anniversary prior to your 70th birthday while the Policy is in force.\n\nThe free-Look Period allows you to review your policy. If you decide that this policy does not suit your needs, you may return it to us within 14 days from the date you received it and we will refund the premiums paid for this Policy less any medical expenses incurred in processing your application. If Policy is received by post, it is considered to have been received by you 7 days from the date of posting.\n\nPremium Waiver (UN) is a non-participating rider that waives six (6) months of Premiums from the next Premium due date if you, working under Full-Time Employment, suffer an Involuntary Loss of Income (ILOI) before age 50.\n\nILOI refers to either of the following happening to you:\n\nâ€¢ Your employer where you are under Full-Time Employment has terminated such Full-Time Employment due to Redundancy or Retrenchment.; or\n\nâ€¢ Short Term Disability for a continuous period of at least three (3) months, and you are unable to be engaged under Full-Time Employment.\n\nIn both instances, you must not be receiving any income from other employment, whether full-time or part-time. After the end of the waiver period, Premium payment of Your Policy will resume and Your Rider will automatically terminate.",
            limitation:
              "AXA Early Saver provides limited financial coverage for Death, Terminal Illness (TI) and Total & Permanent Disability (TPD). The Sum Assured is a notional value, used solely for the calculation of guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death, TI or TPD. The actual payable Bonuses may be higher or lower than projected values, based on future performance of the Participating Fund.\n\nOnce a claim has been admitted for Terminal Illness Benefit, we will not admit a claim for Death, Accidental Death, TPD Benefit or Outpatient Medical Benefit. Refer to the Product Summary for the exclusions, terms & conditions for Benefits to be payable.\n\nFull-Time Employment shall mean that you are working for at least forty (40) hours per week with a registered company on a permanent basis.\n\nYou will only be eligible to claim for ILOI if the Policy has been in force for at least three (3) months from the Commencement Date or the last Reinstatement Date. You can only claim for ILOI once throughout the term of Your Policy.  Upon a claim, this rider will terminate. After the end of the ILOI waiver period, Premium payment of Your Policy will resume. Please refer to product summary for terms and conditions and exclusions. Premium rate is not guaranteed.",
            reason: ",mn,m",
            rop: {
              choiceQ1: "N",
              choiceQ1Sub1: "",
              choiceQ1Sub2: "",
              choiceQ1Sub3: "",
              existCi: 0,
              existLife: 3333,
              existPaAdb: 0,
              existTotalPrem: 44,
              existTpd: 0,
              replaceCi: 0,
              replaceLife: 0,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              replaceTpd: 0
            },
            rop_shield: {
              ropBlock: {
                iCidRopAnswerMap: {},
                ropQ1sub3: "",
                ropQ2: "",
                ropQ3: "",
                shieldRopAnswer_0: "",
                shieldRopAnswer_1: "",
                shieldRopAnswer_2: "",
                shieldRopAnswer_3: "",
                shieldRopAnswer_4: "",
                shieldRopAnswer_5: ""
              }
            }
          }
        },
        compCode: "01",
        createDate: "2018-05-08T01:31:28.882Z",
        dealerGroup: "AGENCY",
        extraFlags: {
          fna: {
            riskProfile: 5
          },
          nationalityResidence: {
            iCategory: "A3",
            iRejected: "N",
            pCategory: "A3",
            pRejected: "N"
          },
          previousInputPremium: 22222,
          previousPremium: 21788.53,
          riders: {}
        },
        fund: null,
        iAge: 30,
        iCid: "CP001003-00006",
        iDob: "1987-11-24",
        iEmail: "alex.tang@eabsystems.com",
        iFirstName: "Alex 4",
        iFullName: "Alex 4",
        iGender: "M",
        iLastName: "",
        iOccupation: "O921",
        iOccupationClass: "4",
        iResidence: "R54",
        iResidenceCity: "T120",
        iSmoke: "Y",
        id: "QU001003-01320",
        isBackDate: "N",
        lastUpdateDate: "2018-05-09T09:27:53.216Z",
        pAge: 30,
        pCid: "CP001003-00006",
        pDob: "1987-11-24",
        pEmail: "alex.tang@eabsystems.com",
        pFirstName: "Alex 4",
        pFullName: "Alex 4",
        pGender: "M",
        pLastName: "",
        pOccupation: "O921",
        pOccupationClass: "4",
        pResidence: "R54",
        pResidenceCity: "T120",
        pSmoke: "Y",
        paymentMode: "S",
        plans: [
          {
            calcBy: "premium",
            covCode: "ESP",
            covName: {
              en: "AXA Early Saver Plus",
              "zh-Hant": "AXA Early Saver 2"
            },
            halfYearPrem: 21788.53,
            monthPrem: 3738.22,
            newPlanName: "AXA Early Saver Plus 5 PAY",
            planCode: "05AES21",
            polTermDesc: "21 Years",
            policyTerm: 21,
            premTerm: 5,
            premTermDesc: "5 Years",
            premium: 21788.53,
            productLine: "WLE",
            quarterPrem: 11107.88,
            saViewInd: "Y",
            sumInsured: 283100,
            version: 1,
            yearPrem: 42722.62
          },
          {
            covCode: "ESR",
            covName: {
              en: "Enhanced Benefit Rider",
              "zh-Hant": "Enhanced Benefit Rider"
            },
            halfYearPrem: 440.36,
            monthPrem: 75.55,
            newPlanName: "Enhanced Benefit Rider 5 PAY",
            planCode: "05ESR21",
            polTermDesc: "21 Years",
            policyTerm: 21,
            premTerm: 5,
            premTermDesc: "5 Years",
            premium: 440.36,
            productLine: "WLE",
            quarterPrem: 224.49,
            saPostfix: {},
            saViewInd: "Y",
            sumInsured: 283100,
            version: 1,
            yearPrem: 863.45
          },
          {
            covCode: "UNBN",
            covName: {
              en: "Premium Waiver (UN)",
              "zh-Hant": "Premium Waiver (UN)"
            },
            halfYearPrem: 162.26,
            monthPrem: 27.83,
            newPlanName: "Premium Waiver (UN)",
            planCode: "05UNBN",
            polTermDesc: "5 Years",
            policyTerm: "5_YR",
            policyTermYr: 5,
            premTerm: "5_YR",
            premTermDesc: "5 Years",
            premTermYr: 5,
            premium: 162.26,
            productLine: "CIT",
            quarterPrem: 82.72,
            version: 1,
            yearPrem: 318.17
          }
        ],
        policyOptions: {
          guaranteedCashPayoutType: "paidOut"
        },
        policyOptionsDesc: {
          guaranteedCashPayoutType: {
            en: "Paid Out"
          }
        },
        premTerm: 5,
        premium: 22391.15,
        productLine: "WLE",
        riskCommenDate: "2018-05-09",
        sameAs: "Y",
        sumInsured: 566200,
        totHalfyearPrem: 22391.15,
        totMonthPrem: 3841.6,
        totQuarterPrem: 11415.09,
        totYearPrem: 43904.24,
        type: "quotation"
      },
      quotationDocId: "QU001003-01320",
      totPremium: 22391.15,
      type: "application",
      appStatus: "SUBMITTED",
      approvalStatus: "Approved",
      paymentMethod: "RP"
    }
  ]
};
export const FAKE_PDA_MEMBERS = [
  {
    cid: "CP001003-00006",
    fullName: "Alex 4",
    relationship: "OWNER"
  },
  {
    cid: "CP001003-00008",
    relationship: "DEPENDANTS",
    filter: "SON",
    fullName: "Alex 4 Son"
  },
  {
    cid: "CP001003-00009",
    relationship: "DEPENDANTS",
    filter: "MOT",
    fullName: "Alex 4 Mother"
  },
  {
    cid: "CP001003-00010",
    relationship: "DEPENDANTS",
    filter: "FAT",
    fullName: "Alex 4 Father"
  },
  {
    cid: "CP001003-00028",
    relationship: "DEPENDANTS",
    filter: "SON",
    fullName: "Alex 4 Son xPR"
  }
];
export const FAKE_QUOT_CHECKED_LIST = {
  "FN001003-00276": [
    {
      id: "QU001003-01365",
      checked: false
    },
    {
      id: "QU001003-01366",
      checked: false
    },
    {
      id: "QU001003-01367",
      checked: false
    }
  ],
  "FN001003-00255": [
    {
      checked: true,
      id: "QU001003-01319"
    },
    {
      checked: true,
      id: "QU001003-01320"
    }
  ]
};
export const FAKE_PRODUCT_LIST = [
  {
    ccy: "",
    covCode: "SAV",
    covName: {
      en: "SavvySaver",
      "zh-Hant": "SavvySaver"
    },
    iCid: "CP001003-00006",
    imageSource: "",
    pCid: "CP001003-00006"
  },
  {
    ccy: "",
    covCode: "BAA",
    covName: {
      en: "AXA Band Aid",
      "zh-Hant": "AXA Band Aid"
    },
    iCid: "CP001003-00008",
    imageSource: "",
    pCid: "CP001003-00006"
  },
  {
    covCode: "ASIM",
    covName: {
      en: "AXA Shield Plan",
      "zh-Hant": "AXA Shield"
    },
    iCid: "CP001003-00006",
    pCid: "CP001003-00006",
    ccy: "",
    imageSource: ""
  }
];
