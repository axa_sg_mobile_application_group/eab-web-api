export const continueBAA = {
  p0: "fe7a64675cb65585b60631b7fa8f5f96bf6a940b3f033797de7dc0bdfb33910f",
  p1: {
    template: {
      items: [
        {
          type: "section",
          title: "Application",
          detailSeq: 1,
          items: [
            {
              type: "menu",
              items: [
                {
                  type: "menuSection",
                  detailSeq: 0,
                  items: [
                    {
                      title: {
                        en: "Personal Details"
                      },
                      type: "menuItem",
                      key: "menu_person",
                      detailSeq: 0,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              priority: "proposer",
                              type: "tab",
                              items: [
                                {
                                  type: "PARAGRAPH",
                                  id: "pWarning",
                                  content:
                                    "<p>â€œPursuant to Section 25(5) of the Insurance Act of Singapore (CAP 142), you are to disclose in this proposal form, fully and faithfully, all the facts which you know or ought to know, or the policy issued below may be void.â€</p>",
                                  paragraphType: "warning",
                                  align: "left",
                                  detailSeq: 0
                                },
                                {
                                  id: "personalInfo",
                                  type: "Block",
                                  items: [
                                    {
                                      id: "branchInfo",
                                      title: "Singpost Branch Details",
                                      type: "BLOCK",
                                      detailSeq: 1,
                                      trigger: {
                                        id: "extra/channelName",
                                        type: "showIfEqual",
                                        value: "SINGPOST"
                                      },
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "PICKER",
                                              id: "branch",
                                              title: "Branch",
                                              subType: "limit",
                                              mandatory: true,
                                              detailSeq: 1,
                                              options: [
                                                {
                                                  value: "Alexandra",
                                                  title: {
                                                    en: "Alexandra"
                                                  },
                                                  rlscode: "10"
                                                },
                                                {
                                                  value: "Ang Mo Kio Central",
                                                  title: {
                                                    en: "Ang Mo Kio Central"
                                                  },
                                                  rlscode: "11"
                                                },
                                                {
                                                  value: "Bedok Central",
                                                  title: {
                                                    en: "Bedok Central"
                                                  },
                                                  rlscode: "12"
                                                },
                                                {
                                                  value: "Bishan",
                                                  title: {
                                                    en: "Bishan"
                                                  },
                                                  rlscode: "13"
                                                },
                                                {
                                                  value: "Bras Basah",
                                                  title: {
                                                    en: "Bras Basah"
                                                  },
                                                  rlscode: "14"
                                                },
                                                {
                                                  value: "Bukit Batok Central",
                                                  title: {
                                                    en: "Bukit Batok Central"
                                                  },
                                                  rlscode: "15"
                                                },
                                                {
                                                  value: "Bukit Merah Central",
                                                  title: {
                                                    en: "Bukit Merah Central"
                                                  },
                                                  rlscode: "16"
                                                },
                                                {
                                                  value: "Bukit Panjang",
                                                  title: {
                                                    en: "Bukit Panjang"
                                                  },
                                                  rlscode: "17"
                                                },
                                                {
                                                  value: "Bukit Timah",
                                                  title: {
                                                    en: "Bukit Timah"
                                                  },
                                                  rlscode: "18"
                                                },
                                                {
                                                  value:
                                                    "Changi Airport Terminal 2",
                                                  title: {
                                                    en:
                                                      "Changi Airport Terminal 2"
                                                  },
                                                  rlscode: "19"
                                                },
                                                {
                                                  value: "Chinatown",
                                                  title: {
                                                    en: "Chinatown"
                                                  },
                                                  rlscode: "20"
                                                },
                                                {
                                                  value:
                                                    "Choa Chua Kang Central",
                                                  title: {
                                                    en: "Choa Chua Kang Central"
                                                  },
                                                  rlscode: "21"
                                                },
                                                {
                                                  value: "Clementi Central",
                                                  title: {
                                                    en: "Clementi Central"
                                                  },
                                                  rlscode: "22"
                                                },
                                                {
                                                  value: "Clementi West",
                                                  title: {
                                                    en: "Clementi West"
                                                  },
                                                  rlscode: "23"
                                                },
                                                {
                                                  value: "Crawford",
                                                  title: {
                                                    en: "Crawford"
                                                  },
                                                  rlscode: "24"
                                                },
                                                {
                                                  value: "Geylang",
                                                  title: {
                                                    en: "Geylang"
                                                  },
                                                  rlscode: "25"
                                                },
                                                {
                                                  value: "Geylang East",
                                                  title: {
                                                    en: "Geylang East"
                                                  },
                                                  rlscode: "26"
                                                },
                                                {
                                                  value: "Ghim Moh Estate",
                                                  title: {
                                                    en: "Ghim Moh Estate"
                                                  },
                                                  rlscode: "27"
                                                },
                                                {
                                                  value: "HarbourFront Centre",
                                                  title: {
                                                    en: "HarbourFront Centre"
                                                  },
                                                  rlscode: "28"
                                                },
                                                {
                                                  value: "Hougang Central",
                                                  title: {
                                                    en: "Hougang Central"
                                                  },
                                                  rlscode: "29"
                                                },
                                                {
                                                  value: "Jurong East",
                                                  title: {
                                                    en: "Jurong East"
                                                  },
                                                  rlscode: "30"
                                                },
                                                {
                                                  value: "Jurong Point",
                                                  title: {
                                                    en: "Jurong Point"
                                                  },
                                                  rlscode: "31"
                                                },
                                                {
                                                  value: "Jurong West",
                                                  title: {
                                                    en: "Jurong West"
                                                  },
                                                  rlscode: "32"
                                                },
                                                {
                                                  value: "Katong",
                                                  title: {
                                                    en: "Katong"
                                                  },
                                                  rlscode: "33"
                                                },
                                                {
                                                  value: "Killiney Road",
                                                  title: {
                                                    en: "Killiney Road"
                                                  },
                                                  rlscode: "34"
                                                },
                                                {
                                                  value: "Kitchener Road",
                                                  title: {
                                                    en: "Kitchener Road"
                                                  },
                                                  rlscode: "35"
                                                },
                                                {
                                                  value: "Lim Ah Pin Road",
                                                  title: {
                                                    en: "Lim Ah Pin Road"
                                                  },
                                                  rlscode: "36"
                                                },
                                                {
                                                  value: "Marine Parade",
                                                  title: {
                                                    en: "Marine Parade"
                                                  },
                                                  rlscode: "37"
                                                },
                                                {
                                                  value: "Newton",
                                                  title: {
                                                    en: "Newton"
                                                  },
                                                  rlscode: "38"
                                                },
                                                {
                                                  value: "Novena",
                                                  title: {
                                                    en: "Novena"
                                                  },
                                                  rlscode: "39"
                                                },
                                                {
                                                  value: "Orchard",
                                                  title: {
                                                    en: "Orchard"
                                                  },
                                                  rlscode: "40"
                                                },
                                                {
                                                  value: "Pasir Panjang",
                                                  title: {
                                                    en: "Pasir Panjang"
                                                  },
                                                  rlscode: "41"
                                                },
                                                {
                                                  value: "Pasir Ris Central",
                                                  title: {
                                                    en: "Pasir Ris Central"
                                                  },
                                                  rlscode: "42"
                                                },
                                                {
                                                  value: "Pioneer Road",
                                                  title: {
                                                    en: "Pioneer Road"
                                                  },
                                                  rlscode: "43"
                                                },
                                                {
                                                  value: "Potong Pasir",
                                                  title: {
                                                    en: "Potong Pasir"
                                                  },
                                                  rlscode: "44"
                                                },
                                                {
                                                  value: "Punggol",
                                                  title: {
                                                    en: "Punggol"
                                                  },
                                                  rlscode: "45"
                                                },
                                                {
                                                  value: "Raffles Place",
                                                  title: {
                                                    en: "Raffles Place"
                                                  },
                                                  rlscode: "46"
                                                },
                                                {
                                                  value: "Robinson Road",
                                                  title: {
                                                    en: "Robinson Road"
                                                  },
                                                  rlscode: "47"
                                                },
                                                {
                                                  value: "Rochor",
                                                  title: {
                                                    en: "Rochor"
                                                  },
                                                  rlscode: "48"
                                                },
                                                {
                                                  value: "Sembawang",
                                                  title: {
                                                    en: "Sembawang"
                                                  },
                                                  rlscode: "49"
                                                },
                                                {
                                                  value: "Sengkang Central",
                                                  title: {
                                                    en: "Sengkang Central"
                                                  },
                                                  rlscode: "50"
                                                },
                                                {
                                                  value: "Serangoon Central",
                                                  title: {
                                                    en: "Serangoon Central"
                                                  },
                                                  rlscode: "51"
                                                },
                                                {
                                                  value: "Serangoon Garden",
                                                  title: {
                                                    en: "Serangoon Garden"
                                                  },
                                                  rlscode: "52"
                                                },
                                                {
                                                  value: "Siglap",
                                                  title: {
                                                    en: "Siglap"
                                                  },
                                                  rlscode: "53"
                                                },
                                                {
                                                  value: "Simpang Bedok",
                                                  title: {
                                                    en: "Simpang Bedok"
                                                  },
                                                  rlscode: "54"
                                                },
                                                {
                                                  value: "Paya Lebar",
                                                  title: {
                                                    en: "Paya Lebar"
                                                  },
                                                  rlscode: "55"
                                                },
                                                {
                                                  value: "Suntec City",
                                                  title: {
                                                    en: "Suntec City"
                                                  },
                                                  rlscode: "56"
                                                },
                                                {
                                                  value: "Tampines Central",
                                                  title: {
                                                    en: "Tampines Central"
                                                  },
                                                  rlscode: "57"
                                                },
                                                {
                                                  value: "Tanglin",
                                                  title: {
                                                    en: "Tanglin"
                                                  },
                                                  rlscode: "58"
                                                },
                                                {
                                                  value: "Tanjong Pagar",
                                                  title: {
                                                    en: "Tanjong Pagar"
                                                  },
                                                  rlscode: "59"
                                                },
                                                {
                                                  value: "Teban Garden",
                                                  title: {
                                                    en: "Teban Garden"
                                                  },
                                                  rlscode: "60"
                                                },
                                                {
                                                  value: "Thomson Road",
                                                  title: {
                                                    en: "Thomson Road"
                                                  },
                                                  rlscode: "61"
                                                },
                                                {
                                                  value: "Tiong Bahru",
                                                  title: {
                                                    en: "Tiong Bahru"
                                                  },
                                                  rlscode: "62"
                                                },
                                                {
                                                  value: "Toa Payoh Central",
                                                  title: {
                                                    en: "Toa Payoh Central"
                                                  },
                                                  rlscode: "63"
                                                },
                                                {
                                                  value: "Toa Payoh North",
                                                  title: {
                                                    en: "Toa Payoh North"
                                                  },
                                                  rlscode: "64"
                                                },
                                                {
                                                  value: "Towner",
                                                  title: {
                                                    en: "Towner"
                                                  },
                                                  rlscode: "65"
                                                },
                                                {
                                                  value: "Woodlands Central",
                                                  title: {
                                                    en: "Woodlands Central"
                                                  },
                                                  rlscode: "66"
                                                },
                                                {
                                                  value: "Yishun Central",
                                                  title: {
                                                    en: "Yishun Central"
                                                  },
                                                  rlscode: "67"
                                                },
                                                {
                                                  value: "Mobile",
                                                  title: {
                                                    en: "Mobile"
                                                  },
                                                  rlscode: "68"
                                                },
                                                {
                                                  value: "SingPost HQ",
                                                  title: {
                                                    en: "SingPost HQ"
                                                  },
                                                  rlscode: "69"
                                                },
                                                {
                                                  value:
                                                    "SingPost Staff Discount",
                                                  title: {
                                                    en:
                                                      "SingPost Staff Discount"
                                                  },
                                                  rlscode: "70"
                                                },
                                                {
                                                  value: "Mobile Sales Tower",
                                                  title: {
                                                    en: "Mobile Sales Tower"
                                                  },
                                                  rlscode: "71"
                                                },
                                                {
                                                  value: "City Square",
                                                  title: {
                                                    en: "City Square"
                                                  },
                                                  rlscode: "72"
                                                }
                                              ]
                                            },
                                            {
                                              type: "TEXT",
                                              id: "bankRefId",
                                              title: "Referrer ID",
                                              subType: "number_leading_zero",
                                              mandatory: true,
                                              min: 4,
                                              max: 4,
                                              detailSeq: 2
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Details of Proposer",
                                      type: "BLOCK",
                                      detailSeq: 2,
                                      items: [
                                        {
                                          type: "qtitle",
                                          title: "Proposer is a Life Assured: ",
                                          subTitle: "Yes",
                                          trigger: {
                                            id: "extra/isPhSameAsLa",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          type: "qtitle",
                                          title: "Proposer is a Life Assured: ",
                                          subTitle: "No",
                                          trigger: {
                                            id: "extra/isPhSameAsLa",
                                            type: "showIfNotEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "READONLY",
                                              id: "lastName",
                                              emptyValue: "-",
                                              title: "Surname (as shown in ID)",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "firstName",
                                              emptyValue: "-",
                                              title:
                                                "Given Name (as shown in ID)",
                                              detailSeq: 2
                                            },
                                            {
                                              type: "READONLY",
                                              id: "othName",
                                              emptyValue: "-",
                                              title: "English or other name",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "hanyuPinyinName",
                                              emptyValue: "-",
                                              title: "Han Yu Pin Yin Name",
                                              detailSeq: 2
                                            },
                                            {
                                              type: "READONLY",
                                              id: "fullName",
                                              title: "Name",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "nationality",
                                              title: "Nationality",
                                              detailSeq: 2,
                                              options: "nationality",
                                              validation:
                                                "function(i,v,rv,vrv){if(v[i.id]==='N1'){v.prStatus='Y';}}"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "prStatus",
                                              title: "Singapore PR Status",
                                              detailSeq: 1,
                                              options: [
                                                {
                                                  title: {
                                                    en: "Yes",
                                                    "zh-Hant": "æ˜¯"
                                                  },
                                                  value: "Y"
                                                },
                                                {
                                                  title: {
                                                    en: "No",
                                                    "zh-Hant": "å¦"
                                                  },
                                                  value: "N"
                                                }
                                              ],
                                              trigger: {
                                                id: "nationality",
                                                type: "showIfNotEqual",
                                                value: "N1"
                                              }
                                            },
                                            {
                                              id: "idDocType",
                                              title: {
                                                en: "ID Document Type"
                                              },
                                              trigger: [
                                                {
                                                  id: "idDocType",
                                                  type: "showIfNotEqual",
                                                  value: "other"
                                                },
                                                {
                                                  id: "idDocType",
                                                  type: "notEmpty"
                                                }
                                              ],
                                              type: "READONLY",
                                              value: "nric",
                                              validation:
                                                "function(i,v,rv,vrv){if(v.nationality=='N1' || v.prstatus=='Y'){i.disabled=true;v[i.id]='nric';}else{i.disabled=false}}",
                                              options: [
                                                {
                                                  title: {
                                                    en: "Birth Certificate No."
                                                  },
                                                  value: "bcNo"
                                                },
                                                {
                                                  title: {
                                                    en: "FIN No."
                                                  },
                                                  value: "fin"
                                                },
                                                {
                                                  title: {
                                                    en: "NRIC"
                                                  },
                                                  value: "nric"
                                                },
                                                {
                                                  title: {
                                                    en: "Passport"
                                                  },
                                                  value: "passport"
                                                },
                                                {
                                                  title: {
                                                    en: "Others"
                                                  },
                                                  isOther: true,
                                                  max: 30,
                                                  value: "other"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "idDocTypeOther",
                                              title: "ID Type Name",
                                              trigger: {
                                                id: "idDocType",
                                                type: "showIfEqual",
                                                value: "other"
                                              }
                                            },
                                            {
                                              type: "READONLY",
                                              id: "idCardNo",
                                              title: "ID Number",
                                              detailSeq: 2
                                            },
                                            {
                                              id: "gender",
                                              title: {
                                                en: "Gender",
                                                "zh-Hant": "æ€§åˆ¥"
                                              },
                                              type: "READONLY",
                                              options: [
                                                {
                                                  title: {
                                                    en: "Male",
                                                    "zh-Hant": "ç”·"
                                                  },
                                                  value: "M"
                                                },
                                                {
                                                  title: {
                                                    en: "Female",
                                                    "zh-Hant": "å¥³"
                                                  },
                                                  value: "F"
                                                }
                                              ],
                                              condition: "existing"
                                            },
                                            {
                                              id: "dob",
                                              title: {
                                                en: "Date of Birth(DD/MM/YYYY)",
                                                "zh-Hant":
                                                  "å‡ºç”Ÿæ—¥æœŸ(DD/MM/YYYY)"
                                              },
                                              type: "READONLY",
                                              subType: "date",
                                              min: 100,
                                              max: 0,
                                              condition: "existing"
                                            },
                                            {
                                              id: "marital",
                                              title: {
                                                en: "Marital status",
                                                "zh-Hant": "å©šå§»ç‹€æ³"
                                              },
                                              type: "READONLY",
                                              options: "marital"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "email",
                                              title: "Email",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "HBOX",
                                              detailSeq: 2,
                                              title: "Mobile",
                                              trigger: {
                                                id: "mobileNo",
                                                type: "notEmpty"
                                              },
                                              items: [
                                                {
                                                  id: "mobileCountryCode",
                                                  type: "READONLY",
                                                  subType: "countryCode",
                                                  style:
                                                    "width:35px !important; margin-right:0 !important",
                                                  placeholder: {
                                                    en: "Country code of Mobile"
                                                  },
                                                  value: "+65",
                                                  options: "countryTelCode"
                                                },
                                                {
                                                  id: "mobileNo",
                                                  placeholder: {
                                                    en: "Mobile No."
                                                  },
                                                  max: 15,
                                                  type: "READONLY",
                                                  subType: "phone"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "Divider"
                                    },
                                    {
                                      title: "Professional Details",
                                      type: "BLOCK",
                                      detailSeq: 2,
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "TEXT",
                                              id: "organization",
                                              title:
                                                "Name of Employer/Business/School",
                                              populate: {
                                                module: "client"
                                              },
                                              max: 50,
                                              detailSeq: 1,
                                              mandatoryTrigger: {
                                                id: "occupation",
                                                value: [
                                                  "O675",
                                                  "O674",
                                                  "O1132",
                                                  "O1450"
                                                ],
                                                type: "trueIfNotContains"
                                              }
                                            },
                                            {
                                              type: "PICKER",
                                              subType: "limit",
                                              id: "organizationCountry",
                                              populate: {
                                                module: "client"
                                              },
                                              title:
                                                "Country of Employer/Business/School",
                                              mandatory: true,
                                              detailSeq: 2,
                                              options: "residency"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "pass",
                                              title: "Type of Pass",
                                              detailSeq: 1,
                                              options: "pass",
                                              triggerType: "and",
                                              trigger: [
                                                {
                                                  id: "nationality",
                                                  type: "showIfNotEqual",
                                                  value: "N1"
                                                },
                                                {
                                                  id: "prStatus",
                                                  type: "showIfEqual",
                                                  value: "N"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "passOther",
                                              title: "Type of pass (Other)",
                                              detailSeq: 4,
                                              trigger: {
                                                id: "pass",
                                                type: "showIfEqual",
                                                value: "o"
                                              }
                                            },
                                            {
                                              type: "DATEPICKER",
                                              id: "passExpDate",
                                              max: 100,
                                              min: 0,
                                              title:
                                                "Pass Expiry Date (DD/MM/YYYY)",
                                              mandatory: true,
                                              detailSeq: 2,
                                              populate: {
                                                module: "client"
                                              },
                                              triggerType: "and",
                                              trigger: [
                                                {
                                                  id: "nationality",
                                                  type: "showIfNotEqual",
                                                  value: "N1"
                                                },
                                                {
                                                  id: "prStatus",
                                                  type: "showIfEqual",
                                                  value: "N"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "industry",
                                              title: "Industry",
                                              subType: "picker",
                                              detailSeq: 2,
                                              options: "industry"
                                            },
                                            {
                                              id: "occupation",
                                              title: "Occupation",
                                              subType: "picker",
                                              type: "READONLY",
                                              options: "occupation"
                                            },
                                            {
                                              id: "occupationOther",
                                              title: "Occupation (Other)",
                                              type: "READONLY",
                                              trigger: {
                                                id: "occupation",
                                                type: "showIfEqual",
                                                value: "O921"
                                              }
                                            },
                                            {
                                              type: "READONLY",
                                              id: "allowance",
                                              subType: "currency",
                                              title:
                                                "Monthly Income/Allowance SGD",
                                              detailSeq: 6,
                                              min: 0,
                                              max: 9999999999,
                                              showZeroValue: true,
                                              ccy: "SGD"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Residential Address",
                                      type: "BLOCK",
                                      detailSeq: 3,
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              id: "addrCountry",
                                              title: "Country",
                                              value: "R2",
                                              options: "residency",
                                              type: "READONLY"
                                            },
                                            {
                                              id: "postalCode",
                                              subType: "postalcode",
                                              countryAllowed: "addrCountry",
                                              mapping: {
                                                bldgNo: "addrBlock,12",
                                                bldgName: "addrEstate,40",
                                                streetName: "addrStreet,24"
                                              },
                                              title: {
                                                en: "Postal Code"
                                              },
                                              type: "text",
                                              mandatory: true,
                                              max: 10
                                            },
                                            {
                                              type: "TEXT",
                                              id: "addrBlock",
                                              title: "Block/House Number",
                                              mandatory: true,
                                              detailSeq: 1,
                                              max: 12,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              type: "TEXT",
                                              id: "addrStreet",
                                              title: "Street/Road Name",
                                              mandatory: true,
                                              detailSeq: 2,
                                              max: 24
                                            },
                                            {
                                              id: "unitNum",
                                              title: {
                                                en: "Unit Number"
                                              },
                                              emptyValue: "-",
                                              type: "text",
                                              subType: "unit_no",
                                              max: 13,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "addrEstate",
                                              title: "Building/ Estate Name",
                                              emptyValue: "-",
                                              type: "text",
                                              max: 40,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "addrCity",
                                              disablePicker: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfNotContains"
                                              },
                                              disableTrigger: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfContains"
                                              },
                                              emptyValue: "-",
                                              title: "City/State",
                                              max: 12,
                                              type: "picker",
                                              options: "city"
                                            },
                                            {
                                              id: "otherResidenceCity",
                                              title: {
                                                en: "Others"
                                              },
                                              disableTrigger: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfContains"
                                              },
                                              emptyValue: "-",
                                              trigger: {
                                                type: "dynamicCondition",
                                                id: "addrCountry,addrCity",
                                                value:
                                                  "(!_.isUndefined(#1) && (#1 ==='R52' ))||((!_.isUndefined(#1)  && !_.isUndefined(#2) && (#2 ==='T110' || #2 ==='T120' ) ))"
                                              },
                                              type: "text",
                                              subType: "text",
                                              maxLength: 50
                                            }
                                          ]
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "isSameAddr",
                                          mandatory: true,
                                          title:
                                            "Is your Residential Address same as mailing address?",
                                          horizontal: true,
                                          value: "Y",
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "Divider"
                                    },
                                    {
                                      title: "Mailing Address",
                                      type: "BLOCK",
                                      detailSeq: 3,
                                      trigger: {
                                        id: "isSameAddr",
                                        type: "showIfEqual",
                                        value: "N"
                                      },
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              id: "mAddrCountry",
                                              mandatory: true,
                                              title: {
                                                en: "Country"
                                              },
                                              value: "R2",
                                              options: "residency",
                                              type: "picker",
                                              subType: "limit"
                                            },
                                            {
                                              id: "mPostalCode",
                                              mandatory: true,
                                              subType: "postalcode",
                                              countryAllowed: "mAddrCountry",
                                              mapping: {
                                                bldgNo: "mAddrBlock,12",
                                                bldgName: "mAddrEstate,40",
                                                streetName: "mAddrStreet,24"
                                              },
                                              title: {
                                                en: "Postal Code"
                                              },
                                              type: "text",
                                              max: 10
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 2,
                                          items: [
                                            {
                                              type: "TEXT",
                                              mandatory: true,
                                              id: "mAddrBlock",
                                              title: "Block/House Number",
                                              detailSeq: 1,
                                              max: 12,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              type: "TEXT",
                                              mandatory: true,
                                              id: "mAddrStreet",
                                              title: "Street/Road Name",
                                              detailSeq: 2,
                                              max: 24
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 3,
                                          items: [
                                            {
                                              id: "mAddrnitNum",
                                              title: {
                                                en: "Unit Number"
                                              },
                                              type: "text",
                                              subType: "unit_no",
                                              max: 13,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "mAddrEstate",
                                              title: "Building/ Estate Name",
                                              type: "text",
                                              max: 40,
                                              populate: {
                                                module: "client"
                                              }
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 3,
                                          items: [
                                            {
                                              id: "mAddrCity",
                                              max: 12,
                                              title: "City/State",
                                              type: "text"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      title: {
                        en: "Residency & Insurance Info"
                      },
                      type: "menuItem",
                      key: "menu_residency",
                      detailSeq: 1,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              type: "tab",
                              items: [
                                {
                                  title: "Residency Questions",
                                  id: "residency",
                                  type: "BLOCK",
                                  detailSeq: 1,
                                  trigger: [
                                    {
                                      id: "personalInfo/nationality",
                                      type: "showIfEqual",
                                      value: "N1"
                                    },
                                    {
                                      id: "personalInfo/prStatus",
                                      type: "showIfEqual",
                                      value: "Y"
                                    },
                                    {
                                      id: "personalInfo/pass",
                                      type: "showIfExist",
                                      value: "ep,sp,wp,s,dp,svp,o,lp"
                                    }
                                  ],
                                  items: [
                                    {
                                      type: "BLOCK",
                                      trigger: {
                                        id: "personalInfo/nationality",
                                        type: "showIfEqual",
                                        value: "N1"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Singapore Citizen",
                                          id: "tSingaporeC"
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F1",
                                          mandatory: true,
                                          align: "left",
                                          title: "I am currently:",
                                          options: [
                                            {
                                              value: "resided",
                                              title: "i)  residing in Singapore"
                                            },
                                            {
                                              value: "outside",
                                              needToHighlight: true,
                                              title:
                                                "ii) residing outside Singapore: I have resided outside Singapore continuously for {{less than 5 years}} preceding the date of proposal."
                                            },
                                            {
                                              value: "outside5",
                                              needToHighlight: true,
                                              title:
                                                "iii) residing outside Singapore: I have resided outside Singapore continuously for {{5 years or more}} preceding the date of proposal. "
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      trigger: {
                                        id:
                                          "@personalInfo.nationality,@personalInfo.prStatus,@personalInfo.pass",
                                        type: "dynamicCondition",
                                        value:
                                          "#1!='N1' && (#2=='Y' || #3 == 'ep' || #3=='wp' || #3=='sp')"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/prStatus",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          title: "Singapore PR",
                                          id: "tSiPrA"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "ep"
                                          },
                                          title: "Employment Pass",
                                          id: "tSiPrB"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "wp"
                                          },
                                          title: "Work Permit",
                                          id: "tSiPrC_"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "sp"
                                          },
                                          title: "S Pass",
                                          id: "tSiPrD"
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F2",
                                          mandatory: true,
                                          title:
                                            "1. Have you resided in Singapore for at least 183 days in the last 12 months preceding the date of proposal?",
                                          value: "",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F3",
                                          mandatory: true,
                                          title:
                                            "2. Do you intend to stay in Singapore on a permanent basis?",
                                          value: "",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      trigger: {
                                        id: "personalInfo/pass",
                                        type: "showIfExist",
                                        value: "dp,s,lp"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Dependent Pass",
                                          id: "tSiPrCde",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "dp"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Student Pass",
                                          id: "tSiPrCStS",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "s"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Long Term Visit Pass",
                                          id: "tSiPrCStL",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "lp"
                                          }
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F4",
                                          title:
                                            "1.Â  Have you resided in Singapore for at least 90 days in the last 12 months preceding the date of proposal?",
                                          mandatory: true,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      trigger: [
                                        {
                                          id: "personalInfo/pass",
                                          type: "showIfExist",
                                          value: "svp,o"
                                        }
                                      ],
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Others",
                                          id: "tSiPrD",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "o"
                                          },
                                          replaceTitle: {
                                            id: "personalInfo/passOther"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Social Visit Pass",
                                          id: "tSiPrDD",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "svp"
                                          }
                                        },
                                        {
                                          type: "CheckBox",
                                          id: "EAPP-P6-F5",
                                          disabled: true,
                                          value: "Y",
                                          title: "I hold Type of Pass above."
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  title: "Foreigner Questions",
                                  id: "foreigner",
                                  type: "BLOCK",
                                  detailSeq: 2,
                                  trigger: [
                                    {
                                      id: "residency/EAPP-P6-F2",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "residency/EAPP-P6-F3",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "residency/EAPP-P6-F4",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "personalInfo/pass",
                                      type: "showIfExist",
                                      value: "dp,s,lp,svp,o"
                                    }
                                  ],
                                  items: [
                                    {
                                      type: "QTITLE",
                                      style: {
                                        marginBottom: "-20px"
                                      },
                                      title:
                                        "When did you first arrive in Singapore (excluding holidays of less than 3 months)?"
                                    },
                                    {
                                      type: "DATEPICKER",
                                      subType: "date",
                                      max: 0,
                                      min: 100,
                                      id: "faDate",
                                      mandatory: true,
                                      dateFormat: "mm/yyyy",
                                      style: {
                                        width: "50%",
                                        margin: 0
                                      }
                                    },
                                    {
                                      type: "QTITLE",
                                      id: "familyResD",
                                      style: {
                                        marginTop: "25px"
                                      },
                                      title:
                                        "Where do your immediate family members currently reside?"
                                    },
                                    {
                                      type: "HBOX",
                                      items: [
                                        {
                                          title: "Country",
                                          type: "PICKER",
                                          subType: "limit",
                                          id: "familyResCountry",
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          title: "City",
                                          type: "TEXT",
                                          id: "familyResCity",
                                          mandatory: true,
                                          max: 50
                                        }
                                      ]
                                    },
                                    {
                                      type: "QTITLE",
                                      id: "5yearDesc",
                                      style: {
                                        paddingTop: "20px",
                                        marginBottom: "5px"
                                      },
                                      title:
                                        "Please provide details of your residence over the last five years:"
                                    },
                                    {
                                      type: "table",
                                      id: "reside5yrs",
                                      mandatory: true,
                                      allowEdit: true,
                                      hasBorder: true,
                                      max: 5,
                                      items: [
                                        {
                                          type: "DATEPICKER",
                                          mandatory: true,
                                          width: "10%",
                                          title: "From (mm/yyyy)",
                                          id: "res5Frm",
                                          max: 0,
                                          min: 5,
                                          dateFormat: "mm/yyyy"
                                        },
                                        {
                                          type: "DATEPICKER",
                                          mandatory: true,
                                          width: "10%",
                                          title: "To (mm/yyyy)",
                                          id: "res5To",
                                          max: 0,
                                          min: 5,
                                          dateFormat: "mm/yyyy",
                                          validation: {
                                            id: "res5Frm",
                                            type: "month_greater"
                                          }
                                        },
                                        {
                                          id: "res5Country",
                                          type: "PICKER",
                                          subType: "limit",
                                          title: "Country of Residence",
                                          detailSeq: 2,
                                          width: "20%",
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          id: "res5City",
                                          type: "TEXT",
                                          title: "City of Residence",
                                          detailSeq: 3,
                                          width: "20%",
                                          mandatory: true,
                                          max: 50
                                        },
                                        {
                                          type: "table-block-t",
                                          title: "Reason",
                                          presentation:
                                            "permanent$work$study$cob$family$immigrate$visit$other",
                                          width: "30%",
                                          mandatory: true,
                                          key: "res_reason_t",
                                          items: [
                                            {
                                              id: "permanent",
                                              type: "checkbox",
                                              title: "Permanent Residence"
                                            },
                                            {
                                              id: "work",
                                              type: "checkbox",
                                              title: "Business / Work"
                                            },
                                            {
                                              id: "study",
                                              type: "checkbox",
                                              title: "Study"
                                            },
                                            {
                                              id: "cob",
                                              type: "checkbox",
                                              title: "Country of birth"
                                            },
                                            {
                                              id: "family",
                                              type: "checkbox",
                                              title:
                                                "Place where family members or relatives residing"
                                            },
                                            {
                                              id: "immigrate",
                                              type: "checkbox",
                                              title: "Immigration"
                                            },
                                            {
                                              id: "visit",
                                              type: "checkbox",
                                              title:
                                                "Visit family/relatives/friends"
                                            },
                                            {
                                              id: "other",
                                              type: "checkbox",
                                              title: "Others"
                                            },
                                            {
                                              type: "TEXT",
                                              title: "Please Specify",
                                              id: "resultOther",
                                              mandatory: true,
                                              max: 50,
                                              detailSeq: 4,
                                              trigger: {
                                                id: "other",
                                                type: "showIfEqual",
                                                value: "Y"
                                              }
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "QRADIOGROUP",
                                      id: "hasTravelPlans",
                                      mandatory: true,
                                      title:
                                        "In the next 2 years, do you expect to go abroad (outside the current country of residence) for any future residency or travel plans? ",
                                      horizontal: true,
                                      align: "left",
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      type: "table",
                                      max: 5,
                                      id: "travelPlans",
                                      allowEdit: true,
                                      hasBorder: true,
                                      mandatory: true,
                                      trigger: {
                                        id: "hasTravelPlans",
                                        type: "showIfEqual",
                                        value: "Y"
                                      },
                                      items: [
                                        {
                                          id: "travlCountry",
                                          title: "Country",
                                          type: "PICKER",
                                          subType: "limit",
                                          detailSeq: 1,
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          id: "travlCity",
                                          type: "TEXT",
                                          title: "City",
                                          detailSeq: 2,
                                          mandatory: true,
                                          max: 50
                                        },
                                        {
                                          id: "travlCityDrn",
                                          type: "TEXT",
                                          subType: "number",
                                          title: "Duration of each Stay (Days)",
                                          detailSeq: 3,
                                          mandatory: true,
                                          max: 99999
                                        },
                                        {
                                          id: "travlCityFrq",
                                          type: "TEXT",
                                          subType: "number",
                                          title: "Frequency (per year)",
                                          detailSeq: 4,
                                          mandatory: true,
                                          max: 99999
                                        },
                                        {
                                          type: "table-block-t",
                                          key: "res_reason_t",
                                          title: "Reason",
                                          presentation:
                                            "permanent$work$study$cob$family$immigrate$visit$other",
                                          width: "30%",
                                          mandatory: true,
                                          items: [
                                            {
                                              id: "permanent",
                                              type: "checkbox",
                                              title: "Permanent Residence"
                                            },
                                            {
                                              id: "work",
                                              type: "checkbox",
                                              title: "Business / Work"
                                            },
                                            {
                                              id: "study",
                                              type: "checkbox",
                                              title: "Study"
                                            },
                                            {
                                              id: "cob",
                                              type: "checkbox",
                                              title: "Country of birth"
                                            },
                                            {
                                              id: "family",
                                              type: "checkbox",
                                              title:
                                                "Place where family members or relatives residing"
                                            },
                                            {
                                              id: "immigrate",
                                              type: "checkbox",
                                              title: "Immigration"
                                            },
                                            {
                                              id: "visit",
                                              type: "checkbox",
                                              title:
                                                "Visit family/relatives/friends"
                                            },
                                            {
                                              id: "other",
                                              type: "checkbox",
                                              title: "Others"
                                            },
                                            {
                                              type: "TEXT",
                                              title: "Please Specify",
                                              id: "resultOther",
                                              max: 50,
                                              detailSeq: 4,
                                              trigger: {
                                                id: "other",
                                                type: "showIfEqual",
                                                value: "Y"
                                              }
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  type: "BLOCK",
                                  title:
                                    "Details of Previous and Concurrent Policies",
                                  id: "policies",
                                  items: [
                                    {
                                      id: "havAccPlans",
                                      mandatory: true,
                                      title:
                                        "1. Do you have any existing personal accident insurance policies?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      detailSeq: 1,
                                      align: "left",
                                      disableTrigger: {
                                        id: "@extra/channel",
                                        value: "AGENCY",
                                        type: "trueIfEqual"
                                      },
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      id: "havPndinApp",
                                      mandatory: true,
                                      title:
                                        "2. Other than this current application, do you have any other pending or concurrent insurance applications with AXA and/or other companies?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      align: "left",
                                      detailSeq: 2,
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      id: "isProslReplace",
                                      mandatory: true,
                                      title:
                                        "3. Is this proposal to replace or intended to replace (in part or full) any existing or recently terminated insurance policy, or other investment product from AXA Insurance Pte Ltd or other Financial Institution?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      align: "left",
                                      detailSeq: 3,
                                      disableTrigger: {
                                        id: "@extra/channel",
                                        value: "AGENCY",
                                        type: "trueIfEqual"
                                      },
                                      trigger: {
                                        id: "extra/isPhSameAsLa",
                                        value: "Y",
                                        type: "showIfEqual"
                                      },
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      type: "PARAGRAPH",
                                      id: "pWarning",
                                      title: "WARNING NOTE:",
                                      paragraphType: "warningWithNote",
                                      align: "left",
                                      content:
                                        "<p>It is usually disadvantageous to replace an existing life insurance policy or other investment product with a new one. Some of the factors to consider:</p><p>1. You may suffer a penalty for terminating the original policy or other investment product.</p><p>2. You may incur transaction costs without gaining any real benefit from replacing the policy or other investment product.</p><p>3. You may not be insurable on standard terms or may have to pay a higher premium in view of higher age or the financial benefits accumulated over the years may be lost.</p><p>In your own interest, we would advise that you consult your own financial consultant before making a final decision. Hear from both sides and make a careful comparison to ensure that you are making a decision that is in your best interest.</p>",
                                      detailSeq: 4,
                                      trigger: {
                                        id: "isProslReplace",
                                        value: "Y",
                                        type: "showIfEqual"
                                      }
                                    },
                                    {
                                      id: "saTable",
                                      type: "saTable",
                                      presentation:
                                        "havAccPlans$havPndinApp$isProslReplace"
                                    },
                                    {
                                      trigger: {
                                        id: "@extra/channel",
                                        value: "FA",
                                        type: "showIfNotEqual"
                                      },
                                      type: "goFNA"
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "menuSection",
                  detailSeq: 1,
                  items: [
                    {
                      title: {
                        en: "Plan Details"
                      },
                      type: "menuItem",
                      skipCheck: true,
                      key: "menu_plan",
                      detailSeq: 0,
                      items: [
                        {
                          type: "BLOCK",
                          id: "planDetails",
                          title: "",
                          items: [
                            {
                              type: "block",
                              style: {
                                paddingBottom: "30px",
                                borderBottom: "1px solid #cac4c4"
                              },
                              items: [
                                {
                                  type: "heading",
                                  id: "planTypeName",
                                  detailSeq: 1
                                },
                                {
                                  type: "table",
                                  no1stItemLeftPadding: true,
                                  equalWidthCol: true,
                                  id: "planList",
                                  allowEdit: false,
                                  style: {
                                    width: "100%"
                                  },
                                  items: [
                                    {
                                      type: "HBOX",
                                      title: "Basic Plan/ Rider",
                                      presentation: "covName(saPostfix)",
                                      items: [
                                        {
                                          type: "TEXT",
                                          id: "covName"
                                        },
                                        {
                                          type: "TEXT",
                                          id: "saPostfix"
                                        }
                                      ]
                                    },
                                    {
                                      type: "TEXT",
                                      id: "sumInsured",
                                      title: "Sum Assured/ Benefits",
                                      subType: "currency",
                                      decimal: 0,
                                      right: false
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              type: "block",
                              title: "Other Plan Details",
                              items: [
                                {
                                  type: "HBOX",
                                  items: [
                                    {
                                      id: "ccy",
                                      title: "Policy Currency",
                                      type: "READONLY",
                                      options: [
                                        {
                                          value: "ALL",
                                          title: {
                                            en: "All Currencies",
                                            "zh-hant": "æ‰€æœ‰è²¨å¹£"
                                          },
                                          exRate: "1",
                                          seq: "1"
                                        },
                                        {
                                          value: "AUD",
                                          title: {
                                            en: "Australian Dollars",
                                            "zh-hant": "æ¾³å…ƒ"
                                          },
                                          shortTitle: "AUD",
                                          exRate: "1.2",
                                          seq: "2"
                                        },
                                        {
                                          value: "CAD",
                                          title: {
                                            en: "Canadian Dollars",
                                            "zh-hant": "åŠ å¹£"
                                          },
                                          exRate: "1.2",
                                          seq: "3"
                                        },
                                        {
                                          value: "CHF",
                                          title: {
                                            en: "Swiss Francs",
                                            "zh-hant": "ç‘žå£«æ³•éƒŽ"
                                          },
                                          exRate: "1.2",
                                          seq: "4"
                                        },
                                        {
                                          value: "EUR",
                                          title: {
                                            en: "Euros",
                                            "zh-hant": "æ­å…ƒ"
                                          },
                                          exRate: "1",
                                          seq: "5"
                                        },
                                        {
                                          value: "GBP",
                                          title: {
                                            en: "British Pounds",
                                            "zh-hant": "è‹±éŽŠ"
                                          },
                                          exRate: "0.64",
                                          seq: "6"
                                        },
                                        {
                                          value: "HKD",
                                          title: {
                                            en: "Hong Kong Dollars",
                                            "zh-hant": "æ¸¯å¹£"
                                          },
                                          shortTitle: "HK$",
                                          exRate: "8",
                                          seq: "7"
                                        },
                                        {
                                          value: "NZD",
                                          title: {
                                            en: "New Zealand Dollars",
                                            "zh-hant": "ç´å…ƒ"
                                          },
                                          exRate: "1.8",
                                          seq: "8"
                                        },
                                        {
                                          value: "USD",
                                          title: {
                                            en: "USD",
                                            "zh-hant": "ç¾Žé‡‘"
                                          },
                                          exRate: "1",
                                          seq: "9"
                                        },
                                        {
                                          value: "SGD",
                                          title: {
                                            en: "SGD",
                                            "zh-hant": "æ–°åŠ å¡å¹£"
                                          },
                                          exRate: "1.6",
                                          seq: "10"
                                        }
                                      ]
                                    },
                                    {
                                      id: "covClass",
                                      title: "Occupation Class",
                                      type: "READONLY"
                                    }
                                  ]
                                },
                                {
                                  type: "HBOX",
                                  items: [
                                    {
                                      id: "paymentMode",
                                      title: "Payment Mode",
                                      type: "READONLY",
                                      options: [
                                        {
                                          value: "M",
                                          title: "Monthly"
                                        },
                                        {
                                          value: "Q",
                                          title: "Quarterly"
                                        },
                                        {
                                          value: "S",
                                          title: "Semi-Annual"
                                        },
                                        {
                                          value: "A",
                                          title: "Annual"
                                        },
                                        {
                                          value: "L",
                                          title: "Single Premium"
                                        }
                                      ]
                                    },
                                    {
                                      id: "premium",
                                      title:
                                        "Total Premium Amount (inclusive of GST)",
                                      type: "READONLY",
                                      subType: "currency",
                                      decimal: 2
                                    }
                                  ]
                                }
                              ]
                            }
                          ],
                          subType: "proposer"
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "menuSection",
                  detailSeq: 2,
                  items: [
                    {
                      title: {
                        en: "Declaration"
                      },
                      type: "menuItem",
                      key: "menu_declaration",
                      detailSeq: 0,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              type: "tab",
                              items: [
                                {
                                  type: "block",
                                  id: "declaration",
                                  items: [
                                    {
                                      type: "BLOCK",
                                      title: "Bankruptcy",
                                      detailSeq: 1,
                                      items: [
                                        {
                                          type: "QRADIOGROUP",
                                          id: "BANKRUPTCY01",
                                          mandatory: true,
                                          title:
                                            "Are you an undischarged bankrupt? ",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Trusted Individual",
                                      triggerType: "and",
                                      trigger: [
                                        {
                                          id: "trustedIndividuals/firstName",
                                          type: "notEmpty"
                                        },
                                        {
                                          id: "extra/channel",
                                          type: "showIfNotEqual",
                                          value: "FA"
                                        }
                                      ],
                                      detailSeq: 4,
                                      items: [
                                        {
                                          type: "hbox",
                                          items: [
                                            {
                                              id: "TRUSTED_IND01",
                                              mandatory: true,
                                              label: "title",
                                              title: "Language",
                                              type: "picker",
                                              options: [
                                                {
                                                  value: "en",
                                                  title: "English"
                                                },
                                                {
                                                  value: "mandarin",
                                                  title: "Mandarin"
                                                },
                                                {
                                                  value: "malay",
                                                  title: "Malay"
                                                },
                                                {
                                                  value: "tamil",
                                                  title: "Tamil"
                                                },
                                                {
                                                  value: "other",
                                                  title: "Other"
                                                }
                                              ]
                                            },
                                            {
                                              id: "TRUSTED_IND02",
                                              mandatory: true,
                                              type: "text",
                                              placeholder: {
                                                en:
                                                  "Please specify the Language if other"
                                              },
                                              trigger: {
                                                id: "TRUSTED_IND01",
                                                type: "showIfEqual",
                                                value: "other"
                                              },
                                              max: 15
                                            }
                                          ]
                                        },
                                        {
                                          id: "TRUSTED_IND04",
                                          mandatory: true,
                                          type: "checkbox",
                                          trigger: {
                                            id: "TRUSTED_IND01",
                                            type: "showIfEqual",
                                            value: "other"
                                          },
                                          hiddenField: {
                                            type: "skip"
                                          },
                                          content:
                                            "I, <b><trustedIndividuals/firstName> <trustedIndividuals/lastName></b> of NRIC/Passport: <b><trustedIndividuals/idCardNo></b> acknowledge that I have fulfilled the definition of a Trusted Individual and I am a Trusted Individual to <b><@personalInfo/firstName> <@personalInfo/lastName></b>. I confirm that the Financial Consultant has conversed in <b><TRUSTED_IND02></b>  to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to <b><@personalInfo/firstName> <@personalInfo/lastName></b>."
                                        },
                                        {
                                          id: "TRUSTED_IND04",
                                          mandatory: true,
                                          type: "checkbox",
                                          trigger: {
                                            id: "TRUSTED_IND01",
                                            type: "showIfNotEqual",
                                            value: "other"
                                          },
                                          hiddenField: {
                                            type: "skip"
                                          },
                                          content:
                                            "I, <b><trustedIndividuals/firstName> <trustedIndividuals/lastName></b> of NRIC/Passport: <b><trustedIndividuals/idCardNo></b> acknowledge that I have fulfilled the definition of a Trusted Individual and I am a Trusted Individual to <b><@personalInfo/firstName> <@personalInfo/lastName></b>. I confirm that the Financial Consultant has conversed in <b><TRUSTED_IND01></b>  to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to <b><@personalInfo/firstName> <@personalInfo/lastName></b>."
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "e-Policy",
                                      detailSeq: 5,
                                      trigger: {
                                        id: "removeThisTriggerInRelease2",
                                        type: "showIfEqual",
                                        value: "Y"
                                      },
                                      items: [
                                        {
                                          id: "EPOLICY01",
                                          type: "checkbox",
                                          title:
                                            "I would also like to receive a copy of my policy documents by mail, aside from my e-policy in MyAXA",
                                          value: "Y"
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Roadshow Declaration",
                                      detailSeq: 6,
                                      trigger: {
                                        id: "extra/channel",
                                        type: "showIfNotEqual",
                                        value: "FA"
                                      },
                                      items: [
                                        {
                                          id: "ROADSHOW01",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          detailSeq: 1,
                                          title:
                                            "Is this case closed at the venue of the roadshow/canvassing or seminar event? ",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ],
                                          trigger: {
                                            id: "extra/channel",
                                            type: "showIfNotEqual",
                                            value: "FA"
                                          }
                                        },
                                        {
                                          id: "ROADSHOW04",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          detailSeq: 1,
                                          title: "",
                                          align: "left",
                                          noPadding: true,
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          options: [
                                            {
                                              value: "roadshow",
                                              title: "Roadshow"
                                            },
                                            {
                                              value: "canvassing",
                                              title: "Canvassing"
                                            },
                                            {
                                              value: "seminar",
                                              title: "Seminar event"
                                            }
                                          ]
                                        },
                                        {
                                          id: "ROADSHOW02",
                                          mandatory: true,
                                          type: "datepicker",
                                          max: 0,
                                          min: 100,
                                          detailSeq: 2,
                                          title: "Date of event",
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          id: "ROADSHOW03",
                                          mandatory: true,
                                          type: "text",
                                          max: 50,
                                          detailSeq: 3,
                                          title:
                                            "Venue where case was closed  ",
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Source of Funds",
                                      detailSeq: 7,
                                      items: [
                                        {
                                          type: "block",
                                          title: "1. Source of Payment",
                                          titleType: "normal",
                                          items: [
                                            {
                                              id: "FUND_SRC01",
                                              mandatory: true,
                                              type: "QRADIOGROUP",
                                              detailSeq: 1,
                                              title:
                                                "Please select the origin of the funds used for this application.",
                                              align: "left",
                                              options: [
                                                {
                                                  value: "sg",
                                                  title:
                                                    "Singapore (e.g. Singapore banks/credit cards)"
                                                },
                                                {
                                                  value: "foreign",
                                                  title:
                                                    "Foreign (e.g. overseas banks/credit cards)"
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          type: "block",
                                          title: "2. Payor's Details",
                                          titleType: "normal",
                                          items: [
                                            {
                                              type: "QTITLE",
                                              title:
                                                "Who is paying the insurance premium for this application?"
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "FUND_SRC02",
                                                  mandatory: true,
                                                  type: "picker",
                                                  detailSeq: 2,
                                                  optionsTable: "payors",
                                                  options: [
                                                    {
                                                      title: "Alex 4",
                                                      value: "CP001003-00006"
                                                    },
                                                    {
                                                      title: "Other",
                                                      value: "other"
                                                    }
                                                  ]
                                                }
                                              ]
                                            },
                                            {
                                              type: "block",
                                              detailSeq: 3,
                                              trigger: {
                                                id: "FUND_SRC02",
                                                type: "showIfEqual",
                                                value: "other"
                                              },
                                              items: [
                                                {
                                                  type: "hbox",
                                                  key: "hnamebox",
                                                  detailSeq: 1,
                                                  errorMsg:
                                                    "Surname and Given Name has max 50 characters",
                                                  mandatory: true,
                                                  max: 50,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC03",
                                                      type: "text",
                                                      max: 30,
                                                      validation: {
                                                        id: "FUND_SRC04",
                                                        type: "sum",
                                                        max: 50
                                                      },
                                                      title:
                                                        "Surname of Payor (as shown in ID)"
                                                    },
                                                    {
                                                      id: "FUND_SRC04",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 30,
                                                      validation: {
                                                        id: "FUND_SRC03",
                                                        type: "sum",
                                                        max: 50
                                                      },
                                                      title:
                                                        "Given Name of Payor (as shown in ID)"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 2,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC05",
                                                      type: "text",
                                                      max: 30,
                                                      title:
                                                        "English or other Name of Payor "
                                                    },
                                                    {
                                                      id: "FUND_SRC06",
                                                      type: "text",
                                                      max: 30,
                                                      title:
                                                        "Han Yu Pin Yin Name of Payor"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 3,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC07",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 30,
                                                      title: "Payor's ID No. "
                                                    },
                                                    {
                                                      id: "FUND_SRC08",
                                                      mandatory: true,
                                                      type: "picker",
                                                      max: 30,
                                                      title:
                                                        "Payor's Nationality or Country of Incorporation",
                                                      options: "residency",
                                                      subType: "limit"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "HBOX",
                                                  detailSeq: 4,
                                                  items: [
                                                    {
                                                      type: "picker",
                                                      id: "FUND_SRC09",
                                                      mandatory: true,
                                                      title:
                                                        "Relationship of Payor to Proposer",
                                                      detailSeq: 1,
                                                      options: "relationship"
                                                    },
                                                    {
                                                      type: "text",
                                                      id: "FUND_SRC10",
                                                      mandatory: true,
                                                      maxLength: 20,
                                                      detailSeq: 5,
                                                      title:
                                                        "Other Relationship",
                                                      trigger: {
                                                        id: "FUND_SRC09",
                                                        type: "showIfEqual",
                                                        value: "OTH"
                                                      }
                                                    },
                                                    {
                                                      type: "HBOX",
                                                      detailSeq: 2,
                                                      title: "Contact No.",
                                                      subType: "TEXT",
                                                      items: [
                                                        {
                                                          type: "PICKER",
                                                          id: "FUND_SRC11",
                                                          mandatory: true,
                                                          detailSeq: 1,
                                                          options:
                                                            "countryTelCode",
                                                          value: "+65"
                                                        },
                                                        {
                                                          type: "TEXT",
                                                          id: "FUND_SRC12",
                                                          subType: "number",
                                                          mandatory: true,
                                                          detailSeq: 2,
                                                          max: 999999999999999
                                                        }
                                                      ]
                                                    },
                                                    {
                                                      id: "FUND_SRC20",
                                                      mandatory: true,
                                                      title:
                                                        "Payor's Occupation",
                                                      type: "text",
                                                      max: 100
                                                    },
                                                    {
                                                      id: "FUND_SRC21",
                                                      mandatory: true,
                                                      title:
                                                        "Payor's Annual Income (S$)",
                                                      allowEmpty: true,
                                                      type: "text",
                                                      max: 999999999,
                                                      subType: "currency",
                                                      ccy: "SGD"
                                                    }
                                                  ]
                                                },
                                                {
                                                  id: "FUND_SRC13title",
                                                  title:
                                                    "Payor's Residential/Registered Address",
                                                  type: "QTITLE",
                                                  detailSeq: 7
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 8,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC13",
                                                      mandatory: false,
                                                      type: "PICKER",
                                                      title: "Country",
                                                      options: "residency",
                                                      value: "R2",
                                                      subType: "limit"
                                                    },
                                                    {
                                                      id: "FUND_SRC14",
                                                      mandatory: true,
                                                      type: "text",
                                                      subType: "postalcode",
                                                      max: 10,
                                                      title: "Postal Code",
                                                      countryAllowed:
                                                        "FUND_SRC13",
                                                      mapping: {
                                                        bldgNo: "FUND_SRC15,12",
                                                        bldgName:
                                                          "FUND_SRC18,40",
                                                        streetName:
                                                          "FUND_SRC16,24"
                                                      }
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 9,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC15",
                                                      mandatory: true,
                                                      type: "text",
                                                      title:
                                                        "Block/House Number",
                                                      max: 12
                                                    },
                                                    {
                                                      id: "FUND_SRC16",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 24,
                                                      title: "Street/Road Name"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 10,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC17",
                                                      type: "text",
                                                      title: "Unit Number",
                                                      subType: "unit_no",
                                                      max: 13
                                                    },
                                                    {
                                                      id: "FUND_SRC18",
                                                      type: "text",
                                                      max: 40,
                                                      title:
                                                        "Building/ Estate Name"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 11,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC19",
                                                      type: "text",
                                                      max: 12,
                                                      title: "City/State",
                                                      mandatoryTrigger: {
                                                        id: "FUND_SRC13",
                                                        value: ["R51", "R52"],
                                                        type: "trueIfContains"
                                                      }
                                                    }
                                                  ]
                                                },
                                                {
                                                  id: "FUND_SRC22",
                                                  type: "text",
                                                  detailSeq: 11,
                                                  mandatory: true,
                                                  max: 100,
                                                  title:
                                                    "Reason for payment by third party"
                                                },
                                                {
                                                  id: "FUND_SRC23",
                                                  type: "paragraph",
                                                  detailSeq: 12,
                                                  paragraphType: "warning",
                                                  align: "left",
                                                  content:
                                                    "<p>For Individual 3rd party payor: Please furnish copy of ID and Proof of Address<br/>For Corporate 3rd party payor: Please furnish copy of business profile showing the shareholders and directors.</p> "
                                                }
                                              ]
                                            },
                                            {
                                              type: "BLOCK",
                                              mandatory: true,
                                              key: "FUND_SRC24-DATA",
                                              title: "3. Source of Funds",
                                              titleType: "normal",
                                              detailSeq: 13,
                                              trigger: [
                                                {
                                                  type: "comparePremium",
                                                  id: "extra/premium",
                                                  ccys: [
                                                    {
                                                      ccy: "SGD",
                                                      amount: 20000
                                                    },
                                                    {
                                                      ccy: "AUD",
                                                      amount: 19000
                                                    },
                                                    {
                                                      ccy: "EUR",
                                                      amount: 13000
                                                    },
                                                    {
                                                      ccy: "GBP",
                                                      amount: 9600
                                                    },
                                                    {
                                                      ccy: "USD",
                                                      amount: 14000
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "showIfEqual",
                                                  id: "FUND_SRC01",
                                                  value: "foreign"
                                                }
                                              ],
                                              items: [
                                                {
                                                  type: "QTITLE",
                                                  normal: true,
                                                  title:
                                                    "Please confirm the origin of the Payor's funds/assets used for this investment. (You may tick more than 1 option)"
                                                },
                                                {
                                                  title: "Salary/Commission",
                                                  id: "FUND_SRC25",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "FUND_SRC26"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from sale of assets ",
                                                  id: "FUND_SRC27"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "FUND_SRC28"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from maturity/surrender of a policy",
                                                  id: "FUND_SRC29"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "FUND_SRC30"
                                                },
                                                {
                                                  id: "FUND_SRC31",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Funds",
                                                  trigger: {
                                                    id: "FUND_SRC30",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "BLOCK",
                                              mandatory: true,
                                              key: "FUND_SRC31-DATA",
                                              title: "4. Source of Wealth",
                                              titleType: "normal",
                                              detailSeq: 1,
                                              trigger: [
                                                {
                                                  type: "comparePremium",
                                                  id: "extra/premium",
                                                  ccys: [
                                                    {
                                                      ccy: "SGD",
                                                      amount: 20000
                                                    },
                                                    {
                                                      ccy: "AUD",
                                                      amount: 19000
                                                    },
                                                    {
                                                      ccy: "EUR",
                                                      amount: 13000
                                                    },
                                                    {
                                                      ccy: "GBP",
                                                      amount: 9600
                                                    },
                                                    {
                                                      ccy: "USD",
                                                      amount: 14000
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "showIfEqual",
                                                  id: "FUND_SRC01",
                                                  value: "foreign"
                                                }
                                              ],
                                              items: [
                                                {
                                                  type: "QTITLE",
                                                  normal: true,
                                                  title:
                                                    "Please confirm the origin of the Payor's total assets and where the Payor's wealth is derived from. (You may tick more than 1 option)"
                                                },
                                                {
                                                  title: "Salary/Commission",
                                                  id: "FUND_SRC33",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "FUND_SRC34"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Investment Profits (shares, bonds, unit trust, property etc)",
                                                  id: "FUND_SRC35"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "FUND_SRC36"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Withdrawal of CPF money",
                                                  id: "FUND_SRC37"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "FUND_SRC38"
                                                },
                                                {
                                                  id: "FUND_SRC39",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Wealth",
                                                  trigger: {
                                                    id: "FUND_SRC38",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title:
                                        "Financial Consultant's Declaration",
                                      items: [
                                        {
                                          id: "FCD_01",
                                          type: "text",
                                          max: 30,
                                          title:
                                            "How long have you known your customer?",
                                          mandatory: true
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "FCD_02",
                                          mandatory: true,
                                          title:
                                            "Are you related to your customer?",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "text",
                                          id: "FCD_03",
                                          mandatory: true,
                                          title: "Relationship to customer",
                                          max: 20,
                                          trigger: {
                                            id: "FCD_02",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Personal Data Acknowledgement",
                                      trigger: {
                                        id: "extra/channel",
                                        type: "showIfEqual",
                                        value: "FA"
                                      },
                                      items: [
                                        {
                                          id: "pda",
                                          type: "paragraph",
                                          paragraphType: "pda",
                                          content:
                                            "<p style='padding: 0px; line-height: 18px'>The information I or We have provided is my personal data and, where it is not my personal data, that I or We have the consent of the owner of such personal data to provide such information.<br/></p><br/>By providing this information, I or We understand and give my or our consent for AXA Insurance Pte Ltd (\"AXA\") and its representatives or agents to:</p><p style='padding: 0px; line-height: 18px'>i. Collect, use, store, transfer and/or disclose the information, to or with all such persons (including any member or the AXA Group or any third party service provider, and whether within or outside of Singapore) for the purpose of enabling AXA to provide me with services required of an insurance provider, including the evaluating, processing, administering and/or managing of my or our relationship and policy(ies) with AXA, and for the purposes set out in AXA's Data Use Statement which can be found at http://axa.com.sg (\"Purposes\").</p><p style='padding: 0px; line-height: 18px'>ii. Collect, use, store, transfer and/or disclose personal data about me or us and those whose personal data I or We have provided from sources other than myself or us for the Purposes.</p><p style='padding: 0px; line-height: 18px'>iii. Contact me or us to share marketing information about products and services from AXA that may be of interest to me or us by post and e-mail and</p><p style='padding: 0px; line-height: 18px'>(Please tick accordingly and you may tick more than one option)</p>"
                                        },
                                        {
                                          type: "BLOCK",
                                          key: "PDPA01",
                                          mandatory: true,
                                          validation:
                                            "function(i,v,rv,vrv){if(v.PDPA01a!='Y' && v.PDPA01b!='Y' && v.PDPA01c!='Y'){return 301;}}",
                                          items: [
                                            {
                                              type: "BLOCK",
                                              style: {
                                                display: "flex"
                                              },
                                              items: [
                                                {
                                                  id: "PDPA01a",
                                                  type: "checkbox",
                                                  title: "By Telephone"
                                                },
                                                {
                                                  id: "PDPA01b",
                                                  type: "checkbox",
                                                  title: "By Text Message"
                                                },
                                                {
                                                  id: "PDPA01c",
                                                  type: "checkbox",
                                                  title: "By Fax"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          id: "stepSign",
          type: "section",
          title: "Signature",
          detailSeq: 2,
          items: [
            {
              id: "sign",
              type: "signature"
            }
          ]
        },
        {
          id: "stepPay",
          type: "section",
          title: "Payment",
          detailSeq: 3,
          items: [
            {
              id: "pay",
              type: "payment"
            }
          ]
        },
        {
          id: "stepSubmit",
          type: "section",
          title: "Submit",
          detailSeq: 4,
          items: [
            {
              id: "submit",
              type: "submission"
            }
          ]
        }
      ]
    },
    application: {
      agentCode: "008008",
      appStep: 0,
      applicationForm: {
        values: {
          appFormTemplate: {
            dynTemplate: {
              declaration: "appform_dyn_declaration_acc",
              insurability: {
                la: {
                  BAA: "dyn_la_insurability_gio"
                }
              },
              personal: "appform_dyn_personal",
              plan: "appform_dyn_plan_acc",
              residency: "appform_dyn_residency_acc"
            },
            main: "appform_report_main",
            properties: {
              dollarSignForAllCcy: true
            },
            template: [
              "appform_report_common",
              "appform_report_personal_details",
              "appform_report_plan_details_baa",
              "appform_report_residency_acc",
              "appform_report_insurability_acc",
              "appform_report_declaration_acc"
            ]
          },
          checkedMenu: ["menu_person"],
          completedMenus: [],
          insured: [],
          isCompleted: false,
          menus: [
            "menu_person",
            "menu_residency",
            "menu_plan",
            "menu_declaration"
          ],
          planDetails: {
            ccy: "SGD",
            covClass: "4",
            displayPlanList: [
              {
                calcBy: "sumAssured",
                covClass: "4",
                covCode: "BAA",
                covName: {
                  en: "AXA Band Aid",
                  "zh-Hant": "AXA Band Aid"
                },
                halfYearPrem: 35.95,
                monthPrem: 6.16,
                planCode: "PAR4",
                polTermDesc: "To Age 75",
                policyTerm: "999",
                premRate: 1.23375,
                premTerm: "999",
                premTermDesc: "To Age 75",
                premium: 75.44,
                productLine: "AP",
                quarterPrem: 18.33,
                saViewInd: "Y",
                sumInsured: 50000,
                tax: {
                  halfYearTax: 2.52,
                  monthTax: 0.43,
                  quarterTax: 1.28,
                  yearTax: 4.94
                },
                totalPremium: 75.44,
                version: 2,
                yearPrem: 70.5
              }
            ],
            hasRiders: "N",
            isBackDate: "N",
            paymentMode: "A",
            paymentTerm: "Regular",
            planList: [
              {
                calcBy: "sumAssured",
                covClass: "4",
                covCode: "BAA",
                covName: {
                  en: "AXA Band Aid",
                  "zh-Hant": "AXA Band Aid"
                },
                halfYearPrem: 35.95,
                monthPrem: 6.16,
                planCode: "PAR4",
                polTermDesc: "To Age 75",
                policyTerm: "999",
                premRate: 1.23375,
                premTerm: "999",
                premTermDesc: "To Age 75",
                premium: 75.44,
                productLine: "AP",
                quarterPrem: 18.33,
                saViewInd: "Y",
                sumInsured: 50000,
                tax: {
                  halfYearTax: 2.52,
                  monthTax: 0.43,
                  quarterTax: 1.28,
                  yearTax: 4.94
                },
                version: 2,
                yearPrem: 70.5
              }
            ],
            planTypeName: {
              en: "AXA Band Aid",
              "zh-Hant": "AXA Band Aid"
            },
            premium: 75.44,
            riderList: [],
            riskCommenDate: "2018-07-17",
            sumInsured: 50000,
            totYearPrem: 75.44
          },
          proposer: {
            declaration: {
              ccy: "SGD",
              trustedIndividuals: {
                firstName: "Alex bun 5 Fa Daug",
                fullName: "Alex bun 5 Fa Daug",
                idCardNo: "S1234567D",
                idDocType: "nric",
                lastName: "",
                mobileCountryCode: "+65",
                mobileNo: 123123,
                nameOrder: "L",
                relationship: "SON",
                tiPhoto: ""
              }
            },
            extra: {
              baseProductCode: "BAA",
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              hasTrustedIndividual: "Y",
              isPhSameAsLa: "Y",
              premium: 75.44
            },
            personalInfo: {
              addrBlock: "aaaaabc",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "sssssasdfsfaaba",
              age: 30,
              agentId: "008008",
              allowance: 123123,
              applicationCount: 0,
              branchInfo: {},
              bundle: [
                {
                  id: "FN001003-00046",
                  isValid: false
                },
                {
                  id: "FN001003-00048",
                  isValid: false
                },
                {
                  id: "FN001003-00049",
                  isValid: false
                },
                {
                  id: "FN001003-00054",
                  isValid: false
                },
                {
                  id: "FN001003-00055",
                  isValid: false
                },
                {
                  id: "FN001003-00056",
                  isValid: false
                },
                {
                  id: "FN001003-00067",
                  isValid: false
                },
                {
                  id: "FN001003-00068",
                  isValid: false
                },
                {
                  id: "FN001003-00069",
                  isValid: false
                },
                {
                  id: "FN001003-00070",
                  isValid: false
                },
                {
                  id: "FN001003-00098",
                  isValid: false
                },
                {
                  id: "FN001003-00099",
                  isValid: false
                },
                {
                  id: "FN001003-00100",
                  isValid: false
                },
                {
                  id: "FN001003-00101",
                  isValid: false
                },
                {
                  id: "FN001003-00102",
                  isValid: false
                },
                {
                  id: "FN001003-00130",
                  isValid: false
                },
                {
                  id: "FN001003-00131",
                  isValid: false
                },
                {
                  id: "FN001003-00132",
                  isValid: false
                },
                {
                  id: "FN001003-00133",
                  isValid: false
                },
                {
                  id: "FN001003-00135",
                  isValid: false
                },
                {
                  id: "FN001003-00137",
                  isValid: false
                },
                {
                  id: "FN001003-00138",
                  isValid: false
                },
                {
                  id: "FN001003-00255",
                  isValid: false
                },
                {
                  id: "FN001003-00274",
                  isValid: false
                },
                {
                  id: "FN001003-00275",
                  isValid: false
                },
                {
                  id: "FN001003-00276",
                  isValid: true
                }
              ],
              cid: "CP001003-00006",
              dependants: [
                {
                  cid: "CP001003-00007",
                  relationship: "SPO",
                  relationshipOther: null
                },
                {
                  cid: "CP001003-00008",
                  relationship: "SON"
                },
                {
                  cid: "CP001003-00009",
                  relationship: "MOT"
                },
                {
                  cid: "CP001003-00010",
                  relationship: "FAT"
                },
                {
                  cid: "CP001003-00028",
                  relationship: "SON"
                }
              ],
              dob: "1987-11-24",
              education: "below",
              email: "alex.tang@eabsystems.com",
              employStatus: "ft",
              firstName: "Alex 4",
              fnaRecordIdArray: "",
              fullName: "Alex 4",
              gender: "M",
              hanyuPinyinName: "",
              haveSignDoc: true,
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I12",
              initial: "A",
              isSameAddr: "Y",
              isSmoker: "Y",
              isValid: true,
              language: "zh",
              lastName: "",
              lastUpdateDate: "2018-06-15T06:09:59.821Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "123123",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 31,
              occupation: "O921",
              occupationOther: "bbbbbb",
              organization: "aaaa",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "1233466789",
              referrals: "",
              residenceCountry: "R2",
              title: "Mr",
              trustedIndividuals: {
                firstName: "Alex bun 5 Fa Daug",
                fullName: "Alex bun 5 Fa Daug",
                idCardNo: "S1234567D",
                idDocType: "nric",
                lastName: "",
                mobileCountryCode: "+65",
                mobileNo: 123123,
                nameOrder: "L",
                relationship: "SON",
                tiPhoto: ""
              },
              type: "cust",
              unitNum: ""
            },
            policies: {
              existCi: 0,
              existCiAXA: 0,
              existCiOther: 0,
              existLife: 3333,
              existLifeAXA: 0,
              existLifeOther: 0,
              existPaAdb: 0,
              existPaAdbAXA: 0,
              existPaAdbOther: 0,
              existTotalPrem: 44,
              existTotalPremAXA: 0,
              existTotalPremOther: 0,
              existTpd: 0,
              existTpdAXA: 0,
              existTpdOther: 0,
              havAccPlans: "N",
              isProslReplace: "N",
              replaceCi: 0,
              replaceCiAXA: 0,
              replaceCiOther: 0,
              replaceLife: 0,
              replaceLifeAXA: 0,
              replaceLifeOther: 0,
              replacePaAdb: 0,
              replacePaAdbAXA: 0,
              replacePaAdbOther: 0,
              replacePolTypeAXA: "-",
              replacePolTypeOther: "-",
              replaceTotalPrem: 0,
              replaceTotalPremAXA: 0,
              replaceTotalPremOther: 0,
              replaceTpd: 0,
              replaceTpdAXA: 0,
              replaceTpdOther: 0
            },
            residency: {}
          },
          undefined: {}
        }
      },
      applicationInforceDate: 0,
      applicationSignedDate: 0,
      applicationStartedDate: "2018-07-17T02:48:48.440Z",
      applicationSubmittedDate: 0,
      bundleId: "FN001003-00276",
      compCode: "01",
      createDate: 1531795728440,
      dealerGroup: "AGENCY",
      iCid: "CP001003-00006",
      id: "NB001003-02060",
      isApplicationSigned: false,
      isBackDate: false,
      isFailSubmit: false,
      isFullySigned: false,
      isMandDocsAllUploaded: false,
      isProposalSigned: false,
      isStartSignature: false,
      isSubmittedStatus: false,
      isValid: true,
      lastUpdateDate: 1531795730984,
      pCid: "CP001003-00006",
      quotation: {
        agent: {
          agentCode: "008008",
          company: "AXA Insurance Pte Ltd",
          dealerGroup: "AGENCY",
          email: "alex.tang@eabsystems.com",
          mobile: "12345678",
          name: "Alex Tang",
          tel: "0"
        },
        agentCode: "008008",
        baseProductCode: "BAA",
        baseProductId: "08_product_BAA_2",
        baseProductName: {
          en: "AXA Band Aid",
          "zh-Hant": "AXA Band Aid"
        },
        budgetRules: ["RP"],
        bundleId: "FN001003-00276",
        ccy: "SGD",
        clientChoice: {
          isClientChoiceSelected: true,
          recommendation: {
            benefit:
              "AXA Band Aid is a comprehensive Accident & Health policy that provides cover up to age 75. It includes 5 core benefits which are payable when death OR injury occurs within 12 months from date of Accident. Please refer to Product Summary for the benefits and terms and conditions.\n\nThe Permanent Disablement Schedule is split into 4 categories with different Scope of Coverage and payout amount based on a percentage of your Sum Assured. \n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.",
            limitation:
              "There are certain conditions under which no benefits will be payable under the Policy:\n1) Any pre-existing condition 2) Acts of war, terrorism, riot, strike or civil commotion, if you are collaborated, participated or provoked such act directly or indirectly. 3) Engaging in illegal or criminal acts 4) Hazardous pursuits including but not limited to sky diving, diving, motorcar and motorcycle racing, deep sea fishing, deep sea diving, scuba diving, skiing, mountaineering and parachuting. Please refer to the policy contract for the full list of exclusions. \nAXA Band Aid premiums rates are based on your Occupation Class and rates may be adjusted based on future experience. Policy is not guaranteed renewable. We must receive in writing, immediately or as soon as possible, information on any change in your occupation, country of residence, or pursuits. We have the right to vary the terms and conditions of this Policy and/or adjust the Premiums payable. If there has been a failure to notify Us as required in this clause, We may, in the event of a claim being made under this Policy, repudiate the claim or adjust the Benefits payable at Our discretion.\n\nThe maximum payable Accidental-related benefits is S$3.5 million inclusive of all other policies issued by AXA and other insurance companies, in respect of the same Life Assured. Refer to your Product Summary for definition of Accident, Schedule of Permanent Disablement Benefits with scope of coverage, the exclusions, terms & conditions for Benefits to be payable.",
            reason: "asdf",
            rop: {
              choiceQ1: "N",
              choiceQ1Sub1: "",
              choiceQ1Sub2: "",
              choiceQ1Sub3: "",
              existCi: 0,
              existLife: 3333,
              existPaAdb: 0,
              existTotalPrem: 44,
              existTpd: 0,
              replaceCi: 0,
              replaceLife: 0,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              replaceTpd: 0
            },
            rop_shield: {
              ropBlock: {
                iCidRopAnswerMap: {},
                ropQ1sub3: "",
                ropQ2: "",
                ropQ3: "",
                shieldRopAnswer_0: "",
                shieldRopAnswer_1: "",
                shieldRopAnswer_2: "",
                shieldRopAnswer_3: "",
                shieldRopAnswer_4: "",
                shieldRopAnswer_5: ""
              }
            }
          }
        },
        compCode: "01",
        createDate: "2018-07-17T02:45:55.077Z",
        dealerGroup: "AGENCY",
        extraFlags: {
          fna: {
            riskProfile: 5
          },
          nationalityResidence: {
            iCategory: "A1",
            iRejected: "N",
            pCategory: "A1",
            pRejected: "N"
          }
        },
        fund: null,
        iAge: 31,
        iCid: "CP001003-00006",
        iDob: "1987-11-24",
        iEmail: "alex.tang@eabsystems.com",
        iFirstName: "Alex 4",
        iFullName: "Alex 4",
        iGender: "M",
        iLastName: "",
        iOccupation: "O921",
        iOccupationClass: "4",
        iResidence: "R2",
        iSmoke: "Y",
        id: "QU001003-01379",
        isBackDate: "N",
        lastUpdateDate: "2018-07-17T02:46:06.093Z",
        pAge: 31,
        pCid: "CP001003-00006",
        pDob: "1987-11-24",
        pEmail: "alex.tang@eabsystems.com",
        pFirstName: "Alex 4",
        pFullName: "Alex 4",
        pGender: "M",
        pLastName: "",
        pOccupation: "O921",
        pOccupationClass: "4",
        pResidence: "R2",
        pSmoke: "Y",
        paymentMode: "A",
        plans: [
          {
            calcBy: "sumAssured",
            covClass: "4",
            covCode: "BAA",
            covName: {
              en: "AXA Band Aid",
              "zh-Hant": "AXA Band Aid"
            },
            halfYearPrem: 35.95,
            monthPrem: 6.16,
            planCode: "PAR4",
            polTermDesc: "To Age 75",
            policyTerm: "999",
            premRate: 1.23375,
            premTerm: "999",
            premTermDesc: "To Age 75",
            premium: 75.44,
            productLine: "AP",
            quarterPrem: 18.33,
            saViewInd: "Y",
            sumInsured: 50000,
            tax: {
              halfYearTax: 2.52,
              monthTax: 0.43,
              quarterTax: 1.28,
              yearTax: 4.94
            },
            version: 2,
            yearPrem: 70.5
          }
        ],
        policyOptions: {},
        policyOptionsDesc: {},
        premTerm: "999",
        premium: 75.44,
        productLine: "AP",
        productVersion: "2.0",
        riskCommenDate: "2018-07-17",
        sameAs: "Y",
        sumInsured: 50000,
        tax: {
          halfYearTax: 2.52,
          monthTax: 0.43,
          quarterTax: 1.28,
          yearTax: 4.94
        },
        totHalfyearPrem: 38.47,
        totMonthPrem: 6.59,
        totQuarterPrem: 19.61,
        totYearPrem: 75.44,
        type: "quotation"
      },
      quotationDocId: "QU001003-01379",
      sameAs: "Y",
      type: "application"
    },
    crossAgeObj: {},
    success: true
  }
};

export const applySAV = {
  p0: "bed80bd651fd39c0f2348534a63f9efab5c22667654ba157e14cda63f12339b4",
  p1: {
    success: true,
    template: {
      items: [
        {
          type: "section",
          title: "Application",
          detailSeq: 1,
          items: [
            {
              type: "menu",
              items: [
                {
                  type: "menuSection",
                  detailSeq: 0,
                  items: [
                    {
                      title: {
                        en: "Personal Details"
                      },
                      type: "menuItem",
                      key: "menu_person",
                      detailSeq: 0,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              priority: "proposer",
                              type: "tab",
                              items: [
                                {
                                  type: "PARAGRAPH",
                                  id: "pWarning",
                                  content:
                                    "<p>â€œPursuant to Section 25(5) of the Insurance Act of Singapore (CAP 142), you are to disclose in this proposal form, fully and faithfully, all the facts which you know or ought to know, or the policy issued below may be void.â€</p>",
                                  paragraphType: "warning",
                                  align: "left",
                                  detailSeq: 0
                                },
                                {
                                  id: "personalInfo",
                                  type: "Block",
                                  items: [
                                    {
                                      id: "branchInfo",
                                      title: "Singpost Branch Details",
                                      type: "BLOCK",
                                      detailSeq: 1,
                                      trigger: {
                                        id: "extra/channelName",
                                        type: "showIfEqual",
                                        value: "SINGPOST"
                                      },
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "PICKER",
                                              id: "branch",
                                              title: "Branch",
                                              subType: "limit",
                                              mandatory: true,
                                              detailSeq: 1,
                                              options: [
                                                {
                                                  value: "Alexandra",
                                                  title: {
                                                    en: "Alexandra"
                                                  },
                                                  rlscode: "10"
                                                },
                                                {
                                                  value: "Ang Mo Kio Central",
                                                  title: {
                                                    en: "Ang Mo Kio Central"
                                                  },
                                                  rlscode: "11"
                                                },
                                                {
                                                  value: "Bedok Central",
                                                  title: {
                                                    en: "Bedok Central"
                                                  },
                                                  rlscode: "12"
                                                },
                                                {
                                                  value: "Bishan",
                                                  title: {
                                                    en: "Bishan"
                                                  },
                                                  rlscode: "13"
                                                },
                                                {
                                                  value: "Bras Basah",
                                                  title: {
                                                    en: "Bras Basah"
                                                  },
                                                  rlscode: "14"
                                                },
                                                {
                                                  value: "Bukit Batok Central",
                                                  title: {
                                                    en: "Bukit Batok Central"
                                                  },
                                                  rlscode: "15"
                                                },
                                                {
                                                  value: "Bukit Merah Central",
                                                  title: {
                                                    en: "Bukit Merah Central"
                                                  },
                                                  rlscode: "16"
                                                },
                                                {
                                                  value: "Bukit Panjang",
                                                  title: {
                                                    en: "Bukit Panjang"
                                                  },
                                                  rlscode: "17"
                                                },
                                                {
                                                  value: "Bukit Timah",
                                                  title: {
                                                    en: "Bukit Timah"
                                                  },
                                                  rlscode: "18"
                                                },
                                                {
                                                  value:
                                                    "Changi Airport Terminal 2",
                                                  title: {
                                                    en:
                                                      "Changi Airport Terminal 2"
                                                  },
                                                  rlscode: "19"
                                                },
                                                {
                                                  value: "Chinatown",
                                                  title: {
                                                    en: "Chinatown"
                                                  },
                                                  rlscode: "20"
                                                },
                                                {
                                                  value:
                                                    "Choa Chua Kang Central",
                                                  title: {
                                                    en: "Choa Chua Kang Central"
                                                  },
                                                  rlscode: "21"
                                                },
                                                {
                                                  value: "Clementi Central",
                                                  title: {
                                                    en: "Clementi Central"
                                                  },
                                                  rlscode: "22"
                                                },
                                                {
                                                  value: "Clementi West",
                                                  title: {
                                                    en: "Clementi West"
                                                  },
                                                  rlscode: "23"
                                                },
                                                {
                                                  value: "Crawford",
                                                  title: {
                                                    en: "Crawford"
                                                  },
                                                  rlscode: "24"
                                                },
                                                {
                                                  value: "Geylang",
                                                  title: {
                                                    en: "Geylang"
                                                  },
                                                  rlscode: "25"
                                                },
                                                {
                                                  value: "Geylang East",
                                                  title: {
                                                    en: "Geylang East"
                                                  },
                                                  rlscode: "26"
                                                },
                                                {
                                                  value: "Ghim Moh Estate",
                                                  title: {
                                                    en: "Ghim Moh Estate"
                                                  },
                                                  rlscode: "27"
                                                },
                                                {
                                                  value: "HarbourFront Centre",
                                                  title: {
                                                    en: "HarbourFront Centre"
                                                  },
                                                  rlscode: "28"
                                                },
                                                {
                                                  value: "Hougang Central",
                                                  title: {
                                                    en: "Hougang Central"
                                                  },
                                                  rlscode: "29"
                                                },
                                                {
                                                  value: "Jurong East",
                                                  title: {
                                                    en: "Jurong East"
                                                  },
                                                  rlscode: "30"
                                                },
                                                {
                                                  value: "Jurong Point",
                                                  title: {
                                                    en: "Jurong Point"
                                                  },
                                                  rlscode: "31"
                                                },
                                                {
                                                  value: "Jurong West",
                                                  title: {
                                                    en: "Jurong West"
                                                  },
                                                  rlscode: "32"
                                                },
                                                {
                                                  value: "Katong",
                                                  title: {
                                                    en: "Katong"
                                                  },
                                                  rlscode: "33"
                                                },
                                                {
                                                  value: "Killiney Road",
                                                  title: {
                                                    en: "Killiney Road"
                                                  },
                                                  rlscode: "34"
                                                },
                                                {
                                                  value: "Kitchener Road",
                                                  title: {
                                                    en: "Kitchener Road"
                                                  },
                                                  rlscode: "35"
                                                },
                                                {
                                                  value: "Lim Ah Pin Road",
                                                  title: {
                                                    en: "Lim Ah Pin Road"
                                                  },
                                                  rlscode: "36"
                                                },
                                                {
                                                  value: "Marine Parade",
                                                  title: {
                                                    en: "Marine Parade"
                                                  },
                                                  rlscode: "37"
                                                },
                                                {
                                                  value: "Newton",
                                                  title: {
                                                    en: "Newton"
                                                  },
                                                  rlscode: "38"
                                                },
                                                {
                                                  value: "Novena",
                                                  title: {
                                                    en: "Novena"
                                                  },
                                                  rlscode: "39"
                                                },
                                                {
                                                  value: "Orchard",
                                                  title: {
                                                    en: "Orchard"
                                                  },
                                                  rlscode: "40"
                                                },
                                                {
                                                  value: "Pasir Panjang",
                                                  title: {
                                                    en: "Pasir Panjang"
                                                  },
                                                  rlscode: "41"
                                                },
                                                {
                                                  value: "Pasir Ris Central",
                                                  title: {
                                                    en: "Pasir Ris Central"
                                                  },
                                                  rlscode: "42"
                                                },
                                                {
                                                  value: "Pioneer Road",
                                                  title: {
                                                    en: "Pioneer Road"
                                                  },
                                                  rlscode: "43"
                                                },
                                                {
                                                  value: "Potong Pasir",
                                                  title: {
                                                    en: "Potong Pasir"
                                                  },
                                                  rlscode: "44"
                                                },
                                                {
                                                  value: "Punggol",
                                                  title: {
                                                    en: "Punggol"
                                                  },
                                                  rlscode: "45"
                                                },
                                                {
                                                  value: "Raffles Place",
                                                  title: {
                                                    en: "Raffles Place"
                                                  },
                                                  rlscode: "46"
                                                },
                                                {
                                                  value: "Robinson Road",
                                                  title: {
                                                    en: "Robinson Road"
                                                  },
                                                  rlscode: "47"
                                                },
                                                {
                                                  value: "Rochor",
                                                  title: {
                                                    en: "Rochor"
                                                  },
                                                  rlscode: "48"
                                                },
                                                {
                                                  value: "Sembawang",
                                                  title: {
                                                    en: "Sembawang"
                                                  },
                                                  rlscode: "49"
                                                },
                                                {
                                                  value: "Sengkang Central",
                                                  title: {
                                                    en: "Sengkang Central"
                                                  },
                                                  rlscode: "50"
                                                },
                                                {
                                                  value: "Serangoon Central",
                                                  title: {
                                                    en: "Serangoon Central"
                                                  },
                                                  rlscode: "51"
                                                },
                                                {
                                                  value: "Serangoon Garden",
                                                  title: {
                                                    en: "Serangoon Garden"
                                                  },
                                                  rlscode: "52"
                                                },
                                                {
                                                  value: "Siglap",
                                                  title: {
                                                    en: "Siglap"
                                                  },
                                                  rlscode: "53"
                                                },
                                                {
                                                  value: "Simpang Bedok",
                                                  title: {
                                                    en: "Simpang Bedok"
                                                  },
                                                  rlscode: "54"
                                                },
                                                {
                                                  value: "Paya Lebar",
                                                  title: {
                                                    en: "Paya Lebar"
                                                  },
                                                  rlscode: "55"
                                                },
                                                {
                                                  value: "Suntec City",
                                                  title: {
                                                    en: "Suntec City"
                                                  },
                                                  rlscode: "56"
                                                },
                                                {
                                                  value: "Tampines Central",
                                                  title: {
                                                    en: "Tampines Central"
                                                  },
                                                  rlscode: "57"
                                                },
                                                {
                                                  value: "Tanglin",
                                                  title: {
                                                    en: "Tanglin"
                                                  },
                                                  rlscode: "58"
                                                },
                                                {
                                                  value: "Tanjong Pagar",
                                                  title: {
                                                    en: "Tanjong Pagar"
                                                  },
                                                  rlscode: "59"
                                                },
                                                {
                                                  value: "Teban Garden",
                                                  title: {
                                                    en: "Teban Garden"
                                                  },
                                                  rlscode: "60"
                                                },
                                                {
                                                  value: "Thomson Road",
                                                  title: {
                                                    en: "Thomson Road"
                                                  },
                                                  rlscode: "61"
                                                },
                                                {
                                                  value: "Tiong Bahru",
                                                  title: {
                                                    en: "Tiong Bahru"
                                                  },
                                                  rlscode: "62"
                                                },
                                                {
                                                  value: "Toa Payoh Central",
                                                  title: {
                                                    en: "Toa Payoh Central"
                                                  },
                                                  rlscode: "63"
                                                },
                                                {
                                                  value: "Toa Payoh North",
                                                  title: {
                                                    en: "Toa Payoh North"
                                                  },
                                                  rlscode: "64"
                                                },
                                                {
                                                  value: "Towner",
                                                  title: {
                                                    en: "Towner"
                                                  },
                                                  rlscode: "65"
                                                },
                                                {
                                                  value: "Woodlands Central",
                                                  title: {
                                                    en: "Woodlands Central"
                                                  },
                                                  rlscode: "66"
                                                },
                                                {
                                                  value: "Yishun Central",
                                                  title: {
                                                    en: "Yishun Central"
                                                  },
                                                  rlscode: "67"
                                                },
                                                {
                                                  value: "Mobile",
                                                  title: {
                                                    en: "Mobile"
                                                  },
                                                  rlscode: "68"
                                                },
                                                {
                                                  value: "SingPost HQ",
                                                  title: {
                                                    en: "SingPost HQ"
                                                  },
                                                  rlscode: "69"
                                                },
                                                {
                                                  value:
                                                    "SingPost Staff Discount",
                                                  title: {
                                                    en:
                                                      "SingPost Staff Discount"
                                                  },
                                                  rlscode: "70"
                                                },
                                                {
                                                  value: "Mobile Sales Tower",
                                                  title: {
                                                    en: "Mobile Sales Tower"
                                                  },
                                                  rlscode: "71"
                                                },
                                                {
                                                  value: "City Square",
                                                  title: {
                                                    en: "City Square"
                                                  },
                                                  rlscode: "72"
                                                }
                                              ]
                                            },
                                            {
                                              type: "TEXT",
                                              id: "bankRefId",
                                              title: "Referrer ID",
                                              subType: "number_leading_zero",
                                              mandatory: true,
                                              min: 4,
                                              max: 4,
                                              detailSeq: 2
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Details of Proposer",
                                      type: "BLOCK",
                                      detailSeq: 2,
                                      items: [
                                        {
                                          type: "qtitle",
                                          title: "Proposer is a Life Assured: ",
                                          subTitle: "Yes",
                                          trigger: {
                                            id: "extra/isPhSameAsLa",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          type: "qtitle",
                                          title: "Proposer is a Life Assured: ",
                                          subTitle: "No",
                                          trigger: {
                                            id: "extra/isPhSameAsLa",
                                            type: "showIfNotEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "READONLY",
                                              id: "lastName",
                                              emptyValue: "-",
                                              title: "Surname (as shown in ID)",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "firstName",
                                              emptyValue: "-",
                                              title:
                                                "Given Name (as shown in ID)",
                                              detailSeq: 2
                                            },
                                            {
                                              type: "READONLY",
                                              id: "othName",
                                              emptyValue: "-",
                                              title: "English or other name",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "hanyuPinyinName",
                                              emptyValue: "-",
                                              title: "Han Yu Pin Yin Name",
                                              detailSeq: 2
                                            },
                                            {
                                              type: "READONLY",
                                              id: "fullName",
                                              title: "Name",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "READONLY",
                                              id: "nationality",
                                              title: "Nationality",
                                              detailSeq: 2,
                                              options: "nationality",
                                              validation:
                                                "function(i,v,rv,vrv){if(v[i.id]==='N1'){v.prStatus='Y';}}"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "prStatus",
                                              title: "Singapore PR Status",
                                              detailSeq: 1,
                                              options: [
                                                {
                                                  title: {
                                                    en: "Yes",
                                                    "zh-Hant": "æ˜¯"
                                                  },
                                                  value: "Y"
                                                },
                                                {
                                                  title: {
                                                    en: "No",
                                                    "zh-Hant": "å¦"
                                                  },
                                                  value: "N"
                                                }
                                              ],
                                              trigger: {
                                                id: "nationality",
                                                type: "showIfNotEqual",
                                                value: "N1"
                                              }
                                            },
                                            {
                                              id: "idDocType",
                                              title: {
                                                en: "ID Document Type"
                                              },
                                              trigger: [
                                                {
                                                  id: "idDocType",
                                                  type: "showIfNotEqual",
                                                  value: "other"
                                                },
                                                {
                                                  id: "idDocType",
                                                  type: "notEmpty"
                                                }
                                              ],
                                              type: "READONLY",
                                              value: "nric",
                                              validation:
                                                "function(i,v,rv,vrv){if(v.nationality=='N1' || v.prstatus=='Y'){i.disabled=true;v[i.id]='nric';}else{i.disabled=false}}",
                                              options: [
                                                {
                                                  title: {
                                                    en: "Birth Certificate No."
                                                  },
                                                  value: "bcNo"
                                                },
                                                {
                                                  title: {
                                                    en: "FIN No."
                                                  },
                                                  value: "fin"
                                                },
                                                {
                                                  title: {
                                                    en: "NRIC"
                                                  },
                                                  value: "nric"
                                                },
                                                {
                                                  title: {
                                                    en: "Passport"
                                                  },
                                                  value: "passport"
                                                },
                                                {
                                                  title: {
                                                    en: "Others"
                                                  },
                                                  isOther: true,
                                                  max: 30,
                                                  value: "other"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "idDocTypeOther",
                                              title: "ID Type Name",
                                              trigger: {
                                                id: "idDocType",
                                                type: "showIfEqual",
                                                value: "other"
                                              }
                                            },
                                            {
                                              type: "READONLY",
                                              id: "idCardNo",
                                              title: "ID Number",
                                              detailSeq: 2
                                            },
                                            {
                                              id: "gender",
                                              title: {
                                                en: "Gender",
                                                "zh-Hant": "æ€§åˆ¥"
                                              },
                                              type: "READONLY",
                                              options: [
                                                {
                                                  title: {
                                                    en: "Male",
                                                    "zh-Hant": "ç”·"
                                                  },
                                                  value: "M"
                                                },
                                                {
                                                  title: {
                                                    en: "Female",
                                                    "zh-Hant": "å¥³"
                                                  },
                                                  value: "F"
                                                }
                                              ],
                                              condition: "existing"
                                            },
                                            {
                                              id: "dob",
                                              title: {
                                                en: "Date of Birth(DD/MM/YYYY)",
                                                "zh-Hant":
                                                  "å‡ºç”Ÿæ—¥æœŸ(DD/MM/YYYY)"
                                              },
                                              type: "READONLY",
                                              subType: "date",
                                              min: 100,
                                              max: 0,
                                              condition: "existing"
                                            },
                                            {
                                              id: "marital",
                                              title: {
                                                en: "Marital status",
                                                "zh-Hant": "å©šå§»ç‹€æ³"
                                              },
                                              type: "READONLY",
                                              options: "marital"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "email",
                                              title: "Email",
                                              detailSeq: 1
                                            },
                                            {
                                              type: "HBOX",
                                              detailSeq: 2,
                                              title: "Mobile",
                                              trigger: {
                                                id: "mobileNo",
                                                type: "notEmpty"
                                              },
                                              items: [
                                                {
                                                  id: "mobileCountryCode",
                                                  type: "READONLY",
                                                  subType: "countryCode",
                                                  style:
                                                    "width:35px !important; margin-right:0 !important",
                                                  placeholder: {
                                                    en: "Country code of Mobile"
                                                  },
                                                  value: "+65",
                                                  options: "countryTelCode"
                                                },
                                                {
                                                  id: "mobileNo",
                                                  placeholder: {
                                                    en: "Mobile No."
                                                  },
                                                  max: 15,
                                                  type: "READONLY",
                                                  subType: "phone"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "Divider"
                                    },
                                    {
                                      title: "Professional Details",
                                      type: "BLOCK",
                                      detailSeq: 2,
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              type: "TEXT",
                                              id: "organization",
                                              title:
                                                "Name of Employer/Business/School",
                                              populate: {
                                                module: "client"
                                              },
                                              max: 50,
                                              detailSeq: 1,
                                              mandatoryTrigger: {
                                                id: "occupation",
                                                value: [
                                                  "O675",
                                                  "O674",
                                                  "O1132",
                                                  "O1450"
                                                ],
                                                type: "trueIfNotContains"
                                              }
                                            },
                                            {
                                              type: "PICKER",
                                              subType: "limit",
                                              id: "organizationCountry",
                                              populate: {
                                                module: "client"
                                              },
                                              title:
                                                "Country of Employer/Business/School",
                                              mandatory: true,
                                              detailSeq: 2,
                                              options: "residency"
                                            },
                                            {
                                              type: "READONLY",
                                              id: "pass",
                                              title: "Type of Pass",
                                              detailSeq: 1,
                                              options: "pass",
                                              triggerType: "and",
                                              trigger: [
                                                {
                                                  id: "nationality",
                                                  type: "showIfNotEqual",
                                                  value: "N1"
                                                },
                                                {
                                                  id: "prStatus",
                                                  type: "showIfEqual",
                                                  value: "N"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "passOther",
                                              title: "Type of pass (Other)",
                                              detailSeq: 4,
                                              trigger: {
                                                id: "pass",
                                                type: "showIfEqual",
                                                value: "o"
                                              }
                                            },
                                            {
                                              type: "DATEPICKER",
                                              id: "passExpDate",
                                              max: 100,
                                              min: 0,
                                              title:
                                                "Pass Expiry Date (DD/MM/YYYY)",
                                              mandatory: true,
                                              detailSeq: 2,
                                              populate: {
                                                module: "client"
                                              },
                                              triggerType: "and",
                                              trigger: [
                                                {
                                                  id: "nationality",
                                                  type: "showIfNotEqual",
                                                  value: "N1"
                                                },
                                                {
                                                  id: "prStatus",
                                                  type: "showIfEqual",
                                                  value: "N"
                                                }
                                              ]
                                            },
                                            {
                                              type: "READONLY",
                                              id: "industry",
                                              title: "Industry",
                                              subType: "picker",
                                              detailSeq: 2,
                                              options: "industry"
                                            },
                                            {
                                              id: "occupation",
                                              title: "Occupation",
                                              subType: "picker",
                                              type: "READONLY",
                                              options: "occupation"
                                            },
                                            {
                                              id: "occupationOther",
                                              title: "Occupation (Other)",
                                              type: "READONLY",
                                              trigger: {
                                                id: "occupation",
                                                type: "showIfEqual",
                                                value: "O921"
                                              }
                                            },
                                            {
                                              type: "READONLY",
                                              id: "allowance",
                                              subType: "currency",
                                              title:
                                                "Monthly Income/Allowance SGD",
                                              detailSeq: 6,
                                              min: 0,
                                              max: 9999999999,
                                              showZeroValue: true,
                                              ccy: "SGD"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Residential Address",
                                      type: "BLOCK",
                                      detailSeq: 3,
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              id: "addrCountry",
                                              title: "Country",
                                              value: "R2",
                                              options: "residency",
                                              type: "READONLY"
                                            },
                                            {
                                              id: "postalCode",
                                              subType: "postalcode",
                                              countryAllowed: "addrCountry",
                                              mapping: {
                                                bldgNo: "addrBlock,12",
                                                bldgName: "addrEstate,40",
                                                streetName: "addrStreet,24"
                                              },
                                              title: {
                                                en: "Postal Code"
                                              },
                                              type: "text",
                                              mandatory: true,
                                              max: 10
                                            },
                                            {
                                              type: "TEXT",
                                              id: "addrBlock",
                                              title: "Block/House Number",
                                              mandatory: true,
                                              detailSeq: 1,
                                              max: 12,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              type: "TEXT",
                                              id: "addrStreet",
                                              title: "Street/Road Name",
                                              mandatory: true,
                                              detailSeq: 2,
                                              max: 24
                                            },
                                            {
                                              id: "unitNum",
                                              title: {
                                                en: "Unit Number"
                                              },
                                              emptyValue: "-",
                                              type: "text",
                                              subType: "unit_no",
                                              max: 13,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "addrEstate",
                                              title: "Building/ Estate Name",
                                              emptyValue: "-",
                                              type: "text",
                                              max: 40,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "addrCity",
                                              disablePicker: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfNotContains"
                                              },
                                              disableTrigger: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfContains"
                                              },
                                              emptyValue: "-",
                                              title: "City/State",
                                              max: 12,
                                              type: "picker",
                                              options: "city"
                                            },
                                            {
                                              id: "otherResidenceCity",
                                              title: {
                                                en: "Others"
                                              },
                                              disableTrigger: {
                                                id: "addrCountry",
                                                value: "city",
                                                type: "trueIfContains"
                                              },
                                              emptyValue: "-",
                                              trigger: {
                                                type: "dynamicCondition",
                                                id: "addrCountry,addrCity",
                                                value:
                                                  "(!_.isUndefined(#1) && (#1 ==='R52' ))||((!_.isUndefined(#1)  && !_.isUndefined(#2) && (#2 ==='T110' || #2 ==='T120' ) ))"
                                              },
                                              type: "text",
                                              subType: "text",
                                              maxLength: 50
                                            }
                                          ]
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "isSameAddr",
                                          mandatory: true,
                                          title:
                                            "Is your Residential Address same as mailing address?",
                                          horizontal: true,
                                          value: "Y",
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "Divider"
                                    },
                                    {
                                      title: "Mailing Address",
                                      type: "BLOCK",
                                      detailSeq: 3,
                                      trigger: {
                                        id: "isSameAddr",
                                        type: "showIfEqual",
                                        value: "N"
                                      },
                                      items: [
                                        {
                                          type: "HBOX",
                                          detailSeq: 1,
                                          items: [
                                            {
                                              id: "mAddrCountry",
                                              mandatory: true,
                                              title: {
                                                en: "Country"
                                              },
                                              value: "R2",
                                              options: "residency",
                                              type: "picker",
                                              subType: "limit"
                                            },
                                            {
                                              id: "mPostalCode",
                                              mandatory: true,
                                              subType: "postalcode",
                                              countryAllowed: "mAddrCountry",
                                              mapping: {
                                                bldgNo: "mAddrBlock,12",
                                                bldgName: "mAddrEstate,40",
                                                streetName: "mAddrStreet,24"
                                              },
                                              title: {
                                                en: "Postal Code"
                                              },
                                              type: "text",
                                              max: 10
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 2,
                                          items: [
                                            {
                                              type: "TEXT",
                                              mandatory: true,
                                              id: "mAddrBlock",
                                              title: "Block/House Number",
                                              detailSeq: 1,
                                              max: 12,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              type: "TEXT",
                                              mandatory: true,
                                              id: "mAddrStreet",
                                              title: "Street/Road Name",
                                              detailSeq: 2,
                                              max: 24
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 3,
                                          items: [
                                            {
                                              id: "mAddrnitNum",
                                              title: {
                                                en: "Unit Number"
                                              },
                                              type: "text",
                                              subType: "unit_no",
                                              max: 13,
                                              populate: {
                                                module: "client"
                                              }
                                            },
                                            {
                                              id: "mAddrEstate",
                                              title: "Building/ Estate Name",
                                              type: "text",
                                              max: 40,
                                              populate: {
                                                module: "client"
                                              }
                                            }
                                          ]
                                        },
                                        {
                                          type: "HBOX",
                                          detailSeq: 3,
                                          items: [
                                            {
                                              id: "mAddrCity",
                                              max: 12,
                                              title: "City/State",
                                              type: "text"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      title: {
                        en: "Residency & Insurance Info"
                      },
                      type: "menuItem",
                      key: "menu_residency",
                      detailSeq: 1,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              type: "tab",
                              items: [
                                {
                                  title: "Residency Questions",
                                  id: "residency",
                                  type: "BLOCK",
                                  detailSeq: 1,
                                  trigger: [
                                    {
                                      id: "personalInfo/nationality",
                                      type: "showIfEqual",
                                      value: "N1"
                                    },
                                    {
                                      id: "personalInfo/prStatus",
                                      type: "showIfEqual",
                                      value: "Y"
                                    },
                                    {
                                      id: "personalInfo/pass",
                                      type: "showIfExist",
                                      value: "ep,sp,wp,s,dp,svp,o,lp"
                                    }
                                  ],
                                  items: [
                                    {
                                      type: "BLOCK",
                                      trigger: {
                                        id: "personalInfo/nationality",
                                        type: "showIfEqual",
                                        value: "N1"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Singapore Citizen",
                                          id: "tSingaporeC"
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F1",
                                          mandatory: true,
                                          align: "left",
                                          title: "I am currently:",
                                          options: [
                                            {
                                              value: "resided",
                                              title: "i)  residing in Singapore"
                                            },
                                            {
                                              value: "outside",
                                              needToHighlight: true,
                                              title:
                                                "ii) residing outside Singapore: I have resided outside Singapore continuously for {{less than 5 years}} preceding the date of proposal."
                                            },
                                            {
                                              value: "outside5",
                                              needToHighlight: true,
                                              title:
                                                "iii) residing outside Singapore: I have resided outside Singapore continuously for {{5 years or more}} preceding the date of proposal. "
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      trigger: {
                                        id:
                                          "@personalInfo.nationality,@personalInfo.prStatus,@personalInfo.pass",
                                        type: "dynamicCondition",
                                        value:
                                          "#1!='N1' && (#2=='Y' || #3 == 'ep' || #3=='wp' || #3=='sp')"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/prStatus",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          title: "Singapore PR",
                                          id: "tSiPrA"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "ep"
                                          },
                                          title: "Employment Pass",
                                          id: "tSiPrB"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "wp"
                                          },
                                          title: "Work Permit",
                                          id: "tSiPrC_"
                                        },
                                        {
                                          type: "QTITLE",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "sp"
                                          },
                                          title: "S Pass",
                                          id: "tSiPrD"
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F2",
                                          mandatory: true,
                                          title:
                                            "1. Have you resided in Singapore for at least 183 days in the last 12 months preceding the date of proposal?",
                                          value: "",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F3",
                                          mandatory: true,
                                          title:
                                            "2. Do you intend to stay in Singapore on a permanent basis?",
                                          value: "",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      trigger: {
                                        id: "personalInfo/pass",
                                        type: "showIfExist",
                                        value: "dp,s,lp"
                                      },
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Dependent Pass",
                                          id: "tSiPrCde",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "dp"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Student Pass",
                                          id: "tSiPrCStS",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "s"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Long Term Visit Pass",
                                          id: "tSiPrCStL",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "lp"
                                          }
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "EAPP-P6-F4",
                                          title:
                                            "1.Â  Have you resided in Singapore for at least 90 days in the last 12 months preceding the date of proposal?",
                                          mandatory: true,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      trigger: [
                                        {
                                          id: "personalInfo/pass",
                                          type: "showIfExist",
                                          value: "svp,o"
                                        }
                                      ],
                                      items: [
                                        {
                                          type: "QTITLE",
                                          title: "Others",
                                          id: "tSiPrD",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "o"
                                          },
                                          replaceTitle: {
                                            id: "personalInfo/passOther"
                                          }
                                        },
                                        {
                                          type: "QTITLE",
                                          title: "Social Visit Pass",
                                          id: "tSiPrDD",
                                          trigger: {
                                            id: "personalInfo/pass",
                                            type: "showIfEqual",
                                            value: "svp"
                                          }
                                        },
                                        {
                                          type: "CheckBox",
                                          id: "EAPP-P6-F5",
                                          disabled: true,
                                          value: "Y",
                                          title: "I hold Type of Pass above."
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  title: "Foreigner Questions",
                                  id: "foreigner",
                                  type: "BLOCK",
                                  detailSeq: 2,
                                  trigger: [
                                    {
                                      id: "residency/EAPP-P6-F2",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "residency/EAPP-P6-F3",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "residency/EAPP-P6-F4",
                                      type: "showIfEqual",
                                      value: "N"
                                    },
                                    {
                                      id: "personalInfo/pass",
                                      type: "showIfExist",
                                      value: "dp,s,lp,svp,o"
                                    }
                                  ],
                                  items: [
                                    {
                                      type: "QTITLE",
                                      style: {
                                        marginBottom: "-20px"
                                      },
                                      title:
                                        "When did you first arrive in Singapore (excluding holidays of less than 3 months)?"
                                    },
                                    {
                                      type: "DATEPICKER",
                                      subType: "date",
                                      max: 0,
                                      min: 100,
                                      id: "faDate",
                                      mandatory: true,
                                      dateFormat: "mm/yyyy",
                                      style: {
                                        width: "50%",
                                        margin: 0
                                      }
                                    },
                                    {
                                      type: "QTITLE",
                                      id: "familyResD",
                                      style: {
                                        marginTop: "25px"
                                      },
                                      title:
                                        "Where do your immediate family members currently reside?"
                                    },
                                    {
                                      type: "HBOX",
                                      items: [
                                        {
                                          title: "Country",
                                          type: "PICKER",
                                          subType: "limit",
                                          id: "familyResCountry",
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          title: "City",
                                          type: "TEXT",
                                          id: "familyResCity",
                                          mandatory: true,
                                          max: 50
                                        }
                                      ]
                                    },
                                    {
                                      type: "QTITLE",
                                      id: "5yearDesc",
                                      style: {
                                        paddingTop: "20px",
                                        marginBottom: "5px"
                                      },
                                      title:
                                        "Please provide details of your residence over the last five years:"
                                    },
                                    {
                                      type: "table",
                                      id: "reside5yrs",
                                      mandatory: true,
                                      allowEdit: true,
                                      hasBorder: true,
                                      max: 5,
                                      items: [
                                        {
                                          type: "DATEPICKER",
                                          mandatory: true,
                                          width: "10%",
                                          title: "From (mm/yyyy)",
                                          id: "res5Frm",
                                          max: 0,
                                          min: 5,
                                          dateFormat: "mm/yyyy"
                                        },
                                        {
                                          type: "DATEPICKER",
                                          mandatory: true,
                                          width: "10%",
                                          title: "To (mm/yyyy)",
                                          id: "res5To",
                                          max: 0,
                                          min: 5,
                                          dateFormat: "mm/yyyy",
                                          validation: {
                                            id: "res5Frm",
                                            type: "month_greater"
                                          }
                                        },
                                        {
                                          id: "res5Country",
                                          type: "PICKER",
                                          title: "Country of Residence",
                                          subType: "limit",
                                          detailSeq: 2,
                                          width: "20%",
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          id: "res5City",
                                          type: "TEXT",
                                          title: "City of Residence",
                                          detailSeq: 3,
                                          width: "20%",
                                          mandatory: true,
                                          max: 50
                                        },
                                        {
                                          type: "table-block-t",
                                          title: "Reason",
                                          presentation:
                                            "permanent$work$study$cob$family$immigrate$visit$other",
                                          width: "30%",
                                          mandatory: true,
                                          key: "res_reason_t",
                                          items: [
                                            {
                                              id: "permanent",
                                              type: "checkbox",
                                              title: "Permanent Residence"
                                            },
                                            {
                                              id: "work",
                                              type: "checkbox",
                                              title: "Business / Work"
                                            },
                                            {
                                              id: "study",
                                              type: "checkbox",
                                              title: "Study"
                                            },
                                            {
                                              id: "cob",
                                              type: "checkbox",
                                              title: "Country of birth"
                                            },
                                            {
                                              id: "family",
                                              type: "checkbox",
                                              title:
                                                "Place where family members or relatives residing"
                                            },
                                            {
                                              id: "immigrate",
                                              type: "checkbox",
                                              title: "Immigration"
                                            },
                                            {
                                              id: "visit",
                                              type: "checkbox",
                                              title:
                                                "Visit family/relatives/friends"
                                            },
                                            {
                                              id: "other",
                                              type: "checkbox",
                                              title: "Others"
                                            },
                                            {
                                              type: "TEXT",
                                              title: "Please Specify",
                                              id: "resultOther",
                                              mandatory: true,
                                              max: 50,
                                              detailSeq: 4,
                                              trigger: {
                                                id: "other",
                                                type: "showIfEqual",
                                                value: "Y"
                                              }
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "QRADIOGROUP",
                                      id: "hasTravelPlans",
                                      mandatory: true,
                                      title:
                                        "In the next 2 years, do you expect to go abroad (outside the current country of residence) for any future residency or travel plans? ",
                                      horizontal: true,
                                      align: "left",
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      type: "table",
                                      max: 5,
                                      id: "travelPlans",
                                      allowEdit: true,
                                      hasBorder: true,
                                      mandatory: true,
                                      trigger: {
                                        id: "hasTravelPlans",
                                        type: "showIfEqual",
                                        value: "Y"
                                      },
                                      items: [
                                        {
                                          id: "travlCountry",
                                          title: "Country",
                                          type: "PICKER",
                                          subType: "limit",
                                          detailSeq: 1,
                                          mandatory: true,
                                          options: "residency"
                                        },
                                        {
                                          id: "travlCity",
                                          type: "TEXT",
                                          title: "City",
                                          detailSeq: 2,
                                          mandatory: true,
                                          max: 50
                                        },
                                        {
                                          id: "travlCityDrn",
                                          type: "TEXT",
                                          subType: "number",
                                          title: "Duration of each Stay (Days)",
                                          detailSeq: 3,
                                          mandatory: true,
                                          max: 99999
                                        },
                                        {
                                          id: "travlCityFrq",
                                          type: "TEXT",
                                          subType: "number",
                                          title: "Frequency (per year)",
                                          detailSeq: 4,
                                          mandatory: true,
                                          max: 99999
                                        },
                                        {
                                          type: "table-block-t",
                                          key: "res_reason_t",
                                          title: "Reason",
                                          presentation:
                                            "permanent$work$study$cob$family$immigrate$visit$other",
                                          width: "30%",
                                          mandatory: true,
                                          items: [
                                            {
                                              id: "permanent",
                                              type: "checkbox",
                                              title: "Permanent Residence"
                                            },
                                            {
                                              id: "work",
                                              type: "checkbox",
                                              title: "Business / Work"
                                            },
                                            {
                                              id: "study",
                                              type: "checkbox",
                                              title: "Study"
                                            },
                                            {
                                              id: "cob",
                                              type: "checkbox",
                                              title: "Country of birth"
                                            },
                                            {
                                              id: "family",
                                              type: "checkbox",
                                              title:
                                                "Place where family members or relatives residing"
                                            },
                                            {
                                              id: "immigrate",
                                              type: "checkbox",
                                              title: "Immigration"
                                            },
                                            {
                                              id: "visit",
                                              type: "checkbox",
                                              title:
                                                "Visit family/relatives/friends"
                                            },
                                            {
                                              id: "other",
                                              type: "checkbox",
                                              title: "Others"
                                            },
                                            {
                                              type: "TEXT",
                                              title: "Please Specify",
                                              id: "resultOther",
                                              max: 50,
                                              detailSeq: 4,
                                              trigger: {
                                                id: "other",
                                                type: "showIfEqual",
                                                value: "Y"
                                              }
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  type: "BLOCK",
                                  title:
                                    "Details of Previous and Concurrent Policies",
                                  id: "policies",
                                  items: [
                                    {
                                      id: "havExtPlans",
                                      mandatory: true,
                                      title:
                                        "1. Do you have any existing life insurance policies or other investment products?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      detailSeq: 1,
                                      align: "left",
                                      disableTrigger: {
                                        id: "@extra/channel",
                                        value: "AGENCY",
                                        type: "trueIfEqual"
                                      },
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      id: "havPndinApp",
                                      mandatory: true,
                                      title:
                                        "2. Other than this current application, do you have any other pending or concurrent insurance applications with AXA and/or other companies?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      align: "left",
                                      detailSeq: 2,
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      id: "isProslReplace",
                                      mandatory: true,
                                      title:
                                        "3. Is this proposal to replace or intended to replace (in part or full) any existing or recently terminated insurance policy, or other investment product from AXA Insurance Pte Ltd or other Financial Institution?",
                                      type: "QRADIOGROUP",
                                      horizontal: true,
                                      align: "left",
                                      detailSeq: 3,
                                      disableTrigger: {
                                        id: "@extra/channel",
                                        value: "AGENCY",
                                        type: "trueIfEqual"
                                      },
                                      trigger: {
                                        id: "extra/isPhSameAsLa",
                                        value: "Y",
                                        type: "showIfEqual"
                                      },
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    },
                                    {
                                      type: "PARAGRAPH",
                                      id: "pWarning",
                                      title: "WARNING NOTE:",
                                      paragraphType: "warningWithNote",
                                      align: "left",
                                      content:
                                        "<p>It is usually disadvantageous to replace an existing life insurance policy or other investment product with a new one. Some of the factors to consider:</p><p>1. You may suffer a penalty for terminating the original policy or other investment product.</p><p>2. You may incur transaction costs without gaining any real benefit from replacing the policy or other investment product.</p><p>3. You may not be insurable on standard terms or may have to pay a higher premium in view of higher age or the financial benefits accumulated over the years may be lost.</p><p>In your own interest, we would advise that you consult your own financial consultant before making a final decision. Hear from both sides and make a careful comparison to ensure that you are making a decision that is in your best interest.</p>",
                                      detailSeq: 4,
                                      trigger: {
                                        id: "isProslReplace",
                                        value: "Y",
                                        type: "showIfEqual"
                                      }
                                    },
                                    {
                                      id: "saTable",
                                      type: "saTable",
                                      presentation:
                                        "havExtPlans$havPndinApp$isProslReplace"
                                    },
                                    {
                                      id: "Eltest_field",
                                      type: "goFNA",
                                      trigger: {
                                        id: "@extra/channel",
                                        value: "FA",
                                        type: "showIfNotEqual"
                                      }
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      title: {
                        en: "Insurability Information"
                      },
                      type: "menuItem",
                      key: "menu_insure",
                      detailSeq: 2,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              type: "tab",
                              items: [
                                {
                                  id: "insurability",
                                  type: "block",
                                  items: [
                                    {
                                      type: "block",
                                      title: "Height and Weight",
                                      items: [
                                        {
                                          type: "hbox",
                                          items: [
                                            {
                                              id: "HW01",
                                              type: "text",
                                              subType: "number",
                                              mandatory: true,
                                              max: 3,
                                              min: 0,
                                              decimal: 2,
                                              title: "Height (m, meter)"
                                            },
                                            {
                                              id: "HW02",
                                              mandatory: true,
                                              type: "text",
                                              subType: "number",
                                              max: 999,
                                              min: 0,
                                              decimal: 0,
                                              title: "Weight (kg, kilogram)"
                                            }
                                          ]
                                        },
                                        {
                                          id: "HW03",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          horizontal: true,
                                          align: "left",
                                          title:
                                            "Any weight change in the last 12 months? (kg)",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "hbox",
                                          trigger: {
                                            id: "HW03",
                                            value: "Y",
                                            type: "showIfEqual"
                                          },
                                          items: [
                                            {
                                              id: "HW03a",
                                              type: "QRADIOGROUP",
                                              horizontal: true,
                                              mandatory: true,
                                              align: "left",
                                              title: "Weight change",
                                              options: [
                                                {
                                                  value: "G",
                                                  title: "Gain"
                                                },
                                                {
                                                  value: "L",
                                                  title: "Lost"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HW03b",
                                              mandatory: true,
                                              type: "text",
                                              subType: "number",
                                              max: 999,
                                              min: 0,
                                              decimal: 0,
                                              title: "How many kg?"
                                            }
                                          ]
                                        },
                                        {
                                          type: "hbox",
                                          items: [
                                            {
                                              id: "HW03c",
                                              mandatory: true,
                                              type: "picker",
                                              trigger: {
                                                id: "HW03",
                                                value: "Y",
                                                type: "showIfEqual"
                                              },
                                              title: "Reason of weight change",
                                              options: [
                                                {
                                                  title: "Diet Control",
                                                  value: "diet"
                                                },
                                                {
                                                  title: "Exercising",
                                                  value: "exercise"
                                                },
                                                {
                                                  title: "Pregnancy",
                                                  value: "pregnancy"
                                                },
                                                {
                                                  title: "Post delivery",
                                                  value: "pdelivery"
                                                },
                                                {
                                                  title: "Other",
                                                  value: "other"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HW03c1",
                                              title: {
                                                en: "Please specify/Others"
                                              },
                                              type: "text",
                                              max: 100,
                                              mandatory: true,
                                              trigger: {
                                                id: "HW03c",
                                                type: "showIfEqual",
                                                value: "other"
                                              }
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      title: "Insurance History",
                                      items: [
                                        {
                                          id: "INS01",
                                          title:
                                            "1. Have you ever made an application or application for reinstatement of a life, disability, accident, medical or critical illness insurance which has been accepted with an extra premium or on special terms, postponed, declined, withdrawn or is still being considered?",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          horizontal: true,
                                          align: "left",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              title: "Yes",
                                              value: "Y"
                                            },
                                            {
                                              title: "No",
                                              value: "N"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          mandatory: true,
                                          id: "INS01_DATA",
                                          trigger: {
                                            id: "INS01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          allowEdit: true,
                                          hasBorder: true,
                                          items: [
                                            {
                                              id: "INS01a",
                                              type: "text",
                                              title:
                                                "Name of Insurance Company",
                                              mandatory: true,
                                              max: 50,
                                              width: "20%"
                                            },
                                            {
                                              type: "table-block-t",
                                              title: "Type of Coverage",
                                              presentation:
                                                "INS01b1$INS01b2$INS01b3$INS01b4$INS01b5",
                                              width: "20%",
                                              mandatory: true,
                                              key: "INS01a1-b",
                                              items: [
                                                {
                                                  id: "INS01b1",
                                                  type: "checkbox",
                                                  title: "Life"
                                                },
                                                {
                                                  id: "INS01b2",
                                                  type: "checkbox",
                                                  title:
                                                    "Total Permanent Disability"
                                                },
                                                {
                                                  id: "INS01b3",
                                                  type: "checkbox",
                                                  title: "Critical Illness"
                                                },
                                                {
                                                  id: "INS01b4",
                                                  type: "checkbox",
                                                  title: "Accident"
                                                },
                                                {
                                                  id: "INS01b5",
                                                  type: "checkbox",
                                                  title:
                                                    "Hospitalisation & Surgical  Benefit"
                                                }
                                              ]
                                            },
                                            {
                                              id: "INS01c",
                                              type: "DATEPICKER",
                                              width: "10%",
                                              title: "Date incurred (MM/YYYY)",
                                              mandatory: true,
                                              max: 0,
                                              min: 100,
                                              dateFormat: "mm/yyyy"
                                            },
                                            {
                                              title:
                                                "Condition of Special Terms ",
                                              mandatory: true,
                                              id: "INS01d",
                                              type: "picker",
                                              width: "25%",
                                              options: [
                                                {
                                                  value: "medical",
                                                  title: "Medical"
                                                },
                                                {
                                                  value: "residential",
                                                  title: "Residential"
                                                },
                                                {
                                                  value: "financial",
                                                  title: "Financial"
                                                },
                                                {
                                                  value: "others",
                                                  title: "Others"
                                                }
                                              ]
                                            },
                                            {
                                              title:
                                                "Decision & Detailed Reason(s) of special terms ",
                                              mandatory: true,
                                              id: "INS01e",
                                              type: "text",
                                              max: 200,
                                              width: "25%"
                                            }
                                          ]
                                        },
                                        {
                                          id: "INS02",
                                          title:
                                            "2. Are you presently receiving a disability benefit or incapable for work or have you ever made or intend to make any claim against any insurer for disability, accident, medical care, hospitalisation, critical illness and/or other benefits?",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          horizontal: true,
                                          align: "left",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              title: "Yes",
                                              value: "Y"
                                            },
                                            {
                                              title: "No",
                                              value: "N"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          mandatory: true,
                                          id: "INS02_DATA",
                                          trigger: {
                                            id: "INS02",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          allowEdit: true,
                                          hasBorder: true,
                                          items: [
                                            {
                                              id: "INS02a",
                                              mandatory: true,
                                              type: "text",
                                              title:
                                                "Name of Insurance Company",
                                              max: 50,
                                              width: "20%"
                                            },
                                            {
                                              type: "table-block-t",
                                              key: "INS02b-b",
                                              mandatory: true,
                                              title: "Type of the Claim",
                                              presentation:
                                                "INS02b1$INS02b2$INS02b3$INS02b4$other",
                                              width: "20%",
                                              items: [
                                                {
                                                  id: "INS02b1",
                                                  type: "checkbox",
                                                  title:
                                                    "Total Permanent Disability"
                                                },
                                                {
                                                  id: "INS02b2",
                                                  type: "checkbox",
                                                  title: "Critical Illness"
                                                },
                                                {
                                                  id: "INS02b3",
                                                  type: "checkbox",
                                                  title: "Accident"
                                                },
                                                {
                                                  id: "INS02b4",
                                                  type: "checkbox",
                                                  title:
                                                    "Hospitalisation & Surgical  Benefit"
                                                },
                                                {
                                                  id: "other",
                                                  type: "checkbox",
                                                  title: "Others"
                                                },
                                                {
                                                  id: "resultOther",
                                                  mandatory: true,
                                                  type: "text",
                                                  placeholder: "Others",
                                                  max: 20,
                                                  trigger: {
                                                    id: "other",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              id: "INS02c",
                                              type: "DATEPICKER",
                                              width: "10%",
                                              mandatory: true,
                                              title: "Date incurred (MM/YYYY)",
                                              max: 0,
                                              min: 100,
                                              dateFormat: "mm/yyyy"
                                            },
                                            {
                                              id: "INS02d",
                                              mandatory: true,
                                              title: "Medical Condition",
                                              type: "text",
                                              width: "25%",
                                              max: 200
                                            },
                                            {
                                              title:
                                                "Claim Details (Duration of hospitalisation /offwork, treatment, follow-up etc)",
                                              id: "INS02e",
                                              mandatory: true,
                                              type: "text",
                                              max: 200,
                                              width: "25%"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Lifestyle and Habits",
                                      type: "block",
                                      items: [
                                        {
                                          id: "LIFESTYLE01",
                                          title:
                                            "1. Have you smoked or used any tobacco, nicotine or smokeless tobacco products (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.) within the past 12 months?",
                                          type: "QRADIOGROUP",
                                          disabled: true,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "block",
                                          trigger: {
                                            id: "LIFESTYLE01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          items: [
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "LIFESTYLE01a",
                                                  mandatory: true,
                                                  type: "picker",
                                                  title:
                                                    "Type of product (e.g. cigarettes, cigar, e-cigarettes, pipes, nicotine patch, etc.)",
                                                  options: [
                                                    {
                                                      value: "cigarettes",
                                                      title: "Cigarettes"
                                                    },
                                                    {
                                                      value: "cigar",
                                                      title: "Cigar"
                                                    },
                                                    {
                                                      value: "eCi",
                                                      title: "e-Cigarettes"
                                                    },
                                                    {
                                                      value: "pipes",
                                                      title: "Pipes"
                                                    },
                                                    {
                                                      value: "nicotine",
                                                      title: "Nicotine patch"
                                                    },
                                                    {
                                                      value: "other",
                                                      title: "Other"
                                                    }
                                                  ]
                                                },
                                                {
                                                  id: "LIFESTYLE01a_OTH",
                                                  mandatory: true,
                                                  type: "text",
                                                  placeHolder: {
                                                    en: "Others"
                                                  },
                                                  max: 50,
                                                  trigger: {
                                                    id: "LIFESTYLE01a",
                                                    type: "showIfEqual",
                                                    value: "other"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "LIFESTYLE01b",
                                                  mandatory: true,
                                                  title:
                                                    "Number of products smoked per day (i.e. how many sticks/ pipes/ patches per day?)",
                                                  type: "text",
                                                  subType: "number",
                                                  max: 999,
                                                  min: 0
                                                }
                                              ]
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "LIFESTYLE01c",
                                                  mandatory: true,
                                                  title:
                                                    "Number of years smoked",
                                                  type: "text",
                                                  subType: "number",
                                                  max: 99,
                                                  min: 0
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          id: "LIFESTYLE02",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          horizontal: true,
                                          align: "left",
                                          title: "2. Do you consume alcohol?",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "BLOCK",
                                          trigger: {
                                            id: "LIFESTYLE02",
                                            value: "Y",
                                            type: "showIfEqual"
                                          },
                                          items: [
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  type: "qtitle",
                                                  key: "LIFESTYLE02-b",
                                                  title:
                                                    "How many alcoholic drinks do you drink {{per week}}, on average?",
                                                  mandatory: true,
                                                  needToHighlight: true
                                                }
                                              ]
                                            },
                                            {
                                              id: "LifeStyleNote",
                                              type: "PARAGRAPH",
                                              style: {
                                                color: "#ec4d33"
                                              },
                                              content:
                                                "<p style='padding: 0px; line-height: 16px'>Note: ONE standard unit of alcoholic drink equates to Beer 330ml/can, Wine 125ml/glass, or Spirits 30ml/cup.</p>"
                                            },
                                            {
                                              type: "HBOX",
                                              key: "checkboxgroup",
                                              validation:
                                                "function(i,v,rv,vrv){if(v.LIFESTYLE02a_1!='Y' && v.LIFESTYLE02b_1!='Y' && v.LIFESTYLE02c_1!='Y'){return 301;}}",
                                              items: [
                                                {
                                                  id: "LIFESTYLE02a_1",
                                                  type: "checkbox",
                                                  title: "Beer (330 ml/can)"
                                                },
                                                {
                                                  id: "LIFESTYLE02a_2",
                                                  mandatory: true,
                                                  type: "text",
                                                  subType: "number",
                                                  title: "can(s)",
                                                  max: 999,
                                                  trigger: {
                                                    id: "LIFESTYLE02a_1",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "LIFESTYLE02b_1",
                                                  type: "checkbox",
                                                  title: "Wine (125ml/glass)"
                                                },
                                                {
                                                  id: "LIFESTYLE02b_2",
                                                  mandatory: true,
                                                  type: "text",
                                                  subType: "number",
                                                  title: "glass(es)",
                                                  max: 999,
                                                  trigger: {
                                                    id: "LIFESTYLE02b_1",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "LIFESTYLE02c_1",
                                                  type: "checkbox",
                                                  title: "Spirits (30ml/cup)"
                                                },
                                                {
                                                  id: "LIFESTYLE02c_2",
                                                  mandatory: true,
                                                  type: "text",
                                                  subType: "number",
                                                  title: "cup(s)",
                                                  max: 999,
                                                  trigger: {
                                                    id: "LIFESTYLE02c_1",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          id: "LIFESTYLE03",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          horizontal: true,
                                          align: "left",
                                          title:
                                            "3. Have you ever used any habit forming drugs or narcotics or been treated for drug habits? ",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          id: "LIFESTYLE03_DATA",
                                          mandatory: true,
                                          hasBorder: true,
                                          allowEdit: true,
                                          trigger: {
                                            id: "LIFESTYLE03",
                                            value: "Y",
                                            type: "showIfEqual"
                                          },
                                          items: [
                                            {
                                              id: "LIFESTYLE03a",
                                              mandatory: true,
                                              type: "text",
                                              title: "Substances used",
                                              max: 50,
                                              width: "20%"
                                            },
                                            {
                                              id: "LIFESTYLE03b",
                                              type: "DATEPICKER",
                                              width: "10%",
                                              mandatory: true,
                                              title: "Date Commenced (MM/YYYY)",
                                              max: 0,
                                              min: 100,
                                              dateFormat: "mm/yyyy"
                                            },
                                            {
                                              id: "LIFESTYLE03c",
                                              type: "DATEPICKER",
                                              width: "10%",
                                              mandatory: true,
                                              title: "Date Ceased (MM/YYYY)",
                                              max: 0,
                                              min: 100,
                                              dateFormat: "mm/yyyy",
                                              validation: {
                                                id: "LIFESTYLE03b",
                                                type: "month_greater"
                                              }
                                            },
                                            {
                                              id: "LIFESTYLE03d",
                                              mandatory: true,
                                              title:
                                                "Details (e.g. Treatment, Any other medical condition, Any relapses/complications etc) ",
                                              type: "text",
                                              max: 200,
                                              width: "40%"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      title: "Family History",
                                      type: "block",
                                      items: [
                                        {
                                          type: "QRADIOGROUP",
                                          id: "FAMILY01",
                                          mandatory: true,
                                          horizontal: true,
                                          needToHighlight: true,
                                          title:
                                            "Has your biological mother, father, or any sister or brother been diagnosed {{prior to age 60}} with any of the following?  Cancer, heart disease, stroke, diabetes, Huntington's disease, polycystic kidney disease, Multiple Sclerosis, Alzheimer's or any other inherited conditions.",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          id: "FAMILY01_DATA",
                                          mandatory: true,
                                          hasBorder: true,
                                          allowEdit: true,
                                          trigger: {
                                            id: "FAMILY01",
                                            value: "Y",
                                            type: "showIfEqual"
                                          },
                                          items: [
                                            {
                                              id: "FAMILY01a",
                                              title: "Relationship",
                                              mandatory: true,
                                              type: "picker",
                                              width: "15%",
                                              options: [
                                                {
                                                  title: "Mother",
                                                  value: "monther"
                                                },
                                                {
                                                  title: "Father",
                                                  value: "Father"
                                                },
                                                {
                                                  title: "Brother",
                                                  value: "Brother"
                                                },
                                                {
                                                  title: "Sister",
                                                  value: "Sister"
                                                }
                                              ],
                                              max: 50
                                            },
                                            {
                                              id: "FAMILY01b",
                                              mandatory: true,
                                              title: "Medical Condition",
                                              type: "text",
                                              width: "70%",
                                              max: 200
                                            },
                                            {
                                              id: "FAMILY01c",
                                              mandatory: true,
                                              title: "Age of Onset",
                                              type: "text",
                                              subType: "number",
                                              width: "15%",
                                              max: 59
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "block",
                                      title: "Medical and Health Information",
                                      items: [
                                        {
                                          id: "HEALTH_GIO01",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          horizontal: true,
                                          align: "left",
                                          title:
                                            "1. During the last 3 years, have you ever been hospitalized or have you consulted a medical practitioner for any medical condition that required medical treatment for over 14 consecutive days, or are you intending to do so, or have you had or been advised to have any operation, test or treatment?*",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          id: "HEALTH_Note",
                                          type: "PARAGRAPH",
                                          content:
                                            "<p style='padding: 0px; line-height: 16px; font-size:13px'>*Consultations, tests or treatment for the following conditions can be ignored: common cold, fever or flu; uncomplicated pregnancy or caesarean sections; contraception, inoculations, minor joint or muscle injuries or uncomplicated bone fractures from which you have fully recovered.</p>"
                                        },
                                        {
                                          type: "table",
                                          mandatory: true,
                                          id: "HEALTH_GIO01_DATA",
                                          trigger: {
                                            id: "HEALTH_GIO01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          allowEdit: true,
                                          hasBorder: true,
                                          items: [
                                            {
                                              id: "HEALTH_GIO01a",
                                              mandatory: true,
                                              type: "text",
                                              title:
                                                "Medical Condition/Diagnosis",
                                              max: 200,
                                              width: "14%"
                                            },
                                            {
                                              id: "HEALTH_GIO01b",
                                              mandatory: true,
                                              title: "Year of diagnosis",
                                              type: "picker",
                                              subType: "selectField",
                                              optionsTable: "years",
                                              width: "10%",
                                              options: [
                                                {
                                                  value: "2018",
                                                  title: "2018"
                                                },
                                                {
                                                  value: "2017",
                                                  title: "2017"
                                                },
                                                {
                                                  value: "2016",
                                                  title: "2016"
                                                },
                                                {
                                                  value: "2015",
                                                  title: "2015"
                                                },
                                                {
                                                  value: "2014",
                                                  title: "2014"
                                                },
                                                {
                                                  value: "2013",
                                                  title: "2013"
                                                },
                                                {
                                                  value: "2012",
                                                  title: "2012"
                                                },
                                                {
                                                  value: "2011",
                                                  title: "2011"
                                                },
                                                {
                                                  value: "2010",
                                                  title: "2010"
                                                },
                                                {
                                                  value: "2009",
                                                  title: "2009"
                                                },
                                                {
                                                  value: "2008",
                                                  title: "2008"
                                                },
                                                {
                                                  value: "2007",
                                                  title: "2007"
                                                },
                                                {
                                                  value: "2006",
                                                  title: "2006"
                                                },
                                                {
                                                  value: "2005",
                                                  title: "2005"
                                                },
                                                {
                                                  value: "2004",
                                                  title: "2004"
                                                },
                                                {
                                                  value: "2003",
                                                  title: "2003"
                                                },
                                                {
                                                  value: "2002",
                                                  title: "2002"
                                                },
                                                {
                                                  value: "2001",
                                                  title: "2001"
                                                },
                                                {
                                                  value: "2000",
                                                  title: "2000"
                                                },
                                                {
                                                  value: "1999",
                                                  title: "1999"
                                                },
                                                {
                                                  value: "1998",
                                                  title: "1998"
                                                },
                                                {
                                                  value: "1997",
                                                  title: "1997"
                                                },
                                                {
                                                  value: "1996",
                                                  title: "1996"
                                                },
                                                {
                                                  value: "1995",
                                                  title: "1995"
                                                },
                                                {
                                                  value: "1994",
                                                  title: "1994"
                                                },
                                                {
                                                  value: "1993",
                                                  title: "1993"
                                                },
                                                {
                                                  value: "1992",
                                                  title: "1992"
                                                },
                                                {
                                                  value: "1991",
                                                  title: "1991"
                                                },
                                                {
                                                  value: "1990",
                                                  title: "1990"
                                                },
                                                {
                                                  value: "1989",
                                                  title: "1989"
                                                },
                                                {
                                                  value: "1988",
                                                  title: "1988"
                                                },
                                                {
                                                  value: "1987",
                                                  title: "1987"
                                                },
                                                {
                                                  value: "1986",
                                                  title: "1986"
                                                },
                                                {
                                                  value: "1985",
                                                  title: "1985"
                                                },
                                                {
                                                  value: "1984",
                                                  title: "1984"
                                                },
                                                {
                                                  value: "1983",
                                                  title: "1983"
                                                },
                                                {
                                                  value: "1982",
                                                  title: "1982"
                                                },
                                                {
                                                  value: "1981",
                                                  title: "1981"
                                                },
                                                {
                                                  value: "1980",
                                                  title: "1980"
                                                },
                                                {
                                                  value: "1979",
                                                  title: "1979"
                                                },
                                                {
                                                  value: "1978",
                                                  title: "1978"
                                                },
                                                {
                                                  value: "1977",
                                                  title: "1977"
                                                },
                                                {
                                                  value: "1976",
                                                  title: "1976"
                                                },
                                                {
                                                  value: "1975",
                                                  title: "1975"
                                                },
                                                {
                                                  value: "1974",
                                                  title: "1974"
                                                },
                                                {
                                                  value: "1973",
                                                  title: "1973"
                                                },
                                                {
                                                  value: "1972",
                                                  title: "1972"
                                                },
                                                {
                                                  value: "1971",
                                                  title: "1971"
                                                },
                                                {
                                                  value: "1970",
                                                  title: "1970"
                                                },
                                                {
                                                  value: "1969",
                                                  title: "1969"
                                                },
                                                {
                                                  value: "1968",
                                                  title: "1968"
                                                },
                                                {
                                                  value: "1967",
                                                  title: "1967"
                                                },
                                                {
                                                  value: "1966",
                                                  title: "1966"
                                                },
                                                {
                                                  value: "1965",
                                                  title: "1965"
                                                },
                                                {
                                                  value: "1964",
                                                  title: "1964"
                                                },
                                                {
                                                  value: "1963",
                                                  title: "1963"
                                                },
                                                {
                                                  value: "1962",
                                                  title: "1962"
                                                },
                                                {
                                                  value: "1961",
                                                  title: "1961"
                                                },
                                                {
                                                  value: "1960",
                                                  title: "1960"
                                                },
                                                {
                                                  value: "1959",
                                                  title: "1959"
                                                },
                                                {
                                                  value: "1958",
                                                  title: "1958"
                                                },
                                                {
                                                  value: "1957",
                                                  title: "1957"
                                                },
                                                {
                                                  value: "1956",
                                                  title: "1956"
                                                },
                                                {
                                                  value: "1955",
                                                  title: "1955"
                                                },
                                                {
                                                  value: "1954",
                                                  title: "1954"
                                                },
                                                {
                                                  value: "1953",
                                                  title: "1953"
                                                },
                                                {
                                                  value: "1952",
                                                  title: "1952"
                                                },
                                                {
                                                  value: "1951",
                                                  title: "1951"
                                                },
                                                {
                                                  value: "1950",
                                                  title: "1950"
                                                },
                                                {
                                                  value: "1949",
                                                  title: "1949"
                                                },
                                                {
                                                  value: "1948",
                                                  title: "1948"
                                                },
                                                {
                                                  value: "1947",
                                                  title: "1947"
                                                },
                                                {
                                                  value: "1946",
                                                  title: "1946"
                                                },
                                                {
                                                  value: "1945",
                                                  title: "1945"
                                                },
                                                {
                                                  value: "1944",
                                                  title: "1944"
                                                },
                                                {
                                                  value: "1943",
                                                  title: "1943"
                                                },
                                                {
                                                  value: "1942",
                                                  title: "1942"
                                                },
                                                {
                                                  value: "1941",
                                                  title: "1941"
                                                },
                                                {
                                                  value: "1940",
                                                  title: "1940"
                                                },
                                                {
                                                  value: "1939",
                                                  title: "1939"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HEALTH_GIO01c",
                                              type: "text",
                                              title:
                                                "Type of test done (if any)",
                                              max: 200,
                                              width: "14%"
                                            },
                                            {
                                              id: "HEALTH_GIO01d",
                                              type: "datepicker",
                                              subType: "date",
                                              title:
                                                "Date of test done (if any)",
                                              max: 0,
                                              min: 100,
                                              width: "10%"
                                            },
                                            {
                                              id: "HEALTH_GIO01e",
                                              type: "QRADIOGROUP",
                                              align: "left",
                                              title: "Test result (if any)",
                                              max: 200,
                                              width: "14%",
                                              options: [
                                                {
                                                  value: "Y",
                                                  title: "Normal"
                                                },
                                                {
                                                  value: "N",
                                                  title: "Abnormal"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HEALTH_GIO01f",
                                              mandatory: true,
                                              type: "text",
                                              title:
                                                "Details  (Medication/Treatment / Follow up / Complication) ",
                                              width: "10%",
                                              max: 200
                                            },
                                            {
                                              id: "HEALTH_GIO01g",
                                              type: "text",
                                              title: "Name of Doctor",
                                              width: "14%",
                                              max: 200
                                            },
                                            {
                                              id: "HEALTH_GIO01h",
                                              type: "text",
                                              title:
                                                "Address of Hospital/Clinic",
                                              width: "14%",
                                              max: 200
                                            }
                                          ]
                                        },
                                        {
                                          id: "HEALTH_GIO02",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          horizontal: true,
                                          align: "left",
                                          title:
                                            "2. Have you ever had or been told that you have, or have been treated for cancer (including carcinoma-in-situ), growth or tumor of any kind, diabetes, high blood pressure, chest pain, stroke, heart diseases, blood disorder, respiratory diseases, kidney diseases, bowel diseases, hepatitis or liver diseases, nervous or mental disorders, spinal disorders, muscular or joint disorders, AIDS or HIV related conditions, or any other serious illness or impairment?",
                                          valueTrigger: {
                                            type: "resetTargetValue",
                                            id: "insAllNoInd",
                                            value: "Y,N"
                                          },
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          mandatory: true,
                                          id: "HEALTH_GIO02_DATA",
                                          trigger: {
                                            id: "HEALTH_GIO02",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          allowEdit: true,
                                          hasBorder: true,
                                          items: [
                                            {
                                              id: "HEALTH_GIO02a",
                                              mandatory: true,
                                              type: "text",
                                              title:
                                                "Medical Condition/Diagnosis",
                                              max: 200,
                                              width: "14%"
                                            },
                                            {
                                              id: "HEALTH_GIO02b",
                                              mandatory: true,
                                              title: "Year of diagnosis",
                                              type: "picker",
                                              subType: "selectField",
                                              optionsTable: "years",
                                              width: "10%",
                                              options: [
                                                {
                                                  value: "2018",
                                                  title: "2018"
                                                },
                                                {
                                                  value: "2017",
                                                  title: "2017"
                                                },
                                                {
                                                  value: "2016",
                                                  title: "2016"
                                                },
                                                {
                                                  value: "2015",
                                                  title: "2015"
                                                },
                                                {
                                                  value: "2014",
                                                  title: "2014"
                                                },
                                                {
                                                  value: "2013",
                                                  title: "2013"
                                                },
                                                {
                                                  value: "2012",
                                                  title: "2012"
                                                },
                                                {
                                                  value: "2011",
                                                  title: "2011"
                                                },
                                                {
                                                  value: "2010",
                                                  title: "2010"
                                                },
                                                {
                                                  value: "2009",
                                                  title: "2009"
                                                },
                                                {
                                                  value: "2008",
                                                  title: "2008"
                                                },
                                                {
                                                  value: "2007",
                                                  title: "2007"
                                                },
                                                {
                                                  value: "2006",
                                                  title: "2006"
                                                },
                                                {
                                                  value: "2005",
                                                  title: "2005"
                                                },
                                                {
                                                  value: "2004",
                                                  title: "2004"
                                                },
                                                {
                                                  value: "2003",
                                                  title: "2003"
                                                },
                                                {
                                                  value: "2002",
                                                  title: "2002"
                                                },
                                                {
                                                  value: "2001",
                                                  title: "2001"
                                                },
                                                {
                                                  value: "2000",
                                                  title: "2000"
                                                },
                                                {
                                                  value: "1999",
                                                  title: "1999"
                                                },
                                                {
                                                  value: "1998",
                                                  title: "1998"
                                                },
                                                {
                                                  value: "1997",
                                                  title: "1997"
                                                },
                                                {
                                                  value: "1996",
                                                  title: "1996"
                                                },
                                                {
                                                  value: "1995",
                                                  title: "1995"
                                                },
                                                {
                                                  value: "1994",
                                                  title: "1994"
                                                },
                                                {
                                                  value: "1993",
                                                  title: "1993"
                                                },
                                                {
                                                  value: "1992",
                                                  title: "1992"
                                                },
                                                {
                                                  value: "1991",
                                                  title: "1991"
                                                },
                                                {
                                                  value: "1990",
                                                  title: "1990"
                                                },
                                                {
                                                  value: "1989",
                                                  title: "1989"
                                                },
                                                {
                                                  value: "1988",
                                                  title: "1988"
                                                },
                                                {
                                                  value: "1987",
                                                  title: "1987"
                                                },
                                                {
                                                  value: "1986",
                                                  title: "1986"
                                                },
                                                {
                                                  value: "1985",
                                                  title: "1985"
                                                },
                                                {
                                                  value: "1984",
                                                  title: "1984"
                                                },
                                                {
                                                  value: "1983",
                                                  title: "1983"
                                                },
                                                {
                                                  value: "1982",
                                                  title: "1982"
                                                },
                                                {
                                                  value: "1981",
                                                  title: "1981"
                                                },
                                                {
                                                  value: "1980",
                                                  title: "1980"
                                                },
                                                {
                                                  value: "1979",
                                                  title: "1979"
                                                },
                                                {
                                                  value: "1978",
                                                  title: "1978"
                                                },
                                                {
                                                  value: "1977",
                                                  title: "1977"
                                                },
                                                {
                                                  value: "1976",
                                                  title: "1976"
                                                },
                                                {
                                                  value: "1975",
                                                  title: "1975"
                                                },
                                                {
                                                  value: "1974",
                                                  title: "1974"
                                                },
                                                {
                                                  value: "1973",
                                                  title: "1973"
                                                },
                                                {
                                                  value: "1972",
                                                  title: "1972"
                                                },
                                                {
                                                  value: "1971",
                                                  title: "1971"
                                                },
                                                {
                                                  value: "1970",
                                                  title: "1970"
                                                },
                                                {
                                                  value: "1969",
                                                  title: "1969"
                                                },
                                                {
                                                  value: "1968",
                                                  title: "1968"
                                                },
                                                {
                                                  value: "1967",
                                                  title: "1967"
                                                },
                                                {
                                                  value: "1966",
                                                  title: "1966"
                                                },
                                                {
                                                  value: "1965",
                                                  title: "1965"
                                                },
                                                {
                                                  value: "1964",
                                                  title: "1964"
                                                },
                                                {
                                                  value: "1963",
                                                  title: "1963"
                                                },
                                                {
                                                  value: "1962",
                                                  title: "1962"
                                                },
                                                {
                                                  value: "1961",
                                                  title: "1961"
                                                },
                                                {
                                                  value: "1960",
                                                  title: "1960"
                                                },
                                                {
                                                  value: "1959",
                                                  title: "1959"
                                                },
                                                {
                                                  value: "1958",
                                                  title: "1958"
                                                },
                                                {
                                                  value: "1957",
                                                  title: "1957"
                                                },
                                                {
                                                  value: "1956",
                                                  title: "1956"
                                                },
                                                {
                                                  value: "1955",
                                                  title: "1955"
                                                },
                                                {
                                                  value: "1954",
                                                  title: "1954"
                                                },
                                                {
                                                  value: "1953",
                                                  title: "1953"
                                                },
                                                {
                                                  value: "1952",
                                                  title: "1952"
                                                },
                                                {
                                                  value: "1951",
                                                  title: "1951"
                                                },
                                                {
                                                  value: "1950",
                                                  title: "1950"
                                                },
                                                {
                                                  value: "1949",
                                                  title: "1949"
                                                },
                                                {
                                                  value: "1948",
                                                  title: "1948"
                                                },
                                                {
                                                  value: "1947",
                                                  title: "1947"
                                                },
                                                {
                                                  value: "1946",
                                                  title: "1946"
                                                },
                                                {
                                                  value: "1945",
                                                  title: "1945"
                                                },
                                                {
                                                  value: "1944",
                                                  title: "1944"
                                                },
                                                {
                                                  value: "1943",
                                                  title: "1943"
                                                },
                                                {
                                                  value: "1942",
                                                  title: "1942"
                                                },
                                                {
                                                  value: "1941",
                                                  title: "1941"
                                                },
                                                {
                                                  value: "1940",
                                                  title: "1940"
                                                },
                                                {
                                                  value: "1939",
                                                  title: "1939"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HEALTH_GIO02c",
                                              type: "text",
                                              title:
                                                "Type of test done (if any)",
                                              max: 200,
                                              width: "14%"
                                            },
                                            {
                                              id: "HEALTH_GIO02d",
                                              type: "datepicker",
                                              subType: "date",
                                              title:
                                                "Date of test done (if any)",
                                              max: 0,
                                              min: 100,
                                              width: "10%"
                                            },
                                            {
                                              id: "HEALTH_GIO02e",
                                              type: "QRADIOGROUP",
                                              align: "left",
                                              title: "Test result (if any)",
                                              max: 200,
                                              width: "14%",
                                              options: [
                                                {
                                                  value: "Y",
                                                  title: "Normal"
                                                },
                                                {
                                                  value: "N",
                                                  title: "Abnormal"
                                                }
                                              ]
                                            },
                                            {
                                              id: "HEALTH_GIO02f",
                                              mandatory: true,
                                              type: "text",
                                              title:
                                                "Details  (Medication/Treatment / Follow up / Complication) ",
                                              width: "10%",
                                              max: 200
                                            },
                                            {
                                              id: "HEALTH_GIO02g",
                                              type: "text",
                                              title: "Name of Doctor",
                                              width: "14%",
                                              max: 200
                                            },
                                            {
                                              id: "HEALTH_GIO02h",
                                              type: "text",
                                              title:
                                                "Address of Hospital/Clinic",
                                              width: "14%",
                                              max: 200
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      id: "insAllNoInd",
                                      type: "checkbox",
                                      disableTrigger: {
                                        type: "AnyFieldMatch",
                                        id:
                                          "HW03,INS01,INS02,LIFESTYLE02,LIFESTYLE03,FAMILY01,HEALTH_GIO01,HEALTH_GIO02",
                                        value: "Y"
                                      },
                                      valueTrigger: {
                                        type: "resetTargetValue",
                                        id:
                                          "HW03,INS01,INS02,LIFESTYLE02,LIFESTYLE03,FAMILY01,HEALTH_GIO01,HEALTH_GIO02",
                                        value: "Y,N"
                                      },
                                      title:
                                        'I confirm the answer to all of the above medical and health questions for life assured is "no"'
                                    },
                                    {
                                      type: "paragraph",
                                      align: "left",
                                      style: {
                                        color: "#ec4d33"
                                      },
                                      content:
                                        "<p><b>Note: </b>The above declaration does not apply to smoking question, which is autopopulated from Client Profile.</p>  "
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "menuSection",
                  detailSeq: 1,
                  items: [
                    {
                      title: {
                        en: "Plan Details"
                      },
                      type: "menuItem",
                      skipCheck: true,
                      key: "menu_plan",
                      detailSeq: 0,
                      items: [
                        {
                          type: "BLOCK",
                          id: "planDetails",
                          title: "",
                          items: [
                            {
                              type: "block",
                              style: {
                                paddingBottom: "30px",
                                borderBottom: "1px solid #cac4c4"
                              },
                              items: [
                                {
                                  type: "heading",
                                  id: "planTypeName",
                                  detailSeq: 1
                                },
                                {
                                  type: "table",
                                  no1stItemLeftPadding: true,
                                  id: "planList",
                                  allowEdit: false,
                                  items: [
                                    {
                                      type: "HBOX",
                                      title: "Basic Plan/ Rider",
                                      presentation: "planCover@covName",
                                      items: [
                                        {
                                          type: "image",
                                          id: "planCover"
                                        },
                                        {
                                          type: "TEXT",
                                          id: "covName"
                                        }
                                      ]
                                    },
                                    {
                                      type: "TEXT",
                                      id: "sumInsured",
                                      title: "Sum Assured/ Benefits",
                                      subType: "currency",
                                      decimal: 0,
                                      right: true
                                    },
                                    {
                                      type: "TEXT",
                                      id: "polTermDesc",
                                      title: "Policy Term",
                                      right: true
                                    },
                                    {
                                      type: "TEXT",
                                      id: "premTermDesc",
                                      title: "Premium Term",
                                      right: true
                                    }
                                  ]
                                }
                              ]
                            },
                            {
                              type: "block",
                              title: "Other Plan Details",
                              items: [
                                {
                                  type: "HBOX",
                                  items: [
                                    {
                                      id: "ccy",
                                      title: "Policy Currency",
                                      type: "READONLY",
                                      options: [
                                        {
                                          value: "ALL",
                                          title: {
                                            en: "All Currencies",
                                            "zh-hant": "æ‰€æœ‰è²¨å¹£"
                                          },
                                          exRate: "1",
                                          seq: "1"
                                        },
                                        {
                                          value: "AUD",
                                          title: {
                                            en: "Australian Dollars",
                                            "zh-hant": "æ¾³å…ƒ"
                                          },
                                          shortTitle: "AUD",
                                          exRate: "1.2",
                                          seq: "2"
                                        },
                                        {
                                          value: "CAD",
                                          title: {
                                            en: "Canadian Dollars",
                                            "zh-hant": "åŠ å¹£"
                                          },
                                          exRate: "1.2",
                                          seq: "3"
                                        },
                                        {
                                          value: "CHF",
                                          title: {
                                            en: "Swiss Francs",
                                            "zh-hant": "ç‘žå£«æ³•éƒŽ"
                                          },
                                          exRate: "1.2",
                                          seq: "4"
                                        },
                                        {
                                          value: "EUR",
                                          title: {
                                            en: "Euros",
                                            "zh-hant": "æ­å…ƒ"
                                          },
                                          exRate: "1",
                                          seq: "5"
                                        },
                                        {
                                          value: "GBP",
                                          title: {
                                            en: "British Pounds",
                                            "zh-hant": "è‹±éŽŠ"
                                          },
                                          exRate: "0.64",
                                          seq: "6"
                                        },
                                        {
                                          value: "HKD",
                                          title: {
                                            en: "Hong Kong Dollars",
                                            "zh-hant": "æ¸¯å¹£"
                                          },
                                          shortTitle: "HK$",
                                          exRate: "8",
                                          seq: "7"
                                        },
                                        {
                                          value: "NZD",
                                          title: {
                                            en: "New Zealand Dollars",
                                            "zh-hant": "ç´å…ƒ"
                                          },
                                          exRate: "1.8",
                                          seq: "8"
                                        },
                                        {
                                          value: "USD",
                                          title: {
                                            en: "USD",
                                            "zh-hant": "ç¾Žé‡‘"
                                          },
                                          exRate: "1",
                                          seq: "9"
                                        },
                                        {
                                          value: "SGD",
                                          title: {
                                            en: "SGD",
                                            "zh-hant": "æ–°åŠ å¡å¹£"
                                          },
                                          exRate: "1.6",
                                          seq: "10"
                                        }
                                      ]
                                    },
                                    {
                                      id: "isBackDate",
                                      title: "Backdating",
                                      type: "READONLY",
                                      options: [
                                        {
                                          value: "Y",
                                          title: "Yes"
                                        },
                                        {
                                          value: "N",
                                          title: "No"
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  type: "HBOX",
                                  items: [
                                    {
                                      id: "paymentMode",
                                      title: "Payment Mode",
                                      type: "READONLY",
                                      options: [
                                        {
                                          value: "M",
                                          title: "Monthly"
                                        },
                                        {
                                          value: "Q",
                                          title: "Quarterly"
                                        },
                                        {
                                          value: "S",
                                          title: "Semi-Annual"
                                        },
                                        {
                                          value: "A",
                                          title: "Annual"
                                        },
                                        {
                                          value: "L",
                                          title: "Single Premium"
                                        }
                                      ]
                                    },
                                    {
                                      id: "riskCommenDate",
                                      title:
                                        "Selected Commencement Date (DD/MM/YYYY)",
                                      type: "READONLY",
                                      subType: "date",
                                      trigger: {
                                        id: "isBackDate",
                                        type: "showIfEqual",
                                        value: "Y"
                                      }
                                    }
                                  ]
                                },
                                {
                                  type: "HBOX",
                                  items: [
                                    {
                                      id: "premium",
                                      decimal: 2,
                                      title:
                                        "Total Premium Amount (including riders if any)",
                                      subType: "currency",
                                      type: "READONLY"
                                    }
                                  ]
                                }
                              ]
                            }
                          ],
                          subType: "proposer"
                        }
                      ]
                    }
                  ]
                },
                {
                  type: "menuSection",
                  detailSeq: 2,
                  items: [
                    {
                      title: {
                        en: "Declaration"
                      },
                      type: "menuItem",
                      key: "menu_declaration",
                      detailSeq: 0,
                      items: [
                        {
                          type: "tabs",
                          items: [
                            {
                              title: "Proposer",
                              subType: "proposer",
                              type: "tab",
                              items: [
                                {
                                  type: "block",
                                  id: "declaration",
                                  items: [
                                    {
                                      type: "BLOCK",
                                      title: "Term Conversion",
                                      detailSeq: 1,
                                      trigger: {
                                        id:
                                          "#insured.0.policies.havExtPlans,@extra.baseProductCode,@extra.isPhSameAsLa,#proposer.policies.havExtPlans",
                                        value:
                                          "( (#3=='N' && #1=='Y') || (#3=='Y' && #4=='Y') ) && (#2=='LMP' || #2 == 'FPX' || #2=='FSX')",
                                        type: "dynamicCondition"
                                      },
                                      items: [
                                        {
                                          type: "QRADIOGROUP",
                                          id: "isTermConversion",
                                          mandatory: true,
                                          title:
                                            "Is this application a Term conversion? ",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "PARAGRAPH",
                                          id: "pWarning",
                                          paragraphType: "warningWithNote",
                                          align: "left",
                                          content:
                                            "<p>Note: Please submit duly completed Servicing Request Form.</p>",
                                          detailSeq: 2,
                                          trigger: {
                                            id: "isTermConversion",
                                            value: "Y",
                                            type: "showIfEqual"
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Bankruptcy",
                                      detailSeq: 1,
                                      items: [
                                        {
                                          type: "QRADIOGROUP",
                                          id: "BANKRUPTCY01",
                                          mandatory: true,
                                          title:
                                            "Are you an undischarged bankrupt? ",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Politically Exposed Person (PEP)",
                                      tips: {
                                        title:
                                          "PEP means an individual who is or has been entrusted with prominent public functions in Singapore, a foreign country or an international organisation, which includes the roles held by a head of state, a head of government, government ministers, senior civil or public servants, senior judicial or military officials, senior executives of state owned corporations, senior political party officials, members of the legislature and senior management of international organisations."
                                      },
                                      detailSeq: 2,
                                      items: [
                                        {
                                          type: "QRADIOGROUP",
                                          id: "PEP01",
                                          mandatory: true,
                                          title:
                                            "Are you a current/former Political Exposed Person (PEP) or a family member (parent, step parent, child, step-child, adopted child, spouse, sibling, step-sibling and adopted sibling) or close associate (closely connected either socially or professionally) of a PEP?",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          id: "PEP01_DATA",
                                          mandatory: true,
                                          allowEdit: true,
                                          hasBorder: true,
                                          max: 8,
                                          trigger: {
                                            id: "PEP01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          items: [
                                            {
                                              id: "PEP01a",
                                              mandatory: true,
                                              type: "TEXT",
                                              title:
                                                "Relationship with the PEP",
                                              max: 50
                                            },
                                            {
                                              id: "PEP01b",
                                              mandatory: true,
                                              type: "TEXT",
                                              title: "Full Name of the PEP",
                                              max: 50
                                            },
                                            {
                                              id: "PEP01c",
                                              mandatory: true,
                                              type: "TEXT",
                                              title: "Position held by the PEP",
                                              max: 50
                                            },
                                            {
                                              id: "PEP01d",
                                              mandatory: true,
                                              type: "picker",
                                              subType: "limit",
                                              title:
                                                "Country where he/she is the PEP",
                                              max: 50,
                                              options: "country"
                                            },
                                            {
                                              type: "TABLE-BLOCK-T",
                                              key: "PEP01-bb",
                                              mandatory: true,
                                              title: "Source of Funds",
                                              presentation:
                                                "PEP01e1$PEP01e2$PEP01e3$PEP01e4$PEP01e5$PEP01e6",
                                              presnetationStyle: {
                                                width: "22%"
                                              },
                                              items: [
                                                {
                                                  title: "Salary/Commission",
                                                  id: "PEP01e1",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "PEP01e2"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from sale of assets ",
                                                  id: "PEP01e3"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "PEP01e4"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from maturity/surrender of a policy",
                                                  id: "PEP01e5"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "PEP01e6",
                                                  valueId: "PEP01e7"
                                                },
                                                {
                                                  id: "PEP01e7",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Funds",
                                                  trigger: {
                                                    id: "PEP01e6",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "TABLE-BLOCK-T",
                                              key: "PEP01-c",
                                              mandatory: true,
                                              title: "Source of Wealth",
                                              presentation:
                                                "PEP01f1$PEP01f2$PEP01f3$PEP01f4$PEP01f5$PEP01f6",
                                              presnetationStyle: {
                                                width: "22%"
                                              },
                                              items: [
                                                {
                                                  title: "Salary/Commission",
                                                  id: "PEP01f1",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "PEP01f2"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Investment Profits (shares, bonds, unit trust, property etc)",
                                                  id: "PEP01f3"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "PEP01f4"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Withdrawal of CPF money",
                                                  id: "PEP01f5"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "PEP01f6",
                                                  valueId: "PEP01f7"
                                                },
                                                {
                                                  id: "PEP01f7",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Wealth",
                                                  trigger: {
                                                    id: "PEP01f6",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title:
                                        "Foreign Account Tax Compliance Act (FATCA) and Common Reporting Standard (CRS) Tax Residency",
                                      detailSeq: 3,
                                      items: [
                                        {
                                          id: "FATCA01",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          title:
                                            "Is the Proposer a tax resident of {{Singapore}}?",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          needToHighlight: true,
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          id: "FATCA01b",
                                          mandatory: true,
                                          type: "text",
                                          max: 15,
                                          title:
                                            "Please provide Taxpayer Identification Number TIN",
                                          trigger: {
                                            id: "FATCA01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          id: "FATCA02",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          title:
                                            "Is the Proposer a citizen or tax resident of {{United States (US)}}?",
                                          detailSeq: 2,
                                          horizontal: true,
                                          align: "left",
                                          needToHighlight: true,
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          id: "FATCA03",
                                          mandatory: true,
                                          type: "TEXT",
                                          max: 15,
                                          title:
                                            "Please provide Taxpayer Identification Number TIN",
                                          detailSeq: 3,
                                          trigger: [
                                            {
                                              id: "FATCA02",
                                              type: "showIfEqual",
                                              value: "Y"
                                            },
                                            {
                                              id: "FATCA03",
                                              type: "showIfEqual",
                                              value: "Y"
                                            }
                                          ]
                                        },
                                        {
                                          id: "CRS01",
                                          mandatory: true,
                                          type: "QRADIOGROUP",
                                          title:
                                            "Other than Singapore and US, is the Proposer a tax resident of other {{countries/jurisdictions}}?",
                                          tips: {
                                            msg:
                                              "Each jurisdiction has its own rules for defining tax residence, and jurisdictions have provided information on how to determine if you are resident in the jurisdiction on the following website: [OECD AEOI Portal]. In general, you will find that tax residence is the country/jurisdiction in which you live. Special circumstances may cause you to be resident elsewhere or resident in more than one country/jurisdiction at the same time (dual residency).For more information on tax residence, please consult your tax adviser or the information at the OECD automatic exchange of information portal."
                                          },
                                          detailSeq: 4,
                                          horizontal: true,
                                          align: "left",
                                          needToHighlight: true,
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "table",
                                          allowEdit: true,
                                          hasBorder: true,
                                          id: "CRS01_DATA",
                                          max: 6,
                                          mandatory: true,
                                          trigger: {
                                            id: "CRS01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          items: [
                                            {
                                              id: "CRS01a",
                                              mandatory: true,
                                              type: "picker",
                                              subType: "limit",
                                              title:
                                                "Country/Jurisdiction of Tax Residency",
                                              options: "residency",
                                              trigger: {
                                                type: "excludeSG",
                                                value: "R2,R252"
                                              }
                                            },
                                            {
                                              id: "CRS01b",
                                              mandatoryTrigger: {
                                                id: "CRS01c",
                                                type: "trueIfEmpty"
                                              },
                                              type: "text",
                                              detailSeq: 2,
                                              max: 25,
                                              title:
                                                "Tax Identification Number (TIN)"
                                            },
                                            {
                                              id: "CRS01c",
                                              type: "QRADIOGROUP",
                                              title:
                                                "Reason (if TIN is not available, please select appropriate Reason)",
                                              detailSeq: 3,
                                              trigger: {
                                                id: "CRS01b",
                                                type: "empty"
                                              },
                                              options: [
                                                {
                                                  value: "A",
                                                  title:
                                                    "Reason A â€“ The country/jurisdiction where the Proposer is resident does not issue TINs to its residents. "
                                                },
                                                {
                                                  value: "B",
                                                  title:
                                                    "Reason B â€“ The Proposer is otherwise unable to obtain a TIN or equivalent number due to reason/s. Reasons to be provided."
                                                }
                                              ]
                                            },
                                            {
                                              id: "CRS01d",
                                              type: "text",
                                              mandatory: true,
                                              title:
                                                "Please provide further details if Reason B is selected",
                                              max: 150,
                                              triggerType: "and",
                                              trigger: [
                                                {
                                                  id: "CRS01b",
                                                  type: "empty"
                                                },
                                                {
                                                  id: "CRS01c",
                                                  type: "showIfEqual",
                                                  value: "B"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Trusted Individual",
                                      triggerType: "and",
                                      trigger: [
                                        {
                                          id: "trustedIndividuals/firstName",
                                          type: "notEmpty"
                                        },
                                        {
                                          id: "extra/channel",
                                          type: "showIfNotEqual",
                                          value: "FA"
                                        }
                                      ],
                                      detailSeq: 4,
                                      items: [
                                        {
                                          type: "hbox",
                                          items: [
                                            {
                                              id: "TRUSTED_IND01",
                                              mandatory: true,
                                              label: "title",
                                              title: "Language",
                                              type: "picker",
                                              options: [
                                                {
                                                  value: "en",
                                                  title: "English"
                                                },
                                                {
                                                  value: "mandarin",
                                                  title: "Mandarin"
                                                },
                                                {
                                                  value: "malay",
                                                  title: "Malay"
                                                },
                                                {
                                                  value: "tamil",
                                                  title: "Tamil"
                                                },
                                                {
                                                  value: "other",
                                                  title: "Other"
                                                }
                                              ]
                                            },
                                            {
                                              id: "TRUSTED_IND02",
                                              mandatory: true,
                                              type: "text",
                                              placeholder: {
                                                en:
                                                  "Please specify the Language if other"
                                              },
                                              trigger: {
                                                id: "TRUSTED_IND01",
                                                type: "showIfEqual",
                                                value: "other"
                                              },
                                              max: 15
                                            }
                                          ]
                                        },
                                        {
                                          id: "TRUSTED_IND04",
                                          mandatory: true,
                                          type: "checkbox",
                                          trigger: {
                                            id: "TRUSTED_IND01",
                                            type: "showIfEqual",
                                            value: "other"
                                          },
                                          hiddenField: {
                                            type: "skip"
                                          },
                                          content:
                                            "I, <b><trustedIndividuals/fullName></b> of NRIC/Passport: <b><trustedIndividuals/idCardNo></b> acknowledge that I have fulfilled the definition of a Trusted Individual and I am a Trusted Individual to <b><@personalInfo/fullName></b>. I confirm that the Financial Consultant has conversed in <b><TRUSTED_IND02></b>  to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to <b><@personalInfo/fullName></b>."
                                        },
                                        {
                                          id: "TRUSTED_IND04",
                                          mandatory: true,
                                          type: "checkbox",
                                          trigger: {
                                            id: "TRUSTED_IND01",
                                            type: "showIfNotEqual",
                                            value: "other"
                                          },
                                          hiddenField: {
                                            type: "skip"
                                          },
                                          content:
                                            "I, <b><trustedIndividuals/fullName></b> of NRIC/Passport: <b><trustedIndividuals/idCardNo></b> acknowledge that I have fulfilled the definition of a Trusted Individual and I am a Trusted Individual to <b><@personalInfo/fullName></b>. I confirm that the Financial Consultant has conversed in <b><TRUSTED_IND01></b>  to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to <b><@personalInfo/fullName></b>."
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "e-Policy",
                                      detailSeq: 5,
                                      trigger: {
                                        id: "removeThisTriggerInRelease2",
                                        type: "showIfEqual",
                                        value: "Y"
                                      },
                                      items: [
                                        {
                                          id: "EPOLICY01",
                                          type: "checkbox",
                                          title:
                                            "I would also like to receive a copy of my policy documents by mail, aside from my e-policy in MyAXA",
                                          value: "Y"
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Roadshow Declaration",
                                      detailSeq: 6,
                                      trigger: {
                                        id: "extra/channel",
                                        type: "showIfNotEqual",
                                        value: "FA"
                                      },
                                      items: [
                                        {
                                          id: "ROADSHOW01",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          detailSeq: 1,
                                          title:
                                            "Is this case closed at the venue of the roadshow/canvassing or seminar event? ",
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          id: "ROADSHOW04",
                                          type: "QRADIOGROUP",
                                          mandatory: true,
                                          detailSeq: 1,
                                          title: "",
                                          align: "left",
                                          noPadding: true,
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          },
                                          options: [
                                            {
                                              value: "roadshow",
                                              title: "Roadshow"
                                            },
                                            {
                                              value: "canvassing",
                                              title: "Canvassing"
                                            },
                                            {
                                              value: "seminar",
                                              title: "Seminar event"
                                            }
                                          ]
                                        },
                                        {
                                          id: "ROADSHOW02",
                                          mandatory: true,
                                          type: "datepicker",
                                          max: 0,
                                          min: 100,
                                          detailSeq: 2,
                                          title: "Date of event",
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        },
                                        {
                                          id: "ROADSHOW03",
                                          mandatory: true,
                                          type: "text",
                                          max: 50,
                                          detailSeq: 3,
                                          title:
                                            "Venue where case was closed  ",
                                          trigger: {
                                            id: "ROADSHOW01",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Source of Funds",
                                      detailSeq: 7,
                                      items: [
                                        {
                                          type: "block",
                                          title: "1. Source of Payment",
                                          titleType: "normal",
                                          items: [
                                            {
                                              id: "FUND_SRC01",
                                              mandatory: true,
                                              type: "QRADIOGROUP",
                                              detailSeq: 1,
                                              title:
                                                "Please select the origin of the funds used for this application.",
                                              align: "left",
                                              options: [
                                                {
                                                  value: "sg",
                                                  title:
                                                    "Singapore (e.g. Singapore banks/credit cards)"
                                                },
                                                {
                                                  value: "foreign",
                                                  title:
                                                    "Foreign (e.g. overseas banks/credit cards)"
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          type: "block",
                                          title: "2. Payor's Details",
                                          titleType: "normal",
                                          items: [
                                            {
                                              type: "QTITLE",
                                              title:
                                                "Who is paying the insurance premium for this application?"
                                            },
                                            {
                                              type: "HBOX",
                                              items: [
                                                {
                                                  id: "FUND_SRC02",
                                                  mandatory: true,
                                                  type: "picker",
                                                  detailSeq: 2,
                                                  optionsTable: "payors",
                                                  options: [
                                                    {
                                                      title: "Alex 4",
                                                      value: "CP001003-00006"
                                                    },
                                                    {
                                                      title: "Other",
                                                      value: "other"
                                                    }
                                                  ]
                                                }
                                              ]
                                            },
                                            {
                                              type: "block",
                                              detailSeq: 3,
                                              trigger: {
                                                id: "FUND_SRC02",
                                                type: "showIfEqual",
                                                value: "other"
                                              },
                                              items: [
                                                {
                                                  type: "hbox",
                                                  key: "hnamebox",
                                                  detailSeq: 1,
                                                  errorMsg:
                                                    "Surname and Given Name has max 50 characters",
                                                  mandatory: true,
                                                  max: 50,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC03",
                                                      type: "text",
                                                      max: 30,
                                                      validation: {
                                                        id: "FUND_SRC04",
                                                        type: "sum",
                                                        max: 50
                                                      },
                                                      title:
                                                        "Surname of Payor (as shown in ID)"
                                                    },
                                                    {
                                                      id: "FUND_SRC04",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 30,
                                                      validation: {
                                                        id: "FUND_SRC03",
                                                        type: "sum",
                                                        max: 50
                                                      },
                                                      title:
                                                        "Given Name of Payor (as shown in ID)"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 2,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC05",
                                                      type: "text",
                                                      max: 30,
                                                      title:
                                                        "English or other Name of Payor "
                                                    },
                                                    {
                                                      id: "FUND_SRC06",
                                                      type: "text",
                                                      max: 30,
                                                      title:
                                                        "Han Yu Pin Yin Name of Payor"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 3,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC07",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 30,
                                                      title: "Payor's ID No. "
                                                    },
                                                    {
                                                      id: "FUND_SRC08",
                                                      mandatory: true,
                                                      type: "picker",
                                                      max: 30,
                                                      title:
                                                        "Payor's Nationality or Country of Incorporation",
                                                      options: "residency",
                                                      subType: "limit"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "HBOX",
                                                  detailSeq: 4,
                                                  items: [
                                                    {
                                                      type: "picker",
                                                      id: "FUND_SRC09",
                                                      mandatory: true,
                                                      title:
                                                        "Relationship of Payor to Proposer",
                                                      detailSeq: 1,
                                                      options: "relationship"
                                                    },
                                                    {
                                                      type: "text",
                                                      id: "FUND_SRC10",
                                                      mandatory: true,
                                                      maxLength: 20,
                                                      detailSeq: 5,
                                                      title:
                                                        "Other Relationship",
                                                      trigger: {
                                                        id: "FUND_SRC09",
                                                        type: "showIfEqual",
                                                        value: "OTH"
                                                      }
                                                    },
                                                    {
                                                      type: "HBOX",
                                                      detailSeq: 2,
                                                      title:
                                                        "Payor's Contact No.",
                                                      subType: "TEXT",
                                                      items: [
                                                        {
                                                          type: "PICKER",
                                                          id: "FUND_SRC11",
                                                          mandatory: true,
                                                          detailSeq: 1,
                                                          options:
                                                            "countryTelCode",
                                                          value: "+65"
                                                        },
                                                        {
                                                          type: "TEXT",
                                                          id: "FUND_SRC12",
                                                          subType: "number",
                                                          mandatory: true,
                                                          detailSeq: 2,
                                                          max: 999999999999999
                                                        }
                                                      ]
                                                    },
                                                    {
                                                      id: "FUND_SRC20",
                                                      mandatory: true,
                                                      title:
                                                        "Payor's Occupation",
                                                      type: "text",
                                                      max: 100
                                                    },
                                                    {
                                                      id: "FUND_SRC21",
                                                      mandatory: true,
                                                      title:
                                                        "Payor's Annual Income (S$)",
                                                      allowEmpty: true,
                                                      type: "text",
                                                      max: 999999999,
                                                      subType: "currency",
                                                      ccy: "SGD"
                                                    }
                                                  ]
                                                },
                                                {
                                                  id: "FUND_SRC13title",
                                                  title:
                                                    "Payor's Residential/Registered Address",
                                                  type: "QTITLE",
                                                  detailSeq: 7
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 8,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC13",
                                                      mandatory: false,
                                                      type: "PICKER",
                                                      subType: "limit",
                                                      title: "Country",
                                                      options: "residency",
                                                      value: "R2"
                                                    },
                                                    {
                                                      id: "FUND_SRC14",
                                                      mandatory: true,
                                                      type: "text",
                                                      subType: "postalcode",
                                                      max: 10,
                                                      title: "Postal Code",
                                                      countryAllowed:
                                                        "FUND_SRC13",
                                                      mapping: {
                                                        bldgNo: "FUND_SRC15,12",
                                                        bldgName:
                                                          "FUND_SRC18,40",
                                                        streetName:
                                                          "FUND_SRC16,24"
                                                      }
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 9,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC15",
                                                      mandatory: true,
                                                      type: "text",
                                                      title:
                                                        "Block/House Number",
                                                      max: 12
                                                    },
                                                    {
                                                      id: "FUND_SRC16",
                                                      mandatory: true,
                                                      type: "text",
                                                      max: 24,
                                                      title: "Street/Road Name"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 10,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC17",
                                                      type: "text",
                                                      title: "Unit Number",
                                                      subType: "unit_no",
                                                      max: 13
                                                    },
                                                    {
                                                      id: "FUND_SRC18",
                                                      type: "text",
                                                      max: 40,
                                                      title:
                                                        "Building/ Estate Name"
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "hbox",
                                                  detailSeq: 11,
                                                  items: [
                                                    {
                                                      id: "FUND_SRC19",
                                                      type: "text",
                                                      max: 12,
                                                      title: "City/State",
                                                      mandatoryTrigger: {
                                                        id: "FUND_SRC13",
                                                        value: ["R51", "R52"],
                                                        type: "trueIfContains"
                                                      }
                                                    }
                                                  ]
                                                },
                                                {
                                                  id: "FUND_SRC22",
                                                  type: "text",
                                                  detailSeq: 11,
                                                  mandatory: true,
                                                  max: 100,
                                                  title:
                                                    "Reason for payment by third party"
                                                },
                                                {
                                                  id: "FUND_SRC23",
                                                  type: "paragraph",
                                                  detailSeq: 12,
                                                  paragraphType: "warning",
                                                  align: "left",
                                                  content:
                                                    "<p>For Individual 3rd party payor: Please furnish copy of ID and Proof of Address<br/>For Corporate 3rd party payor: Please furnish copy of business profile showing the shareholders and directors.</p> "
                                                }
                                              ]
                                            },
                                            {
                                              type: "BLOCK",
                                              mandatory: true,
                                              key: "FUND_SRC24-DATA",
                                              title: "3. Source of Funds",
                                              titleType: "normal",
                                              detailSeq: 13,
                                              trigger: [
                                                {
                                                  type: "comparePremium",
                                                  id: "extra/premium",
                                                  ccys: [
                                                    {
                                                      ccy: "SGD",
                                                      amount: 20000
                                                    },
                                                    {
                                                      ccy: "AUD",
                                                      amount: 19000
                                                    },
                                                    {
                                                      ccy: "EUR",
                                                      amount: 13000
                                                    },
                                                    {
                                                      ccy: "GBP",
                                                      amount: 9600
                                                    },
                                                    {
                                                      ccy: "USD",
                                                      amount: 14000
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "showIfEqual",
                                                  id: "FUND_SRC01",
                                                  value: "foreign"
                                                }
                                              ],
                                              items: [
                                                {
                                                  type: "QTITLE",
                                                  normal: true,
                                                  title:
                                                    "Please confirm the origin of the Payor's funds/assets used for this investment. (You may tick more than 1 option)"
                                                },
                                                {
                                                  title: "Salary/Commission",
                                                  id: "FUND_SRC25",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "FUND_SRC26"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from sale of assets ",
                                                  id: "FUND_SRC27"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "FUND_SRC28"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Proceeds from maturity/surrender of a policy",
                                                  id: "FUND_SRC29"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "FUND_SRC30"
                                                },
                                                {
                                                  id: "FUND_SRC31",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Funds",
                                                  trigger: {
                                                    id: "FUND_SRC30",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              type: "BLOCK",
                                              mandatory: true,
                                              key: "FUND_SRC31-DATA",
                                              title: "4. Source of Wealth",
                                              titleType: "normal",
                                              trigger: [
                                                {
                                                  type: "comparePremium",
                                                  id: "extra/premium",
                                                  ccys: [
                                                    {
                                                      ccy: "SGD",
                                                      amount: 20000
                                                    },
                                                    {
                                                      ccy: "AUD",
                                                      amount: 19000
                                                    },
                                                    {
                                                      ccy: "EUR",
                                                      amount: 13000
                                                    },
                                                    {
                                                      ccy: "GBP",
                                                      amount: 9600
                                                    },
                                                    {
                                                      ccy: "USD",
                                                      amount: 14000
                                                    }
                                                  ]
                                                },
                                                {
                                                  type: "showIfEqual",
                                                  id: "FUND_SRC01",
                                                  value: "foreign"
                                                }
                                              ],
                                              detailSeq: 1,
                                              items: [
                                                {
                                                  type: "QTITLE",
                                                  normal: true,
                                                  title:
                                                    "Please confirm the origin of the Payor's total assets and where the Payor's wealth is derived from. (You may tick more than 1 option)"
                                                },
                                                {
                                                  title: "Salary/Commission",
                                                  id: "FUND_SRC33",
                                                  type: "CHECKBOX"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Gift/Inheritance",
                                                  id: "FUND_SRC34"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Investment Profits (shares, bonds, unit trust, property etc)",
                                                  id: "FUND_SRC35"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Business Income",
                                                  id: "FUND_SRC36"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title:
                                                    "Withdrawal of CPF money",
                                                  id: "FUND_SRC37"
                                                },
                                                {
                                                  type: "CHECKBOX",
                                                  title: "Others",
                                                  id: "FUND_SRC38"
                                                },
                                                {
                                                  id: "FUND_SRC39",
                                                  type: "text",
                                                  mandatory: true,
                                                  max: 50,
                                                  title:
                                                    "Please specify the Source of Wealth",
                                                  trigger: {
                                                    id: "FUND_SRC38",
                                                    type: "showIfEqual",
                                                    value: "Y"
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title:
                                        "Financial Consultant's Declaration",
                                      items: [
                                        {
                                          id: "FCD_01",
                                          type: "text",
                                          max: 30,
                                          title:
                                            "How long have you known your customer?",
                                          mandatory: true
                                        },
                                        {
                                          type: "QRADIOGROUP",
                                          id: "FCD_02",
                                          mandatory: true,
                                          title:
                                            "Are you related to your customer?",
                                          detailSeq: 1,
                                          horizontal: true,
                                          align: "left",
                                          options: [
                                            {
                                              value: "Y",
                                              title: "Yes"
                                            },
                                            {
                                              value: "N",
                                              title: "No"
                                            }
                                          ]
                                        },
                                        {
                                          type: "text",
                                          id: "FCD_03",
                                          mandatory: true,
                                          title: "Relationship to customer",
                                          max: 20,
                                          trigger: {
                                            id: "FCD_02",
                                            type: "showIfEqual",
                                            value: "Y"
                                          }
                                        }
                                      ]
                                    },
                                    {
                                      type: "BLOCK",
                                      title: "Personal Data Acknowledgement",
                                      trigger: {
                                        id: "extra/channel",
                                        type: "showIfEqual",
                                        value: "FA"
                                      },
                                      items: [
                                        {
                                          id: "pda",
                                          type: "paragraph",
                                          paragraphType: "pda",
                                          content:
                                            "<p style='padding: 0px; line-height: 18px'>The information I or We have provided is my personal data and, where it is not my personal data, that I or We have the consent of the owner of such personal data to provide such information.<br/></p><br/>By providing this information, I or We understand and give my or our consent for AXA Insurance Pte Ltd (\"AXA\") and its representatives or agents to:</p><p style='padding: 0px; line-height: 18px'>i. Collect, use, store, transfer and/or disclose the information, to or with all such persons (including any member or the AXA Group or any third party service provider, and whether within or outside of Singapore) for the purpose of enabling AXA to provide me with services required of an insurance provider, including the evaluating, processing, administering and/or managing of my or our relationship and policy(ies) with AXA, and for the purposes set out in AXA's Data Use Statement which can be found at http://axa.com.sg (\"Purposes\").</p><p style='padding: 0px; line-height: 18px'>ii. Collect, use, store, transfer and/or disclose personal data about me or us and those whose personal data I or We have provided from sources other than myself or us for the Purposes.</p><p style='padding: 0px; line-height: 18px'>iii. Contact me or us to share marketing information about products and services from AXA that may be of interest to me or us by post and e-mail and</p><p style='padding: 0px; line-height: 18px'>(Please tick accordingly and you may tick more than one option)</p>"
                                        },
                                        {
                                          type: "BLOCK",
                                          key: "PDPA01",
                                          mandatory: true,
                                          validation:
                                            "function(i,v,rv,vrv){if(v.PDPA01a!='Y' && v.PDPA01b!='Y' && v.PDPA01c!='Y'){return 301;}}",
                                          items: [
                                            {
                                              type: "BLOCK",
                                              style: {
                                                display: "flex"
                                              },
                                              items: [
                                                {
                                                  id: "PDPA01a",
                                                  type: "checkbox",
                                                  title: "By Telephone"
                                                },
                                                {
                                                  id: "PDPA01b",
                                                  type: "checkbox",
                                                  title: "By Text Message"
                                                },
                                                {
                                                  id: "PDPA01c",
                                                  type: "checkbox",
                                                  title: "By Fax"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          id: "stepSign",
          type: "section",
          title: "Signature",
          detailSeq: 2,
          items: [
            {
              id: "sign",
              type: "signature"
            }
          ]
        },
        {
          id: "stepPay",
          type: "section",
          title: "Payment",
          detailSeq: 3,
          items: [
            {
              id: "pay",
              type: "payment"
            }
          ]
        },
        {
          id: "stepSubmit",
          type: "section",
          title: "Submit",
          detailSeq: 4,
          items: [
            {
              id: "submit",
              type: "submission"
            }
          ]
        }
      ]
    },
    application: {
      type: "application",
      quotationDocId: "QU001003-01380",
      applicationForm: {
        values: {
          insured: [],
          proposer: {
            personalInfo: {
              addrBlock: "aaaaabc",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "sssssasdfsfaaba",
              age: 30,
              agentId: "008008",
              allowance: 123123,
              applicationCount: 1,
              branchInfo: {},
              bundle: [
                {
                  id: "FN001003-00046",
                  isValid: false
                },
                {
                  id: "FN001003-00048",
                  isValid: false
                },
                {
                  id: "FN001003-00049",
                  isValid: false
                },
                {
                  id: "FN001003-00054",
                  isValid: false
                },
                {
                  id: "FN001003-00055",
                  isValid: false
                },
                {
                  id: "FN001003-00056",
                  isValid: false
                },
                {
                  id: "FN001003-00067",
                  isValid: false
                },
                {
                  id: "FN001003-00068",
                  isValid: false
                },
                {
                  id: "FN001003-00069",
                  isValid: false
                },
                {
                  id: "FN001003-00070",
                  isValid: false
                },
                {
                  id: "FN001003-00098",
                  isValid: false
                },
                {
                  id: "FN001003-00099",
                  isValid: false
                },
                {
                  id: "FN001003-00100",
                  isValid: false
                },
                {
                  id: "FN001003-00101",
                  isValid: false
                },
                {
                  id: "FN001003-00102",
                  isValid: false
                },
                {
                  id: "FN001003-00130",
                  isValid: false
                },
                {
                  id: "FN001003-00131",
                  isValid: false
                },
                {
                  id: "FN001003-00132",
                  isValid: false
                },
                {
                  id: "FN001003-00133",
                  isValid: false
                },
                {
                  id: "FN001003-00135",
                  isValid: false
                },
                {
                  id: "FN001003-00137",
                  isValid: false
                },
                {
                  id: "FN001003-00138",
                  isValid: false
                },
                {
                  id: "FN001003-00255",
                  isValid: false
                },
                {
                  id: "FN001003-00274",
                  isValid: false
                },
                {
                  id: "FN001003-00275",
                  isValid: false
                },
                {
                  id: "FN001003-00276",
                  isValid: true
                }
              ],
              cid: "CP001003-00006",
              dependants: [
                {
                  cid: "CP001003-00007",
                  relationship: "SPO",
                  relationshipOther: null
                },
                {
                  cid: "CP001003-00008",
                  relationship: "SON"
                },
                {
                  cid: "CP001003-00009",
                  relationship: "MOT"
                },
                {
                  cid: "CP001003-00010",
                  relationship: "FAT"
                },
                {
                  cid: "CP001003-00028",
                  relationship: "SON"
                }
              ],
              dob: "1987-11-24",
              education: "below",
              email: "alex.tang@eabsystems.com",
              employStatus: "ft",
              firstName: "Alex 4",
              fnaRecordIdArray: "",
              fullName: "Alex 4",
              gender: "M",
              hanyuPinyinName: "",
              haveSignDoc: true,
              idCardNo: "S1234567D",
              idDocType: "nric",
              industry: "I12",
              initial: "A",
              isSmoker: "Y",
              isValid: true,
              language: "zh",
              lastName: "",
              lastUpdateDate: "2018-06-15T06:09:59.821Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "123123",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 31,
              occupation: "O921",
              occupationOther: "bbbbbb",
              organization: "aaaa",
              organizationCountry: "R2",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "1233466789",
              referrals: "",
              residenceCountry: "R2",
              title: "Mr",
              trustedIndividuals: {
                firstName: "Alex bun 5 Fa Daug",
                fullName: "Alex bun 5 Fa Daug",
                idCardNo: "S1234567D",
                idDocType: "nric",
                lastName: "",
                mobileCountryCode: "+65",
                mobileNo: 123123,
                nameOrder: "L",
                relationship: "SON",
                tiPhoto: ""
              },
              type: "cust",
              unitNum: ""
            },
            declaration: {
              trustedIndividuals: {
                firstName: "Alex bun 5 Fa Daug",
                fullName: "Alex bun 5 Fa Daug",
                idCardNo: "S1234567D",
                idDocType: "nric",
                lastName: "",
                mobileCountryCode: "+65",
                mobileNo: 123123,
                nameOrder: "L",
                relationship: "SON",
                tiPhoto: ""
              },
              ccy: "SGD"
            },
            residency: {},
            extra: {
              isPhSameAsLa: "Y",
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              premium: 1169.29,
              baseProductCode: "SAV",
              hasTrustedIndividual: "Y"
            },
            insurability: {
              LIFESTYLE01: "Y"
            },
            policies: {
              existLife: 3333,
              existTpd: 0,
              existCi: 0,
              existPaAdb: 0,
              existTotalPrem: 44,
              replaceLife: 0,
              replaceTpd: 0,
              replaceCi: 0,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              existLifeAXA: 0,
              existTpdAXA: 0,
              existCiAXA: 0,
              existPaAdbAXA: 0,
              existTotalPremAXA: 0,
              existLifeOther: 0,
              existTpdOther: 0,
              existCiOther: 0,
              existPaAdbOther: 0,
              existTotalPremOther: 0,
              replaceLifeAXA: 0,
              replaceTpdAXA: 0,
              replaceCiAXA: 0,
              replacePaAdbAXA: 0,
              replaceTotalPremAXA: 0,
              replaceLifeOther: 0,
              replaceTpdOther: 0,
              replaceCiOther: 0,
              replacePaAdbOther: 0,
              replaceTotalPremOther: 0,
              replacePolTypeAXA: "-",
              replacePolTypeOther: "-",
              havExtPlans: "Y",
              isProslReplace: "N"
            }
          },
          planDetails: {
            planList: [
              {
                calcBy: "premium",
                covCode: "SAV",
                covName: {
                  en: "SavvySaver",
                  "zh-Hant": "SavvySaver"
                },
                halfYearPrem: 583.84,
                monthPrem: 100.17,
                planCode: "15SS",
                polTermDesc: "15 Years",
                policyTerm: "15",
                premRate: 108,
                premTerm: "15",
                premTermDesc: "15 Years",
                premium: 1144.8,
                productLine: "WLE",
                quarterPrem: 297.64,
                saViewInd: "Y",
                sumInsured: 10600,
                version: 2,
                yearPrem: 1144.8
              },
              {
                covCode: "PET",
                covName: {
                  en: "PremiumEraser Total",
                  "zh-Hant": "PremiumEraser Total"
                },
                halfYearPrem: 12.48,
                monthPrem: 2.14,
                planCode: "15WOP",
                polTermDesc: "15 Years",
                policyTerm: "15",
                premRate: 2.14,
                premTerm: "15",
                premTermDesc: "15 Years",
                premium: 24.49,
                productLine: "WLE",
                quarterPrem: 6.36,
                saViewInd: "N",
                sumInsured: " - ",
                version: 1,
                yearPrem: 24.49
              }
            ],
            type: 1,
            ccy: "SGD",
            isBackDate: "N",
            paymentMode: "A",
            riskCommenDate: "2018-07-17",
            totYearPrem: 1169.29,
            premium: 1169.29,
            sumInsured: 11744.8,
            paymentTerm: "Regular",
            displayPlanList: [
              {
                calcBy: "premium",
                covCode: "SAV",
                covName: {
                  en: "SavvySaver",
                  "zh-Hant": "SavvySaver"
                },
                halfYearPrem: 583.84,
                monthPrem: 100.17,
                planCode: "15SS",
                polTermDesc: "15 Years",
                policyTerm: "15",
                premRate: 108,
                premTerm: "15",
                premTermDesc: "15 Years",
                premium: 1144.8,
                productLine: "WLE",
                quarterPrem: 297.64,
                saViewInd: "Y",
                sumInsured: 10600,
                version: 2,
                yearPrem: 1144.8,
                totalPremium: 1169.29
              }
            ],
            riderList: [
              {
                covCode: "PET",
                covName: {
                  en: "PremiumEraser Total",
                  "zh-Hant": "PremiumEraser Total"
                },
                halfYearPrem: 12.48,
                monthPrem: 2.14,
                planCode: "15WOP",
                polTermDesc: "15 Years",
                policyTerm: "15",
                premRate: 2.14,
                premTerm: "15",
                premTermDesc: "15 Years",
                premium: 24.49,
                productLine: "WLE",
                quarterPrem: 6.36,
                saViewInd: "N",
                sumInsured: " - ",
                version: 1,
                yearPrem: 24.49
              }
            ],
            hasRiders: "Y",
            planTypeName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            }
          },
          appFormTemplate: {
            main: "appform_report_main",
            template: [
              "appform_report_common",
              "appform_report_personal_details",
              "appform_report_plan_details_sav",
              "appform_report_residency",
              "appform_report_insurability",
              "appform_report_declaration"
            ],
            properties: {
              dollarSignForAllCcy: true
            },
            dynTemplate: {
              personal: "appform_dyn_personal",
              residency: "appform_dyn_residency",
              insurability: {
                la: {
                  PET: "dyn_la_insurability_sio"
                }
              },
              plan: "appform_dyn_plan",
              declaration: "appform_dyn_declaration"
            }
          }
        }
      },
      isSubmittedStatus: false,
      isValid: true,
      isFailSubmit: false,
      isApplicationSigned: false,
      isProposalSigned: false,
      isStartSignature: false,
      isFullySigned: false,
      isBackDate: false,
      isMandDocsAllUploaded: false,
      applicationStartedDate: "2018-07-17T06:59:36.942Z",
      applicationSignedDate: 0,
      applicationSubmittedDate: 0,
      applicationInforceDate: 0,
      appStep: 0,
      sameAs: "Y",
      lastUpdateDate: 1531810776942,
      createDate: 1531810776942,
      quotation: {
        agent: {
          agentCode: "008008",
          company: "AXA Insurance Pte Ltd",
          dealerGroup: "AGENCY",
          email: "alex.tang@eabsystems.com",
          mobile: "12345678",
          name: "Alex Tang",
          tel: "0"
        },
        agentCode: "008008",
        baseProductCode: "SAV",
        baseProductId: "08_product_SAV_2",
        baseProductName: {
          en: "SavvySaver",
          "zh-Hant": "SavvySaver"
        },
        budgetRules: ["RP"],
        bundleId: "FN001003-00276",
        ccy: "SGD",
        clientChoice: {
          isClientChoiceSelected: true,
          recommendation: {
            benefit:
              "SavvySaver is a regular premium participating anticipated endowment policy. It provides you Death and Terminal Illness coverage, yearly Guaranteed Cash Payouts from the end of the second Policy year and a Guaranteed Maturity benefit plus bonuses (if any), payable to you in a lump sum upon Policy maturity. SavvySaver terminates upon full payout of either your TI benefit, Death benefit, Policy maturity amount or any other reasons as stated in Product Summary.\n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.\n\nPremiumEraser Total Rider will waive future Premiums upon Total and Permanent Disability before the Policy Anniversary nearest to your 70th year birthday or you are diagnosed with one of the 36 Critical Illness, while this Additional Benefit is in force.",
            limitation:
              "The Sum Assured is a notional value, used solely for the calculation of Guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death or Terminal Illness. For the actual payout in the event of death and terminal illness please refer to the product summary.\nGuaranteed Cash Payouts could be deposited with us at a non-guaranteed interest rate. You can withdraw partial OR all of the re-deposited amounts with earned interest, subject to the minimum withdrawal amount equal to 1 Guaranteed Cash Payout payment. The bonus rates used for the policy illustration are non-guaranteed, the actual benefit payable may vary according to the future performance of the Participating Fund. There are certain conditions under which no benefits will be payable under this Policy. These are stated as exclusions in this Policy. Refer to your Product Summary for the exclusions, terms & conditions for payable Benefits.\nBuying a life insurance is a long-term commitment. An early termination of this Policy usually involves high costs and the surrender value may be less than the total Premiums paid.\n\nPremiumEraser Total Rider terminates upon its coverage expiry date of this additional benefit OR when Policy is assigned OR when you no longer suffers from TPD  or any other event which results in the termination of this Additional Benefit as set out in the Policy.\nThe premium rates for this Additional Benefit are not guaranteed. The rates may be adjusted based on future experience. Premiums are payable throughout the term of this Additional Benefit.\nPlease refer to product summary for terms and conditions and exclusions.",
            reason: "ff",
            rop: {
              choiceQ1: "N",
              choiceQ1Sub1: "",
              choiceQ1Sub2: "",
              choiceQ1Sub3: "",
              existCi: 0,
              existLife: 3333,
              existPaAdb: 0,
              existTotalPrem: 44,
              existTpd: 0,
              replaceCi: 0,
              replaceLife: 0,
              replacePaAdb: 0,
              replaceTotalPrem: 0,
              replaceTpd: 0
            },
            rop_shield: {
              ropBlock: {
                iCidRopAnswerMap: {},
                ropQ1sub3: "",
                ropQ2: "",
                ropQ3: "",
                shieldRopAnswer_0: "",
                shieldRopAnswer_1: "",
                shieldRopAnswer_2: "",
                shieldRopAnswer_3: "",
                shieldRopAnswer_4: "",
                shieldRopAnswer_5: ""
              }
            }
          }
        },
        compCode: "01",
        createDate: "2018-07-17T02:46:15.385Z",
        dealerGroup: "AGENCY",
        extraFlags: {
          fna: {
            riskProfile: 5
          },
          nationalityResidence: {
            iCategory: "A1",
            iRejected: "N",
            pCategory: "A1",
            pRejected: "N"
          }
        },
        fund: null,
        iAge: 31,
        iCid: "CP001003-00006",
        iDob: "1987-11-24",
        iEmail: "alex.tang@eabsystems.com",
        iFirstName: "Alex 4",
        iFullName: "Alex 4",
        iGender: "M",
        iLastName: "",
        iOccupation: "O921",
        iOccupationClass: "4",
        iResidence: "R2",
        iSmoke: "Y",
        id: "QU001003-01380",
        isBackDate: "N",
        lastUpdateDate: "2018-07-17T02:46:27.452Z",
        pAge: 31,
        pCid: "CP001003-00006",
        pDob: "1987-11-24",
        pEmail: "alex.tang@eabsystems.com",
        pFirstName: "Alex 4",
        pFullName: "Alex 4",
        pGender: "M",
        pLastName: "",
        pOccupation: "O921",
        pOccupationClass: "4",
        pResidence: "R2",
        pSmoke: "Y",
        paymentMode: "A",
        plans: [
          {
            calcBy: "premium",
            covCode: "SAV",
            covName: {
              en: "SavvySaver",
              "zh-Hant": "SavvySaver"
            },
            halfYearPrem: 583.84,
            monthPrem: 100.17,
            planCode: "15SS",
            polTermDesc: "15 Years",
            policyTerm: "15",
            premRate: 108,
            premTerm: "15",
            premTermDesc: "15 Years",
            premium: 1144.8,
            productLine: "WLE",
            quarterPrem: 297.64,
            saViewInd: "Y",
            sumInsured: 10600,
            version: 2,
            yearPrem: 1144.8
          },
          {
            covCode: "PET",
            covName: {
              en: "PremiumEraser Total",
              "zh-Hant": "PremiumEraser Total"
            },
            halfYearPrem: 12.48,
            monthPrem: 2.14,
            planCode: "15WOP",
            polTermDesc: "15 Years",
            policyTerm: "15",
            premRate: 2.14,
            premTerm: "15",
            premTermDesc: "15 Years",
            premium: 24.49,
            productLine: "WLE",
            quarterPrem: 6.36,
            saViewInd: "N",
            sumInsured: 1144.8,
            version: 1,
            yearPrem: 24.49
          }
        ],
        policyOptions: {},
        policyOptionsDesc: {},
        premTerm: "15",
        premium: 1169.29,
        productLine: "WLE",
        productVersion: "2.0",
        riskCommenDate: "2018-07-17",
        sameAs: "Y",
        sumInsured: 11744.8,
        totHalfyearPrem: 596.32,
        totMonthPrem: 102.31,
        totQuarterPrem: 304,
        totYearPrem: 1169.29,
        type: "quotation"
      },
      bundleId: "FN001003-00276",
      compCode: "01",
      agentCode: "008008",
      dealerGroup: "AGENCY",
      iCid: "CP001003-00006",
      pCid: "CP001003-00006",
      id: "NB001003-02061"
    }
  }
};

export const shieldData = {
  application: {
    agent: {
      agentCode: "084620",
      company: "AXA Insurance Pte Ltd",
      dealerGroup: "AGENCY",
      email: "UAT.testapprovalmanager@axaplanner.com.sg",
      mobile: "+65 90728021",
      name: "KELVIN LIM TECK HWA",
      tel: "0"
    },
    agentChannelType: "N",
    agentCode: "084620",
    appCompletedStep: -1,
    appStep: 0,
    applicationForm: {
      values: {
        appFormTemplate: {
          main: "appform_report_shield_main",
          properties: {
            dollarSignForAllCcy: false
          },
          template: [
            "appform_report_common_shield",
            "appform_report_personal_details_shield",
            "appform_report_plan_details_shield",
            "appform_report_rop_insurability_shield",
            "appform_report_declaration_ph_shield",
            "appform_report_declaration_la_shield"
          ]
        },
        checkedMenu: ["menu_person", "menu_plan"],
        completedMenus: ["menu_residency", "menu_plan"],
        insured: [
          {
            declaration: {},
            extra: {
              ccy: "SGD",
              channel: "AGENCY",
              channelName: "AGENCY",
              isCompleted: false,
              isPdfGenerated: false,
              isPhSameAsLa: "Y",
              premium: 1105.3
            },
            groupedPlanDetails: {
              awl: 300,
              axaShield: 156,
              basicPlanList: [
                {
                  cashPortion: 0,
                  covClass: "B",
                  covCode: "ASIM",
                  covName: {
                    en: "AXA Shield (Plan B)",
                    "zh-Hant": "AXA Shield (Plan B)"
                  },
                  covNameDesc: {
                    en: "AXA Shield",
                    "zh-Hant": "AXA Shield"
                  },
                  cpfPortion: 156,
                  medisave: 466,
                  payFreq: "A",
                  payFreqDesc: "Annual",
                  paymentMethod: "CPF",
                  paymentMethodDesc: "CPF",
                  planCode: "ASIMSB",
                  planType: "Health Plan",
                  planTypeDesc: "Plan B",
                  premium: 466,
                  productId: "08_product_ASIM_3",
                  productLine: "HP",
                  sumInsured: " - ",
                  tax: {
                    yearTax: 30.49
                  },
                  version: 3
                }
              ],
              cashPortion: 0,
              ccy: "SGD",
              covClass: "B",
              cpfPortion: 156,
              medisave: 466,
              medishieldLife: 310,
              paymentMode: "A",
              planTypeName: {
                en: "AXA Shield (Plan B)",
                "zh-Hant": "AXA Shield (Plan B)"
              },
              premium: 0,
              riderPlanList: [],
              totYearPrem: 0,
              totalRiderPremium: 0
            },
            insurability: {
              LIFESTYLE01: "N"
            },
            personalInfo: {
              addrBlock: "101010",
              addrCity: "",
              addrCountry: "R2",
              addrEstate: "",
              addrStreet: "0110",
              age: 38,
              agentCode: "084620",
              agentId: "084620",
              allowance: 10000,
              bundle: [
                {
                  id: "FN001031-00012",
                  isValid: true
                }
              ],
              cid: "CP001031-00003",
              compCode: "01",
              dealerGroup: "AGENCY",
              dependants: [
                {
                  cid: "CP001031-00002",
                  relationship: "SPO",
                  relationshipOther: null
                }
              ],
              dob: "1980-09-04",
              education: "above",
              email: "",
              employStatus: "hw",
              firstName: "jackwife",
              fnaRecordIdArray: "",
              fullName: "jackwife",
              gender: "F",
              hanyuPinyinName: "",
              idCardNo: "S1771351E",
              idDocType: "nric",
              industry: "I12",
              initial: "J",
              isSmoker: "N",
              language: "en",
              lastName: "",
              lastUpdateDate: "2018-09-04T13:17:38.415Z",
              marital: "M",
              mobileCountryCode: "+65",
              mobileNo: "123",
              nameOrder: "L",
              nationality: "N1",
              nearAge: 38,
              occupation: "O675",
              organization: "N/A",
              organizationCountry: "na",
              othName: "",
              otherMobileCountryCode: "+65",
              otherNo: "",
              photo: "",
              postalCode: "",
              referrals: "",
              relationship: "SPO",
              residenceCountry: "R2",
              title: "Dr",
              type: "cust",
              unitNum: ""
            },
            policies: {
              ROP_01: "N"
            },
            residency: {}
          }
        ],
        menus: [
          "menu_person",
          "menu_residency",
          "menu_insure",
          "menu_plan",
          "menu_declaration"
        ],
        proposer: {
          declaration: {
            trustedIndividuals: {}
          },
          extra: {
            ccy: "SGD",
            channel: "AGENCY",
            channelName: "AGENCY",
            hasTrustedIndividual: null,
            isCompleted: false,
            isPdfGenerated: false,
            isPhSameAsLa: "Y",
            premium: 1105.3
          },
          groupedPlanDetails: {
            awl: 300,
            axaShield: 283,
            basicPlanList: [
              {
                cashPortion: 0,
                covClass: "A",
                covCode: "ASIM",
                covName: {
                  en: "AXA Shield (Plan A)",
                  "zh-Hant": "AXA Shield (Plan A)"
                },
                covNameDesc: {
                  en: "AXA Shield",
                  "zh-Hant": "AXA Shield"
                },
                cpfPortion: 283,
                medisave: 593,
                payFreq: "A",
                payFreqDesc: "Annual",
                paymentMethod: "CPF",
                paymentMethodDesc: "CPF",
                planCode: "ASIMSA",
                planType: "Health Plan",
                planTypeDesc: "Plan A",
                premium: 593,
                productId: "08_product_ASIM_3",
                productLine: "HP",
                sumInsured: " - ",
                tax: {
                  yearTax: 38.8
                },
                version: 3
              }
            ],
            cashPortion: 0,
            ccy: "SGD",
            covClass: "A",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310,
            paymentMode: "A",
            planTypeName: {
              en: "AXA Shield (Plan A)",
              "zh-Hant": "AXA Shield (Plan A)"
            },
            premium: 0,
            riderPlanList: [
              {
                covClass: "A",
                covCode: "ASP",
                covName: {
                  en: "AXA Basic Care (Plan A)",
                  "zh-Hant": "AXA Basic Care (Plan A)"
                },
                covNameDesc: {
                  en: "AXA Basic Care",
                  "zh-Hant": "AXA Basic Care"
                },
                payFreq: "M",
                payFreqDesc: "Monthly",
                paymentMethod: "CASH",
                paymentMethodDesc: "Cash",
                planCode: "ASPA",
                planType: "Health Plan",
                planTypeDesc: "Plan A",
                premium: 37.7,
                productId: "08_product_ASP_3",
                productLine: "HP",
                sumInsured: " - ",
                tax: {
                  monthTax: 2.47,
                  yearTax: 29.64
                },
                version: 3,
                yearPrem: 452.4
              },
              {
                covClass: "1",
                covCode: "ASH",
                covName: {
                  en: "AXA Home Care",
                  "zh-Hant": "AXA Home Care"
                },
                covNameDesc: {
                  en: "AXA Home Care",
                  "zh-Hant": "AXA Home Care"
                },
                payFreq: "M",
                payFreqDesc: "Monthly",
                paymentMethod: "CASH",
                paymentMethodDesc: "Cash",
                planCode: "ASHC",
                planType: "Health Plan",
                premium: 8.6,
                productId: "08_product_ASH_3",
                productLine: "HP",
                sumInsured: " - ",
                tax: {
                  monthTax: 0.56,
                  yearTax: 6.72
                },
                version: 3,
                yearPrem: 103.2
              }
            ],
            totYearPrem: 0,
            totalRiderPremium: 46.3
          },
          insurability: {
            LIFESTYLE01: "Y"
          },
          personalInfo: {
            addrBlock: "101010",
            addrCity: "",
            addrCountry: "R2",
            addrEstate: "",
            addrStreet: "0110",
            age: 30,
            agentCode: "084620",
            agentId: "084620",
            allowance: 1000,
            applicationCount: 4,
            bundle: [
              {
                id: "FN001031-00004",
                isValid: false
              },
              {
                id: "FN001031-00005",
                isValid: false
              },
              {
                id: "FN001031-00006",
                isValid: false
              },
              {
                id: "FN001031-00011",
                isValid: true
              }
            ],
            cid: "CP001031-00002",
            compCode: "01",
            dealerGroup: "AGENCY",
            dependants: [
              {
                cid: "CP001031-00003",
                relationship: "SPO"
              }
            ],
            dob: "1988-05-17",
            education: "above",
            email: "test@test.com",
            employStatus: "ft",
            firstName: "Jack",
            fnaRecordIdArray: "",
            fullName: "Jack",
            gender: "M",
            hanyuPinyinName: "",
            idCardNo: "S6963596B",
            idDocType: "nric",
            industry: "I8",
            initial: "J",
            isSameAddr: "Y",
            isSmoker: "Y",
            language: "en,zh",
            lastName: "",
            lastUpdateDate: "2018-05-23T02:22:12.037Z",
            marital: "M",
            mobileCountryCode: "+65",
            mobileNo: "11112345",
            nameOrder: "L",
            nationality: "N1",
            nearAge: 30,
            occupation: "O2",
            organization: "",
            organizationCountry: "R3",
            othName: "",
            otherMobileCountryCode: "+65",
            otherNo: "",
            photo: "",
            postalCode: "",
            referrals: "",
            residenceCountry: "R2",
            title: "Mr",
            type: "cust",
            unitNum: ""
          },
          policies: {
            ROP_01: "N"
          },
          residency: {}
        },
        undefined: {}
      }
    },
    applicationInforceDate: 0,
    applicationSignedDate: 0,
    applicationStartedDate: "2018-09-11T02:56:48.750Z",
    applicationSubmittedDate: 0,
    biSignedDate: 0,
    bundleId: "FN001031-00011",
    childIds: ["NB001031-00015", "NB001031-00016", "NB001031-00017"],
    compCode: "01",
    createDate: "2018-09-11T02:56:48.750Z",
    dealerGroup: "AGENCY",
    iCidMapping: {
      "CP001031-00002": [
        {
          applicationId: "NB001031-00015",
          covCode: "ASIM",
          policyNumber: ""
        },
        {
          applicationId: "NB001031-00016",
          covCode: "ASP",
          policyNumber: ""
        }
      ],
      "CP001031-00003": [
        {
          applicationId: "NB001031-00017",
          covCode: "ASIM",
          policyNumber: ""
        }
      ]
    },
    iCids: ["CP001031-00003"],
    id: "SA001031-00015",
    isAppFormInsuredSigned: [false],
    isAppFormProposerSigned: false,
    isCrossAge: false,
    isFailSubmit: false,
    isFullySigned: false,
    isMandDocsAllUploaded: false,
    isPolicyNumberGenerated: false,
    isProposalSigned: false,
    isStartSignature: false,
    isSubmittedStatus: false,
    isValid: true,
    lastUpdateDate: "2018-09-11T03:23:01.123Z",
    pCid: "CP001031-00002",
    payment: {},
    quotation: {
      agent: {
        agentCode: "084620",
        company: "AXA Insurance Pte Ltd",
        dealerGroup: "AGENCY",
        email: "UAT.testapprovalmanager@axaplanner.com.sg",
        mobile: "+65 90728021",
        name: "KELVIN LIM TECK HWA",
        tel: "0"
      },
      agentCode: "084620",
      baseProductCode: "ASIM",
      baseProductId: "08_product_ASIM_3",
      baseProductName: {
        en: "AXA Shield",
        "zh-Hant": "AXA Shield"
      },
      budgetRules: ["RP"],
      bundleId: "FN001031-00011",
      clientChoice: {
        isClientChoiceSelected: true,
        recommendation: {
          benefit:
            "Jack\nAXA Shield Plan A with AXA Basic Care and AXA Home Care: 1) Reimburses eligible expenses in private hospital standard rooms 2) 365 days of post-hospitalisation coverage 3) Covers Deductibles and Co-Insurance 4) Covers home nursing, home care medical services, GP home visits, and purchase or rental or mobility aids(within 30 days of post hospitalisation), Inpatient Hospice Care Daily Reimbursement(up to 90 days per Policy Year).  You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.\n\njackwife\nAXA Shield Plan B: 1) Reimburses eligible expenses in single-bedded wards in restructured (public) hospitals 2) 365 days of post-hospitalisation coverage.  You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.",
          limitation:
            "Jack\nAXA Shield Plan A with AXA Basic Care and AXA Home Care: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated under the additional benefits of your AXA Shield Policy). 5) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy.\n\nThis plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date.\n\nYou have bought the Basic Care Rider (no copay).\nIn line with the Ministry of Health guidelines issued on 8 Mar 2018, you will have to transit to a rider with co-payment of 5% (or more) from 1 April 2021 upon policy renewal. You also have the option to transit to a rider with co-payment earlier when it is available from 1 April 2019 upon policy renewal.\n\njackwife\nAXA Shield Plan B: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover Deductibles and Co-Insurance 5) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated under the additional benefits of your AXA Shield Policy). 6) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy.\n\nThis plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date.",
          reason: "asd",
          rop: {
            choiceQ1: "",
            choiceQ1Sub1: "",
            choiceQ1Sub2: "",
            choiceQ1Sub3: "",
            existCi: 0,
            existLife: 0,
            existPaAdb: 0,
            existTotalPrem: 0,
            existTpd: 0,
            replaceCi: 0,
            replaceLife: 0,
            replacePaAdb: 0,
            replaceTotalPrem: 0,
            replaceTpd: 0
          },
          rop_shield: {
            ropBlock: {
              iCidRopAnswerMap: {
                "CP001031-00002": "shieldRopAnswer_0",
                "CP001031-00003": "shieldRopAnswer_1"
              },
              ropQ1sub3: "",
              ropQ2: "",
              ropQ3: "",
              shieldRopAnswer_0: "N",
              shieldRopAnswer_1: "N",
              shieldRopAnswer_2: "",
              shieldRopAnswer_3: "",
              shieldRopAnswer_4: "",
              shieldRopAnswer_5: ""
            }
          }
        }
      },
      compCode: "01",
      createDate: "2018-09-11T02:55:02.129Z",
      dealerGroup: "AGENCY",
      extraFlags: {},
      id: "QU001031-00023",
      insureds: {
        "CP001031-00002": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "UAT.testapprovalmanager@axaplanner.com.sg",
            mobile: "+65 90728021",
            name: "KELVIN LIM TECK HWA",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 31,
          iCid: "CP001031-00002",
          iDob: "1988-05-17",
          iEmail: "test@test.com",
          iFirstName: "Jack",
          iFullName: "Jack",
          iGender: "M",
          iLastName: "",
          iOccupation: "O2",
          iOccupationClass: "DCL",
          iResidence: "R2",
          iSmoke: "Y",
          pAge: 31,
          pCid: "CP001031-00002",
          pDob: "1988-05-17",
          pEmail: "test@test.com",
          pFirstName: "Jack",
          pFullName: "Jack",
          pGender: "M",
          pLastName: "",
          pOccupation: "O2",
          pOccupationClass: "DCL",
          pResidence: "R2",
          pSmoke: "Y",
          paymentMode: "A",
          plans: [
            {
              covClass: "A",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan A)",
                "zh-Hant": "AXA Shield (Plan A)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSA",
              premium: 593,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 38.8
              },
              version: 3
            },
            {
              covClass: "A",
              covCode: "ASP",
              covName: {
                en: "AXA Basic Care (Plan A)",
                "zh-Hant": "AXA Basic Care (Plan A)"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASPA",
              premium: 37.7,
              productId: "08_product_ASP_3",
              productLine: "HP",
              tax: {
                monthTax: 2.47,
                yearTax: 29.64
              },
              version: 3,
              yearPrem: 452.4
            },
            {
              covClass: "1",
              covCode: "ASH",
              covName: {
                en: "AXA Home Care",
                "zh-Hant": "AXA Home Care"
              },
              payFreq: "M",
              payFreqDesc: "Monthly",
              paymentMethod: "CASH",
              paymentMethodDesc: "Cash",
              planCode: "ASHC",
              premium: 8.6,
              productId: "08_product_ASH_3",
              productLine: "HP",
              tax: {
                monthTax: 0.56,
                yearTax: 6.72
              },
              version: 3,
              yearPrem: 103.2
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 283,
            cashPortion: 0,
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 283,
            cashPortion: "-",
            cpfPortion: 283,
            medisave: 593,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "Y",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 46.3,
          totRiderYearPrem: 555.6,
          totYearCashPortion: 555.6,
          totYearPrem: 0,
          type: "quotation"
        },
        "CP001031-00003": {
          agent: {
            agentCode: "084620",
            company: "AXA Insurance Pte Ltd",
            dealerGroup: "AGENCY",
            email: "UAT.testapprovalmanager@axaplanner.com.sg",
            mobile: "+65 90728021",
            name: "KELVIN LIM TECK HWA",
            tel: "0"
          },
          agentCode: "084620",
          baseProductCode: "ASIM",
          baseProductId: "08_product_ASIM_3",
          baseProductName: {
            en: "AXA Shield",
            "zh-Hant": "AXA Shield"
          },
          budgetRules: ["RP"],
          ccy: "SGD",
          compCode: "01",
          dealerGroup: "AGENCY",
          iAge: 39,
          iCid: "CP001031-00003",
          iDob: "1980-09-04",
          iEmail: "",
          iFirstName: "jackwife",
          iFullName: "jackwife",
          iGender: "F",
          iLastName: "",
          iOccupation: "O675",
          iOccupationClass: "2",
          iResidence: "R2",
          iSmoke: "N",
          pAge: 31,
          pCid: "CP001031-00002",
          pDob: "1988-05-17",
          pEmail: "test@test.com",
          pFirstName: "Jack",
          pFullName: "Jack",
          pGender: "M",
          pLastName: "",
          pOccupation: "O2",
          pOccupationClass: "DCL",
          pResidence: "R2",
          pSmoke: "Y",
          paymentMode: "A",
          plans: [
            {
              covClass: "B",
              covCode: "ASIM",
              covName: {
                en: "AXA Shield (Plan B)",
                "zh-Hant": "AXA Shield (Plan B)"
              },
              payFreq: "A",
              payFreqDesc: "Annual",
              paymentMethod: "CPF",
              paymentMethodDesc: "CPF",
              planCode: "ASIMSB",
              premium: 466,
              productId: "08_product_ASIM_3",
              productLine: "HP",
              tax: {
                yearTax: 30.49
              },
              version: 3
            }
          ],
          policyOptions: {
            awl: 300,
            axaShield: 156,
            cashPortion: 0,
            cpfPortion: 156,
            medisave: 466,
            medishieldLife: 310
          },
          policyOptionsDesc: {
            awl: 300,
            axaShield: 156,
            cashPortion: "-",
            cpfPortion: 156,
            medisave: 466,
            medishieldLife: 310
          },
          premium: 0,
          productLine: "HP",
          productVersion: "2.0",
          sameAs: "N",
          totHalfyearPrem: 0,
          totMonthPrem: 0,
          totQuarterPrem: 0,
          totRiderPrem: 0,
          totRiderYearPrem: 0,
          totYearCashPortion: 0,
          totYearPrem: 0,
          type: "quotation"
        }
      },
      isBackDate: "N",
      lastUpdateDate: "2018-09-11T02:55:34.277Z",
      pAge: 31,
      pCid: "CP001031-00002",
      pDob: "1988-05-17",
      pEmail: "test@test.com",
      pFirstName: "Jack",
      pFullName: "Jack",
      pGender: "M",
      pLastName: "",
      pOccupation: "O2",
      pOccupationClass: "DCL",
      pResidence: "R2",
      pSmoke: "Y",
      premium: 1105.3,
      productLine: "HP",
      productVersion: "2.0",
      quotType: "SHIELD",
      riskCommenDate: "2018-09-11",
      totCashPortion: 46.3,
      totMedisave: 1059,
      totPremium: 1105.3,
      totYearCashPortion: 555.6,
      totYearPrem: 1614.6,
      type: "quotation"
    },
    quotationDocId: "QU001031-00023",
    signExpiryShown: false,
    submission: {},
    supportDocuments: {},
    type: "masterApplication"
  }
};
