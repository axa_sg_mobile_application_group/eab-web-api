export const FAKE_RECOMMENDATION_DATA = {
  recommendation: {
    "QU001003-01367": {
      extra: {
        proposerAndLifeAssuredName: "Alex 4  / Alex 4 Son ",
        basicPlanName: "AXA Band Aid (RP)",
        ridersName: [],
        recommendBenefitDefault:
          "AXA Band Aid is a comprehensive Accident & Health policy that provides cover up to age 75. It includes 5 core benefits which are payable when death OR injury occurs within 12 months from date of Accident. Please refer to Product Summary for the benefits and terms and conditions.\n\nThe Permanent Disablement Schedule is split into 4 categories with different Scope of Coverage and payout amount based on a percentage of your Sum Assured. \n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.",
        recommendLimitDefault:
          "There are certain conditions under which no benefits will be payable under the Policy:\n1) Any pre-existing condition 2) Acts of war, terrorism, riot, strike or civil commotion, if the Life Assured collaborated, participated or provoked such act directly or indirectly. 3) Engaging in illegal or criminal acts 4) Hazardous pursuits including but not limited to sky diving, diving, motorcar and motorcycle racing, deep sea fishing, deep sea diving, scuba diving, skiing, mountaineering and parachuting. Please refer to the policy contract for the full list of exclusions. \nAXA Band Aid premiums rates are based on your Occupation Class and rates may be adjusted based on future experience. Policy is not guaranteed renewable. We must receive in writing, immediately or as soon as possible, information on any change in the Life Assuredâ€™s occupation, country of residence, or pursuits. We have the right to vary the terms and conditions of this Policy and/or adjust the Premiums payable. If there has been a failure to notify us as required in this clause, We may, in the event of a claim being made under this Policy, repudiate the claim or adjust the Benefits payable at Our discretion.\n\nThe maximum payable Accidental-related benefits is S$3.5 million inclusive of all other policies issued by AXA and other insurance companies, in respect of the same Life Assured. Refer to your Product Summary for definition of Accident, Schedule of Permanent Disablement Benefits with scope of coverage, the exclusions, terms & conditions for Benefits to be payable.",
        paymentMode: "A",
        isShield: false,
        lastUpdateDate: "2018-06-15T06:14:52.217Z"
      },
      benefit:
        "AXA Band Aid is a comprehensive Accident & Health policy that provides cover up to age 75. It includes 5 core benefits which are payable when death OR injury occurs within 12 months from date of Accident. Please refer to Product Summary for the benefits and terms and conditions.\n\nThe Permanent Disablement Schedule is split into 4 categories with different Scope of Coverage and payout amount based on a percentage of your Sum Assured. \n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.",
      limitation:
        "There are certain conditions under which no benefits will be payable under the Policy:\n1) Any pre-existing condition 2) Acts of war, terrorism, riot, strike or civil commotion, if the Life Assured collaborated, participated or provoked such act directly or indirectly. 3) Engaging in illegal or criminal acts 4) Hazardous pursuits including but not limited to sky diving, diving, motorcar and motorcycle racing, deep sea fishing, deep sea diving, scuba diving, skiing, mountaineering and parachuting. Please refer to the policy contract for the full list of exclusions. \nAXA Band Aid premiums rates are based on your Occupation Class and rates may be adjusted based on future experience. Policy is not guaranteed renewable. We must receive in writing, immediately or as soon as possible, information on any change in the Life Assured’s occupation, country of residence, or pursuits. We have the right to vary the terms and conditions of this Policy and/or adjust the Premiums payable. If there has been a failure to notify us as required in this clause, We may, in the event of a claim being made under this Policy, repudiate the claim or adjust the Benefits payable at Our discretion.\n\nThe maximum payable Accidental-related benefits is S$3.5 million inclusive of all other policies issued by AXA and other insurance companies, in respect of the same Life Assured. Refer to your Product Summary for definition of Accident, Schedule of Permanent Disablement Benefits with scope of coverage, the exclusions, terms & conditions for Benefits to be payable.",
      reason: "",
      rop: {
        choiceQ1: "",
        choiceQ1Sub1: "",
        choiceQ1Sub2: "",
        choiceQ1Sub3: "",
        existCi: 0,
        existLife: 1111,
        existPaAdb: 0,
        existTotalPrem: 222,
        existTpd: 0,
        replaceCi: 0,
        replaceLife: 0,
        replacePaAdb: 0,
        replaceTotalPrem: 0,
        replaceTpd: 0
      },
      rop_shield: {
        ropBlock: {
          iCidRopAnswerMap: {},
          ropQ1sub3: "",
          ropQ2: "",
          ropQ3: "",
          shieldRopAnswer_0: "",
          shieldRopAnswer_1: "",
          shieldRopAnswer_2: "",
          shieldRopAnswer_3: "",
          shieldRopAnswer_4: "",
          shieldRopAnswer_5: ""
        }
      }
    },
    "QU001003-01366": {
      extra: {
        proposerAndLifeAssuredName: "Alex 4 ",
        basicPlanName: "SavvySaver (RP)",
        ridersName: [
          {
            en: "PremiumEraser Total",
            "zh-Hant": "PremiumEraser Total"
          }
        ],
        recommendBenefitDefault:
          "SavvySaver is a regular premium participating anticipated endowment policy. It provides you Death and Terminal Illness coverage, yearly Guaranteed Cash Payouts from the end of the second Policy year and a Guaranteed Maturity benefit plus bonuses (if any), payable to you in a lump sum upon Policy maturity. SavvySaver terminates upon full payout of either your TI benefit, Death benefit, Policy maturity amount or any other reasons as stated in Product Summary.\n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.\n\nPremiumEraser Total Rider will waive future Premiums upon Total and Permanent Disability before the Policy Anniversary nearest to your 70th year birthday or you are diagnosed with one of the 36 Critical Illness, while this Additional Benefit is in force.",
        recommendLimitDefault:
          "The Sum Assured is a notional value, used solely for the calculation of Guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses â€“ it is not the amount payable in the event of Death or Terminal Illness. For the actual payout in the event of death and terminal illness please refer to the product summary.\nGuaranteed Cash Payouts could be deposited with us at a non-guaranteed interest rate. You can withdraw partial OR all of the re-deposited amounts with earned interest, subject to the minimum withdrawal amount equal to 1 Guaranteed Cash Payout payment. The bonus rates used for the policy illustration are non-guaranteed, the actual benefit payable may vary according to the future performance of the Participating Fund. There are certain conditions under which no benefits will be payable under this Policy. These are stated as exclusions in this Policy. Refer to your Product Summary for the exclusions, terms & conditions for payable Benefits.\nBuying a life insurance is a long-term commitment. An early termination of this Policy usually involves high costs and the surrender value may be less than the total Premiums paid.\n\nPremiumEraser Total Rider terminates upon its coverage expiry date of this additional benefit OR when Policy is assigned OR when you no longer suffers from TPD  or any other event which results in the termination of this Additional Benefit as set out in the Policy.\nThe premium rates for this Additional Benefit are not guaranteed. The rates may be adjusted based on future experience. Premiums are payable throughout the term of this Additional Benefit.\nPlease refer to product summary for terms and conditions and exclusions.",
        paymentMode: "S",
        isShield: false,
        lastUpdateDate: "2018-06-15T06:14:27.876Z"
      },
      benefit:
        "SavvySaver is a regular premium participating anticipated endowment policy. It provides you Death and Terminal Illness coverage, yearly Guaranteed Cash Payouts from the end of the second Policy year and a Guaranteed Maturity benefit plus bonuses (if any), payable to you in a lump sum upon Policy maturity. SavvySaver terminates upon full payout of either your TI benefit, Death benefit, Policy maturity amount or any other reasons as stated in Product Summary.\n\nFree-Look Period: We will give you a period of fourteen (14) days from the date you receive this Policy to review it. If your Policy is delivered by post or email, it is considered to have been received by you seven (7) days from the date of posting or email. If you decide to cancel this Policy, you must write to us and return the Policy documents within the period of 14 days allowed. We will refund the Premium paid less any medical fees and other expenses such as payments for medical check-ups and medical reports incurred in processing your Application.\n\nPremiumEraser Total Rider will waive future Premiums upon Total and Permanent Disability before the Policy Anniversary nearest to your 70th year birthday or you are diagnosed with one of the 36 Critical Illness, while this Additional Benefit is in force.",
      limitation:
        "The Sum Assured is a notional value, used solely for the calculation of Guaranteed Maturity Payout, Guaranteed Cash Payout, Reversionary and Terminal Bonuses – it is not the amount payable in the event of Death or Terminal Illness. For the actual payout in the event of death and terminal illness please refer to the product summary.\nGuaranteed Cash Payouts could be deposited with us at a non-guaranteed interest rate. You can withdraw partial OR all of the re-deposited amounts with earned interest, subject to the minimum withdrawal amount equal to 1 Guaranteed Cash Payout payment. The bonus rates used for the policy illustration are non-guaranteed, the actual benefit payable may vary according to the future performance of the Participating Fund. There are certain conditions under which no benefits will be payable under this Policy. These are stated as exclusions in this Policy. Refer to your Product Summary for the exclusions, terms & conditions for payable Benefits.\nBuying a life insurance is a long-term commitment. An early termination of this Policy usually involves high costs and the surrender value may be less than the total Premiums paid.\n\nPremiumEraser Total Rider terminates upon its coverage expiry date of this additional benefit OR when Policy is assigned OR when you no longer suffers from TPD  or any other event which results in the termination of this Additional Benefit as set out in the Policy.\nThe premium rates for this Additional Benefit are not guaranteed. The rates may be adjusted based on future experience. Premiums are payable throughout the term of this Additional Benefit.\nPlease refer to product summary for terms and conditions and exclusions.",
      reason: "",
      rop: {
        choiceQ1: "",
        choiceQ1Sub1: "",
        choiceQ1Sub2: "",
        choiceQ1Sub3: "",
        existCi: 0,
        existLife: 3333,
        existPaAdb: 0,
        existTotalPrem: 44,
        existTpd: 0,
        replaceCi: 0,
        replaceLife: 0,
        replacePaAdb: 0,
        replaceTotalPrem: 0,
        replaceTpd: 0
      },
      rop_shield: {
        ropBlock: {
          iCidRopAnswerMap: {},
          ropQ1sub3: "",
          ropQ2: "",
          ropQ3: "",
          shieldRopAnswer_0: "",
          shieldRopAnswer_1: "",
          shieldRopAnswer_2: "",
          shieldRopAnswer_3: "",
          shieldRopAnswer_4: "",
          shieldRopAnswer_5: ""
        }
      }
    },
    "QU001003-01365": {
      extra: {
        proposerAndLifeAssuredName: "",
        basicPlanName: "AXA Shield Plan (RP)",
        ridersName: [],
        shieldInsuredPlans: [
          {
            shieldInsured: "Alex 4:",
            shieldPlan:
              "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
          }
        ],
        shieldInsuredAndPlanGroupList: [
          {
            insuredNameList: ["Alex 4 "],
            planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
            planNameList: [
              "AXA Shield (Plan A)",
              "AXA Basic Care (Plan A)",
              "AXA General Care (Plan A)",
              "AXA Home Care"
            ]
          }
        ],
        recommendBenefitDefault:
          "Alex 4\nAXA Shield Plan A with AXA Basic Care, AXA General Care and AXA Home Care: 1) Reimburses eligible expenses in private hospital standard rooms 2) 365 days of post-hospitalisation coverage 3) Covers Deductibles and Co-Insurance 4) Provides Daily Hospital Cash Incentive of up to $250 if the Life Assured stays in a four-bedded or lower ward class in public hospital 5) Covers Traditional Chinese Medicine treatment, certain planned overseas medical treatments, emergency outpatient treatments due to an accident, outpatient treatments for fractures, dislocations, sports injuries, food poisoning, dengue and Hand Foot and Mouth Disease 6)  Covers home nursing, home care medical services, GP home visits, and purchase or rental of mobility aids(within 30 days of post hospitalisation), Inpatient Hospice Care Daily Reimbursement(up to 90 days per Policy Year). You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.",
        recommendLimitDefault:
          "Alex 4\nAXA Shield Plan A with AXA Basic Care, AXA General Care and AXA Home Care: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated in the AXA General Care Supplementary Provisions). 5) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy. \n\nThe plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date.\n\nYou have bought the Basic Care Rider (no copay).\nIn line with the Ministry of Health guidelines issued on 8 Mar 2018, you will have to transit to a rider with co-payment of 5% (or more) from 1 April 2021 upon policy renewal. You also have the option to transit to a rider with co-payment earlier when it is available from 1 April 2019 upon policy renewal.",
        paymentMode: "S",
        isShield: true,
        lastUpdateDate: "2018-06-15T06:10:26.983Z"
      },
      benefit:
        "Alex 4\nAXA Shield Plan A with AXA Basic Care, AXA General Care and AXA Home Care: 1) Reimburses eligible expenses in private hospital standard rooms 2) 365 days of post-hospitalisation coverage 3) Covers Deductibles and Co-Insurance 4) Provides Daily Hospital Cash Incentive of up to $250 if the Life Assured stays in a four-bedded or lower ward class in public hospital 5) Covers Traditional Chinese Medicine treatment, certain planned overseas medical treatments, emergency outpatient treatments due to an accident, outpatient treatments for fractures, dislocations, sports injuries, food poisoning, dengue and Hand Foot and Mouth Disease 6)  Covers home nursing, home care medical services, GP home visits, and purchase or rental of mobility aids(within 30 days of post hospitalisation), Inpatient Hospice Care Daily Reimbursement(up to 90 days per Policy Year). You can decide within 21 days from the date of receipt of the Policy whether You want to continue with Your Policy.  If You do not want to continue, You may cancel this Policy by giving us written notice and We shall refund the Premiums paid for this Policy.",
      limitation:
        "Alex 4\nAXA Shield Plan A with AXA Basic Care, AXA General Care and AXA Home Care: 1) Premiums are not guaranteed 2) Premiums increase with age band 3) The additional private insurance coverage portion of AXA Shield premium in excess of the AWL is not payable via Medisave 4) Does not cover medical treatment outside of Singapore, except in the case of emergency, or certain planned medical treatments (as stated in the AXA General Care Supplementary Provisions). 5) Pro-ration Factors will apply if Life Assured is Hospitalized in a higher class ward than the Hospital class ward entitled under your policy. \n\nThe plan does not cover treatment for congenital abnormalities such as, but not limited to genetics, hereditary conditions and physical or birth defects from childbirth, and first diagnosed by a Physician or signs and symptoms were first presented within 365 days from the Effective Date or last Reinstatement Date.\n\nYou have bought the Basic Care Rider (no copay).\nIn line with the Ministry of Health guidelines issued on 8 Mar 2018, you will have to transit to a rider with co-payment of 5% (or more) from 1 April 2021 upon policy renewal. You also have the option to transit to a rider with co-payment earlier when it is available from 1 April 2019 upon policy renewal.",
      reason: "",
      rop: {
        choiceQ1: "",
        choiceQ1Sub1: "",
        choiceQ1Sub2: "",
        choiceQ1Sub3: "",
        existCi: 0,
        existLife: 0,
        existPaAdb: 0,
        existTotalPrem: 0,
        existTpd: 0,
        replaceCi: 0,
        replaceLife: 0,
        replacePaAdb: 0,
        replaceTotalPrem: 0,
        replaceTpd: 0
      },
      rop_shield: {
        ropBlock: {
          iCidRopAnswerMap: {
            "CP001003-00006": "shieldRopAnswer_0"
          },
          ropQ1sub3: "",
          ropQ2: "",
          ropQ3: "",
          shieldRopAnswer_0: "",
          shieldRopAnswer_1: "",
          shieldRopAnswer_2: "",
          shieldRopAnswer_3: "",
          shieldRopAnswer_4: "",
          shieldRopAnswer_5: ""
        }
      }
    },
    chosenList: [],
    notChosenList: []
  },
  budget: {
    spBudget: 222,
    spTotalPremium: 0,
    spCompare: 222,
    spCompareResult: "Underutilized",
    rpBudget: 1111,
    rpTotalPremium: 75.44,
    rpCompare: 1035.56,
    rpCompareResult: "Underutilized",
    cpfOaBudget: 333,
    cpfOaTotalPremium: 0,
    cpfOaCompare: 333,
    cpfOaCompareResult: "Underutilized",
    cpfSaBudget: 333,
    cpfSaTotalPremium: 0,
    cpfSaCompare: 333,
    cpfSaCompareResult: "Underutilized",
    srsBudget: 333,
    srsTotalPremium: 0,
    srsCompare: 333,
    srsCompareResult: "Underutilized",
    cpfMsBudget: 3331,
    cpfMsTotalPremium: 0,
    cpfMsCompare: 3331,
    cpfMsCompareResult: "Underutilized",
    budgetMoreChoice: "",
    budgetMoreReason: "",
    budgetLessChoice: "",
    budgetLessReason: ""
  },
  acceptance: [
    {
      quotationDocId: "QU001003-01367",
      proposerAndLifeAssuredName: "Alex 4  / Alex 4 Son ",
      basicPlanName: "AXA Band Aid (RP)",
      ridersName: []
    },
    {
      quotationDocId: "QU001003-01366",
      proposerAndLifeAssuredName: "Alex 4 ",
      basicPlanName: "SavvySaver (RP)",
      ridersName: [
        {
          en: "PremiumEraser Total",
          "zh-Hant": "PremiumEraser Total"
        },
        {
          en: "PremiumEraser Total 2",
          "zh-Hant": "PremiumEraser Total 2"
        }
      ]
    }
  ]
};

export default {};
