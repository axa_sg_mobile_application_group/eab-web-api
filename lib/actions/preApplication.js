"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateUiAppListFilter = exports.updateUiSelectMode = exports.updateUiSelectedList = exports.updateRecommendChoiceList = exports.deleteApplications = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * saga actions
 */

/**
 * deleteApplications
 * @description delete applications selected in app/web
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * */
var deleteApplications = exports.deleteApplications = function deleteApplications(_ref) {
  var dispatch = _ref.dispatch,
      callback = _ref.callback;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].SAGA_DELETE_APPLICATIONS,
    callback: callback
  });
};

/**
 * updateRecommendChoiceList
 * @description update recommendation choice list selected in app/web
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {object} choiceData - updated data from app/web
 * @param {string} choiceData.id - quotation ID
 * @param {bool} choiceData.checked - choice is selected or not for the quotation
 * */
var updateRecommendChoiceList = exports.updateRecommendChoiceList = function updateRecommendChoiceList(_ref2) {
  var dispatch = _ref2.dispatch,
      choiceData = _ref2.choiceData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].SAGA_UPDATE_RECOMMENDATION_CHOICE_LIST,
    choiceData: choiceData
  });
};

/**
 * updateUiSelectedList
 * @description keep the select list under select mode in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {object} changeData - updated data from app/web
 * @param {string} changeData.id - quotation ID / application ID
 * @param {bool} changeData.selected - updated data from app/web
 * */
var updateUiSelectedList = exports.updateUiSelectedList = function updateUiSelectedList(_ref3) {
  var dispatch = _ref3.dispatch,
      changeData = _ref3.changeData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].SAGA_UPDATE_UI_SELECTED_LIST,
    changeData: changeData
  });
};

/**
 * normal actions
 */

/**
 * updateUiSelectMode
 * @description indicate if select mode is on in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {bool} selectModeOn - flag to indicate select mode is on or not
 * */
var updateUiSelectMode = exports.updateUiSelectMode = function updateUiSelectMode(_ref4) {
  var dispatch = _ref4.dispatch,
      selectModeOn = _ref4.selectModeOn;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECT_MODE,
    selectModeOn: selectModeOn
  });
};

/**
 * updateUiAppListFilter
 * @description indicate which application list filter is selected in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {number} selectedAppListFilter - selected filter index
 * */
var updateUiAppListFilter = exports.updateUiAppListFilter = function updateUiAppListFilter(_ref5) {
  var dispatch = _ref5.dispatch,
      selectedAppListFilter = _ref5.selectedAppListFilter;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_APPLICATION_LIST_FILTER,
    selectedAppListFilter: selectedAppListFilter
  });
};