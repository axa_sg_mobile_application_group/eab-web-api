"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateAvailableFunds = exports.updateAvailableInsureds = exports.updateQuotationWarnings = exports.updateQuotationErrors = exports.updateInputConfigs = exports.updatePlanDetails = exports.updateQuotation = exports.getFunds = exports.getQuotation = exports.quickQuoteOnChange = exports.cleanQuotation = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * cleanQuotation
 * @description get quotation data
 * @reducer quotation
 * @param {function} dispatch - redux dispatch function
 * */
var cleanQuotation = exports.cleanQuotation = function cleanQuotation(_ref) {
  var dispatch = _ref.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION
  });
};

/**
 * quickQuoteOnChange
 * @description change reducer quotation.isQuickQuote state
 * @reducer quotation.isQuickQuote
 * @param {function} dispatch - redux dispatch function
 * @param {string} isQuickQuoteData - new isQuickQuote data
 * */
var quickQuoteOnChange = exports.quickQuoteOnChange = function quickQuoteOnChange(_ref2) {
  var dispatch = _ref2.dispatch,
      isQuickQuoteData = _ref2.isQuickQuoteData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_IS_QUICK_QUOTE,
    isQuickQuoteData: isQuickQuoteData
  });
};

/**
 * getQuotation
 * @description get quotation data
 * @param {function} dispatch - redux dispatch function
 * @param {string} productID - ID of quotation source product
 * @param {function} openQuotationFunction - callback when quotation require
 *   data is ready
 * */
var getQuotation = exports.getQuotation = function getQuotation(_ref3) {
  var dispatch = _ref3.dispatch,
      productID = _ref3.productID,
      openQuotationFunction = _ref3.openQuotationFunction;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION,
    productID: productID,
    openQuotationFunction: openQuotationFunction
  });
};

/**
 * getFunds
 * @description get quotation data
 * @param {function} dispatch - redux dispatch function
 * @param {string} productID - ID of quotation source product
 * @param {function} invOpt - investment option
 * @param {string} paymentMethod - payment method
 * @param {function} openSelectFundsDialogFunction - call back when select
 *   funds dialog require data is ready
 * */
var getFunds = exports.getFunds = function getFunds(_ref4) {
  var dispatch = _ref4.dispatch,
      productID = _ref4.productID,
      invOpt = _ref4.invOpt,
      paymentMethod = _ref4.paymentMethod,
      openSelectFundsDialogFunction = _ref4.openSelectFundsDialogFunction;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_FUNDS,
    productID: productID,
    invOpt: invOpt,
    paymentMethod: paymentMethod,
    openSelectFundsDialogFunction: openSelectFundsDialogFunction
  });
};

/**
 * updateQuotation
 * @description update quotation
 * @reducer quotation.quotation
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotation - new quotation object
 * */
var updateQuotation = exports.updateQuotation = function updateQuotation(_ref5) {
  var dispatch = _ref5.dispatch,
      newQuotation = _ref5.newQuotation;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION,
    newQuotation: newQuotation
  });
};

/**
 * updatePlanDetails
 * @description update quotation plan details
 * @reducer quotation.planDetails
 * @param {function} dispatch - redux dispatch function
 * @param {object} newPlanDetails - new quotation plan details object
 * */
var updatePlanDetails = exports.updatePlanDetails = function updatePlanDetails(_ref6) {
  var dispatch = _ref6.dispatch,
      newPlanDetails = _ref6.newPlanDetails;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_PLAN_DETAILS,
    newPlanDetails: newPlanDetails
  });
};

/**
 * updateInputConfigs
 * @description update quotation input configs
 * @reducer quotation.inputConfigs
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotation - new quotation input configs object
 * */
var updateInputConfigs = exports.updateInputConfigs = function updateInputConfigs(_ref7) {
  var dispatch = _ref7.dispatch,
      newInputConfigs = _ref7.newInputConfigs;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INPUT_CONFIGS,
    newInputConfigs: newInputConfigs
  });
};

/**
 * updateQuotationErrors
 * @description update quotation errors configs
 * @reducer quotation.quotationErrors
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotationErrors - new quotation errors object
 * */
var updateQuotationErrors = exports.updateQuotationErrors = function updateQuotationErrors(_ref8) {
  var dispatch = _ref8.dispatch,
      newQuotationErrors = _ref8.newQuotationErrors;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION_ERRORS,
    newQuotationErrors: newQuotationErrors
  });
};

/**
 * updateQuotationWarnings
 * @description update quotation warning configs
 * @reducer quotation.quotationWarning
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotationWarnings - new quotation warning object
 * */
var updateQuotationWarnings = exports.updateQuotationWarnings = function updateQuotationWarnings(_ref9) {
  var dispatch = _ref9.dispatch,
      newQuotationWarnings = _ref9.newQuotationWarnings;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION_WARNINGS,
    newQuotationWarnings: newQuotationWarnings
  });
};

/**
 * updateAvailableInsureds
 * @description update quotation available insureds configs
 * @reducer quotation.availableInsureds
 * @param {function} dispatch - redux dispatch function
 * @param {object} newAvailableInsureds - new quotation available insureds
 *   object
 * */
var updateAvailableInsureds = exports.updateAvailableInsureds = function updateAvailableInsureds(_ref10) {
  var dispatch = _ref10.dispatch,
      newAvailableInsureds = _ref10.newAvailableInsureds;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_INSUREDS,
    newAvailableInsureds: newAvailableInsureds
  });
};

/**
 * updateAvailableFunds
 * @description update quotation available funds configs
 * @reducer quotation.availableFunds
 * @param {function} dispatch - redux dispatch function
 * @param {object} newAvailableFunds - new quotation available funds object
 * */
var updateAvailableFunds = exports.updateAvailableFunds = function updateAvailableFunds(_ref11) {
  var dispatch = _ref11.dispatch,
      newAvailableFunds = _ref11.newAvailableFunds;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_FUNDS,
    newAvailableFunds: newAvailableFunds
  });
};