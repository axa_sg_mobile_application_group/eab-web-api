"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateProfile = exports.getProfile = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * getProfile
 * @description get profile
 * @param {function} dispatch - redux dispatch function
 * @param {string} cid - client ID
 * @param {function} callback - callback of this dispatch
 * */
var getProfile = exports.getProfile = function getProfile(_ref) {
  var dispatch = _ref.dispatch,
      cid = _ref.cid,
      callback = _ref.callback;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].GET_PROFILE,
    cid: cid,
    callback: callback
  });
};

/**
 * updateProfile
 * @description update Profile
 * @param {function} dispatch - redux dispatch function
 * @param {object} newProfile - new profile object
 * */
var updateProfile = exports.updateProfile = function updateProfile(_ref2) {
  var dispatch = _ref2.dispatch,
      newProfile = _ref2.newProfile;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE,
    newProfile: newProfile
  });
};