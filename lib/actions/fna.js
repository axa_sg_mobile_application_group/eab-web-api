"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateCKAValue = exports.updateNAValue = exports.updatePDAValue = exports.updatePDA = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _PROGRESS_TABS = require("../constants/PROGRESS_TABS");

var _PROGRESS_TABS2 = _interopRequireDefault(_PROGRESS_TABS);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updatePDA = exports.updatePDA = function updatePDA(_ref) {
  var dispatch = _ref.dispatch,
      value = _ref.value;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA,
    value: value
  });
};

var updatePDAValue = exports.updatePDAValue = function updatePDAValue(_ref2) {
  var dispatch = _ref2.dispatch,
      target = _ref2.target,
      value = _ref2.value;

  switch (target) {
    case "applicant":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_APPLICATE,
        newPda: value
      });
      break;
    case "dependants":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_DEPENDANTS,
        newPda: value
      });
      break;
    case "isCompleted":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_ISCOMPLETED,
        newPda: value
      });
      break;
    case "lastStepIndex":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_LAST_STEP_INDEX,
        newPda: value
      });
      break;
    case "applicantHasChanged":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_APPLICANT_HAS_CHANGED,
        newPda: value
      });
      break;
    case "consentNoticesHasChanged":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_CONSENT_NOTICES_HAS_CHANGED,
        newPda: value
      });
      break;
    case "ownerConsentMethod":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_OWNERCONSENTMETHOD,
        newPda: value
      });
      break;
    case "spouseConsentMethod":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_SPOUSECONSENTMETHOD,
        newPda: value
      });
      break;
    case "trustedIndividual":
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_TRUSTINDIVIDUAL,
        newPda: value
      });
      break;
    default:
      break;
  }
};

/**
 * updateNAValue
 * @description Update the Needs Analysis Value
 * @param {function} dispatch - redux dispatch function
 * @param {string} target - which type of Needs Analysis out of 5
 * @param {object} value - the content which use to change
 * */
var updateNAValue = exports.updateNAValue = function updateNAValue(_ref3) {
  var dispatch = _ref3.dispatch,
      target = _ref3.target,
      value = _ref3.value;

  switch (target) {
    case _PROGRESS_TABS2.default[_REDUCER_TYPES.FNA].NEEDS:
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_NEEDS,
        value: value
      });
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS,
        value: value
      });
      break;
    case _PROGRESS_TABS2.default[_REDUCER_TYPES.FNA].PRIORITY:
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY,
        value: value
      });
      break;
    case _PROGRESS_TABS2.default[_REDUCER_TYPES.FNA].PRODUCT_TYPES:
      dispatch({
        type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP,
        value: value
      });
      break;

    default:
      break;
  }
};

/**
 * updateCKAValue
 * @description Update the Customer Knowledge Assessment Value
 * @param {function} dispatch - redux dispatch function
 * @param {string} key - the CKA key which need to change
 * @param {object} value - the value which need to change
 * */
var updateCKAValue = exports.updateCKAValue = function updateCKAValue(_ref4) {
  var dispatch = _ref4.dispatch,
      data = _ref4.data;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_CKA,
    data: data
  });
};