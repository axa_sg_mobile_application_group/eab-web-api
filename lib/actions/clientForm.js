"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.typeOthPassOtherOnChange = exports.photoOnChange = exports.buildingEstateOnChange = exports.unitOnChange = exports.streetRoadOnChange = exports.blockHouseOnChange = exports.postalOnChange = exports.cityStateOnChange = exports.validateEmail = exports.emailOnChange = exports.mobileNoBOnChange = exports.prefixBOnChange = exports.mobileNoAOnChange = exports.prefixAOnchange = exports.incomeOnChange = exports.employStatusOtherOnChange = exports.nameOfEmployBusinessSchoolOnChange = exports.languageOtherOnChange = exports.languageOnChange = exports.validateID = exports.IDOnChange = exports.nameOnChange = exports.hanYuPinYinNameOnChange = exports.otherNameOnChange = exports.surnameOnChange = exports.givenNameOnChange = exports.cancelCreateClient = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * cancelCreateClient
 * @description clean all client form data
 * @reducer clientForm
 * @param {function} dispatch - redux dispatch function
 * */
var cancelCreateClient = exports.cancelCreateClient = function cancelCreateClient(_ref) {
  var dispatch = _ref.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
  });
};
/**
 * givenNameOnChange
 * @description change givenName reducer state
 * @reducer clientForm.givenName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newGivenName - new reducer value
 * */
var givenNameOnChange = exports.givenNameOnChange = function givenNameOnChange(_ref2) {
  var dispatch = _ref2.dispatch,
      newGivenName = _ref2.newGivenName;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
    newGivenName: newGivenName
  });
};
/**
 * surnameOnChange
 * @description change surname reducer state
 * @reducer clientForm.surname
 * @param {function} dispatch - redux dispatch function
 * @param {string} newSurname - new reducer value
 * */
var surnameOnChange = exports.surnameOnChange = function surnameOnChange(_ref3) {
  var dispatch = _ref3.dispatch,
      newSurname = _ref3.newSurname;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].SURNAME_ON_CHANGE,
    newSurname: newSurname
  });
};
/**
 * otherNameOnChange
 * @description change otherName reducer state
 * @reducer clientForm.otherName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newOtherName - new reducer value
 * */
var otherNameOnChange = exports.otherNameOnChange = function otherNameOnChange(_ref4) {
  var dispatch = _ref4.dispatch,
      newOtherName = _ref4.newOtherName;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OTHER_NAME_ON_CHANGE,
    newOtherName: newOtherName
  });
};
/**
 * hanYuPinYinNameOnChange
 * @description change hanYuPinYinName reducer state
 * @reducer clientForm.hanYuPinYinName
 * @param {function} dispatch - redux dispatch function
 * @param {string} newHanYuPinYinName - new reducer value
 * */
var hanYuPinYinNameOnChange = exports.hanYuPinYinNameOnChange = function hanYuPinYinNameOnChange(_ref5) {
  var dispatch = _ref5.dispatch,
      newHanYuPinYinName = _ref5.newHanYuPinYinName;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].HAN_YU_PIN_YIN_ON_CHANGE,
    newHanYuPinYinName: newHanYuPinYinName
  });
};
/**
 * nameOnChange
 * @description change name reducer state
 * @reducer clientForm.name
 * @param {function} dispatch - redux dispatch function
 * @param {string} newName - new reducer value
 * */
var nameOnChange = exports.nameOnChange = function nameOnChange(_ref6) {
  var dispatch = _ref6.dispatch,
      newName = _ref6.newName;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_ON_CHANGE,
    newName: newName
  });
};
/**
 * IDOnChange
 * @description change ID reducer state
 * @reducer clientForm.ID
 * @param {function} dispatch - redux dispatch function
 * @param {string} newID - new reducer value
 * */
var IDOnChange = exports.IDOnChange = function IDOnChange(_ref7) {
  var dispatch = _ref7.dispatch,
      newID = _ref7.newID;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_ON_CHANGE,
    newID: newID
  });
};
/**
 * validateID
 * @description ID validation
 * @reducer clientForm.ID
 * @reducer clientForm.IDType
 * @reducer clientForm.isIDError
 * @param {function} dispatch - redux dispatch function
 * @param {string} IDData - ID value
 * @param {string} IDTypeData - ID format
 * */
var validateID = exports.validateID = function validateID(_ref8) {
  var dispatch = _ref8.dispatch,
      IDData = _ref8.IDData,
      IDTypeData = _ref8.IDTypeData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID,
    IDData: IDData,
    IDTypeData: IDTypeData
  });
};
/**
 * languageOnChange
 * @description change language reducer state
 * @reducer clientForm.language
 * @param {function} dispatch - redux dispatch function
 * @param {string} newLanguage - new reducer value
 * */
var languageOnChange = exports.languageOnChange = function languageOnChange(_ref9) {
  var dispatch = _ref9.dispatch,
      newLanguage = _ref9.newLanguage;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE,
    newLanguage: newLanguage
  });
};

/**
 * languageOnChange
 * @description change languageOther reducer state
 * @reducer clientForm.languageOther
 * @param {function} dispatch - redux dispatch function
 * @param {string} newLanguage - new reducer value
 * */
var languageOtherOnChange = exports.languageOtherOnChange = function languageOtherOnChange(_ref10) {
  var dispatch = _ref10.dispatch,
      newLanguageOther = _ref10.newLanguageOther;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE,
    newLanguageOther: newLanguageOther
  });
};

/**
 * nameOfEmployBusinessSchoolOnChange
 * @description change nameOfEmployBusinessSchool reducer state
 * @reducer clientForm.nameOfEmployBusinessSchool
 * @param {function} dispatch - redux dispatch function
 * @param {string} newNameOfEmployBusinessSchool - new reducer value
 * */
var nameOfEmployBusinessSchoolOnChange = exports.nameOfEmployBusinessSchoolOnChange = function nameOfEmployBusinessSchoolOnChange(_ref11) {
  var dispatch = _ref11.dispatch,
      newNameOfEmployBusinessSchool = _ref11.newNameOfEmployBusinessSchool;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
    newNameOfEmployBusinessSchool: newNameOfEmployBusinessSchool
  });
};

/**
 * employStatusOtherOnChange
 * @description change employStatusOtherOnChange reducer state
 * @reducer clientForm.employStatusOtherOnChange
 * @param {function} dispatch - redux dispatch function
 * @param {string} employStatusOtherOnChange - new reducer value
 * * @param {string} employStatusOnChange - new reducer value
 * */
var employStatusOtherOnChange = exports.employStatusOtherOnChange = function employStatusOtherOnChange(_ref12) {
  var dispatch = _ref12.dispatch,
      newEmployStatus = _ref12.newEmployStatus,
      newEmployStatusOther = _ref12.newEmployStatusOther;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE,
    newEmployStatus: newEmployStatus,
    newEmployStatusOther: newEmployStatusOther
  });
};

/**
 * incomeOnChange
 * @description change incomeOnChange reducer state
 * @reducer clientForm.incomeOnChange
 * @param {function} dispatch - redux dispatch function
 * @param {string} newIncome - new reducer value
 * */
var incomeOnChange = exports.incomeOnChange = function incomeOnChange(_ref13) {
  var dispatch = _ref13.dispatch,
      newIncome = _ref13.newIncome;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INCOME_ON_CHANGE,
    newIncome: newIncome
  });
};
/**
 * prefixAOnchange
 * @description change prefixA reducer state
 * @reducer clientForm.prefixA
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPrefixA - new reducer value
 * */
var prefixAOnchange = exports.prefixAOnchange = function prefixAOnchange(_ref14) {
  var dispatch = _ref14.dispatch,
      newPrefixA = _ref14.newPrefixA;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE,
    newPrefixA: newPrefixA
  });
};
/**
 * mobileNoAOnChange
 * @description change mobileNoA reducer state
 * @reducer clientForm.mobileNoA
 * @reducer clientForm.prefixA
 * @param {function} dispatch - redux dispatch function
 * @param {string} newMobileNoA - new reducer value
 * @param {string} prefix - mobile number prefix
 * */
var mobileNoAOnChange = exports.mobileNoAOnChange = function mobileNoAOnChange(_ref15) {
  var dispatch = _ref15.dispatch,
      newMobileNoA = _ref15.newMobileNoA,
      prefix = _ref15.prefix;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
    newMobileNoA: newMobileNoA,
    prefix: prefix
  });
};
/**
 * prefixBOnChange
 * @description change prefixB reducer state
 * @reducer clientForm.prefixB
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPrefixB - new reducer value
 * */
var prefixBOnChange = exports.prefixBOnChange = function prefixBOnChange(_ref16) {
  var dispatch = _ref16.dispatch,
      newPrefixB = _ref16.newPrefixB;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PREFIX_B_ON_CHANGE,
    newPrefixB: newPrefixB
  });
};
/**
 * mobileNoBOnChange
 * @description change mobileNoB reducer state
 * @reducer clientForm.mobileNoB
 * @reducer clientForm.prefixB
 * @param {function} dispatch - redux dispatch function
 * @param {string} newMobileNoB - new reducer value
 * @param {string} prefix - mobile number prefix
 * */
var mobileNoBOnChange = exports.mobileNoBOnChange = function mobileNoBOnChange(_ref17) {
  var dispatch = _ref17.dispatch,
      newMobileNoB = _ref17.newMobileNoB,
      prefix = _ref17.prefix;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_B_ON_CHANGE,
    newMobileNoB: newMobileNoB,
    prefix: prefix
  });
};
/**
 * emailOnChange
 * @description change email reducer state
 * @reducer clientForm.email
 * @param {function} dispatch - redux dispatch function
 * @param {string} newEmail - new reducer value
 * */
var emailOnChange = exports.emailOnChange = function emailOnChange(_ref18) {
  var dispatch = _ref18.dispatch,
      newEmail = _ref18.newEmail;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMAIL_ON_CHANGE,
    newEmail: newEmail
  });
};
/**
 * validateEmail
 * @description email validation
 * @reducer clientForm.email
 * @reducer clientForm.isEmailError
 * @param {function} dispatch - redux dispatch function
 * @param {string} emailData - email value
 * */
var validateEmail = exports.validateEmail = function validateEmail(_ref19) {
  var dispatch = _ref19.dispatch,
      emailData = _ref19.emailData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMAIL,
    emailData: emailData
  });
};
/**
 * cityStateOnChange
 * @description change cityState reducer state
 * @reducer clientForm.cityState
 * @param {function} dispatch - redux dispatch function
 * @param {string} newCityState - new reducer value
 * */
var cityStateOnChange = exports.cityStateOnChange = function cityStateOnChange(_ref20) {
  var dispatch = _ref20.dispatch,
      newCityState = _ref20.newCityState;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CITY_STATE_ON_CHANGE,
    newCityState: newCityState
  });
};
/**
 * postalOnChange
 * @description change postal reducer state
 * @reducer clientForm.postal
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPostal - new reducer value
 * */
var postalOnChange = exports.postalOnChange = function postalOnChange(_ref21) {
  var dispatch = _ref21.dispatch,
      newPostal = _ref21.newPostal;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].POSTAL_ON_CHANGE,
    newPostal: newPostal
  });
};
/**
 * blockHouseOnChange
 * @description change blockHouse reducer state
 * @reducer clientForm.blockHouse
 * @param {function} dispatch - redux dispatch function
 * @param {string} newBlockHouse - new reducer value
 * */
var blockHouseOnChange = exports.blockHouseOnChange = function blockHouseOnChange(_ref22) {
  var dispatch = _ref22.dispatch,
      newBlockHouse = _ref22.newBlockHouse;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
    newBlockHouse: newBlockHouse
  });
};
/**
 * streetRoadOnChange
 * @description change streetRoad reducer state
 * @reducer clientForm.streetRoad
 * @param {function} dispatch - redux dispatch function
 * @param {string} newStreetRoad - new reducer value
 * */
var streetRoadOnChange = exports.streetRoadOnChange = function streetRoadOnChange(_ref23) {
  var dispatch = _ref23.dispatch,
      newStreetRoad = _ref23.newStreetRoad;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].STREET_ROAD_ON_CHANGE,
    newStreetRoad: newStreetRoad
  });
};
/**
 * unitOnChange
 * @description change unit reducer state
 * @reducer clientForm.unit
 * @param {function} dispatch - redux dispatch function
 * @param {string} newUnit - new reducer value
 * */
var unitOnChange = exports.unitOnChange = function unitOnChange(_ref24) {
  var dispatch = _ref24.dispatch,
      newUnit = _ref24.newUnit;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNIT_ON_CHANGE,
    newUnit: newUnit
  });
};
/**
 * buildingEstateOnChange
 * @description change buildingEstate reducer state
 * @reducer clientForm.buildingEstate
 * @param {function} dispatch - redux dispatch function
 * @param {string} newBuildingEstate - new reducer value
 * */
var buildingEstateOnChange = exports.buildingEstateOnChange = function buildingEstateOnChange(_ref25) {
  var dispatch = _ref25.dispatch,
      newBuildingEstate = _ref25.newBuildingEstate;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
    newBuildingEstate: newBuildingEstate
  });
};
/**
 * photoOnChange
 * @description change photo reducer state
 * @reducer clientForm.photo
 * @param {function} dispatch - redux dispatch function
 * @param {string} newPhoto - new reducer value
 * */
var photoOnChange = exports.photoOnChange = function photoOnChange(_ref26) {
  var dispatch = _ref26.dispatch,
      newPhoto = _ref26.newPhoto;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PHOTO_ON_CHANGE,
    newPhoto: newPhoto
  });
};

/**
 * typeOfPassOtherOnChange
 * @description change typeOfPassOther reducer state
 * @reducer clientForm.typeOfPassOther
 * @param {function} dispatch - redux dispatch function
 * @param {string} newtypeOthPassOther - new reducer value
 * */
var typeOthPassOtherOnChange = exports.typeOthPassOtherOnChange = function typeOthPassOtherOnChange(_ref27) {
  var dispatch = _ref27.dispatch,
      newtypeOfPassOther = _ref27.newtypeOfPassOther;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PHOTO_ON_CHANGE,
    newtypeOfPassOther: newtypeOfPassOther
  });
};