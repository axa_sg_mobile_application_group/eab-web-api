"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * quotation
 * @description quotation main state
 * @default {}
 * */
var quotation = function quotation() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.quotation);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * pdfData
 * @description pdfData main state
 * @default []
 * */
var pdfData = function pdfData() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return action.pdfData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * illustrateData
 * @description illustrateData main state
 * @default {}
 * */
var illustrateData = function illustrateData() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.illustrateData);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * planDetails
 * @description planDetails main state
 * @default {}
 * */
var planDetails = function planDetails() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return Object.assign({}, state, action.planDetails);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * canRequote
 * @description canRequote main state
 * @default false
 * */
var canRequote = function canRequote() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return action.canRequote;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canRequoteInvalid
 * @description canRequoteInvalid main state
 * @default false
 * */
var canRequoteInvalid = function canRequoteInvalid() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return action.canRequoteInvalid;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canClone
 * @description canClone main state
 * @default false
 * */
var canClone = function canClone() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return action.canClone;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * canEmail
 * @description canEmail main state
 * @default false
 * */
var canEmail = function canEmail() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL:
      return action.canEmail;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * proposalEmail
 * @description proposalEmail main state
 * @default {}
 */
var proposalEmail = function proposalEmail() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      emails = _ref.emails,
      tab = _ref.tab,
      option = _ref.option,
      reports = _ref.reports,
      report = _ref.report,
      isSelected = _ref.isSelected;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_EMAIL:
      {
        var mappedReport = reports.reduce(function (map, obj) {
          map[obj.id] = true;
          return map;
        }, {});
        return Object.assign({}, state, {
          emails: emails,
          reportOptions: {
            agent: mappedReport,
            client: mappedReport
          },
          tab: "agent"
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        reportOptions: Object.assign({}, state.reportOptions, _defineProperty({}, tab, Object.assign({}, state.reportOptions[tab], _defineProperty({}, report, !isSelected))))
      });

    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab: option
      });

    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  quotation: quotation,
  pdfData: pdfData,
  illustrateData: illustrateData,
  planDetails: planDetails,
  canRequote: canRequote,
  canRequoteInvalid: canRequoteInvalid,
  canClone: canClone,
  canEmail: canEmail,
  proposalEmail: proposalEmail
});