"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _message;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _NUMBER_FORMAT = require("../constants/NUMBER_FORMAT");

var _locales = require("../locales");

var _locales2 = _interopRequireDefault(_locales);

var _common = require("../utilities/common");

var _trigger = require("../utilities/trigger");

var _validation = require("../utilities/validation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// =============================================================================
// initial states
// =============================================================================
var configInitialState = {
  isCreate: false,
  isFamilyMember: false,
  isApplication: false,
  isProposerMissing: false,
  isFromProfile: false,
  isFromProduct: false,
  isFNA: false,
  isPDA: false,
  isTrustedIndividual: false
};
var errorInitialState = {
  hasError: false,
  message: (_message = {}, _defineProperty(_message, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message)
};
// =============================================================================
// reducers
// =============================================================================
/**
 * config
 * @description clientForm config. trigger function or change behavior
 * @requires configInitialState
 * @default configInitialState
 * */
var config = function config() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : configInitialState;
  var _ref = arguments[1];
  var type = _ref.type,
      configData = _ref.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_CLIENT_FORM:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG:
      return Object.assign({}, configInitialState, configData);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return configInitialState;
    default:
      return state;
  }
};
/**
 * relationship
 * @description client form dialog relationship field
 * @default ""
 * */
var relationship = function relationship() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref2 = arguments[1];
  var type = _ref2.type,
      newRelationship = _ref2.newRelationship;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      state = newRelationship;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE:
      state = newRelationship;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isRelationshipError
 * @requires errorInitialState
 * @reducer clientForm.isFamilyMember
 * @reducer clientForm.relationship
 * @reducer clientForm.gender
 * @reducer client.profile
 * @rule if reducer isFamilyMember, isPDA, isTrustedIndividual is true,
 *   relationship is mandatory field
 * @rule if isTrustedIndividual is false, reducer relationship should match
 *   reducer gender state
 * @rule if isTrustedIndividual is false, only allow one spouse
 * @default Object.assign({ cid: "" }, errorInitialState)
 * */
var isRelationshipError = function isRelationshipError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Object.assign({ cid: "" }, errorInitialState);
  var _ref3 = arguments[1];
  var type = _ref3.type,
      relationshipData = _ref3.relationshipData,
      genderData = _ref3.genderData,
      profileData = _ref3.profileData,
      configData = _ref3.configData,
      newCid = _ref3.newCid;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return Object.assign({}, { cid: newCid });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_RELATIONSHIP:
      {
        /* check mandatory */
        var checkMandatory = (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: configData.isFamilyMember || configData.isPDA || configData.isTrustedIndividual,
            disabled: false
          },
          value: relationshipData
        });

        if (checkMandatory.hasError) {
          return Object.assign({
            cid: newCid || state.cid
          }, checkMandatory);
        }

        /* check gender rule */
        if (!configData.isTrustedIndividual) {
          var maleRule = ["GFA", "SON", "FAT", "BRO", "GSO"].indexOf(relationshipData) > -1 && genderData === "F";
          var femaleRule = ["GMO", "MOT", "DAU", "GDA", "SIS"].indexOf(relationshipData) > -1 && genderData === "M";

          if (maleRule || femaleRule || relationshipData === "SPO" && genderData === profileData.gender) {
            var _message2;

            return {
              hasError: true,
              message: (_message2 = {}, _defineProperty(_message2, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.601"]), _defineProperty(_message2, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.601"]), _message2),
              cid: newCid || state.cid
            };
          }
        }

        /* check spouse rule */
        if (!configData.isTrustedIndividual) {
          var isSpouseInvalid = relationshipData === "SPO" && profileData.marital !== "M";
          if (isSpouseInvalid) {
            var _message3;

            return {
              hasError: true,
              message: (_message3 = {}, _defineProperty(_message3, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.607"]), _defineProperty(_message3, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.607"]), _message3),
              cid: newCid || state.cid
            };
          }

          var spouseRule = function spouseRule(dependant) {
            return dependant.relationship === "SPO" && relationshipData === "SPO" && (newCid ? newCid !== dependant.cid : state.cid !== dependant.cid) && !configData.isAPP;
          };

          if (profileData.dependants && !configData.isPDA) {
            profileData.dependants.forEach(function (dependant) {
              isSpouseInvalid = isSpouseInvalid || spouseRule(dependant);
            });
          }

          if (isSpouseInvalid) {
            var _message4;

            return {
              hasError: true,
              message: (_message4 = {}, _defineProperty(_message4, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.602"]), _defineProperty(_message4, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.602"]), _message4),
              cid: newCid || state.cid
            };
          }
        }

        /* relationship is valid */
        return errorInitialState;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return Object.assign({ cid: "" }, errorInitialState);
    default:
      return state;
  }
};
/**
 * relationshipOther
 * @description client form dialog please specify field
 * @reducer clientForm.relationship
 * @rule if relationship !== "OTH", clean
 * @default ""
 * */
var relationshipOther = function relationshipOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newRelationshipOther = _ref4.newRelationshipOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      state = newRelationshipOther;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE:
      state = newRelationshipOther;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isRelationshipOtherError
 * @reducer clientForm.relationship
 * @reducer clientForm.relationshipOther
 * @rule if reducer relationship is OTH(other), please specify field is
 *   mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isRelationshipOtherError = function isRelationshipOtherError() {
  var _message5, _message6;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message5 = {}, _defineProperty(_message5, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message5, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message5)
  };
  var _ref5 = arguments[1];
  var type = _ref5.type,
      relationshipOtherData = _ref5.relationshipOtherData,
      relationshipData = _ref5.relationshipData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_RELATIONSHIP_OTHER:
      {
        var mandatory = relationshipData === "OTH";

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: relationshipOtherData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message6 = {}, _defineProperty(_message6, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message6, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message6)
      };
    default:
      return state;
  }
};
/**
 * givenName
 * @description client form dialog given name field
 * @rule up to 30 characters
 * @default ""
 * */
var givenName = function givenName() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref6 = arguments[1];
  var type = _ref6.type,
      newGivenName = _ref6.newGivenName;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newGivenName;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE:
      state = newGivenName.substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isGivenNameError
 * @reducer clientForm.givenName
 * @rule given name is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isGivenNameError = function isGivenNameError() {
  var _message7, _message8;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message7 = {}, _defineProperty(_message7, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message7, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message7)
  };
  var _ref7 = arguments[1];
  var type = _ref7.type,
      givenNameData = _ref7.givenNameData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_GIVEN_NAME:
      return (0, _validation.validateMandatory)({
        field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
        value: givenNameData
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message8 = {}, _defineProperty(_message8, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message8, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message8)
      };
    default:
      return state;
  }
};
/**
 * surname
 * @description client form dialog surname name field
 * @rule up to 30 characters
 * @default ""
 * */
var surname = function surname() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref8 = arguments[1];
  var type = _ref8.type,
      newSurname = _ref8.newSurname;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newSurname;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].SURNAME_ON_CHANGE:
      state = newSurname.substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * otherName
 * @description client form dialog other name field
 * @rule up to 30 characters
 * @default ""
 * */
var otherName = function otherName() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref9 = arguments[1];
  var type = _ref9.type,
      newOtherName = _ref9.newOtherName;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newOtherName;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OTHER_NAME_ON_CHANGE:
      state = newOtherName.substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * hanYuPinYinName
 * @description client form dialog hanyu pinyin name field
 * @rule up to 30 characters
 * @default ""
 * */
var hanYuPinYinName = function hanYuPinYinName() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref10 = arguments[1];
  var type = _ref10.type,
      newHanYuPinYinName = _ref10.newHanYuPinYinName;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newHanYuPinYinName;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].HAN_YU_PIN_YIN_ON_CHANGE:
      state = newHanYuPinYinName.substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * name
 * @description client form dialog name field
 * @rule up to 30 characters
 * @rule automatically populate by new name, new first name and name order
 * @default ""
 * */
var name = function name() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref11 = arguments[1];
  var type = _ref11.type,
      newName = _ref11.newName;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newName;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_ON_CHANGE:
      state = newName.trim().substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isNameError
 * @requires errorInitialState
 * @reducer clientForm.givenName
 * @rule mandatory field
 * @rule should fill given name first
 * @default errorInitialState
 * */
var isNameError = function isNameError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : errorInitialState;
  var _ref12 = arguments[1];
  var type = _ref12.type,
      nameData = _ref12.nameData,
      givenNameData = _ref12.givenNameData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_NAME:
      {
        var error = errorInitialState;

        error = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: nameData
        });

        if (!error.hasError && nameData !== "" && !givenNameData) {
          var _message9;

          error = {
            hasError: true,
            message: (_message9 = {}, _defineProperty(_message9, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.603"]), _defineProperty(_message9, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.603"]), _message9)
          };
        }

        return error;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * nameOrder
 * @description client form dialog name order field
 * @default "L"
 * */
var nameOrder = function nameOrder() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "L";
  var _ref13 = arguments[1];
  var type = _ref13.type,
      newNameOrder = _ref13.newNameOrder;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newNameOrder;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_ORDER_ON_CHANGE:
      return newNameOrder;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "L";
    default:
      return state;
  }
};
/**
 * title
 * @description client form title order field
 * @default ""
 * */
var title = function title() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref14 = arguments[1];
  var type = _ref14.type,
      newTitle = _ref14.newTitle;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newTitle;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].TITLE_ON_CHANGE:
      return newTitle;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isTitleError
 * @description client form dialog title field validation status
 * @reducer clientForm.title
 * @reducer clientForm.gender
 * @rule if reducer title is Mr and gender is filled, reducer gender must be M.
 *   if reducer title is Ms, Mrs or Mdm and gender is filled, reducer gender
 *   must be M
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isTitleError = function isTitleError() {
  var _message10, _message13;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message10 = {}, _defineProperty(_message10, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message10, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message10)
  };
  var _ref15 = arguments[1];
  var type = _ref15.type,
      titleData = _ref15.titleData,
      genderData = _ref15.genderData,
      isFNA = _ref15.isFNA,
      isTrustedIndividual = _ref15.isTrustedIndividual;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TITLE:
      {
        var _message12;

        var ruleA = titleData === "Mr" && genderData !== "M" && genderData !== "";
        var ruleB = (titleData === "Ms" || titleData === "Mrs" || titleData === "Mdm") && genderData !== "F" && genderData !== "";

        if ((ruleA || ruleB) && !isTrustedIndividual) {
          var _message11;

          state = {
            hasError: true,
            message: (_message11 = {}, _defineProperty(_message11, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.601"]), _defineProperty(_message11, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.601"]), _message11)
          };
          return state;
        }

        var mandatory = isFNA;
        if (mandatory) {
          return (0, _validation.validateMandatory)({
            field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
            value: titleData
          });
        }

        return {
          hasError: false,
          message: (_message12 = {}, _defineProperty(_message12, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message12, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message12)
        };
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message13 = {}, _defineProperty(_message13, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message13, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message13)
      };
      return state;
    default:
      return state;
  }
};
/**
 * gender
 * @description client form gender field
 * @reducer clientForm.relationship
 * @reducer client.profile
 * @rule if relationship is one of GFA(grand father), SON(son), FAT(father),
 *   BRO(brother) or GSO(grand son), gender is M(male)
 * @rule if relationship is one of GMO(grand mother), MOT(mother),
 *   DAU(daughter), GDA(grand daughter) or SIS(sister), gender is F(female)
 * @rule if relationship is SPO(Spouse) and client.profile.gender is F(female),
 *   gender is M(male)
 * @rule if relationship is SPO(Spouse) and client.profile.gender is M(male),
 *   gender is F(female)
 * @default ""
 * */
var gender = function gender() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref16 = arguments[1];
  var type = _ref16.type,
      newGender = _ref16.newGender;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newGender;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE:
      return newGender;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isGenderError
 * @description client form dialog gender field validation status
 * @reducer clientForm.gender
 * @rule if form is for proposer or family member, gender is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isGenderError = function isGenderError() {
  var _message14, _message15;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message14 = {}, _defineProperty(_message14, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message14, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message14)
  };
  var _ref17 = arguments[1];
  var type = _ref17.type,
      genderData = _ref17.genderData,
      configData = _ref17.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_GENDER:
      {
        var mandatory = configData.isCreate && !configData.isTrustedIndividual || configData.isFamilyMember;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: genderData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message15 = {}, _defineProperty(_message15, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message15, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message15)
      };
      return state;
    default:
      return state;
  }
};
/**
 * birthday
 * @description client form gender field
 * @default ""
 * */
var birthday = function birthday() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref18 = arguments[1];
  var type = _ref18.type,
      newBirthday = _ref18.newBirthday;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newBirthday;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE:
      return newBirthday;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isBirthdayError
 * */
var isBirthdayError = function isBirthdayError() {
  var _message16, _message17;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message16 = {}, _defineProperty(_message16, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message16, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message16)
  };
  var _ref19 = arguments[1];
  var type = _ref19.type,
      birthdayData = _ref19.birthdayData,
      isProposerMissingData = _ref19.isProposerMissingData,
      isFamilyMemberData = _ref19.isFamilyMemberData,
      isFNA = _ref19.isFNA,
      isPDA = _ref19.isPDA,
      isAPP = _ref19.isAPP;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_BIRTHDAY:
      {
        var mandatory = isProposerMissingData || isFamilyMemberData || isFNA || isPDA || isAPP;
        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: birthdayData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message17 = {}, _defineProperty(_message17, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message17, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message17)
      };
      return state;
    default:
      return state;
  }
};
/**
 * nationality
 * @description client form nationality field
 * @default ""
 * */
var nationality = function nationality() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref20 = arguments[1];
  var type = _ref20.type,
      newNationality = _ref20.newNationality;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newNationality;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE:
      return newNationality;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * singaporePRStatus
 * @description client form singapore PR status field
 * @reducer clientForm.nationality
 * @rule if nationality have value and === "N1" or "N2", singaporePRStatus clean
 * @default ""
 * */
var singaporePRStatus = function singaporePRStatus() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref21 = arguments[1];
  var type = _ref21.type,
      newSingaporePRStatus = _ref21.newSingaporePRStatus;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newSingaporePRStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE:
      return newSingaporePRStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isSingaporePRStatusError
 * @description client form dialog singaporePRStatus field validation status
 * @reducer clientForm.nationality
 * @reducer clientForm.singaporePRStatus
 * @rule if reducer nationality is not N1(Singaporean) or N2(unknown),
 *   singaporePRStatus is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isSingaporePRStatusError = function isSingaporePRStatusError() {
  var _message18, _message19;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message18 = {}, _defineProperty(_message18, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message18, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message18)
  };
  var _ref22 = arguments[1];
  var type = _ref22.type,
      singaporePRStatusData = _ref22.singaporePRStatusData,
      nationalityData = _ref22.nationalityData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_SINGAPORE_PR_STATUS:
      {
        var mandatory = !!nationalityData && nationalityData !== "N1" && nationalityData !== "N2";

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: singaporePRStatusData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message19 = {}, _defineProperty(_message19, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message19, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message19)
      };
      return state;
    default:
      return state;
  }
};
/**
 * countryOfResidence
 * @description client form country of residence field
 * @default ""
 * */
var countryOfResidence = function countryOfResidence() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "R2";
  var _ref23 = arguments[1];
  var type = _ref23.type,
      newCountryOfResidence = _ref23.newCountryOfResidence;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newCountryOfResidence;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE:
      return newCountryOfResidence;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "R2";
    default:
      return state;
  }
};
/**
 * cityOfResidence
 * @description client form city of residence field
 * @reducer clientForm.countryOfResidence
 * @rule if reducer countryOfResidence value do not match some value,
 *   cityOfResidence will be set to ""
 * @default ""
 * */
var cityOfResidence = function cityOfResidence() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref24 = arguments[1];
  var type = _ref24.type,
      newCityOfResidence = _ref24.newCityOfResidence;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newCityOfResidence;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE:
      return newCityOfResidence;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "";
    default:
      return state;
  }
};
/**
 * isCityOfResidenceError
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.countryOfResidence
 * @reducer optionsMap
 * @rule if reducer countryOfResidence value match some value, cityOfResidence
 *   is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isCityOfResidenceError = function isCityOfResidenceError() {
  var _message20, _message21;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message20 = {}, _defineProperty(_message20, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message20, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message20)
  };
  var _ref25 = arguments[1];
  var type = _ref25.type,
      cityOfResidenceData = _ref25.cityOfResidenceData,
      countryOfResidenceData = _ref25.countryOfResidenceData,
      optionsMapData = _ref25.optionsMapData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_CITY_OF_RESIDENCE:
      {
        var mandatory = (0, _trigger.optionCondition)({
          value: countryOfResidenceData,
          options: "city",
          optionsMap: optionsMapData,
          showIfNoValue: false
        });

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: cityOfResidenceData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message21 = {}, _defineProperty(_message21, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message21, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message21)
      };
      return state;
    default:
      return state;
  }
};
/**
 * otherCityOfResidence
 * @reducer clientForm.cityOfResidence
 * @rule if cityOfResidence do not match some value, clean
 * @default ""
 * */
var otherCityOfResidence = function otherCityOfResidence() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref26 = arguments[1];
  var type = _ref26.type,
      newOtherCityOfResidence = _ref26.newOtherCityOfResidence;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newOtherCityOfResidence;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE:
      state = newOtherCityOfResidence.substring(0, 50);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isOtherCityOfResidenceError
 * @reducer clientForm.isApplication
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.otherCityOfResidence
 * @reducer optionsMap
 * @rule if isApplication is true and cityOfResidence is other option,
 *   otherCityOfResidence is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isOtherCityOfResidenceError = function isOtherCityOfResidenceError() {
  var _message22, _message23;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message22 = {}, _defineProperty(_message22, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message22, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message22)
  };
  var _ref27 = arguments[1];
  var type = _ref27.type,
      cityOfResidenceData = _ref27.cityOfResidenceData,
      otherCityOfResidenceData = _ref27.otherCityOfResidenceData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_OTHER_CITY_OF_RESIDENCE:
      {
        var otherCityOfResidenceList = ["T110", "T120", "T205", "T207", "T208", "T209", "T224"];
        var rule = _.includes(otherCityOfResidenceList, cityOfResidenceData);

        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: rule,
            disabled: false
          },
          value: otherCityOfResidenceData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message23 = {}, _defineProperty(_message23, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message23, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message23)
      };
      return state;
    default:
      return state;
  }
};
/**
 * IDDocumentType
 * @description client form dialog ID document type field
 * @reducer clientForm.nationality
 * @reducer clientForm.prStatus
 * @rule if reducer nationality is N1(Singaporean) or N2. Or reducer
 *   singaporePRStatus is Y, value will be lock to nric
 * @default ""
 * */
var IDDocumentType = function IDDocumentType() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref28 = arguments[1];
  var type = _ref28.type,
      newIDDocType = _ref28.newIDDocType,
      nationalityData = _ref28.nationalityData,
      singaporePRStatusData = _ref28.singaporePRStatusData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newIDDocType;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE:
      state = nationalityData === "N1" || nationalityData === "N2" || singaporePRStatusData === "Y" ? "nric" : newIDDocType;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDDocumentTypeError
 * @requires errorInitialState
 * @reducer clientForm.IDDocumentType
 * @reducer clientForm.config
 * @rule if config.isFNA or config.isTrustedIndividual is true, IDDocumentType
 *   is mandatory
 * @default errorInitialState
 * */
var isIDDocumentTypeError = function isIDDocumentTypeError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : errorInitialState;
  var _ref29 = arguments[1];
  var type = _ref29.type,
      IDDocumentTypeData = _ref29.IDDocumentTypeData,
      configData = _ref29.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE:
      {
        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: configData.isFNA || configData.isTrustedIndividual || configData.isPDA || configData.isAPP,
            disabled: false
          },
          value: IDDocumentTypeData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * IDDocumentTypeOther
 * @reducer clientForm.IDDocumentType
 * @rule up to 25 characters
 * @rule if IDDocumentType !== "other", clean
 * @default ""
 * */
var IDDocumentTypeOther = function IDDocumentTypeOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref30 = arguments[1];
  var type = _ref30.type,
      newIDDocTypeOther = _ref30.newIDDocTypeOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newIDDocTypeOther;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE:
      state = newIDDocTypeOther.substring(0, 25);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDDocumentTypeOtherError
 * @reducer clientForm.IDDocumentType
 * @reducer clientForm.IDDocumentTypeOther
 * @rule if IDDocumentType is other(other), IDDocumentTypeOther is mandatory
 *   field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
       [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isIDDocumentTypeOtherError = function isIDDocumentTypeOtherError() {
  var _message24, _message25;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message24 = {}, _defineProperty(_message24, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message24, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message24)
  };
  var _ref31 = arguments[1];
  var type = _ref31.type,
      IDDocumentTypeOtherData = _ref31.IDDocumentTypeOtherData,
      IDDocumentTypeData = _ref31.IDDocumentTypeData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE_OTHER:
      {
        var mandatory = IDDocumentTypeData === "other";

        state = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: IDDocumentTypeOtherData
        });

        return state;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message25 = {}, _defineProperty(_message25, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message25, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message25)
      };
      return state;
    default:
      return state;
  }
};
/**
 * ID
 * @description client form dialog ID field
 * @rule up to 20 characters
 * @rule must be uppercase
 * @default ""
 * */
var ID = function ID() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref32 = arguments[1];
  var type = _ref32.type,
      newID = _ref32.newID;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newID;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_ON_CHANGE:
      state = newID.substring(0, 20).toUpperCase();
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIDError
 * @description client form dialog ID field validation status
 * @requires errorInitialState
 * @reducer clientForm.config
 * @reducer clientForm.ID
 * @reducer clientForm.IDType
 * @rule if reducer ID or reducer IDType update, update value by result of the
 *   ID format validation with ID type
 * @rule if config.isFNA or config.isTrustedIndividual is true, ID is mandatory
 * @default errorInitialState
 * */
var isIDError = function isIDError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : errorInitialState;
  var _ref33 = arguments[1];
  var type = _ref33.type,
      IDData = _ref33.IDData,
      IDDocumentTypeData = _ref33.IDDocumentTypeData,
      configData = _ref33.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID:
      {
        var error = errorInitialState;

        if (configData.isFNA || configData.isTrustedIndividual || configData.isAPP) {
          error = (0, _validation.validateMandatory)({
            field: {
              type: _FIELD_TYPES.TEXT_FIELD,
              mandatory: true,
              disabled: false
            },
            value: IDData
          });
        }

        if (!error.hasError && IDData !== "") {
          error = (0, _validation.validateID)({
            ID: IDData,
            IDDocumentType: IDDocumentTypeData
          });
        }
        return error;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * smokingStatus
 * @description client form dialog smoking status field
 * @default ""
 * */
var smokingStatus = function smokingStatus() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref34 = arguments[1];
  var type = _ref34.type,
      newSmokingStatus = _ref34.newSmokingStatus;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newSmokingStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].SMOKING_STATUS_ON_CHANGE:
      state = newSmokingStatus;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isSmokingError
 * */
var isSmokingError = function isSmokingError() {
  var _message26, _message27;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message26 = {}, _defineProperty(_message26, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message26, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message26)
  };
  var _ref35 = arguments[1];
  var type = _ref35.type,
      smokingStatusData = _ref35.smokingStatusData,
      isProposerMissingData = _ref35.isProposerMissingData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_SMOKING_STATUS:
      {
        var mandatory = isProposerMissingData;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: smokingStatusData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message27 = {}, _defineProperty(_message27, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message27, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message27)
      };
      return state;
    default:
      return state;
  }
};
/**
 * maritalStatus
 * @description client form dialog marital status field
 * @rule if reducer relationship is "SPO"(spouse), value will be lock to "M"
 * @default ""
 * */
var maritalStatus = function maritalStatus() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref36 = arguments[1];
  var type = _ref36.type,
      newMaritalStatus = _ref36.newMaritalStatus,
      relationshipData = _ref36.relationshipData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newMaritalStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE:
      return relationshipData === "SPO" ? "M" : newMaritalStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isMaritalStatusError
 * */
var isMaritalStatusError = function isMaritalStatusError() {
  var _message28, _message29;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message28 = {}, _defineProperty(_message28, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message28, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message28)
  };
  var _ref37 = arguments[1];
  var type = _ref37.type,
      maritalData = _ref37.maritalData,
      isFNA = _ref37.isFNA,
      isAPP = _ref37.isAPP;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_MARITAL_STATUS:
      {
        var mandatory = isFNA || isAPP;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: maritalData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message29 = {}, _defineProperty(_message29, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message29, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message29)
      };
      return state;
    default:
      return state;
  }
};

/**
 * education
 * @default ""
 * */
var education = function education() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref38 = arguments[1];
  var type = _ref38.type,
      newEducation = _ref38.newEducation;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newEducation;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EDUCATION_ON_CHANGE:
      state = newEducation;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isEducationError
 * */
var isEducationError = function isEducationError() {
  var _message30, _message31;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message30 = {}, _defineProperty(_message30, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message30, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message30)
  };
  var _ref39 = arguments[1];
  var type = _ref39.type,
      educationData = _ref39.educationData,
      isFNA = _ref39.isFNA;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EDUCATION:
      {
        var mandatory = isFNA;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: educationData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message31 = {}, _defineProperty(_message31, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message31, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message31)
      };
      return state;
    default:
      return state;
  }
};

/**
 * employStatus
 * @description client form dialog employ status field
 * @reducer clientForm.birthday
 * @reducer clientForm.occupation
 * @rule if occupation change to O674(Househusband), change employStatus to
 *   hh(Househusband)
 * @rule if occupation change to O675(Housewife), change employStatus to
 *   hw(Housewife)
 * @rule if occupation change to O1450(Unemployed), change employStatus to
 *   ue(Unemployed)
 * @rule if occupation change to O1132(Retiree / Pensioner), change
 *   employStatus to rt(Retired)
 * @rule if occupation change to O1321(Student (Age 18 and above)) or
 *   O1322(Student (Below age 18)) or age < 18, change employStatus to
 *   sd(Student)
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1321(Student (Age 18 and above)) or O1322(Student
 *   (Below age 18)) to out of these value, change employStatus to ""
 * @default ""
 * */
var employStatus = function employStatus() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref40 = arguments[1];
  var type = _ref40.type,
      newEmployStatus = _ref40.newEmployStatus;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newEmployStatus;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE:
      state = newEmployStatus;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmployStatusError
 * @description client form dialog ID field validation status
 * @reducer clientForm.employStatus
 * @reducer clientForm.gender
 * @rule if reducer gender is F(female), reducer employStatus can not be
 *   hh(house husband)
 * @rule if reducer gender is M(male), reducer employStatus can not be hw(house
 *   wife)
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
       [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isEmployStatusError = function isEmployStatusError() {
  var _message32, _message36;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message32 = {}, _defineProperty(_message32, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message32, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message32)
  };
  var _ref41 = arguments[1];
  var type = _ref41.type,
      employStatusData = _ref41.employStatusData,
      genderData = _ref41.genderData,
      isFNA = _ref41.isFNA;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMPLOY_STATUS:
      {
        var _message33;

        var error = {
          hasError: false,
          message: (_message33 = {}, _defineProperty(_message33, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message33, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message33)
        };
        var mandatory = isFNA;
        if (mandatory) {
          error = (0, _validation.validateMandatory)({
            field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
            value: employStatusData
          });
        }

        if (!error.hasError && employStatusData === "hh" && genderData === "F") {
          var _message34;

          error = {
            hasError: true,
            message: (_message34 = {}, _defineProperty(_message34, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.605"]), _defineProperty(_message34, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.605"]), _message34)
          };
        }

        if (!error.hasError && employStatusData === "hw" && genderData === "M") {
          var _message35;

          error = {
            hasError: true,
            message: (_message35 = {}, _defineProperty(_message35, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.605"]), _defineProperty(_message35, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.605"]), _message35)
          };
        }

        return error;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message36 = {}, _defineProperty(_message36, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message36, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message36)
      };
      return state;
    default:
      return state;
  }
};
/**
 * employStatusOther
 * @default ""
 * */
var employStatusOther = function employStatusOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref42 = arguments[1];
  var type = _ref42.type,
      newEmployStatusOther = _ref42.newEmployStatusOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newEmployStatusOther;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE:
      state = newEmployStatusOther;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmployStatusOtherError
 * @reducer clientForm.employStatus
 * @reducer clientForm.employStatusOther
 * @rule other rule - if reducer employStatus is ot(other), other field is
 *   mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isEmployStatusOtherError = function isEmployStatusOtherError() {
  var _message37, _message38;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message37 = {}, _defineProperty(_message37, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message37, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message37)
  };
  var _ref43 = arguments[1];
  var type = _ref43.type,
      employStatusData = _ref43.employStatusData,
      employStatusOtherData = _ref43.employStatusOtherData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMPLOY_STATUS_OTHER:
      {
        // other rule
        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: employStatusData === "ot",
            disabled: false
          },
          value: employStatusOtherData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message38 = {}, _defineProperty(_message38, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message38, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message38)
      };
    default:
      return state;
  }
};
/**
 * industry
 * @reducer clientForm.occupation
 * @reducer optionsMap
 * @rule if occupation change, auto fill industry by occupation condition
 * @default ""
 * */
var industry = function industry() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref44 = arguments[1];
  var type = _ref44.type,
      newIndustry = _ref44.newIndustry;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newIndustry;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE:
      state = newIndustry;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isIndustryError
 * @reducer clientForm.industry
 * @reducer clientForm.employStatus
 * @rule if reducer employStatus is sd(student) and moduleTypes is APPLICATION,
 *   industry field is mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isIndustryError = function isIndustryError() {
  var _message39, _message40;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message39 = {}, _defineProperty(_message39, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message39, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message39)
  };
  var _ref45 = arguments[1];
  var type = _ref45.type,
      industryData = _ref45.industryData,
      employStatusData = _ref45.employStatusData,
      isApplicationData = _ref45.isApplicationData,
      isProposerMissingData = _ref45.isProposerMissingData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_INDUSTRY:
      {
        var mandatory = isApplicationData && employStatusData === "sd" || isProposerMissingData;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: industryData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message40 = {}, _defineProperty(_message40, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message40, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message40)
      };
    default:
      return state;
  }
};
/**
 * isIndustryError
 * */
var isNationalityError = function isNationalityError() {
  var _message41, _message42;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message41 = {}, _defineProperty(_message41, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message41, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message41)
  };
  var _ref46 = arguments[1];
  var type = _ref46.type,
      nationalityData = _ref46.nationalityData,
      isProposerMissingData = _ref46.isProposerMissingData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_NATIONALITY:
      {
        var mandatory = isProposerMissingData;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: nationalityData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message42 = {}, _defineProperty(_message42, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message42, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message42)
      };
    default:
      return state;
  }
};
/**
 * occupation
 * @reducer clientForm.age
 * @reducer clientForm.employStatus
 * @reducer clientForm.industry
 * @rule if employ status is hh(house husband), occupation is O674(house
 *   husband)
 * @rule if employ status is hw(house wife), occupation is O675(house wife)
 * @rule if employ status is ue(unemployed), occupation is O1450(unemployed)
 * @rule if employ status is ue(retired), occupation is
 *   O1132(retiree/pensioner)
 * @rule if age < 6, occupation is O246(child/juvenile/infant)
 * @rule if age < 18, occupation is O1322(student(below age 18))
 * @rule if age >= 18 and employStatus is sd(student), occupation is
 *   O1321(student(age 18 and above))
 * @rule if industry change(not trigger by saga), clean occupation state
 * @default ""
 * */
var occupation = function occupation() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref47 = arguments[1];
  var type = _ref47.type,
      newOccupation = _ref47.newOccupation;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newOccupation;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE:
      state = newOccupation;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isOccupationError
 * @reducer clientForm.occupation
 * @reducer clientForm.birthday
 * @rule when age < 18, 01321(student above 18) cannot be selected
 * @rule when age >= 18, Student (Below age 18) cannot be selected, when age <
 *   6, should be select Child / Juvenile / Infant, not student (Below age 18)
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isOccupationError = function isOccupationError() {
  var _message43, _message46;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message43 = {}, _defineProperty(_message43, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message43, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message43)
  };
  var _ref48 = arguments[1];
  var type = _ref48.type,
      occupationData = _ref48.occupationData,
      birthdayData = _ref48.birthdayData,
      isProposerMissingData = _ref48.isProposerMissingData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_OCCUPATION:
      {
        var _message45;

        var mandatory = isProposerMissingData;
        if (mandatory) {
          return (0, _validation.validateMandatory)({
            field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
            value: occupationData
          });
        }

        var age = (0, _common.calcAge)(birthdayData);

        var ruleA = occupationData === "O1321" && age && age < 18;
        var ruleB = occupationData === "O1322" && age && (age < 6 || age >= 18);

        if (ruleA || ruleB) {
          var _message44;

          return {
            hasError: true,
            message: (_message44 = {}, _defineProperty(_message44, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.713"]), _defineProperty(_message44, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.713"]), _message44)
          };
        }

        return {
          hasError: false,
          message: (_message45 = {}, _defineProperty(_message45, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message45, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message45)
        };
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message46 = {}, _defineProperty(_message46, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message46, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message46)
      };
    default:
      return state;
  }
};
/**
 * occupationOther
 * @reducer clientForm.occupation
 * @rule up to 50 characters
 * @rule if occupation !== O921, clean occupationOther data
 * @default ""
 * */
var occupationOther = function occupationOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref49 = arguments[1];
  var type = _ref49.type,
      newOccupationOther = _ref49.newOccupationOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newOccupationOther;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OCCUPATION_OTHER_ON_CHANGE:
      return newOccupationOther.substring(0, 50);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * language
 * @rule up to 30 characters
 * @default ""
 * */
var language = function language() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref50 = arguments[1];
  var type = _ref50.type,
      newLanguage = _ref50.newLanguage;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newLanguage;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE:
      state = newLanguage.substring(0, 30);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isLanguageOtherError
 * @requires errorInitialState
 * @reducer clientForm.language
 * @reducer clientForm.config
 * @rule if config.isFNA is true, language field is mandatory
 * @default errorInitialState
 * */
var isLanguageError = function isLanguageError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : errorInitialState;
  var _ref51 = arguments[1];
  var type = _ref51.type,
      languageData = _ref51.languageData,
      configData = _ref51.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_LANGUAGE:
      {
        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: configData.isFNA,
            disabled: false
          },
          value: languageData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};
/**
 * languageOther
 * @default ""
 * */
var languageOther = function languageOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref52 = arguments[1];
  var type = _ref52.type,
      newLanguageOther = _ref52.newLanguageOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newLanguageOther;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE:
      state = newLanguageOther;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isLanguageOtherError
 * @reducer clientForm.language
 * @reducer clientForm.languageOther
 * @rule if reducer language is other(other), languageOther field is mandatory
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isLanguageOtherError = function isLanguageOtherError() {
  var _message47, _message48;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message47 = {}, _defineProperty(_message47, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message47, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message47)
  };
  var _ref53 = arguments[1];
  var type = _ref53.type,
      languageData = _ref53.languageData,
      languageOtherData = _ref53.languageOtherData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_LANGUAGE_OTHER:
      {
        var mandatory = languageData.indexOf("other") !== -1;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: languageOtherData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message48 = {}, _defineProperty(_message48, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message48, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message48)
      };
    default:
      return state;
  }
};
/**
 * nameOfEmployBusinessSchool
 * @reducer clientForm.occupation
 * @reducer clientForm.employStatus
 * @reducer clientForm.birthday
 * @rule if employStatus is one of hh(house husband, hw(house wife),
 *   rt(retired), ue(unemployed), value will be lock to N/A
 * @rule if age < 6, value will be lock to N/A
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1132(Retiree / Pensioner), O1321(Student(Age 18 and
 *   above)) or O1322(Student (Below age 18)) to out of these value,
 *   change nameOfEmployBusinessSchool to ""
 * @default ""
 * */
var nameOfEmployBusinessSchool = function nameOfEmployBusinessSchool() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref54 = arguments[1];
  var type = _ref54.type,
      newNameOfEmployBusinessSchool = _ref54.newNameOfEmployBusinessSchool,
      employStatusData = _ref54.employStatusData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newNameOfEmployBusinessSchool;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE:
      {
        var ruleA = ["hh", "hw", "rt", "ue"].indexOf(employStatusData) > -1;
        if (ruleA) {
          state = "N/A";
        } else {
          state = newNameOfEmployBusinessSchool;
        }
        return state;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * countryOfEmployBusinessSchool
 * @reducer clientForm.occupation
 * @reducer clientForm.employStatus
 * @reducer clientForm.birthday
 * @rule if employStatus is one of hh(house husband, hw(house wife),
 *   rt(retired), ue(unemployed), value will be lock to na
 * @rule if age < 6, value will be lock to na
 * @rule if occupation change from O674(Househusband), O675(Housewife),
 *   O1450(Unemployed), O1132(Retiree / Pensioner), O1321(Student(Age 18 and
 *   above)) or O1322(Student (Below age 18)) to out of these value,
 *   change countryOfEmployBusinessSchool to R2(singapore)
 * @default ""
 * */
var countryOfEmployBusinessSchool = function countryOfEmployBusinessSchool() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "R2";
  var _ref55 = arguments[1];
  var type = _ref55.type,
      newCountryOfEmployBusinessSchool = _ref55.newCountryOfEmployBusinessSchool,
      employStatusData = _ref55.employStatusData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newCountryOfEmployBusinessSchool;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE:
      {
        var ruleA = ["hh", "hw", "rt", "ue"].indexOf(employStatusData) > -1;

        if (ruleA) {
          state = "na";
        } else {
          state = newCountryOfEmployBusinessSchool;
        }

        return state;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "R2";
      return state;
    default:
      return state;
  }
};
/**
 * typeOfPass
 * @description client form dialog type of pass field
 * @default ""
 * */
var typeOfPass = function typeOfPass() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref56 = arguments[1];
  var type = _ref56.type,
      newTypeOfPass = _ref56.newTypeOfPass;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newTypeOfPass;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE:
      state = newTypeOfPass;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isTypeOfPassError
 * @reducer clientForm.typeOfPass
 * @rule if typeOfPass is not o(other),typeOfPassOther is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isTypeOfPassError = function isTypeOfPassError() {
  var _message49, _message50;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message49 = {}, _defineProperty(_message49, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message49, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message49)
  };
  var _ref57 = arguments[1];
  var type = _ref57.type,
      typeOfPassData = _ref57.typeOfPassData,
      nationalityData = _ref57.nationalityData,
      singaporePRStatusData = _ref57.singaporePRStatusData,
      isAPP = _ref57.isAPP;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TYPE_OF_PASS:
      {
        var mandatory = isAPP;

        var rule = nationalityData && nationality !== "N1" && nationality !== "N2" && singaporePRStatusData && singaporePRStatusData !== "Y";

        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: mandatory && rule,
            disabled: false
          },
          value: typeOfPassData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message50 = {}, _defineProperty(_message50, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message50, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message50)
      };
    default:
      return state;
  }
};

/**
 * typeOfPassOther
 * @rule up to 24 characters
 * @default ""
 * */
var typeOfPassOther = function typeOfPassOther() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref58 = arguments[1];
  var type = _ref58.type,
      newTypeOfPassOther = _ref58.newTypeOfPassOther;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE:
      state = newTypeOfPassOther.substring(0, 24);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isTypeOfPassOtherError
 * @reducer clientForm.typeOfPass
 * @rule if typeOfPass is o(other),typeOfPassOther is mandatory field
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isTypeOfPassOtherError = function isTypeOfPassOtherError() {
  var _message51, _message52;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message51 = {}, _defineProperty(_message51, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message51, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message51)
  };
  var _ref59 = arguments[1];
  var type = _ref59.type,
      typeOfPassData = _ref59.typeOfPassData,
      typeOfPassOtherData = _ref59.typeOfPassOtherData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TYPE_OF_PASS_OTHER:
      {
        var mandatory = typeOfPassData === "o";

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: typeOfPassOtherData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message52 = {}, _defineProperty(_message52, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message52, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message52)
      };
    default:
      return state;
  }
};
/**
 * passExpiryDate
 * @description client form dialog pass expiry date field
 * @rule state should be timestamp format
 * @default NaN
 * */
var passExpiryDate = function passExpiryDate() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var _ref60 = arguments[1];
  var type = _ref60.type,
      newPassExpiryDate = _ref60.newPassExpiryDate;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PASS_EXPIRY_DATE_ON_CHANGE:
      return newPassExpiryDate;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = NaN;
      return state;
    default:
      return state;
  }
};
/**
 * income
 * @default NaN
 * */
var income = function income() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var _ref61 = arguments[1];
  var type = _ref61.type,
      newIncome = _ref61.newIncome;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newIncome;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INCOME_ON_CHANGE:
      {
        /* covert to number type */
        var numberValue = parseInt(newIncome, 10);

        /* check is it covert to a valid number */
        if (!Number.isNaN(numberValue) && numberValue >= 0) {
          return numberValue >= _NUMBER_FORMAT.MAX_CURRENCY_VALUE ? Number(String(numberValue).substring(0, String(_NUMBER_FORMAT.MAX_CURRENCY_VALUE).length)) : numberValue;
        }
        return NaN;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return NaN;
    default:
      return state;
  }
};

/**
 * isIncomeError
 * */
var isIncomeError = function isIncomeError() {
  var _message53, _message54;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message53 = {}, _defineProperty(_message53, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message53, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message53)
  };
  var _ref62 = arguments[1];
  var type = _ref62.type,
      incomeData = _ref62.incomeData,
      isFNA = _ref62.isFNA,
      isAPP = _ref62.isAPP;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_INCOME:
      {
        var mandatory = isFNA || isAPP;

        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: mandatory,
            disabled: false,
            numberAllowZero: true
          },
          value: incomeData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message54 = {}, _defineProperty(_message54, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message54, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message54)
      };
      return state;
    default:
      return state;
  }
};

/**
 * prefixA
 * @default "+65"
 * */
var prefixA = function prefixA() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "+65";
  var _ref63 = arguments[1];
  var type = _ref63.type,
      newPrefixA = _ref63.newPrefixA;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newPrefixA;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE:
      state = newPrefixA;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "+65";
      return state;
    default:
      return state;
  }
};

/**
 * isPrefixAError
 * */
var isPrefixAError = function isPrefixAError() {
  var _message55, _message56;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message55 = {}, _defineProperty(_message55, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message55, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message55)
  };
  var _ref64 = arguments[1];
  var type = _ref64.type,
      prefixAData = _ref64.prefixAData,
      isFNA = _ref64.isFNA;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_PREFIX_A:
      {
        var mandatory = isFNA;

        return (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: prefixAData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {
        hasError: false,
        message: (_message56 = {}, _defineProperty(_message56, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message56, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message56)
      };
      return state;
    default:
      return state;
  }
};

/**
 * mobileNoA
 * @reducer clientForm.prefixA
 * @rule add prefixA before the data
 * @rule The prefixA + mobileNoA number up to 15 characters, if not, delete the
 *   mobile number character
 * @default ""
 * */
var mobileNoA = function mobileNoA() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref65 = arguments[1];
  var type = _ref65.type,
      newMobileNoA = _ref65.newMobileNoA,
      prefixAData = _ref65.prefixAData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newMobileNoA;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_A_ON_CHANGE:
      {
        var maxLength = 15 - prefixAData.length;
        state = newMobileNoA.replace(/[^0-9]/g, "").substring(0, maxLength);
        return state;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * isMobileNoAError
 * @requires errorInitialState
 * @reducer clientForm.mobileNoA
 * @reducer clientForm.config
 * @rule if config.isFNA or config.isTrustedIndividual is true, mobileNoA is
 *   mandatory
 * @default errorInitialState
 * */
var isMobileNoAError = function isMobileNoAError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : errorInitialState;
  var _ref66 = arguments[1];
  var type = _ref66.type,
      mobileNoAData = _ref66.mobileNoAData,
      configData = _ref66.configData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_MOBILE_A_NO:
      {
        return (0, _validation.validateMandatory)({
          field: {
            type: _FIELD_TYPES.TEXT_FIELD,
            mandatory: configData.isFNA || configData.isTrustedIndividual,
            disabled: false
          },
          value: mobileNoAData
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return errorInitialState;
    default:
      return state;
  }
};

/**
 * prefixB
 * @default "+65"
 * */
var prefixB = function prefixB() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "+65";
  var _ref67 = arguments[1];
  var type = _ref67.type,
      newPrefixB = _ref67.newPrefixB;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newPrefixB;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PREFIX_B_ON_CHANGE:
      state = newPrefixB;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "+65";
      return state;
    default:
      return state;
  }
};
/**
 * mobileNoB
 * @reducer clientForm.prefixB
 * @rule add prefixB before the data
 * @rule The prefixB + mobileNoB number up to 15 characters, if not, delete the
 *   mobile number character
 * @default ""
 * */
var mobileNoB = function mobileNoB() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref68 = arguments[1];
  var type = _ref68.type,
      newMobileNoB = _ref68.newMobileNoB,
      prefixBData = _ref68.prefixBData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newMobileNoB;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_B_ON_CHANGE:
      {
        var maxLength = 15 - prefixBData.length;
        state = newMobileNoB.replace(/[^0-9]/g, "").substring(0, maxLength);
        return state;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * email
 * @rule up to 50 characters
 * @default ""
 * */
var email = function email() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref69 = arguments[1];
  var type = _ref69.type,
      newEmail = _ref69.newEmail;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newEmail;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMAIL_ON_CHANGE:
      state = newEmail.substring(0, 50);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * isEmailError
 * @rule if email !=="", reducer email should match email format
 * @default {
 *   hasError: false,
 *   message: {
 *     [LANGUAGE_TYPES.ENGLISH]: "",
 *     [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
 *   }
 * }
 * */
var isEmailError = function isEmailError() {
  var _message57, _message59;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    hasError: false,
    message: (_message57 = {}, _defineProperty(_message57, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message57, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message57)
  };
  var _ref70 = arguments[1];
  var type = _ref70.type,
      emailData = _ref70.emailData,
      isAPP = _ref70.isAPP,
      isProposerMissingData = _ref70.isProposerMissingData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMAIL:
      {
        var _message58;

        var mandatory = isAPP && isProposerMissingData;

        var checkMandatory = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: mandatory, disabled: false },
          value: emailData
        });

        if (mandatory && checkMandatory.hasError) {
          return checkMandatory;
        }

        return emailData !== "" ? (0, _validation.validateEmail)(emailData) : {
          hasError: false,
          message: (_message58 = {}, _defineProperty(_message58, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message58, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message58)
        };
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {
        hasError: false,
        message: (_message59 = {}, _defineProperty(_message59, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message59, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message59)
      };
    default:
      return state;
  }
};
/**
 * country
 * @description client form country field
 * @rule clientForm.countryOfResidence
 * @rule if countryOfResidence !== "", state will be lock to countryOfResidence
 *   state
 * @default ""
 * */
var country = function country() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "R2";
  var _ref71 = arguments[1];
  var type = _ref71.type,
      newCountry = _ref71.newCountry,
      countryOfResidenceData = _ref71.countryOfResidenceData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newCountry;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].COUNTRY_ON_CHANGE:
      return countryOfResidenceData || newCountry;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return "R2";
    default:
      return state;
  }
};
/**
 * cityState
 * @reducer clientForm.cityOfResidence
 * @reducer clientForm.otherCityOfResidence
 * @reducer if cityOfResidence is not other option, auto fill from
 *   cityOfResidence state
 * @reducer if otherCityOfResidence is not "", auto fill from
 *   otherCityOfResidence state
 * @default ""
 * */
var cityState = function cityState() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref72 = arguments[1];
  var type = _ref72.type,
      newCityState = _ref72.newCityState;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newCityState;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CITY_STATE_ON_CHANGE:
      return newCityState;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * postal
 * @rule up to 12 characters
 * @default ""
 * */
var postal = function postal() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref73 = arguments[1];
  var type = _ref73.type,
      newPostal = _ref73.newPostal;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newPostal;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].POSTAL_ON_CHANGE:
      state = newPostal.substring(0, 10);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * blockHouse
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 12 characters
 * @default ""
 * */
var blockHouse = function blockHouse() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref74 = arguments[1];
  var type = _ref74.type,
      newBlockHouse = _ref74.newBlockHouse;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newBlockHouse;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE:
      state = newBlockHouse.substring(0, 12);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * streetRoad
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 24 characters
 * @default ""
 * */
var streetRoad = function streetRoad() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref75 = arguments[1];
  var type = _ref75.type,
      newStreetRoad = _ref75.newStreetRoad;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newStreetRoad;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].STREET_ROAD_ON_CHANGE:
      state = newStreetRoad.substring(0, 24);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * unit
 * @rule up to 13 characters
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule this field have prefix "Unit"
 * @default ""
 * */
var unit = function unit() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref76 = arguments[1];
  var type = _ref76.type,
      newUnit = _ref76.newUnit,
      prefix = _ref76.prefix;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newUnit;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNIT_ON_CHANGE:
      prefix = _.isEmpty(prefix) ? "" : prefix + " ";
      return state === "" ? (prefix + newUnit).substring(0, 13) : newUnit.substring(0, 13);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};
/**
 * buildingEstate
 * @reducer clientForm.postal
 * @rule if postal matching to value linked to db, auto-fill this field.
 * @rule up to 40 characters
 * @default ""
 * */
var buildingEstate = function buildingEstate() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref77 = arguments[1];
  var type = _ref77.type,
      newBuildingEstate = _ref77.newBuildingEstate;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      return newBuildingEstate;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE:
      state = newBuildingEstate.substring(0, 40);
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = "";
      return state;
    default:
      return state;
  }
};

/**
 * photo
 * @description client form dialog photo/avatar
 * @default ""
 * */
var photo = function photo() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref78 = arguments[1];
  var type = _ref78.type,
      newPhoto = _ref78.newPhoto;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      state = newPhoto;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PHOTO_ON_CHANGE:
      state = newPhoto;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      state = {};
      return state;
    default:
      return state;
  }
};
/**
 * @description for some data will not be edit by clientForm, but contain in the
 * clientProfile data structure
 * @default {}
 * */
var profileBackUp = function profileBackUp() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref79 = arguments[1];
  var type = _ref79.type,
      newHaveSignDoc = _ref79.newHaveSignDoc,
      newBundle = _ref79.newBundle,
      newTrustedIndividuals = _ref79.newTrustedIndividuals,
      newCid = _ref79.newCid,
      newDependants = _ref79.newDependants;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT:
      {
        var object = {
          cid: newCid,
          dependants: newDependants
        };

        if (newBundle.length > 0) {
          object.bundle = newBundle;
        }

        if (newTrustedIndividuals) {
          object.trustedIndividuals = newTrustedIndividuals;
        }

        if (newHaveSignDoc) {
          object.haveSignDoc = newHaveSignDoc;
        }

        return object;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM:
      return {};
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  config: config,
  relationship: relationship,
  isRelationshipError: isRelationshipError,
  relationshipOther: relationshipOther,
  isRelationshipOtherError: isRelationshipOtherError,
  givenName: givenName,
  isGivenNameError: isGivenNameError,
  surname: surname,
  otherName: otherName,
  hanYuPinYinName: hanYuPinYinName,
  name: name,
  isNameError: isNameError,
  nameOrder: nameOrder,
  title: title,
  isTitleError: isTitleError,
  gender: gender,
  isGenderError: isGenderError,
  birthday: birthday,
  nationality: nationality,
  singaporePRStatus: singaporePRStatus,
  isSingaporePRStatusError: isSingaporePRStatusError,
  countryOfResidence: countryOfResidence,
  cityOfResidence: cityOfResidence,
  isCityOfResidenceError: isCityOfResidenceError,
  otherCityOfResidence: otherCityOfResidence,
  isOtherCityOfResidenceError: isOtherCityOfResidenceError,
  IDDocumentType: IDDocumentType,
  isIDDocumentTypeError: isIDDocumentTypeError,
  IDDocumentTypeOther: IDDocumentTypeOther,
  isIDDocumentTypeOtherError: isIDDocumentTypeOtherError,
  ID: ID,
  isIDError: isIDError,
  smokingStatus: smokingStatus,
  maritalStatus: maritalStatus,
  language: language,
  isLanguageError: isLanguageError,
  languageOther: languageOther,
  isLanguageOtherError: isLanguageOtherError,
  education: education,
  isEducationError: isEducationError,
  employStatus: employStatus,
  isEmployStatusError: isEmployStatusError,
  employStatusOther: employStatusOther,
  isEmployStatusOtherError: isEmployStatusOtherError,
  industry: industry,
  isIndustryError: isIndustryError,
  occupation: occupation,
  isOccupationError: isOccupationError,
  occupationOther: occupationOther,
  nameOfEmployBusinessSchool: nameOfEmployBusinessSchool,
  countryOfEmployBusinessSchool: countryOfEmployBusinessSchool,
  typeOfPass: typeOfPass,
  isTypeOfPassError: isTypeOfPassError,
  typeOfPassOther: typeOfPassOther,
  isTypeOfPassOtherError: isTypeOfPassOtherError,
  passExpiryDate: passExpiryDate,
  income: income,
  prefixA: prefixA,
  mobileNoA: mobileNoA,
  prefixB: prefixB,
  mobileNoB: mobileNoB,
  email: email,
  isEmailError: isEmailError,
  country: country,
  cityState: cityState,
  postal: postal,
  blockHouse: blockHouse,
  streetRoad: streetRoad,
  unit: unit,
  buildingEstate: buildingEstate,
  photo: photo,
  isBirthdayError: isBirthdayError,
  isSmokingError: isSmokingError,
  isNationalityError: isNationalityError,
  isMaritalStatusError: isMaritalStatusError,
  isIncomeError: isIncomeError,
  isMobileNoAError: isMobileNoAError,
  isPrefixAError: isPrefixAError,
  profileBackUp: profileBackUp
});