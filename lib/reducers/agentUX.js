"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * profile
 * @default {}
 * */
exports.default = function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      data = _ref.data;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.AGENTUX].UPDATE_UX_AGENT_DATA:
      return data;
    default:
      return state;
  }
};