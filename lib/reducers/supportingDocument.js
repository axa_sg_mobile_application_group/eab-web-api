"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * supportingDocumentData
 * @description store the SUPPORTING_DOCUMENT data retrieved from core-api and web/app UI
 * @default { chosenList: [], notChosenList: [] }
 * */
var supportingDocument = function supportingDocument() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      newSupportingDocument = _ref.newSupportingDocument;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA:
      return Object.assign({}, state, newSupportingDocument);
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

var pendingSubmitList = function pendingSubmitList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref2 = arguments[1];
  var type = _ref2.type,
      newPendingSubmitList = _ref2.newPendingSubmitList;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST:
      return newPendingSubmitList;
    default:
      return state;
  }
};

var viewedListState = {
  appPdf: false,
  fnaReport: false,
  proposal: false
};

/**
 * error
 * @description TODO dont know what is this, but nothing important so far
 * */
var viewedList = function viewedList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : viewedListState;
  var _ref3 = arguments[1];
  var type = _ref3.type,
      newViewedList = _ref3.newViewedList;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_VIEWED_LIST_DATA:
      return Object.assign({}, state, newViewedList);
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * error
 * @description store the error data retrieved from core-api and web/app UI
 * */
var error = function error() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newError = _ref4.newError;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].VALIDATE_SUPPORTING_DOCUMENT:
      return Object.assign({}, state, newError);
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  pendingSubmitList: pendingSubmitList,
  supportingDocument: supportingDocument,
  error: error,
  viewedList: viewedList
});