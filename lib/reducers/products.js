"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * currency
 * @description affect product list filter
 * @default ALL
 * */
var currency = function currency() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "ALL";
  var _ref = arguments[1];
  var type = _ref.type,
      currencyData = _ref.currencyData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_CURRENCY:
      return currencyData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UPDATE_PRODUCT_LIST:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return "ALL";
    default:
      return state;
  }
};

/**
 * insuredCid
 * @description product list target client. affect product list filter
 * @default ""
 * */
var insuredCid = function insuredCid() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var _ref2 = arguments[1];
  var type = _ref2.type,
      insuredCidData = _ref2.insuredCidData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_INSURED_CID:
      return insuredCidData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_PRODUCT_LIST:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return null;
    default:
      return state;
  }
};

/**
 * dependants
 * @description dependants list. Unlike client.profile.dependants, so can not
 *   reuse client.profile.dependants.
 * @default []
 * */
var dependants = function dependants() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref3 = arguments[1];
  var type = _ref3.type,
      dependantsData = _ref3.dependantsData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_DEPENDANTS:
      /* do not need to assign a new array, because the data is from server/core-api response */
      return dependantsData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNLINK_CLIENT:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * productList
 * @default []
 * */
var productList = function productList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref4 = arguments[1];
  var type = _ref4.type,
      productListData = _ref4.productListData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_PRODUCT_LIST:
      /* do not need to assign a new array, because the data is from server/core-api response */
      return productListData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNLINK_CLIENT:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * error message handle
 */
var errorMsg = function errorMsg() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref5 = arguments[1];
  var type = _ref5.type,
      errorMessage = _ref5.errorMessage;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_PRODUCT_LIST:
      return errorMessage;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNLINK_CLIENT:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  currency: currency,
  insuredCid: insuredCid,
  dependants: dependants,
  productList: productList,
  errorMsg: errorMsg
});