"use strict";

var _faker = require("faker");

var _faker2 = _interopRequireDefault(_faker);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _recommendation = require("./recommendation");

var _recommendation2 = _interopRequireDefault(_recommendation);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var randomRecommendationDataNormal = function randomRecommendationDataNormal() {
  var normalProductChoice1 = _faker2.default.random.arrayElement(["Y", "N"]);

  return {
    extra: {
      proposerAndLifeAssuredName: _faker2.default.name.firstName() + " / " + _faker2.default.name.lastName(),
      basicPlanName: _faker2.default.random.arrayElement(["AXA Band Aid (RP)", "SavvySaver (RP)"]),
      ridersName: [_faker2.default.random.arrayElement(["Rider 1", "Rider 2"])],
      recommendBenefitDefault: _faker2.default.random.words(5),
      recommendLimitDefault: _faker2.default.random.words(5),
      paymentMode: _faker2.default.random.arrayElement(["A", "S", "Q", "M", "L"]),
      isShield: false,
      lastUpdateDate: _faker2.default.date.past(0)
    },
    benefit: _faker2.default.random.words(5),
    limitation: _faker2.default.random.words(5),
    reason: _faker2.default.random.words(5),
    rop: {
      choiceQ1: normalProductChoice1,
      choiceQ1Sub1: normalProductChoice1 === "Y" ? _faker2.default.random.arrayElement(["Y", "N"]) : "",
      choiceQ1Sub2: normalProductChoice1 === "Y" ? _faker2.default.random.arrayElement(["Y", "N"]) : "",
      choiceQ1Sub3: _faker2.default.random.words(5),
      existCi: _faker2.default.random.number(9999999999),
      existLife: _faker2.default.random.number(9999999999),
      existPaAdb: _faker2.default.random.number(9999999999),
      existTotalPrem: _faker2.default.random.number(9999999999),
      existTpd: _faker2.default.random.number(9999999999),
      replaceCi: _faker2.default.random.number(9999999999),
      replaceLife: _faker2.default.random.number(9999999999),
      replacePaAdb: _faker2.default.random.number(9999999999),
      replaceTotalPrem: _faker2.default.random.number(9999999999),
      replaceTpd: _faker2.default.random.number(9999999999)
    },
    rop_shield: {
      ropBlock: {
        iCidRopAnswerMap: {},
        ropQ1sub3: "",
        ropQ2: "",
        ropQ3: "",
        shieldRopAnswer_0: "",
        shieldRopAnswer_1: "",
        shieldRopAnswer_2: "",
        shieldRopAnswer_3: "",
        shieldRopAnswer_4: "",
        shieldRopAnswer_5: ""
      }
    }
  };
};

var randomRecommendationDataShield = function randomRecommendationDataShield() {
  var shieldProductChoice0 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldProductChoice1 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldProductChoice2 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldProductChoice3 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldProductChoice4 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldProductChoice5 = _faker2.default.random.arrayElement(["Y", "N"]);
  var shieldHasRop = shieldProductChoice0 === "Y" || shieldProductChoice1 === "Y" || shieldProductChoice2 === "Y" || shieldProductChoice3 === "Y" || shieldProductChoice4 === "Y" || shieldProductChoice5 === "Y";

  return {
    extra: {
      proposerAndLifeAssuredName: "",
      basicPlanName: "AXA Shield Plan (RP)",
      ridersName: [],
      shieldInsuredPlans: [{
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }, {
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }, {
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }, {
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }, {
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }, {
        shieldInsured: _faker2.default.name.firstName() + ":",
        shieldPlan: "AXA Shield (Plan A), AXA Basic Care (Plan A), AXA General Care (Plan A), AXA Home Care"
      }],
      shieldInsuredAndPlanGroupList: [{
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }, {
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }, {
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }, {
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }, {
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }, {
        insuredNameList: [_faker2.default.name.firstName()],
        planCodeList: ["ASIMSA", "ASPA", "ASGC", "ASHCD"],
        planNameList: ["AXA Shield (Plan A)", "AXA Basic Care (Plan A)", "AXA General Care (Plan A)", "AXA Home Care"]
      }],
      recommendBenefitDefault: _faker2.default.random.words(5),
      recommendLimitDefault: _faker2.default.random.words(5),
      paymentMode: _faker2.default.random.arrayElement(["A", "S", "Q", "M"]),
      isShield: true,
      lastUpdateDate: _faker2.default.date.past(0)
    },
    benefit: _faker2.default.random.words(5),
    limitation: _faker2.default.random.words(5),
    reason: _faker2.default.random.words(5),
    rop: {
      choiceQ1: "",
      choiceQ1Sub1: "",
      choiceQ1Sub2: "",
      choiceQ1Sub3: "",
      existCi: 0,
      existLife: 0,
      existPaAdb: 0,
      existTotalPrem: 0,
      existTpd: 0,
      replaceCi: 0,
      replaceLife: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0,
      replaceTpd: 0
    },
    rop_shield: {
      ropBlock: {
        iCidRopAnswerMap: {
          "CP001001-00001": "shieldRopAnswer_0",
          "CP001001-00002": "shieldRopAnswer_1",
          "CP001001-00003": "shieldRopAnswer_2",
          "CP001001-00004": "shieldRopAnswer_3",
          "CP001001-00005": "shieldRopAnswer_4",
          "CP001001-00006": "shieldRopAnswer_5"
        },
        ropQ1sub3: _faker2.default.random.words(5),
        ropQ2: shieldHasRop === "Y" ? _faker2.default.random.arrayElement(["Y", "N"]) : "",
        ropQ3: shieldHasRop === "Y" ? _faker2.default.random.arrayElement(["Y", "N"]) : "",
        shieldRopAnswer_0: shieldProductChoice0,
        shieldRopAnswer_1: shieldProductChoice1,
        shieldRopAnswer_2: shieldProductChoice2,
        shieldRopAnswer_3: shieldProductChoice3,
        shieldRopAnswer_4: shieldProductChoice4,
        shieldRopAnswer_5: shieldProductChoice5
      }
    }
  };
};

var randomBudgetData = function randomBudgetData() {
  var budget = {
    spBudget: _faker2.default.random.number(9999999999),
    spTotalPremium: _faker2.default.random.number(9999999999),
    spCompare: 0,
    spCompareResult: "",
    rpBudget: _faker2.default.random.number(9999999999),
    rpTotalPremium: _faker2.default.random.number(9999999999),
    rpCompare: 0,
    rpCompareResult: "",
    cpfOaBudget: _faker2.default.random.number(9999999999),
    cpfOaTotalPremium: _faker2.default.random.number(9999999999),
    cpfOaCompare: 0,
    cpfOaCompareResult: "",
    cpfSaBudget: _faker2.default.random.number(9999999999),
    cpfSaTotalPremium: _faker2.default.random.number(9999999999),
    cpfSaCompare: 0,
    cpfSaCompareResult: "",
    srsBudget: _faker2.default.random.number(9999999999),
    srsTotalPremium: _faker2.default.random.number(9999999999),
    srsCompare: 0,
    srsCompareResult: "",
    cpfMsBudget: _faker2.default.random.number(9999999999),
    cpfMsTotalPremium: _faker2.default.random.number(9999999999),
    cpfMsCompare: 0,
    cpfMsCompareResult: "",
    budgetMoreChoice: _faker2.default.random.arrayElement(["Y", "N"]),
    budgetMoreReason: _faker2.default.random.words(300),
    budgetLessChoice: _faker2.default.random.arrayElement(["Y", "N"]),
    budgetLessReason: _faker2.default.random.words(300)
  };
  budget.spCompare = budget.spBudget - budget.spTotalPremium;
  budget.rpCompare = budget.rpBudget - budget.rpTotalPremium;
  budget.cpfOaCompare = budget.cpfOaBudget - budget.cpfOaTotalPremium;
  budget.cpfSaCompare = budget.cpfSaBudget - budget.cpfSaTotalPremium;
  budget.srsCompare = budget.srsBudget - budget.srsTotalPremium;
  budget.cpfMsCompare = budget.cpfMsBudget - budget.cpfMsTotalPremium;

  budget.spCompareResult = budget.spCompare > 9.99 ? "Underutilized" : budget.spCompare < -9.99 ? "Exceeded" : "Aligned";
  budget.rpCompareResult = budget.rpCompare > 9.99 ? "Underutilized" : budget.rpCompare < -9.99 ? "Exceeded" : "Aligned";
  budget.cpfOaCompareResult = budget.cpfOaCompare > 9.99 ? "Underutilized" : budget.cpfOaCompare < -9.99 ? "Exceeded" : "Aligned";
  budget.cpfSaCompareResult = budget.cpfSaCompare > 9.99 ? "Underutilized" : budget.cpfSaCompare < -9.99 ? "Exceeded" : "Aligned";
  budget.srsCompareResult = budget.srsCompare > 9.99 ? "Underutilized" : budget.srsCompare < -9.99 ? "Exceeded" : "Aligned";
  budget.cpfMsCompareResult = budget.cpfMsCompare > 9.99 ? "Underutilized" : budget.cpfMsCompare < -9.99 ? "Exceeded" : "Aligned";

  return budget;
};

var randomAcceptanceData = function randomAcceptanceData() {
  return {
    proposerAndLifeAssuredName: _faker2.default.name.firstName() + " / " + _faker2.default.name.lastName(),
    basicPlanName: _faker2.default.random.arrayElement(["AXA Band Aid (RP)", "SavvySaver (RP)"]),
    ridersName: [_faker2.default.random.arrayElement(["Rider 1", "Rider 2"])]
  };
};

var randomAcceptanceDataList = function randomAcceptanceDataList(count) {
  var list = [];
  for (var i = 0; i < count; i++) {
    list.push(randomAcceptanceData());
  }
  return list;
};

describe("config reducer", function () {
  it("should test the initial state", function () {
    expect((0, _recommendation2.default)(undefined, {})).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test CLEAN_CLIENT_DATA", function () {
    // TODO: use fake data instead of undefined
    expect((0, _recommendation2.default)(undefined, {
      type: _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test CLOSE_RECOMMENDATION", function () {
    expect((0, _recommendation2.default)(undefined, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test GET_RECOMMENDATION", function () {
    var chosenList = [_faker2.default.random.arrayElement(["QU001002-11111", "QU003004-22222", "QU005006-33333", "QU007008-44444"])];
    var notChosenList = [_faker2.default.random.arrayElement(["QU001002-55555", "QU003004-66666", "QU005006-77777", "QU007008-88888"])];

    var normalProduct1 = randomRecommendationDataNormal();
    var normalProduct2 = randomRecommendationDataNormal();
    var shieldProduct1 = randomRecommendationDataShield();
    var shieldProduct2 = randomRecommendationDataShield();
    var recommendation = {
      chosenList: chosenList,
      notChosenList: notChosenList
    };
    recommendation[chosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });
    recommendation[notChosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });

    var budget = randomBudgetData();
    var acceptance = randomAcceptanceDataList(chosenList.length);
    var component = {
      completedStep: -1,
      currentStep: 0,
      completedSection: { recommendation: {}, budget: false },
      selectedQuotId: "",
      showDetails: false,
      disabled: false
    };

    expect((0, _recommendation2.default)(undefined, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION,
      recommendation: recommendation,
      budget: budget,
      acceptance: acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: component
    })).toEqual({
      recommendation: recommendation,
      budget: budget,
      acceptance: acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: component
    });
  });

  it("should test UPDATE_RECOMMENDATION", function () {
    var chosenList = [_faker2.default.random.arrayElement(["QU001002-11111", "QU003004-22222", "QU005006-33333", "QU007008-44444"])];
    var notChosenList = [_faker2.default.random.arrayElement(["QU001002-55555", "QU003004-66666", "QU005006-77777", "QU007008-88888"])];

    var normalProduct1 = randomRecommendationDataNormal();
    var normalProduct2 = randomRecommendationDataNormal();
    var shieldProduct1 = randomRecommendationDataShield();
    var shieldProduct2 = randomRecommendationDataShield();
    var recommendation = {
      chosenList: chosenList,
      notChosenList: notChosenList
    };
    recommendation[chosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });
    recommendation[notChosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });

    var budget = randomBudgetData();
    var acceptance = randomAcceptanceDataList(chosenList.length);

    expect((0, _recommendation2.default)({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION,
      recommendation: recommendation,
      budget: budget,
      acceptance: acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    })).toEqual({
      recommendation: recommendation,
      budget: budget,
      acceptance: acceptance,
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_DATA", function () {
    var chosenList = [_faker2.default.random.arrayElement(["QU001002-11111", "QU003004-22222", "QU005006-33333", "QU007008-44444"])];
    var notChosenList = [_faker2.default.random.arrayElement(["QU001002-55555", "QU003004-66666", "QU005006-77777", "QU007008-88888"])];

    var normalProduct1 = randomRecommendationDataNormal();
    var normalProduct2 = randomRecommendationDataNormal();
    var shieldProduct1 = randomRecommendationDataShield();
    var shieldProduct2 = randomRecommendationDataShield();
    var recommendation = {
      chosenList: chosenList,
      notChosenList: notChosenList
    };
    recommendation[chosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });
    recommendation[notChosenList[0]] = _faker2.default.random.objectElement({ normalProduct1: normalProduct1, normalProduct2: normalProduct2, shieldProduct1: shieldProduct1, shieldProduct2: shieldProduct2 });

    expect((0, _recommendation2.default)({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DATA,
      recommendation: recommendation
    })).toEqual({
      recommendation: recommendation,
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_SELECTED_QUOT", function () {
    var newSelectedQuotId = _faker2.default.random.arrayElement(["QU003004-22222", "QU005006-33333", "QU007008-44444"]);

    expect((0, _recommendation2.default)(undefined, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT,
      newSelectedQuotId: newSelectedQuotId
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: newSelectedQuotId,
        showDetails: false,
        disabled: false
      }
    });
  });

  it("should test UPDATE_RECOMMENDATION_COMPONENT", function () {
    var currentStep = _faker2.default.random.number(2);
    var completedStep = currentStep - _faker2.default.random.number(1);
    var component = {
      completedStep: completedStep,
      currentStep: currentStep,
      completedSection: {
        recommendation: {
          "QU003004-22222": _faker2.default.random.boolean(),
          "QU005006-33333": _faker2.default.random.boolean(),
          "QU007008-44444": _faker2.default.random.boolean()
        },
        budget: _faker2.default.random.boolean()
      },
      selectedQuotId: _faker2.default.random.arrayElement(["QU003004-22222", "QU005006-33333", "QU007008-44444"]),
      showDetails: _faker2.default.random.boolean(),
      disabled: _faker2.default.random.boolean()
    };

    expect((0, _recommendation2.default)({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    }, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
      component: component
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: component
    });
  });

  it("should test UPDATE_RECOMMENDATION_DETAILS", function () {
    var currentStep = _faker2.default.random.number(2);
    var completedStep = currentStep - _faker2.default.random.number(1);
    var component = {
      completedStep: completedStep,
      currentStep: currentStep,
      completedSection: {
        recommendation: {
          "QU003004-22222": _faker2.default.random.boolean(),
          "QU005006-33333": _faker2.default.random.boolean(),
          "QU007008-44444": _faker2.default.random.boolean()
        },
        budget: _faker2.default.random.boolean()
      },
      selectedQuotId: _faker2.default.random.arrayElement(["QU003004-22222", "QU005006-33333", "QU007008-44444"]),
      showDetails: _faker2.default.random.boolean(),
      disabled: _faker2.default.random.boolean()
    };

    var expectedComponent = _.cloneDeep(component);
    expectedComponent.showDetails = !component.showDetails;

    expect((0, _recommendation2.default)({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: component
    }, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS,
      newShowDetails: !component.showDetails
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: expectedComponent
    });
  });

  // TODO after complet budget component
  it("should test UPDATE_BUDGET_DATA", function () {
    expect((0, _recommendation2.default)(undefined, {
      type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_BUDGET_DATA
    })).toEqual({
      recommendation: {
        chosenList: [],
        notChosenList: []
      },
      budget: {},
      acceptance: [],
      error: {
        recommendation: {},
        budget: {},
        acceptance: {}
      },
      component: {
        completedStep: -1,
        currentStep: 0,
        completedSection: { recommendation: {}, budget: false },
        selectedQuotId: "",
        showDetails: false,
        disabled: false
      }
    });
  });
});