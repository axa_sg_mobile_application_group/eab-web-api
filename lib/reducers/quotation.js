"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * isQuickQuote
 * @description indicates whether quotation is quick quote or not
 * @default false
 * */
var isQuickQuote = function isQuickQuote() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var _ref = arguments[1];
  var type = _ref.type,
      isQuickQuoteData = _ref.isQuickQuoteData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_IS_QUICK_QUOTE:
      return isQuickQuoteData;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

/**
 * quickQuotes
 * @description quick quotes list
 * @default []
 * */
var quickQuotes = function quickQuotes() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref2 = arguments[1];
  var type = _ref2.type,
      quickQuotesData = _ref2.quickQuotesData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUICK_QUOTES:
      return quickQuotesData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * quotation
 * @description quotation main state
 * @default {}
 * */
var quotation = function quotation() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref3 = arguments[1];
  var type = _ref3.type,
      newQuotation = _ref3.newQuotation;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].ALLOC_FUNDS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_RIDERS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].RESET_QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INSURED_QUOTATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newQuotation);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * planDetails
 * @description quotation plan details
 * @default {}
 * */
var planDetails = function planDetails() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newPlanDetails = _ref4.newPlanDetails;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_PLAN_DETAILS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      state = newPlanDetails;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * inputConfigs
 * @description quotation input config
 * @default {}
 * */
var inputConfigs = function inputConfigs() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref5 = arguments[1];
  var type = _ref5.type,
      newInputConfigs = _ref5.newInputConfigs;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INPUT_CONFIGS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_RIDERS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].RESET_QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INSURED_QUOTATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newInputConfigs);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * quotationErrors
 * @description store the quotation errors
 * @default {}
 * */
var quotationErrors = function quotationErrors() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref6 = arguments[1];
  var type = _ref6.type,
      newQuotationErrors = _ref6.newQuotationErrors;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION_ERRORS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].ALLOC_FUNDS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_RIDERS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INSURED_QUOTATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].RESET_QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      return Object.assign({}, newQuotationErrors);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = {};
      return state;
    default:
      return state;
  }
};

/**
 * quotWarnings
 * @description store the quotation warnings
 * @default []
 * */
var quotWarnings = function quotWarnings() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref7 = arguments[1];
  var type = _ref7.type,
      newQuotationWarnings = _ref7.newQuotationWarnings;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION_WARNINGS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_RIDERS_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].RESET_QUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      state = newQuotationWarnings;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

/**
 * availableInsureds
 * @description quotation available insureds
 * @default []
 * */
var availableInsureds = function availableInsureds() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref8 = arguments[1];
  var type = _ref8.type,
      newAvailableInsureds = _ref8.newAvailableInsureds;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_INSUREDS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED:
      state = newAvailableInsureds;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

/**
 * availableFunds
 * @description quotation available funds
 * @default []
 * */
var availableFunds = function availableFunds() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref9 = arguments[1];
  var type = _ref9.type,
      newAvailableFunds = _ref9.newAvailableFunds;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_FUNDS:
      return newAvailableFunds ? newAvailableFunds.slice(0) : [];
    case _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      state = [];
      return state;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  isQuickQuote: isQuickQuote,
  quickQuotes: quickQuotes,
  quotation: quotation,
  planDetails: planDetails,
  inputConfigs: inputConfigs,
  quotationErrors: quotationErrors,
  quotWarnings: quotWarnings,
  availableInsureds: availableInsureds,
  availableFunds: availableFunds
});