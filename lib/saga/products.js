"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDependants = getDependants;
exports.getProductList = getProductList;
exports.productTabBarSaga = productTabBarSaga;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getDependants),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(getProductList),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(productTabBarSaga);

function getDependants(action) {
  var callServer, cid, isQuickQuote, response;
  return regeneratorRuntime.wrap(function getDependants$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          _context.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 6:
          cid = _context.sent;
          _context.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].isQuickQuote;
          });

        case 9:
          isQuickQuote = _context.sent;
          _context.next = 12;
          return (0, _effects.call)(callServer, {
            url: "product",
            data: {
              action: "getDependantList",
              cid: cid,
              quickQuote: isQuickQuote
            }
          });

        case 12:
          response = _context.sent;
          _context.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_DEPENDANTS,
            dependantsData: response.dependants
          });

        case 15:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context.next = 20;
          break;

        case 18:
          _context.prev = 18;
          _context.t0 = _context["catch"](0);

        case 20:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 18]]);
}

function getProductList(action) {
  var callServer, currency, isQuickQuote, insuredCid, cid, optionsMap, response;
  return regeneratorRuntime.wrap(function getProductList$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRODUCTS].currency;
          });

        case 6:
          currency = _context2.sent;
          _context2.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].isQuickQuote;
          });

        case 9:
          isQuickQuote = _context2.sent;
          _context2.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRODUCTS].insuredCid;
          });

        case 12:
          insuredCid = _context2.sent;
          _context2.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 15:
          cid = _context2.sent;
          _context2.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 18:
          optionsMap = _context2.sent;
          _context2.next = 21;
          return (0, _effects.call)(callServer, {
            url: "product",
            data: {
              action: "queryProducts",
              insuredCid: insuredCid || cid,
              proposerCid: insuredCid ? cid : null,
              quickQuote: isQuickQuote,
              params: {
                ccy: currency
              },
              optionsMap: optionsMap
            }
          });

        case 21:
          response = _context2.sent;
          _context2.next = 24;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_PRODUCT_LIST,
            productListData: response.prodCategories || [],
            errorMessage: response.error || response.errorMsg || ""
          });

        case 24:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context2.next = 29;
          break;

        case 27:
          _context2.prev = 27;
          _context2.t0 = _context2["catch"](0);

        case 29:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 27]]);
}

function productTabBarSaga(action) {
  return regeneratorRuntime.wrap(function productTabBarSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_DEPENDANTS,
            callback: action.callback
          });

        case 3:
          _context3.next = 5;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_PRODUCT_LIST
          });

        case 5:
          _context3.next = 9;
          break;

        case 7:
          _context3.prev = 7;
          _context3.t0 = _context3["catch"](0);

        case 9:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 7]]);
}