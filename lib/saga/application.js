"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.applyApplicationFormNormal = applyApplicationFormNormal;
exports.continueApplicationForm = continueApplicationForm;
exports.updateApplicationFormPersonalDetails = updateApplicationFormPersonalDetails;
exports.updatePersonalDetailsAddress = updatePersonalDetailsAddress;
exports.saveApplicationForm = saveApplicationForm;
exports.saveApplicationFormBefore = saveApplicationFormBefore;
exports.closeDialogAndGetApplicationList = closeDialogAndGetApplicationList;
exports.redirectToFNA = redirectToFNA;
exports.updateEappStep = updateEappStep;
exports.getApplication = getApplication;
exports.validatePayment = validatePayment;
exports.validateSubmission = validateSubmission;
exports.updateROPTable = updateROPTable;
exports.updateDynamicApplicationFormAddress = updateDynamicApplicationFormAddress;
exports.updateApplicationFormDynamicQuestions = updateApplicationFormDynamicQuestions;
exports.saveApplicationFormTableRecord = saveApplicationFormTableRecord;
exports.deleteApplicationFormTableRecord = deleteApplicationFormTableRecord;
exports.reRenderPage = reRenderPage;
exports.updateApplicationBackDate = updateApplicationBackDate;
exports.invalidateApplication = invalidateApplication;
exports.updateShownSignatureExpiryAlert = updateShownSignatureExpiryAlert;
exports.updateSelectedSectionKey = updateSelectedSectionKey;
exports.applyApplicationFormShield = applyApplicationFormShield;
exports.continueApplicationFormShield = continueApplicationFormShield;
exports.changeTabShield = changeTabShield;
exports.saveApplicationFormShieldBefore = saveApplicationFormShieldBefore;
exports.updatePolicyNumber = updatePolicyNumber;
exports.applyApplicationForm = applyApplicationForm;
exports.getApplicationsAfterSubmission = getApplicationsAfterSubmission;
exports.genFNA = genFNA;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _SECTION_KEYS = require("../constants/SECTION_KEYS");

var _SECTION_KEYS2 = _interopRequireDefault(_SECTION_KEYS);

var _EAPP = require("../constants/EAPP");

var _EAPP2 = _interopRequireDefault(_EAPP);

var _DYNAMIC_VIEWS = require("../constants/DYNAMIC_VIEWS");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _dynamicApplicationForm = require("../utilities/dynamicApplicationForm");

var _application = require("../utilities/application");

var _paymentAndSubmission = require("./paymentAndSubmission");

var paymentAndSubmission = _interopRequireWildcard(_paymentAndSubmission);

var _signature = require("./signature");

var signature = _interopRequireWildcard(_signature);

var _client = require("./client");

var client = _interopRequireWildcard(_client);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(applyApplicationFormNormal),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(continueApplicationForm),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(updateApplicationFormPersonalDetails),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updatePersonalDetailsAddress),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(saveApplicationForm),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(saveApplicationFormBefore),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(closeDialogAndGetApplicationList),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(redirectToFNA),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(updateEappStep),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(getApplication),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(validatePayment),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(validateSubmission),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(updateROPTable),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(updateDynamicApplicationFormAddress),
    _marked15 = /*#__PURE__*/regeneratorRuntime.mark(updateApplicationFormDynamicQuestions),
    _marked16 = /*#__PURE__*/regeneratorRuntime.mark(saveApplicationFormTableRecord),
    _marked17 = /*#__PURE__*/regeneratorRuntime.mark(deleteApplicationFormTableRecord),
    _marked18 = /*#__PURE__*/regeneratorRuntime.mark(updateApplicationBackDate),
    _marked19 = /*#__PURE__*/regeneratorRuntime.mark(invalidateApplication),
    _marked20 = /*#__PURE__*/regeneratorRuntime.mark(updateShownSignatureExpiryAlert),
    _marked21 = /*#__PURE__*/regeneratorRuntime.mark(updateSelectedSectionKey),
    _marked22 = /*#__PURE__*/regeneratorRuntime.mark(initApplicationFormShield),
    _marked23 = /*#__PURE__*/regeneratorRuntime.mark(initEappStoreForShield),
    _marked24 = /*#__PURE__*/regeneratorRuntime.mark(applyApplicationFormShield),
    _marked25 = /*#__PURE__*/regeneratorRuntime.mark(continueApplicationFormShield),
    _marked26 = /*#__PURE__*/regeneratorRuntime.mark(changeTabShield),
    _marked27 = /*#__PURE__*/regeneratorRuntime.mark(saveApplicationFormShieldBefore),
    _marked28 = /*#__PURE__*/regeneratorRuntime.mark(updatePolicyNumber),
    _marked29 = /*#__PURE__*/regeneratorRuntime.mark(applyApplicationForm),
    _marked30 = /*#__PURE__*/regeneratorRuntime.mark(getApplicationsAfterSubmission),
    _marked31 = /*#__PURE__*/regeneratorRuntime.mark(genFNA);

var _EAPP$action = _EAPP2.default.action,
    SWITCHMENU = _EAPP$action.SWITCHMENU,
    SWITCHTAB = _EAPP$action.SWITCHTAB,
    OPENSUPPORTINGDOCUMENT = _EAPP$action.OPENSUPPORTINGDOCUMENT,
    CLOSEAPPLICATION = _EAPP$action.CLOSEAPPLICATION,
    NEXTPAGE = _EAPP$action.NEXTPAGE,
    GOFNA = _EAPP$action.GOFNA;
var PLAN_DETAILS = _SECTION_KEYS2.default[_REDUCER_TYPES.APPLICATION].PLAN_DETAILS;
function applyApplicationFormNormal(action) {
  var callServer, applicationFormError, callback, resp, newApplication;
  return regeneratorRuntime.wrap(function applyApplicationFormNormal$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 3:
          _context.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 5:
          callServer = _context.sent;
          applicationFormError = {};
          callback = action.callback;
          _context.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "apply",
              id: action.quotationId
            }
          });

        case 10:
          resp = _context.sent;
          newApplication = (0, _application.initApplicationFormPersonalDetailsValue)({
            application: resp.application,
            template: resp.template
          });
          _context.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newTemplate: resp.template,
            newApplication: newApplication
          });

        case 14:
          // init validate state when enters the application form
          (0, _application.initValidateApplicationPersonalDetailsForm)({
            application: newApplication,
            errorObj: applicationFormError
          });
          applicationFormError.applicationForm = {};
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: resp.template,
            rootValues: newApplication,
            errorObj: applicationFormError
          });
          // update completedMenus
          _.forEach(_.get(newApplication, "applicationForm.values.menus"), function (menu) {
            if (menu !== PLAN_DETAILS) {
              (0, _application.updateApplicationFormCompletedMenus)({
                template: resp.template,
                application: newApplication,
                error: applicationFormError,
                sectionKey: menu,
                isInit: true
              });
            }
          });
          _context.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
            errorObj: applicationFormError
          });

        case 20:
          _context.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: resp.application.appStep
          });

        case 22:
          if (_.isFunction(callback)) {
            callback();
          }
          _context.next = 27;
          break;

        case 25:
          _context.prev = 25;
          _context.t0 = _context["catch"](0);

        case 27:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 25]]);
}

function continueApplicationForm(action) {
  var callServer, applicationFormError, data, currentStep, callback, resp, newCurrentStep, newCompletedStep, newApplication;
  return regeneratorRuntime.wrap(function continueApplicationForm$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;

          if (!action.cleanData) {
            _context2.next = 4;
            break;
          }

          _context2.next = 4;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 4:
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context2.sent;
          applicationFormError = {};
          data = action.data, currentStep = action.currentStep, callback = action.callback;
          resp = data;

          if (resp) {
            _context2.next = 14;
            break;
          }

          _context2.next = 13;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "goApplication",
              id: action.applicationId
            }
          });

        case 13:
          resp = _context2.sent;

        case 14:
          newCurrentStep = currentStep || resp.application.appStep;
          newCompletedStep = -1;
          newApplication = (0, _application.initApplicationFormPersonalDetailsValue)({
            application: resp.application,
            template: resp.template
          });
          _context2.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newTemplate: resp.template,
            newApplication: newApplication
          });

        case 19:
          // init validate state when enters the application form
          (0, _application.initValidateApplicationPersonalDetailsForm)({
            application: newApplication,
            errorObj: applicationFormError
          });
          applicationFormError.applicationForm = {};
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: resp.template,
            rootValues: newApplication,
            errorObj: applicationFormError
          });

          // update completedMenus
          _.forEach(_.get(newApplication, "applicationForm.values.menus"), function (menu) {
            if (menu !== PLAN_DETAILS) {
              (0, _application.updateApplicationFormCompletedMenus)({
                template: resp.template,
                application: newApplication,
                error: applicationFormError,
                sectionKey: menu,
                isInit: true
              });
            }
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: applicationFormError
          })) {
            newCompletedStep += 1;
            if (!(0, _application.checkSignatureHasError)(newApplication)) {
              newCompletedStep += 1;
            }
          }

          _context2.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
            errorObj: applicationFormError
          });

        case 26:
          _context2.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: newCurrentStep,
            newCompletedStep: newCompletedStep
          });

        case 28:
          if (![_.get(resp.crossAge, "insuredStatus"), _.get(resp.crossAge, "proposerStatus")].some(function (s) {
            return s === _EAPP2.default.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
          })) {
            _context2.next = 33;
            break;
          }

          _context2.next = 31;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_CROSS_AGE,
            newCrossAge: resp.crossAge
          });

        case 31:
          _context2.next = 33;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
            newIsAlertDelayed: newCurrentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsCrossAgeAlertShow: true
          });

        case 33:
          _context2.next = 35;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
            newIsAlertDelayed: newCurrentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsSignatureExpiryAlertShow: !!resp.showSignExpiryWarning
          });

        case 35:

          if (_.isFunction(callback)) {
            callback({
              currentStep: newCurrentStep
            });
          }
          _context2.next = 40;
          break;

        case 38:
          _context2.prev = 38;
          _context2.t0 = _context2["catch"](0);

        case 40:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 38]]);
}

function updateApplicationFormPersonalDetails(action) {
  var newApplication, validateObj, triggerValidate, callback, template, error, completedStep, selectedSectionKey, newError, newCompletedStep;
  return regeneratorRuntime.wrap(function updateApplicationFormPersonalDetails$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          newApplication = action.newApplication, validateObj = action.validateObj, triggerValidate = action.triggerValidate, callback = action.callback;
          _context3.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 4:
          template = _context3.sent;
          _context3.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 7:
          error = _context3.sent;
          _context3.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 10:
          completedStep = _context3.sent;
          _context3.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.selectedSectionKey;
          });

        case 13:
          selectedSectionKey = _context3.sent;
          newError = _.cloneDeep(error);
          newCompletedStep = completedStep;


          (0, _application.validateApplicationFormPersonalDetails)({
            validateObj: validateObj,
            errorObj: newError
          });

          // trigger other validation
          _.forEach(triggerValidate, function (validate) {
            (0, _application.validateApplicationFormPersonalDetails)({
              validateObj: validate,
              errorObj: newError
            });
          });

          // update completedMenus
          (0, _application.updateApplicationFormCompletedMenus)({
            template: template,
            application: newApplication,
            error: newError,
            sectionKey: selectedSectionKey
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
          } else {
            newCompletedStep = -1;
          }

          _context3.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
            newApplication: newApplication
          });

        case 22:
          _context3.next = 24;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newError,
            newCompletedStep: newCompletedStep
          });

        case 24:

          if (_.isFunction(callback)) {
            callback();
          }
          _context3.next = 29;
          break;

        case 27:
          _context3.prev = 27;
          _context3.t0 = _context3["catch"](0);

        case 29:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 27]]);
}

function updatePersonalDetailsAddress(action) {
  var application, callServer, applicationFormError, completedStep, fileName, postalCode, fieldType, targetProfile, callback, newApplication, newApplicationFormError, response, list;
  return regeneratorRuntime.wrap(function updatePersonalDetailsAddress$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context4.sent;
          _context4.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context4.sent;
          _context4.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 9:
          applicationFormError = _context4.sent;
          _context4.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 12:
          completedStep = _context4.sent;
          fileName = action.fileName, postalCode = action.postalCode, fieldType = action.fieldType, targetProfile = action.targetProfile, callback = action.callback;
          newApplication = _.cloneDeep(application);
          newApplicationFormError = _.cloneDeep(applicationFormError);
          _context4.next = 18;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              method: "post",
              action: "getAddressByPostalCode",
              pC: postalCode,
              fileName: fileName
            }
          });

        case 18:
          response = _context4.sent;
          list = void 0;

          if (fieldType === "mailingAddress") {
            list = {
              valueUpdateList: [{ fieldKey: "mPostalCode", responsekey: "postalCode" }, { fieldKey: "mAddrBlock", responsekey: "BLDGNO" }, { fieldKey: "mAddrStreet", responsekey: "STREETNAME" }, { fieldKey: "mAddrEstate", responsekey: "BLDGNAME" }],
              errorUpdateList: [{ fieldKey: "mPostalCode", responsekey: "postalCode" }, { fieldKey: "mAddrBlock", responsekey: "BLDGNO" }, { fieldKey: "mAddrStreet", responsekey: "STREETNAME" }]
            };
          } else {
            list = {
              valueUpdateList: [{ fieldKey: "postalCode", responsekey: "postalCode" }, { fieldKey: "addrBlock", responsekey: "BLDGNO" }, { fieldKey: "addrStreet", responsekey: "STREETNAME" }, { fieldKey: "addrEstate", responsekey: "BLDGNAME" }],
              errorUpdateList: [{ fieldKey: "postalCode", responsekey: "postalCode" }, { fieldKey: "addrBlock", responsekey: "BLDGNO" }, { fieldKey: "addrStreet", responsekey: "STREETNAME" }]
            };
          }

          _.each(list.valueUpdateList, function (value) {
            var newValue = value.responsekey === "postalCode" ? postalCode : response[value.responsekey] || "";
            _.set(newApplication, "applicationForm.values." + targetProfile + ".personalInfo." + value.fieldKey, newValue);
          });

          _.each(list.errorUpdateList, function (value) {
            var newValue = value.responsekey === "postalCode" ? postalCode : response[value.responsekey] || "";
            _.set(newApplicationFormError, "application." + targetProfile + ".personalInfo." + value.fieldKey, validation.validateMandatory({
              field: {
                type: _FIELD_TYPES.TEXT_FIELD,
                mandatory: true,
                disabled: false
              },
              value: newValue
            }));
          });
          _context4.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
            newApplication: newApplication
          });

        case 25:
          _context4.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newApplicationFormError,
            newCompletedStep: completedStep
          });

        case 27:

          if (!response.success && _.isFunction(callback)) {
            callback();
          }
          _context4.next = 32;
          break;

        case 30:
          _context4.prev = 30;
          _context4.t0 = _context4["catch"](0);

        case 32:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 30]]);
}

function saveApplicationForm(action) {
  var callServer, application, newApplication, clientId, extra, genPDF, policyNumber, callback, response, profileList;
  return regeneratorRuntime.wrap(function saveApplicationForm$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context5.sent;
          newApplication = _.cloneDeep(application);
          clientId = action.clientId, extra = action.extra, genPDF = action.genPDF, policyNumber = action.policyNumber, callback = action.callback;
          _context5.next = 11;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "saveForm",
              applicationId: application.id,
              clientId: clientId === "" ? null : application.pCid,
              values: application.applicationForm.values,
              policyNumber: policyNumber,
              genPDF: genPDF,
              extra: extra
            }
          });

        case 11:
          response = _context5.sent;

          if (!response.success) {
            _context5.next = 22;
            break;
          }

          if (!response.policyNumber) {
            _context5.next = 17;
            break;
          }

          newApplication.policyNumber = policyNumber;
          _context5.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: newApplication
          });

        case 17:
          if (!response.updIds) {
            _context5.next = 22;
            break;
          }

          profileList = (0, _application.updateProfileList)({
            updIds: response.updIds,
            application: newApplication
          });

          if (!(profileList.length > 0)) {
            _context5.next = 22;
            break;
          }

          _context5.next = 22;
          return client.updateProfile({ profileList: profileList });

        case 22:
          if (_.isFunction(callback)) {
            callback();
          }
          _context5.next = 27;
          break;

        case 25:
          _context5.prev = 25;
          _context5.t0 = _context5["catch"](0);

        case 27:
          return _context5.abrupt("return", null);

        case 28:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 25]]);
}

function saveApplicationFormBefore(action) {
  var application, policyNumber, currentStep, nextStep, actionType, callback, genPDF, clientId, extra, exit;
  return regeneratorRuntime.wrap(function saveApplicationFormBefore$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application.policyNumber;
          });

        case 6:
          policyNumber = _context6.sent;


          // keep
          // const selectedSectionKey = yield select(
          //   state => state[APPLICATION].component.selectedSectionKey
          // );
          currentStep = action.currentStep, nextStep = action.nextStep, actionType = action.actionType, callback = action.callback;
          genPDF = action.genPDF;
          clientId = currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION] ? application.pCid : "";
          extra = {};
          exit = false;
          _context6.t0 = actionType;
          _context6.next = _context6.t0 === SWITCHMENU ? 15 : _context6.t0 === SWITCHTAB ? 17 : _context6.t0 === OPENSUPPORTINGDOCUMENT ? 28 : _context6.t0 === CLOSEAPPLICATION ? 30 : _context6.t0 === NEXTPAGE ? 32 : _context6.t0 === GOFNA ? 44 : 46;
          break;

        case 15:
          // TODO trigger save form only while personal form switch to others
          clientId = application.pCid;
          return _context6.abrupt("break", 47);

        case 17:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION] && nextStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE])) {
            _context6.next = 21;
            break;
          }

          extra.formCompleted = true;
          _context6.next = 27;
          break;

        case 21:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT])) {
            _context6.next = 26;
            break;
          }

          _context6.next = 24;
          return (0, _effects.call)(paymentAndSubmission.updatePaymentMethods, {
            docId: application.id
          });

        case 24:
          _context6.next = 27;
          break;

        case 26:
          exit = true;

        case 27:
          return _context6.abrupt("break", 47);

        case 28:
          if (currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE] || currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT]) {
            exit = true;
          }
          return _context6.abrupt("break", 47);

        case 30:
          if (currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION]) {
            extra.resetAppStep = false;
          } else {
            exit = true;
          }
          return _context6.abrupt("break", 47);

        case 32:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION])) {
            _context6.next = 36;
            break;
          }

          extra.formCompleted = true;
          _context6.next = 43;
          break;

        case 36:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE])) {
            _context6.next = 40;
            break;
          }

          exit = true;
          _context6.next = 43;
          break;

        case 40:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT])) {
            _context6.next = 43;
            break;
          }

          _context6.next = 43;
          return (0, _effects.call)(paymentAndSubmission.updatePaymentMethods, {
            docId: application.id
          });

        case 43:
          return _context6.abrupt("break", 47);

        case 44:
          clientId = application.pCid;
          return _context6.abrupt("break", 47);

        case 46:
          return _context6.abrupt("break", 47);

        case 47:
          // TODO: Need to retest this scenario if use this code: click into Insurability page, then click to Personal Details page, crashes.
          // if (selectedSectionKey === SECTION_KEYS[APPLICATION].INSURABILITY) {
          //   // TODO if (Declaration) && (Insurability Info) do not exist; true
          //   extra.toGetPolicyNumber = true;
          // }
          if (policyNumber) {
            extra.toGetPolicyNumber = true;
          }

          if (exit) {
            _context6.next = 51;
            break;
          }

          _context6.next = 51;
          return (0, _effects.call)(saveApplicationForm, {
            extra: extra,
            clientId: clientId,
            genPDF: genPDF,
            policyNumber: policyNumber,
            callback: callback
          });

        case 51:

          if (_.isFunction(callback)) {
            callback();
          }
          _context6.next = 56;
          break;

        case 54:
          _context6.prev = 54;
          _context6.t1 = _context6["catch"](0);

        case 56:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 54]]);
}

function closeDialogAndGetApplicationList(action) {
  return regeneratorRuntime.wrap(function closeDialogAndGetApplicationList$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 3:
          _context7.next = 5;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS
          });

        case 5:
          if (!_.isFunction(action.callback)) {
            _context7.next = 8;
            break;
          }

          _context7.next = 8;
          return (0, _effects.call)(action.callback);

        case 8:
          _context7.next = 12;
          break;

        case 10:
          _context7.prev = 10;
          _context7.t0 = _context7["catch"](0);

        case 12:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 10]]);
}

function redirectToFNA(action) {
  var page, callback;
  return regeneratorRuntime.wrap(function redirectToFNA$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          page = action.page, callback = action.callback;
          _context8.next = 4;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 4:
          _context8.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].REDIRECT_TO,
            newPage: page
          });

        case 6:

          if (_.isFunction(callback)) {
            callback();
          }
          _context8.next = 11;
          break;

        case 9:
          _context8.prev = 9;
          _context8.t0 = _context8["catch"](0);

        case 11:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 9]]);
}

function updateEappStep(action) {
  var callServer, application, currentStep, nextStep, isShield, callback, resp;
  return regeneratorRuntime.wrap(function updateEappStep$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context9.sent;
          _context9.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.currentStep;
          });

        case 9:
          currentStep = _context9.sent;
          nextStep = action.nextStep, isShield = action.isShield, callback = action.callback;
          _context9.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: nextStep
          });

        case 13:
          if (!(!isShield && currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE] && nextStep === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT] && application.isFullySigned)) {
            _context9.next = 20;
            break;
          }

          _context9.next = 16;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "updateAppStep",
              docId: application.id,
              stepperIndex: currentStep
            }
          });

        case 16:
          resp = _context9.sent;

          if (!resp.success) {
            _context9.next = 20;
            break;
          }

          _context9.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: nextStep
          });

        case 20:

          if (_.isFunction(callback)) {
            callback();
          }
          _context9.next = 25;
          break;

        case 23:
          _context9.prev = 23;
          _context9.t0 = _context9["catch"](0);

        case 25:
          return _context9.abrupt("return", null);

        case 26:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 23]]);
}

// getApplication the application object
function getApplication(action) {
  var callServer, docId, getApplicationResponse, getApplicationResult, errorMsg, _getApplicationResult;

  return regeneratorRuntime.wrap(function getApplication$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context10.sent;
          docId = action.docId;
          _context10.next = 7;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "goApplication",
              id: docId
            }
          });

        case 7:
          getApplicationResponse = _context10.sent;
          getApplicationResult = {};

          if (getApplicationResponse.success) {
            getApplicationResult = {
              hasError: false,
              result: getApplicationResponse
            };
          } else {
            getApplicationResult = {
              hasError: true,
              errorMsg: "Cannot get the application"
            };
            // yield put({
            //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
            //   errorMsg: getApplicationResponse.id
            // });
          }
          action.callback(getApplicationResult);
          _context10.next = 18;
          break;

        case 13:
          _context10.prev = 13;
          _context10.t0 = _context10["catch"](0);

          /* TODO need error handling */
          errorMsg = _context10.t0;
          _getApplicationResult = {
            hasError: true,
            errorMsg: "Catch Error" + errorMsg
          };

          action.callback(_getApplicationResult);

        case 18:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 13]]);
}

// validatePayment: Checks are performed before data sync when payment button is pressed.
function validatePayment(action) {
  var callServer, docId, validatePaymentResponse, isPaymentAllowed, errorMsg, validatePaymentResult, initPayMethodCheck, trxStatusCheck, trxTimeCheck, _errorMsg, _validatePaymentResult;

  return regeneratorRuntime.wrap(function validatePayment$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          console.log("[Saga]-[validatePayment]-[Start]");
          _context11.prev = 1;
          _context11.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context11.sent;
          docId = action.docId;
          _context11.next = 8;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "goApplication",
              id: docId
            }
          });

        case 8:
          validatePaymentResponse = _context11.sent;

          console.log("[Saga]-[validatePaymentResponse]-" + validatePaymentResponse);
          console.log(validatePaymentResponse);
          isPaymentAllowed = false;
          errorMsg = "";
          validatePaymentResult = {};

          if (validatePaymentResponse.success) {
            if (validatePaymentResponse.application.type === "application" || validatePaymentResponse.application.type === "masterApplication") {
              if (validatePaymentResponse.application.payment) {
                initPayMethodCheck = validatePaymentResponse.application.payment.initPayMethod;
                trxStatusCheck = validatePaymentResponse.application.payment.trxStatus;
                trxTimeCheck = validatePaymentResponse.application.payment.trxTime;

                if ((initPayMethodCheck === "crCard" || initPayMethodCheck === "eNets" || initPayMethodCheck === "dbsCrCardlpp") && trxStatusCheck === "Y" && trxTimeCheck > 0) {
                  isPaymentAllowed = false;
                  errorMsg = "Payment is performed already";
                } else if (initPayMethodCheck && initPayMethodCheck.length > 0) {
                  isPaymentAllowed = false;
                  errorMsg = "Payment is performed already";
                } else {
                  // Can perform payment la
                  isPaymentAllowed = true;
                }
              } else {
                // Error: no payment object in the application object
                isPaymentAllowed = false;
                errorMsg = "No payment object in the application object";
              }
            } else {
              // Error: document type is not application or masterApplication
              isPaymentAllowed = false;
              errorMsg = "document type is not application or masterApplication";
            }
          } else {
            isPaymentAllowed = false;
            errorMsg = "Fail to get Application Object";
            // yield put({
            //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
            //   errorMsg
            // });
          }
          // ToDO: In order to testing, set isPaymentAllowed = true;
          isPaymentAllowed = true;
          if (isPaymentAllowed) {
            // can do payment
            validatePaymentResult = {
              hasError: false,
              result: validatePaymentResponse
            };
          } else {
            // payment is not allowed to do
            validatePaymentResult = {
              hasError: true,
              errorMsg: errorMsg
            };
          }
          // callback
          action.callback(validatePaymentResult);
          _context11.next = 25;
          break;

        case 20:
          _context11.prev = 20;
          _context11.t0 = _context11["catch"](1);

          /* TODO need error handling */
          _errorMsg = _context11.t0;
          _validatePaymentResult = {
            hasError: true,
            errorMsg: "Catch Error" + _errorMsg
          };

          action.callback(_validatePaymentResult);

        case 25:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[1, 20]]);
}

// validateSubmission: Checks are performed before data sync when submission button is pressed.
function validateSubmission(action) {
  var callServer, docId, validateSubmissionResponse, isSubmissionAllowed, errorMsg, validateSubmissionResult, isSubmittedStatusCheck, isMandDocsAllUploadedCheck, _errorMsg2, _validateSubmissionResult;

  return regeneratorRuntime.wrap(function validateSubmission$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          console.log("[Saga]-[validateSubmission]-[Start]");
          _context12.prev = 1;
          _context12.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context12.sent;
          docId = action.docId;
          _context12.next = 8;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "goApplication",
              id: docId
            }
          });

        case 8:
          validateSubmissionResponse = _context12.sent;

          console.log("[Saga]-[validateSubmissionResponse]-" + validateSubmissionResponse);
          console.log(validateSubmissionResponse);
          isSubmissionAllowed = false;
          errorMsg = "";
          validateSubmissionResult = {};

          if (validateSubmissionResponse.success) {
            isSubmittedStatusCheck = validateSubmissionResponse.application.isSubmittedStatus;
            isMandDocsAllUploadedCheck = validateSubmissionResponse.application.isMandDocsAllUploaded;

            if (isSubmittedStatusCheck) {
              isSubmissionAllowed = false;
              errorMsg = "Submission is performed.";
            } else if (isMandDocsAllUploadedCheck) {
              isSubmissionAllowed = true;
            } else {
              isSubmissionAllowed = false;
              errorMsg = "Mand. Documents are not all uploaded, please upload ALL before submission";
            }
          } else {
            isSubmissionAllowed = false;
            errorMsg = "Fail to get Application Object";
            // yield put({
            //   type: ACTION_TYPES[APPLICATION].SHOW_APP_ERR_MSG,
            //   errorMsg: validateSubmissionResult.id
            // });
          }
          // ToDO: In order to testing, set isSubmissionAllowed = true;
          isSubmissionAllowed = true;
          if (isSubmissionAllowed) {
            // can do payment
            validateSubmissionResult = {
              hasError: false,
              result: validateSubmissionResponse
            };
          } else {
            // payment is not allowed to do
            validateSubmissionResult = {
              hasError: true,
              errorMsg: errorMsg
            };
          }
          // callback
          action.callback(validateSubmissionResult);
          _context12.next = 25;
          break;

        case 20:
          _context12.prev = 20;
          _context12.t0 = _context12["catch"](1);

          /* TODO need error handling */
          _errorMsg2 = _context12.t0;
          _validateSubmissionResult = {
            hasError: true,
            errorMsg: "Catch Error" + _errorMsg2
          };

          action.callback(_validateSubmissionResult);

        case 25:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[1, 20]]);
}

function updateROPTable(action) {
  var path, pathValue, id, error, template, application, completedStep, selectedSectionKey, newError, newApplication, newCompletedStep;
  return regeneratorRuntime.wrap(function updateROPTable$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.prev = 0;
          path = action.path, pathValue = action.pathValue, id = action.id;
          _context13.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 4:
          error = _context13.sent;
          _context13.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 7:
          template = _context13.sent;
          _context13.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 10:
          application = _context13.sent;
          _context13.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 13:
          completedStep = _context13.sent;
          _context13.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.selectedSectionKey;
          });

        case 16:
          selectedSectionKey = _context13.sent;
          newError = _.cloneDeep(error);
          newApplication = _.cloneDeep(application);
          newCompletedStep = completedStep;

          // calculate column

          (0, _dynamicApplicationForm.calculateRopTable)({
            rootValues: newApplication,
            path: path,
            id: id,
            pathValue: pathValue
          });
          // Can't get iTemplate from here, so need isAcc.
          validation.validateROPTable({
            iErrorObj: _.get(newError, path, {}),
            iValues: _.get(newApplication, path, {}),
            baseProductCode: _.get(newApplication, "quotation.baseProductCode", "")
          });

          // update completedMenus
          (0, _application.updateApplicationFormCompletedMenus)({
            template: template,
            application: newApplication,
            error: newError,
            sectionKey: selectedSectionKey
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
          } else {
            newCompletedStep = -1;
          }

          _context13.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
            newApplication: newApplication
          });

        case 26:
          _context13.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newError,
            newCompletedStep: newCompletedStep
          });

        case 28:
          _context13.next = 32;
          break;

        case 30:
          _context13.prev = 30;
          _context13.t0 = _context13["catch"](0);

        case 32:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13, this, [[0, 30]]);
}

function updateDynamicApplicationFormAddress(action) {
  var application, callServer, applicationFormError, completedStep, fileName, postalCode, path, callback, newApplication, newApplicationFormError, response, list;
  return regeneratorRuntime.wrap(function updateDynamicApplicationFormAddress$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.prev = 0;
          _context14.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context14.sent;
          _context14.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context14.sent;
          _context14.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 9:
          applicationFormError = _context14.sent;
          _context14.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 12:
          completedStep = _context14.sent;
          fileName = action.fileName, postalCode = action.postalCode, path = action.path, callback = action.callback;
          newApplication = _.cloneDeep(application);
          newApplicationFormError = _.cloneDeep(applicationFormError);
          _context14.next = 18;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              method: "post",
              action: "getAddressByPostalCode",
              pC: postalCode,
              fileName: fileName
            }
          });

        case 18:
          response = _context14.sent;


          if (_.isFunction(callback)) {
            callback({ success: !!response.success });
          }

          list = "";

          list = [{ fieldKey: "FUND_SRC15", responsekey: "BLDGNO", mandatory: true }, { fieldKey: "FUND_SRC16", responsekey: "STREETNAME", mandatory: true }, { fieldKey: "FUND_SRC18", responsekey: "BLDGNAME", mandatory: false }];
          list.forEach(function (value) {
            _.set(newApplication, path + "." + value.fieldKey, response[value.responsekey] || "");
            _.set(newApplicationFormError, path + "." + value.fieldKey, validation.validateMandatory({
              field: {
                type: _FIELD_TYPES.TEXT_FIELD,
                mandatory: value.mandatory,
                disabled: false
              },
              value: response[value.responsekey] || ""
            }));
          });
          _context14.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
            newApplication: newApplication
          });

        case 25:
          _context14.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newApplicationFormError,
            newCompletedStep: completedStep
          });

        case 27:
          _context14.next = 31;
          break;

        case 29:
          _context14.prev = 29;
          _context14.t0 = _context14["catch"](0);

        case 31:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14, this, [[0, 29]]);
}

function updateApplicationFormDynamicQuestions(action) {
  var id, iTemplate, path, pathValue, error, application, template, completedStep, selectedSectionKey, itemPath, newError, newApplication, newCompletedStep;
  return regeneratorRuntime.wrap(function updateApplicationFormDynamicQuestions$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.prev = 0;
          id = action.id, iTemplate = action.iTemplate, path = action.path, pathValue = action.pathValue;
          _context15.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 4:
          error = _context15.sent;
          _context15.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 7:
          application = _context15.sent;
          _context15.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 10:
          template = _context15.sent;
          _context15.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 13:
          completedStep = _context15.sent;
          _context15.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.selectedSectionKey;
          });

        case 16:
          selectedSectionKey = _context15.sent;
          itemPath = path + "." + id;
          newError = _.cloneDeep(error);
          newApplication = _.cloneDeep(application);
          newCompletedStep = completedStep;

          // update values

          (0, _dynamicApplicationForm.updateFieldValue)({
            iTemplate: iTemplate,
            rootValues: newApplication,
            path: path,
            id: id,
            pathValue: pathValue
          });

          // update error
          // TODOL update type based on template type
          _.set(newError, itemPath, validation.validateMandatory({
            field: { type: _FIELD_TYPES.TEXT_SELECTION, mandatory: true, disabled: false },
            value: pathValue
          }));

          // update influenced values and errors by trigger
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: template,
            rootValues: newApplication,
            errorObj: newError
          });

          // update completedMenus
          (0, _application.updateApplicationFormCompletedMenus)({
            template: template,
            application: newApplication,
            error: newError,
            sectionKey: selectedSectionKey
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
          } else {
            newCompletedStep = -1;
          }

          _context15.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
            newApplication: newApplication
          });

        case 28:
          _context15.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newError,
            newCompletedStep: newCompletedStep
          });

        case 30:
          _context15.next = 34;
          break;

        case 32:
          _context15.prev = 32;
          _context15.t0 = _context15["catch"](0);

        case 34:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15, this, [[0, 32]]);
}

function saveApplicationFormTableRecord(action) {
  var path, dataId, values, recordIndex, template, application, error, completedStep, selectedSectionKey, newApplication, newError, dataPath, dataValues, newCompletedStep;
  return regeneratorRuntime.wrap(function saveApplicationFormTableRecord$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.prev = 0;
          path = action.path, dataId = action.dataId, values = action.values, recordIndex = action.recordIndex;
          _context16.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 4:
          template = _context16.sent;
          _context16.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 7:
          application = _context16.sent;
          _context16.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 10:
          error = _context16.sent;
          _context16.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 13:
          completedStep = _context16.sent;
          _context16.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.selectedSectionKey;
          });

        case 16:
          selectedSectionKey = _context16.sent;
          newApplication = _.cloneDeep(application);
          newError = _.cloneDeep(error);
          dataPath = path + "." + dataId;
          dataValues = _.get(newApplication, dataPath, []);
          newCompletedStep = completedStep;


          if (recordIndex === _DYNAMIC_VIEWS.DIALOG_NEW_RECORD) {
            dataValues.push(values);
          } else if (!_.isUndefined(dataValues[recordIndex])) {
            dataValues[recordIndex] = values;
          }
          _.set(newApplication, dataPath, dataValues);

          // update influenced values and errors by trigger
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: template,
            rootValues: newApplication,
            errorObj: newError
          });

          // update completedMenus
          (0, _application.updateApplicationFormCompletedMenus)({
            template: template,
            application: newApplication,
            error: newError,
            sectionKey: selectedSectionKey
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
          } else {
            newCompletedStep = -1;
          }

          _context16.next = 29;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
            newApplication: newApplication
          });

        case 29:
          _context16.next = 31;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newError,
            newCompletedStep: newCompletedStep
          });

        case 31:
          _context16.next = 35;
          break;

        case 33:
          _context16.prev = 33;
          _context16.t0 = _context16["catch"](0);

        case 35:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16, this, [[0, 33]]);
}

function deleteApplicationFormTableRecord(action) {
  var path, dataId, recordIndex, template, application, error, completedStep, selectedSectionKey, newApplication, newError, dataPath, dataValues, dataError, newCompletedStep;
  return regeneratorRuntime.wrap(function deleteApplicationFormTableRecord$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.prev = 0;
          path = action.path, dataId = action.dataId, recordIndex = action.recordIndex;
          _context17.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 4:
          template = _context17.sent;
          _context17.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 7:
          application = _context17.sent;
          _context17.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 10:
          error = _context17.sent;
          _context17.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.completedStep;
          });

        case 13:
          completedStep = _context17.sent;
          _context17.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.selectedSectionKey;
          });

        case 16:
          selectedSectionKey = _context17.sent;
          newApplication = _.cloneDeep(application);
          newError = _.cloneDeep(error);
          dataPath = path + "." + dataId;
          dataValues = _.get(newApplication, dataPath, []);
          dataError = _.get(newError, dataPath, []);
          newCompletedStep = completedStep;


          _.remove(dataValues, function (v, index) {
            return index === recordIndex;
          });
          _.remove(dataError, function (v, index) {
            return index === recordIndex;
          });
          _.set(newApplication, dataPath, dataValues);
          _.set(newError, dataPath, dataError);

          // update influenced values and errors by trigger
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: template,
            rootValues: newApplication,
            errorObj: newError
          });

          // update completedMenus
          (0, _application.updateApplicationFormCompletedMenus)({
            template: template,
            application: newApplication,
            error: newError,
            sectionKey: selectedSectionKey
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep = newCompletedStep < 0 ? 0 : newCompletedStep;
          } else {
            newCompletedStep = -1;
          }

          _context17.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES,
            newApplication: newApplication
          });

        case 32:
          _context17.next = 34;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM,
            errorObj: newError,
            newCompletedStep: newCompletedStep
          });

        case 34:
          _context17.next = 38;
          break;

        case 36:
          _context17.prev = 36;
          _context17.t0 = _context17["catch"](0);

        case 38:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked17, this, [[0, 36]]);
}

function reRenderPage(action) {
  try {
    var dispatch = action.dispatch,
        currentPage = action.currentPage,
        applicationId = action.applicationId,
        quotType = action.quotType;

    switch (currentPage) {
      case "ApplicationForm":
        dispatch({
          type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
          applicationId: applicationId,
          quotType: quotType
        });
        break;
      case "Signatures":
        break;
      case "PaymentAndSubmisstion":
        dispatch({
          type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT_AND_SUBMISSION
        });
        break;
      default:
        break;
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

function updateApplicationBackDate(action) {
  var callback, callServer, application, resp;
  return regeneratorRuntime.wrap(function updateApplicationBackDate$(_context18) {
    while (1) {
      switch (_context18.prev = _context18.next) {
        case 0:
          _context18.prev = 0;
          callback = action.callback;
          _context18.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context18.sent;
          _context18.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 7:
          application = _context18.sent;
          _context18.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "updateBIBackDateTrue",
              appId: application.id
            }
          });

        case 10:
          resp = _context18.sent;

          if (!resp.success) {
            _context18.next = 16;
            break;
          }

          _context18.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: resp.application
          });

        case 14:
          _context18.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_BACKDATE,
            newAttachments: resp.attachments
          });

        case 16:
          if (!_.isFunction(callback)) {
            _context18.next = 19;
            break;
          }

          _context18.next = 19;
          return (0, _effects.call)(callback);

        case 19:
          _context18.next = 23;
          break;

        case 21:
          _context18.prev = 21;
          _context18.t0 = _context18["catch"](0);

        case 23:
        case "end":
          return _context18.stop();
      }
    }
  }, _marked18, this, [[0, 21]]);
}

function invalidateApplication(action) {
  var callback, callServer, application, isShield, resp;
  return regeneratorRuntime.wrap(function invalidateApplication$(_context19) {
    while (1) {
      switch (_context19.prev = _context19.next) {
        case 0:
          _context19.prev = 0;
          callback = action.callback;
          _context19.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context19.sent;
          _context19.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 7:
          application = _context19.sent;
          isShield = application.quotation.quotType === "SHIELD";

          if (!isShield) {
            _context19.next = 15;
            break;
          }

          _context19.next = 12;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "invalidateApplication",
              appId: application.id
            }
          });

        case 12:
          _context19.t0 = _context19.sent;
          _context19.next = 18;
          break;

        case 15:
          _context19.next = 17;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "invalidateApplication",
              appId: application.id
            }
          });

        case 17:
          _context19.t0 = _context19.sent;

        case 18:
          resp = _context19.t0;


          if (resp.success) {
            // TODO
          }

          if (!_.isFunction(callback)) {
            _context19.next = 23;
            break;
          }

          _context19.next = 23;
          return (0, _effects.call)(callback);

        case 23:
          _context19.next = 27;
          break;

        case 25:
          _context19.prev = 25;
          _context19.t1 = _context19["catch"](0);

        case 27:
        case "end":
          return _context19.stop();
      }
    }
  }, _marked19, this, [[0, 25]]);
}

function updateShownSignatureExpiryAlert() {
  var callServer, application, resp;
  return regeneratorRuntime.wrap(function updateShownSignatureExpiryAlert$(_context20) {
    while (1) {
      switch (_context20.prev = _context20.next) {
        case 0:
          _context20.prev = 0;
          _context20.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context20.sent;
          _context20.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context20.sent;
          _context20.next = 9;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "setSignExpiryShown",
              appId: application.id
            }
          });

        case 9:
          resp = _context20.sent;

          if (!resp.success) {
            _context20.next = 13;
            break;
          }

          _context20.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
            newIsAlertDelayed: false,
            newIsSignatureExpiryAlertShow: false
          });

        case 13:
          _context20.next = 17;
          break;

        case 15:
          _context20.prev = 15;
          _context20.t0 = _context20["catch"](0);

        case 17:
        case "end":
          return _context20.stop();
      }
    }
  }, _marked20, this, [[0, 15]]);
}

function updateSelectedSectionKey(action) {
  var newSelectedSectionKey, template, application, currTemplate, error, newError, newApplication, checkedMenu;
  return regeneratorRuntime.wrap(function updateSelectedSectionKey$(_context21) {
    while (1) {
      switch (_context21.prev = _context21.next) {
        case 0:
          _context21.prev = 0;
          newSelectedSectionKey = action.newSelectedSectionKey, template = action.template;
          _context21.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 4:
          application = _context21.sent;
          _context21.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 7:
          currTemplate = _context21.sent;
          _context21.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 10:
          error = _context21.sent;
          newError = _.cloneDeep(error);
          newApplication = _.cloneDeep(application);

          // update completedMenus

          _.forEach(_.get(application, "applicationForm.values.menus"), function (menu) {
            if (menu !== PLAN_DETAILS) {
              (0, _application.updateApplicationFormCompletedMenus)({
                template: template || currTemplate,
                application: application,
                error: newError,
                sectionKey: menu,
                isInit: true
              });
            }
          });

          checkedMenu = _.get(newApplication, "applicationForm.values.checkedMenu", []);

          if (!checkedMenu.includes(newSelectedSectionKey)) {
            checkedMenu.push(newSelectedSectionKey);
          }

          _context21.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: newApplication
          });

        case 18:
          _context21.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SELECTED_SECTION_KEY,
            newSelectedSectionKey: newSelectedSectionKey
          });

        case 20:
          _context21.next = 24;
          break;

        case 22:
          _context21.prev = 22;
          _context21.t0 = _context21["catch"](0);

        case 24:
        case "end":
          return _context21.stop();
      }
    }
  }, _marked21, this, [[0, 22]]);
}

// =============================================================================
// For Shield Below
// =============================================================================

function initApplicationFormShield(action) {
  var newCompletedStep, template, application, currTemplate, error, newError, newApplication;
  return regeneratorRuntime.wrap(function initApplicationFormShield$(_context22) {
    while (1) {
      switch (_context22.prev = _context22.next) {
        case 0:
          newCompletedStep = -1;
          template = action.template, application = action.application;
          _context22.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].template;
          });

        case 4:
          currTemplate = _context22.sent;
          _context22.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].error;
          });

        case 7:
          error = _context22.sent;
          newError = _.cloneDeep(error);
          newApplication = (0, _application.initApplicationFormPersonalDetailsValue)({
            application: application,
            template: template || currTemplate
          });
          _context22.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newTemplate: template,
            newApplication: newApplication
          });

        case 12:
          // init validate state when enters the application form
          (0, _application.initValidateApplicationPersonalDetailsForm)({
            application: newApplication,
            errorObj: newError
          });
          newError.applicationForm = {};
          (0, _dynamicApplicationForm.initValidateApplicationFormError)({
            template: template || currTemplate,
            rootValues: newApplication,
            errorObj: newError
          });

          // update stepper
          if (!(0, _application.checkApplicationFormHasError)({
            application: newApplication,
            error: newError
          })) {
            newCompletedStep += 1;
            if (!(0, _application.checkSignatureHasError)(newApplication)) {
              newCompletedStep += 1;
            }
          }

          _context22.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS,
            errorObj: newError
          });

        case 18:
          _context22.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION],
            newCompletedStep: newCompletedStep
          });

        case 20:
        case "end":
          return _context22.stop();
      }
    }
  }, _marked22, this);
}

function initEappStoreForShield(eappData) {
  var currentStep, paymentErrorObj, newIsSignatureExpiryAlertShow;
  return regeneratorRuntime.wrap(function initEappStoreForShield$(_context23) {
    while (1) {
      switch (_context23.prev = _context23.next) {
        case 0:
          _context23.prev = 0;
          currentStep = eappData.stepper.index.current;
          _context23.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error;
          });

        case 4:
          paymentErrorObj = _context23.sent;
          _context23.t0 = currentStep;
          _context23.next = _context23.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION] ? 8 : _context23.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE] ? 11 : _context23.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT] ? 14 : _context23.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.SUBMISSION] ? 28 : 33;
          break;

        case 8:
          _context23.next = 10;
          return initApplicationFormShield(eappData);

        case 10:
          return _context23.abrupt("break", 34);

        case 11:
          _context23.next = 13;
          return signature.initSignatureShield(eappData);

        case 13:
          return _context23.abrupt("break", 34);

        case 14:
          _context23.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: eappData.application
          });

        case 16:
          _context23.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].BEFORE_UPDATE_PAYMENT_STATUS,
            newPayment: eappData.application.payment
          });

        case 18:
          _context23.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newPayment: eappData.application.payment
          });

        case 20:
          paymentAndSubmission.initValidatePayment({
            dataObj: eappData.application.payment,
            errorObj: paymentErrorObj
          });
          _context23.next = 23;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_COMPLETED_STEP,
            newCompletedStep: Object.values(paymentErrorObj).find(function (value) {
              if (value.hasError) {
                return true;
              }
              return false;
            }) ? 2 : 3
          });

        case 23:
          _context23.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: paymentErrorObj
          });

        case 25:
          _context23.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newTemplate: eappData.template
          });

        case 27:
          return _context23.abrupt("break", 34);

        case 28:
          _context23.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newSubmission: eappData.application.submission
          });

        case 30:
          _context23.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: eappData.application
          });

        case 32:
          return _context23.abrupt("break", 34);

        case 33:
          return _context23.abrupt("break", 34);

        case 34:
          if (!(_.get(eappData.crossAge, "status") === _EAPP2.default.CROSSAGE_STATUS.CROSSED_AGE_SIGNED)) {
            _context23.next = 39;
            break;
          }

          _context23.next = 37;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_CROSS_AGE,
            newCrossAge: eappData.crossAge
          });

        case 37:
          _context23.next = 39;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
            newIsAlertDelayed: currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsCrossAgeAlertShow: true
          });

        case 39:
          newIsSignatureExpiryAlertShow = false;

          if (eappData.warningMsg && eappData.warningMsg.showSignExpiryWarning) {
            newIsSignatureExpiryAlertShow = eappData.warningMsg.showSignExpiryWarning;
          }
          _context23.next = 43;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
            newIsAlertDelayed: currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsSignatureExpiryAlertShow: newIsSignatureExpiryAlertShow
          });

        case 43:
          _context23.next = 47;
          break;

        case 45:
          _context23.prev = 45;
          _context23.t1 = _context23["catch"](0);

        case 47:
        case "end":
          return _context23.stop();
      }
    }
  }, _marked23, this, [[0, 45]]);
}

function applyApplicationFormShield(action) {
  var callServer, quotationId, callback, response;
  return regeneratorRuntime.wrap(function applyApplicationFormShield$(_context24) {
    while (1) {
      switch (_context24.prev = _context24.next) {
        case 0:
          _context24.prev = 0;
          _context24.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 3:
          _context24.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 5:
          callServer = _context24.sent;
          quotationId = action.quotationId, callback = action.callback;
          _context24.next = 9;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "applyAppForm",
              quotId: quotationId
            }
          });

        case 9:
          response = _context24.sent;

          if (!response.success) {
            _context24.next = 13;
            break;
          }

          _context24.next = 13;
          return (0, _effects.call)(initEappStoreForShield, response);

        case 13:
          if (_.isFunction(callback)) {
            callback();
          }
          _context24.next = 18;
          break;

        case 16:
          _context24.prev = 16;
          _context24.t0 = _context24["catch"](0);

        case 18:
        case "end":
          return _context24.stop();
      }
    }
  }, _marked24, this, [[0, 16]]);
}

function continueApplicationFormShield(action) {
  var callServer, applicationId, callback, response;
  return regeneratorRuntime.wrap(function continueApplicationFormShield$(_context25) {
    while (1) {
      switch (_context25.prev = _context25.next) {
        case 0:
          _context25.prev = 0;

          if (!action.cleanData) {
            _context25.next = 4;
            break;
          }

          _context25.next = 4;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA
          });

        case 4:
          _context25.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context25.sent;
          applicationId = action.applicationId, callback = action.callback;
          _context25.next = 10;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "continueApplication",
              appId: applicationId
            }
          });

        case 10:
          response = _context25.sent;

          if (!response.success) {
            _context25.next = 14;
            break;
          }

          _context25.next = 14;
          return (0, _effects.call)(initEappStoreForShield, response);

        case 14:
          if (_.isFunction(callback)) {
            callback({
              currentStep: _.get(response, "stepper.index.current", 0)
            });
          }
          _context25.next = 19;
          break;

        case 17:
          _context25.prev = 17;
          _context25.t0 = _context25["catch"](0);

        case 19:
        case "end":
          return _context25.stop();
      }
    }
  }, _marked25, this, [[0, 17]]);
}

function changeTabShield(action) {
  var callServer, application, currentStep, nextStep, callback, changedValues, response;
  return regeneratorRuntime.wrap(function changeTabShield$(_context26) {
    while (1) {
      switch (_context26.prev = _context26.next) {
        case 0:
          _context26.prev = 0;
          _context26.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context26.sent;
          _context26.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context26.sent;
          currentStep = action.currentStep, nextStep = action.nextStep, callback = action.callback;
          changedValues = "";
          _context26.t0 = currentStep;
          _context26.next = _context26.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION] ? 12 : _context26.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE] ? 16 : _context26.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT] ? 20 : _context26.t0 === _EAPP2.default.tabStep[_REDUCER_TYPES.SUBMISSION] ? 24 : 28;
          break;

        case 12:
          _context26.next = 14;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application.applicationForm.values;
          });

        case 14:
          changedValues = _context26.sent;
          return _context26.abrupt("break", 29);

        case 16:
          _context26.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application.applicationForm.values;
          });

        case 18:
          changedValues = _context26.sent;
          return _context26.abrupt("break", 29);

        case 20:
          _context26.next = 22;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment;
          });

        case 22:
          changedValues = _context26.sent;
          return _context26.abrupt("break", 29);

        case 24:
          _context26.next = 26;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION];
          });

        case 26:
          changedValues = _context26.sent;
          return _context26.abrupt("break", 29);

        case 28:
          return _context26.abrupt("break", 29);

        case 29:
          _context26.next = 31;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "goNextStep",
              appId: application.id,
              currIndex: currentStep,
              nextIndex: nextStep,
              changedValues: changedValues
            }
          });

        case 31:
          response = _context26.sent;

          if (!response.success) {
            _context26.next = 36;
            break;
          }

          _.set(response, "stepper.index.current", nextStep);
          _context26.next = 36;
          return (0, _effects.call)(initEappStoreForShield, response);

        case 36:
          if (_.isFunction(callback)) {
            callback();
          }
          _context26.next = 41;
          break;

        case 39:
          _context26.prev = 39;
          _context26.t1 = _context26["catch"](0);

        case 41:
        case "end":
          return _context26.stop();
      }
    }
  }, _marked26, this, [[0, 39]]);
}

function saveApplicationFormShieldBefore(action) {
  var callServer, application, shieldPolicyNumber, currentStep, actionType, callback, changedValues, exit, handleResponse, response, profileList;
  return regeneratorRuntime.wrap(function saveApplicationFormShieldBefore$(_context27) {
    while (1) {
      switch (_context27.prev = _context27.next) {
        case 0:
          _context27.prev = 0;
          _context27.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context27.sent;
          _context27.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context27.sent;
          _context27.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.shieldPolicyNumber;
          });

        case 9:
          shieldPolicyNumber = _context27.sent;
          currentStep = action.currentStep, actionType = action.actionType, callback = action.callback;
          changedValues = application.applicationForm.values;
          exit = true;
          handleResponse = true;
          _context27.t0 = actionType;
          _context27.next = _context27.t0 === SWITCHMENU ? 17 : _context27.t0 === OPENSUPPORTINGDOCUMENT ? 19 : _context27.t0 === CLOSEAPPLICATION ? 21 : 30;
          break;

        case 17:
          exit = false;
          return _context27.abrupt("break", 31);

        case 19:
          if (currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION]) {
            exit = false;
            handleResponse = false;
          }
          return _context27.abrupt("break", 31);

        case 21:
          if (!(currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.PAYMENT] || currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SUBMISSION])) {
            _context27.next = 28;
            break;
          }

          _context27.next = 24;
          return (0, _effects.call)(paymentAndSubmission.updatePaymentMethodsShield);

        case 24:
          _context27.next = 26;
          return (0, _effects.call)(paymentAndSubmission.saveSubmissionShield);

        case 26:
          _context27.next = 29;
          break;

        case 28:
          if (currentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.APPLICATION]) {
            exit = false;
          }

        case 29:
          return _context27.abrupt("break", 31);

        case 30:
          return _context27.abrupt("break", 31);

        case 31:
          if (exit) {
            _context27.next = 44;
            break;
          }

          _context27.next = 34;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "saveAppForm",
              appId: application.id,
              policyNumber: shieldPolicyNumber,
              changedValues: changedValues
            }
          });

        case 34:
          response = _context27.sent;

          if (!(response.success && handleResponse)) {
            _context27.next = 44;
            break;
          }

          _.set(response, "stepper.index.current", currentStep);
          _context27.next = 39;
          return (0, _effects.call)(initEappStoreForShield, response);

        case 39:
          if (!response.updIds) {
            _context27.next = 44;
            break;
          }

          profileList = (0, _application.updateProfileList)({
            updIds: response.updIds,
            application: application
          });

          if (!(profileList.length > 0)) {
            _context27.next = 44;
            break;
          }

          _context27.next = 44;
          return client.updateProfile({ profileList: profileList });

        case 44:
          if (_.isFunction(callback)) {
            callback();
          }
          _context27.next = 49;
          break;

        case 47:
          _context27.prev = 47;
          _context27.t1 = _context27["catch"](0);

        case 49:
        case "end":
          return _context27.stop();
      }
    }
  }, _marked27, this, [[0, 47]]);
}

function updatePolicyNumber(action) {
  var application, policyNumber, isShield, callback, newiCidMapping, policyNumberIndex;
  return regeneratorRuntime.wrap(function updatePolicyNumber$(_context28) {
    while (1) {
      switch (_context28.prev = _context28.next) {
        case 0:
          _context28.prev = 0;
          _context28.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context28.sent;
          policyNumber = action.policyNumber, isShield = action.isShield, callback = action.callback;

          if (!isShield) {
            _context28.next = 17;
            break;
          }

          newiCidMapping = _.cloneDeep(application.iCidMapping);
          policyNumberIndex = 0;

          Object.values(newiCidMapping).forEach(function (profile) {
            profile.forEach(function (value) {
              value.policyNumber = policyNumber[policyNumberIndex];
              policyNumberIndex += 1;
            });
          });
          _context28.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_SHIELD_POLICY_NUMBER,
            newShieldPolicyNumber: policyNumber
          });

        case 11:
          _context28.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: Object.assign({}, application, {
              iCidMapping: newiCidMapping
            })
          });

        case 13:
          _context28.next = 15;
          return (0, _effects.call)(saveApplicationFormShieldBefore, {
            currentStep: 0,
            actionType: SWITCHMENU
          });

        case 15:
          _context28.next = 19;
          break;

        case 17:
          _context28.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_POLICY_NUMBER,
            newPolicuNumnber: policyNumber[0]
          });

        case 19:
          if (_.isFunction(callback)) {
            callback();
          }
          _context28.next = 24;
          break;

        case 22:
          _context28.prev = 22;
          _context28.t0 = _context28["catch"](0);

        case 24:
        case "end":
          return _context28.stop();
      }
    }
  }, _marked28, this, [[0, 22]]);
}

function applyApplicationForm(action) {
  var isShield;
  return regeneratorRuntime.wrap(function applyApplicationForm$(_context29) {
    while (1) {
      switch (_context29.prev = _context29.next) {
        case 0:
          isShield = action.isShield;

          if (!isShield) {
            _context29.next = 6;
            break;
          }

          _context29.next = 4;
          return applyApplicationFormShield(action);

        case 4:
          _context29.next = 8;
          break;

        case 6:
          _context29.next = 8;
          return applyApplicationFormNormal(action);

        case 8:
        case "end":
          return _context29.stop();
      }
    }
  }, _marked29, this);
}

function getApplicationsAfterSubmission(action) {
  var callServer, application, callback, resp;
  return regeneratorRuntime.wrap(function getApplicationsAfterSubmission$(_context30) {
    while (1) {
      switch (_context30.prev = _context30.next) {
        case 0:
          _context30.prev = 0;
          _context30.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context30.sent;
          _context30.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context30.sent;
          callback = action.callback;
          _context30.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "goApplication",
              id: application.id
            }
          });

        case 10:
          resp = _context30.sent;
          _context30.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: resp.application
          });

        case 13:
          _context30.next = 15;
          return (0, _effects.call)(paymentAndSubmission.getSubmission, { docId: application.id });

        case 15:

          if (_.isFunction(callback)) {
            callback();
          }
          _context30.next = 20;
          break;

        case 18:
          _context30.prev = 18;
          _context30.t0 = _context30["catch"](0);

        case 20:
        case "end":
          return _context30.stop();
      }
    }
  }, _marked30, this, [[0, 18]]);
}

function genFNA(action) {
  var cid, callServer, callback;
  return regeneratorRuntime.wrap(function genFNA$(_context31) {
    while (1) {
      switch (_context31.prev = _context31.next) {
        case 0:
          _context31.prev = 0;
          _context31.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 3:
          cid = _context31.sent;
          _context31.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context31.sent;
          callback = action.callback;
          _context31.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "genFNA",
              cid: cid
            }
          });

        case 10:
          if (_.isFunction(callback)) {
            callback();
          }
          _context31.next = 15;
          break;

        case 13:
          _context31.prev = 13;
          _context31.t0 = _context31["catch"](0);

        case 15:
        case "end":
          return _context31.stop();
      }
    }
  }, _marked31, this, [[0, 13]]);
}