"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRecommendation = getRecommendation;
exports.saveRecommendation = saveRecommendation;
exports.closeRecommendation = closeRecommendation;
exports.updateRecommendationStep = updateRecommendationStep;
exports.validateRecommendation = validateRecommendation;
exports.updateRecommendationData = updateRecommendationData;
exports.validateBudget = validateBudget;
exports.updateBudgetData = updateBudgetData;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _TOLERANCE_LEVEL = require("../constants/TOLERANCE_LEVEL");

var TOLERANCE_LEVEL = _interopRequireWildcard(_TOLERANCE_LEVEL);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _locales = require("../locales");

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getRecommendation),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(saveRecommendation),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(closeRecommendation),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updateRecommendationStep),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(validateRecommendation),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(updateRecommendationData),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(validateBudget),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(updateBudgetData);

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Support functions
 */

var prepareErrorObject = function prepareErrorObject(values, error) {
  var _message;

  var defaultError = {
    hasError: false,
    message: (_message = {}, _defineProperty(_message, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message)
  };

  _.forEach(values, function (value, key) {
    if (key !== "extra") {
      if (_.isObject(value)) {
        error[key] = {};
        prepareErrorObject(value, error[key]);
      } else {
        error[key] = defaultError;
      }
    }
  });
};

var checkDataHasError = function checkDataHasError(error) {
  var hasError = false;

  _.forEach(error, function (err) {
    if (_.isObject(err)) {
      if (_.has(err, "hasError")) {
        hasError = hasError || err.hasError;
      } else {
        hasError = hasError || checkDataHasError(err);
      }
    }
  });
  return hasError;
};

var validateRecommendationNormalData = function validateRecommendationNormalData(_ref) {
  var value = _ref.value,
      error = _ref.error,
      isChosen = _ref.isChosen,
      disabled = _ref.disabled;

  var newError = _.cloneDeep(error);
  var hasRop = _.get(value, "rop.choiceQ1") === "Y";

  _.set(newError, "reason", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "reason")
  }));
  _.set(newError, "benefit", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "benefit")
  }));
  _.set(newError, "limitation", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "limitation")
  }));
  if (isChosen) {
    _.set(newError, "rop.choiceQ1", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: true,
        disabled: disabled
      },
      value: _.get(value, "rop.choiceQ1")
    }));

    _.set(newError, "rop.choiceQ1Sub1", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: hasRop,
        disabled: disabled
      },
      value: _.get(value, "rop.choiceQ1Sub1")
    }));
    _.set(newError, "rop.choiceQ1Sub2", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: hasRop,
        disabled: disabled
      },
      value: _.get(value, "rop.choiceQ1Sub2")
    }));
    _.set(newError, "rop.choiceQ1Sub3", validation.validateMandatory({
      field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: hasRop, disabled: disabled },
      value: _.get(value, "rop.choiceQ1Sub3")
    }));
    _.set(newError, "rop.replaceLife", validation.validateRecommendationROP({
      field: { mandatory: hasRop, disabled: disabled },
      value: value
    }));
  }

  return newError;
};

var validateRecommendationShieldData = function validateRecommendationShieldData(_ref2) {
  var value = _ref2.value,
      error = _ref2.error,
      isChosen = _ref2.isChosen,
      disabled = _ref2.disabled;

  // prepare new state
  var newError = _.cloneDeep(error);
  var hasRop = _.get(value, "rop_shield.ropBlock.shieldRopAnswer_0") === "Y" || _.get(value, "rop_shield.ropBlock.shieldRopAnswer_1") === "Y" || _.get(value, "rop_shield.ropBlock.shieldRopAnswer_2") === "Y" || _.get(value, "rop_shield.ropBlock.shieldRopAnswer_3") === "Y" || _.get(value, "rop_shield.ropBlock.shieldRopAnswer_4") === "Y" || _.get(value, "rop_shield.ropBlock.shieldRopAnswer_5") === "Y";

  _.set(newError, "reason", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "reason")
  }));
  _.set(newError, "benefit", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "benefit")
  }));
  _.set(newError, "limitation", validation.validateMandatory({
    field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
    value: _.get(value, "limitation")
  }));
  if (isChosen) {
    var shieldRopAnswers = _.values(_.get(value, "rop_shield.ropBlock.iCidRopAnswerMap"));
    _.forEach(shieldRopAnswers, function (answer) {
      _.set(newError, "rop_shield.ropBlock." + answer, validation.validateMandatory({
        field: {
          type: _FIELD_TYPES.TEXT_SELECTION,
          mandatory: true,
          disabled: disabled
        },
        value: _.get(value, "rop_shield.ropBlock." + answer)
      }));
    });

    _.set(newError, "rop_shield.ropBlock.ropQ2", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: hasRop,
        disabled: disabled
      },
      value: _.get(value, "rop_shield.ropBlock.ropQ2")
    }));
    _.set(newError, "rop_shield.ropBlock.ropQ3", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: hasRop,
        disabled: disabled
      },
      value: _.get(value, "rop_shield.ropBlock.ropQ3")
    }));
    _.set(newError, "rop_shield.ropBlock.ropQ1sub3", validation.validateMandatory({
      field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: hasRop, disabled: disabled },
      value: _.get(value, "rop_shield.ropBlock.ropQ1sub3")
    }));
  }
  return newError;
};

var validateBudgetData = function validateBudgetData(_ref3) {
  var value = _ref3.value,
      error = _ref3.error,
      disabled = _ref3.disabled;

  var newError = _.cloneDeep(error);

  var showBudgetLess = _.get(value, "spCompare") > TOLERANCE_LEVEL.POSITIVE || _.get(value, "rpCompare") > TOLERANCE_LEVEL.POSITIVE || _.get(value, "cpfOaCompare") > TOLERANCE_LEVEL.POSITIVE || _.get(value, "cpfSaCompare") > TOLERANCE_LEVEL.POSITIVE || _.get(value, "srsCompare") > TOLERANCE_LEVEL.POSITIVE || _.get(value, "cpfMsCompare") > TOLERANCE_LEVEL.POSITIVE;

  var showBudgetMore = _.get(value, "spCompare") < TOLERANCE_LEVEL.NEGATIVE || _.get(value, "rpCompare") < TOLERANCE_LEVEL.NEGATIVE || _.get(value, "cpfOaCompare") < TOLERANCE_LEVEL.NEGATIVE || _.get(value, "cpfSaCompare") < TOLERANCE_LEVEL.NEGATIVE || _.get(value, "srsCompare") < TOLERANCE_LEVEL.NEGATIVE || _.get(value, "cpfMsCompare") < TOLERANCE_LEVEL.NEGATIVE;

  if (showBudgetMore) {
    _.set(newError, "budgetMoreChoice", validation.validateBudgetChoice({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: true,
        disabled: disabled
      },
      value: _.get(value, "budgetMoreChoice")
    }));
    _.set(newError, "budgetMoreReason", validation.validateMandatory({
      field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
      value: _.get(value, "budgetMoreReason")
    }));
  }

  if (showBudgetLess) {
    _.set(newError, "budgetLessChoice", validation.validateBudgetChoice({
      field: {
        type: _FIELD_TYPES.TEXT_SELECTION,
        mandatory: true,
        disabled: disabled
      },
      value: _.get(value, "budgetLessChoice")
    }));
    _.set(newError, "budgetLessReason", validation.validateMandatory({
      field: { type: _FIELD_TYPES.TEXT_BOX, mandatory: true, disabled: disabled },
      value: _.get(value, "budgetLessReason")
    }));
  }

  return newError;
};

/**
 * Saga functions
 */
function getRecommendation() {
  var choiceList, profile, callServer, resp, completedStep, rError, bError, error, component;
  return regeneratorRuntime.wrap(function getRecommendation$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].recommendation.choiceList;
          });

        case 3:
          choiceList = _context.sent;
          _context.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context.sent;
          _context.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 9:
          callServer = _context.sent;
          _context.next = 12;
          return (0, _effects.call)(callServer, {
            url: "clientChoice",
            data: {
              method: "post",
              action: "clientChoice",
              quotCheckedList: choiceList,
              clientId: profile.cid
            }
          });

        case 12:
          resp = _context.sent;
          completedStep = -1;

          if (resp.isRecommendationCompleted) {
            completedStep += 1;
            if (resp.isBudgetCompleted) {
              completedStep += 1;
              if (resp.isAcceptanceCompleted || resp.clientChoiceStep > 1) {
                completedStep += 1;
              }
            }
          }

          // prepare error object for validation
          rError = {};
          bError = {};

          _.forEach(resp.recommendation, function (value, key) {
            if (["chosenList", "notChosenList"].indexOf(key) < 0) {
              rError[key] = {};
              prepareErrorObject(value, rError[key]);
            }
          });
          prepareErrorObject(resp.budget, bError);

          error = {
            recommendation: rError,
            budget: bError,
            acceptance: {}
          };

          // prepare component object

          component = {
            completedStep: completedStep,
            currentStep: resp.clientChoiceStep,
            completedSection: {
              recommendation: {},
              budget: resp.isBudgetCompleted
            },
            showDetails: false,
            disabled: resp.isReadOnly
          };

          _.forEach(resp.recommendation, function (value, key) {
            if (["chosenList", "notChosenList"].indexOf(key) < 0) {
              component.completedSection.recommendation[key] = resp.isRecommendationCompleted;
            }
          });

          _context.next = 24;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION,
            recommendation: resp.recommendation,
            budget: resp.budget,
            acceptance: resp.acceptance,
            error: error,
            component: component
          });

        case 24:
          _context.next = 28;
          break;

        case 26:
          _context.prev = 26;
          _context.t0 = _context["catch"](0);

        case 28:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 26]]);
}

function saveRecommendation(action) {
  var profile, recommendation, budget, acceptance, component, callServer, toSaveRecommendation, resp;
  return regeneratorRuntime.wrap(function saveRecommendation$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 3:
          profile = _context2.sent;
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].recommendation;
          });

        case 6:
          recommendation = _context2.sent;
          _context2.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].budget;
          });

        case 9:
          budget = _context2.sent;
          _context2.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].acceptance;
          });

        case 12:
          acceptance = _context2.sent;
          _context2.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 15:
          component = _context2.sent;
          _context2.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 18:
          callServer = _context2.sent;
          toSaveRecommendation = _.cloneDeep(recommendation);

          _.forEach(toSaveRecommendation, function (value, key) {
            if (["chosenList", "notChosenList"].indexOf(key) < 0) {
              _.set(value, "extra.isCompleted", component.completedSection.recommendation[key]);
            }
          });

          _context2.next = 23;
          return (0, _effects.call)(callServer, {
            url: "clientChoice",
            data: {
              method: "post",
              action: "updateQuotationClientChoice",
              clientId: profile.cid,
              values: {
                recommendation: toSaveRecommendation,
                budget: budget,
                acceptance: acceptance
              },
              stepperIndex: action.step,
              isSaveOnly: action.isSaveOnly
            }
          });

        case 23:
          resp = _context2.sent;
          _context2.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION,
            recommendation: !_.isEmpty(resp.recommendation) ? resp.recommendation : recommendation,
            budget: !_.isEmpty(resp.budget) ? resp.budget : budget,
            acceptance: !_.isEmpty(resp.acceptance) ? resp.acceptance : acceptance
          });

        case 26:
          _context2.next = 30;
          break;

        case 28:
          _context2.prev = 28;
          _context2.t0 = _context2["catch"](0);

        case 30:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 28]]);
}

function closeRecommendation() {
  var component;
  return regeneratorRuntime.wrap(function closeRecommendation$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 3:
          component = _context3.sent;
          return _context3.delegateYield(saveRecommendation({
            step: component.currentStep,
            isSaveOnly: true
          }), "t0", 5);

        case 5:
          _context3.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION
          });

        case 7:
          _context3.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_RECOMMENDATION_COMPLETED,
            completed: component.completedStep === 2
          });

        case 9:
          _context3.next = 13;
          break;

        case 11:
          _context3.prev = 11;
          _context3.t1 = _context3["catch"](0);

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 11]]);
}

function updateRecommendationStep(action) {
  var component, isSaveOnly, newComponent;
  return regeneratorRuntime.wrap(function updateRecommendationStep$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 3:
          component = _context4.sent;
          isSaveOnly = component.completedStep > 1 || action.newCurrentStep <= component.currentStep;
          newComponent = _.cloneDeep(component);

          if (newComponent.completedStep === 1 && action.newCurrentStep > 1) {
            newComponent.completedStep = action.newCurrentStep;
          }
          newComponent.currentStep = action.newCurrentStep;

          return _context4.delegateYield(saveRecommendation({
            step: component.currentStep,
            isSaveOnly: isSaveOnly
          }), "t0", 9);

        case 9:
          _context4.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
            component: newComponent
          });

        case 11:
          _context4.next = 15;
          break;

        case 13:
          _context4.prev = 13;
          _context4.t1 = _context4["catch"](0);

        case 15:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 13]]);
}

function validateRecommendation(action) {
  var init, recommendation, error, component, disabled, newError, newComponent;
  return regeneratorRuntime.wrap(function validateRecommendation$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          init = action.init;

          // get state

          _context5.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].recommendation;
          });

        case 4:
          recommendation = _context5.sent;
          _context5.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].error;
          });

        case 7:
          error = _context5.sent;
          _context5.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 10:
          component = _context5.sent;
          disabled = component.disabled;

          // prepare new state

          newError = _.cloneDeep(error);
          newComponent = _.cloneDeep(component);


          _.forEach(recommendation, function (value, quotId) {
            if (["chosenList", "notChosenList"].indexOf(quotId) < 0) {
              var isCompleted = false;
              var isChosen = recommendation.chosenList.indexOf(quotId) >= 0;
              var errorByQuotId = _.get(error, "recommendation[" + quotId + "]", {});
              var newErrorByQuotId = _.get(value, "extra.isShield") ? validateRecommendationShieldData({
                value: value,
                error: errorByQuotId,
                isChosen: isChosen,
                disabled: disabled
              }) : validateRecommendationNormalData({
                value: value,
                error: errorByQuotId,
                isChosen: isChosen,
                disabled: disabled
              });
              newError.recommendation[quotId] = newErrorByQuotId;
              var hasError = checkDataHasError(newErrorByQuotId);
              newComponent.completedSection.recommendation[quotId] = !hasError;
              if (component.completedStep <= 0) {
                isCompleted = _.values(newComponent.completedSection.recommendation).indexOf(false) < 0;
                newComponent.completedStep = isCompleted ? 0 : -1;
              }
            }
          });

          if (init) {
            newComponent.selectedQuotId = _.get(recommendation, "chosenList[0]", "");
          }

          _context5.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
            error: newError
          });

        case 18:
          _context5.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
            component: newComponent
          });

        case 20:
          _context5.next = 24;
          break;

        case 22:
          _context5.prev = 22;
          _context5.t0 = _context5["catch"](0);

        case 24:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 22]]);
}

function updateRecommendationData(action) {
  var quotationId, newValue, recommendation, error, component, isChosen, disabled, errorByQuotId, newRecommendation, newError, newComponent, newErrorByQuotId, isCompleted, hasError;
  return regeneratorRuntime.wrap(function updateRecommendationData$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          quotationId = action.quotationId, newValue = action.newValue;

          // get state

          _context6.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].recommendation;
          });

        case 4:
          recommendation = _context6.sent;
          _context6.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].error;
          });

        case 7:
          error = _context6.sent;
          _context6.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 10:
          component = _context6.sent;
          isChosen = recommendation.chosenList.indexOf(quotationId) >= 0;
          disabled = component.disabled;
          errorByQuotId = _.get(error, "recommendation[" + quotationId + "]", {});

          // prepare new state

          newRecommendation = _.cloneDeep(recommendation);
          newError = _.cloneDeep(error);
          newComponent = _.cloneDeep(component);
          newErrorByQuotId = {};
          isCompleted = false;

          // perform validation

          if (_.get(recommendation, "[" + quotationId + "].extra.isShield")) {
            newErrorByQuotId = validateRecommendationShieldData({
              value: newValue,
              error: errorByQuotId,
              isChosen: isChosen,
              disabled: disabled
            });
          } else {
            newErrorByQuotId = validateRecommendationNormalData({
              value: newValue,
              error: errorByQuotId,
              isChosen: isChosen,
              disabled: disabled
            });
          }

          newRecommendation[quotationId] = newValue;
          newError.recommendation[quotationId] = newErrorByQuotId;

          hasError = checkDataHasError(newErrorByQuotId);

          newComponent.completedSection.recommendation[quotationId] = !hasError;
          isCompleted = _.values(newComponent.completedSection.recommendation).indexOf(false) < 0;

          if (isCompleted) {
            if (component.completedStep <= 0) {
              newComponent.completedStep = 0;
            }
          } else {
            newComponent.completedStep = -1;
          }

          // dispatch
          _context6.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DATA,
            recommendation: newRecommendation
          });

        case 28:
          _context6.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
            error: newError
          });

        case 30:
          _context6.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
            component: newComponent
          });

        case 32:
          _context6.next = 36;
          break;

        case 34:
          _context6.prev = 34;
          _context6.t0 = _context6["catch"](0);

        case 36:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 34]]);
}

function validateBudget() {
  var budget, error, component, disabled, newError, newComponent, newBudgetError, hasError;
  return regeneratorRuntime.wrap(function validateBudget$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].budget;
          });

        case 3:
          budget = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].error;
          });

        case 6:
          error = _context7.sent;
          _context7.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 9:
          component = _context7.sent;
          disabled = component.disabled;

          // prepare new state

          newError = _.cloneDeep(error);
          newComponent = _.cloneDeep(component);
          newBudgetError = validateBudgetData({
            value: budget,
            error: _.get(error, "budget", {}),
            disabled: disabled
          });


          newError.budget = newBudgetError;
          hasError = checkDataHasError(newBudgetError);


          newComponent.completedSection.budget = !hasError;

          if (component.completedStep >= 0 && component.completedStep <= 1) {
            newComponent.completedStep = newComponent.completedSection.budget ? 1 : 0;
          }

          _context7.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_BUDGET_ERROR,
            error: newError
          });

        case 20:
          _context7.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
            component: newComponent
          });

        case 22:
          _context7.next = 26;
          break;

        case 24:
          _context7.prev = 24;
          _context7.t0 = _context7["catch"](0);

        case 26:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 24]]);
}

function updateBudgetData(action) {
  var newValue, error, component, disabled, newError, newComponent, newBudgetError, hasError;
  return regeneratorRuntime.wrap(function updateBudgetData$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          newValue = action.newValue;

          // get state

          _context8.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].error;
          });

        case 4:
          error = _context8.sent;
          _context8.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.RECOMMENDATION].component;
          });

        case 7:
          component = _context8.sent;
          disabled = component.disabled;

          // prepare new value

          newError = _.cloneDeep(error);
          newComponent = _.cloneDeep(component);
          newBudgetError = validateBudgetData({
            value: newValue,
            error: _.get(error, "budget", {}),
            disabled: disabled
          });


          newError.budget = newBudgetError;
          hasError = checkDataHasError(newBudgetError);


          newComponent.completedSection.budget = !hasError;

          if (component.completedStep >= 0 && component.completedStep <= 1) {
            newComponent.completedStep = newComponent.completedSection.budget ? 1 : 0;
          }

          // dispatch
          _context8.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_BUDGET_DATA,
            budget: newValue
          });

        case 18:
          _context8.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR,
            error: newError
          });

        case 20:
          _context8.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT,
            component: newComponent
          });

        case 22:
          _context8.next = 26;
          break;

        case 24:
          _context8.prev = 24;
          _context8.t0 = _context8["catch"](0);

        case 26:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 24]]);
}