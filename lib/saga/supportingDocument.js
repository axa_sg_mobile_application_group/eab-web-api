"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.validateSupportingDocument = validateSupportingDocument;
exports.getSupportingDocument = getSupportingDocument;
exports.saveSupportingDocument = saveSupportingDocument;
exports.updateSupportingDocumentData = updateSupportingDocumentData;
exports.addOtherSupportingDocument = addOtherSupportingDocument;
exports.fileUpload = fileUpload;
exports.deleteFile = deleteFile;
exports.deleteOtherDocumentSection = deleteOtherDocumentSection;
exports.editOtherDocumentSection = editOtherDocumentSection;
exports.getSupportingDocumentShield = getSupportingDocumentShield;
exports.AdditionalDocumentNotification = AdditionalDocumentNotification;
exports.saveSubmittedDocument = saveSubmittedDocument;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _EAPP = require("../constants/EAPP");

var _EAPP2 = _interopRequireDefault(_EAPP);

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(validateSupportingDocument),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(getSupportingDocument),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(saveSupportingDocument),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updateSupportingDocumentData),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(addOtherSupportingDocument),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(fileUpload),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(deleteFile),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(deleteOtherDocumentSection),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(editOtherDocumentSection),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(getSupportingDocumentShield),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(AdditionalDocumentNotification),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(saveSubmittedDocument);

function validateSupportingDocument() {
  var supportingDocument, newError, isMandDocsAllUploaded;
  return regeneratorRuntime.wrap(function validateSupportingDocument$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 3:
          supportingDocument = _context.sent;
          newError = {};

          Object.entries(supportingDocument.template).forEach(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                profileName = _ref2[0],
                template = _ref2[1];

            var haveMandDocs = false;
            if (template.tabContent) {
              if (template.tabContent.mandDocs) {
                template.tabContent.mandDocs.items.forEach(function (item) {
                  _.set(newError, profileName + "." + item.id, {});
                });
              }
            } else {
              template.some(function (value) {
                if (value.id === "mandDocs") {
                  haveMandDocs = true;
                  value.items.forEach(function (item) {
                    _.set(newError, profileName + "." + item.id, {});
                  });
                }
                return haveMandDocs;
              });
            }
          });
          isMandDocsAllUploaded = true;

          Object.entries(newError).forEach(function (_ref3) {
            var _ref4 = _slicedToArray(_ref3, 2),
                profileName = _ref4[0],
                content = _ref4[1];

            Object.keys(content).forEach(function (fieldName) {
              var result = validation.validateMandatory({
                field: {
                  type: _FIELD_TYPES.TEXT_FIELD,
                  mandatory: true,
                  disabled: false
                },
                value: supportingDocument.values[profileName].mandDocs[fieldName].length ? 1 : null
              });
              if (result.hasError) {
                isMandDocsAllUploaded = false;
              }
              _.set(newError, profileName + "." + fieldName, result);
            });
          });

          _context.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].VALIDATE_SUPPORTING_DOCUMENT,
            newError: newError
          });

        case 10:
          if (isMandDocsAllUploaded) {
            _context.next = 13;
            break;
          }

          _context.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON,
            newIsMandDocsAllUploaded: false
          });

        case 13:
          _context.next = 17;
          break;

        case 15:
          _context.prev = 15;
          _context.t0 = _context["catch"](0);

        case 17:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 15]]);
}

function getSupportingDocument(action) {
  var callServer, appId, appStatus, callback, res;
  return regeneratorRuntime.wrap(function getSupportingDocument$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          appId = action.appId, appStatus = action.appStatus, callback = action.callback;
          _context2.next = 7;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getSupportDocuments",
              applicationId: appId,
              appStatus: appStatus
            }
          });

        case 7:
          res = _context2.sent;

          if (!res.success) {
            _context2.next = 16;
            break;
          }

          _context2.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: res
          });

        case 11:
          if (!(appStatus === _EAPP2.default.appStatus.SUBMITTED)) {
            _context2.next = 14;
            break;
          }

          _context2.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT,
            newApplication: res.application
          });

        case 14:
          _context2.next = 16;
          return (0, _effects.call)(validateSupportingDocument);

        case 16:
          if (_.isFunction(callback)) {
            callback();
          }
          _context2.next = 21;
          break;

        case 19:
          _context2.prev = 19;
          _context2.t0 = _context2["catch"](0);

        case 21:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 19]]);
}

function saveSupportingDocument(action) {
  var callServer, supportingDocument, appId, callback, response;
  return regeneratorRuntime.wrap(function saveSupportingDocument$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context3.sent;
          _context3.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context3.sent;
          appId = action.appId, callback = action.callback;
          _context3.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "closeSuppDocs",
              template: supportingDocument.template,
              values: supportingDocument.values,
              appId: appId
            }
          });

        case 10:
          response = _context3.sent;

          if (!response) {
            _context3.next = 16;
            break;
          }

          _context3.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newSubmission: response
          });

        case 14:
          _context3.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON,
            newIsMandDocsAllUploaded: response.isMandDocsAllUploaded
          });

        case 16:
          if (_.isFunction(callback)) {
            callback();
          }
          // TODO the response stored some ui template that maybe need to handle
          _context3.next = 21;
          break;

        case 19:
          _context3.prev = 19;
          _context3.t0 = _context3["catch"](0);

        case 21:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 19]]);
}

function updateSupportingDocumentData(action) {
  var supportingDocument, path, newValue;
  return regeneratorRuntime.wrap(function updateSupportingDocumentData$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 3:
          supportingDocument = _context4.sent;
          path = action.path, newValue = action.newValue;

          _.set(supportingDocument, path, newValue);
          _context4.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: supportingDocument
          });

        case 8:
          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context4.next = 13;
          break;

        case 11:
          _context4.prev = 11;
          _context4.t0 = _context4["catch"](0);

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 11]]);
}

function addOtherSupportingDocument(action) {
  var callServer, supportingDocument, appId, docName, docNameOption, tabId, res;
  return regeneratorRuntime.wrap(function addOtherSupportingDocument$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context5.sent;
          appId = action.appId, docName = action.docName, docNameOption = action.docNameOption, tabId = action.tabId;
          _context5.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "addOtherDocument",
              values: supportingDocument.values[tabId],
              docName: docName,
              docNameOption: docNameOption,
              appId: appId,
              tabId: tabId,
              rootValues: supportingDocument.values
            }
          });

        case 10:
          res = _context5.sent;

          if (!res.success) {
            _context5.next = 15;
            break;
          }

          supportingDocument.values = res.values;
          _context5.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: Object.assign({}, supportingDocument)
          });

        case 15:
          _context5.next = 19;
          break;

        case 17:
          _context5.prev = 17;
          _context5.t0 = _context5["catch"](0);

        case 19:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 17]]);
}

function fileUpload(action) {
  var callServer, supportingDocument, viewedList, attachment, applicationId, attachmentValues, valueLocation, tabId, isSupervisorChannel, callback, res;
  return regeneratorRuntime.wrap(function fileUpload$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context6.sent;
          _context6.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].viewedList;
          });

        case 9:
          viewedList = _context6.sent;
          attachment = action.attachment, applicationId = action.applicationId, attachmentValues = action.attachmentValues, valueLocation = action.valueLocation, tabId = action.tabId, isSupervisorChannel = action.isSupervisorChannel, callback = action.callback;
          _context6.next = 13;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "upsertSuppDocsFile",
              attachment: attachment,
              applicationId: applicationId,
              attachmentValues: attachmentValues,
              valueLocation: valueLocation,
              tabId: tabId,
              suppDocsValues: supportingDocument.values[tabId],
              viewedList: viewedList,
              isSupervisorChannel: isSupervisorChannel,
              rootValues: supportingDocument.values
            }
          });

        case 13:
          res = _context6.sent;

          if (!res.success) {
            _context6.next = 25;
            break;
          }

          if (res.showTotalFilesSizeLargeDialog) {
            supportingDocument.exceededMaximumFileSize = true;
          } else {
            supportingDocument.values = res.values;
            supportingDocument.exceededMaximumFileSize = false;
          }
          _context6.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST,
            newPendingSubmitList: res.pendingSubmitList || []
          });

        case 18:
          _context6.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: Object.assign({}, supportingDocument)
          });

        case 20:
          _context6.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_VIEWED_LIST_DATA,
            newViewedList: res.viewedList
          });

        case 22:
          _context6.next = 24;
          return (0, _effects.call)(validateSupportingDocument);

        case 24:
          if (_.isFunction(callback)) {
            callback();
          }

        case 25:
          _context6.next = 29;
          break;

        case 27:
          _context6.prev = 27;
          _context6.t0 = _context6["catch"](0);

        case 29:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 27]]);
}

function deleteFile(action) {
  var callServer, supportingDocument, applicationId, attachmentId, valueLocation, tabId, res;
  return regeneratorRuntime.wrap(function deleteFile$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context7.sent;
          applicationId = action.applicationId, attachmentId = action.attachmentId, valueLocation = action.valueLocation, tabId = action.tabId;
          _context7.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "deleteSupportDocumentFile",
              applicationId: applicationId,
              attachmentId: attachmentId,
              valueLocation: valueLocation,
              tabId: tabId
            }
          });

        case 10:
          res = _context7.sent;

          if (!res.success) {
            _context7.next = 17;
            break;
          }

          supportingDocument.values = res.supportDocuments.values;
          _context7.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: Object.assign({}, supportingDocument)
          });

        case 15:
          _context7.next = 17;
          return (0, _effects.call)(validateSupportingDocument);

        case 17:
          _context7.next = 21;
          break;

        case 19:
          _context7.prev = 19;
          _context7.t0 = _context7["catch"](0);

        case 21:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 19]]);
}

function deleteOtherDocumentSection(action) {
  var callServer, supportingDocument, applicationId, documentId, tabId, res;
  return regeneratorRuntime.wrap(function deleteOtherDocumentSection$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context8.sent;
          _context8.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context8.sent;
          applicationId = action.applicationId, documentId = action.documentId, tabId = action.tabId;
          _context8.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "deleteSupportDocument",
              applicationId: applicationId,
              documentId: documentId,
              tabId: tabId
            }
          });

        case 10:
          res = _context8.sent;

          supportingDocument.values = res.values;
          _context8.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: Object.assign({}, supportingDocument)
          });

        case 14:
          _context8.next = 18;
          break;

        case 16:
          _context8.prev = 16;
          _context8.t0 = _context8["catch"](0);

        case 18:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 16]]);
}

function editOtherDocumentSection(action) {
  var callServer, supportingDocument, applicationId, documentId, tabId, docName, res;
  return regeneratorRuntime.wrap(function editOtherDocumentSection$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context9.sent;
          applicationId = action.applicationId, documentId = action.documentId, tabId = action.tabId, docName = action.docName;
          _context9.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "editSupportDocument",
              applicationId: applicationId,
              docName: docName,
              documentId: documentId,
              tabId: tabId
            }
          });

        case 10:
          res = _context9.sent;

          supportingDocument.values = res.values;
          _context9.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: Object.assign({}, supportingDocument)
          });

        case 14:
          _context9.next = 18;
          break;

        case 16:
          _context9.prev = 16;
          _context9.t0 = _context9["catch"](0);

        case 18:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 16]]);
}

function getSupportingDocumentShield(action) {
  var callServer, appId, appStatus, callback, res;
  return regeneratorRuntime.wrap(function getSupportingDocumentShield$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context10.sent;
          appId = action.appId, appStatus = action.appStatus, callback = action.callback;
          _context10.next = 7;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              action: "showSupportDocments",
              applicationId: appId,
              appStatus: appStatus
            }
          });

        case 7:
          res = _context10.sent;

          if (!res.success) {
            _context10.next = 16;
            break;
          }

          _context10.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
            newSupportingDocument: res
          });

        case 11:
          if (!(appStatus === _EAPP2.default.appStatus.SUBMITTED)) {
            _context10.next = 14;
            break;
          }

          _context10.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT,
            newApplication: res.application
          });

        case 14:
          _context10.next = 16;
          return (0, _effects.call)(validateSupportingDocument);

        case 16:
          if (_.isFunction(callback)) {
            callback();
          }
          _context10.next = 21;
          break;

        case 19:
          _context10.prev = 19;
          _context10.t0 = _context10["catch"](0);

        case 21:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 19]]);
}

function AdditionalDocumentNotification(action) {
  var callServer, agentId, authToken, webServiceUrl, isHealthDeclaration, appId;
  return regeneratorRuntime.wrap(function AdditionalDocumentNotification$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context11.sent;
          _context11.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT].agentCode;
          });

        case 6:
          agentId = _context11.sent;
          _context11.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 9:
          authToken = _context11.sent;
          _context11.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 12:
          webServiceUrl = _context11.sent;
          isHealthDeclaration = action.isHealthDeclaration;
          _context11.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application.policyNumber;
          });

        case 16:
          appId = _context11.sent;
          _context11.next = 19;
          return (0, _effects.call)(callServer, {
            url: "approvalNotification",
            data: {
              action: "AdditionalDocumentNotification",
              id: appId,
              agentId: agentId,
              isHealthDeclaration: isHealthDeclaration,
              authToken: authToken,
              webServiceUrl: webServiceUrl
            }
          });

        case 19:
          _context11.next = 23;
          break;

        case 21:
          _context11.prev = 21;
          _context11.t0 = _context11["catch"](0);

        case 23:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 21]]);
}

function saveSubmittedDocument(action) {
  var callServer, supportingDocument, viewedList, appId, isSupervisorChannel, callback, res;
  return regeneratorRuntime.wrap(function saveSubmittedDocument$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          _context12.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context12.sent;
          _context12.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument;
          });

        case 6:
          supportingDocument = _context12.sent;
          _context12.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SUPPORTING_DOCUMENT].viewedList;
          });

        case 9:
          viewedList = _context12.sent;
          appId = action.appId, isSupervisorChannel = action.isSupervisorChannel, callback = action.callback;
          _context12.next = 13;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "submitSuppDocsFiles",
              appId: appId,
              values: supportingDocument.values,
              viewedList: viewedList,
              isSupervisorChannel: isSupervisorChannel
            }
          });

        case 13:
          res = _context12.sent;

          if (!res.success) {
            _context12.next = 17;
            break;
          }

          _context12.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST,
            newPendingSubmitList: []
          });

        case 17:
          _context12.next = 19;
          return (0, _effects.call)(AdditionalDocumentNotification, {
            appId: appId,
            isHealthDeclaration: res.sendHealthDeclarationNoti
          });

        case 19:
          if (_.isFunction(callback)) {
            callback();
          }
          _context12.next = 24;
          break;

        case 22:
          _context12.prev = 22;
          _context12.t0 = _context12["catch"](0);

        case 24:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 22]]);
}