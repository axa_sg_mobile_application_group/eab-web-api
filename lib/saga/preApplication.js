"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getApplications = getApplications;
exports.deleteApplications = deleteApplications;
exports.updateUiSelectedList = updateUiSelectedList;
exports.updateRecommendChoiceList = updateRecommendChoiceList;
exports.openEmailDialog = openEmailDialog;
exports.sendEmail = sendEmail;
exports.loadClientProfileAndInitValidate = loadClientProfileAndInitValidate;
exports.checkBeforeApply = checkBeforeApply;
exports.confirmApplicationPaid = confirmApplicationPaid;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _PDF_TYPE = require("../constants/PDF_TYPE");

var PDF_TYPE = _interopRequireWildcard(_PDF_TYPE);

var _DIALOG_TYPES = require("../constants/DIALOG_TYPES");

var _common = require("../utilities/common");

var _dataMapping = require("../utilities/dataMapping");

var _validation = require("../utilities/validation");

var _preApplication = require("../utilities/preApplication");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getApplications),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(deleteApplications),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(updateUiSelectedList),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updateRecommendChoiceList),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(openEmailDialog),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(sendEmail),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(loadClientProfileAndInitValidate),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(checkBeforeApply),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(confirmApplicationPaid);

/**
 * support functions
 */

var getPILabelByQuotType = function getPILabelByQuotType(quotType) {
  var result = {};
  if (quotType === "SHIELD") {
    result.label = "Policy Illustration";
    result.fileName = "policy_illustration.pdf";
  } else {
    result.label = "Policy Illustration Document(s)";
    result.fileName = "policy_illustration_document(s).pdf";
  }
  return result;
};

/**
 * Saga functions
 */
function getApplications(action) {
  var callServer, profile, bundleId, resp;
  return regeneratorRuntime.wrap(function getApplications$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].START_LOADING
          });

        case 3:
          _context.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 5:
          callServer = _context.sent;
          _context.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 8:
          profile = _context.sent;
          bundleId = action.bundleId;
          _context.next = 12;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "getAppListView",
              profile: profile,
              bundleId: bundleId
            }
          });

        case 12:
          resp = _context.sent;
          _context.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST,
            productList: (0, _preApplication.prepareProductList)(resp.applicationsList),
            applicationsList: resp.applicationsList,
            pdaMembers: resp.pdaMembers,
            agentChannel: resp.agentChannel,
            recommendation: {
              completed: resp.clientChoiceFinished,
              choiceList: resp.quotCheckedList
            },
            newProductInvalidList: resp.productInvalidList
          });

        case 15:
          _context.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECTED_BUNDLE_ID,
            selectedBundleId: bundleId || (0, _preApplication.getValidBundleId)(profile)
          });

        case 17:
          _context.next = 21;
          break;

        case 19:
          _context.prev = 19;
          _context.t0 = _context["catch"](0);

        case 21:
          _context.prev = 21;
          _context.next = 24;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].FINISH_LOADING
          });

        case 24:
          return _context.finish(21);

        case 25:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 19, 21, 25]]);
}

function deleteApplications(action) {
  var callback, callServer, applicationsList, selectedIdList, resp;
  return regeneratorRuntime.wrap(function deleteApplications$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          callback = action.callback;
          _context2.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context2.sent;
          _context2.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].applicationsList;
          });

        case 7:
          applicationsList = _context2.sent;
          _context2.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].component.selectedIdList;
          });

        case 10:
          selectedIdList = _context2.sent;
          _context2.next = 13;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "deleteApplications",
              deleteItemsSelectedArray: selectedIdList,
              applicationsList: applicationsList
            }
          });

        case 13:
          resp = _context2.sent;
          _context2.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].DELETE_APPLICATION_LIST,
            productList: (0, _preApplication.prepareProductList)(resp.result),
            applicationsList: resp.result,
            recommendation: {
              completed: false,
              choiceList: resp.quotCheckedList
            }
          });

        case 16:
          if (!_.isFunction(callback)) {
            _context2.next = 19;
            break;
          }

          _context2.next = 19;
          return (0, _effects.call)(callback);

        case 19:
          _context2.next = 23;
          break;

        case 21:
          _context2.prev = 21;
          _context2.t0 = _context2["catch"](0);

        case 23:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 21]]);
}

function updateUiSelectedList(action) {
  var selectedIdList, _action$changeData, id, isSelected, newSelectedIdList, selectedIdIndex;

  return regeneratorRuntime.wrap(function updateUiSelectedList$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].component.selectedIdList;
          });

        case 3:
          selectedIdList = _context3.sent;
          _action$changeData = action.changeData, id = _action$changeData.id, isSelected = _action$changeData.isSelected;
          newSelectedIdList = _.cloneDeep(selectedIdList);
          selectedIdIndex = _.findIndex(selectedIdList, function (selectedId) {
            return selectedId === id;
          });

          if (isSelected && selectedIdIndex < 0) {
            newSelectedIdList.push(id);
          } else if (!isSelected && selectedIdIndex >= 0) {
            newSelectedIdList.splice(selectedIdIndex, 1);
          }

          _context3.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECTED_LIST,
            selectedIdList: newSelectedIdList
          });

        case 10:
          _context3.next = 14;
          break;

        case 12:
          _context3.prev = 12;
          _context3.t0 = _context3["catch"](0);

        case 14:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 12]]);
}

function updateRecommendChoiceList(action) {
  var choiceList, completed, _action$choiceData, id, checked, newChoiceList, choiceIndex, hasChange;

  return regeneratorRuntime.wrap(function updateRecommendChoiceList$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].recommendation.choiceList;
          });

        case 3:
          choiceList = _context4.sent;
          _context4.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].recommendation.completed;
          });

        case 6:
          completed = _context4.sent;
          _action$choiceData = action.choiceData, id = _action$choiceData.id, checked = _action$choiceData.checked;
          newChoiceList = _.cloneDeep(choiceList);
          choiceIndex = _.findIndex(newChoiceList, function (choice) {
            return choice.id === id;
          });

          if (choiceIndex >= 0) {
            hasChange = choiceList[choiceIndex].checked !== checked;

            if (hasChange) {
              completed = false;
            }
            newChoiceList[choiceIndex].checked = checked;
          }

          _context4.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_RECOMMENDATION_CHOICE_LIST,
            choiceList: newChoiceList,
            completed: completed
          });

        case 13:
          _context4.next = 17;
          break;

        case 15:
          _context4.prev = 15;
          _context4.t0 = _context4["catch"](0);

        case 17:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 15]]);
}

function openEmailDialog(action) {
  var callServer, profile, agent, preApplication, selectedApps, appList, proposalsMappingData, getAttKeysResp, getEmailResp, agentContent, clientContent, agentSubject, clientSubject, embedImgs, params, emails, attKeysMap, selectedAttachments, quotIds, options;
  return regeneratorRuntime.wrap(function openEmailDialog$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context5.sent;
          _context5.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT];
          });

        case 9:
          agent = _context5.sent;
          _context5.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION];
          });

        case 12:
          preApplication = _context5.sent;
          selectedApps = preApplication.component.selectedIdList;
          appList = preApplication.applicationsList;
          proposalsMappingData = {
            bi: PDF_TYPE.BI,
            prodSummary: PDF_TYPE.PROD_SUMMARY,
            FPXProdSummary: PDF_TYPE.FPX_PROD_SUMMARY,
            shield_product_summary: PDF_TYPE.SHIELD_PRODUCT_SUMMARY,
            phs: PDF_TYPE.PHS,
            fib: PDF_TYPE.FIB,
            fna: PDF_TYPE.FNA
          };
          _context5.next = 18;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getAppSumEmailAttKeys"
            }
          });

        case 18:
          getAttKeysResp = _context5.sent;
          _context5.next = 21;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getEmailTemplate"
            }
          });

        case 21:
          getEmailResp = _context5.sent;

          if (!(getAttKeysResp && getEmailResp && getEmailResp.success)) {
            _context5.next = 33;
            break;
          }

          agentContent = getEmailResp.agentContent, clientContent = getEmailResp.clientContent, agentSubject = getEmailResp.agentSubject, clientSubject = getEmailResp.clientSubject, embedImgs = getEmailResp.embedImgs;
          params = {
            agentName: agent.name,
            agentContactNum: agent.mobile,
            clientName: profile.fullName
          };
          emails = [];
          attKeysMap = getAttKeysResp.attKeysMap;
          selectedAttachments = {};
          quotIds = {};


          selectedApps.forEach(function (appKey) {
            var _$find = _.find(appList, function (app) {
              return app.id === appKey;
            }),
                baseProductCode = _$find.baseProductCode,
                baseProductName = _$find.baseProductName,
                quotationDocId = _$find.quotationDocId,
                id = _$find.id;

            var attKeysReMap = {};
            attKeysMap.forEach(function (item) {
              if (item.productCode.includes(baseProductCode)) {
                item.attKeys.forEach(function (attKey) {
                  attKeysReMap[attKey.id] = true;
                });
              }
            });
            selectedAttachments[appKey] = attKeysReMap;
            quotIds[appKey] = quotationDocId || id;

            var appListViewObj = _.find(appList, function (obj) {
              return obj.id === appKey;
            });
            var attachments = [];
            var attachmentsDetail = [];
            Object.keys(selectedAttachments[appKey]).forEach(function (attKey) {
              if (selectedAttachments[appKey][attKey] === true) {
                var title = attKey === "bi" ? _.get(getPILabelByQuotType(appListViewObj.quotType), "label") : proposalsMappingData[attKey];

                attachments.push(attKey);
                attachmentsDetail.push({
                  id: attKey,
                  title: title
                });
              }
            });
            emails.push({
              id: "agent_" + appKey,
              agentCode: agent.agentCode,
              dob: "",
              receiverType: "agent",
              to: [agent.email],
              title: agentSubject,
              content: (0, _common.getEmailContent)(agentContent, params, '<img src="cid:axaLogo">'),
              attachmentIds: attachments,
              attachmentsDetail: attachmentsDetail,
              embedImgs: embedImgs,
              quotId: quotIds[appKey],
              appId: appKey,
              baseProductName: baseProductName.en
            });
            emails.push({
              id: "client_" + appKey,
              agentCode: "",
              dob: profile.dob,
              receiverType: "client",
              to: [profile.email],
              title: clientSubject,
              content: (0, _common.getEmailContent)(clientContent, params, '<img src="cid:axaLogo">'),
              attachmentIds: attachments.slice(),
              attachmentsDetail: attachmentsDetail,
              embedImgs: embedImgs,
              quotId: quotIds[appKey],
              appId: appKey,
              baseProductName: baseProductName.en
            });
          });

          options = _.map(emails, function (email, index) {
            return email.attachmentsDetail.map(function (attachment) {
              return {
                key: attachment.id,
                id: email.id,
                index: index,
                text: attachment.title,
                isSelected: true
              };
            });
          });
          _context5.next = 33;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].OPEN_EMAIL,
            attKeysMap: getAttKeysResp.success ? attKeysMap : [],
            emails: emails,
            options: options
          });

        case 33:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context5.next = 38;
          break;

        case 36:
          _context5.prev = 36;
          _context5.t0 = _context5["catch"](0);

        case 38:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 36]]);
}

function sendEmail(action) {
  var callServer, cid, emails, options, authToken, webServiceUrl, newEmails, response;
  return regeneratorRuntime.wrap(function sendEmail$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 6:
          cid = _context6.sent;
          _context6.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].preApplicationEmail.emails;
          });

        case 9:
          emails = _context6.sent;
          _context6.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].preApplicationEmail.options;
          });

        case 12:
          options = _context6.sent;
          _context6.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 15:
          authToken = _context6.sent;
          _context6.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 18:
          webServiceUrl = _context6.sent;
          newEmails = _.cloneDeep(emails);


          _.forEach(newEmails, function (email, index) {
            newEmails[index].content += "<img src='cid:axaLogo'>";

            var checkboxOptions = options.find(function (cbOptions) {
              return cbOptions.length > 0 && cbOptions[0].id === email.id;
            });
            _.forEach(checkboxOptions, function (option) {
              if (!option.isSelected) {
                var idIndex = email.attachmentIds.findIndex(function (id) {
                  return id === option.key;
                });
                if (idIndex >= 0) {
                  newEmails[index].attachmentIds.splice(idIndex, 1);
                }
              }
            });
          });
          _context6.next = 23;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "sendApplicationEmail",
              emails: newEmails,
              cid: cid,
              authToken: authToken,
              webServiceUrl: webServiceUrl
            }
          });

        case 23:
          response = _context6.sent;


          if (response && response.success && _.isFunction(action.callback)) {
            action.callback();
          }
          _context6.next = 29;
          break;

        case 27:
          _context6.prev = 27;
          _context6.t0 = _context6["catch"](0);

        case 29:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 27]]);
}

function loadClientProfileAndInitValidate(action) {
  var cid, profile, configData, callback, profileData, optionsMapData, loadClientData, clientForm, validatedClientForm, hasErrorList, newHasErrorList, _isClientFormHasError, hasError, errorFields, clientHasErrorIndex;

  return regeneratorRuntime.wrap(function loadClientProfileAndInitValidate$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          cid = action.cid, profile = action.profile, configData = action.configData, callback = action.callback;
          _context7.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 4:
          profileData = _context7.sent;
          _context7.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 7:
          optionsMapData = _context7.sent;
          loadClientData = Object.assign({}, (0, _dataMapping.profileMapForm)(_.cloneDeep(profile)), {
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT
          });
          _context7.next = 11;
          return (0, _effects.put)(loadClientData);

        case 11:
          _context7.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
            configData: configData
          });

        case 13:
          _context7.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 15:
          clientForm = _context7.sent;
          _context7.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE,
            configData: clientForm.config,
            isPDA: clientForm.config.isPDA,
            isFNA: clientForm.config.isFNA,
            isAPP: clientForm.config.isAPP,
            relationshipData: clientForm.relationship,
            genderData: clientForm.gender,
            profileData: profileData,
            incomeData: clientForm.income,
            maritalData: clientForm.maritalStatus,
            educationData: clientForm.education,
            isFamilyMemberData: clientForm.config.isFamilyMember,
            isProposerMissingData: clientForm.config.isProposerMissing,
            isInsuredMissingData: clientForm.config.isInsuredMissing,
            relationshipOtherData: clientForm.relationshipOther,
            givenNameData: clientForm.givenName,
            nameData: clientForm.givenName,
            titleData: clientForm.title,
            isCreateData: clientForm.config.isCreate,
            singaporePRStatusData: clientForm.singaporePRStatus,
            nationalityData: clientForm.nationality,
            cityOfResidenceData: clientForm.cityOfResidence,
            countryOfResidenceData: clientForm.countryOfResidence,
            optionsMapData: optionsMapData,
            isApplicationData: clientForm.config.isApplication,
            otherCityOfResidenceData: clientForm.otherCityOfResidence,
            IDData: clientForm.ID,
            IDDocumentTypeData: clientForm.IDDocumentType,
            IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
            employStatusData: clientForm.employStatus,
            employStatusOtherData: clientForm.employStatusOther,
            industryData: clientForm.industry,
            occupationData: clientForm.occupation,
            birthdayData: clientForm.birthday,
            smokingStatusData: clientForm.smokingStatus,
            occupationOtherData: clientForm.occupationOther,
            languageData: clientForm.language,
            languageOtherData: clientForm.languageOther,
            emailData: clientForm.email,
            clientFormCid: clientForm.profileBackUp.cid,
            prefixAData: clientForm.prefixA,
            mobileNoAData: clientForm.mobileNoA,
            typeOfPassData: clientForm.typeOfPass,
            typeOfPassOtherData: clientForm.typeOfPassOther
          });

        case 18:
          _context7.next = 20;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 20:
          validatedClientForm = _context7.sent;
          _context7.next = 23;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].multiClientProfile.hasErrorList;
          });

        case 23:
          hasErrorList = _context7.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);
          _isClientFormHasError = (0, _validation.isClientFormHasError)(validatedClientForm), hasError = _isClientFormHasError.hasError, errorFields = _isClientFormHasError.errorFields;
          clientHasErrorIndex = newHasErrorList.findIndex(function (clientHasError) {
            return clientHasError.cid === cid;
          });

          if (clientHasErrorIndex >= 0) {
            newHasErrorList[clientHasErrorIndex].hasError = hasError;
          } else if (hasError) {
            newHasErrorList.push({ cid: cid, hasError: hasError, errorFields: errorFields });
          }

          _context7.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 30:
          if (!_.isFunction(callback)) {
            _context7.next = 33;
            break;
          }

          _context7.next = 33;
          return (0, _effects.call)(callback);

        case 33:
          _context7.next = 37;
          break;

        case 35:
          _context7.prev = 35;
          _context7.t0 = _context7["catch"](0);

        case 37:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 35]]);
}

function checkBeforeApply(action) {
  var pCid, iCids, callback, agentChannel, profile, dependantProfiles, isFnaInvalid, dialogType, i, iCid, dependantProfile, hasErrorList, showProfileDialog;
  return regeneratorRuntime.wrap(function checkBeforeApply$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          pCid = action.pCid, iCids = action.iCids, callback = action.callback;
          _context8.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].agentChannel;
          });

        case 4:
          agentChannel = _context8.sent;
          _context8.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 7:
          profile = _context8.sent;
          _context8.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 10:
          dependantProfiles = _context8.sent;
          _context8.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].isFnaInvalid;
          });

        case 13:
          isFnaInvalid = _context8.sent;
          dialogType = "";

          if (!isFnaInvalid) {
            _context8.next = 19;
            break;
          }

          dialogType = _DIALOG_TYPES.ERROR;
          _context8.next = 41;
          break;

        case 19:
          _context8.next = 21;
          return loadClientProfileAndInitValidate({
            cid: pCid,
            profile: profile,
            configData: {
              isCreate: false,
              isFamilyMember: false,
              isProposerMissing: true,
              isInsuredMissing: false,
              isFNA: agentChannel !== "FA",
              isAPP: true
            }
          });

        case 21:
          _context8.next = 23;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 23:
          i = 0;

        case 24:
          if (!(i < iCids.length)) {
            _context8.next = 36;
            break;
          }

          iCid = iCids[i];

          if (!(pCid !== iCid)) {
            _context8.next = 33;
            break;
          }

          dependantProfile = dependantProfiles[iCid];

          if (!dependantProfile) {
            _context8.next = 33;
            break;
          }

          _context8.next = 31;
          return loadClientProfileAndInitValidate({
            cid: iCid,
            profile: dependantProfile,
            configData: {
              isCreate: false,
              isFamilyMember: false,
              isProposerMissing: false,
              isInsuredMissing: true,
              isFNA: false,
              isAPP: true
            }
          });

        case 31:
          _context8.next = 33;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 33:
          i += 1;
          _context8.next = 24;
          break;

        case 36:
          _context8.next = 38;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].multiClientProfile.hasErrorList;
          });

        case 38:
          hasErrorList = _context8.sent;
          showProfileDialog = hasErrorList.some(function (clientHasError) {
            return clientHasError.hasError;
          });


          if (showProfileDialog) {
            dialogType = _DIALOG_TYPES.OTHER;
          }

        case 41:
          if (!_.isFunction(callback)) {
            _context8.next = 44;
            break;
          }

          _context8.next = 44;
          return (0, _effects.call)(callback, dialogType);

        case 44:
          _context8.next = 48;
          break;

        case 46:
          _context8.prev = 46;
          _context8.t0 = _context8["catch"](0);

        case 48:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 46]]);
}

function confirmApplicationPaid(action) {
  var callServer, selectedBundleId, isShield, applicationId, response;
  return regeneratorRuntime.wrap(function confirmApplicationPaid$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].component.selectedBundleId;
          });

        case 6:
          selectedBundleId = _context9.sent;
          isShield = action.isShield, applicationId = action.applicationId;

          if (!isShield) {
            _context9.next = 14;
            break;
          }

          _context9.next = 11;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              action: "confirmAppPaid",
              appId: applicationId
            }
          });

        case 11:
          _context9.t0 = _context9.sent;
          _context9.next = 17;
          break;

        case 14:
          _context9.next = 16;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "confirmAppPaid",
              appId: applicationId
            }
          });

        case 16:
          _context9.t0 = _context9.sent;

        case 17:
          response = _context9.t0;

          if (!(response && response.success)) {
            _context9.next = 21;
            break;
          }

          _context9.next = 21;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS,
            bundleId: selectedBundleId
          });

        case 21:
          _context9.next = 25;
          break;

        case 23:
          _context9.prev = 23;
          _context9.t1 = _context9["catch"](0);

        case 25:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 23]]);
}