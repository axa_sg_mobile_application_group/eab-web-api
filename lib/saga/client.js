"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProfile = getProfile;
exports.getContactList = getContactList;
exports.updateProfile = updateProfile;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _invalidation = require("./invalidation");

var invalidation = _interopRequireWildcard(_invalidation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getProfile),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(getContactList),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(updateProfile);

function getProfile(action) {
  var callServer, cid, profileResponse, dependantProfilesResponse, signExpiryResponse;
  return regeneratorRuntime.wrap(function getProfile$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          cid = action.cid;
          _context.next = 7;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "getProfile",
              docId: cid
            }
          });

        case 7:
          profileResponse = _context.sent;
          _context.next = 10;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "getAllDependantsProfile",
              cid: cid
            }
          });

        case 10:
          dependantProfilesResponse = _context.sent;
          _context.next = 13;
          return (0, _effects.call)(invalidation.signatureExpiry, {
            cid: cid
          });

        case 13:
          signExpiryResponse = _context.sent;


          if (signExpiryResponse.success) {
            // TODO
          }

          _context.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CLIENT_AND_DEPENDANTS,
            profileData: profileResponse.profile,
            dependantProfilesData: dependantProfilesResponse.dependantProfiles
          });

        case 17:
          _context.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!profileResponse.showFnaInvalidFlag
          });

        case 19:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context.next = 24;
          break;

        case 22:
          _context.prev = 22;
          _context.t0 = _context["catch"](0);

        case 24:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 22]]);
}

function getContactList(action) {
  var callServer, response;
  return regeneratorRuntime.wrap(function getContactList$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].START_LOADING
          });

        case 6:
          _context2.next = 8;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "getContactList"
            }
          });

        case 8:
          response = _context2.sent;
          _context2.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CONTACT_LIST,
            contactListData: response.contactList
          });

        case 11:
          _context2.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].FINISH_LOADING
          });

        case 13:
          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context2.next = 18;
          break;

        case 16:
          _context2.prev = 16;
          _context2.t0 = _context2["catch"](0);

        case 18:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 16]]);
}

function updateProfile(action) {
  var profileList, profile, dependantProfiles, newProfile, newDependantProfiles;
  return regeneratorRuntime.wrap(function updateProfile$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          profileList = action.profileList;
          _context3.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 4:
          profile = _context3.sent;
          _context3.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 7:
          dependantProfiles = _context3.sent;
          newProfile = _.cloneDeep(profile);
          newDependantProfiles = _.cloneDeep(dependantProfiles);


          _.forEach(profileList, function (p) {
            var cid = _.get(p, "cid");
            if (cid) {
              if (cid === newProfile.cid) {
                newProfile = p;
              } else if (!_.isEmpty(dependantProfiles) && dependantProfiles[cid]) {
                newDependantProfiles[cid] = p;
              }
            }
          });

          _context3.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CLIENT_AND_DEPENDANTS,
            profileData: newProfile,
            dependantProfilesData: newDependantProfiles
          });

        case 13:
          _context3.next = 17;
          break;

        case 15:
          _context3.prev = 15;
          _context3.t0 = _context3["catch"](0);

        case 17:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 15]]);
}