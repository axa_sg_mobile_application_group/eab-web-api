"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getQuotation = getQuotation;
exports.getFunds = getFunds;
exports.allocFunds = allocFunds;
exports.updateIsQuickQuote = updateIsQuickQuote;
exports.queryQuickQuotes = queryQuickQuotes;
exports.deleteQuickQuotes = deleteQuickQuotes;
exports.createProposal = createProposal;
exports.updateRiders = updateRiders;
exports.updateClient = updateClient;
exports.updateShieldRiders = updateShieldRiders;
exports.quote = quote;
exports.resetQuot = resetQuot;
exports.viewProposal = viewProposal;
exports.requote = requote;
exports.requoteInvalid = requoteInvalid;
exports.clone = clone;
exports.selectInsuredQuotation = selectInsuredQuotation;
exports.openEmailDialog = openEmailDialog;
exports.sendEmail = sendEmail;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _DIALOG_TYPES = require("../constants/DIALOG_TYPES");

var _common = require("../utilities/common");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _fundChecker = require("../utilities/fundChecker");

var _fundChecker2 = _interopRequireDefault(_fundChecker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getQuotation),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(getFunds),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(allocFunds),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updateIsQuickQuote),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(queryQuickQuotes),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(deleteQuickQuotes),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(createProposal),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(updateRiders),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(updateClient),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(updateShieldRiders),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(quote),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(resetQuot),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(viewProposal),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(requote),
    _marked15 = /*#__PURE__*/regeneratorRuntime.mark(requoteInvalid),
    _marked16 = /*#__PURE__*/regeneratorRuntime.mark(clone),
    _marked17 = /*#__PURE__*/regeneratorRuntime.mark(selectInsuredQuotation),
    _marked18 = /*#__PURE__*/regeneratorRuntime.mark(openEmailDialog),
    _marked19 = /*#__PURE__*/regeneratorRuntime.mark(sendEmail);

function getQuotation(action) {
  var callServer, profile, insuredCid, isQuickQuote, resp;
  return regeneratorRuntime.wrap(function getQuotation$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          _context.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context.sent;
          _context.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRODUCTS].insuredCid;
          });

        case 9:
          insuredCid = _context.sent;
          _context.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].isQuickQuote;
          });

        case 12:
          isQuickQuote = _context.sent;
          _context.next = 15;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "get",
              action: "initQuotation",
              productId: action.productID,
              iCid: !_.isEmpty(insuredCid) ? insuredCid : profile.cid,
              pCid: profile.cid,
              quickQuote: isQuickQuote,
              params: {
                ccy: null
              },
              confirm: action.confirm
            }
          });

        case 15:
          resp = _context.sent;

          if (!resp.success) {
            _context.next = 36;
            break;
          }

          _context.t0 = true;
          _context.next = _context.t0 === !!resp.errorMsg ? 20 : _context.t0 === !!resp.requireConfirm ? 22 : _context.t0 === !!(resp.quotation && resp.planDetails) ? 24 : 33;
          break;

        case 20:
          action.alert(resp.errorMsg);
          return _context.abrupt("break", 34);

        case 22:
          action.confirmRequest("products.confirm.invalidateApplications");
          return _context.abrupt("break", 34);

        case 24:
          _context.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_QUOTATION_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

        case 26:
          _context.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE,
            profileData: resp.profile
          });

        case 28:
          if (!(resp.quotation.baseProductCode === "PNP" || resp.quotation.baseProductCode === "PNP2" || resp.quotation.baseProductCode === "PNPP" || resp.quotation.baseProductCode === "PNPP2")) {
            _context.next = 31;
            break;
          }

          _context.next = 31;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE,
            quotationData: resp.quotation
          });

        case 31:

          action.openQuotationFunction();
          return _context.abrupt("break", 34);

        case 33:
          throw new Error("saga 'getQuotation' got unexpected response");

        case 34:
          _context.next = 37;
          break;

        case 36:
          throw new Error("saga 'getQuotation' request fail");

        case 37:
          _context.next = 41;
          break;

        case 39:
          _context.prev = 39;
          _context.t1 = _context["catch"](0);

        case 41:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 39]]);
}

function getFunds(action) {
  var callServer, quotation, _action$isReQuote, isReQuote, _action$isClone, isClone, updateObject, callback, getFundsAPI;

  return regeneratorRuntime.wrap(function getFunds$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          _context2.t0 = _;
          _context2.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 7:
          _context2.t1 = _context2.sent;
          quotation = _context2.t0.cloneDeep.call(_context2.t0, _context2.t1);
          _action$isReQuote = action.isReQuote, isReQuote = _action$isReQuote === undefined ? false : _action$isReQuote, _action$isClone = action.isClone, isClone = _action$isClone === undefined ? false : _action$isClone, updateObject = action.updateObject, callback = action.callback;
          _context2.next = 12;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "getFundDetails",
              productId: quotation.baseProductId,
              invOpt: quotation.fund ? quotation.fund.invOpt : action.invOpt,
              paymentMethod: quotation.policyOptions && quotation.policyOptions.paymentMethod
            }
          });

        case 12:
          getFundsAPI = _context2.sent;
          _context2.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_FUNDS,
            newAvailableFunds: getFundsAPI.funds
          });

        case 15:
          if (!isReQuote) {
            _context2.next = 18;
            break;
          }

          _context2.next = 18;
          return (0, _effects.put)(Object.assign({}, updateObject, {
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED
          }));

        case 18:
          if (!isClone) {
            _context2.next = 21;
            break;
          }

          _context2.next = 21;
          return (0, _effects.put)(Object.assign({}, updateObject, {
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED
          }));

        case 21:

          if (_.isFunction(callback)) {
            callback();
          }
          _context2.next = 26;
          break;

        case 24:
          _context2.prev = 24;
          _context2.t2 = _context2["catch"](0);

        case 26:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 24]]);
}

function allocFunds(action) {
  var callServer, quotation, invOpt, portfolio, fundAllocs, topUpAllocs, hasTopUpAlloc, adjustedModel, callback, allocFundsAPI;
  return regeneratorRuntime.wrap(function allocFunds$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context3.sent;
          _context3.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 6:
          quotation = _context3.sent;
          invOpt = action.invOpt, portfolio = action.portfolio, fundAllocs = action.fundAllocs, topUpAllocs = action.topUpAllocs, hasTopUpAlloc = action.hasTopUpAlloc, adjustedModel = action.adjustedModel, callback = action.callback;
          _context3.next = 10;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "allocFunds",
              quotation: _.cloneDeep(quotation),
              invOpt: invOpt,
              portfolio: portfolio,
              fundAllocs: fundAllocs,
              hasTopUpAlloc: hasTopUpAlloc,
              topUpAllocs: topUpAllocs,
              adjustedModel: adjustedModel
            }
          });

        case 10:
          allocFundsAPI = _context3.sent;
          _context3.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].ALLOC_FUNDS_COMPLETED,
            newQuotation: allocFundsAPI.quotation,
            newQuotationErrors: allocFundsAPI.errors
          });

        case 13:

          if (_.isFunction(callback)) {
            callback();
          }
          _context3.next = 18;
          break;

        case 16:
          _context3.prev = 16;
          _context3.t0 = _context3["catch"](0);

        case 18:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 16]]);
}

function updateIsQuickQuote(action) {
  return regeneratorRuntime.wrap(function updateIsQuickQuote$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_INSURED_CID,
            insuredCidData: null
          });

        case 3:
          _context4.next = 5;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_DEPENDANTS,
            callback: function callback() {}
          });

        case 5:
          _context4.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_PRODUCT_LIST,
            callback: action.callback ? action.callback : function () {}
          });

        case 7:
          _context4.next = 11;
          break;

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);

        case 11:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 9]]);
}

function queryQuickQuotes(action) {
  var pCid, callServer, response;
  return regeneratorRuntime.wrap(function queryQuickQuotes$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 3:
          pCid = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context5.sent;
          _context5.next = 9;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "queryQuickQuotes",
              pCid: pCid
            }
          });

        case 9:
          response = _context5.sent;
          _context5.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUICK_QUOTES,
            quickQuotesData: response.quickQuotes
          });

        case 12:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context5.next = 17;
          break;

        case 15:
          _context5.prev = 15;
          _context5.t0 = _context5["catch"](0);

        case 17:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 15]]);
}

function deleteQuickQuotes(action) {
  var pCid, callServer, quotIds, response;
  return regeneratorRuntime.wrap(function deleteQuickQuotes$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 3:
          pCid = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context6.sent;
          quotIds = action.quotIds;
          _context6.next = 10;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "deleteQuickQuotes",
              pCid: pCid,
              quotIds: quotIds
            }
          });

        case 10:
          response = _context6.sent;
          _context6.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUICK_QUOTES,
            quickQuotesData: response.quickQuotes
          });

        case 13:

          action.callback();
          _context6.next = 18;
          break;

        case 16:
          _context6.prev = 16;
          _context6.t0 = _context6["catch"](0);

        case 18:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 16]]);
}

function createProposal(action) {
  var callback, warningCallback, errorCallback, quotation, callServer, optionsMap, resp;
  return regeneratorRuntime.wrap(function createProposal$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          callback = action.callback, warningCallback = action.warningCallback, errorCallback = action.errorCallback;
          _context7.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 4:
          quotation = _context7.sent;
          _context7.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 7:
          callServer = _context7.sent;
          _context7.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 10:
          optionsMap = _context7.sent;
          _context7.next = 13;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "post",
              action: "genProposal",
              quotation: quotation,
              optionsMap: optionsMap
            }
          });

        case 13:
          resp = _context7.sent;

          if (!resp.success) {
            _context7.next = 22;
            break;
          }

          if (!resp.errorMsg) {
            _context7.next = 19;
            break;
          }

          errorCallback(resp.errorMsg);
          _context7.next = 22;
          break;

        case 19:
          _context7.next = 21;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL,
            quotation: resp.quotation,
            pdfData: resp.proposal,
            illustrateData: resp.illustrateData,
            planDetails: resp.planDetails,
            canRequote: _.get(resp, "funcPerms.requote", false),
            canRequoteInvalid: _.get(resp, "funcPerms.requoteInvalid", false),
            canClone: _.get(resp, "funcPerms.clone", false),
            canEmail: true
          });

        case 21:

          if (resp.warningMsg && action.warningCallback) {
            warningCallback(resp.warningMsg);
          } else if (_.isFunction(callback)) {
            callback();
          }

        case 22:
          _context7.next = 26;
          break;

        case 24:
          _context7.prev = 24;
          _context7.t0 = _context7["catch"](0);

        case 26:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 24]]);
}

function updateRiders(action) {
  var covCodes, quotation, callServer, resp;
  return regeneratorRuntime.wrap(function updateRiders$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          covCodes = action.covCodes;
          _context8.t0 = _;
          _context8.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 5:
          _context8.t1 = _context8.sent;
          quotation = _context8.t0.cloneDeep.call(_context8.t0, _context8.t1);
          _context8.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 9:
          callServer = _context8.sent;
          _context8.next = 12;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "updateRiderList",
              quotation: quotation,
              newRiderList: covCodes
            }
          });

        case 12:
          resp = _context8.sent;

          if (!resp.success) {
            _context8.next = 16;
            break;
          }

          _context8.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_RIDERS_COMPLETED,
            newQuotation: resp.quotation,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors
          });

        case 16:
          _context8.next = 20;
          break;

        case 18:
          _context8.prev = 18;
          _context8.t2 = _context8["catch"](0);

        case 20:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 18]]);
}

function updateClient() {
  var quotation, callServer, resp;
  return regeneratorRuntime.wrap(function updateClient$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 3:
          quotation = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context9.sent;
          _context9.next = 9;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "updateClients",
              quotation: quotation
            }
          });

        case 9:
          resp = _context9.sent;

          if (!resp.success) {
            _context9.next = 13;
            break;
          }

          _context9.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_AVAILABLE_INSUREDS,
            newAvailableInsureds: resp.availableInsureds
          });

        case 13:
          _context9.next = 17;
          break;

        case 15:
          _context9.prev = 15;
          _context9.t0 = _context9["catch"](0);

        case 17:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 15]]);
}

function updateShieldRiders(action) {
  var covCodes, cid, alert, quotation, callServer, resp;
  return regeneratorRuntime.wrap(function updateShieldRiders$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          covCodes = action.covCodes, cid = action.cid, alert = action.alert;
          _context10.t0 = _;
          _context10.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 5:
          _context10.t1 = _context10.sent;
          quotation = _context10.t0.cloneDeep.call(_context10.t0, _context10.t1);
          _context10.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 9:
          callServer = _context10.sent;
          _context10.next = 12;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "updateShieldRiderList",
              quotation: quotation,
              newRiderList: covCodes,
              cid: cid
            }
          });

        case 12:
          resp = _context10.sent;

          if (!resp.success) {
            _context10.next = 20;
            break;
          }

          if (!resp.errorMsg) {
            _context10.next = 18;
            break;
          }

          alert(resp.errorMsg);
          _context10.next = 20;
          break;

        case 18:
          _context10.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INSURED_QUOTATION,
            newQuotation: resp.quotation,
            newInputConfigs: resp.inputConfigs,
            newQuotationErrors: resp.errors
          });

        case 20:
          _context10.next = 24;
          break;

        case 22:
          _context10.prev = 22;
          _context10.t2 = _context10["catch"](0);

        case 24:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 22]]);
}

function quote(action) {
  var quotationData, callServer, resp;
  return regeneratorRuntime.wrap(function quote$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          quotationData = action.quotationData;
          _context11.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context11.sent;
          _context11.next = 7;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "quote",
              quotation: _.cloneDeep(quotationData)
            }
          });

        case 7:
          resp = _context11.sent;

          if (!resp.success) {
            _context11.next = 11;
            break;
          }

          _context11.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE_COMPLETED,
            newQuotation: resp.quotation,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors
          });

        case 11:
          _context11.next = 15;
          break;

        case 13:
          _context11.prev = 13;
          _context11.t0 = _context11["catch"](0);

        case 15:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 13]]);
}

function resetQuot(action) {
  var quotation, keepConfigs, keepPolicyOptions, keepPlans, keepFunds, callback, callServer, resp;
  return regeneratorRuntime.wrap(function resetQuot$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          quotation = action.quotation, keepConfigs = action.keepConfigs, keepPolicyOptions = action.keepPolicyOptions, keepPlans = action.keepPlans, keepFunds = action.keepFunds, callback = action.callback;
          _context12.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context12.sent;
          _context12.next = 7;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "resetQuot",
              quotation: _.cloneDeep(quotation),
              keepConfigs: keepConfigs,
              keepPolicyOptions: keepPolicyOptions,
              keepPlans: keepPlans,
              keepFunds: keepFunds
            }
          });

        case 7:
          resp = _context12.sent;

          if (!resp.success) {
            _context12.next = 14;
            break;
          }

          _context12.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].RESET_QUOTE_COMPLETED,
            newQuotation: resp.quotation,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors
          });

        case 11:
          if (!_.isFunction(callback)) {
            _context12.next = 14;
            break;
          }

          _context12.next = 14;
          return (0, _effects.call)(callback);

        case 14:
          _context12.next = 18;
          break;

        case 16:
          _context12.prev = 16;
          _context12.t0 = _context12["catch"](0);

        case 18:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 16]]);
}

function viewProposal(action) {
  var callServer, readonly, quotationId, callback, optionsMap, resp;
  return regeneratorRuntime.wrap(function viewProposal$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.prev = 0;
          _context13.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context13.sent;
          readonly = action.readonly, quotationId = action.quotationId, callback = action.callback;
          _context13.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 7:
          optionsMap = _context13.sent;
          _context13.next = 10;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "post",
              action: "getProposal",
              quotId: quotationId,
              readonly: readonly,
              optionsMap: optionsMap
            }
          });

        case 10:
          resp = _context13.sent;
          _context13.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_PROPOSAL,
            quotation: resp.quotation,
            pdfData: resp.proposal,
            illustrateData: resp.illustrateData,
            planDetails: resp.planDetails,
            canRequote: _.get(resp, "funcPerms.requote", false),
            canRequoteInvalid: _.get(resp, "funcPerms.requoteInvalid", false),
            canClone: _.get(resp, "funcPerms.clone", false),
            canEmail: !readonly
          });

        case 13:

          if (_.isFunction(callback)) {
            callback();
          }
          _context13.next = 18;
          break;

        case 16:
          _context13.prev = 16;
          _context13.t0 = _context13["catch"](0);

        case 18:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13, this, [[0, 16]]);
}

function requote(action) {
  var callServer, quotationId, callback, resp;
  return regeneratorRuntime.wrap(function requote$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.prev = 0;
          _context14.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context14.sent;
          quotationId = action.quotationId, callback = action.callback;
          _context14.next = 7;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "post",
              action: "requote",
              quotId: quotationId
            }
          });

        case 7:
          resp = _context14.sent;

          if (!resp.success) {
            _context14.next = 19;
            break;
          }

          if (!(0, _fundChecker2.default)({
            quotation: resp.quotation,
            planDetails: resp.planDetails
          })) {
            _context14.next = 16;
            break;
          }

          _context14.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION,
            newQuotation: {
              baseProductId: resp.quotation.baseProductId,
              policyOptions: resp.quotation.policyOptions
            }
          });

        case 12:
          _context14.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_FUNDS,
            isReQuote: true,
            invOpt: resp.quotation.fund ? resp.quotation.fund.invOpt : "mixedAssets",
            updateObject: {
              newQuotation: resp.quotation,
              newPlanDetails: resp.planDetails,
              newInputConfigs: resp.inputConfigs,
              newQuotationWarnings: resp.quotWarnings,
              newAvailableInsureds: resp.availableInsureds
            },
            callback: callback
          });

        case 14:
          _context14.next = 19;
          break;

        case 16:
          _context14.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

        case 18:

          if (_.isFunction(callback)) {
            callback();
          }

        case 19:
          _context14.next = 23;
          break;

        case 21:
          _context14.prev = 21;
          _context14.t0 = _context14["catch"](0);

        case 23:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14, this, [[0, 21]]);
}

function requoteInvalid(action) {
  var callServer, quotationId, confirm, callback, dialogType, resp;
  return regeneratorRuntime.wrap(function requoteInvalid$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.prev = 0;
          _context15.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context15.sent;
          quotationId = action.quotationId, confirm = action.confirm, callback = action.callback;
          dialogType = "";
          _context15.next = 8;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "post",
              action: "requoteInvalid",
              quotId: quotationId,
              confirm: confirm
            }
          });

        case 8:
          resp = _context15.sent;

          if (!resp.success) {
            _context15.next = 23;
            break;
          }

          if (!resp.requireConfirm) {
            _context15.next = 14;
            break;
          }

          dialogType = _DIALOG_TYPES.CONFIRMATION;
          _context15.next = 20;
          break;

        case 14:
          if (resp.allowRequote) {
            _context15.next = 18;
            break;
          }

          dialogType = _DIALOG_TYPES.ERROR;
          _context15.next = 20;
          break;

        case 18:
          _context15.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].REQUOTE_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

        case 20:
          if (!_.isFunction(callback)) {
            _context15.next = 23;
            break;
          }

          _context15.next = 23;
          return (0, _effects.call)(callback, dialogType);

        case 23:
          _context15.next = 27;
          break;

        case 25:
          _context15.prev = 25;
          _context15.t0 = _context15["catch"](0);

        case 27:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15, this, [[0, 25]]);
}

function clone(action) {
  var callServer, quotationId, _callback, resp;

  return regeneratorRuntime.wrap(function clone$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.prev = 0;
          _context16.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context16.sent;
          quotationId = action.quotationId, _callback = action.callback;

          // TODO: update confirm

          _context16.next = 7;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              method: "post",
              action: "cloneQuotation",
              quotId: quotationId,
              confirm: true
            }
          });

        case 7:
          resp = _context16.sent;

          if (!resp.success) {
            _context16.next = 23;
            break;
          }

          if (!resp.allowClone) {
            _context16.next = 22;
            break;
          }

          if (!(0, _fundChecker2.default)({
            quotation: resp.quotation,
            planDetails: resp.planDetails
          })) {
            _context16.next = 17;
            break;
          }

          _context16.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_QUOTATION,
            newQuotation: {
              baseProductId: resp.quotation.baseProductId,
              policyOptions: resp.quotation.policyOptions
            }
          });

        case 13:
          _context16.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].GET_FUNDS,
            isClone: true,
            invOpt: resp.quotation.fund ? resp.quotation.fund.invOpt : "mixedAssets",
            updateObject: {
              newQuotation: resp.quotation,
              newPlanDetails: resp.planDetails,
              newInputConfigs: resp.inputConfigs,
              newQuotationWarnings: resp.quotWarnings,
              newAvailableInsureds: resp.availableInsureds
            },
            callback: function callback() {
              _callback(resp.allowClone);
            }
          });

        case 15:
          _context16.next = 22;
          break;

        case 17:
          _context16.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].CLONE_COMPLETED,
            newQuotation: resp.quotation,
            newPlanDetails: resp.planDetails,
            newInputConfigs: resp.inputConfigs,
            newQuotationWarnings: resp.quotWarnings,
            newQuotationErrors: resp.errors,
            newAvailableInsureds: resp.availableInsureds
          });

        case 19:
          if (!(resp.quotation.baseProductCode === "PNP" || resp.quotation.baseProductCode === "PNP2" || resp.quotation.baseProductCode === "PNPP" || resp.quotation.baseProductCode === "PNPP2")) {
            _context16.next = 22;
            break;
          }

          _context16.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].QUOTE,
            quotationData: resp.quotation
          });

        case 22:
          if (_.isFunction(_callback)) {
            _callback(resp.allowClone);
          }

        case 23:
          _context16.next = 27;
          break;

        case 25:
          _context16.prev = 25;
          _context16.t0 = _context16["catch"](0);

        case 27:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16, this, [[0, 25]]);
}

function selectInsuredQuotation(action) {
  var cid, covClass, callServer, quotation, resp;
  return regeneratorRuntime.wrap(function selectInsuredQuotation$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.prev = 0;
          cid = action.cid, covClass = action.covClass;
          _context17.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context17.sent;
          _context17.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].quotation;
          });

        case 7:
          quotation = _context17.sent;
          _context17.next = 10;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "selectInsured",
              quotation: quotation,
              cid: cid,
              covClass: covClass
            }
          });

        case 10:
          resp = _context17.sent;

          if (!resp.success) {
            _context17.next = 14;
            break;
          }

          _context17.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.QUOTATION].UPDATE_INSURED_QUOTATION,
            newQuotation: resp.quotation,
            newInputConfigs: resp.inputConfigs,
            newQuotationErrors: resp.errors
          });

        case 14:
          _context17.next = 18;
          break;

        case 16:
          _context17.prev = 16;
          _context17.t0 = _context17["catch"](0);

        case 18:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked17, this, [[0, 16]]);
}

function openEmailDialog(action) {
  var callServer, profile, agent, proposal, isQuickQuote, reports, response, agentContent, clientContent, agentSubject, clientSubject, embedImgs, params, attachments, emails;
  return regeneratorRuntime.wrap(function openEmailDialog$(_context18) {
    while (1) {
      switch (_context18.prev = _context18.next) {
        case 0:
          _context18.prev = 0;
          _context18.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context18.sent;
          _context18.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context18.sent;
          _context18.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT];
          });

        case 9:
          agent = _context18.sent;
          _context18.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PROPOSAL];
          });

        case 12:
          proposal = _context18.sent;
          _context18.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.QUOTATION].isQuickQuote;
          });

        case 15:
          isQuickQuote = _context18.sent;
          reports = proposal.pdfData;
          _context18.next = 19;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "getEmailTemplate",
              standalone: isQuickQuote
            }
          });

        case 19:
          response = _context18.sent;

          if (!(response && response.success)) {
            _context18.next = 28;
            break;
          }

          agentContent = response.agentContent, clientContent = response.clientContent, agentSubject = response.agentSubject, clientSubject = response.clientSubject, embedImgs = response.embedImgs;
          params = {
            agentName: agent.name,
            agentContactNum: agent.mobile,
            clientName: profile.fullName
          };
          attachments = _.map(proposal.pdfData, function (pdf) {
            return pdf.id;
          });
          emails = [{
            id: "agent",
            isQQ: isQuickQuote,
            agentCode: agent.agentCode,
            dob: "",
            to: [agent.email],
            title: agentSubject,
            content: (0, _common.getEmailContent)(agentContent, params, '<img src="cid:axa_logo.png" width="72" height="72">'),
            attachmentIds: attachments,
            embedImgs: embedImgs
          }, {
            id: "client",
            isQQ: isQuickQuote,
            agentCode: "",
            dob: profile.dob,
            to: [profile.email],
            title: clientSubject,
            content: (0, _common.getEmailContent)(clientContent, params, '<img src="cid:axa_logo.png" width="72" height="72">'),
            attachmentIds: attachments,
            embedImgs: embedImgs
          }];
          _context18.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PROPOSAL].OPEN_EMAIL,
            emails: emails,
            reports: reports
          });

        case 27:

          if (_.isFunction(action.callback)) {
            action.callback();
          }

        case 28:
          _context18.next = 32;
          break;

        case 30:
          _context18.prev = 30;
          _context18.t0 = _context18["catch"](0);

        case 32:
        case "end":
          return _context18.stop();
      }
    }
  }, _marked18, this, [[0, 30]]);
}

function sendEmail(action) {
  var callServer, emails, reportOptions, quotation, authToken, webServiceUrl, newEmail, response;
  return regeneratorRuntime.wrap(function sendEmail$(_context19) {
    while (1) {
      switch (_context19.prev = _context19.next) {
        case 0:
          _context19.prev = 0;
          _context19.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context19.sent;
          _context19.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PROPOSAL].proposalEmail.emails;
          });

        case 6:
          emails = _context19.sent;
          _context19.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PROPOSAL].proposalEmail.reportOptions;
          });

        case 9:
          reportOptions = _context19.sent;
          _context19.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PROPOSAL].quotation;
          });

        case 12:
          quotation = _context19.sent;
          _context19.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 15:
          authToken = _context19.sent;
          _context19.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 18:
          webServiceUrl = _context19.sent;
          newEmail = _.cloneDeep(emails);


          newEmail.forEach(function (item) {
            item.attachmentIds = [];
          });

          Object.keys(reportOptions).forEach(function (user) {
            Object.keys(reportOptions[user]).forEach(function (report) {
              if (reportOptions[user][report] === true) {
                var index = newEmail.findIndex(function (x) {
                  return x.id === user;
                });
                newEmail[index].attachmentIds.push(report);
              }
            });
          });

          newEmail = newEmail.filter(function (item) {
            return item.attachmentIds.length !== 0;
          });

          _.forEach(newEmail, function (email) {
            email.content += "<img src='cid:axa_logo.png'>";
          });
          _context19.next = 26;
          return (0, _effects.call)(callServer, {
            url: "quot",
            data: {
              action: "emailProposal",
              emails: newEmail,
              quotId: quotation.id,
              authToken: authToken,
              webServiceUrl: webServiceUrl
            }
          });

        case 26:
          response = _context19.sent;


          if (response && response.success) {
            if (_.isFunction(action.callback)) {
              action.callback();
            }
          }
          _context19.next = 32;
          break;

        case 30:
          _context19.prev = 30;
          _context19.t0 = _context19["catch"](0);

        case 32:
        case "end":
          return _context19.stop();
      }
    }
  }, _marked19, this, [[0, 30]]);
}