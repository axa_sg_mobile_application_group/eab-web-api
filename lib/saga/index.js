"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = saga;

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var REDUCER_TYPES = _interopRequireWildcard(_REDUCER_TYPES);

var _clientForm = require("./clientForm");

var clientForm = _interopRequireWildcard(_clientForm);

var _preApplication = require("./preApplication");

var preApplication = _interopRequireWildcard(_preApplication);

var _products = require("./products");

var products = _interopRequireWildcard(_products);

var _quotation = require("./quotation");

var quotation = _interopRequireWildcard(_quotation);

var _recommendation = require("./recommendation");

var recommendation = _interopRequireWildcard(_recommendation);

var _client = require("./client");

var client = _interopRequireWildcard(_client);

var _fna = require("./fna");

var fna = _interopRequireWildcard(_fna);

var _fnaCKA = require("./fnaCKA");

var fnaCKA = _interopRequireWildcard(_fnaCKA);

var _fnaRA = require("./fnaRA");

var fnaRA = _interopRequireWildcard(_fnaRA);

var _application = require("./application");

var application = _interopRequireWildcard(_application);

var _supportingDocument = require("./supportingDocument");

var supportingDocument = _interopRequireWildcard(_supportingDocument);

var _paymentAndSubmission = require("./paymentAndSubmission");

var paymentAndSubmission = _interopRequireWildcard(_paymentAndSubmission);

var _signature = require("./signature");

var signature = _interopRequireWildcard(_signature);

var _agent = require("./agent");

var agent = _interopRequireWildcard(_agent);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(saga);

function saga() {
  return regeneratorRuntime.wrap(function saga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.AGENT].UPDATE_AGENT_PROFILE_AVATAR, agent.updateAgentProfileAvatar);

        case 2:
          _context.next = 4;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.AGENT].ASSIGN_ELIG_PRODUCTS_TO_AGENT, agent.assignEligProductsToAgent);

        case 4:
          _context.next = 6;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GET_PROFILE_DATA, clientForm.getProfileData);

        case 6:
          _context.next = 8;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].DELETE_CLIENT, clientForm.deleteClient);

        case 8:
          _context.next = 10;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].UNLINK_CLIENT, clientForm.unlinkRelationship);

        case 10:
          _context.next = 12;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SAVE_CLIENT, clientForm.saveClient);

        case 12:
          _context.next = 14;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SAVE_TRUSTED_INDIVIDUAL, clientForm.saveTrustedIndividual);

        case 14:
          _context.next = 16;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].INITIAL_CLIENT_FORM, clientForm.initialValidateSaga);

        case 16:
          _context.next = 18;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE], clientForm.relationShipOtherSaga);

        case 18:
          _context.next = 20;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE, clientForm.validateGivenName);

        case 20:
          _context.next = 22;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SMOKING_STATUS_ON_CHANGE, clientForm.validateSmoking);

        case 22:
          _context.next = 24;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE, clientForm.validateMaritalStatus);

        case 24:
          _context.next = 26;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE, clientForm.validateGender);

        case 26:
          _context.next = 28;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE], clientForm.validateRelationship);

        case 28:
          _context.next = 30;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE], clientForm.validateEmployStatus);

        case 30:
          _context.next = 32;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE], clientForm.validateEmployStatusOther);

        case 32:
          _context.next = 34;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE, clientForm.passSaga);

        case 34:
          _context.next = 36;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE, clientForm.genderSaga);

        case 36:
          _context.next = 38;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE, clientForm.validateNationality);

        case 38:
          _context.next = 40;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE], clientForm.singaporePRStatusValidation);

        case 40:
          _context.next = 42;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE], clientForm.validateIDDocumentTypeOther);

        case 42:
          _context.next = 44;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE, clientForm.maritalStatusSaga);

        case 44:
          _context.next = 46;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SURNAME_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NAME_ORDER_ON_CHANGE], clientForm.autoFillName);

        case 46:
          _context.next = 48;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NAME_ON_CHANGE, clientForm.nameSaga);

        case 48:
          _context.next = 50;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TITLE_ON_CHANGE], clientForm.validateTitle);

        case 50:
          _context.next = 52;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE], clientForm.employStatusSaga);

        case 52:
          _context.next = 54;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE], clientForm.employStatusOtherSaga);

        case 54:
          _context.next = 56;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE], clientForm.occupationSaga);

        case 56:
          _context.next = 58;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE], clientForm.industrySaga);

        case 58:
          _context.next = 60;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE, clientForm.validateLanguage);

        case 60:
          _context.next = 62;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE], clientForm.validateLanguageOther);

        case 62:
          _context.next = 64;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, clientForm.validateOccupation);

        case 64:
          _context.next = 66;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE, clientForm.validateIndustry);

        case 66:
          _context.next = 68;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE], clientForm.validateOccupation);

        case 68:
          _context.next = 70;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, clientForm.occupationOtherSaga);

        case 70:
          _context.next = 72;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE], clientForm.nameOfEmployBusinessSchoolSaga);

        case 72:
          _context.next = 74;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE], clientForm.countryOfEmployBusinessSchoolSaga);

        case 74:
          _context.next = 76;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE], clientForm.validateTypeOfPass);

        case 76:
          _context.next = 78;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE], clientForm.TypeOfPassOtherSaga);

        case 78:
          _context.next = 80;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE], clientForm.validateTypeOfPassOther);

        case 80:
          _context.next = 82;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE, clientForm.singaporePRStatusSaga);

        case 82:
          _context.next = 84;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE, clientForm.countrySaga);

        case 84:
          _context.next = 86;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].POSTALCODE_ON_BLUR, clientForm.postalCodeSaga);

        case 86:
          _context.next = 88;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE], clientForm.documentTypeSaga);

        case 88:
          _context.next = 90;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE], clientForm.isCityOfResidenceErrorSaga);

        case 90:
          _context.next = 92;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE, clientForm.cityOfResidenceSaga);

        case 92:
          _context.next = 94;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE], clientForm.validateOtherCityOfResidence);

        case 94:
          _context.next = 96;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE, clientForm.validateBirthday);

        case 96:
          _context.next = 98;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE, clientForm.validateID);

        case 98:
          _context.next = 100;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE, clientForm.cityStateSaga);

        case 100:
          _context.next = 102;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE, clientForm.mobileNoASaga);

        case 102:
          _context.next = 104;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].PREFIX_B_ON_CHANGE, clientForm.mobileNoBSaga);

        case 104:
          _context.next = 106;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE], clientForm.languageOtherSaga);

        case 106:
          _context.next = 108;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].INCOME_ON_CHANGE, clientForm.validateIncome);

        case 108:
          _context.next = 110;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EDUCATION_ON_CHANGE, clientForm.validateEducation);

        case 110:
          _context.next = 112;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE, clientForm.validateIDDocumentType);

        case 112:
          _context.next = 114;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE, clientForm.validatePrefixA);

        case 114:
          _context.next = 116;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_A_ON_CHANGE, clientForm.validateMobileNoA);

        case 116:
          _context.next = 118;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].ID_ON_CHANGE, clientForm.validateID);

        case 118:
          _context.next = 120;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].EMAIL_ON_CHANGE, clientForm.validateEmail);

        case 120:
          _context.next = 122;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].SAVE_CLIENT_EMAIL, clientForm.saveClientEmail);

        case 122:
          _context.next = 124;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT_PROFILE_AND_INIT_VALIDATE, clientForm.loadClientProfileAndInitValidate);

        case 124:
          _context.next = 126;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].UPDATE_CLIENT_PROFILE_AND_VALIDATE, clientForm.updateClientProfileAndValidate);

        case 126:
          _context.next = 128;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].CHECK_INSURED_LIST, clientForm.checkInsuredList);

        case 128:
          _context.next = 130;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT_FORM].REMOVE_COMPLETE_PROFILE_FROM_LIST, clientForm.removeCompleteProfileFromList);

        case 130:
          _context.next = 132;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].GET_QUOTATION, quotation.getQuotation);

        case 132:
          _context.next = 134;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].GET_FUNDS, quotation.getFunds);

        case 134:
          _context.next = 136;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].ALLOC_FUNDS, quotation.allocFunds);

        case 136:
          _context.next = 138;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].QUOTE, quotation.quote);

        case 138:
          _context.next = 140;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].RESET_QUOTE, quotation.resetQuot);

        case 140:
          _context.next = 142;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].UPDATE_IS_QUICK_QUOTE, quotation.updateIsQuickQuote);

        case 142:
          _context.next = 144;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].GET_QUICK_QUOTES, quotation.queryQuickQuotes);

        case 144:
          _context.next = 146;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].DELETE_QUICK_QUOTES, quotation.deleteQuickQuotes);

        case 146:
          _context.next = 148;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].CREATE_PROPOSAL, quotation.createProposal);

        case 148:
          _context.next = 150;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].UPDATE_SHIELD_RIDERS, quotation.updateShieldRiders);

        case 150:
          _context.next = 152;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].UPDATE_CLIENTS, quotation.updateClient);

        case 152:
          _context.next = 154;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].UPDATE_RIDERS, quotation.updateRiders);

        case 154:
          _context.next = 156;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].SAGA_VIEW_PROPOSAL, quotation.viewProposal);

        case 156:
          _context.next = 158;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].SELECT_INSURED_QUOTATION, quotation.selectInsuredQuotation);

        case 158:
          _context.next = 160;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].OPEN_EMAIL_DIALOG, quotation.openEmailDialog);

        case 160:
          _context.next = 162;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.QUOTATION].SEND_EMAIL, quotation.sendEmail);

        case 162:
          _context.next = 164;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE, quotation.requote);

        case 164:
          _context.next = 166;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE_INVALID, quotation.requoteInvalid);

        case 166:
          _context.next = 168;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PROPOSAL].SAGA_CLONE, quotation.clone);

        case 168:
          _context.next = 170;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS, preApplication.getApplications);

        case 170:
          _context.next = 172;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].SAGA_DELETE_APPLICATIONS, preApplication.deleteApplications);

        case 172:
          _context.next = 174;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].SAGA_UPDATE_UI_SELECTED_LIST, preApplication.updateUiSelectedList);

        case 174:
          _context.next = 176;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].SAGA_UPDATE_RECOMMENDATION_CHOICE_LIST, preApplication.updateRecommendChoiceList);

        case 176:
          _context.next = 178;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].OPEN_EMAIL_DIALOG, preApplication.openEmailDialog);

        case 178:
          _context.next = 180;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].SEND_EMAIL, preApplication.sendEmail);

        case 180:
          _context.next = 182;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].CHECK_BEFORE_APPLY, preApplication.checkBeforeApply);

        case 182:
          _context.next = 184;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].LOAD_CLIENT_AND_INIT_VALIDATE, preApplication.loadClientProfileAndInitValidate);

        case 184:
          _context.next = 186;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRE_APPLICATION].CONFIRM_APPLICATION_PAID, preApplication.confirmApplicationPaid);

        case 186:
          _context.next = 188;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_GET_RECOMMENDATION, recommendation.getRecommendation);

        case 188:
          _context.next = 190;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_CLOSE_RECOMMENDATION, recommendation.closeRecommendation);

        case 190:
          _context.next = 192;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_RECOMMENDATION, recommendation.validateRecommendation);

        case 192:
          _context.next = 194;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_DATA, recommendation.updateRecommendationData);

        case 194:
          _context.next = 196;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_STEP, recommendation.updateRecommendationStep);

        case 196:
          _context.next = 198;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_BUDGET, recommendation.validateBudget);

        case 198:
          _context.next = 200;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_BUDGET_DATA, recommendation.updateBudgetData);

        case 200:
          _context.next = 202;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRODUCTS].PRODUCT_TAB_BAR_SAGA, products.productTabBarSaga);

        case 202:
          _context.next = 204;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PRODUCTS].GET_DEPENDANTS, products.getDependants);

        case 204:
          _context.next = 206;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.PRODUCTS].GET_PRODUCT_LIST, _ACTION_TYPES2.default[REDUCER_TYPES.PRODUCTS].UPDATE_CURRENCY, _ACTION_TYPES2.default[REDUCER_TYPES.PRODUCTS].UPDATE_INSURED_CID], products.getProductList);

        case 206:
          _context.next = 208;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT].GET_PROFILE, client.getProfile);

        case 208:
          _context.next = 210;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.CLIENT].GET_CONTACT_LIST, client.getContactList);

        case 210:
          _context.next = 212;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].INIT_PDA, fna.initPDA);

        case 212:
          _context.next = 214;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].SAVE_PDA, fna.savePDA);

        case 214:
          _context.next = 216;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA, fna.validateNA);

        case 216:
          _context.next = 218;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].VALIDATE_NA, fna.updateNAValidStatus);

        case 218:
          _context.next = 220;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_FE, fna.validateFE);

        case 220:
          _context.next = 222;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].INIT_NA_ANALYSIS, fna.initNaAnalysis);

        case 222:
          _context.next = 224;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].INIT_COMPLETED_STEP_SAGA, fna.initCompletedStepSaga);

        case 224:
          _context.next = 226;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].IAR_RATE_ON_CHANGE, fna.roiValidation);

        case 226:
          _context.next = 228;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].IAR_RATE_NOT_MATCH_REASON_ON_CHANGE, fna.iarRateNotMatchReasonSaga);

        case 228:
          _context.next = 230;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_DATA, fna.updateNaAnalysisDataAndValidate);

        case 230:
          _context.next = 232;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_DATASET, fna.updateNaAnalysis);

        case 232:
          _context.next = 234;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS, fna.updateNaAspects);

        case 234:
          _context.next = 236;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].SAVE_FE, fna.saveFE);

        case 236:
          _context.next = 238;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].SAVE_LAST_SELECTED_PRODUCT, fna.updateLastSelectedProduct);

        case 238:
          _context.next = 240;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY, fna.resetProductTypeCkaRaIsValid);

        case 240:
          _context.next = 242;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].INIT_PRODUCT_TYPES, fna.initProductTypesSaga);

        case 242:
          _context.next = 244;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].GET_FNA, fna.getFNA);

        case 244:
          _context.next = 246;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].SAVE_NA, fna.saveNA);

        case 246:
          _context.next = 248;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP, fna.updateProductTypesCompletedStep);

        case 248:
          _context.next = 250;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].SEND_EMAIL, fna.sendEmail);

        case 250:
          _context.next = 252;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].INIT_CKA, fnaCKA.initCKA);

        case 252:
          _context.next = 254;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].UPDATE_CKA_INIT, fnaCKA.updateCKAinit);

        case 254:
          _context.next = 256;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].COURSE_ON_CHANGE, fnaCKA.courseSaga);

        case 256:
          _context.next = 258;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].INSTITUTION_ON_CHANGE, fnaCKA.institutionSaga);

        case 258:
          _context.next = 260;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].STUDYPERIODENDYEAR_ON_CHANGE, fnaCKA.studyPeriodEndYearSaga);

        case 260:
          _context.next = 262;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].COLLECTIVEINVESTMENT_ON_CHANGE, fnaCKA.collectiveInvestmentSaga);

        case 262:
          _context.next = 264;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].TRANSACTIONTYPE_ON_CHANGE, fnaCKA.transactionTypeSaga);

        case 264:
          _context.next = 266;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].INSURANCEINVESTMENT_ON_CHANGE, fnaCKA.insuranceInvestmentSaga);

        case 266:
          _context.next = 268;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].INSURANCETYPE_ON_CHANGE, fnaCKA.insuranceTypeSaga);

        case 268:
          _context.next = 270;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].PROFESSION_ON_CHANGE, fnaCKA.professionSaga);

        case 270:
          _context.next = 272;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].YEARS_ON_CHANGE, _ACTION_TYPES2.default[REDUCER_TYPES.FNA_CKA].YEARS_ON_BLUR], fnaCKA.yearsSaga);

        case 272:
          _context.next = 274;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].INIT_RA, fnaRA.initRA);

        case 274:
          _context.next = 276;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].UPDATE_RA_INIT, fnaRA.updateRAinit);

        case 276:
          _context.next = 278;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].RISKPOTENTIALRETURN_ON_CHANGE, fnaRA.riskPotentialReturnSaga);

        case 278:
          _context.next = 280;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].AVGAGRETURN_ON_CHANGE, fnaRA.avgAGReturnSaga);

        case 280:
          _context.next = 282;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].SMDROPED_ON_CHANGE, fnaRA.smDropedSaga);

        case 282:
          _context.next = 284;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].ALOFLOSSES_ON_CHANGE, fnaRA.alofLossesSaga);

        case 284:
          _context.next = 286;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].EXPINVTIME_ON_CHANGE, fnaRA.expInvTimeSaga);

        case 286:
          _context.next = 288;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].INVPREF_ON_CHANGE, fnaRA.invPrefSaga);

        case 288:
          _context.next = 290;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE, fnaRA.selfSelectedRiskLevelSaga);

        case 290:
          _context.next = 292;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA_RA].SELF_RL_REASON_RP_ON_CHANGE, fnaRA.selfRLReasonRpSaga);

        case 292:
          _context.next = 294;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].GET_FNA_REPORT, fna.loadFNAReport);

        case 294:
          _context.next = 296;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].OPEN_EMAIL_DIALOG, fna.openEmailDialog);

        case 296:
          _context.next = 298;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].INITIAL_TRUSTED_INDIVIDUAL_FORM, fna.initialValidateTrustedIndividualSaga);

        case 298:
          _context.next = 300;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.FNA].TRUSTED_INDIVIDUAL_GIVEN_NAME_ON_CHANGE, fna.validateGivenName);

        case 300:
          _context.next = 302;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_GEN_FNA, application.genFNA);

        case 302:
          _context.next = 304;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_PERSONAL_DETAILS_ADDRESS, application.updatePersonalDetailsAddress);

        case 304:
          _context.next = 306;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_POLICY_NUMBER, application.updatePolicyNumber);

        case 306:
          _context.next = 308;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_APPLY_APPLICATION_FORM, application.applyApplicationForm);

        case 308:
          _context.next = 310;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_CONTINUE_APPLICATION_FORM, application.continueApplicationForm);

        case 310:
          _context.next = 312;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_CONTINUE_APPLICATION_FORM_SHIELD, application.continueApplicationFormShield);

        case 312:
          _context.next = 314;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_CHANGE_TAB_SHIELD, application.changeTabShield);

        case 314:
          _context.next = 316;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_APPLICATION_FORM_PERSONAL_DETAILS, application.updateApplicationFormPersonalDetails);

        case 316:
          _context.next = 318;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_BEFORE, application.saveApplicationFormBefore);

        case 318:
          _context.next = 320;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_SHIELD_BEFORE, application.saveApplicationFormShieldBefore);

        case 320:
          _context.next = 322;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_APPLICATION_FORM_VALUES, application.updateApplicationFormDynamicQuestions);

        case 322:
          _context.next = 324;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_DYNAMIC_APPLICATION_FORM_ADDRESS, application.updateDynamicApplicationFormAddress);

        case 324:
          _context.next = 326;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_ROP_TABLE, application.updateROPTable);

        case 326:
          _context.next = 328;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_SAVE_APPLICATION_FORM_TABLE_RECORD, application.saveApplicationFormTableRecord);

        case 328:
          _context.next = 330;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_DELETE_APPLICATION_FORM_TABLE_RECORD, application.deleteApplicationFormTableRecord);

        case 330:
          _context.next = 332;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_APPLICATION_RERENDER_PAGE, application.reRenderPage);

        case 332:
          _context.next = 334;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_EAPP_STEP, application.updateEappStep);

        case 334:
          _context.next = 336;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_APPLICATION_BACKDATE, application.updateApplicationBackDate);

        case 336:
          _context.next = 338;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_INVALIDATE_APPLICATION, application.invalidateApplication);

        case 338:
          _context.next = 340;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_SHOWN_SIGNATURE_EXPIRY_ALERT, application.updateShownSignatureExpiryAlert);

        case 340:
          _context.next = 342;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST, application.closeDialogAndGetApplicationList);

        case 342:
          _context.next = 344;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_REDIRECT_TO_FNA, application.redirectToFNA);

        case 344:
          _context.next = 346;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_SELECTED_SECTION_KEY, application.updateSelectedSectionKey);

        case 346:
          _context.next = 348;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].SAGA_GET_APPLICATIONS_AFTER_SUBMISSION, application.getApplicationsAfterSubmission);

        case 348:
          _context.next = 350;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_SAVE_SUBMITTED_DOCUMENT, supportingDocument.saveSubmittedDocument);

        case 350:
          _context.next = 352;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_GET_SUPPORTING_DOCUMENT_DATA, supportingDocument.getSupportingDocument);

        case 352:
          _context.next = 354;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_VALIDATE_SUPPORTING_DOCUMENT, supportingDocument.validateSupportingDocument);

        case 354:
          _context.next = 356;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_ADD_OTHER_SUPPORTING_DOCUMENT, supportingDocument.addOtherSupportingDocument);

        case 356:
          _context.next = 358;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_EDIT_OTHER_SUPPORTING_DOCUMENT, supportingDocument.editOtherDocumentSection);

        case 358:
          _context.next = 360;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_DELETE_OTHER_DOCUMENT_SECTION, supportingDocument.deleteOtherDocumentSection);

        case 360:
          _context.next = 362;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_SAVE_SUPPORTING_DOCUMENT_DATA, supportingDocument.saveSupportingDocument);

        case 362:
          _context.next = 364;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_DELETE_FILE_SUPPORTING_DOCUMENT, supportingDocument.deleteFile);

        case 364:
          _context.next = 366;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_ON_FILE_UPLOAD_SUPPORTING_DOCUMENT, supportingDocument.fileUpload);

        case 366:
          _context.next = 368;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SUPPORTING_DOCUMENT].SAGA_GET_SUPPORTING_DOCUMENT_DATA_SHIELD, supportingDocument.getSupportingDocumentShield);

        case 368:
          _context.next = 370;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_PAYMENT, application.getApplication);

        case 370:
          _context.next = 372;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_SUBMISSION, application.getApplication);

        case 372:
          _context.next = 374;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].VALIDATE_PAYMENT, application.validatePayment);

        case 374:
          _context.next = 376;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.APPLICATION].VALIDATE_SUBMISSION, application.validateSubmission);

        case 376:
          _context.next = 378;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT, paymentAndSubmission.getPayment);

        case 378:
          _context.next = 380;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_SUBMISSION, paymentAndSubmission.getSubmission);

        case 380:
          _context.next = 382;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_UPDATE_PAYMENT_AND_SUBMISSION, paymentAndSubmission.updatePayment);

        case 382:
          _context.next = 384;
          return (0, _effects.takeLatest)([_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].BEFORE_UPDATE_PAYMENT_STATUS, _ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION], paymentAndSubmission.beforeUpdatePaymentStatus);

        case 384:
          _context.next = 386;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_UPDATE_PAYMENT_METHODS, paymentAndSubmission.updatePaymentMethods);

        case 386:
          _context.next = 388;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_SUBMIT_APPLICATION, paymentAndSubmission.callApiSubmitApplication);

        case 388:
          _context.next = 390;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC, paymentAndSubmission.eappDataSync);

        case 390:
          _context.next = 392;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].REFRESH_PAYMENT_STATUS, paymentAndSubmission.refreshPaymentStatus);

        case 392:
          _context.next = 394;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].GET_PAYMENT_URL, paymentAndSubmission.getPaymentURL);

        case 394:
          _context.next = 396;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SIGNATURE].SAGA_GET_SIGNATURE, signature.getSignature);

        case 396:
          _context.next = 398;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SIGNATURE].SAGA_GET_SIGNATURE_SIGNATURE, signature.getSignatureShield);

        case 398:
          _context.next = 400;
          return (0, _effects.takeLatest)(_ACTION_TYPES2.default[REDUCER_TYPES.SIGNATURE].SAGA_SAVE_SIGNED_PDF, signature.saveSignedPdf);

        case 400:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this);
}