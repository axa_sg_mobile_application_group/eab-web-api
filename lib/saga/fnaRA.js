"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateRAisValid = updateRAisValid;
exports.updateRAinit = updateRAinit;
exports.initRA = initRA;
exports.riskPotentialReturnSaga = riskPotentialReturnSaga;
exports.avgAGReturnSaga = avgAGReturnSaga;
exports.smDropedSaga = smDropedSaga;
exports.alofLossesSaga = alofLossesSaga;
exports.expInvTimeSaga = expInvTimeSaga;
exports.invPrefSaga = invPrefSaga;
exports.selfSelectedRiskLevelSaga = selfSelectedRiskLevelSaga;
exports.selfRLReasonRpSaga = selfRLReasonRpSaga;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _PRODUCT_TYPES = require("../constants/PRODUCT_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(updateRAisValid),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(updateRAinit),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(initRA),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(riskPotentialReturnSaga),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(avgAGReturnSaga),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(smDropedSaga),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(alofLossesSaga),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(expInvTimeSaga),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(invPrefSaga),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(selfSelectedRiskLevelSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(selfRLReasonRpSaga);

function calAssessedRL(raData) {
  var riskPotentialReturn = Number(raData.riskPotentialReturn) || 0;
  var avgAGReturn = Number(raData.avgAGReturn) || 0;
  var smDroped = Number(raData.smDroped) || 0;
  var alofLosses = Number(raData.alofLosses) || 0;
  var expInvTime = Number(raData.expInvTime) || 0;
  var invPref = Number(raData.invPref) || 0;

  var source = riskPotentialReturn + avgAGReturn + smDroped + alofLosses + expInvTime + invPref;

  if (source > 26) {
    return 5;
  } else if (source > 21) {
    return 4;
  } else if (source > 14) {
    return 3;
  } else if (source > 9) {
    return 2;
  }
  return 1;
}

function updateRAisValid() {
  var raData, prodType, ckaSection, prodTypeArr, isCompleted, raIsValid;
  return regeneratorRuntime.wrap(function updateRAisValid$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 2:
          raData = _context.sent;
          _context.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.productType.prodType;
          });

        case 5:
          prodType = _context.sent;
          _context.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection;
          });

        case 8:
          ckaSection = _context.sent;
          prodTypeArr = prodType.split(",");
          isCompleted = true;
          raIsValid = true;


          if (raData.riskPotentialReturn === null || raData.avgAGReturn === null || raData.smDroped === null || raData.alofLosses === null || raData.expInvTime === null || raData.invPref === null) {
            raIsValid = false;
          }

          if (raData.selfSelectedriskLevel !== "" && raData.selfSelectedriskLevel !== raData.assessedRL && raData.selfRLReasonRP === "") {
            raIsValid = false;
          }

          if (prodTypeArr.indexOf(_PRODUCT_TYPES.INVESTLINKEDPLANS) > -1 && (!ckaSection.isValid || !raIsValid) || prodTypeArr.indexOf(_PRODUCT_TYPES.PARTICIPATINGPLANS) > -1 && !raIsValid) {
            isCompleted = false;
          }

          _context.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_RA_IS_VALID,
            isValid: raIsValid
          });

        case 17:
          _context.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_COMPLETED,
            isCompleted: isCompleted
          });

        case 19:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this);
}

function updateRAinit(action) {
  var raData;
  return regeneratorRuntime.wrap(function updateRAinit$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context2.sent;

          if (!raData.init) {
            _context2.next = 9;
            break;
          }

          _context2.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_RA_OWNER_INIT,
            newInit: false
          });

        case 7:
          _context2.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 9:
          if (action && _.isFunction(action.callback)) {
            action.callback();
          }
          _context2.next = 14;
          break;

        case 12:
          _context2.prev = 12;
          _context2.t0 = _context2["catch"](0);

        case 14:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 12]]);
}

function initRA(action) {
  var raSection, raData, noError;
  return regeneratorRuntime.wrap(function initRA$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection;
          });

        case 3:
          raSection = _context3.sent;
          raData = raSection.owner;
          noError = true;


          if (raData.riskPotentialReturn === null || raData.avgAGReturn === null || raData.smDroped === null || raData.alofLosses === null || raData.expInvTime === null || raData.invPref === null) {
            noError = false;
          }

          if (raData.selfSelectedriskLevel !== "" && raData.selfSelectedriskLevel !== raData.assessedRL && raData.selfRLReasonRP === "") {
            noError = false;
          }

          // if it already completed but it shows incomplete,
          // it won't trigger the save action.
          // So, set NA is changed, in order to trigger the save action.

          if (!(noError && !raSection.isValid)) {
            _context3.next = 11;
            break;
          }

          _context3.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 11:
          _context3.next = 13;
          return updateRAisValid();

        case 13:
          _context3.next = 15;
          return updateRAinit();

        case 15:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context3.next = 20;
          break;

        case 18:
          _context3.prev = 18;
          _context3.t0 = _context3["catch"](0);

        case 20:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 18]]);
}

function riskPotentialReturnSaga() {
  var raData;
  return regeneratorRuntime.wrap(function riskPotentialReturnSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context4.sent;
          _context4.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context4.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context4.next = 10;
          return updateRAisValid();

        case 10:
          _context4.next = 14;
          break;

        case 12:
          _context4.prev = 12;
          _context4.t0 = _context4["catch"](0);

        case 14:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 12]]);
}
function avgAGReturnSaga() {
  var raData;
  return regeneratorRuntime.wrap(function avgAGReturnSaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context5.sent;
          _context5.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context5.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context5.next = 10;
          return updateRAisValid();

        case 10:
          _context5.next = 14;
          break;

        case 12:
          _context5.prev = 12;
          _context5.t0 = _context5["catch"](0);

        case 14:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 12]]);
}
function smDropedSaga() {
  var raData;
  return regeneratorRuntime.wrap(function smDropedSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context6.sent;
          _context6.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context6.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context6.next = 10;
          return updateRAisValid();

        case 10:
          _context6.next = 14;
          break;

        case 12:
          _context6.prev = 12;
          _context6.t0 = _context6["catch"](0);

        case 14:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 12]]);
}
function alofLossesSaga() {
  var raData;
  return regeneratorRuntime.wrap(function alofLossesSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context7.sent;
          _context7.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context7.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context7.next = 10;
          return updateRAisValid();

        case 10:
          _context7.next = 14;
          break;

        case 12:
          _context7.prev = 12;
          _context7.t0 = _context7["catch"](0);

        case 14:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 12]]);
}
function expInvTimeSaga() {
  var raData;
  return regeneratorRuntime.wrap(function expInvTimeSaga$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context8.sent;
          _context8.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context8.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context8.next = 10;
          return updateRAisValid();

        case 10:
          _context8.next = 14;
          break;

        case 12:
          _context8.prev = 12;
          _context8.t0 = _context8["catch"](0);

        case 14:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 12]]);
}
function invPrefSaga() {
  var raData;
  return regeneratorRuntime.wrap(function invPrefSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection.owner;
          });

        case 3:
          raData = _context9.sent;
          _context9.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL,
            newAssessedRL: calAssessedRL(raData)
          });

        case 6:
          _context9.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context9.next = 10;
          return updateRAisValid();

        case 10:
          _context9.next = 14;
          break;

        case 12:
          _context9.prev = 12;
          _context9.t0 = _context9["catch"](0);

        case 14:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 12]]);
}
function selfSelectedRiskLevelSaga() {
  return regeneratorRuntime.wrap(function selfSelectedRiskLevelSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 3:
          _context10.next = 5;
          return updateRAisValid();

        case 5:
          _context10.next = 9;
          break;

        case 7:
          _context10.prev = 7;
          _context10.t0 = _context10["catch"](0);

        case 9:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 7]]);
}
function selfRLReasonRpSaga() {
  return regeneratorRuntime.wrap(function selfRLReasonRpSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 3:
          _context11.next = 5;
          return updateRAisValid();

        case 5:
          _context11.next = 9;
          break;

        case 7:
          _context11.prev = 7;
          _context11.t0 = _context11["catch"](0);

        case 9:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 7]]);
}