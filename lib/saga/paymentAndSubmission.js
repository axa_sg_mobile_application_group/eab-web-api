"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initValidatePayment = exports.cpfBlockValidation = exports.initCPFBlock = exports.initSRSBlock = exports.srsBlockValidation = exports.triggerExtraValidation = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.getSubmission = getSubmission;
exports.getPayment = getPayment;
exports.updatePaymentMethods = updatePaymentMethods;
exports.updatePaymentMethodsShield = updatePaymentMethodsShield;
exports.updatePayment = updatePayment;
exports.submitPaymentAndSubmission = submitPaymentAndSubmission;
exports.saveSubmissionShield = saveSubmissionShield;
exports.refreshPaymentStatus = refreshPaymentStatus;
exports.beforeUpdatePaymentStatus = beforeUpdatePaymentStatus;
exports.getPaymentURL = getPaymentURL;
exports.eappDataSync = eappDataSync;
exports.callApiSubmitApplication = callApiSubmitApplication;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _application = require("../utilities/application");

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _EAPP = require("../constants/EAPP");

var _EAPP2 = _interopRequireDefault(_EAPP);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _supportingDocument = require("./supportingDocument");

var supportingDocument = _interopRequireWildcard(_supportingDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getSubmission),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(getPayment),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(updatePaymentMethods),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updatePaymentMethodsShield),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(updatePayment),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(submitPaymentAndSubmission),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(saveSubmissionShield),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(refreshPaymentStatus),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(beforeUpdatePaymentStatus),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(getPaymentURL),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(eappDataSync),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(callApiSubmitApplication);

var _EAPP$fieldId$PAYMENT = _EAPP2.default.fieldId[_REDUCER_TYPES.PAYMENT],
    INITPAYMETHOD = _EAPP$fieldId$PAYMENT.INITPAYMETHOD,
    TELETRANSFTER = _EAPP$fieldId$PAYMENT.TELETRANSFTER,
    TTREMITTINGBANK = _EAPP$fieldId$PAYMENT.TTREMITTINGBANK,
    TTDOR = _EAPP$fieldId$PAYMENT.TTDOR,
    TTREMARKS = _EAPP$fieldId$PAYMENT.TTREMARKS,
    SRSBLOCK = _EAPP$fieldId$PAYMENT.SRSBLOCK;
var SRS = _EAPP2.default.fieldValue[_REDUCER_TYPES.PAYMENT].SRS;
var triggerExtraValidation = exports.triggerExtraValidation = function triggerExtraValidation(_ref) {
  var dataObj = _ref.dataObj,
      errorObj = _ref.errorObj;

  var fieldTriggerList = [{
    id: INITPAYMETHOD,
    value: TELETRANSFTER,
    extraCheckPathList: [TTREMITTINGBANK, TTDOR, TTREMARKS]
  }];
  fieldTriggerList.forEach(function (fieldTrigger) {
    var extraCheckPathList = fieldTrigger.extraCheckPathList,
        id = fieldTrigger.id,
        value = fieldTrigger.value;

    extraCheckPathList.forEach(function (path) {
      _.set(errorObj, path, validation.validateMandatory({
        field: {
          type: _FIELD_TYPES.TEXT_FIELD,
          mandatory: dataObj[id] === value,
          disabled: false
        },
        value: _.get(dataObj, path)
      }));
    });
  });
};

var srsBlockValidation = exports.srsBlockValidation = function srsBlockValidation(_ref2) {
  var dataObj = _ref2.dataObj,
      errorObj = _ref2.errorObj;

  var srsBlock = dataObj[SRSBLOCK];
  var errorObjClear = function errorObjClear(dataGroup) {
    dataGroup.forEach(function (key) {
      if (errorObj[key]) {
        errorObj[key].hasError = false;
      }
    });
  };
  var groupList = {
    dateGroup: ["srsFundReqDate", "srsFundProcessDate"],
    bankGroup: ["srsBankNamePicker", "srsInvmAcctNo"],
    declareGroup: ["srsAcctDeclare"]
  };

  Object.keys(groupList).forEach(function (group) {
    groupList[group].forEach(function (key) {
      if (errorObj[key]) {
        errorObj[key].hasError = errorObj[key].message.ENGLISH !== "";
      }
    });
  });

  if (srsBlock.srsIsHaveSrsAcct) {
    if (srsBlock.srsIsHaveSrsAcct === "Y") {
      if (srsBlock.srsFundReqDate !== "Y") {
        // Include "N" and ""
        errorObjClear(groupList.dateGroup);
      }
      errorObjClear(groupList.declareGroup);
    } else if (srsBlock.srsIsHaveSrsAcct === "N") {
      errorObjClear(groupList.bankGroup);
      errorObjClear(groupList.dateGroup);
    }
  } else {
    errorObj.srsIsHaveSrsAcct = validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_FIELD,
        mandatory: true,
        disabled: false
      },
      value: ""
    });
  }
};

var initSRSBlock = exports.initSRSBlock = function initSRSBlock(values) {
  var srsBlockKey = ["srsIsHaveSrsAcct", "srsBankNamePicker", "srsInvmAcctNo", "srsFundReqDate", "srsFundProcessDate", "srsAcctDeclare"];
  srsBlockKey.forEach(function (key) {
    if (!_.isInteger(values[SRSBLOCK][key]) && (values[SRSBLOCK][key] || "").toString().length === 0) {
      values[SRSBLOCK][key] = "";
    }
  });
};

var initCPFBlock = exports.initCPFBlock = function initCPFBlock(values, blockName) {
  var blockKey = [blockName + "IsHaveCpfAcct", blockName + "BankNamePicker", blockName + "InvmAcctNo", blockName + "FundReqDate", blockName + "FundProcessDate", blockName + "AcctDeclare"];
  blockKey.forEach(function (key) {
    if (!_.isInteger(values[blockName + "Block"][key]) && (values[blockName + "Block"][key] || "").toString().length === 0) {
      values[blockName + "Block"][key] = "";
    }
  });
};

var cpfBlockValidation = exports.cpfBlockValidation = function cpfBlockValidation(_ref3) {
  var dataObj = _ref3.dataObj,
      blockName = _ref3.blockName,
      errorObj = _ref3.errorObj;

  var block = dataObj[blockName + "Block"];
  var errorObjClear = function errorObjClear(dataGroup) {
    dataGroup.forEach(function (key) {
      if (errorObj[key]) {
        errorObj[key].hasError = false;
      }
    });
  };
  var groupList = {
    dateGroup: [blockName + "FundReqDate", blockName + "FundProcessDate"],
    bankGroup: [blockName + "BankNamePicker", blockName + "InvmAcctNo"],
    declareGroup: [blockName + "AcctDeclare"]
  };

  Object.keys(groupList).forEach(function (group) {
    groupList[group].forEach(function (key) {
      if (errorObj[key]) {
        errorObj[key].hasError = errorObj[key].message.ENGLISH !== "";
      }
    });
  });

  if (blockName === "cpfissa") {
    if (block[blockName + "FundReqDate"] !== "Y") {
      // Include "N" and ""
      errorObjClear(groupList.dateGroup);
    }
  } else if (block[blockName + "IsHaveCpfAcct"]) {
    if (block[blockName + "IsHaveCpfAcct"] === "Y") {
      if (block[blockName + "FundReqDate"] !== "Y") {
        // Include "N" and ""
        errorObjClear(groupList.dateGroup);
      }
      errorObjClear(groupList.declareGroup);
    } else if (block[blockName + "IsHaveCpfAcct"] === "N") {
      errorObjClear(groupList.bankGroup);
      errorObjClear(groupList.dateGroup);
    }
  } else {
    errorObj[blockName + "IsHaveCpfAcct"] = validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_FIELD,
        mandatory: true,
        disabled: false
      },
      value: ""
    });
  }
};

var initValidatePayment = exports.initValidatePayment = function initValidatePayment(_ref4) {
  var dataObj = _ref4.dataObj,
      errorObj = _ref4.errorObj;

  var validateList = [TTREMITTINGBANK, TTDOR, TTREMARKS];
  if (!dataObj.initPayMethod) {
    dataObj.initPayMethod = "";
  }

  Object.entries(dataObj).forEach(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
        key = _ref6[0],
        value = _ref6[1];

    if (key === INITPAYMETHOD) {
      _.set(errorObj, key, validation.validateApplicationInitPaymentMethod(dataObj));
    } else if (validateList.indexOf(key) !== -1) {
      _.set(errorObj, key, validation.validateMandatory({
        field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
        value: value
      }));
    } else if (key === SRSBLOCK) {
      Object.keys(value).forEach(function (srsKey) {
        if (srsKey !== "idCardNo") {
          if (srsKey === "srsInvmAcctNo") {
            _.set(errorObj, srsKey, validation.validateBankNumber({
              field: {
                type: _FIELD_TYPES.TEXT_FIELD,
                mandatory: true,
                disabled: false,
                bankName: value.srsBankNamePicker
              },
              value: value[srsKey]
            }));
          } else {
            _.set(errorObj, srsKey, validation.validateMandatory({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: value[srsKey]
            }));
          }
        }
      });
    } else if (key === "cpfisoaBlock") {
      Object.keys(value).forEach(function (cpfKey) {
        if (cpfKey !== "idCardNo") {
          if (cpfKey === dataObj.initPayMethod + "InvmAcctNo") {
            _.set(errorObj, cpfKey, validation.validateBankNumber({
              field: {
                type: _FIELD_TYPES.TEXT_FIELD,
                mandatory: true,
                disabled: false,
                bankName: value[dataObj.initPayMethod + "BankNamePicker"],
                cpf: true
              },
              value: value[cpfKey]
            }));
          } else {
            _.set(errorObj, cpfKey, validation.validateMandatory({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: value[cpfKey]
            }));
          }
        }
      });
    } else if (key === "cpfissaBlock") {
      Object.keys(value).forEach(function (cpfKey) {
        if (cpfKey === dataObj.initPayMethod + "FundReqDate" && value[dataObj.initPayMethod + "FundReqDate"] === "Y") {
          _.set(errorObj, dataObj.initPayMethod + "FundProcessDate", validation.validateMandatory({
            field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
            value: value[dataObj.initPayMethod + "FundProcessDate"]
          }));
        }
      });
    }
  });

  if (dataObj.initPayMethod === SRS) {
    srsBlockValidation({
      dataObj: dataObj,
      errorObj: errorObj
    });
  } else if (dataObj.initPayMethod === "cpfissa" || dataObj.initPayMethod === "cpfisoa") {
    cpfBlockValidation({
      dataObj: dataObj,
      blockName: dataObj.initPayMethod,
      errorObj: errorObj
    });
  }
  triggerExtraValidation({ dataObj: dataObj, errorObj: errorObj });
};

function getSubmission(action) {
  var callServer, docId, submissionResponse;
  return regeneratorRuntime.wrap(function getSubmission$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          docId = action.docId;
          _context.next = 7;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getSubmissionTemplateValues",
              docId: docId
            }
          });

        case 7:
          submissionResponse = _context.sent;

          if (!submissionResponse.success) {
            _context.next = 11;
            break;
          }

          _context.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newSubmission: submissionResponse
          });

        case 11:
          _context.next = 15;
          break;

        case 13:
          _context.prev = 13;
          _context.t0 = _context["catch"](0);

        case 15:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 13]]);
}

function getPayment(action) {
  var callServer, application, errorObj, docId, callback, paymentResponse, newApplication;
  return regeneratorRuntime.wrap(function getPayment$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 6:
          application = _context2.sent;
          _context2.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error;
          });

        case 9:
          errorObj = _context2.sent;
          docId = action.docId, callback = action.callback;
          _context2.next = 13;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getPaymentTemplateValues",
              docId: docId
            }
          });

        case 13:
          paymentResponse = _context2.sent;

          if (!paymentResponse.success) {
            _context2.next = 32;
            break;
          }

          _context2.next = 17;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              method: "post",
              action: "goApplication",
              id: action.applicationId
            }
          });

        case 17:
          newApplication = _context2.sent;
          _context2.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: newApplication
          });

        case 20:
          _context2.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_STATUS,
            applicationStatus: {
              isMandDocsAllUploaded: paymentResponse.isMandDocsAllUploaded,
              isSubmitted: paymentResponse.isSubmitted,
              success: paymentResponse.success
            }
          });

        case 22:

          if (paymentResponse.values.initPayMethod === SRS) {
            initSRSBlock(paymentResponse.values);
          }

          _context2.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newPayment: paymentResponse
          });

        case 25:

          initValidatePayment({ dataObj: paymentResponse.values, errorObj: errorObj });
          _context2.next = 28;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: errorObj
          });

        case 28:
          _context2.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: 2,
            newCompletedStep: (0, _application.checkPaymentHasError)({ application: application, error: errorObj }) ? 1 : 2
          });

        case 30:
          _context2.next = 32;
          return (0, _effects.call)(supportingDocument.getSupportingDocument, {
            appId: application.id,
            appStatus: _EAPP2.default.appStatus.APPLYING
          });

        case 32:
          if (_.isFunction(callback)) {
            callback();
          }
          _context2.next = 37;
          break;

        case 35:
          _context2.prev = 35;
          _context2.t0 = _context2["catch"](0);

        case 37:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 35]]);
}

function updatePaymentMethods() {
  var application, callServer, errorObj, payment, response;
  return regeneratorRuntime.wrap(function updatePaymentMethods$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context3.sent;
          _context3.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context3.sent;
          _context3.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error;
          });

        case 9:
          errorObj = _context3.sent;
          _context3.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment;
          });

        case 12:
          payment = _context3.sent;

          if (!(payment.values[INITPAYMETHOD] === TELETRANSFTER)) {
            _context3.next = 17;
            break;
          }

          if (!payment.values[TTREMITTINGBANK] && payment.values[TTREMITTINGBANK] !== 0) {
            payment.values[TTREMITTINGBANK] = "";
          } else if (!payment.values[TTDOR] && payment.values[TTDOR] !== 0) {
            payment.values[TTDOR] = "";
          } else if (!payment.values[TTREMARKS] && payment.values[TTREMARKS] !== 0) {
            payment.values[TTREMARKS] = "";
          }
          _context3.next = 34;
          break;

        case 17:
          if (!(payment.values[INITPAYMETHOD] === SRS)) {
            _context3.next = 24;
            break;
          }

          initSRSBlock(payment.values);

          srsBlockValidation({
            dataObj: payment.values,
            errorObj: errorObj
          });
          _context3.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: errorObj
          });

        case 22:
          _context3.next = 34;
          break;

        case 24:
          if (!(payment.values[INITPAYMETHOD] === "cpfissa" || payment.values[INITPAYMETHOD] === "cpfisoa")) {
            _context3.next = 31;
            break;
          }

          initCPFBlock(payment.values, payment.values[INITPAYMETHOD]);

          cpfBlockValidation({
            dataObj: payment.values,
            blockName: payment.values[INITPAYMETHOD],
            errorObj: errorObj
          });
          _context3.next = 29;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: errorObj
          });

        case 29:
          _context3.next = 34;
          break;

        case 31:
          // if payment method !== "teleTransfter", then clear all values under teleTransfter option
          payment.values[TTREMITTINGBANK] = "";
          payment.values[TTDOR] = "";
          payment.values[TTREMARKS] = "";

        case 34:
          _context3.next = 36;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "updatePaymentMethods",
              docId: application.id,
              values: payment.values,
              stepperIndex: 2,
              updateStepper: true
            }
          });

        case 36:
          response = _context3.sent;

          if (!response.success) {
            _context3.next = 44;
            break;
          }

          _context3.next = 40;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: response.application
          });

        case 40:
          _context3.next = 42;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: response.application.appStep,
            newCompletedStep: response.application.appStep
          });

        case 42:
          _context3.next = 44;
          return (0, _effects.call)(supportingDocument.getSupportingDocument, {
            appId: application.id,
            appStatus: _EAPP2.default.appStatus.APPLYING
          });

        case 44:
          _context3.next = 48;
          break;

        case 46:
          _context3.prev = 46;
          _context3.t0 = _context3["catch"](0);

        case 48:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 46]]);
}

function updatePaymentMethodsShield() {
  var application, callServer, errorObj, payment, update, response;
  return regeneratorRuntime.wrap(function updatePaymentMethodsShield$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context4.sent;
          _context4.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context4.sent;
          _context4.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error;
          });

        case 9:
          errorObj = _context4.sent;
          _context4.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment;
          });

        case 12:
          payment = _context4.sent;
          update = true;

          if (payment[INITPAYMETHOD] === TELETRANSFTER && (payment[TTREMITTINGBANK] === "" || payment[TTDOR] === "" || payment[TTREMARKS] === "")) {
            update = false;
          }

          if (!update) {
            _context4.next = 25;
            break;
          }

          _context4.next = 18;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "savePaymentMethods",
              appId: application.id,
              changedValues: payment
            }
          });

        case 18:
          response = _context4.sent;

          if (!response.success) {
            _context4.next = 25;
            break;
          }

          _context4.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newPayment: response.application.payment
          });

        case 22:
          initValidatePayment({
            dataObj: response.application.payment,
            errorObj: errorObj
          });
          _context4.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: errorObj
          });

        case 25:
          _context4.next = 29;
          break;

        case 27:
          _context4.prev = 27;
          _context4.t0 = _context4["catch"](0);

        case 29:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 27]]);
}

function updatePayment(action) {
  var application, payment, errorObj, newErrorObj, path, newValue, validateObj, isShield, callback, newPayment;
  return regeneratorRuntime.wrap(function updatePayment$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          application = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment;
          });

        case 6:
          payment = _context5.sent;
          _context5.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error;
          });

        case 9:
          errorObj = _context5.sent;
          newErrorObj = _.cloneDeep(errorObj);
          path = action.path, newValue = action.newValue, validateObj = action.validateObj, isShield = action.isShield, callback = action.callback;
          newPayment = _.cloneDeep(payment);

          _.set(newPayment, path, newValue);
          _context5.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
            newPayment: newPayment
          });

        case 16:
          if (!validateObj) {
            _context5.next = 20;
            break;
          }

          if (validateObj.mandatory) {
            if (path.includes("srsInvmAcctNo")) {
              _.set(newErrorObj, validateObj.field, validation.validateBankNumber({
                field: {
                  type: _FIELD_TYPES.TEXT_FIELD,
                  mandatory: true,
                  disabled: false,
                  bankName: validateObj.bankName
                },
                value: validateObj.value
              }));
            } else if (path.includes("cpfissaInvmAcctNo") || path.includes("cpfisoaInvmAcctNo")) {
              _.set(newErrorObj, validateObj.field, validation.validateBankNumber({
                field: {
                  type: _FIELD_TYPES.TEXT_FIELD,
                  mandatory: true,
                  disabled: false,
                  bankName: validateObj.bankName,
                  cpf: true
                },
                value: validateObj.value
              }));
            } else {
              _.set(newErrorObj, validateObj.field, validation.validateMandatory({
                field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
                value: validateObj.value
              }));
            }

            if (!isShield && payment.values.initPayMethod === SRS) {
              srsBlockValidation({
                dataObj: newPayment.values,
                errorObj: newErrorObj
              });
            } else if (!isShield && (payment.values.initPayMethod === "cpfissa" || payment.values.initPayMethod === "cpfisoa")) {
              cpfBlockValidation({
                dataObj: newPayment.values,
                blockName: payment.values.initPayMethod,
                errorObj: newErrorObj
              });
            } else {
              triggerExtraValidation({
                dataObj: isShield ? newPayment : newPayment.values,
                errorObj: newErrorObj
              });
            }
          }

          _context5.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: 2,
            newCompletedStep: (0, _application.checkPaymentHasError)({ application: application, error: errorObj }) ? 1 : 2
          });

        case 20:
          _.set(newErrorObj, "initPayMethod", validation.validateApplicationInitPaymentMethod(isShield ? newPayment : newPayment.values));
          _context5.next = 23;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION,
            errorObj: Object.assign({}, newErrorObj)
          });

        case 23:
          if (!isShield) {
            _context5.next = 28;
            break;
          }

          _context5.next = 26;
          return (0, _effects.call)(updatePaymentMethodsShield);

        case 26:
          _context5.next = 30;
          break;

        case 28:
          _context5.next = 30;
          return (0, _effects.call)(updatePaymentMethods);

        case 30:
          if (_.isFunction(callback)) {
            callback();
          }
          _context5.next = 35;
          break;

        case 33:
          _context5.prev = 33;
          _context5.t0 = _context5["catch"](0);

        case 35:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 33]]);
}

function submitPaymentAndSubmission(action) {
  var currentApplication, callServer, application, policyDocument, appPdf, bundle, isShield, callback, response;
  return regeneratorRuntime.wrap(function submitPaymentAndSubmission$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 3:
          currentApplication = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context6.sent;
          application = action.application, policyDocument = action.policyDocument, appPdf = action.appPdf, bundle = action.bundle, isShield = action.isShield, callback = action.callback;
          _context6.next = 10;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "applicationSubmit",
              isShield: isShield,
              application: application,
              policyDocument: policyDocument,
              appPdf: appPdf,
              bundle: bundle
            }
          });

        case 10:
          response = _context6.sent;

          if (!response.success) {
            _context6.next = 20;
            break;
          }

          if (!isShield) {
            _context6.next = 17;
            break;
          }

          _context6.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: application.find(function (value) {
              return value.id === currentApplication.id;
            })
          });

        case 15:
          _context6.next = 19;
          break;

        case 17:
          _context6.next = 19;
          return (0, _effects.call)(getSubmission, { docId: application.id });

        case 19:
          if (_.isFunction(callback)) {
            callback();
          }

        case 20:
          _context6.next = 24;
          break;

        case 22:
          _context6.prev = 22;
          _context6.t0 = _context6["catch"](0);

        case 24:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 22]]);
}

function saveSubmissionShield() {
  var application, callServer, response;
  return regeneratorRuntime.wrap(function saveSubmissionShield$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION];
          });

        case 3:
          application = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 6:
          callServer = _context7.sent;
          _context7.next = 9;
          return (0, _effects.call)(callServer, {
            url: "shieldApplication",
            data: {
              method: "post",
              action: "saveSubmissionValues",
              appId: application.application.id,
              changedValues: application
            }
          });

        case 9:
          response = _context7.sent;

          if (!response.success) {
            _context7.next = 13;
            break;
          }

          _context7.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: response.application
          });

        case 13:
          _context7.next = 17;
          break;

        case 15:
          _context7.prev = 15;
          _context7.t0 = _context7["catch"](0);

        case 17:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 15]]);
}
/**
 * @description this function just used for trigger EASE API to update
 *   application payment status. all change should get by data sync, so just
 *   call it and DO NOT handle anything.
 * */
function refreshPaymentStatus(action) {
  var callback, error, webServiceUrl, authToken, callServer, docId, response;
  return regeneratorRuntime.wrap(function refreshPaymentStatus$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          callback = action.callback, error = action.error;
          _context8.prev = 1;
          _context8.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 4:
          webServiceUrl = _context8.sent;
          _context8.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 7:
          authToken = _context8.sent;
          _context8.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 10:
          callServer = _context8.sent;
          _context8.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application.id;
          });

        case 13:
          docId = _context8.sent;
          _context8.next = 16;
          return (0, _effects.call)(callServer, {
            url: "/payment/status",
            method: "POST",
            headers: {
              Authorization: "Bearer " + authToken
            },
            data: {
              docId: docId
            },
            webServiceUrl: webServiceUrl
          });

        case 16:
          response = _context8.sent;


          if (response.data.status === "success" && response.data.result.isSuccess) {
            if (_.isFunction(callback)) {
              callback();
            }
          } else if (_.isFunction(error)) {
            error("Can not get payment status");
          }
          _context8.next = 23;
          break;

        case 20:
          _context8.prev = 20;
          _context8.t0 = _context8["catch"](1);

          if (_.isFunction(error)) {
            error("Get payment status fail");
          }

        case 23:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[1, 20]]);
}

function beforeUpdatePaymentStatus(action) {
  var newPayment, application, quotation;
  return regeneratorRuntime.wrap(function beforeUpdatePaymentStatus$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          newPayment = action.newPayment;

          if (!newPayment) {
            _context9.next = 13;
            break;
          }

          _context9.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 4:
          application = _context9.sent;
          quotation = application.quotation;

          if (!(quotation && quotation.quotType === "SHIELD")) {
            _context9.next = 11;
            break;
          }

          _context9.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS,
            newStatus: newPayment
          });

        case 9:
          _context9.next = 13;
          break;

        case 11:
          _context9.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS,
            newStatus: newPayment.values
          });

        case 13:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this);
}

function getPaymentURL(action) {
  var callback, alertFunction, prepareDataSync, webServiceUrl, authToken, callServer, application, payment, docId, quotation, isShield, values, response;
  return regeneratorRuntime.wrap(function getPaymentURL$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          callback = action.callback, alertFunction = action.alertFunction, prepareDataSync = action.prepareDataSync;
          _context10.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 4:
          webServiceUrl = _context10.sent;
          _context10.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 7:
          authToken = _context10.sent;
          _context10.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 10:
          callServer = _context10.sent;
          _context10.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].application;
          });

        case 13:
          application = _context10.sent;
          _context10.next = 16;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment;
          });

        case 16:
          payment = _context10.sent;
          docId = application.id, quotation = application.quotation;
          isShield = quotation.quotType === "SHIELD";
          values = isShield ? payment : payment.values;
          _context10.next = 22;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getPaymentUrl",
              docId: docId,
              values: values,
              isShield: isShield,
              authToken: authToken,
              webServiceUrl: webServiceUrl
            }
          });

        case 22:
          response = _context10.sent;

          if (!(response && response.success && response.url && response.url !== "undefined")) {
            _context10.next = 27;
            break;
          }

          callback(response.url);
          _context10.next = 34;
          break;

        case 27:
          _context10.t0 = response.error.response.status;
          _context10.next = _context10.t0 === 409 ? 30 : 32;
          break;

        case 30:
          // already have payment data
          prepareDataSync();
          return _context10.abrupt("break", 34);

        case 32:
          alertFunction();
          return _context10.abrupt("break", 34);

        case 34:
          _context10.next = 38;
          break;

        case 36:
          _context10.prev = 36;
          _context10.t1 = _context10["catch"](0);

        case 38:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 36]]);
}

// TODO
function dataSync(_ref7) {
  var api = _ref7.api,
      environment = _ref7.environment,
      agentCode = _ref7.agentCode,
      loginId = _ref7.loginId,
      authToken = _ref7.authToken,
      isLeftMenu = _ref7.isLeftMenu,
      isBackground = _ref7.isBackground,
      sessionCookieSyncGatewaySession = _ref7.sessionCookieSyncGatewaySession,
      sessionCookieExpiryDate = _ref7.sessionCookieExpiryDate,
      sessionCookiePath = _ref7.sessionCookiePath,
      syncDocumentIds = _ref7.syncDocumentIds;

  return new Promise(function (resolve) {
    api(environment, agentCode, loginId, "Bearer " + authToken, isLeftMenu, isBackground, sessionCookieSyncGatewaySession, sessionCookieExpiryDate, sessionCookiePath, syncDocumentIds, function (response) {
      console.log("dataSync --> call back function");
      console.log(response);
      resolve(true);
    });
  });
}

function eappDataSync(action) {
  var authToken, agentCode, loginId, environment, syncDocumentIds, api, callback, isLeftMenu, isBackground, sessionCookieSyncGatewaySession, sessionCookieExpiryDate, sessionCookiePath, dataSyncPromise;
  return regeneratorRuntime.wrap(function eappDataSync$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.select)(function (state) {
            return state.login.authToken;
          });

        case 3:
          authToken = _context11.sent;
          _context11.next = 6;
          return (0, _effects.select)(function (state) {
            return state.agent.agentCode;
          });

        case 6:
          agentCode = _context11.sent;
          _context11.next = 9;
          return (0, _effects.select)(function (state) {
            return state.login.profileId;
          });

        case 9:
          loginId = _context11.sent;
          _context11.next = 12;
          return (0, _effects.select)(function (state) {
            return state.login.environment;
          });

        case 12:
          environment = _context11.sent;
          _context11.next = 15;
          return (0, _effects.select)(function (state) {
            return state.login.syncDocumentIds;
          });

        case 15:
          syncDocumentIds = _context11.sent;
          api = action.api, callback = action.callback;
          isLeftMenu = false;
          isBackground = false;
          _context11.next = 21;
          return (0, _effects.select)(function (state) {
            return state.login.sessionCookieSyncGatewaySession;
          });

        case 21:
          sessionCookieSyncGatewaySession = _context11.sent;
          _context11.next = 24;
          return (0, _effects.select)(function (state) {
            return state.login.sessionCookieExpiryDate;
          });

        case 24:
          sessionCookieExpiryDate = _context11.sent;
          _context11.next = 27;
          return (0, _effects.select)(function (state) {
            return state.login.sessionCookiePath;
          });

        case 27:
          sessionCookiePath = _context11.sent;
          _context11.next = 30;
          return (0, _effects.call)(dataSync, {
            api: api,
            environment: environment,
            agentCode: agentCode,
            loginId: loginId,
            authToken: authToken,
            isLeftMenu: isLeftMenu,
            isBackground: isBackground,
            sessionCookieSyncGatewaySession: sessionCookieSyncGatewaySession,
            sessionCookieExpiryDate: sessionCookieExpiryDate,
            sessionCookiePath: sessionCookiePath,
            syncDocumentIds: syncDocumentIds
          });

        case 30:
          dataSyncPromise = _context11.sent;

          Promise.all([dataSyncPromise]).then(function (dataSyncStatus) {
            if (_.isFunction(callback)) {
              callback(dataSyncStatus);
            }
          });
          _context11.next = 36;
          break;

        case 34:
          _context11.prev = 34;
          _context11.t0 = _context11["catch"](0);

        case 36:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 34]]);
}

function callApiSubmitApplication(action) {
  var docId, lang, callback, api, authToken, environment, getPaymentStatusResponse;
  return regeneratorRuntime.wrap(function callApiSubmitApplication$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          docId = action.docId, lang = action.lang, callback = action.callback, api = action.api;
          _context12.next = 4;
          return (0, _effects.select)(function (state) {
            return state.login.authToken;
          });

        case 4:
          authToken = _context12.sent;
          _context12.next = 7;
          return (0, _effects.select)(function (state) {
            return state.login.environment;
          });

        case 7:
          environment = _context12.sent;

          // keep
          // const authToken =
          // "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vbWFhbS1kZXYuYXhhLmNvbS9kZXYvandrcz9raWQ9NjBEREE4Rjc2OUMxRTA0NTZBNkZDNjc0QUQ3MDI3RDM5ODc5MkQwRiIsImtpZCI6IjYwRERBOEY3NjlDMUUwNDU2QTZGQzY3NEFENzAyN0QzOTg3OTJEMEYiLCJ4NXUiOiJodHRwczovL21hYW0tZGV2LmF4YS5jb20vZGV2L3g1MDk_eDV0PTYwRERBOEY3NjlDMUUwNDU2QTZGQzY3NEFENzAyN0QzOTg3OTJEMEYiLCJ4NXQiOiI2MEREQThGNzY5QzFFMDQ1NkE2RkM2NzRBRDcwMjdEMzk4NzkyRDBGIn0.ew0KCSJzdWIiOiB7InZhbHVlIjogIkFHMDEzODdTRlNNIn0sDQoJImF1ZCI6ICI0M2RkNzViYy0zY2FlLTQyODEtOTA2Yi1mZTA0MDRkZGFiNzUiLA0KCSJleHAiOiAxNTM4MDM5NjY0LA0KCSJpYXQiOiAxNTM4MDM2MDY0LA0KCSJuYmYiOiAxNTM4MDM2MDU0LA0KCSJpc3MiOiAiaHR0cHM6Ly9tYWFtLWRldi5heGEuY29tIiwNCgkiYW1yIjogIiIsDQoJImN1c3RvbURhdGEiOiB7InVpZCI6IkFHMDEzODdTRlNNIiwicmVzb3VyY2Vfb3duZXIiOiJBRzAxMzg3U0ZTTSIsIm1haWwiOiJhZzAxMzg3c2ZzbUB0ZXN0LmNvbSIsImdpdmVuTmFtZSI6IkFORyIsInNuIjoiQ1lOVEhJQSJ9LA0KCSJzY29wZSI6ICJheGEtc2ctbGlmZWFwaSIsDQoJIm5vbmNlIjogIjhhMzQzNWNlLWM3NzYtNDBjMS1hODBjLWYwODMzZjJmOGY1MiINCn0.IFXAD9yasEKAl8WSVLLL77-4QzlilnI5Ts0KPKTLwWI_IweZWrk_U0UtY1Tq1eDyYgKIDCksPywD1gVbdWMsah5Rnk7zH5sz_tmndEpM9KDqnbNE365mWKDuZMjheUEtwU1LtMWldTXG275INAr5hf7SPV4tmF5E5Fpav1U7S-hpBBp-RwNgwxYgxymPom4vpeJ6_ymMYs_4EZr4VsMtZfh_-783SLXthKDBfbk5zOU_y5TWoiWm_rIPDbdLHmSY-jUKnnZpmK9Oe2smTyUOqY1TNQ8RGFBlnJIwnU1z8kYzJNAUN19ogbEfYS4fH7hSPRqzqX-wb_pW9D2XW4lgvl9fjL7rcGTajScP8jhh2NXmhVtc6C85uX1XGGNGxUZLUXEwEO4mYWRn6lyP5T05lFMIjGSQLILO14a2Ow_Rh9nMYT55y9iVMDcG_AwQJg6_3qKWyyWRrZkFpBiqRI5O4KhzFcXzIHcrg7KZMfMCuccuM2OBVe12PJvYHSxsld0KmPPikaVtHxFKWCoNMryVC45ouJBB4_tE8FBBnhRaDHKfY-F37jQ00Z64pJOOO7D5erbFlgZp_3g3YeB8YsR6ShUw9haV_lm3iZSSPhX881FnTVe8fKaCvBo0HLuIchqaLt14z_QU9XW0eE8GzLPT2i7uNe3S5Vk1BhN7HGozMvM";
          // const getPaymentStatusResponse = yield call(api, authToken, docId);
          getPaymentStatusResponse = api(authToken, docId, environment, lang);

          Promise.all([getPaymentStatusResponse]).then(function (response) {
            if (_.isFunction(callback)) {
              callback(response);
            }
          });
          _context12.next = 14;
          break;

        case 12:
          _context12.prev = 12;
          _context12.t0 = _context12["catch"](0);

        case 14:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 12]]);
}