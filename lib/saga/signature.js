"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSignature = getSignature;
exports.saveSignedPdf = saveSignedPdf;
exports.initSignatureShield = initSignatureShield;
exports.getSignatureShield = getSignatureShield;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _EAPP = require("../constants/EAPP");

var _EAPP2 = _interopRequireDefault(_EAPP);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(getSignature),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(saveSignedPdf),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(initSignatureShield),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(getSignatureShield);

function getSignature(action) {
  var callServer, docId, successCallback, failCallback, newCurrentStep, response, signingTabIdx, allSigned, _response$application, tabCount, isFaChannel, attachments, agentSignFields, clientSignFields, newSignature;

  return regeneratorRuntime.wrap(function getSignature$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          docId = action.docId, successCallback = action.successCallback, failCallback = action.failCallback;
          newCurrentStep = _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE];
          _context.next = 8;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "getSignatureStatusFromCb",
              docId: docId
            }
          });

        case 8:
          response = _context.sent;

          if (!(response.success && response.application)) {
            _context.next = 29;
            break;
          }

          signingTabIdx = 0;
          allSigned = false;

          response.application.attachments.some(function (att) {
            if (att.isSigned) {
              if (signingTabIdx + 1 < response.application.tabCount) {
                signingTabIdx += 1;
              } else if (signingTabIdx + 1 === response.application.tabCount) {
                signingTabIdx = -1;
                allSigned = true;
              }
              return false;
            }
            return true;
          });
          _response$application = response.application, tabCount = _response$application.tabCount, isFaChannel = _response$application.isFaChannel, attachments = _response$application.attachments, agentSignFields = _response$application.agentSignFields, clientSignFields = _response$application.clientSignFields;
          newSignature = {
            isFaChannel: isFaChannel,
            // TODO: update selectedPdfIdx after auto switch is completed
            // selectedPdfIdx: signingTabIdx < 0 ? tabCount - 1 : signingTabIdx,
            selectedPdfIdx: -1,
            signingTabIdx: signingTabIdx,
            isSigningProcess: [],
            numOfTabs: tabCount,
            attachments: attachments,
            attUrls: [],
            agentSignFields: agentSignFields,
            clientSignFields: clientSignFields
          };
          _context.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: newCurrentStep
          });

        case 17:
          if ([_.get(response.crossAge, "insuredStatus"), _.get(response.crossAge, "proposerStatus")].some(function (s) {
            return s === _EAPP2.default.CROSSAGE_STATUS.CROSSED_AGE_SIGNED;
          })) {
            _context.next = 22;
            break;
          }

          _context.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_CROSS_AGE,
            newCrossAge: response.crossAge
          });

        case 20:
          _context.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
            newIsAlertDelayed: newCurrentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsCrossAgeAlertShow: true
          });

        case 22:
          _context.next = 24;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].UPDATE_SIGNATURE,
            newSignature: newSignature
          });

        case 24:
          _context.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].SIGNATURE_SWITCH_PDF,
            newSelectedPdfIdx: allSigned ? 0 : signingTabIdx
          });

        case 26:
          if (_.isFunction(successCallback)) {
            successCallback();
          }
          _context.next = 30;
          break;

        case 29:
          if (_.isFunction(failCallback)) {
            failCallback();
          }

        case 30:
          _context.next = 34;
          break;

        case 32:
          _context.prev = 32;
          _context.t0 = _context["catch"](0);

        case 34:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 32]]);
}

function saveSignedPdf(action) {
  var docId, attId, pdfData, isShield, callback, apiOptions, currentStep, signature, signingTabIdx, numOfTabs, attachments, callServer, response, getResponse, newSelectedPdfIdx, newSigningTabIdx, newAttachments, newCompletedStep, cid;
  return regeneratorRuntime.wrap(function saveSignedPdf$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          docId = action.docId, attId = action.attId, pdfData = action.pdfData, isShield = action.isShield, callback = action.callback;
          apiOptions = {};
          _context2.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.APPLICATION].component.currentStep;
          });

        case 5:
          currentStep = _context2.sent;
          _context2.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SIGNATURE].signature;
          });

        case 8:
          signature = _context2.sent;
          signingTabIdx = signature.signingTabIdx, numOfTabs = signature.numOfTabs, attachments = signature.attachments;
          _context2.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 12:
          callServer = _context2.sent;
          _context2.next = 15;
          return (0, _effects.call)(callServer, {
            url: "application",
            data: {
              action: "saveSignedPdf",
              docId: docId,
              attId: attId,
              pdfData: pdfData
            }
          });

        case 15:
          response = _context2.sent;


          if (isShield) {
            apiOptions = {
              url: "shieldApplication",
              data: {
                action: "getSignatureUpdatedPdfString",
                sdwebDocid: docId + "_" + attId,
                tabIdx: signingTabIdx
              }
            };
          } else {
            apiOptions = {
              url: "application",
              data: {
                action: "getUpdatedAttachmentUrl",
                signDocId: docId + "_" + attId,
                tabIdx: signingTabIdx
              }
            };
          }
          _context2.next = 19;
          return (0, _effects.call)(callServer, apiOptions);

        case 19:
          getResponse = _context2.sent;

          if (!(response.success && getResponse.success)) {
            _context2.next = 38;
            break;
          }

          newSelectedPdfIdx = -1;
          newSigningTabIdx = signingTabIdx !== numOfTabs - 1 ? signingTabIdx + 1 : -1;
          newAttachments = _.cloneDeep(attachments);

          newAttachments[signingTabIdx].isSigned = true;

          getResponse.newPdfStrings.forEach(function (item) {
            newAttachments[item.index].pdfStr = item.pdfStr;
          });
          newAttachments.some(function (value, index) {
            if (value.isSigned) {
              return false;
            }
            newSelectedPdfIdx = index;
            return true;
          });
          newCompletedStep = -1;

          if (isShield) {
            newCompletedStep = response.application.appCompletedStep;
          } else {
            newCompletedStep = newAttachments.findIndex(function (attachment) {
              return !attachment.isSigned;
            }) < 0 ? 1 : 0;
          }

          if (!(newSelectedPdfIdx !== -1)) {
            _context2.next = 32;
            break;
          }

          _context2.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].SIGNATURE_SWITCH_PDF,
            newSelectedPdfIdx: newSelectedPdfIdx
          });

        case 32:
          _context2.next = 34;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].UPDATE_SIGNED_PDF,
            newSigningTabIdx: newSigningTabIdx,
            newAttachments: newAttachments
          });

        case 34:
          _context2.next = 36;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP,
            newCurrentStep: currentStep,
            newCompletedStep: newCompletedStep
          });

        case 36:
          _context2.next = 38;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newTemplate: getResponse.appFormTemplate,
            newApplication: isShield ? response.application : getResponse.application
          });

        case 38:
          if (!_.isFunction(callback)) {
            _context2.next = 45;
            break;
          }

          _context2.next = 41;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 41:
          cid = _context2.sent;
          _context2.next = 44;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].GET_PROFILE,
            cid: cid
          });

        case 44:
          callback();

        case 45:
          _context2.next = 49;
          break;

        case 47:
          _context2.prev = 47;
          _context2.t0 = _context2["catch"](0);

        case 49:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 47]]);
}

function initSignatureShield(action) {
  var signature, application, crossAge, newCurrentStep, allSigned, signingTabIdx, newSignature;
  return regeneratorRuntime.wrap(function initSignatureShield$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          signature = action.signature, application = action.application, crossAge = action.crossAge;
          newCurrentStep = _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE];
          // yield put({
          //   type: ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP,
          //   newCurrentStep
          // });

          allSigned = false;
          signingTabIdx = 0;

          signature.pdfStr.some(function (att, index) {
            if (signature.isSigned[index]) {
              if (signingTabIdx + 1 < signature.numOfTabs) {
                signingTabIdx += 1;
              } else if (signingTabIdx + 1 === signature.numOfTabs) {
                signingTabIdx = -1;
                allSigned = true;
              }
              return false;
            }
            return true;
          });

          newSignature = {
            isFaChannel: signature.isFaChannel,
            // TODO: update selectedPdfIdx after auto switch is completed
            // selectedPdfIdx: signingTabIdx < 0 ? tabCount - 1 : signingTabIdx,
            selectedPdfIdx: -1,
            signingTabIdx: signingTabIdx,
            isSigningProcess: [],
            numOfTabs: signature.numOfTabs,
            attachments: signature.pdfStr.map(function (value, index) {
              return {
                isSigned: signature.isSigned[index],
                pdfStr: value,
                docid: signature.signDocConfig.ids[index]
              };
            }),
            agentSignFields: signature.agentSignFields,
            clientSignFields: signature.clientSignFields
          };
          _context3.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_COMPLETED_STEP,
            newCompletedStep: allSigned ? 1 : 0
          });

        case 9:
          _context3.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION,
            newApplication: application
          });

        case 11:
          _context3.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_CROSS_AGE,
            newCrossAge: crossAge
          });

        case 13:
          _context3.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
            newIsAlertDelayed: newCurrentStep === _EAPP2.default.tabStep[_REDUCER_TYPES.SIGNATURE],
            newIsCrossAgeAlertShow: true
          });

        case 15:
          _context3.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].UPDATE_SIGNATURE,
            newSignature: newSignature
          });

        case 17:
          _context3.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].SIGNATURE_SWITCH_PDF,
            newSelectedPdfIdx: allSigned ? 0 : signingTabIdx
          });

        case 19:
          _context3.next = 23;
          break;

        case 21:
          _context3.prev = 21;
          _context3.t0 = _context3["catch"](0);

        case 23:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 21]]);
}

function getSignatureShield() {
  var signature;
  return regeneratorRuntime.wrap(function getSignatureShield$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.SIGNATURE].signature;
          });

        case 3:
          signature = _context4.sent;
          _context4.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].SIGNATURE_SWITCH_PDF,
            newSelectedPdfIdx: signature.signingTabIdx
          });

        case 6:
          _context4.next = 10;
          break;

        case 8:
          _context4.prev = 8;
          _context4.t0 = _context4["catch"](0);

        case 10:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 8]]);
}