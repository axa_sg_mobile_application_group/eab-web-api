"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.signatureExpiry = signatureExpiry;

var _effects = require("redux-saga/effects");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _marked = /*#__PURE__*/regeneratorRuntime.mark(signatureExpiry);

function signatureExpiry(action) {
  var response, callServer, cid, applicationId;
  return regeneratorRuntime.wrap(function signatureExpiry$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          response = null;
          _context.prev = 1;
          _context.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 4:
          callServer = _context.sent;
          cid = action.cid, applicationId = action.applicationId;
          _context.next = 8;
          return (0, _effects.call)(callServer, {
            url: "invalidation",
            data: {
              action: "signatureExpiry",
              cid: cid,
              applicationId: applicationId
            }
          });

        case 8:
          response = _context.sent;
          _context.next = 14;
          break;

        case 11:
          _context.prev = 11;
          _context.t0 = _context["catch"](1);
          return _context.abrupt("return", null);

        case 14:
          return _context.abrupt("return", response);

        case 15:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[1, 11]]);
}

exports.default = {};