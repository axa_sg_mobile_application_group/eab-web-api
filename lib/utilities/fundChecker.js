"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (_ref) {
  var planDetails = _ref.planDetails,
      quotation = _ref.quotation;

  var bpDetail = planDetails[quotation.baseProductCode];

  return bpDetail && bpDetail.fundInd === "Y";
};