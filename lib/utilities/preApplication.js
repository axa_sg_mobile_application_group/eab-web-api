"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getValidBundleId = exports.prepareProductList = undefined;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _getProductId = require("./getProductId");

var _getProductId2 = _interopRequireDefault(_getProductId);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var prepareProductList = exports.prepareProductList = function prepareProductList(applicationsList) {
  var productList = [];
  _.forEach(applicationsList, function (item) {
    var _item$plans = item.plans,
        plans = _item$plans === undefined ? [] : _item$plans,
        baseProductCode = item.baseProductCode,
        baseProductName = item.baseProductName,
        iCid = item.iCid,
        iCids = item.iCids,
        pCid = item.pCid,
        version = item.version;

    var covName = baseProductName || _.get(plans, "[0].covName");
    var id = _.get(plans, "[0].productId");
    var pIndex = _.findIndex(productList, function (pro) {
      return pro.covCode === baseProductCode;
    });
    if (pIndex < 0) {
      if (_.isArray(iCids) && iCids.length > 0) {
        _.forEach(iCids, function (cid) {
          var product = {
            id: id,
            covCode: baseProductCode,
            covName: covName,
            iCid: cid,
            pCid: pCid,
            ccy: "",
            attachmentId: "thumbnail3",
            version: version
          };
          product.id = id || (0, _getProductId2.default)(product);
          productList.push(product);
        });
      } else {
        var product = {
          id: id,
          covCode: baseProductCode,
          covName: covName,
          iCid: iCid,
          pCid: pCid,
          ccy: "",
          attachmentId: "thumbnail3",
          version: version
        };
        product.id = id || (0, _getProductId2.default)(product);
        productList.push(product);
      }
    }
  });
  return productList;
};

var getValidBundleId = exports.getValidBundleId = function getValidBundleId(profile) {
  return _.get(_.find(_.get(profile, "bundle"), function (bundle) {
    return bundle.isValid;
  }), "id", "");
};