"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getProductId;
/* eslint-disable no-underscore-dangle */
function getProductId(product) {
  switch (true) {
    case !product:
      break;
    case !!product.id:
      return product.id;
    case !!product._id:
      return product._id;
    default:
      {
        var idA = product.compCode || "08";
        var idB = product.covCode;
        var idC = product.version ? product.version : "1";

        return idA + "_product_" + idB + "_" + idC;
      }
  }
  throw new Error("can not get product id.");
}