"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = genProfileMandatory;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _SYSTEM = require("../constants/SYSTEM");

var _SYSTEM2 = _interopRequireDefault(_SYSTEM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var genProductMandatory = function genProductMandatory() {
  var mandatory = ["nationality", "prStatus", "dob", "gender", "isSmoker", "industry", "occupation", "addrCountry", "residenceCountry"];
  return mandatory;
};

var genFnaMandatory = function genFnaMandatory(isSpouse) {
  var mandatory = ["idDocType", "idCardNo", "title", "dob", "marital", "mobileNo", "countryCode", "language", "education", "employStatus", "allowance"];
  if (isSpouse) {
    mandatory.push("relationship");
  }
  return mandatory;
};

var genApplicationMandatory = function genApplicationMandatory(moduleId, hasFNA, isProposer) {
  var mandatory = ["nameOrder", "dob", "nationality", "residenceCountry", "idDocType", "idCardNo", "marital", "isSmoker", "industry", "occupation", "addrCountry", "allowance", "pass"];
  if (isProposer) {
    mandatory = _.concat(mandatory, ["mobileNo", "mobileNoSection", "email"]);
    // only for agent proposer
    if (hasFNA) {
      mandatory = _.concat(mandatory, ["language", "education"]);
    }
  }
  if (hasFNA) {
    mandatory = _.concat(mandatory, ["title"]);
  }

  return mandatory;
};

function genProfileMandatory(moduleId, hasFNA, isProposer, isSpouse) {
  switch (moduleId) {
    case _SYSTEM2.default.FEATURES.FNA:
      return genFnaMandatory(isSpouse);
    case _SYSTEM2.default.FEATURES.FQ:
    case _SYSTEM2.default.FEATURES.QQ:
      return genProductMandatory(moduleId, hasFNA, isProposer, isSpouse);
    case _SYSTEM2.default.FEATURES.EAPP:
      return genApplicationMandatory(moduleId, hasFNA, isProposer);
    default:
      return [];
  }
}