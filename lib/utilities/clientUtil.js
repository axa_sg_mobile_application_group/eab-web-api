"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSpouseProfile = exports.getClientName = undefined;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _DEPENDANT = require("../constants/DEPENDANT");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var getClientName = exports.getClientName = function getClientName(_ref) {
  var profile = _ref.profile,
      dependantProfiles = _ref.dependantProfiles,
      cid = _ref.cid;

  var targetProfile = profile.cid === cid ? profile : dependantProfiles[cid];
  return targetProfile && targetProfile.fullName;
};

var getSpouseProfile = exports.getSpouseProfile = function getSpouseProfile(_ref2) {
  var profile = _ref2.profile,
      dependantProfiles = _ref2.dependantProfiles;

  var spouse = _.find(_.get(profile, "dependants"), function (d) {
    return d.relationship === _DEPENDANT.SPO;
  });
  return _.get(dependantProfiles, _.get(spouse, "cid"));
};