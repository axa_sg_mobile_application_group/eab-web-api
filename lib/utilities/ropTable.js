"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculateRopTable = calculateRopTable;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ROP_TABLE = require("../constants/ROP_TABLE");

var ROP_TABLE = _interopRequireWildcard(_ROP_TABLE);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var REPLACE_POLICY_TYPE_AXA = ROP_TABLE.REPLACE_POLICY_TYPE_AXA,
    REPLACE_POLICY_TYPE_OTHER = ROP_TABLE.REPLACE_POLICY_TYPE_OTHER;
function calculateRopTable(_ref) {
  var rootValues = _ref.rootValues,
      path = _ref.path,
      id = _ref.id,
      pathValue = _ref.pathValue;

  if (!_.isString(id)) {
    return;
  }

  if ([REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER].indexOf(id) > -1) {
    _.set(rootValues, path + "." + id, pathValue);
  } else if (_.isNumber(pathValue)) {
    if (pathValue < 0) {
      return;
    }

    var values = _.get(rootValues, path, {});
    var relatedId = void 0;
    var relatedTotalId = void 0;

    if (id.indexOf("AXA") > -1) {
      relatedId = _.replace(id, "AXA", "Other");
      relatedTotalId = _.replace(id, "AXA", "");
    } else if (id.indexOf("Other") > -1) {
      relatedId = _.replace(id, "Other", "AXA");
      relatedTotalId = _.replace(id, "Other", "");
    }

    if (id.indexOf("exist") > -1 || id.indexOf("replace") > -1) {
      var relatedTotalValue = _.get(values, relatedTotalId, 0);
      if (pathValue > relatedTotalValue) {
        return;
      }
      _.set(values, id, pathValue);
      if (_.isNumber(relatedTotalValue)) {
        _.set(values, relatedId, relatedTotalValue - pathValue);
      }
    } else if (id.indexOf("pending") > -1) {
      var relatedValue = _.get(values, relatedId, 0);
      _.set(values, id, pathValue);
      if (_.isNumber(relatedValue)) {
        _.set(values, relatedTotalId, relatedValue + pathValue);
      }
    }

    _.set(rootValues, path, values);
  }
}

exports.default = {};