"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.naDataGetter = naDataGetter;
exports.validateFiProtection = validateFiProtection;
exports.validateDiProtection = validateDiProtection;
exports.validateCiProtection = validateCiProtection;
exports.validateRPlanning = validateRPlanning;
exports.validatePaProtection = validatePaProtection;
exports.validatePcHeadstart = validatePcHeadstart;
exports.validateHcProtection = validateHcProtection;
exports.validateEPlanning = validateEPlanning;
exports.validateOther = validateOther;
exports.validatePsGoals = validatePsGoals;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _NEEDS = require("../constants/NEEDS");

var _naAnalysis = require("./naAnalysis");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// =============================================================================
// variables
// =============================================================================
var fiProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  unMatchPmtReason: "",
  requireYrIncome: "",
  finExpenses: "",
  othFundNeedsName: "",
  othFundNeeds: "",
  assets: null
};
var diProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  requireYrIncome: "",
  othRegIncome: ""
};
var ciProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: "",
  requireYrIncome: "",
  mtCost: "",
  assets: null
};
var rPlanningInitialState = {
  init: true,
  isActive: false,
  retireAge: 65,
  avgInflatRate: 2.65,
  inReqRetirement: "",
  unMatchPmtReason: "",
  othRegIncome: "",
  assets: null
};
var paProtectionInitialState = {
  init: true,
  isActive: false,
  pmt: ""
};
var pcHeadstartInitialState = {
  init: true,
  isActive: false,
  providedFor: "",
  sumAssuredCritical: "",
  sumAssuredProvided: "",
  totShortfall: "NotShow"
};
var hcProtectionInitialState = {
  init: true,
  isActive: false,
  provideHeadstart: "",
  typeofWard: ""
};
var ePlanningInitialState = {
  init: true,
  isActive: false,
  yrtoSupport: "",
  costofEdu: "",
  avgEduInflatRate: 5,
  assets: null
};
var otherInitialState = {
  init: true,
  isActive: false,
  goalNo: 1,
  goals: [{
    extInsuDisplay: "",
    extInsuSelField: "",
    goalName: "",
    needsValue: "",
    typeofNeeds: ""
  }, {
    extInsuDisplay: "",
    extInsuSelField: "",
    goalName: "",
    needsValue: "",
    typeofNeeds: ""
  }, {
    extInsuDisplay: "",
    extInsuSelField: "",
    goalName: "",
    needsValue: "",
    typeofNeeds: ""
  }]
};
var psGoalsInitialState = {
  init: true,
  isActive: false,
  goalNo: 1,
  goals: [{
    goalName: "",
    timeHorizon: "",
    compFv: "",
    assets: null,
    projMaturity: ""
  }, {
    goalName: "",
    timeHorizon: "",
    compFv: "",
    assets: null,
    projMaturity: ""
  }, {
    goalName: "",
    timeHorizon: "",
    compFv: "",
    assets: null,
    projMaturity: ""
  }]
};
// =============================================================================
// support functions
// =============================================================================
function dependantDataGetter(_ref) {
  var cid = _ref.cid,
      protection = _ref.protection,
      calculator = _ref.calculator,
      naData = _ref.naData,
      dependantFeData = _ref.dependantFeData,
      dependantProfileData = _ref.dependantProfileData,
      initialState = _ref.initialState,
      shouldInitProtection = _ref.shouldInitProtection;

  var protectionDependants = _.cloneDeep(protection.dependants) || [];
  var index = protectionDependants.findIndex(function (dependant) {
    return dependant.cid === cid;
  });

  var dependantProtectionData = calculator({
    protection: Object.assign({
      cid: cid
    }, initialState, _.get(protectionDependants, "[" + index + "]", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: dependantFeData,
    dependantProfileData: dependantProfileData,
    naData: naData
  });

  if (index !== -1) {
    protectionDependants[index] = dependantProtectionData;
  } else {
    protectionDependants.push(dependantProtectionData);
  }

  return protectionDependants;
}

function fiProtectionCalculator(_ref2) {
  var protection = _ref2.protection,
      naData = _ref2.naData,
      dependantFeData = _ref2.dependantFeData;

  var newFiProtection = _.cloneDeep(protection);

  newFiProtection.annualRepIncome = Number(newFiProtection.pmt !== "" ? newFiProtection.pmt : 0) * 12;
  newFiProtection.iarRate2 = (0, _naAnalysis.calIarRate2)(Number(naData.iarRate));
  newFiProtection.lumpSum = (0, _naAnalysis.calLumpSum)(newFiProtection.iarRate2, newFiProtection.requireYrIncome, newFiProtection.pmt);
  newFiProtection.totLiabilities = dependantFeData.liabilities;
  newFiProtection.totRequired = (0, _naAnalysis.calTotRequired)({
    finExpenses: newFiProtection.finExpenses || 0,
    totLiabilities: newFiProtection.totLiabilities,
    lumpSum: newFiProtection.lumpSum,
    othFundNeeds: newFiProtection.othFundNeeds || 0
  });
  newFiProtection.existLifeIns = (0, _naAnalysis.calExitLifeIns)(dependantFeData.lifeInsProt, dependantFeData.invLinkPol);
  newFiProtection.assets = newFiProtection.assets && newFiProtection.assets.map(function (asset) {
    var newAsset = _.cloneDeep(asset);
    newAsset.calAsset = newAsset.usedAsset;
    return newAsset;
  });
  newFiProtection.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.FIPROTECTION, {
    assets: newFiProtection.assets,
    totRequired: newFiProtection.totRequired,
    existLifeIns: newFiProtection.existLifeIns
  });

  return newFiProtection;
}

function diProtectionCalculator(_ref3) {
  var protection = _ref3.protection,
      dependantFeData = _ref3.dependantFeData;

  var newDiProtection = _.cloneDeep(protection);

  newDiProtection.annualPmt = Number(newDiProtection.pmt !== "" ? newDiProtection.pmt : 0) * 12;
  newDiProtection.disabilityBenefit = dependantFeData.disIncomeProt;
  newDiProtection.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.DIPROTECTION, {
    annualPmt: newDiProtection.annualPmt,
    disabilityBenefit: newDiProtection.disabilityBenefit,
    othRegIncome: newDiProtection.othRegIncome || 0
  });

  return newDiProtection;
}

function ciProtectionCalculator(_ref4) {
  var protection = _ref4.protection,
      dependantFeData = _ref4.dependantFeData,
      naData = _ref4.naData;

  var newCiProtection = _.cloneDeep(protection);

  newCiProtection.annualLivingExp = Number(newCiProtection.pmt !== "" ? newCiProtection.pmt : 0) * 12;
  newCiProtection.iarRate2 = (0, _naAnalysis.calIarRate2)(Number(naData.iarRate));
  newCiProtection.lumpSum = (0, _naAnalysis.calLumpSum)(newCiProtection.iarRate2, newCiProtection.requireYrIncome, newCiProtection.pmt);
  newCiProtection.totCoverage = (0, _naAnalysis.calTotCoverage)(newCiProtection.mtCost, newCiProtection.lumpSum);
  newCiProtection.ciProt = dependantFeData.ciPort || 0;
  newCiProtection.assets = newCiProtection.assets && newCiProtection.assets.map(function (asset) {
    var newAsset = _.cloneDeep(asset);
    newAsset.calAsset = newAsset.usedAsset;
    return newAsset;
  });
  newCiProtection.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.CIPROTECTION, {
    assets: newCiProtection.assets,
    totCoverage: newCiProtection.totCoverage,
    ciProt: newCiProtection.ciProt
  });

  return newCiProtection;
}

function rPlanningCalculator(_ref5) {
  var protection = _ref5.protection,
      dependantFeData = _ref5.dependantFeData,
      naData = _ref5.naData,
      dependantProfileData = _ref5.dependantProfileData;

  var newRPlanning = _.cloneDeep(protection);

  newRPlanning.annualInReqRetirement = Number(newRPlanning.inReqRetirement !== "" ? newRPlanning.inReqRetirement : 0) * 12;

  newRPlanning.timeHorizon = (0, _naAnalysis.getTimeHorizon)(newRPlanning, dependantProfileData, "rPlanning");
  newRPlanning.iarRate2 = (0, _naAnalysis.calIarRate2)(Number(naData.iarRate));
  newRPlanning.compFv = (0, _naAnalysis.calCompFv)(newRPlanning.avgInflatRate, newRPlanning.annualInReqRetirement, newRPlanning.timeHorizon);
  newRPlanning.firstYrPMT = (0, _naAnalysis.calFirstYrPmt)(newRPlanning);
  newRPlanning.retireDuration = (0, _naAnalysis.calRetireDurtion)(newRPlanning, dependantProfileData);
  newRPlanning.compPv = (0, _naAnalysis.calCompPv)(newRPlanning.iarRate2, newRPlanning.retireDuration, newRPlanning.firstYrPMT);
  newRPlanning.maturityValue = dependantFeData.retirePol;
  newRPlanning.assets = newRPlanning.assets && newRPlanning.assets.map(function (asset) {
    var newAsset = _.cloneDeep(asset);
    newAsset.calAsset = (0, _naAnalysis.calAsset)(asset.usedAsset, asset.return, newRPlanning.timeHorizon);
    return newAsset;
  });
  newRPlanning.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.RPLANNING, {
    assets: newRPlanning.assets,
    timeHorizon: newRPlanning.timeHorizon,
    maturityValue: newRPlanning.maturityValue,
    compPv: newRPlanning.compPv
  });

  return newRPlanning;
}

function paProtectionCalculator(_ref6) {
  var protection = _ref6.protection,
      dependantFeData = _ref6.dependantFeData;

  var newPaProtection = _.cloneDeep(protection);

  newPaProtection.extInsurance = dependantFeData.personAcc;
  newPaProtection.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.PAPROTECTION, {
    pmt: newPaProtection.pmt,
    extInsurance: newPaProtection.extInsurance
  });

  return newPaProtection;
}

function pcHeadstartCalculator(_ref7) {
  var protection = _ref7.protection,
      dependantFeData = _ref7.dependantFeData;

  var newPcHeadstart = _.cloneDeep(protection);

  newPcHeadstart.extInsurance = dependantFeData.lifeInsProt + dependantFeData.invLinkPol;
  newPcHeadstart.ciExtInsurance = dependantFeData.ciPort;
  newPcHeadstart.pTotShortfall = newPcHeadstart.extInsurance - newPcHeadstart.sumAssuredProvided;
  newPcHeadstart.ciTotShortfall = newPcHeadstart.ciExtInsurance - newPcHeadstart.sumAssuredCritical;

  return newPcHeadstart;
}

function hcProtectionCalculator(_ref8) {
  var protection = _ref8.protection;

  return _.cloneDeep(protection);
}

function ePlanningCalculator(_ref9) {
  var protection = _ref9.protection,
      dependantFeData = _ref9.dependantFeData,
      dependantProfileData = _ref9.dependantProfileData;

  var newEPlanning = _.cloneDeep(protection);

  newEPlanning.curAgeofChild = dependantProfileData.nearAge;
  newEPlanning.timeHorizon = (0, _naAnalysis.getTimeHorizon)(newEPlanning, dependantProfileData, "ePlanning");
  newEPlanning.estTotFutureCost = (0, _naAnalysis.calEstToFutureCost)(newEPlanning);
  newEPlanning.maturityValue = dependantFeData.eduFund;
  newEPlanning.assets = newEPlanning.assets && newEPlanning.assets.map(function (asset) {
    var newAsset = _.cloneDeep(asset);
    newAsset.calAsset = (0, _naAnalysis.calAsset)(asset.usedAsset, asset.return, newEPlanning.timeHorizon);
    return newAsset;
  });
  newEPlanning.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.EPLANNING, {
    assets: newEPlanning.assets,
    timeHorizon: newEPlanning.timeHorizon,
    maturityValue: newEPlanning.maturityValue,
    estTotFutureCost: newEPlanning.estTotFutureCost
  });

  return newEPlanning;
}

function otherCalculator(protection) {
  var newOther = _.cloneDeep(protection);

  newOther.goals = newOther.goals.map(function (goal) {
    return Object.assign({}, goal, {
      totShortfall: (0, _naAnalysis.calTotShortfall)(_NEEDS.OTHER, {
        needsValue: goal.needsValue,
        extInsuDisplay: goal.extInsuDisplay
      }, "goal"),
      cid: newOther.cid
    });
  });
  newOther.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.OTHER, {
    goalNo: newOther.goalNo,
    goals: newOther.goals
  });

  return newOther;
}

function psGoalsCalculator(protection) {
  var newPsGoals = _.cloneDeep(protection);

  newPsGoals.goals = newPsGoals.goals.map(function (goal) {
    return Object.assign({}, goal, {
      totShortfall: (0, _naAnalysis.calTotShortfall)(_NEEDS.PSGOALS, {
        assets: goal.assets && goal.assets.map(function (asset) {
          var newAsset = _.cloneDeep(asset);
          newAsset.calAsset = (0, _naAnalysis.calAsset)(asset.usedAsset, asset.return, goal.timeHorizon);
          return newAsset;
        }),
        timeHorizon: goal.timeHorizon.toString(),
        projMaturity: goal.projMaturity,
        compFv: goal.compFv
      }, "goal"),
      assets: goal.assets && goal.assets.map(function (asset) {
        var newAsset = _.cloneDeep(asset);
        newAsset.calAsset = (0, _naAnalysis.calAsset)(asset.usedAsset, asset.return, goal.timeHorizon);
        return newAsset;
      })
    });
  });
  newPsGoals.totShortfall = (0, _naAnalysis.calTotShortfall)(_NEEDS.PSGOALS, {
    goalNo: newPsGoals.goalNo,
    goals: newPsGoals.goals
  });

  return newPsGoals;
}
// =============================================================================
// main functions
// =============================================================================
function naDataGetter(_ref10) {
  var naData = _ref10.naData,
      pdaData = _ref10.pdaData,
      profileData = _ref10.profileData,
      dependantProfilesData = _ref10.dependantProfilesData,
      feData = _ref10.feData,
      shouldInitProtection = _ref10.shouldInitProtection;

  // na initialize
  var newNaData = Object.assign({
    rPlanning: {},
    ciProtection: {},
    fiProtection: {},
    diProtection: {},
    paProtection: {},
    pcHeadstart: {},
    hcProtection: {},
    ePlanning: {},
    other: {},
    psGoals: {}
  }, _.cloneDeep(naData));

  // owner fiProtection
  newNaData.fiProtection.owner = fiProtectionCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, fiProtectionInitialState, _.get(naData, "fiProtection.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    naData: naData,
    dependantFeData: feData.owner
  });

  // owner diProtection
  newNaData.diProtection.owner = diProtectionCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, diProtectionInitialState, _.get(naData, "diProtection.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: feData.owner
  });

  // owner ciProtection
  newNaData.ciProtection.owner = ciProtectionCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, ciProtectionInitialState, _.get(naData, "ciProtection.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: feData.owner,
    naData: newNaData
  });

  // owner rPlanning
  newNaData.rPlanning.owner = rPlanningCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, rPlanningInitialState, _.get(naData, "rPlanning.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: feData.owner,
    naData: newNaData,
    dependantProfileData: profileData
  });

  // owner paProtection
  newNaData.paProtection.owner = paProtectionCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, paProtectionInitialState, _.get(naData, "paProtection.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: feData.owner
  });

  // owner hcProtection
  newNaData.hcProtection.owner = hcProtectionCalculator({
    protection: Object.assign({
      cid: profileData.cid
    }, hcProtectionInitialState, _.get(naData, "hcProtection.owner", {}), shouldInitProtection ? {
      init: true
    } : {}),
    dependantFeData: feData.owner
  });

  // owner other
  newNaData.other.owner = otherCalculator(Object.assign({
    cid: profileData.cid
  }, otherInitialState, _.get(naData, "other.owner", {}), shouldInitProtection ? {
    init: true
  } : {}));

  // owner psGoals
  newNaData.psGoals.owner = psGoalsCalculator(Object.assign({
    cid: profileData.cid
  }, psGoalsInitialState, _.get(naData, "psGoals.owner", {}), shouldInitProtection ? {
    init: true
  } : {}));

  // spouse
  if (pdaData.applicant === "joint") {
    var spouseData = _.find(profileData.dependants, function (dependant) {
      return dependant.relationship === "SPO";
    });
    var spouseKey = Object.keys(dependantProfilesData).find(function (key) {
      return dependantProfilesData[key].relationship === "SPO";
    });
    var spouseProfileData = dependantProfilesData[spouseKey];

    // spouse fiProtection
    newNaData.fiProtection.spouse = fiProtectionCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, fiProtectionInitialState, _.get(naData, "fiProtection.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      naData: naData,
      dependantFeData: feData.spouse
    });

    // spouse diProtection
    newNaData.diProtection.spouse = diProtectionCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, diProtectionInitialState, _.get(naData, "diProtection.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      dependantFeData: feData.spouse
    });

    // spouse ciProtection
    newNaData.ciProtection.spouse = ciProtectionCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, ciProtectionInitialState, _.get(naData, "ciProtection.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      dependantFeData: feData.spouse,
      naData: newNaData
    });

    // spouse rPlanning
    newNaData.rPlanning.spouse = rPlanningCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, rPlanningInitialState, _.get(naData, "rPlanning.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      dependantFeData: feData.spouse,
      naData: newNaData,
      dependantProfileData: spouseProfileData
    });

    // spouse paProtection
    newNaData.paProtection.spouse = paProtectionCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, paProtectionInitialState, _.get(naData, "paProtection.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      dependantFeData: feData.spouse
    });

    // spouse hcProtection
    newNaData.hcProtection.spouse = hcProtectionCalculator({
      protection: Object.assign({
        cid: spouseData.cid
      }, hcProtectionInitialState, _.get(naData, "hcProtection.spouse", {}), shouldInitProtection ? {
        init: true
      } : {}),
      dependantFeData: feData.spouse
    });

    // spouse other
    newNaData.other.spouse = otherCalculator(Object.assign({
      cid: spouseData.cid
    }, otherInitialState, _.get(naData, "other.spouse", {}), shouldInitProtection ? {
      init: true
    } : {}));

    // spouse psGoals
    newNaData.psGoals.spouse = psGoalsCalculator(Object.assign({
      cid: spouseData.cid
    }, psGoalsInitialState, _.get(naData, "psGoals.spouse", {}), shouldInitProtection ? {
      init: true
    } : {}));
  }

  // dependants
  if (pdaData.dependants !== "") {
    pdaData.dependants.split(",").forEach(function (cid) {
      var dependantData = profileData.dependants.find(function (dependant) {
        return dependant.cid === cid;
      });
      var dependantFeData = feData.dependants ? feData.dependants.find(function (dependant) {
        return dependant.cid === cid;
      }) : false;
      var dependantKey = Object.keys(dependantProfilesData).find(function (key) {
        return dependantProfilesData[key].cid === cid;
      });
      var dependantProfileData = dependantKey !== -1 ? dependantProfilesData[dependantKey] : false;

      if (dependantData && dependantFeData && dependantProfileData) {
        switch (dependantData.relationship) {
          case "SON":
          case "DAU":
            {
              newNaData.diProtection.dependants = dependantDataGetter({
                cid: cid,
                initialState: diProtectionInitialState,
                protection: newNaData.diProtection,
                calculator: diProtectionCalculator,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });

              newNaData.paProtection.dependants = dependantDataGetter({
                cid: cid,
                initialState: paProtectionInitialState,
                protection: newNaData.paProtection,
                calculator: paProtectionCalculator,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });

              newNaData.pcHeadstart.dependants = dependantDataGetter({
                cid: cid,
                initialState: pcHeadstartInitialState,
                protection: newNaData.pcHeadstart,
                calculator: pcHeadstartCalculator,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });

              newNaData.hcProtection.dependants = dependantDataGetter({
                cid: cid,
                initialState: hcProtectionInitialState,
                protection: newNaData.hcProtection,
                calculator: hcProtectionCalculator,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });

              newNaData.ePlanning.dependants = dependantDataGetter({
                cid: cid,
                initialState: ePlanningInitialState,
                protection: newNaData.ePlanning,
                calculator: ePlanningCalculator,
                dependantProfileData: dependantProfileData,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });
              break;
            }
          case "FAT":
          case "MOT":
          case "GFA":
          case "GMO":
            {
              newNaData.hcProtection.dependants = dependantDataGetter({
                cid: cid,
                initialState: hcProtectionInitialState,
                protection: newNaData.hcProtection,
                calculator: hcProtectionCalculator,
                dependantFeData: dependantFeData,
                shouldInitProtection: shouldInitProtection
              });
              break;
            }
          default:
            throw new Error("naUtilities.naDataGetter got unexpected dependant case " + dependantData.relationship);
        }
      }
    });
  }

  return newNaData;
}

function validateFiProtection(_ref11) {
  var fiProtectionData = _ref11.fiProtectionData,
      dependantProfileData = _ref11.dependantProfileData,
      selectedProfile = _ref11.selectedProfile,
      na = _ref11.na,
      fe = _ref11.fe;

  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    unMatchPmtReason: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    },
    othFundNeedsName: {
      hasError: false,
      messagePath: ""
    },
    othFundNeeds: {
      hasError: false,
      messagePath: ""
    },
    assets: [{
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }]
  };

  if (fiProtectionData.isActive) {
    // validate init
    if (fiProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (fiProtectionData.pmt === "" || fiProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (fiProtectionData.requireYrIncome === "" || fiProtectionData.requireYrIncome === "0" || fiProtectionData.requireYrIncome === 0) {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate unMatchPmtReason
    if (
    // if client do not fill income field, allowance is undefined
    !Number.isNaN(parseFloat(dependantProfileData.allowance)) &&
    // if client do not fill income field, allowance is ""
    !Number.isNaN(parseFloat(fiProtectionData.pmt)) && fiProtectionData.pmt > dependantProfileData.allowance && fiProtectionData.unMatchPmtReason === "") {
      validation.unMatchPmtReason = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate othFundNeedsName
    // if client do not fill othFundNeeds, othFundNeeds is ""
    var realOthFundNeedsValue = parseFloat(fiProtectionData.othFundNeeds);
    if (!Number.isNaN(realOthFundNeedsValue) && realOthFundNeedsValue !== 0 && fiProtectionData.othFundNeedsName === "") {
      validation.othFundNeedsName = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validation for the first time
    if (Number.isNaN(realOthFundNeedsValue) && fiProtectionData.othFundNeedsName !== "") {
      validation.othFundNeeds = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate othFundNeeds
    if (fiProtectionData.othFundNeedsName !== "" && fiProtectionData.othFundNeeds === 0) {
      validation.othFundNeeds = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (fiProtectionData.assets) {
      fiProtectionData.assets.forEach(function (asset, index) {
        var otherAssetsValue = (0, _naAnalysis.calOtherAsset)({
          key: asset.key,
          selectedProduct: "fiProtection",
          selectedProfile: selectedProfile,
          na: na
        });

        if (fe[selectedProfile][asset.key] < asset.usedAsset + otherAssetsValue) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (asset.key === "all" || asset.key !== "all" && (asset.usedAsset === 0 || asset.usedAsset === null)) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

function validateDiProtection(diProtectionData) {
  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    }
  };

  if (diProtectionData.isActive) {
    // validate init
    if (diProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (diProtectionData.pmt === "" || diProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (diProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

function validateCiProtection(_ref12) {
  var ciProtectionData = _ref12.ciProtectionData,
      selectedProfile = _ref12.selectedProfile,
      na = _ref12.na,
      fe = _ref12.fe;

  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    },
    requireYrIncome: {
      hasError: false,
      messagePath: ""
    },
    mtCost: {
      hasError: false,
      messagePath: ""
    },
    assets: [{
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      }
    }]
  };

  if (ciProtectionData.isActive) {
    // validate init
    if (ciProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (ciProtectionData.pmt === "" || ciProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate requireYrIncome
    if (ciProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate mtCost
    if (ciProtectionData.requireYrIncome === "") {
      validation.requireYrIncome = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (ciProtectionData.assets) {
      ciProtectionData.assets.forEach(function (asset, index) {
        var otherAssetsValue = (0, _naAnalysis.calOtherAsset)({
          key: asset.key,
          selectedProduct: "ciProtection",
          selectedProfile: selectedProfile,
          na: na
        });
        if (fe[selectedProfile][asset.key] < asset.usedAsset + otherAssetsValue) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (asset.key === "all" || asset.key !== "all" && (asset.usedAsset === 0 || asset.usedAsset === null)) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

function validateRPlanning(_ref13) {
  var rPlanningData = _ref13.rPlanningData,
      dependantProfileData = _ref13.dependantProfileData,
      selectedProfile = _ref13.selectedProfile,
      na = _ref13.na,
      fe = _ref13.fe;

  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    retireAge: {
      hasError: false,
      messagePath: ""
    },
    inReqRetirement: {
      hasError: false,
      messagePath: ""
    },
    unMatchPmtReason: {
      hasError: false,
      messagePath: ""
    },
    othRegIncome: {
      hasError: false,
      messagePath: ""
    },
    assets: [{
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }]
  };

  if (rPlanningData.isActive) {
    // validate init
    if (rPlanningData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate inReqRetirement
    if (rPlanningData.inReqRetirement === "" || rPlanningData.inReqRetirement === 0) {
      validation.inReqRetirement = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    if (
    // if client do not fill income field, allowance is undefined
    !Number.isNaN(parseFloat(dependantProfileData.allowance)) &&
    // if client do not fill income field, allowance is ""
    !Number.isNaN(parseFloat(rPlanningData.inReqRetirement)) && rPlanningData.inReqRetirement > dependantProfileData.allowance && rPlanningData.unMatchPmtReason === "") {
      validation.unMatchPmtReason = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (rPlanningData.assets) {
      rPlanningData.assets.forEach(function (asset, index) {
        var otherAssetsValue = (0, _naAnalysis.calOtherAsset)({
          key: asset.key,
          selectedProduct: "rPlanning",
          selectedProfile: selectedProfile,
          na: na
        });
        if (fe[selectedProfile][asset.key] < asset.usedAsset + otherAssetsValue) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (asset.key === "all" || asset.key !== "all" && (asset.usedAsset === 0 || asset.usedAsset === null)) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }

        if ("key" in asset && !("return" in asset)) {
          validation.assets[index].return = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

function validatePaProtection(paProtectionData) {
  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    pmt: {
      hasError: false,
      messagePath: ""
    }
  };

  if (paProtectionData.isActive) {
    // validate init
    if (paProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate pmt
    if (paProtectionData.pmt === "" || paProtectionData.pmt === 0) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.302"
      };
    } else if (paProtectionData.pmt < 50000) {
      validation.pmt = {
        hasError: true,
        messagePath: "error.708"
      };
    }
  }

  return validation;
}

function validatePcHeadstart(pcHeadstartData) {
  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    providedFor: {
      hasError: false,
      messagePath: ""
    },
    sumAssuredCritical: {
      hasError: false,
      messagePath: ""
    },
    sumAssuredProvided: {
      hasError: false,
      messagePath: ""
    }
  };

  if (pcHeadstartData.isActive) {
    // validate init
    if (pcHeadstartData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate providedFor
    if (pcHeadstartData.providedFor === "") {
      validation.providedFor = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate sumAssuredCritical
    if (pcHeadstartData.providedFor.indexOf("CI") !== -1 && (pcHeadstartData.sumAssuredCritical === "" || pcHeadstartData.sumAssuredCritical === 0)) {
      validation.sumAssuredCritical = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate sumAssuredProvided
    if (pcHeadstartData.providedFor.indexOf("P") !== -1 && (pcHeadstartData.sumAssuredProvided === "" || pcHeadstartData.sumAssuredProvided === 0)) {
      validation.sumAssuredProvided = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

function validateHcProtection(hcProtectionData) {
  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    provideHeadstart: {
      hasError: false,
      messagePath: ""
    },
    typeofWard: {
      hasError: false,
      messagePath: ""
    }
  };

  if (hcProtectionData.isActive) {
    // validate init
    if (hcProtectionData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate provideHeadstart
    if (hcProtectionData.provideHeadstart === "" || hcProtectionData.isActive && !hcProtectionData.provideHeadstart) {
      validation.provideHeadstart = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate typeofWard
    if (hcProtectionData.typeofWard === "" || hcProtectionData.isActive && !hcProtectionData.typeofWard) {
      validation.typeofWard = {
        hasError: true,
        messagePath: "error.302"
      };
    }
  }

  return validation;
}

function validateEPlanning(_ref14) {
  var ePlanningData = _ref14.ePlanningData,
      na = _ref14.na,
      fe = _ref14.fe;

  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    yrtoSupport: {
      hasError: false,
      messagePath: ""
    },
    costofEdu: {
      hasError: false,
      messagePath: ""
    },
    assets: [{
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }, {
      usedAsset: {
        hasError: false,
        messagePath: ""
      },
      key: {
        hasError: false,
        messagePath: ""
      },
      return: {
        hasError: false,
        messagePath: ""
      }
    }]
  };

  if (ePlanningData.isActive) {
    // validate init
    if (ePlanningData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate yrtoSupport
    if (ePlanningData.yrtoSupport === "" || ePlanningData.yrtoSupport === 0) {
      validation.yrtoSupport = {
        hasError: true,
        messagePath: "error.302"
      };
    } else if (ePlanningData.yrtoSupport < ePlanningData.curAgeofChild) {
      validation.yrtoSupport = {
        hasError: true,
        messagePath: "error.405"
      };
    }

    // validate costofEdu
    if (ePlanningData.costofEdu === "" || ePlanningData.costofEdu === 0) {
      validation.costofEdu = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate assets
    if (ePlanningData.assets) {
      ePlanningData.assets.forEach(function (asset, index) {
        var otherAssetsValue = (0, _naAnalysis.calOtherAsset)({
          key: asset.key,
          selectedProduct: "ePlanning",
          selectedProfile: "owner",
          na: na
        });
        if (fe.owner[asset.key] < asset.usedAsset + otherAssetsValue) {
          if (asset.usedAsset === 0 || asset.usedAsset === null) {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: ""
            };
          } else {
            validation.assets[index].usedAsset = {
              hasError: true,
              messagePath: "error.702"
            };
          }
        }

        if (asset.key === "all" || asset.key !== "all" && (asset.usedAsset === 0 || asset.usedAsset === null)) {
          validation.assets[index].key = {
            hasError: true,
            messagePath: ""
          };
        }

        if ("key" in asset && !("return" in asset)) {
          validation.assets[index].return = {
            hasError: true,
            messagePath: ""
          };
        }
      });
    }
  }

  return validation;
}

function validateOther(otherData) {
  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    goalNo: {
      hasError: false,
      messagePath: ""
    },
    goals: [{
      goalName: {
        hasError: false,
        messagePath: ""
      },
      needsValue: {
        hasError: false,
        messagePath: ""
      },
      typeofNeeds: {
        hasError: false,
        messagePath: ""
      }
    }, {
      goalName: {
        hasError: false,
        messagePath: ""
      },
      needsValue: {
        hasError: false,
        messagePath: ""
      },
      typeofNeeds: {
        hasError: false,
        messagePath: ""
      }
    }, {
      goalName: {
        hasError: false,
        messagePath: ""
      },
      needsValue: {
        hasError: false,
        messagePath: ""
      },
      typeofNeeds: {
        hasError: false,
        messagePath: ""
      }
    }]
  };

  if (otherData.isActive) {
    // validate init
    if (otherData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate goalNo
    if (otherData.goalNo === "" || otherData.goalNo === 0) {
      validation.goalNo = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate goals
    for (var goalIndex = 0; goalIndex < otherData.goalNo; goalIndex += 1) {
      // validate goals[].goalName
      if (otherData.goals[goalIndex].goalName === "") {
        validation.goals[goalIndex].goalName = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].needsValue
      if (otherData.goals[goalIndex].needsValue === "" || otherData.goals[goalIndex].needsValue === 0 || otherData.goals[goalIndex].needsValue === null) {
        validation.goals[goalIndex].needsValue = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].typeofNeeds
      if (otherData.goals[goalIndex].typeofNeeds === "") {
        validation.goals[goalIndex].typeofNeeds = {
          hasError: true,
          messagePath: "error.302"
        };
      }
    }
  }

  return validation;
}

function validatePsGoals(_ref15) {
  var psGoalsData = _ref15.psGoalsData,
      selectedProfile = _ref15.selectedProfile,
      na = _ref15.na,
      fe = _ref15.fe;

  var validation = {
    init: {
      hasError: false,
      messagePath: ""
    },
    goalNo: {
      hasError: false,
      messagePath: ""
    },
    goals: [{
      goalName: {
        hasError: false,
        messagePath: ""
      },
      timeHorizon: {
        hasError: false,
        messagePath: ""
      },
      compFv: {
        hasError: false,
        messagePath: ""
      },
      assets: [{
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }]
    }, {
      goalName: {
        hasError: false,
        messagePath: ""
      },
      timeHorizon: {
        hasError: false,
        messagePath: ""
      },
      compFv: {
        hasError: false,
        messagePath: ""
      },
      assets: [{
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }]
    }, {
      goalName: {
        hasError: false,
        messagePath: ""
      },
      timeHorizon: {
        hasError: false,
        messagePath: ""
      },
      compFv: {
        hasError: false,
        messagePath: ""
      },
      assets: [{
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }, {
        usedAsset: {
          hasError: false,
          messagePath: ""
        },
        key: {
          hasError: false,
          messagePath: ""
        },
        return: {
          hasError: false,
          messagePath: ""
        }
      }]
    }]
  };

  if (psGoalsData.isActive) {
    // validate init
    if (psGoalsData.init) {
      validation.init = {
        hasError: true,
        messagePath: ""
      };
    }

    // validate goalNo
    if (psGoalsData.goalNo === "" || psGoalsData.goalNo === 0) {
      validation.goalNo = {
        hasError: true,
        messagePath: "error.302"
      };
    }

    // validate goals

    var _loop = function _loop(goalIndex) {
      // validate goals[].goalName
      if (psGoalsData.goals[goalIndex].goalName === "") {
        validation.goals[goalIndex].goalName = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].timeHorizon
      if (psGoalsData.goals[goalIndex].timeHorizon === "") {
        validation.goals[goalIndex].timeHorizon = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate goals[].compFv
      if (psGoalsData.goals[goalIndex].compFv === "" || psGoalsData.goals[goalIndex].compFv === 0) {
        validation.goals[goalIndex].compFv = {
          hasError: true,
          messagePath: "error.302"
        };
      }

      // validate assets
      if (psGoalsData.goals && psGoalsData.goals[goalIndex] && psGoalsData.goals[goalIndex].assets) {
        psGoalsData.goals[goalIndex].assets.forEach(function (asset, index) {
          var otherAssetsValue = (0, _naAnalysis.calOtherAsset)({
            key: asset.key,
            selectedProduct: "psGoals",
            selectedProfile: selectedProfile,
            na: na,
            goalIndex: goalIndex
          });
          if (fe[selectedProfile][asset.key] < asset.usedAsset + otherAssetsValue) {
            if (asset.usedAsset === 0 || asset.usedAsset === null) {
              validation.goals[goalIndex].assets[index].usedAsset = {
                hasError: true,
                messagePath: ""
              };
            } else {
              validation.goals[goalIndex].assets[index].usedAsset = {
                hasError: true,
                messagePath: "error.702"
              };
            }
          }

          if (asset.key === "all" || asset.key !== "all" && (asset.usedAsset === 0 || asset.usedAsset === null)) {
            validation.goals[goalIndex].assets[index].key = {
              hasError: true,
              messagePath: ""
            };
          }

          if ("key" in asset && !("return" in asset)) {
            validation.goals[goalIndex].assets[index].return = {
              hasError: true,
              messagePath: ""
            };
          }
        });
      }
    };

    for (var goalIndex = 0; goalIndex < psGoalsData.goalNo; goalIndex += 1) {
      _loop(goalIndex);
    }
  }

  return validation;
}