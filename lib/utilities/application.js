"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateApplicationFormCompletedMenus = exports.updateProfileList = exports.checkCanShieldApplicationGoPayment = exports.checkCanShieldApplicationGoSignature = exports.checkApplicationFormHasError = exports.checkPaymentHasError = exports.checkSignatureHasError = exports.checkApplicationFormSectionHasError = exports.validateApplicationFormPersonalDetails = exports.initValidateApplicationFormError = exports.initValidateApplicationPersonalDetailsForm = exports.initApplicationFormPersonalDetailsValue = exports.checkHasPolicyNumber = exports.checkHasInsurability = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _sectionMapping;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _SECTION_KEYS = require("../constants/SECTION_KEYS");

var _SECTION_KEYS2 = _interopRequireDefault(_SECTION_KEYS);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _SECTION_KEYS$APPLICA = _SECTION_KEYS2.default[_REDUCER_TYPES.APPLICATION],
    PERSONAL_DETAILS = _SECTION_KEYS$APPLICA.PERSONAL_DETAILS,
    RESIDENCY = _SECTION_KEYS$APPLICA.RESIDENCY,
    INSURABILITY = _SECTION_KEYS$APPLICA.INSURABILITY,
    PLAN_DETAILS = _SECTION_KEYS$APPLICA.PLAN_DETAILS,
    DECLARATION = _SECTION_KEYS$APPLICA.DECLARATION;


var sectionMapping = (_sectionMapping = {}, _defineProperty(_sectionMapping, PERSONAL_DETAILS, ["personalInfo"]), _defineProperty(_sectionMapping, RESIDENCY, ["residency", "foreigner", "policies"]), _defineProperty(_sectionMapping, INSURABILITY, ["insurability"]), _defineProperty(_sectionMapping, DECLARATION, ["declaration"]), _sectionMapping);

function checkBranchInfo(_ref) {
  var profile = _ref.profile,
      field = _ref.field,
      errorObj = _ref.errorObj;

  if (profile.branchInfo) {
    field.forEach(function (fieldName) {
      if (!profile.branchInfo[fieldName] || profile.branchInfo[fieldName] === "") {
        _.set(errorObj.application, "proposer.personalInfo." + fieldName, validation.validateMandatory({
          field: {
            type: _FIELD_TYPES.TEXT_BOX,
            mandatory: true,
            disabled: false
          },
          value: profile.branchInfo[fieldName]
        }));
      }
    });
  } else {
    field.forEach(function (fieldName) {
      _.set(errorObj.application, "proposer.personalInfo." + fieldName, validation.validateMandatory({
        field: {
          type: _FIELD_TYPES.TEXT_BOX,
          mandatory: true,
          disabled: false
        },
        value: null
      }));
    });
  }
}

var checkHasInsurability = exports.checkHasInsurability = function checkHasInsurability(_ref2) {
  var application = _ref2.application,
      template = _ref2.template;

  var isShield = application.quotation.quotType === "SHIELD";

  /**
   * template -> items[0] (section "Application")
   * -> items[0] (menu in left side)
   * -> items[0] (1st section in three sections)
   * */
  var menuTemplate =
  // Shield case & non-Shield case
  isShield ? _.get(template, "items[0]") : _.get(template, "items[0].items[0]");
  var menuSections = _.get(menuTemplate, "items[0].items");
  return _.findIndex(menuSections, function (section) {
    return section.key === INSURABILITY;
  }) >= 0;
};

var checkHasPolicyNumber = exports.checkHasPolicyNumber = function checkHasPolicyNumber(_ref3) {
  var application = _ref3.application,
      error = _ref3.error;

  var policyNumberExist = true;
  if (application.quotation.quotType === "SHIELD") {
    Object.values(application.iCidMapping).forEach(function (value) {
      return value.forEach(function (insured) {
        if (!insured.policyNumber) {
          policyNumberExist = false;
        }
      });
    });
  } else if (!application.policyNumber) {
    policyNumberExist = false;
  }
  if (error) {
    _.set(error, "applicationForm.policyNumberExist", policyNumberExist);
  }
  return policyNumberExist;
};

/**
 * initApplicationFormPersonalDetailsError
 * @description Only trigger once after enters the Application Form Page
 * @param {object} errorObj - error object from reducer store application.error
 * @param {object} profile - the real user data from reducer store application.application
 * @param {object} targetProfileName - only to init the object fields name
 * */
function initApplicationFormPersonalDetailsError(_ref4) {
  var errorObj = _ref4.errorObj,
      profile = _ref4.profile,
      targetProfileName = _ref4.targetProfileName,
      isShield = _ref4.isShield;

  var initFields = {
    personalInfo: ["organization", "organizationCountry", "postalCode", "addrBlock", "addrStreet", "isSameAddr", "mAddrCountry", "mPostalCode", "mAddrBlock", "mAddrStreet", "branchInfo"]
  };

  if (isShield && targetProfileName.includes("insured")) {
    initFields = {
      personalInfo: ["organization", "branchInfo"]
    };
  }

  var fieldTriggerList = {
    mAddrCountry: "isSameAddr",
    mPostalCode: "isSameAddr",
    mAddrBlock: "isSameAddr",
    mAddrStreet: "isSameAddr",
    branchInfo: ["bankRefId", "branch"]
  };
  if (profile.extra.channelName !== "SINGPOST" && targetProfileName === "proposer") {
    fieldTriggerList = {
      mAddrCountry: "isSameAddr",
      mPostalCode: "isSameAddr",
      mAddrBlock: "isSameAddr",
      mAddrStreet: "isSameAddr"
    };
  }
  var fieldTriggerValues = {
    isSameAddr: "Y"
  };

  // Init the Error Obj
  _.set(errorObj, "application." + targetProfileName + ".personalInfo", {});
  if (profile.extra.channelName === "SINGPOST" && targetProfileName === "proposer") {
    checkBranchInfo({
      profile: profile.personalInfo,
      field: fieldTriggerList.branchInfo,
      errorObj: errorObj
    });
  }

  if (profile.personalInfo.nationality !== "N1" && profile.personalInfo.prStatus === "N") {
    _.set(errorObj, "application." + targetProfileName + ".personalInfo.passExpDate", validation.validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_BOX,
        mandatory: true,
        disabled: false
      },
      value: profile.personalInfo.passExpDate || null
    }));
  }

  Object.entries(initFields).forEach(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
        formName = _ref6[0],
        formFieldArr = _ref6[1];

    Object.keys(profile[formName]).forEach(function (fieldName) {
      if (formFieldArr.indexOf(fieldName) >= 0) {
        var fieldTriggerFrom = fieldTriggerList[fieldName];
        var fieldTriggerValue = fieldTriggerValues[fieldName];
        if (_.isEmpty(fieldTriggerFrom) || profile[formName][fieldTriggerFrom] === fieldTriggerValue) {
          _.set(errorObj, "application." + targetProfileName + "." + formName + "." + fieldName, validation.validateMandatory({
            field: {
              type: _FIELD_TYPES.TEXT_BOX,
              mandatory: true,
              disabled: false
            },
            value: profile[formName][fieldName]
          }));
        }
      }
    });
    if (profile[formName].isSameAddr === "N") {
      Object.keys(fieldTriggerList).forEach(function (trigerListKey) {
        if (formFieldArr.indexOf(trigerListKey) >= 0) {
          _.set(errorObj, "application." + targetProfileName + "." + formName + "." + trigerListKey, validation.validateMandatory({
            field: {
              type: _FIELD_TYPES.TEXT_BOX,
              mandatory: true,
              disabled: false
            },
            value: profile[formName][trigerListKey]
          }));
        }
      });
    }
  });
}

/**
 * initApplicationFormPersonalDetailsValue
 * @description Only trigger once after enters the Application Form Page
 * @param {object} application - The real application data object from reducer store application.application
 * */
var initApplicationFormPersonalDetailsValue = exports.initApplicationFormPersonalDetailsValue = function initApplicationFormPersonalDetailsValue(_ref7) {
  var application = _ref7.application,
      template = _ref7.template;

  var newApplication = _.cloneDeep(application);

  var proposerValues = _.get(newApplication, "applicationForm.values.proposer", {});
  var insuredValues = _.get(newApplication, "applicationForm.values.insured", []);

  if (!_.isEmpty(proposerValues)) {
    if (!_.has(proposerValues, "personalInfo.isSameAddr")) {
      proposerValues.personalInfo.isSameAddr = "Y";
    } else if (proposerValues.personalInfo.isSameAddr === "" || !proposerValues.personalInfo.isSameAddr) {
      proposerValues.personalInfo.isSameAddr = "Y";
    }
  }
  if (!_.isEmpty(insuredValues)) {
    _.forEach(insuredValues, function (insured) {
      if (!_.has(insured, "personalInfo.isSameAddr")) {
        insured.personalInfo.isSameAddr = "Y";
      } else if (insured.personalInfo.isSameAddr === "" || !insured.personalInfo.isSameAddr) {
        insured.personalInfo.isSameAddr = "Y";
      }
    });
  }

  if (!_.has(newApplication, "applicationForm.values.checkedMenu")) {
    _.set(newApplication, "applicationForm.values.checkedMenu", [PERSONAL_DETAILS]);
  }

  if (!_.has(newApplication, "applicationForm.values.completedMenus")) {
    _.set(newApplication, "applicationForm.values.completedMenus", []);
  }

  var menus = [PERSONAL_DETAILS, RESIDENCY];
  if (checkHasInsurability({
    application: newApplication,
    template: template
  })) {
    menus.push(INSURABILITY);
  }
  menus.push(PLAN_DETAILS);
  menus.push(DECLARATION);

  if (!_.has(newApplication, "applicationForm.values.menus")) {
    _.set(newApplication, "applicationForm.values.menus", menus);
  }

  return newApplication;
};

/**
 * initValidateApplicationPersonalDetailsForm
 * @description Only trigger once after enters the Application Form Page
 * @param {object} application - The real application data object from reducer store application.application
 * @param {object} errorObj - error object from reducer store application.error
 * */
var initValidateApplicationPersonalDetailsForm = exports.initValidateApplicationPersonalDetailsForm = function initValidateApplicationPersonalDetailsForm(_ref8) {
  var application = _ref8.application,
      errorObj = _ref8.errorObj;

  if (!_.isEmpty(_.get(application, "applicationForm.values"))) {
    Object.entries(application.applicationForm.values).forEach(function (_ref9) {
      var _ref10 = _slicedToArray(_ref9, 2),
          key = _ref10[0],
          profile = _ref10[1];

      var isShield = application.quotation.quotType === "SHIELD";
      if (key === "proposer") {
        initApplicationFormPersonalDetailsError({
          errorObj: errorObj,
          profile: profile,
          targetProfileName: key,
          isShield: isShield
        });
      } else if (key === "insured") {
        application.applicationForm.values.insured.forEach(function (insuredProfile, arrayIndex) {
          initApplicationFormPersonalDetailsError({
            errorObj: errorObj,
            profile: insuredProfile,
            targetProfileName: "insured[" + arrayIndex + "]",
            isShield: isShield
          });
        });
      }
    });
  }
};

var initValidateApplicationFormError = exports.initValidateApplicationFormError = function initValidateApplicationFormError(_ref11) {
  var template = _ref11.template,
      rootValues = _ref11.rootValues,
      errorObj = _ref11.errorObj;

  var values = _.get(rootValues, "applicationForm.values", {});
  var applicationFormPageIds = ["residency", "insurability", "declaration"];
  var menuTemplate =
  // Shield case & non-Shield case
  _.toUpper(_.get(template, "items[0].type", "")) === "MENU" ? _.get(template, "items[0]") : _.get(template, "items[0].items[0]");
  var mapping = {
    residency: {
      template: _.get(menuTemplate, "items[0].items[1].items[0]", {})
    },
    insurability: {
      template: _.get(menuTemplate, "items[0].items[2].items[0]", {})
    },
    declaration: {
      template: _.get(menuTemplate, "items[2].items[0].items[0]", {})
    }
  };

  if (_.has(values, "proposer") && !_.isEmpty(values, "proposer")) {
    _.each(applicationFormPageIds, function (pageId) {
      var pageTemplate = _.get(mapping, pageId + ".template");
      var profileTemplate = _.find(pageTemplate.items, function (item) {
        return item.subType === "proposer";
      });
      if (!_.isUndefined(profileTemplate)) {
        _.each(_.get(profileTemplate, "items"), function (sectionTemplate) {
          var sectionId = sectionTemplate.id;
          var path = "applicationForm.values.proposer." + sectionId;
          if (_.isUndefined(_.get(errorObj, path))) {
            _.set(errorObj, path, {});
          }
          validation.validateApplicationFormTemplate({
            iTemplate: sectionTemplate,
            iValues: _.get(rootValues, path, {}),
            iErrorObj: _.get(errorObj, path),
            rootValues: rootValues,
            path: path
          });
        });
      }
    });
  }

  if (_.has(values, "insured") && !_.isEmpty(values, "insured")) {
    _.each(values.insured, function (insured, index) {
      _.each(applicationFormPageIds, function (pageId) {
        var pageTemplate = _.get(mapping, pageId + ".template");
        var profileTemplate = _.get(_.filter(pageTemplate.items, function (item) {
          return item.subType === "insured";
        }), "[" + index + "]");
        if (!_.isUndefined(profileTemplate)) {
          _.each(_.get(profileTemplate, "items"), function (sectionTemplate) {
            var sectionId = sectionTemplate.id;
            var path = "applicationForm.values.insured[" + index + "]." + sectionId;
            if (_.isUndefined(_.get(errorObj, path))) {
              _.set(errorObj, path, {});
            }
            validation.validateApplicationFormTemplate({
              iTemplate: sectionTemplate,
              iValues: _.get(rootValues, path, {}),
              iErrorObj: _.get(errorObj, path),
              rootValues: rootValues,
              path: path
            });
          });
        }
      });
    });
  }
};

/**
 * validateApplicationFormPersonalDetails
 * @description Trigger each time after input data into the Application Form Page
 * @param {object} validateObj - The rules set from axa-app
 * @param {object} errorObj - error object from reducer store application.error
 * */

var validateApplicationFormPersonalDetails = exports.validateApplicationFormPersonalDetails = function validateApplicationFormPersonalDetails(_ref12) {
  var validateObj = _ref12.validateObj,
      errorObj = _ref12.errorObj;

  _.set(errorObj.application, validateObj.field, validation.validateMandatory({
    field: {
      type: _FIELD_TYPES.TEXT_FIELD,
      mandatory: validateObj.mandatory,
      disabled: false
    },
    value: validateObj.value
  }));
};

var checkPersonalDetailsProfileHasError = function checkPersonalDetailsProfileHasError(profile) {
  var hasError = false;
  Object.values(profile.personalInfo).some(function (field) {
    if (field.hasError) {
      hasError = true;
      return true;
    }
    return false;
  });
  return hasError;
};

var checkPersonalDetailsHasError = function checkPersonalDetailsHasError(_ref13) {
  var _ref13$targetProfile = _ref13.targetProfile,
      targetProfile = _ref13$targetProfile === undefined ? "" : _ref13$targetProfile,
      error = _ref13.error;

  var hasError = false;
  if (!_.isEmpty(error) && !_.isEmpty(error.application)) {
    Object.values(error.application).forEach(function (profile) {
      if (profile.personalInfo && ["", "proposer"].includes(targetProfile)) {
        var profileHasError = checkPersonalDetailsProfileHasError(profile);
        hasError = hasError || profileHasError;
      } else if (Array.isArray(profile)) {
        profile.forEach(function (p, index) {
          if (["", "insured[" + index + "]"].includes(targetProfile)) {
            var _profileHasError = checkPersonalDetailsProfileHasError(p);
            hasError = hasError || _profileHasError;
          }
        });
      }
    });
  }
  return hasError;
};

var checkDynamicFormByProfileHasError = function checkDynamicFormByProfileHasError(_ref14) {
  var dataKey = _ref14.dataKey,
      profileError = _ref14.profileError;

  var hasError = false;
  if (profileError[dataKey]) {
    Object.values(profileError[dataKey]).some(function (field) {
      if (field.hasError) {
        hasError = true;
        return true;
      }
      return false;
    });
  }
  return hasError;
};

var checkDynamicFormHasError = function checkDynamicFormHasError(_ref15) {
  var dataKey = _ref15.dataKey,
      error = _ref15.error,
      _ref15$targetProfile = _ref15.targetProfile,
      targetProfile = _ref15$targetProfile === undefined ? "" : _ref15$targetProfile;

  var hasError = false;
  var iError = _.get(error, "applicationForm.values", {});

  if (!_.isEmpty(iError)) {
    if (_.has(iError, "proposer") && ["", "proposer"].includes(targetProfile)) {
      var profileError = iError.proposer;
      hasError = hasError || checkDynamicFormByProfileHasError({ dataKey: dataKey, profileError: profileError });
    }
    if (_.has(iError, "insured")) {
      _.each(iError.insured, function (insured, index) {
        if (["", "insured[" + index + "]"].includes(targetProfile)) {
          var _profileError = insured;
          hasError = hasError || checkDynamicFormByProfileHasError({ dataKey: dataKey, profileError: _profileError });
        }
      });
    }
  }
  return hasError;
};

var checkApplicationFormSectionHasError = exports.checkApplicationFormSectionHasError = function checkApplicationFormSectionHasError(_ref16) {
  var sectionKey = _ref16.sectionKey,
      _ref16$targetProfile = _ref16.targetProfile,
      targetProfile = _ref16$targetProfile === undefined ? "" : _ref16$targetProfile,
      error = _ref16.error;

  if (sectionKey === _SECTION_KEYS2.default[_REDUCER_TYPES.APPLICATION].PERSONAL_DETAILS) {
    return checkPersonalDetailsHasError({ targetProfile: targetProfile, error: error });
  }
  var formHasError = false;
  _.forEach(sectionMapping[sectionKey], function (dataKey) {
    formHasError = formHasError || checkDynamicFormHasError({ dataKey: dataKey, targetProfile: targetProfile, error: error });
  });
  return formHasError;
};

var checkSignatureHasError = exports.checkSignatureHasError = function checkSignatureHasError(application) {
  return !application.isFullySigned;
};

var checkPaymentHasError = exports.checkPaymentHasError = function checkPaymentHasError(_ref17) {
  var application = _ref17.application,
      error = _ref17.error;

  if (!application.payment) {
    return true;
  }

  var hasError = false;
  _.forEach(error, function (e) {
    if (_.isObject(e)) {
      hasError = hasError || e.hasError;
    }
  });

  return hasError;
};

var checkApplicationFormByProfileHasError = function checkApplicationFormByProfileHasError(_ref18) {
  var application = _ref18.application,
      error = _ref18.error,
      targetProfile = _ref18.targetProfile;

  var hasError = false;
  _.forEach(sectionMapping, function (dataKey, sectionKey) {
    var sectionHasError = checkApplicationFormSectionHasError({
      sectionKey: sectionKey,
      targetProfile: targetProfile,
      error: error
    });
    hasError = hasError || sectionHasError;
  });

  if (targetProfile !== "") {
    _.set(application, "applicationForm.values." + targetProfile + ".extra.isCompleted", !hasError);
  }

  return hasError;
};

var checkApplicationFormHasError = exports.checkApplicationFormHasError = function checkApplicationFormHasError(_ref19) {
  var application = _ref19.application,
      error = _ref19.error;

  var hasError = false;
  if (!_.isEmpty(_.get(application, "applicationForm.values"))) {
    Object.entries(application.applicationForm.values).forEach(function (_ref20) {
      var _ref21 = _slicedToArray(_ref20, 1),
          key = _ref21[0];

      if (key === "proposer") {
        var profileHasError = checkApplicationFormByProfileHasError({
          application: application,
          error: error,
          targetProfile: key
        });
        hasError = hasError || profileHasError;
      } else if (key === "insured") {
        application.applicationForm.values.insured.forEach(function (insuredProfile, index) {
          var profileHasError = checkApplicationFormByProfileHasError({
            application: application,
            error: error,
            targetProfile: "insured[" + index + "]"
          });
          hasError = hasError || profileHasError;
        });
      }
    });
  }
  return hasError;
};

var checkCanShieldApplicationGoSignature = exports.checkCanShieldApplicationGoSignature = function checkCanShieldApplicationGoSignature(error) {
  var personalHasError = checkPersonalDetailsHasError({ error: error });

  var proposerHasError = false;
  _.forEach(sectionMapping, function (dataKey, sectionKey) {
    var sectionHasError = checkApplicationFormSectionHasError({
      sectionKey: sectionKey,
      targetProfile: "proposer",
      error: error
    });
    proposerHasError = proposerHasError || sectionHasError;
  });

  return !personalHasError && !proposerHasError;
};

var checkCanShieldApplicationGoPayment = exports.checkCanShieldApplicationGoPayment = function checkCanShieldApplicationGoPayment(_ref22) {
  var application = _ref22.application,
      error = _ref22.error;

  var canGoSignature = checkCanShieldApplicationGoSignature(error);
  return canGoSignature && application.isProposalSigned && application.isAppFormProposerSigned;
};

var updateProfileList = exports.updateProfileList = function updateProfileList(_ref23) {
  var updIds = _ref23.updIds,
      application = _ref23.application;

  var profileList = [];

  var proposerPersonalInfo = _.get(application, "applicationForm.values.proposer.personalInfo");
  var insuredsArr = _.get(application, "applicationForm.values.insured", []);

  if (updIds.indexOf(_.get(proposerPersonalInfo, "cid") > -1)) {
    profileList.push(proposerPersonalInfo);
  }
  if (insuredsArr.length > 0) {
    insuredsArr.forEach(function (insured) {
      if (updIds.indexOf(_.get(insured, "personalInfo.cid")) > -1) {
        profileList.push(insured.personalInfo);
      }
    });
  }
  return profileList;
};

var updateApplicationFormCompletedMenus = exports.updateApplicationFormCompletedMenus = function updateApplicationFormCompletedMenus(_ref24) {
  var template = _ref24.template,
      application = _ref24.application,
      error = _ref24.error,
      sectionKey = _ref24.sectionKey,
      isInit = _ref24.isInit;

  var checkedMenu = _.get(application, "applicationForm.values.checkedMenu", []);
  var completedMenus = _.get(application, "applicationForm.values.completedMenus", []);
  var hasInsurability = checkHasInsurability({ application: application, template: template });

  var sectionHasError = sectionKey === PERSONAL_DETAILS ? checkPersonalDetailsHasError({ error: error }) : checkApplicationFormSectionHasError({ sectionKey: sectionKey, error: error });
  if (hasInsurability && sectionKey === INSURABILITY || !hasInsurability && sectionKey === DECLARATION) {
    sectionHasError = sectionHasError || !checkHasPolicyNumber({ application: application, error: error });
  }

  var removeCompletedMenu = sectionHasError && completedMenus.includes(sectionKey);
  var addCompletedMenu = !sectionHasError && !completedMenus.includes(sectionKey);

  var removeCheckedMenu = isInit && sectionHasError && checkedMenu.includes(sectionKey);
  var addCheckedMenu = isInit && !sectionHasError && !checkedMenu.includes(sectionKey);

  if (removeCompletedMenu) {
    var index = completedMenus.indexOf(sectionKey);
    completedMenus.splice(index, 1);
  } else if (addCompletedMenu) {
    completedMenus.push(sectionKey);
  }

  if (removeCheckedMenu) {
    var _index = checkedMenu.indexOf(sectionKey);
    checkedMenu.splice(_index, 1);
  } else if (addCheckedMenu) {
    checkedMenu.push(sectionKey);
  }
};