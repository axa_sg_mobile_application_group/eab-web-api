"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNeedsSummaryOptions = exports.feDataGetter = exports.getPriorityOptions = exports.getAspectOptions = undefined;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _NEEDS = require("../constants/NEEDS");

var _DEPENDANT = require("../constants/DEPENDANT");

var _clientUtil = require("./clientUtil");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var RELATIONSHIP_TYPE = {
  OWNER: "OWNER",
  SPOUSE: "SPOUSE",
  DEPENDANTS: "DEPENDANTS"
};

var getSeperateObj = function getSeperateObj(id) {
  var seperateObjectById = {
    spAspects: {
      id: "pcHeadstart",
      items: [{
        title: "Planning for children's headstart - Protection",
        value: "pTotShortfall",
        checkValue: "P"
      }, {
        title: "Planning for children's headstart - Critical Illness",
        value: "ciTotShortfall",
        checkValue: "CI"
      }]
    },
    sfAspects: {
      id: "pcHeadstart",
      items: [{
        title: "Protection",
        value: "pTotShortfall",
        type: "protection",
        checkValue: "P"
      }, {
        title: "Critical Illness",
        value: "ciTotShortfall",
        type: "criticalIllness",
        checkValue: "CI"
      }]
    }
  };
  return seperateObjectById[id];
};

var filterAspect = function filterAspect(_ref) {
  var relationship = _ref.relationship,
      filter = _ref.filter,
      dependants = _ref.dependants,
      pdaDependants = _ref.pdaDependants,
      applicant = _ref.applicant,
      cid = _ref.cid,
      type = _ref.type;

  type = _.toUpper(type);
  relationship = _.toUpper(relationship);
  // is exist options
  if (relationship.indexOf(type) > -1) {
    if (type === RELATIONSHIP_TYPE.OWNER) {
      return !filter || filter.indexOf(type) > -1;
    } else if (type === RELATIONSHIP_TYPE.SPOUSE) {
      return !filter || filter.indexOf(type) > -1 && applicant === "joint" && _.find(dependants, function (_d) {
        return _d.cid === cid && _d.relationship === _DEPENDANT.SPO;
      });
    } else if (type === RELATIONSHIP_TYPE.DEPENDANTS) {
      // is option selected in personal data acknowledge?
      if (pdaDependants && pdaDependants.indexOf(cid) > -1) {
        var dependant = _.find(dependants, function (_d) {
          return _d.cid === cid;
        });

        if (_.get(dependant, "relationship") === _DEPENDANT.SPO && applicant === "single") {
          // Make Spouse visiable when spouse is in filter of the items and the application is single
          return !filter || _.indexOf(_.split(filter, ","), dependant.relationship) > -1 || false;
        } else if (_.get(dependant, "relationship") === _DEPENDANT.SPO && applicant === "joint") {
          // Filter out the SPO in dependants When the application is joint
          return false;
        }
        if (dependant) {
          return !filter || filter.indexOf(dependant.relationship) > -1;
        }
      }
    } else {
      return true;
    }
  }
  return false;
};

var returnOptionValue = function returnOptionValue(_ref2) {
  var profile = _ref2.profile,
      dependantProfiles = _ref2.dependantProfiles,
      aspectItem = _ref2.aspectItem,
      aspectValue = _ref2.aspectValue,
      index = _ref2.index,
      inCompleteFlag = _ref2.inCompleteFlag;

  var optionTitle = "";
  if (aspectItem.id === "psGoals") {
    optionTitle = "Planning for specific goals";
  } else if (aspectItem.id === "other") {
    optionTitle = "Other needs (e.g. mortgage, pregnancy, savings)";
  }

  return {
    // for sortable list
    id: aspectValue.cid + "-" + aspectItem.id + "-" + index,
    name: (0, _clientUtil.getClientName)({ profile: profile, dependantProfiles: dependantProfiles, cid: aspectValue.cid }),
    cid: aspectValue.cid,
    aid: aspectItem.id,
    title: aspectItem.title || _.get(aspectValue, "goals[" + index + "].goalName") || optionTitle,
    typeOfNeeds: _.get(aspectValue, "goals[" + index + "].typeofNeeds"),
    type: _.get(aspectValue, "goals[" + index + "].typeofNeeds"),
    totShortfall: aspectValue.totShortfall,
    timeHorizon: aspectValue.timeHorizon,
    image: aspectItem.image,
    inCompleteOption: inCompleteFlag
  };
};

var handlePriorityOption = function handlePriorityOption(_ref3) {
  var profile = _ref3.profile,
      dependantProfiles = _ref3.dependantProfiles,
      aspectItem = _ref3.aspectItem,
      aspectValue = _ref3.aspectValue,
      _ref3$seperateObj = _ref3.seperateObj,
      seperateObj = _ref3$seperateObj === undefined ? [] : _ref3$seperateObj,
      inCompleteFlag = _ref3.inCompleteFlag;

  var result = [];
  var tempAspectValue = _.cloneDeep(aspectValue);
  var tempGoals = _.get(tempAspectValue, "goals");
  var tempAspectItem = _.cloneDeep(aspectItem);
  var goalNo = _.get(aspectValue, "goalNo") || 0;
  if (tempGoals) {
    _.forEach(tempGoals, function (Obj, index) {
      tempAspectValue.totShortfall = Obj.totShortfall;
      tempAspectValue.timeHorizon = Obj.timeHorizon;
      tempAspectItem.title = Obj.title;
      if (index < goalNo) {
        result.push(returnOptionValue({
          profile: profile,
          dependantProfiles: dependantProfiles,
          aspectItem: tempAspectItem,
          aspectValue: tempAspectValue,
          index: index,
          inCompleteFlag: inCompleteFlag
        }));
      }
    });
    return result;
  }

  if (aspectItem.id === "pcHeadstart" && !inCompleteFlag) {
    var providedFor = aspectValue.providedFor || "";
    if (providedFor.indexOf("P") > -1) {
      var pAspectValue = _.cloneDeep(aspectValue);
      var pAspectItem = _.cloneDeep(aspectItem);
      pAspectValue.totShortfall = pAspectValue.pTotShortfall;
      pAspectItem.title = "Planning for children's headstart - Protection";
      result.push(returnOptionValue({
        profile: profile,
        dependantProfiles: dependantProfiles,
        aspectItem: pAspectItem,
        aspectValue: pAspectValue,
        index: 0,
        inCompleteFlag: inCompleteFlag
      }));
    }

    if (providedFor.indexOf("C") > -1) {
      var cAspectValue = _.cloneDeep(aspectValue);
      var cAspectItem = _.cloneDeep(aspectItem);
      cAspectValue.totShortfall = cAspectValue.ciTotShortfall;
      cAspectItem.title = "Planning for children's headstart - Critical Illness";
      result.push(returnOptionValue({
        profile: profile,
        dependantProfiles: dependantProfiles,
        aspectItem: cAspectItem,
        aspectValue: cAspectValue,
        index: 1,
        inCompleteFlag: inCompleteFlag
      }));
    }

    if (result.length) {
      return result;
    }
  }

  // seperate option
  var sObj = _.find(seperateObj, function (s) {
    return s.id === aspectItem.id;
  });
  if (sObj) {
    _.forEach(sObj.items, function (obj, sIndex) {
      var providedForArray = _.split(aspectValue.providedFor, ",");

      if (providedForArray.indexOf(obj.checkValue) > -1) {
        var sAspectValue = _.cloneDeep(aspectValue);
        var sAspectItem = _.cloneDeep(aspectItem);
        sAspectValue.totShortfall = aspectValue[obj.value];
        sAspectItem.title = obj.title;
        sAspectItem.type = obj.type;
        result.push(returnOptionValue({
          profile: profile,
          dependantProfiles: dependantProfiles,
          aspectItem: sAspectItem,
          aspectValue: sAspectValue,
          index: sIndex,
          inCompleteFlag: inCompleteFlag
        }));
      }
    });
    return result;
  }

  return returnOptionValue({
    profile: profile,
    dependantProfiles: dependantProfiles,
    aspectItem: aspectItem,
    aspectValue: aspectValue,
    index: 0,
    inCompleteFlag: inCompleteFlag
  });
};

var concatPriorityOption = function concatPriorityOption(_ref4) {
  var profile = _ref4.profile,
      dependantProfiles = _ref4.dependantProfiles,
      aspectItem = _ref4.aspectItem,
      aspectValue = _ref4.aspectValue,
      seperateObj = _ref4.seperateObj,
      seperateId = _ref4.seperateId,
      aspect = _ref4.aspect,
      relationship = _ref4.relationship,
      inCompleteFlag = _ref4.inCompleteFlag;

  var tempOptions = handlePriorityOption({
    profile: profile,
    dependantProfiles: dependantProfiles,
    aspectItem: aspectItem,
    aspectValue: aspectValue,
    seperateObj: seperateObj,
    seperateId: seperateId,
    aspect: aspect,
    relationship: relationship,
    inCompleteFlag: inCompleteFlag
  });
  var result = [];
  if (_.isArray(tempOptions)) {
    result = result.concat(tempOptions);
  } else {
    result.push(tempOptions);
  }

  return result;
};

var sortProfiles = function sortProfiles(a, b) {
  var sortArr = [_DEPENDANT.SON, _DEPENDANT.DAU, _DEPENDANT.SPO];
  var priorityA = sortArr.indexOf(a.relationship);
  var priorityB = sortArr.indexOf(b.relationship);

  if (a.relationship === b.relationship && a.fullName > b.fullName) return 1;else if (a.relationship === b.relationship && a.fullName < b.fullName) return -1;
  if (priorityA > priorityB && priorityA !== -1 && priorityB !== -1) return -1;else if (priorityA < priorityB && priorityA !== -1 && priorityB !== -1) return 1;else if (priorityA > -1 && priorityB === -1) return -1;else if (priorityB > -1 && priorityA === -1) return 1;else if (a.relationship > b.relationship) return 1;else if (a.relationship < b.relationship) return -1;
  return 0;
};

var getAspectOptions = exports.getAspectOptions = function getAspectOptions() {
  return [{
    id: _NEEDS.FIPROTECTION,
    relationship: "OWNER,SPOUSE",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE],
    image: "fiProtectionImg"
  }, {
    id: _NEEDS.CIPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE],
    image: "ciProtectionImg"
  }, {
    id: _NEEDS.DIPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE, _DEPENDANT.SON, _DEPENDANT.DAU],
    image: "diProtectionImg"
  }, {
    id: _NEEDS.PAPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE, _DEPENDANT.SON, _DEPENDANT.DAU],
    image: "paProtectionImg"
  }, {
    id: _NEEDS.PCHEADSTART,
    relationship: "DEPENDANTS",
    filter: [_DEPENDANT.SON, _DEPENDANT.DAU],
    image: "pcHeadstartImg"
  }, {
    id: _NEEDS.HCPROTECTION,
    relationship: "OWNER,SPOUSE,DEPENDANTS",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE, _DEPENDANT.SPO, _DEPENDANT.SON, _DEPENDANT.DAU, _DEPENDANT.FAT, _DEPENDANT.MOT, _DEPENDANT.GFA, _DEPENDANT.GMO],
    image: "hcProtectionImg"
  }, {
    id: _NEEDS.PSGOALS,
    relationship: "OWNER,SPOUSE",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE],
    image: "psGoalsImg"
  }, {
    id: _NEEDS.EPLANNING,
    relationship: "DEPENDANTS",
    filter: [_DEPENDANT.SON, _DEPENDANT.DAU],
    image: "ePlanningImg"
  }, {
    id: _NEEDS.RPLANNING,
    relationship: "OWNER,SPOUSE",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE],
    image: "rPlanningImg"
  }, {
    id: _NEEDS.OTHER,
    relationship: "OWNER,SPOUSE",
    filter: [_DEPENDANT.OWNER, _DEPENDANT.SPOUSE],
    image: "otherAspectImg"
  }];
};

var getPriorityOptions = exports.getPriorityOptions = function getPriorityOptions(_ref5) {
  var subType = _ref5.subType,
      fnaValue = _ref5.fnaValue,
      profile = _ref5.profile,
      dependantProfiles = _ref5.dependantProfiles,
      pda = _ref5.pda;

  var id = _.toLower(subType) === "shortfall" ? "sfAspects" : "spAspects";

  var options = [];
  var cAspectValue = fnaValue[id];
  var dependants = profile.dependants;
  var applicant = pda.applicant,
      pdaDependants = pda.dependants;

  var seperateId = [_NEEDS.PCHEADSTART];
  var seperateObj = getSeperateObj(id);
  var groupId = _NEEDS.HCPROTECTION;

  var aspects = _.split(fnaValue.aspects, ",");
  _.forEach(aspects, function (aspect) {
    var aspectValue = fnaValue[aspect];
    if (aspectValue) {
      var aspectItem = _.find(getAspectOptions(), function (option) {
        return option.id === aspect;
      });
      var ownerCid = _.get(aspectValue, "owner.cid");
      if (ownerCid && filterAspect({
        relationship: aspectItem.relationship,
        filter: aspectItem.filter,
        dependants: dependants,
        pdaDependants: pdaDependants,
        applicant: applicant,
        cid: ownerCid,
        type: RELATIONSHIP_TYPE.OWNER
      }) && aspectValue.owner.isActive) {
        options = options.concat(concatPriorityOption({
          profile: profile,
          dependantProfiles: dependantProfiles,
          aspectItem: aspectItem,
          aspectValue: aspectValue.owner,
          seperateObj: seperateObj,
          seperateId: seperateId,
          aspect: aspect,
          relationship: RELATIONSHIP_TYPE.OWNER
        }));
      }
      var spouseId = _.get(aspectValue, "spouse.cid");
      if (spouseId && filterAspect({
        relationship: aspectItem.relationship,
        filter: aspectItem.filter,
        dependants: dependants,
        pdaDependants: pdaDependants,
        applicant: applicant,
        cid: spouseId,
        type: RELATIONSHIP_TYPE.SPOUSE
      }) && aspectValue.spouse.isActive) {
        options = options.concat(concatPriorityOption({
          profile: profile,
          dependantProfiles: dependantProfiles,
          aspectItem: aspectItem,
          aspectValue: aspectValue.spouse,
          seperateObj: seperateObj,
          seperateId: seperateId,
          aspect: aspect,
          relationship: RELATIONSHIP_TYPE.SPOUSE
        }));
      }
      if (!_.isEmpty(aspectValue.dependants)) {
        _.forEach(aspectValue.dependants, function (de) {
          if (filterAspect({
            relationship: aspectItem.relationship,
            filter: aspectItem.filter,
            dependants: dependants,
            pdaDependants: pdaDependants,
            applicant: applicant,
            cid: de.cid,
            type: RELATIONSHIP_TYPE.DEPENDANTS
          }) && de.isActive) {
            options = options.concat(concatPriorityOption({
              profile: profile,
              dependantProfiles: dependantProfiles,
              aspectItem: aspectItem,
              aspectValue: de,
              seperateObj: seperateObj,
              seperateId: seperateId,
              aspect: aspect,
              relationship: RELATIONSHIP_TYPE.OWNER
            }));
          }
        });
      }
    }
  });

  var orderedCid = [{ cid: profile.cid }];
  var orderedDepCid = [];
  var tempOrderedDepCid = _.cloneDeep(dependantProfiles) || [];
  _.forEach(Object.keys(tempOrderedDepCid || []), function (key) {
    orderedDepCid.push(tempOrderedDepCid[key]);
  });
  var orderedOptions = [];
  orderedDepCid.sort(sortProfiles);
  orderedCid = orderedCid.concat(orderedDepCid);

  if (groupId) {
    var gids = groupId.split(",");
    _.forEach(gids, function (gid) {
      var opts = [];
      for (var i = options.length - 1; i >= 0; i -= 1) {
        var opt = options[i];
        if (opt.aid === gid) {
          opts.push(opt);
          options.splice(i, 1);
        }
      }
      _.forEach(orderedCid, function (item) {
        var opt = _.find(opts, ["cid", item.cid]);
        if (_.isObject(opt)) orderedOptions.push(opt);
      });
      var cid = "";
      var name = "";
      var nameArray = [];
      if (orderedOptions.length) {
        _.forEach(orderedOptions, function (opt) {
          cid += (cid !== "" ? "," : "") + opt.cid;
          name += (name !== "" ? ", " : "") + opt.name;
          nameArray.push(opt.name);
        });
        options.push({
          id: gid,
          cid: cid,
          name: name,
          nameArray: nameArray,
          aid: gid,
          title: orderedOptions[0].title,
          image: orderedOptions[0].image
        });
      }
    });
  }
  var optionsFullLength = options.length;

  if (_.toUpper(subType) === "SHORTFALL") {
    options = options.filter(function (o) {
      return !_.isNumber(o.totShortfall) || o.totShortfall < 0;
    });
  } else if (_.toUpper(subType) === "SURPLUS") {
    options = options.filter(function (o) {
      return o.totShortfall >= 0;
    });
  }

  if (cAspectValue) {
    var cOptions = [];
    _.forEach(cAspectValue, function (c) {
      var oi = _.findIndex(options, function (o) {
        return (groupId && groupId.indexOf(c.aid) > -1 || o.cid === c.cid) && o.aid === c.aid;
      });
      if (oi !== -1) {
        cOptions.push(options[oi]);
        options.splice(oi, 1);
      }
    });

    _.forEach(options, function (opt) {
      cOptions.push(opt);
    });
    options = cOptions;
  }

  return [options, optionsFullLength];
};
/**
 * @description map fe data form fe(with data initial)
 * @param {object} profileData - reducer client.profile state
 * @param {object} dependantProfilesData - reducer client.dependantProfilesData
 *   state
 * @param {object} fe - reducer fna.fe state
 * @param {object} relationshipObject - relationship between the client and the
 *   editing person
 * @param {object} relationshipObject.cid - editing person cid
 * @param {object} relationshipObject.relationship - editing person cid
 * @return {object} fe data object
 * */
var feDataGetter = exports.feDataGetter = function feDataGetter(_ref6) {
  var profileData = _ref6.profileData,
      dependantProfilesData = _ref6.dependantProfilesData,
      fe = _ref6.fe,
      relationshipObject = _ref6.relationshipObject;

  switch (relationshipObject.relationship) {
    case "owner":
      {
        var initialData = Object.assign({
          cid: relationshipObject.cid,
          init: true,
          // net worth
          savAcc: 0,
          fixDeposit: 0,
          invest: 0,
          property: 0,
          car: 0,
          cpfOa: 0,
          cpfSa: 0,
          cpfMs: 0,
          srs: 0,
          otherAssetTitle: "",
          otherAsset: 0,
          mortLoan: 0,
          motLoan: 0,
          eduLoan: 0,
          otherLiabilitesTitle: "",
          otherLiabilites: 0,
          noALReason: "",
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: "",
          // cash flow
          lessMCPF: 0,
          aBonus: 0,
          personExpenses: 0,
          householdExpenses: 0,
          regSav: 0,
          otherExpenseTitle: "",
          otherExpense: 0,
          additionComment: "",
          noCFReason: "",
          // My Budget (Annual Disposable Income Committed for Regular Premium Budget)
          aRegPremBudget: 0,
          aRegPremBudgetSrc: "",
          confirmBudget: "",
          confirmBudgetReason: "",
          // My Budget (Single Premium)
          singPrem: 0,
          singPremSrc: "",
          confirmSingPremBudget: "",
          confirmSingPremBudgetReason: "",
          // My Budget - CPF(OA)
          cpfOaBudget: 0,
          // My Budget - CPF(SA)
          cpfSaBudget: 0,
          // My Budget - CPF Medisave
          cpfMsBudget: 0,
          // My Budget - SRS
          srsBudget: 0,
          // forceIncome
          forceIncome: "",
          forceIncomeReason: ""
        }, fe.owner || {});
        // net worth
        var assets = initialData.savAcc + initialData.fixDeposit + initialData.invest + initialData.property + initialData.car + initialData.cpfOa + initialData.cpfSa + initialData.cpfMs + initialData.srs + initialData.otherAsset;
        var liabilities = initialData.mortLoan + initialData.motLoan + initialData.eduLoan + initialData.otherLiabilites;
        var netWorth = assets - liabilities;
        // existing insurance portfolio
        var eiPorf = initialData.lifeInsProt + initialData.retirePol + initialData.ednowSavPol + initialData.eduFund + initialData.invLinkPol + initialData.disIncomeProt + initialData.ciPort + initialData.personAcc;
        var aInsPrem = initialData.pAInsPrem + initialData.sAInsPrem + initialData.oAInsPrem;
        var sInsPrem = initialData.caInsPrem + initialData.csInsPrem + initialData.mdInsPrem + initialData.fpInsPrem;
        // cash flow
        var mIncome = Number.isNaN(profileData.allowance) ? 0 : profileData.allowance;
        var netMIcncome = mIncome - initialData.lessMCPF;
        var totAIncome = netMIcncome * 12 + initialData.aBonus;

        // get insurancePrem
        var insurancePrem = initialData.pAInsPrem;
        if (fe.spouse && fe.spouse.pAInsPrem) {
          insurancePrem += fe.spouse.pAInsPrem || 0;
        }
        if (fe.dependants && Array.isArray(fe.dependants)) {
          fe.dependants.forEach(function (dependant) {
            insurancePrem += dependant.pAInsPrem || 0;
          });
        }
        insurancePrem = Math.round(insurancePrem / 12 * 100) / 100;
        var totMExpense = initialData.personExpenses + initialData.householdExpenses + insurancePrem + initialData.regSav + initialData.otherExpense;
        var totAExpense = totMExpense * 12;
        var aDisposeIncome = totAIncome - totAExpense;
        var mDisposeIncome = Math.round(aDisposeIncome / 12);
        // My Budget (Annual Disposable Income Committed for Regular Premium Budget)
        // get confirmBudget, need to initial in initialData for do not cover the source data(fe)
        var confirmBudget = initialData.confirmBudget;

        if (mDisposeIncome !== 0) {
          confirmBudget = initialData.aRegPremBudget > aDisposeIncome / 2 ? "Y" : "N";
        }
        // My Budget (Single Premium)
        // get confirmSingPremBudget, need to initial in initialData for do not cover the source data(fe)
        var confirmSingPremBudget = initialData.confirmSingPremBudget;

        if (initialData.savAcc + initialData.fixDeposit + initialData.invest !== 0) {
          confirmSingPremBudget = initialData.singPrem > (initialData.savAcc + initialData.fixDeposit + initialData.invest) / 2 ? "Y" : "N";
        }

        return Object.assign({}, initialData, {
          netWorth: netWorth,
          assets: assets,
          liabilities: liabilities,
          eiPorf: eiPorf,
          aInsPrem: aInsPrem,
          sInsPrem: sInsPrem,
          mIncome: mIncome,
          netMIcncome: netMIcncome,
          totAIncome: totAIncome,
          insurancePrem: insurancePrem,
          totMExpense: totMExpense,
          totAExpense: totAExpense,
          aDisposeIncome: aDisposeIncome,
          mDisposeIncome: mDisposeIncome,
          confirmBudget: confirmBudget,
          confirmSingPremBudget: confirmSingPremBudget
        });
      }
    case "SPO":
      {
        var _initialData = Object.assign({
          init: true,
          // net worth
          savAcc: 0,
          fixDeposit: 0,
          invest: 0,
          property: 0,
          car: 0,
          cpfOa: 0,
          cpfSa: 0,
          cpfMs: 0,
          srs: 0,
          otherAssetTitle: "",
          otherAsset: 0,
          mortLoan: 0,
          motLoan: 0,
          eduLoan: 0,
          otherLiabilitesTitle: "",
          otherLiabilites: 0,
          noALReason: "",
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: "",
          // cash flow
          lessMCPF: 0,
          aBonus: 0,
          personExpenses: 0,
          householdExpenses: 0,
          regSav: 0,
          otherExpenseTitle: "",
          otherExpense: 0,
          additionComment: "",
          noCFReason: ""
        }, fe.spouse || {}, {
          cid: relationshipObject.cid
        });
        // net worth
        var _assets = _initialData.savAcc + _initialData.fixDeposit + _initialData.invest + _initialData.property + _initialData.car + _initialData.cpfOa + _initialData.cpfSa + _initialData.cpfMs + _initialData.srs + _initialData.otherAsset;
        var _liabilities = _initialData.mortLoan + _initialData.motLoan + _initialData.eduLoan + _initialData.otherLiabilites;
        var _netWorth = _assets - _liabilities;
        // existing insurance portfolio
        var _eiPorf = _initialData.lifeInsProt + _initialData.retirePol + _initialData.ednowSavPol + _initialData.eduFund + _initialData.invLinkPol + _initialData.disIncomeProt + _initialData.ciPort + _initialData.personAcc;
        var _aInsPrem = _initialData.pAInsPrem + _initialData.sAInsPrem + _initialData.oAInsPrem;
        var _sInsPrem = _initialData.caInsPrem + _initialData.csInsPrem + _initialData.mdInsPrem + _initialData.fpInsPrem;
        // cash flow
        var _mIncome = Number.isNaN(dependantProfilesData[relationshipObject.cid].allowance) ? 0 : dependantProfilesData[relationshipObject.cid].allowance;
        var _netMIcncome = _mIncome - _initialData.lessMCPF;
        var _totAIncome = _netMIcncome * 12 + _initialData.aBonus;

        // get insurancePrem
        var _insurancePrem = _initialData.sAInsPrem + (fe.owner.sAInsPrem || 0);
        if (fe.dependants && Array.isArray(fe.dependants)) {
          fe.dependants.forEach(function (dependant) {
            _insurancePrem += dependant.sAInsPrem || 0;
          });
        }
        _insurancePrem = Math.round(_insurancePrem / 12 * 100) / 100;

        var _totMExpense = _initialData.personExpenses + _initialData.householdExpenses + _insurancePrem + _initialData.regSav + _initialData.otherExpense;
        var _totAExpense = _totMExpense * 12;
        var _aDisposeIncome = _totAIncome - _totAExpense;
        var _mDisposeIncome = Math.round(_aDisposeIncome / 12);

        return Object.assign({}, _initialData, {
          netWorth: _netWorth,
          assets: _assets,
          liabilities: _liabilities,
          eiPorf: _eiPorf,
          aInsPrem: _aInsPrem,
          sInsPrem: _sInsPrem,
          mIncome: _mIncome,
          netMIcncome: _netMIcncome,
          totAIncome: _totAIncome,
          insurancePrem: _insurancePrem,
          totMExpense: _totMExpense,
          totAExpense: _totAExpense,
          aDisposeIncome: _aDisposeIncome,
          mDisposeIncome: _mDisposeIncome
        });
      }
    case "SON":
    case "DAU":
      {
        var dependantData = _.find(fe.dependants, function (opt) {
          return opt.cid === relationshipObject.cid;
        });
        var _initialData2 = Object.assign({
          init: true,
          // existing insurance portfolio
          lifeInsProt: 0,
          retirePol: 0,
          ednowSavPol: 0,
          eduFund: 0,
          invLinkPol: 0,
          disIncomeProt: 0,
          ciPort: 0,
          personAcc: 0,
          hospNSurg: "",
          pAInsPrem: 0,
          sAInsPrem: 0,
          oAInsPrem: 0,
          caInsPrem: 0,
          csInsPrem: 0,
          mdInsPrem: 0,
          fpInsPrem: 0,
          noEIReason: ""
        }, dependantData || {}, {
          cid: relationshipObject.cid
        });
        // existing insurance portfolio
        var _eiPorf2 = _initialData2.lifeInsProt + _initialData2.retirePol + _initialData2.ednowSavPol + _initialData2.eduFund + _initialData2.invLinkPol + _initialData2.disIncomeProt + _initialData2.ciPort + _initialData2.personAcc;
        var _aInsPrem2 = _initialData2.pAInsPrem + _initialData2.sAInsPrem + _initialData2.oAInsPrem;
        var _sInsPrem2 = _initialData2.caInsPrem + _initialData2.csInsPrem + _initialData2.mdInsPrem + _initialData2.fpInsPrem;

        return Object.assign({}, _initialData2, {
          eiPorf: _eiPorf2,
          aInsPrem: _aInsPrem2,
          sInsPrem: _sInsPrem2
        });
      }
    default:
      throw new Error("feData got unexpected relationship \"" + relationshipObject.relationship + "\".");
  }
};

var getNeedsSummaryOptions = exports.getNeedsSummaryOptions = function getNeedsSummaryOptions(_ref7) {
  var subType = _ref7.subType,
      fnaValue = _ref7.fnaValue,
      profile = _ref7.profile,
      dependantProfiles = _ref7.dependantProfiles,
      pda = _ref7.pda;

  var id = _.toLower(subType) === "shortfall" ? "sfAspects" : "spAspects";

  var dependants = profile.dependants;
  var applicant = pda.applicant,
      pdaDependants = pda.dependants;


  var aspects = fnaValue.aspects ? fnaValue.aspects.split(",") : undefined;
  var cAspectValue = fnaValue[id];

  var listNoforSurplus = void 0;
  var fnaError = {};
  // TODO update validation without dynamic form logic
  // _na.validateFNA(context, needs.template.fnaForm, changedValues, fnaError);

  var seperateId = [_NEEDS.PCHEADSTART];
  var seperateObj = getSeperateObj(id);
  var groupId = _NEEDS.HCPROTECTION;

  var options = [];
  var inCompleteFlag = void 0;
  _.forEach(aspects, function (aspect) {
    var aspectValue = fnaValue[aspect];
    var aError = fnaError[aspect];
    inCompleteFlag = false;
    // Selected the aspect but not yet fil the fna, initialize the value
    if (_.isUndefined(aspectValue) || _.isEmpty(aspectValue)) {
      aspectValue = {};
      aspectValue.owner = profile;
      aspectValue.spouse = (0, _clientUtil.getSpouseProfile)({ profile: profile, dependantProfiles: dependantProfiles });
      aspectValue.dependants = [];
      _.forEach(aspectValue.owner.dependants, function (dep) {
        if (_.isUndefined(aspectValue.spouce)) {
          aspectValue.dependants.push(dependantProfiles[dep.cid]);
        } else if (dep.cid !== aspectValue.spouse.cid && dependantProfiles[dep.cid]) {
          aspectValue.dependants.push(dependantProfiles[dep.cid]);
        }
      });
      inCompleteFlag = true;
    }

    var aspectItem = _.find(getAspectOptions(), function (option) {
      return option.id === aspect;
    });
    var ownerCid = _.get(aspectValue, "owner.cid");
    if (aspectItem && filterAspect({
      relationship: aspectItem.relationship,
      filter: aspectItem.filter,
      dependants: dependants,
      pdaDependants: pdaDependants,
      applicant: applicant,
      cid: ownerCid,
      type: RELATIONSHIP_TYPE.OWNER
    }) && _.get(aspectValue, "owner.isActive") && aspectValue.owner && _.isBoolean(_.get(aspectValue, "owner.init"))) {
      inCompleteFlag = !_.isEmpty(_.get(aError, "owner.code"));

      options = options.concat(concatPriorityOption({
        profile: profile,
        dependantProfiles: dependantProfiles,
        aspectItem: aspectItem,
        aspectValue: aspectValue.owner,
        seperateObj: seperateObj,
        seperateId: seperateId,
        aspect: aspect,
        relationship: RELATIONSHIP_TYPE.OWNER,
        inCompleteFlag: inCompleteFlag
      }));
    }
    var spouseId = _.get(aspectValue, "spouse.cid");
    if (aspectItem && filterAspect({
      relationship: aspectItem.relationship,
      filter: aspectItem.filter,
      dependants: dependants,
      pdaDependants: pdaDependants,
      applicant: applicant,
      cid: spouseId,
      type: RELATIONSHIP_TYPE.SPOUSE
    }) && _.get(aspectValue, "spouse.isActive") && aspectValue.spouse && _.isBoolean(_.get(aspectValue, "spouse.init"))) {
      inCompleteFlag = !_.isEmpty(_.get(aError, "spouse.code"));

      options = options.concat(concatPriorityOption({
        profile: profile,
        dependantProfiles: dependantProfiles,
        aspectItem: aspectItem,
        aspectValue: aspectValue.spouse,
        seperateObj: seperateObj,
        seperateId: seperateId,
        aspect: aspect,
        relationship: RELATIONSHIP_TYPE.SPOUSE,
        inCompleteFlag: inCompleteFlag
      }));
    }
    if (aspectItem && !_.isEmpty(aspectValue.dependants)) {
      _.forEach(aspectValue.dependants, function (de) {
        if (filterAspect({
          relationship: aspectItem.relationship,
          filter: aspectItem.filter,
          dependants: dependants,
          pdaDependants: pdaDependants,
          applicant: applicant,
          cid: de.cid,
          type: RELATIONSHIP_TYPE.DEPENDANTS
        }) && de && de.isActive && _.isBoolean(de.init)) {
          inCompleteFlag = !_.isEmpty(_.get(aError, "dependants." + de.cid + ".code")) || _.get(aError, "dependants." + de.cid + ".skip");

          options = options.concat(concatPriorityOption({
            profile: profile,
            dependantProfiles: dependantProfiles,
            aspectItem: aspectItem,
            aspectValue: de,
            seperateObj: seperateObj,
            seperateId: seperateId,
            aspect: aspect,
            relationship: RELATIONSHIP_TYPE.OWNER,
            inCompleteFlag: inCompleteFlag
          }));
        }
      });
    }
    // }
  });

  var orderedCid = [{ cid: profile.cid }];
  var orderedDepCid = [];
  var tempOrderedDepCid = _.cloneDeep(dependantProfiles) || [];
  _.forEach(Object.keys(tempOrderedDepCid || []), function (key) {
    orderedDepCid.push(tempOrderedDepCid[key]);
  });
  var orderedOptions = [];
  orderedDepCid.sort(sortProfiles);
  orderedCid = orderedCid.concat(orderedDepCid);

  if (groupId) {
    var gids = groupId.split(",");
    _.forEach(gids, function (gid) {
      var opts = [];
      for (var i = options.length - 1; i >= 0; i -= 1) {
        var opt = options[i];
        if (opt.aid === gid) {
          opts.push(opt);
          options.splice(i, 1);
        }
      }

      var tempOption = void 0;
      _.forEach(orderedCid, function (obj) {
        tempOption = _.find(opts, ["cid", obj.cid]);
        if (_.isObject(tempOption)) orderedOptions.push(tempOption);
      });

      var cid = "";
      var name = "";
      if (orderedOptions.length) {
        _.forEach(orderedOptions, function (opt) {
          cid += (cid !== "" ? "," : "") + opt.cid;
          name += (name !== "" ? ", " : "") + opt.name;
        });
        options.push({
          id: gid,
          cid: cid,
          name: name,
          aid: gid,
          aTitle: orderedOptions[0].aTitle,
          image: orderedOptions[0].image,
          inCompleteOption: orderedOptions[0].inCompleteOption
        });
      }
    });
  }

  listNoforSurplus = options.length;
  if (_.toUpper(subType) === "SHORTFALL") {
    options = options.filter(function (o) {
      return o.totShortfall < 0 || o.aid === _NEEDS.HCPROTECTION;
    });
  } else if (_.toUpper(subType) === "SURPLUS") {
    options = options.filter(function (o) {
      return o.aid !== _NEEDS.HCPROTECTION && (o.totShortfall >= 0 || !o.totShortfall);
    });
    listNoforSurplus -= options.length;
  }

  if (cAspectValue) {
    var cOptions = [];
    _.forEach(cAspectValue, function (c) {
      var oi = _.findIndex(options, function (o) {
        return (groupId && groupId.indexOf(c.aid) > -1 || o.cid === c.cid) && o.aid === c.aid;
      });
      if (oi !== -1) {
        cOptions.push(options[oi]);
        options.splice(oi, 1);
      }
    });

    _.forEach(options, function (opt) {
      cOptions.push(opt);
    });
    options = cOptions;
  }

  return [options, listNoforSurplus];
};