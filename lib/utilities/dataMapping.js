"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var formMapProfile = exports.formMapProfile = function formMapProfile(clientForm) {
  var object = {
    photo: clientForm.photo,
    firstName: clientForm.givenName,
    lastName: clientForm.surname,
    othName: clientForm.otherName,
    hanyuPinyinName: clientForm.hanYuPinYinName,
    fullName: clientForm.name,
    nameOrder: clientForm.nameOrder,
    title: clientForm.title,
    gender: clientForm.gender,
    nationality: clientForm.nationality,
    residenceCountry: clientForm.countryOfResidence,
    otherCityOfResidence: clientForm.otherCityOfResidence,
    idDocType: clientForm.IDDocumentType,
    idCardNo: clientForm.ID,
    isSmoker: clientForm.smokingStatus,
    marital: clientForm.maritalStatus,
    education: clientForm.education,
    employStatus: clientForm.employStatus,
    industry: clientForm.industry,
    occupation: clientForm.occupation,
    organization: clientForm.nameOfEmployBusinessSchool,
    organizationCountry: clientForm.countryOfEmployBusinessSchool,
    allowance: Number.isNaN(clientForm.income) ? "" : clientForm.income,
    mobileCountryCode: clientForm.prefixA,
    mobileNo: clientForm.mobileNoA,
    otherMobileCountryCode: clientForm.prefixB,
    otherNo: clientForm.mobileNoB,
    email: clientForm.email,
    addrCountry: clientForm.country,
    addrCity: clientForm.cityState,
    postalCode: clientForm.postal,
    addrBlock: clientForm.blockHouse,
    addrStreet: clientForm.streetRoad,
    unitNum: clientForm.unit,
    addrEstate: clientForm.buildingEstate,
    newCid: clientForm.profileBackUp.cid
  };

  if (clientForm.cityOfResidence !== "") {
    object.residenceCity = clientForm.cityOfResidence;
  }

  if (clientForm.relationship !== "") {
    object.relationship = clientForm.relationship;
  }

  if (clientForm.relationshipOther !== "") {
    object.relationshipOther = clientForm.relationshipOther;
  }

  if (clientForm.birthday !== "") {
    object.dob = clientForm.birthday;
  }

  if (clientForm.otherCityOfResidence !== "") {
    object.otherResidenceCity = clientForm.otherCityOfResidence;
  }

  if (clientForm.typeOfPassOther !== "") {
    object.passOther = clientForm.typeOfPassOther;
  }

  if (clientForm.typeOfPass !== "") {
    object.pass = clientForm.typeOfPass;
  }

  if (clientForm.employStatusOther !== "") {
    object.employStatusOther = clientForm.employStatusOther;
  }

  if (clientForm.languageOther !== "") {
    object.languageOther = clientForm.languageOther;
  }

  if (clientForm.language !== "") {
    object.language = clientForm.language;
  }

  if (clientForm.IDDocumentTypeOther !== "") {
    object.idDocTypeOther = clientForm.IDDocumentTypeOther;
  }

  if (clientForm.occupationOther !== "") {
    object.occupationOther = clientForm.occupationOther;
  }

  if (clientForm.singaporePRStatus !== "") {
    object.prStatus = clientForm.singaporePRStatus;
  }

  if (!Number.isNaN(clientForm.passExpiryDate)) {
    object.passExpDate = clientForm.passExpiryDate;
  }

  return object;
};

var profileMapForm = exports.profileMapForm = function profileMapForm(profile) {
  return {
    newTitle: profile.title || "",
    newGivenName: profile.firstName || "",
    newSurname: profile.lastName || "",
    newOtherName: profile.othName || "",
    newName: profile.fullName || "",
    newHanYuPinYinName: profile.hanyuPinyinName || "",
    newHaveSignDoc: profile.haveSignDoc || false,
    newGender: profile.gender || "",
    newBirthday: profile.dob || "",
    newNameOrder: profile.nameOrder || "L",
    newNationality: profile.nationality || "",
    newCountryOfResidence: profile.residenceCountry || "R2",
    newOtherCityOfResidence: profile.otherCityOfResidence || "",
    newIDDocType: profile.idDocType || "",
    newIDDocTypeOther: profile.idDocTypeOther || "",
    newID: profile.idCardNo || "",
    newSmokingStatus: profile.isSmoker || "",
    newMaritalStatus: profile.marital || "",
    newEducation: profile.education || "",
    newEmployStatus: profile.employStatus || "",
    newIndustry: profile.industry || "",
    newOccupation: profile.occupation || "",
    newOccupationOther: profile.occupationOther || "",
    newLanguage: profile.language || "",
    newLanguageOther: profile.languageOther || "",
    newSingaporePRStatus: profile.prStatus || "",
    newCityOfResidence: profile.residenceCity || "",
    newEmployStatusOther: profile.employStatusOther || "",
    newTypeOfPass: profile.pass || "",
    newTypeOfPassOther: profile.passOther || "",
    newPassExpiryDate: profile.passExpDate !== undefined && profile.passExpDate !== null ? profile.passExpDate : NaN,
    newIncome: profile.allowance !== undefined && profile.allowance !== null ? profile.allowance : NaN,
    newNameOfEmployBusinessSchool: profile.organization || "",
    newCountryOfEmployBusinessSchool: profile.organizationCountry || "R2",
    newPrefixA: profile.mobileCountryCode || "+65",
    newMobileNoA: profile.mobileNo || "",
    newPrefixB: profile.otherMobileCountryCode || "+65",
    newMobileNoB: profile.otherNo || "",
    newEmail: profile.email || "",
    newCountry: profile.addrCountry || "R2",
    newCityState: profile.addrCity || "",
    newPostal: profile.postalCode || "",
    newBlockHouse: profile.addrBlock || "",
    newStreetRoad: profile.addrStreet || "",
    newUnit: profile.unitNum || "",
    newBuildingEstate: profile.addrEstate || "",
    newRelationship: profile.relationship || "",
    newRelationshipOther: profile.relationshipOther || "",
    newDependants: profile.dependants || "",
    newPhoto: profile.photo || {},
    newCid: profile.cid || "",
    newBundle: profile.bundle || [],
    newTrustedIndividuals: profile.trustedIndividuals || undefined
  };
};

var needsAnalysisReMap = exports.needsAnalysisReMap = function needsAnalysisReMap(data) {
  return {
    agentCode: data.agentCode,
    preNeedsProductsList: data.aspects,
    fiProtection: data.fiProtection || "",
    ciProtection: data.ciProtection || "",
    diProtection: data.diProtection || "",
    paProtection: data.paProtection || "",
    pcHeadstart: data.pcHeadstart || "",
    rPlanning: data.rPlanning || "",
    hcProtection: data.hcProtection || "",
    ePlanning: data.ePlanning || "",
    psGoals: data.psGoals || "",
    other: data.other || "",
    lastStepIndex: data.lastStepIndex,
    lastSelectedProduct: data.lastSelectedProduct || data.selectedMenuId,
    compCode: data.compCode
  };
};

var fnaReMapToSave = exports.fnaReMapToSave = function fnaReMapToSave(data) {
  return Object.assign({}, data, {
    lastStepIndex: data.lastStepIndex,
    lastSelectedProduct: data.lastSelectedProduct,
    selectedMenuId: data.lastSelectedProduct
  });
};

var mapCKA = exports.mapCKA = function mapCKA(cka) {
  return {
    isValid: cka.isValid !== undefined ? cka.isValid : false,
    owner: {
      init: cka.owner && cka.owner.init !== undefined ? cka.owner.init : true,
      course: cka.owner && cka.owner.course !== undefined ? cka.owner.course : "",
      institution: cka.owner && cka.owner.institution !== undefined ? cka.owner.institution : "",
      studyPeriodEndYear: cka.owner && cka.owner.studyPeriodEndYear !== undefined ? cka.owner.studyPeriodEndYear : "",
      collectiveInvestment: cka.owner && cka.owner.collectiveInvestment !== undefined ? cka.owner.collectiveInvestment : "",
      transactionType: cka.owner && cka.owner.transactionType !== undefined ? cka.owner.transactionType : "",
      insuranceInvestment: cka.owner && cka.owner.insuranceInvestment !== undefined ? cka.owner.insuranceInvestment : "",
      insuranceType: cka.owner && cka.owner.insuranceType !== undefined ? cka.owner.insuranceType : "",
      profession: cka.owner && cka.owner.profession !== undefined ? cka.owner.profession : "",
      years: cka.owner && cka.owner.years !== undefined ? cka.owner.years : "",
      passCka: cka.owner && cka.owner.passCka !== undefined ? cka.owner.passCka : ""
    }
  };
};

var pdaMapForm = exports.pdaMapForm = function pdaMapForm(pda) {
  return {
    id: pda.id === undefined ? "" : pda.id,
    applicant: pda.applicant === undefined ? "" : pda.applicant,
    trustedIndividual: pda.trustedIndividual === undefined ? null : pda.trustedIndividual,
    dependants: pda.dependants === undefined ? "" : pda.dependants,
    lastStepIndex: pda.lastStepIndex === undefined ? 0 : pda.lastStepIndex,
    isCompleted: pda.isCompleted === undefined ? false : pda.isCompleted,
    type: pda.type === undefined ? "pda" : pda.type,
    lastUpd: pda.lastUpd === undefined ? 0 : pda.lastUpd,
    agentCode: pda.agentCode === undefined ? "" : pda.agentCode,
    compCode: pda.compCode === undefined ? "" : pda.compCode,
    dealerGroup: pda.dealerGroup === undefined ? "AGENCY" : pda.dealerGroup,
    ownerConsentMethod: pda.ownerConsentMethod === undefined ? undefined : pda.ownerConsentMethod,
    applicantHasChanged: pda.applicantHasChanged === undefined ? false : pda.applicantHasChanged,
    consentNoticesHasChanged: pda.consentNoticesHasChanged === undefined ? false : pda.consentNoticesHasChanged
  };
};

var naMapForm = exports.naMapForm = function naMapForm(na) {
  return {
    agentCode: na.agentCode === undefined ? "" : na.agentCode,
    aspects: na.aspects === undefined ? "" : na.aspects,
    ciProtection: na.ciProtection === undefined ? {} : na.ciProtection,
    ckaSection: {
      isValid: na.ckaSection && na.ckaSection.isValid !== undefined ? na.ckaSection.isValid : false,
      owner: {
        cid: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.cid !== undefined ? na.ckaSection.owner.cid : "",
        ckaTitle: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.ckaTitle !== undefined ? na.ckaSection.owner.ckaTitle : null,
        init: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.init !== undefined ? na.ckaSection.owner.init : true,
        isActive: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.isActive !== undefined ? na.ckaSection.owner.isActive : false,
        passCka: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.passCka !== undefined ? na.ckaSection.owner.passCka : "",
        course: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.course !== undefined ? na.ckaSection.owner.course : "",
        institution: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.institution !== undefined ? na.ckaSection.owner.institution : "",
        studyPeriodEndYear: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.studyPeriodEndYear !== undefined ? na.ckaSection.owner.studyPeriodEndYear : "",
        collectiveInvestment: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.collectiveInvestment !== undefined ? na.ckaSection.owner.collectiveInvestment : "",
        transactionType: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.transactionType !== undefined ? na.ckaSection.owner.transactionType : "",
        insuranceInvestment: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.insuranceInvestment !== undefined ? na.ckaSection.owner.insuranceInvestment : "",
        insuranceType: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.insuranceType !== undefined ? na.ckaSection.owner.insuranceType : "",
        profession: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.profession !== undefined ? na.ckaSection.owner.profession : "",
        years: na.ckaSection && na.ckaSection.owner && na.ckaSection.owner.years !== undefined ? na.ckaSection.owner.years : ""
      }
    },
    compCode: na.compCode === undefined ? "" : na.compCode,
    completed: na.completed === undefined ? true : na.completed,
    completedStep: na.lastStepIndex === undefined ? -1 : na.lastStepIndex,
    dealerGroup: na.dealerGroup === undefined ? "" : na.dealerGroup,
    diProtection: na.diProtection === undefined ? {} : na.diProtection,
    ePlanning: na.ePlanning === undefined ? {} : na.ePlanning,
    fiProtection: na.fiProtection === undefined ? {} : na.fiProtection,
    haveFnaCompleted: na.haveFnaCompleted === undefined ? false : na.haveFnaCompleted,
    hcProtection: na.hcProtection === undefined ? {} : na.hcProtection,
    iarRate: na.iarRate === undefined ? "" : na.iarRate,
    iarRateNotMatchReason: na.iarRateNotMatchReason === undefined ? "" : na.iarRateNotMatchReason,
    iarRateNote: na.iarRateNote === undefined ? null : na.iarRateNote,
    id: na.id === undefined ? "" : na.id,
    isCompleted: na.isCompleted === undefined ? false : na.isCompleted,
    lastStepIndex: na.lastStepIndex === undefined ? 0 : na.lastStepIndex,
    lastUpd: na.lastUpd === undefined ? 0 : na.lastUpd,
    needsImgDesc: na.needsImgDesc === undefined ? null : na.needsImgDesc,
    other: na.other === undefined ? {} : na.other,
    paProtection: na.paProtection === undefined ? {} : na.paProtection,
    pcHeadstart: na.pcHeadstart === undefined ? {} : na.pcHeadstart,
    productType: {
      prodType: na.productType && na.productType.prodType !== undefined ? na.productType.prodType : "",
      lastProdType: na.productType && na.productType.lastProdType !== undefined ? na.productType.lastProdType : ""
    },
    preNeedsProductsList: na.aspects === undefined ? "" : na.aspects,
    psGoals: na.psGoals === undefined ? {} : na.psGoals,
    rPlanning: na.rPlanning === undefined ? {} : na.rPlanning,
    raSection: {
      isValid: na.raSection && na.raSection.isValid !== undefined ? na.raSection.isValid : false,
      owner: {
        assessedRL: na.raSection && na.raSection.owner && na.raSection.owner.assessedRL !== undefined ? na.raSection.owner.assessedRL : 0,
        cid: na.raSection && na.raSection.owner && na.raSection.owner.cid !== undefined ? na.raSection.owner.cid : "",
        disagreeRP2: na.raSection && na.raSection.owner && na.raSection.owner.disagreeRP2 !== undefined ? na.raSection.owner.disagreeRP2 : "",
        init: na.raSection && na.raSection.owner && na.raSection.owner.init !== undefined ? na.raSection.owner.init : false,
        isActive: na.raSection && na.raSection.owner && na.raSection.owner.isActive !== undefined ? na.raSection.owner.isActive : false,
        passStatement: na.raSection && na.raSection.owner && na.raSection.owner.passStatement !== undefined ? na.raSection.owner.passStatement : "",
        selfRLReasonRP: na.raSection && na.raSection.owner && na.raSection.owner.selfRLReasonRP !== undefined ? na.raSection.owner.selfRLReasonRP : "",
        selfSelectedriskLevel: na.raSection && na.raSection.owner && na.raSection.owner.selfSelectedriskLevel !== undefined ? na.raSection.owner.selfSelectedriskLevel : "",
        riskPotentialReturn: na.raSection && na.raSection.owner && na.raSection.owner.riskPotentialReturn !== undefined ? na.raSection.owner.riskPotentialReturn : null,
        avgAGReturn: na.raSection && na.raSection.owner && na.raSection.owner.avgAGReturn !== undefined ? na.raSection.owner.avgAGReturn : null,
        smDroped: na.raSection && na.raSection.owner && na.raSection.owner.smDroped !== undefined ? na.raSection.owner.smDroped : null,
        alofLosses: na.raSection && na.raSection.owner && na.raSection.owner.alofLosses !== undefined ? na.raSection.owner.alofLosses : null,
        expInvTime: na.raSection && na.raSection.owner && na.raSection.owner.expInvTime !== undefined ? na.raSection.owner.expInvTime : null,
        invPref: na.raSection && na.raSection.owner && na.raSection.owner.invPref !== undefined ? na.raSection.owner.invPref : null
      }
    },
    selectedMenuId: na.selectedMenuId === undefined ? "" : na.selectedMenuId,
    sfAspects: na.sfAspects === undefined ? [] : na.sfAspects,
    sfFailNotes: na.sfFailNotes === undefined ? null : na.sfFailNotes,
    spAspects: na.spAspects === undefined ? [] : na.spAspects,
    spNotes: na.spNotes === undefined ? null : na.spNotes,
    type: na.type === undefined ? "na" : na.type,
    undefined: na.undefined === undefined ? null : na.undefined
  };
};

var formMapTrustedIndividuel = exports.formMapTrustedIndividuel = function formMapTrustedIndividuel(clientForm) {
  return {
    firstName: clientForm.givenName,
    fullName: clientForm.name,
    idCardNo: clientForm.ID,
    idDocType: clientForm.IDDocumentType,
    idDocTypeOther: clientForm.IDDocumentTypeOther,
    lastName: clientForm.surname,
    mobileCountryCode: clientForm.prefixA,
    mobileNo: clientForm.mobileNoA,
    nameOrder: clientForm.nameOrder,
    relationship: clientForm.relationship,
    relationshipOther: clientForm.relationshipOther,
    tiPhoto: ""
  };
};