"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.analysisAspect = undefined;
exports.calIarRate2 = calIarRate2;
exports.calTotCoverage = calTotCoverage;
exports.getTimeHorizon = getTimeHorizon;
exports.calTotShortfall = calTotShortfall;
exports.calAsset = calAsset;
exports.calEstToFutureCost = calEstToFutureCost;
exports.calTotRequired = calTotRequired;
exports.calExitLifeIns = calExitLifeIns;
exports.removeCash = removeCash;
exports.calFv = calFv;
exports.calCompPv = calCompPv;
exports.calCompFv = calCompFv;
exports.calFirstYrPmt = calFirstYrPmt;
exports.calRetireDurtion = calRetireDurtion;
exports.retireAgeOnChange = retireAgeOnChange;
exports.inReqRetirementOnChange = inReqRetirementOnChange;
exports.othRegIncomeOnChange = othRegIncomeOnChange;
exports.calLumpSum = calLumpSum;
exports.ciProtectionPmtOnChange = ciProtectionPmtOnChange;
exports.ciProtectionRequireYrIncomeOnChange = ciProtectionRequireYrIncomeOnChange;
exports.ciProtectionMtCostOnChange = ciProtectionMtCostOnChange;
exports.fiProtectionPmtOnChange = fiProtectionPmtOnChange;
exports.fiProtectionRequireYrIncomeOnChange = fiProtectionRequireYrIncomeOnChange;
exports.fiProtectionFinExpensesOnChange = fiProtectionFinExpensesOnChange;
exports.fiProtectionOthFundNeedsOnChange = fiProtectionOthFundNeedsOnChange;
exports.fiProtectionAssetsOnChange = fiProtectionAssetsOnChange;
exports.diProtectionPmtOnChange = diProtectionPmtOnChange;
exports.diProtectionOthRegIncomeOnChange = diProtectionOthRegIncomeOnChange;
exports.diProtectionRequireYrIncomeOnChange = diProtectionRequireYrIncomeOnChange;
exports.paProtectionPmtOnChange = paProtectionPmtOnChange;
exports.sumAssuredCriticalOnChange = sumAssuredCriticalOnChange;
exports.sumAssuredProvidedOnChange = sumAssuredProvidedOnChange;
exports.otherNeedsGoalsOnChange = otherNeedsGoalsOnChange;
exports.psGoalsGoalsOnChange = psGoalsGoalsOnChange;
exports.ePlanningYrtoSupportOnChange = ePlanningYrtoSupportOnChange;
exports.ePlanningCostofEduOnChange = ePlanningCostofEduOnChange;
exports.calOtherAsset = calOtherAsset;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _NEEDS = require("../constants/NEEDS");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// function getProfile(validRefValues = {}, values = {}, type) {
//   const { profile = {}, dependantProfiles = {} } = validRefValues;
//   const { cid } = values;
//   return ["dependants", "spouse"].indexOf(type) > -1
//     ? dependantProfiles[cid]
//     : profile;
// }

function calIarRate2(iarRate) {
  return Number(Number(iarRate || 0).toFixed(2)) || 0;
}

function calTotCoverage() {
  var mtCost = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var lumpSum = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  return mtCost + lumpSum;
}

function getTimeHorizon(values, dependantProfileData, id) {
  var timeHorizon = void 0;
  if (id === "ePlanning") {
    var _values$yrtoSupport = values.yrtoSupport,
        yrtoSupport = _values$yrtoSupport === undefined ? 0 : _values$yrtoSupport,
        _values$curAgeofChild = values.curAgeofChild,
        curAgeofChild = _values$curAgeofChild === undefined ? 0 : _values$curAgeofChild;

    timeHorizon = yrtoSupport - curAgeofChild < 0 ? 0 : yrtoSupport - curAgeofChild;
  } else {
    timeHorizon = (values.retireAge || dependantProfileData.nearAge || 0) - dependantProfileData.nearAge || 0;
  }
  return timeHorizon;
}

function calTotShortfall(aspect, values) {
  var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";

  switch (aspect) {
    case _NEEDS.PAPROTECTION:
      return Number(-values.pmt || 0) + Number(values.extInsurance || 0);
    case _NEEDS.CIPROTECTION:
      {
        var assetsVal = _.sumBy(values.assets || [], function (asset) {
          return asset.usedAsset;
        });
        return Number(-values.totCoverage || 0) + Number(values.ciProt || 0) + Number(assetsVal || 0);
      }
    case _NEEDS.EPLANNING:
      {
        var _values$assets = values.assets,
            assets = _values$assets === undefined ? [] : _values$assets,
            _values$timeHorizon = values.timeHorizon,
            timeHorizon = _values$timeHorizon === undefined ? 0 : _values$timeHorizon,
            _values$maturityValue = values.maturityValue,
            maturityValue = _values$maturityValue === undefined ? 0 : _values$maturityValue,
            _values$estTotFutureC = values.estTotFutureCost,
            estTotFutureCost = _values$estTotFutureC === undefined ? 0 : _values$estTotFutureC;

        var _assetsVal = _.sumBy(assets, function (asset) {
          return Number(asset.usedAsset) * Math.pow(1 + Number(asset.return) / 100, timeHorizon);
        });
        return Number(-estTotFutureCost || 0) + Number(_assetsVal || 0) + Number(maturityValue || 0);
      }
    case _NEEDS.FIPROTECTION:
      {
        var _assetsVal2 = _.sumBy(values.assets || [], function (asset) {
          return Number(asset.usedAsset);
        });
        return Number(-values.totRequired || 0) + Number(values.existLifeIns || 0) + Number(_assetsVal2 || 0);
      }
    case _NEEDS.DIPROTECTION:
      return Number(-values.annualPmt || 0) + Number(values.disabilityBenefit || 0) + Number(values.othRegIncome || 0);
    case _NEEDS.OTHER:
      {
        switch (type) {
          case "goal":
            {
              var _values$needsValue = values.needsValue,
                  needsValue = _values$needsValue === undefined ? 0 : _values$needsValue,
                  _values$extInsuDispla = values.extInsuDisplay,
                  extInsuDisplay = _values$extInsuDispla === undefined ? 0 : _values$extInsuDispla;

              return Number(extInsuDisplay || 0) - Number(needsValue || 0);
            }
          default:
            return values.goalNo === 1 ? values.goals[0].totShortfall : "NotShow";
        }
      }
    case _NEEDS.PSGOALS:
      {
        switch (type) {
          case "goal":
            {
              var _values$assets2 = values.assets,
                  _assets = _values$assets2 === undefined ? [] : _values$assets2,
                  _values$timeHorizon2 = values.timeHorizon,
                  _timeHorizon = _values$timeHorizon2 === undefined ? 0 : _values$timeHorizon2,
                  _values$projMaturity = values.projMaturity,
                  projMaturity = _values$projMaturity === undefined ? 0 : _values$projMaturity,
                  _values$compFv = values.compFv,
                  compFv = _values$compFv === undefined ? 0 : _values$compFv;

              var _assetsVal3 = _.sumBy(_assets, function (asset) {
                return Number(asset.usedAsset) * Math.pow(1 + Number(asset.return) / 100, _timeHorizon);
              });

              return Number(-compFv || 0) + Number(_assetsVal3 || 0) + Number(projMaturity || 0);
            }
          default:
            return values.goalNo === 1 ? values.goals[0].totShortfall : "NotShow";
        }
      }
    case _NEEDS.PCHEADSTART:
      {
        var totShortfallCI = Number(-values.sumAssuredCritical || 0) + Number(values.ciExtInsurance || 0);
        var totShortfallP = Number(-values.sumAssuredProvided || 0) + Number(values.extInsurance || 0);
        return { totShortfallCI: totShortfallCI, totShortfallP: totShortfallP };
      }
    case _NEEDS.RPLANNING:
      {
        var _values$assets3 = values.assets,
            _assets2 = _values$assets3 === undefined ? [] : _values$assets3,
            _values$timeHorizon3 = values.timeHorizon,
            _timeHorizon2 = _values$timeHorizon3 === undefined ? 0 : _values$timeHorizon3,
            _values$maturityValue2 = values.maturityValue,
            _maturityValue = _values$maturityValue2 === undefined ? 0 : _values$maturityValue2,
            _values$compPv = values.compPv,
            compPv = _values$compPv === undefined ? 0 : _values$compPv;

        var _assetsVal4 = _.sumBy(_assets2, function (asset) {
          return Number(asset.usedAsset) * Math.pow(1 + Number(asset.return) / 100, _timeHorizon2);
        });
        return Number(-compPv || 0) + Number(_assetsVal4 || 0) + Number(_maturityValue || 0);
      }
    default:
      return 0;
  }
}

function calAsset(assetValue, calRate, timeHorizon) {
  if (calRate && calRate !== 0 && timeHorizon && timeHorizon !== 0) {
    return assetValue * Math.pow(1 + calRate / 100, timeHorizon);
  }
  return assetValue;
}

function calEstToFutureCost(values) {
  var _values$avgEduInflatR = values.avgEduInflatRate,
      avgEduInflatRate = _values$avgEduInflatR === undefined ? 0 : _values$avgEduInflatR,
      _values$timeHorizon4 = values.timeHorizon,
      timeHorizon = _values$timeHorizon4 === undefined ? 0 : _values$timeHorizon4,
      _values$costofEdu = values.costofEdu,
      costofEdu = _values$costofEdu === undefined ? 0 : _values$costofEdu;

  return costofEdu * Math.pow(1 + avgEduInflatRate / 100, timeHorizon);
}

function calTotRequired(_ref) {
  var _ref$finExpenses = _ref.finExpenses,
      finExpenses = _ref$finExpenses === undefined ? 0 : _ref$finExpenses,
      _ref$totLiabilities = _ref.totLiabilities,
      totLiabilities = _ref$totLiabilities === undefined ? 0 : _ref$totLiabilities,
      _ref$lumpSum = _ref.lumpSum,
      lumpSum = _ref$lumpSum === undefined ? 0 : _ref$lumpSum,
      _ref$othFundNeeds = _ref.othFundNeeds,
      othFundNeeds = _ref$othFundNeeds === undefined ? 0 : _ref$othFundNeeds;

  return Number(finExpenses) + Number(totLiabilities) + Number(lumpSum) + Number(othFundNeeds);
}

function calExitLifeIns() {
  var lifeInsProt = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var invLinkPol = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  return Number(lifeInsProt) + Number(invLinkPol);
}

function removeCash(data) {
  if (!data.value || typeof data.value !== "string") {
    return data;
  }
  if (data.value.indexOf("$") > -1) {
    data.value = data.value.replace("$", "");
  }
  return data;
}

function calFv(fvRate, fvNper, fvPmt, fvPv) {
  if (fvRate === 0) {
    return -fvPv - fvPmt * fvNper || 0;
  }
  return Math.round(((1 - Math.pow(1 + fvRate, fvNper)) / fvRate * fvPmt * (1 + fvRate * 1) / Math.pow(1 + fvRate, fvNper) || 0) * 10000) / 10000;
}

// Retirement Planning

function calCompPv() {
  var iarRate2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var retireDuration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var firstYrPMT = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

  return calFv(iarRate2 / 100, retireDuration, -firstYrPMT, 0);
}

function calCompFv(avgInflatRate, annualInReqRetirement, timeHorizon) {
  return annualInReqRetirement * Math.pow(1 + avgInflatRate / 100, timeHorizon);
}

function calFirstYrPmt(values) {
  return (values.compFv || 0) - (values.othRegIncome || 0);
}

function calRetireDurtion(values, dependantProfileData) {
  var avgAge = dependantProfileData.gender === "M" ? 80 : 85;
  return avgAge - (values.retireAge || 0);
}

function retireAgeOnChange(data) {
  var profile = data.profile,
      analysisData = data.analysisData;

  if (profile && analysisData) {
    var retireAge = data.value || _NEEDS.RETIREAGE;
    var timeHorizon = (retireAge || profile.nearAge || 0) - (profile.nearAge || 0);
    var firstYrPMT = calFirstYrPmt(analysisData);
    var compFv = calCompFv(analysisData.avgInflatRate || _NEEDS.AVGINFLATRATE, analysisData.annualInReqRetirement || 0, timeHorizon);
    var retireDuration = (profile.gender === "M" ? 80 : 85) - retireAge;
    var compPv = calCompPv(analysisData.iarRate2, retireDuration, firstYrPMT);
    return { retireAge: retireAge, timeHorizon: timeHorizon, compFv: compFv, retireDuration: retireDuration, compPv: compPv };
  }
  return {};
}

function inReqRetirementOnChange(data) {
  var profile = data.profile,
      analysisData = data.analysisData;


  if (analysisData) {
    var inReqRetirement = removeCash(data.value);
    var annualInReqRetirement = inReqRetirement * 12;
    var compFv = calCompFv(analysisData.avgInflatRate || _NEEDS.AVGINFLATRATE, annualInReqRetirement, analysisData.timeHorizon);

    var firstYrPMT = calFirstYrPmt(analysisData);
    var iarRate2 = calIarRate2(data.iarRate);
    var retireDuration = (profile.gender === "M" ? 80 : 85) - (data.retireAge || _NEEDS.RETIREAGE);
    var compPv = calCompPv(iarRate2, retireDuration, firstYrPMT);

    return {
      inReqRetirement: inReqRetirement,
      annualInReqRetirement: annualInReqRetirement,
      compFv: compFv,
      compPv: compPv,
      firstYrPMT: firstYrPMT,
      iarRate2: iarRate2
    };
  }
  return {};
}

function othRegIncomeOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var othRegIncome = data.value;
    var firstYrPMT = (analysisData.compFv || 0) - (othRegIncome || 0);
    var compPv = calCompPv(analysisData.iarRate2, analysisData.retireDuration, firstYrPMT);
    return { othRegIncome: othRegIncome, firstYrPMT: firstYrPMT, compPv: compPv };
  }
  return {};
}

// Critical Illness Protection

function calLumpSum() {
  var iarRate2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var requireYrIncome = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var pmt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

  return calFv(Number(iarRate2) / 100, Number(requireYrIncome), Number(-pmt) * 12, 0);
}

function calCiProtectionTotCoverage(lumpSum, mtCost) {
  return Number(lumpSum || 0) + Number(mtCost || 0);
}

function ciProtectionPmtOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var pmt = removeCash(data.value);
    var annualLivingExp = Number(pmt || 0) * 12;
    var mtCost = analysisData.mtCost,
        iarRate = analysisData.iarRate,
        requireYrIncome = analysisData.requireYrIncome;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { pmt: pmt, annualLivingExp: annualLivingExp, lumpSum: lumpSum, totCoverage: totCoverage };
  }
  return {};
}

function ciProtectionRequireYrIncomeOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var requireYrIncome = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        mtCost = analysisData.mtCost;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { requireYrIncome: requireYrIncome, lumpSum: lumpSum, totCoverage: totCoverage };
  }
  return {};
}

function ciProtectionMtCostOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var mtCost = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        requireYrIncome = analysisData.requireYrIncome;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { mtCost: mtCost, totCoverage: totCoverage };
  }
  return {};
}

// Income Protection (fiProtection)
function fiProtectionPmtOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var iarRate = analysisData.iarRate,
        requireYrIncome = analysisData.requireYrIncome,
        finExpenses = analysisData.finExpenses,
        othFundNeeds = analysisData.othFundNeeds,
        assets = analysisData.assets,
        feProfile = analysisData.feProfile;


    var pmt = removeCash(data.value);
    var annualRepIncome = Number(pmt || 0) * 12;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var existLifeIns = calExitLifeIns(feProfile.lifeInsProt, feProfile.invLinkPol);
    var totRequired = calTotRequired({
      finExpenses: finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum: lumpSum,
      othFundNeeds: othFundNeeds
    });
    var totShortfall = calTotShortfall(_NEEDS.FIPROTECTION, {
      assets: assets,
      totRequired: totRequired,
      existLifeIns: existLifeIns
    });

    return {
      pmt: pmt,
      annualRepIncome: annualRepIncome,
      lumpSum: lumpSum,
      iarRate2: iarRate2,
      totRequired: totRequired,
      existLifeIns: existLifeIns,
      totShortfall: totShortfall
    };
  }
  return {};
}

function fiProtectionRequireYrIncomeOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var requireYrIncome = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        finExpenses = analysisData.finExpenses,
        othFundNeeds = analysisData.othFundNeeds,
        assets = analysisData.assets,
        feProfile = analysisData.feProfile;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var existLifeIns = calExitLifeIns(feProfile.lifeInsProt, feProfile.invLinkPol);
    var totRequired = calTotRequired({
      finExpenses: finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum: lumpSum,
      othFundNeeds: othFundNeeds
    });
    var totShortfall = calTotShortfall(_NEEDS.FIPROTECTION, {
      assets: assets,
      totRequired: totRequired,
      existLifeIns: existLifeIns
    });

    return {
      requireYrIncome: requireYrIncome,
      lumpSum: lumpSum,
      iarRate2: iarRate2,
      totRequired: totRequired,
      existLifeIns: existLifeIns,
      totShortfall: totShortfall
    };
  }
  return {};
}

function fiProtectionFinExpensesOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var finExpenses = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        requireYrIncome = analysisData.requireYrIncome,
        othFundNeeds = analysisData.othFundNeeds,
        assets = analysisData.assets,
        feProfile = analysisData.feProfile;


    var annualRepIncome = Number(pmt || 0) * 12;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var existLifeIns = calExitLifeIns(feProfile.lifeInsProt, feProfile.invLinkPol);
    var totRequired = calTotRequired({
      finExpenses: finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum: lumpSum,
      othFundNeeds: othFundNeeds
    });
    var totShortfall = calTotShortfall(_NEEDS.FIPROTECTION, {
      assets: assets,
      totRequired: totRequired,
      existLifeIns: existLifeIns
    });

    return {
      finExpenses: finExpenses,
      annualRepIncome: annualRepIncome,
      lumpSum: lumpSum,
      iarRate2: iarRate2,
      totRequired: totRequired,
      existLifeIns: existLifeIns,
      totShortfall: totShortfall
    };
  }
  return {};
}

function fiProtectionOthFundNeedsOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var othFundNeeds = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        finExpenses = analysisData.finExpenses,
        requireYrIncome = analysisData.requireYrIncome,
        assets = analysisData.assets,
        feProfile = analysisData.feProfile;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var existLifeIns = calExitLifeIns(feProfile.lifeInsProt, feProfile.invLinkPol);
    var totRequired = calTotRequired({
      finExpenses: finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum: lumpSum,
      othFundNeeds: othFundNeeds
    });
    var totShortfall = calTotShortfall(_NEEDS.FIPROTECTION, {
      assets: assets,
      totRequired: totRequired,
      existLifeIns: existLifeIns
    });

    return { othFundNeeds: othFundNeeds, totRequired: totRequired, existLifeIns: existLifeIns, totShortfall: totShortfall };
  }
  return {};
}

function fiProtectionAssetsOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        requireYrIncome = analysisData.requireYrIncome,
        finExpenses = analysisData.finExpenses,
        othFundNeeds = analysisData.othFundNeeds,
        assets = analysisData.assets,
        feProfile = analysisData.feProfile;


    var annualRepIncome = Number(pmt || 0) * 12;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var existLifeIns = calExitLifeIns(feProfile.lifeInsProt, feProfile.invLinkPol);
    var totRequired = calTotRequired({
      finExpenses: finExpenses,
      totLiabilities: feProfile.totLiabilities,
      lumpSum: lumpSum,
      othFundNeeds: othFundNeeds
    });
    var totShortfall = calTotShortfall(_NEEDS.FIPROTECTION, {
      assets: assets,
      totRequired: totRequired,
      existLifeIns: existLifeIns
    });

    return {
      pmt: pmt,
      annualRepIncome: annualRepIncome,
      lumpSum: lumpSum,
      iarRate2: iarRate2,
      totRequired: totRequired,
      existLifeIns: existLifeIns,
      totShortfall: totShortfall,
      assets: assets
    };
  }
  return {};
}

// Disability Benefit
function diProtectionPmtOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var pmt = removeCash(data.value);
    var annualPmt = Number(pmt || 0) * 12;
    var disabilityBenefit = analysisData.disabilityBenefit,
        othRegIncome = analysisData.othRegIncome;

    var totShortfall = calTotShortfall(_NEEDS.DIPROTECTION, {
      annualPmt: annualPmt,
      disabilityBenefit: disabilityBenefit,
      othRegIncome: othRegIncome
    });
    return { pmt: pmt, annualPmt: annualPmt, totShortfall: totShortfall };
  }
  return {};
}

function diProtectionOthRegIncomeOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var othRegIncome = Number(data.value || 0);
    var annualPmt = analysisData.annualPmt,
        disabilityBenefit = analysisData.disabilityBenefit;

    var totShortfall = calTotShortfall(_NEEDS.DIPROTECTION, {
      annualPmt: annualPmt,
      disabilityBenefit: disabilityBenefit,
      othRegIncome: othRegIncome
    });
    return { othRegIncome: othRegIncome, totShortfall: totShortfall };
  }
  return {};
}

function diProtectionRequireYrIncomeOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var requireYrIncome = Number(data.value || 0);
    var pmt = analysisData.pmt,
        iarRate = analysisData.iarRate,
        mtCost = analysisData.mtCost;

    var iarRate2 = calIarRate2(iarRate);
    var lumpSum = calLumpSum(iarRate2, requireYrIncome, pmt);
    var totCoverage = calCiProtectionTotCoverage(lumpSum, mtCost);
    return { requireYrIncome: requireYrIncome, lumpSum: lumpSum, totCoverage: totCoverage };
  }
  return {};
}

// Personal Accident Protection
function paProtectionPmtOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var pmt = Number(data.value || 0);
    var extInsurance = analysisData.extInsurance;

    var totShortfall = calTotShortfall(_NEEDS.PAPROTECTION, { pmt: pmt, extInsurance: extInsurance });
    return { pmt: pmt, totShortfall: totShortfall };
  }
  return {};
}

// Personal Children's headStart - CriticalIlliness
function sumAssuredCriticalOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var sumAssuredCritical = Number(data.value || 0);
    var ciExtInsurance = analysisData.ciExtInsurance;

    var _calTotShortfall = calTotShortfall(_NEEDS.PCHEADSTART, {
      sumAssuredCritical: sumAssuredCritical,
      ciExtInsurance: ciExtInsurance
    }),
        totShortfallCI = _calTotShortfall.totShortfallCI;

    return { sumAssuredCritical: sumAssuredCritical, totShortfallCI: totShortfallCI };
  }
  return {};
}

// Personal Children's headStart - ProtectionProvided
function sumAssuredProvidedOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var sumAssuredProvided = Number(data.value || 0);
    var extInsurance = analysisData.extInsurance;

    var _calTotShortfall2 = calTotShortfall(_NEEDS.PCHEADSTART, {
      sumAssuredProvided: sumAssuredProvided,
      extInsurance: extInsurance
    }),
        totShortfallP = _calTotShortfall2.totShortfallP;

    return { sumAssuredProvided: sumAssuredProvided, totShortfallP: totShortfallP };
  }
  return {};
}

// Other Needs
function otherNeedsGoalsOnChange(data) {
  var goals = data.value;
  var updatedGoals = calTotShortfall(_NEEDS.OTHER, { goals: goals });
  return { goals: updatedGoals };
}

// Specific Goals
function psGoalsGoalsOnChange(data) {
  var goals = data.value;
  var updatedGoals = calTotShortfall(_NEEDS.PSGOALS, { goals: goals });
  return { goals: updatedGoals };
}

// Education Planning
function ePlanningYrtoSupportOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var yrtoSupport = Number(data.value || 0);
    var curAgeofChild = analysisData.curAgeofChild;

    var timeHorizon = yrtoSupport - curAgeofChild < 0 ? 0 : yrtoSupport - curAgeofChild;
    return { yrtoSupport: yrtoSupport, curAgeofChild: curAgeofChild, timeHorizon: timeHorizon };
  }
  return {};
}

function ePlanningCostofEduOnChange(data) {
  var analysisData = data.analysisData;

  if (analysisData) {
    var costofEdu = Number(data.value || 0);
    var _analysisData$avgEduI = analysisData.avgEduInflatRate,
        avgEduInflatRate = _analysisData$avgEduI === undefined ? 5 : _analysisData$avgEduI,
        timeHorizon = analysisData.timeHorizon,
        assets = analysisData.assets,
        maturityValue = analysisData.maturityValue;

    var estTotFutureCost = calEstToFutureCost({
      avgEduInflatRate: avgEduInflatRate,
      timeHorizon: timeHorizon,
      costofEdu: costofEdu
    });
    var totShortfall = calTotShortfall({
      assets: assets,
      timeHorizon: timeHorizon,
      maturityValue: maturityValue,
      estTotFutureCost: estTotFutureCost
    });
    return { costofEdu: costofEdu, estTotFutureCost: estTotFutureCost, totShortfall: totShortfall };
  }
  return {};
}

var analysisAspect = exports.analysisAspect = {
  needs: [{
    title: "Income protection",
    value: "fiProtection",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON"],
    rule: {
      pmt: {
        type: "int",
        mandatory: true,
        min: 1
      },
      unMatchPmtReason: {
        type: "string",
        mandatory: false
      },
      assets: {
        type: "assets",
        mandatory: true,
        min: 1,
        max: "{max}"
      }
    }
  }, {
    title: "Critical Illness protection",
    value: "ciProtection",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
    rule: {
      mtCost: {
        type: "int",
        min: 0
      },
      assets: {
        type: "assets",
        mandatory: true,
        min: 1,
        max: "{max}"
      },
      requireYrIncome: {
        mandatory: true,
        numberAllowZero: true
      },
      pmt: {
        type: "int",
        mandatory: true,
        min: 1,
        max: "{max}"
      }
    }
  }, {
    title: "Disability benefit",
    value: "diProtection",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON", "DAU"],
    rule: {
      pmt: {
        type: "int",
        mandatory: true,
        min: 1
      },
      requireYrIncome: {
        mandatory: true,
        numberAllowZero: true
      }
    }
  }, {
    title: "Personal accident protection",
    value: "paProtection",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON", "DAU"],
    rule: {
      pmt: {
        type: "int",
        mandatory: true,
        min: 50000
      }
    }
  }, {
    title: "Planning for children's headstart",
    value: "pcHeadstart",
    relationshipRequired: ["SON", "DAU"],
    rule: {
      providedFor: {
        type: "string",
        mandatory: true
      },
      sumAssuredProvided: {
        type: "int",
        mandatory: true,
        min: 1
      },
      sumAssuredCritical: {
        type: "int",
        mandatory: true,
        min: 1
      }
    }
  }, {
    title: "Hospitalisation cost protection",
    value: "hcProtection",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO", "SON", "DAU", "FAT", "MOT", "GFA", "GMO"],
    rule: {
      provideHeadstart: {
        type: "string",
        mandatory: true
      },
      typeofWard: {
        type: "string",
        mandatory: true
      }
    }
  }, {
    title: "Retirement planning",
    value: "rPlanning",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
    rule: {
      inReqRetirement: {
        type: "int",
        mandatory: true,
        min: 1,
        max: 9999999999
      },
      unMatchPmtReason: {
        type: "string",
        mandatory: false
      }
    }
  }, {
    title: "Education Planning",
    value: "ePlanning",
    relationshipRequired: ["SON", "DAU"],
    rule: {
      yrtoSupport: {
        type: "int",
        mandatory: true,
        min: "{min}"
      },
      costofEdu: {
        type: "int",
        mandatory: true,
        min: 1
      }
    }
  }, {
    title: "Planning for specific goals",
    value: "psGoals",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
    rule: {
      goalNo: {
        type: "int",
        mandatory: true
      }
    }
  }, {
    title: "Other needs (e.g. mortgage,\npregnancy, savings)",
    value: "other",
    relationshipRequired: ["OWNER", "SPOUSE", "SPO"],
    rule: {
      goalNo: {
        type: "int",
        mandatory: true
      }
    }
  }],
  assets: [{
    title: "Savings Account",
    value: "savAcc",
    needsRequired: ["fiProtection", "ciProtection", "rPlanning", "ePlanning", "psGoals"]
  }, {
    title: "Fixed Deposits",
    value: "fixDeposit",
    needsRequired: ["fiProtection", "ciProtection", "rPlanning", "ePlanning", "psGoals"]
  }, {
    title: "Investment(Mutual Fund/Direct investment)",
    value: "invest",
    needsRequired: ["fiProtection", "ciProtection", "rPlanning", "ePlanning", "psGoals"]
  }, {
    title: "CPF (OA)",
    value: "cpfOa",
    needsRequired: ["fiProtection", "rPlanning"]
  }, {
    title: "CPF (SA)",
    value: "cpfSa",
    needsRequired: ["fiProtection", "rPlanning"]
  }, {
    title: "CPF Medisave",
    value: "cpfMs",
    needsRequired: [""]
  }, {
    title: "SRS",
    value: "srs",
    needsRequired: ["fiProtection", "rPlanning"]
  }, {
    title: "Please select assets",
    value: "all",
    needsRequired: ["fiProtection", "ciProtection", "rPlanning", "ePlanning", "psGoals"]
  }]
};

function calOtherAsset(_ref2) {
  var key = _ref2.key,
      selectedProduct = _ref2.selectedProduct,
      selectedProfile = _ref2.selectedProfile,
      na = _ref2.na,
      goalIndex = _ref2.goalIndex;
  var assets = analysisAspect.assets;

  var needs = assets.find(function (ass) {
    return ass.value === key;
  }).needsRequired;
  var selectedAspects = na.aspects.split(",");
  var otherNeeds = needs.filter(function (item) {
    return item !== selectedProduct;
  }).filter(function (item) {
    return selectedAspects.indexOf(item) > -1;
  });

  var total = 0;
  otherNeeds.forEach(function (otherNeed) {
    if (key !== "all") {
      if (otherNeed === "psGoals" && na.psGoals && na.psGoals[selectedProfile] && na.psGoals[selectedProfile].goals) {
        na.psGoals[selectedProfile].goals.forEach(function (goal) {
          if (goal.assets) {
            goal.assets.forEach(function (asset) {
              if (asset.key === key) {
                total += asset.usedAsset;
              }
            });
          }
        });
      } else if (otherNeed === "ePlanning" && na.ePlanning && na.ePlanning.dependants) {
        na.ePlanning.dependants.forEach(function (depandant) {
          if (depandant.assets) {
            depandant.assets.forEach(function (asset) {
              if (asset.key === key) {
                total += asset.usedAsset;
              }
            });
          }
        });
      } else if (na[otherNeed] && na[otherNeed][selectedProfile] && na[otherNeed][selectedProfile].assets) {
        var otherProductAssets = na[otherNeed][selectedProfile].assets;
        otherProductAssets.forEach(function (asset) {
          if (asset.key === key) {
            total += asset.usedAsset;
          }
        });
      }
    }
  });

  /*
  Since PsGoals has to check its own other goals assets value,
  the above forEach excluded its own selectedProduct (PsGoals),
  so have to do it outside.
  */
  if (selectedProduct === "psGoals") {
    var goals = na.psGoals[selectedProfile].goals;

    goals.forEach(function (goal, i) {
      if (i !== goalIndex) {
        var goalAssets = goal.assets;
        if (goalAssets) {
          goalAssets.forEach(function (asset) {
            if (asset.key === key) {
              total += asset.usedAsset;
            }
          });
        }
      }
    });
  }

  return total;
}