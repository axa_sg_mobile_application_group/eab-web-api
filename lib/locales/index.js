"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LANGUAGE_TYPES = undefined;

var _version$LANGUAGE_TYP;

var _ENGLISH = require("./textStore/ENGLISH");

var _ENGLISH2 = _interopRequireDefault(_ENGLISH);

var _TRADITIONAL_CHINESE = require("./textStore/TRADITIONAL_CHINESE");

var _TRADITIONAL_CHINESE2 = _interopRequireDefault(_TRADITIONAL_CHINESE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LANGUAGE_TYPES = exports.LANGUAGE_TYPES = {
  TRADITIONAL_CHINESE: "TRADITIONAL_CHINESE",
  ENGLISH: "ENGLISH"
};

exports.default = (_version$LANGUAGE_TYP = {
  version: "0.0.0"
}, _defineProperty(_version$LANGUAGE_TYP, LANGUAGE_TYPES.TRADITIONAL_CHINESE, _TRADITIONAL_CHINESE2.default), _defineProperty(_version$LANGUAGE_TYP, LANGUAGE_TYPES.ENGLISH, _ENGLISH2.default), _version$LANGUAGE_TYP);