"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var AGENT = exports.AGENT = "agent";

var CLIENT_PROFILE = exports.CLIENT_PROFILE = "cust";
var BUNDLE = exports.BUNDLE = "bundle";
var FNA_PDA = exports.FNA_PDA = "pda";
var FNA_FE = exports.FNA_FE = "fe";
var FNA_NA = exports.FNA_NA = "na";
var QUOTATION = exports.QUOTATION = "quotation";
var APPLICATION_NORMAL = exports.APPLICATION_NORMAL = "application";
var APPLICATION_MASTER = exports.APPLICATION_MASTER = "masterApplication";

var PRODUCT = exports.PRODUCT = "product";
var FUND = exports.FUND = "fund";
var PDF_TEMPLATE = exports.PDF_TEMPLATE = "pdfTemplate";