"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var MIN_CURRENCY_VALUE = exports.MIN_CURRENCY_VALUE = 0;
var MAX_CURRENCY_VALUE = exports.MAX_CURRENCY_VALUE = 9999999999;
var ROP_TABLE_MAX_CURRENCY_VALUE = exports.ROP_TABLE_MAX_CURRENCY_VALUE = 99999999999;