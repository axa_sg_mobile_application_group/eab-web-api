"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _REDUCER_TYPES = require("./REDUCER_TYPES");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = _defineProperty({}, _REDUCER_TYPES.APPLICATION, {
  PERSONAL_DETAILS: "menu_person",
  RESIDENCY: "menu_residency",
  INSURABILITY: "menu_insure",
  PLAN_DETAILS: "menu_plan",
  DECLARATION: "menu_declaration"
});