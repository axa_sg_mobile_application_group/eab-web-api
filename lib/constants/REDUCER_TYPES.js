"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * although we save config constant in web-api, but the reducer source should
 * place in the front-end project. Because this reducer should be platform
 * specific
 *  */
var CONFIG = exports.CONFIG = "config";
var CLIENT = exports.CLIENT = "client";
var FNA = exports.FNA = "fna";
var FNA_CKA = exports.FNA_CKA = "fnaCKA";
var FNA_RA = exports.FNA_RA = "fnaRA";
var CLIENT_FORM = exports.CLIENT_FORM = "clientForm";
var PRE_APPLICATION = exports.PRE_APPLICATION = "preApplication";
var RECOMMENDATION = exports.RECOMMENDATION = "recommendation";
var QUOTATION = exports.QUOTATION = "quotation";
var PROPOSAL = exports.PROPOSAL = "proposal";
var PRODUCTS = exports.PRODUCTS = "products";
var OPTIONS_MAP = exports.OPTIONS_MAP = "optionsMap";
var AGENT = exports.AGENT = "agent";
var AGENTUX = exports.AGENTUX = "agentUX";
var COMMON = exports.COMMON = "common";
var APPLICATION = exports.APPLICATION = "application";
var SUPPORTING_DOCUMENT = exports.SUPPORTING_DOCUMENT = "supportingDocument";
var PAYMENT_AND_SUBMISSION = exports.PAYMENT_AND_SUBMISSION = "paymentAndSubmission";
var PAYMENT = exports.PAYMENT = "payment";
var SIGNATURE = exports.SIGNATURE = "signature";
var LOGIN = exports.LOGIN = "login";