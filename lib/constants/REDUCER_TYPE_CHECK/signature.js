"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.component = exports.signature = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var signature = exports.signature = {
  isRequired: _propTypes2.default.shape({
    success: _propTypes2.default.bool.isRequired,
    application: _propTypes2.default.shape({
      showCoverTab: _propTypes2.default.bool,
      isFaChannel: _propTypes2.default.bool,
      tabCount: _propTypes2.default.string,
      agentSignFields: _propTypes2.default.array,
      clientSignFields: _propTypes2.default.array,
      textFields: _propTypes2.default.array,
      attachments: _propTypes2.default.array.isRequired,
      isMandDocsAllUploaded: _propTypes2.default.bool.isRequired,
      appStep: _propTypes2.default.string
    }),
    signDoc: _propTypes2.default.bool,
    crossAge: _propTypes2.default.bool
  })
};

var component = exports.component = {
  isRequired: _propTypes2.default.shape({
    selectedPdf: _propTypes2.default.string
  })
};