"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.productList = exports.dependants = exports.insuredCid = exports.currency = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var currency = exports.currency = {
  isRequired: _propTypes2.default.string.isRequired
};

var insuredCid = exports.insuredCid = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.oneOf([null])])
};

var dependants = exports.dependants = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    cid: _propTypes2.default.string.isRequired,
    fullName: _propTypes2.default.string.isRequired
  })).isRequired
};

var productList = exports.productList = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    title: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired,
    seq: _propTypes2.default.number.isRequired,
    products: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      compCode: _propTypes2.default.string.isRequired,
      covCode: _propTypes2.default.string.isRequired,
      covName: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired,
      version: _propTypes2.default.number.isRequired
    })).isRequired
  })).isRequired
};