"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.save = exports.isLoading = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isLoading = exports.isLoading = {
  isRequired: _propTypes2.default.bool.isRequired
};

var save = exports.save = {};