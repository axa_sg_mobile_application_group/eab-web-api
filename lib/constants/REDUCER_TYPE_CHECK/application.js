"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.crossAge = exports.dialogValues = exports.rootValues = exports.template = exports.planDetails = exports.error = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _common = require("./common");

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var applicationFormErrorTypes = _propTypes2.default.objectOf(_propTypes2.default.objectOf(common.error));

var error = exports.error = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.objectOf(_propTypes2.default.shape({
    proposer: _propTypes2.default.objectOf(applicationFormErrorTypes),
    insured: _propTypes2.default.arrayOf(_propTypes2.default.objectOf(applicationFormErrorTypes))
  })))
};

var planDetails = exports.planDetails = {
  availableFunds: {
    isRequired: _propTypes2.default.oneOfType([_propTypes2.default.array])
  },
  applicationPlanDetailsData: {
    isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
  }
};

// TODO:
var template = exports.template = {
  isRequired: {}
};

// TODO:
var rootValues = exports.rootValues = {
  isRequired: {}
};

// TODO:
var dialogValues = exports.dialogValues = {};

var crossAge = exports.crossAge = _propTypes2.default.shape({
  crossedAge: _propTypes2.default.bool.isRequired,
  allowBackdate: _propTypes2.default.bool.isRequired,
  proposerStatus: _propTypes2.default.number.isRequired,
  insuredStatus: _propTypes2.default.number.isRequired,
  crossedAgeCid: _propTypes2.default.arrayOf(_propTypes2.default.string).isRequired,
  status: _propTypes2.default.number.isRequired
});