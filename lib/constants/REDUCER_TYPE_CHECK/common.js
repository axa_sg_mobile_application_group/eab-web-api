"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.error = undefined;

var _PropTypes$shape;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _locales = require("../../locales");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var error = exports.error = _propTypes2.default.shape({
  hasError: _propTypes2.default.bool.isRequired,
  message: _propTypes2.default.oneOfType([_propTypes2.default.shape((_PropTypes$shape = {}, _defineProperty(_PropTypes$shape, _locales.LANGUAGE_TYPES.ENGLISH, _propTypes2.default.string), _defineProperty(_PropTypes$shape, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _propTypes2.default.string), _PropTypes$shape)), _propTypes2.default.string]).isRequired
});

exports.default = {};