"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.proposalEmail = exports.canClone = exports.canRequote = exports.canEmail = exports.illustrateData = exports.proposal = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var proposal = exports.proposal = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    tabLabel: _propTypes2.default.string.isRequired,
    label: _propTypes2.default.string.isRequired,
    allowSave: _propTypes2.default.bool.isRequired,
    fileName: _propTypes2.default.string.isRequired,
    docId: _propTypes2.default.string,
    attachmentId: _propTypes2.default.string,
    data: _propTypes2.default.string
  })).isRequired
};

// TODO update data inside array
var illustrateData = exports.illustrateData = _propTypes2.default.objectOf(_propTypes2.default.arrayOf(_propTypes2.default.shape({})));

var canEmail = exports.canEmail = _propTypes2.default.bool;
var canRequote = exports.canRequote = _propTypes2.default.bool;
var canClone = exports.canClone = _propTypes2.default.bool;

var proposalEmail = exports.proposalEmail = {
  isRequired: _propTypes2.default.array.isRequired
};