"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nameOrder = exports.isFNA = exports.ra = exports.analysisContainerFunc = exports.analysisRule = exports.analysisData = exports.analysis = exports.na = exports.FNAReportEmail = exports.FNAReport = exports.feErrors = exports.fe = exports.dependantProfiles = exports.pda = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pda = exports.pda = {
  isRequired: _propTypes2.default.shape({
    agentCode: _propTypes2.default.string,
    applicant: _propTypes2.default.string,
    compCode: _propTypes2.default.string,
    completed: _propTypes2.default.bool,
    dealerGroup: _propTypes2.default.string,
    dependants: _propTypes2.default.string,
    id: _propTypes2.default.string,
    isCompleted: _propTypes2.default.bool,
    lastStepIndex: _propTypes2.default.number,
    lastUpd: _propTypes2.default.number,
    notes: _propTypes2.default.string,
    ownerConsentMethod: _propTypes2.default.string,
    trustedIndividual: _propTypes2.default.string,
    type: _propTypes2.default.number,
    undefined: _propTypes2.default.string
  })
};

var dependantProfiles = exports.dependantProfiles = {
  isRequired: _propTypes2.default.shape({
    agentCode: _propTypes2.default.string.isRequired,
    aspects: _propTypes2.default.string.isRequired,
    ciProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    // TODO
    ckaSection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    compCode: _propTypes2.default.string.isRequired,
    completed: _propTypes2.default.bool.isRequired,
    dealerGroup: _propTypes2.default.string.isRequired,
    diProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    ePlanning: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    fiProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    haveFnaCompleted: _propTypes2.default.bool.isRequired,
    hcProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    iarRate: _propTypes2.default.string,
    iarRateNote: _propTypes2.default.string,
    id: _propTypes2.default.string.isRequired,
    isCompleted: _propTypes2.default.bool.isRequired,
    lastStepIndex: _propTypes2.default.number.isRequired,
    lastUpd: _propTypes2.default.number,
    needsImgDesc: _propTypes2.default.string,
    other: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    paProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    pcHeadstart: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    productType: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    psGoals: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    rPlanning: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    raSection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    selectedMenuId: _propTypes2.default.string.isRequired,
    sfAspects: _propTypes2.default.array,
    sfFailNotes: _propTypes2.default.string,
    spAspects: _propTypes2.default.array,
    spNotes: _propTypes2.default.string,
    type: _propTypes2.default.string
  })
};

var fe = exports.fe = {
  isRequired: _propTypes2.default.shape({
    agentCode: _propTypes2.default.string,
    compCode: _propTypes2.default.string,
    completed: _propTypes2.default.bool,
    dealerGroup: _propTypes2.default.string,
    id: _propTypes2.default.string,
    isCompleted: _propTypes2.default.bool,
    lastUpd: _propTypes2.default.number,
    owner: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    spouse: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    dependants: _propTypes2.default.arrayOf([_propTypes2.default.object])
  })
};

var feErrors = exports.feErrors = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.objectOf(_propTypes2.default.shape({
    hasError: _propTypes2.default.bool.isRequired,
    message: _propTypes2.default.string.isRequired
  })).isRequired).isRequired
};

var FNAReport = exports.FNAReport = {
  isRequired: _propTypes2.default.string.isRequired
};

var FNAReportEmail = exports.FNAReportEmail = {
  isRequired: _propTypes2.default.array.isRequired
};

var na = exports.na = {
  isRequired: _propTypes2.default.shape({
    agentCode: _propTypes2.default.string,
    aspects: _propTypes2.default.string,
    ciProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    ckaSection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    compCode: _propTypes2.default.string,
    completed: _propTypes2.default.bool,
    dealerGroup: _propTypes2.default.string,
    diProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    ePlanning: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    fiProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    haveFnaCompleted: _propTypes2.default.bool,
    hcProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    iarRate: _propTypes2.default.string,
    iarRateNote: _propTypes2.default.string,
    id: _propTypes2.default.string,
    isCompleted: _propTypes2.default.bool,
    lastStepIndex: _propTypes2.default.number,
    lastUpd: _propTypes2.default.number,
    needsImgDesc: _propTypes2.default.string,
    other: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    paProtection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    pcHeadstart: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    productType: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    psGoals: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    rPlanning: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    raSection: _propTypes2.default.oneOfType([_propTypes2.default.object]),
    selectedMenuId: _propTypes2.default.string,
    sfAspects: _propTypes2.default.array,
    sfFailNotes: _propTypes2.default.string,
    spAspects: _propTypes2.default.array,
    spNotes: _propTypes2.default.string,
    type: _propTypes2.default.string,
    completedStep: _propTypes2.default.number
  })
};

var analysis = exports.analysis = {
  isRequired: _propTypes2.default.shape({
    // TODO
  })
};

var analysisData = exports.analysisData = {
  isRequired: _propTypes2.default.shape({
    // TODO
  })
};

var analysisRule = exports.analysisRule = {
  isRequired: _propTypes2.default.shape({
    // TODO
  })
};

var analysisContainerFunc = exports.analysisContainerFunc = {
  isRequired: _propTypes2.default.shape({
    // TODO
  })
};

var ra = exports.ra = {
  isRequired: _propTypes2.default.shape({
    // TODO
  })
};

var isFNA = exports.isFNA = {
  isRequired: _propTypes2.default.bool
};

var nameOrder = exports.nameOrder = {
  isRequired: _propTypes2.default.string.isRequired
};