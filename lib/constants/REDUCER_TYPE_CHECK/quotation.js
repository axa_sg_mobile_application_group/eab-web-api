"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.availableFunds = exports.availableInsureds = exports.quotWarnings = exports.quotationErrors = exports.planErrors = exports.inputConfigs = exports.planDetails = exports.quotation = exports.quickQuotes = exports.isQuickQuote = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _client = require("./client");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isQuickQuote = exports.isQuickQuote = {
  isRequired: _propTypes2.default.bool.isRequired
};

var quickQuotes = exports.quickQuotes = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    covName: _propTypes2.default.objectOf(_propTypes2.default.string.isRequired).isRequired,
    createDate: _propTypes2.default.string.isRequired,
    iFullName: _propTypes2.default.string.isRequired,
    id: _propTypes2.default.string.isRequired,
    lastUpdateDate: _propTypes2.default.string.isRequired,
    pCid: _propTypes2.default.string.isRequired,
    pFullName: _propTypes2.default.string.isRequired,
    type: _propTypes2.default.string.isRequired
  })).isRequired
};

var quotation = exports.quotation = {
  isRequired: _propTypes2.default.shape({
    type: _propTypes2.default.oneOf(["quotation"]).isRequired,
    baseProductCode: _propTypes2.default.string.isRequired,
    baseProductId: _propTypes2.default.string.isRequired,
    productLine: _propTypes2.default.string.isRequired,
    compCode: _propTypes2.default.string.isRequired,
    agentCode: _propTypes2.default.string.isRequired,
    dealerGroup: _propTypes2.default.string.isRequired,
    agent: _propTypes2.default.shape({
      agentCode: _propTypes2.default.string.isRequired,
      name: _propTypes2.default.string.isRequired,
      dealerGroup: _propTypes2.default.string.isRequired,
      company: _propTypes2.default.string.isRequired,
      tel: _propTypes2.default.string.isRequired,
      mobile: _propTypes2.default.string.isRequired,
      email: _propTypes2.default.string.isRequired
    }).isRequired,
    lastUpdateDate: _propTypes2.default.string,
    pCid: _propTypes2.default.string.isRequired,
    pFullName: _propTypes2.default.string.isRequired,
    pFirstName: _propTypes2.default.string.isRequired,
    pLastName: _propTypes2.default.string.isRequired,
    pGender: _propTypes2.default.string.isRequired,
    pDob: _propTypes2.default.string.isRequired,
    pAge: _propTypes2.default.number.isRequired,
    pResidence: _propTypes2.default.string.isRequired,
    pSmoke: _propTypes2.default.oneOf(["Y", "N"]).isRequired,
    pEmail: _propTypes2.default.string.isRequired,
    pOccupation: _propTypes2.default.string.isRequired,
    paymentMode: _propTypes2.default.PropTypes.oneOf(["A", "S", "Q", "M", "L"]),
    isBackDate: _propTypes2.default.oneOf(["Y", "N"]).isRequired,
    riskCommenDate: _propTypes2.default.string.isRequired,
    sameAs: _propTypes2.default.oneOf(["Y", "N"]),
    iCid: _propTypes2.default.string,
    iFullName: _propTypes2.default.string,
    iFirstName: _propTypes2.default.string,
    iLastName: _propTypes2.default.string,
    iGender: _propTypes2.default.string,
    iDob: _propTypes2.default.string,
    iAge: _propTypes2.default.number,
    iResidence: _propTypes2.default.string,
    iSmoke: _propTypes2.default.oneOf(["Y", "N"]),
    iEmail: _propTypes2.default.string,
    iOccupation: _propTypes2.default.string,
    ccy: _propTypes2.default.string,
    plans: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      covCode: _propTypes2.default.string.isRequired,
      covName: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired,
      covClass: _propTypes2.default.string,
      policyTerm: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
      policyTermYr: _propTypes2.default.number,
      polTermDesc: _propTypes2.default.string,
      premTerm: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
      premTermYr: _propTypes2.default.number,
      premTermDesc: _propTypes2.default.string,
      calcBy: _propTypes2.default.oneOf(["sumAssured", "premium"]),
      sumInsured: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
      premium: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
      yearPrem: _propTypes2.default.number,
      halfYearPrem: _propTypes2.default.number,
      quarterPrem: _propTypes2.default.number,
      monthPrem: _propTypes2.default.number,
      tax: _propTypes2.default.shape({
        yearYax: _propTypes2.default.number,
        halfYearTax: _propTypes2.default.number,
        quarterTax: _propTypes2.default.number,
        monthTax: _propTypes2.default.number
      }),
      planCode: _propTypes2.default.string
    })),
    quotType: _propTypes2.default.oneOf(["SHIELD"]),
    quickQuote: _propTypes2.default.bool,
    fund: _propTypes2.default.shape({
      funds: _propTypes2.default.arrayOf(_propTypes2.default.shape({
        alloc: _propTypes2.default.number,
        fundCode: _propTypes2.default.string.isRequired,
        fundName: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired
      })).isRequired,
      invOpt: _propTypes2.default.string.isRequired,
      portfolio: _propTypes2.default.string
    }),
    policyOptions: _propTypes2.default.shape({}),
    premium: _propTypes2.default.number,
    sumInsured: _propTypes2.default.number,
    totHalfyearPrem: _propTypes2.default.number,
    totMonthPrem: _propTypes2.default.number,
    totQuarterPrem: _propTypes2.default.number,
    totYearPrem: _propTypes2.default.number,
    attachments: _propTypes2.default.oneOfType([_propTypes2.default.object])
  }).isRequired
};

/* TODO need more detail prop type check */
var planDetails = exports.planDetails = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.object).isRequired
};

/* TODO need more detail prop type check */
var inputConfigs = exports.inputConfigs = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.object).isRequired
};

/* TODO need more detail prop type check */
var planErrors = exports.planErrors = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.object).isRequired
};

/* TODO need more detail prop type check */
var quotationErrors = exports.quotationErrors = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.oneOfType([_propTypes2.default.object, _propTypes2.default.array])).isRequired
};

/* TODO need more detail prop type check */
var quotWarnings = exports.quotWarnings = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.string).isRequired
};

/* TODO need more detail prop type check */
var availableInsureds = exports.availableInsureds = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    availableClasses: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      covClass: _propTypes2.default.string.isRequired,
      className: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired
    })),
    eligible: _propTypes2.default.bool.isRequired,
    profile: _client.profile.isRequired,
    valid: _propTypes2.default.bool.isRequired
  })).isRequired
};

/* TODO need more detail prop type check */
var availableFunds = exports.availableFunds = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.object).isRequired
};