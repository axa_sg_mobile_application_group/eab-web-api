"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.preApplicationEmail = exports.ApplicationItem = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ApplicationItem = exports.ApplicationItem = {
  value: {
    isRequired: _propTypes2.default.shape({
      // Common - from View
      id: _propTypes2.default.string.isRequired,
      type: _propTypes2.default.string.isRequired,
      isValid: _propTypes2.default.bool,
      bundleId: _propTypes2.default.string,
      baseProductCode: _propTypes2.default.string,
      baseProductName: _propTypes2.default.object,
      iName: _propTypes2.default.string,
      pName: _propTypes2.default.string,
      ccy: _propTypes2.default.string,
      paymentMode: _propTypes2.default.string,
      lastUpdateDate: _propTypes2.default.string,
      productLine: _propTypes2.default.string,
      plans: _propTypes2.default.array,
      iCid: _propTypes2.default.string,
      pCid: _propTypes2.default.string,
      iCids: _propTypes2.default.array,
      quotType: _propTypes2.default.string,
      // Common - from core-api
      paymentMethod: _propTypes2.default.string,
      // Quotation - from View
      insureds: _propTypes2.default.object,
      policyOptions: _propTypes2.default.object,
      policyOptionsDesc: _propTypes2.default.object,
      clientChoice: _propTypes2.default.object,
      totCpfPortion: _propTypes2.default.number,
      totCashPortion: _propTypes2.default.number,
      totMedisave: _propTypes2.default.number,
      totPremium: _propTypes2.default.number,
      // Application - from View
      policyNumber: _propTypes2.default.string,
      quotation: _propTypes2.default.object,
      iCidMapping: _propTypes2.default.object,
      isInitialPaymentCompleted: _propTypes2.default.bool,
      isStartSignature: _propTypes2.default.bool,
      isFullySigned: _propTypes2.default.bool,
      isMandDocsAllUploaded: _propTypes2.default.bool,
      // Application - from core-api
      approvalStatus: _propTypes2.default.string
    })
  }
};

var preApplicationEmail = exports.preApplicationEmail = {
  isRequired: _propTypes2.default.array.isRequired
};