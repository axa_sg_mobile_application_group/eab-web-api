"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var OWNER = exports.OWNER = "OWNER";
var SPOUSE = exports.SPOUSE = "SPOUSE";
var SPO = exports.SPO = "SPO";
var SON = exports.SON = "SON";
var DAU = exports.DAU = "DAU";
var FAT = exports.FAT = "FAT";
var MOT = exports.MOT = "MOT";
var GFA = exports.GFA = "GFA";
var GMO = exports.GMO = "GMO";
var DEPENDANTS = exports.DEPENDANTS = "DEPENDANTS";