"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  FEATURES: {
    CP: "CP",
    FNA: "FNA",
    QQ: "QQ",
    FQ: "FQ",
    EAPP: "EAPP"
  },
  PREAPP: {
    SUMMARY: {
      SHOW_SA: 100,
      SHOW_MODAL_PREM: 110,
      SHOW_RETIRE_INCOME: 120,
      SHOW_PREM: 200,
      SHOW_ANNUAL_PREM: 210
    }
  },
  EAPP: {
    STEP: {
      APPLICATION: 0,
      SIGNATURE: 1,
      PAYMENT: 2,
      SUBMISSION: 3
    },
    CROSSAGE_STATUS: {
      WILL_CROSSAGE: 100,
      CROSSED_AGE_SIGNED: 200,
      CROSSED_AGE_NOTSIGNED: 300
    }
  },
  DOCTYPE: {
    QUOTATION: "quotation",
    APP: "application",
    MASTERAPP: "masterApplication"
  },
  PAGE_SIZE: {
    A4: {
      WIDTH: 1731,
      HEIGHT: 2192
    }
  }
};