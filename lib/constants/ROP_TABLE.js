"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var PENDING_LIFE = exports.PENDING_LIFE = "pendingLife";
var PENDING_CI = exports.PENDING_CI = "pendingCi";
var PENDING_TPD = exports.PENDING_TPD = "pendingTpd";
var PENDING_PAADB = exports.PENDING_PAADB = "pendingPaAdb";
var PENDING_TOTALPREM = exports.PENDING_TOTALPREM = "pendingTotalPrem";

var EXIST_LIFE = exports.EXIST_LIFE = "existLife";
var EXIST_CI = exports.EXIST_CI = "existCi";
var EXIST_TPD = exports.EXIST_TPD = "existTpd";
var EXIST_PAADB = exports.EXIST_PAADB = "existPaAdb";
var EXIST_TOTALPREM = exports.EXIST_TOTALPREM = "existTotalPrem";

var REPLACE_LIFE = exports.REPLACE_LIFE = "replaceLife";
var REPLACE_CI = exports.REPLACE_CI = "replaceCi";
var REPLACE_TPD = exports.REPLACE_TPD = "replaceTpd";
var REPLACE_PAADB = exports.REPLACE_PAADB = "replacePaAdb";
var REPLACE_TOTALPREM = exports.REPLACE_TOTALPREM = "replaceTotalPrem";

var PENDING_LIFE_AXA = exports.PENDING_LIFE_AXA = "pendingLifeAXA";
var PENDING_CI_AXA = exports.PENDING_CI_AXA = "pendingCiAXA";
var PENDING_TPD_AXA = exports.PENDING_TPD_AXA = "pendingTpdAXA";
var PENDING_PAADB_AXA = exports.PENDING_PAADB_AXA = "pendingPaAdbAXA";
var PENDING_TOTALPREM_AXA = exports.PENDING_TOTALPREM_AXA = "pendingTotalPremAXA";

var EXIST_LIFE_AXA = exports.EXIST_LIFE_AXA = "existLifeAXA";
var EXIST_CI_AXA = exports.EXIST_CI_AXA = "existCiAXA";
var EXIST_TPD_AXA = exports.EXIST_TPD_AXA = "existTpdAXA";
var EXIST_PAADB_AXA = exports.EXIST_PAADB_AXA = "existPaAdbAXA";
var EXIST_TOTALPREM_AXA = exports.EXIST_TOTALPREM_AXA = "existTotalPremAXA";

var REPLACE_LIFE_AXA = exports.REPLACE_LIFE_AXA = "replaceLifeAXA";
var REPLACE_CI_AXA = exports.REPLACE_CI_AXA = "replaceCiAXA";
var REPLACE_TPD_AXA = exports.REPLACE_TPD_AXA = "replaceTpdAXA";
var REPLACE_PAADB_AXA = exports.REPLACE_PAADB_AXA = "replacePaAdbAXA";
var REPLACE_TOTALPREM_AXA = exports.REPLACE_TOTALPREM_AXA = "replaceTotalPremAXA";

var PENDING_LIFE_OTHER = exports.PENDING_LIFE_OTHER = "pendingLifeOther";
var PENDING_CI_OTHER = exports.PENDING_CI_OTHER = "pendingCiOther";
var PENDING_TPD_OTHER = exports.PENDING_TPD_OTHER = "pendingTpdOther";
var PENDING_PAADB_OTHER = exports.PENDING_PAADB_OTHER = "pendingPaAdbOther";
var PENDING_TOTALPREM_OTHER = exports.PENDING_TOTALPREM_OTHER = "pendingTotalPremOther";

var EXIST_LIFE_OTHER = exports.EXIST_LIFE_OTHER = "existLifeOther";
var EXIST_CI_OTHER = exports.EXIST_CI_OTHER = "existCiOther";
var EXIST_TPD_OTHER = exports.EXIST_TPD_OTHER = "existTpdOther";
var EXIST_PAADB_OTHER = exports.EXIST_PAADB_OTHER = "existPaAdbOther";
var EXIST_TOTALPREM_OTHER = exports.EXIST_TOTALPREM_OTHER = "existTotalPremOther";

var REPLACE_LIFE_OTHER = exports.REPLACE_LIFE_OTHER = "replaceLifeOther";
var REPLACE_CI_OTHER = exports.REPLACE_CI_OTHER = "replaceCiOther";
var REPLACE_TPD_OTHER = exports.REPLACE_TPD_OTHER = "replaceTpdOther";
var REPLACE_PAADB_OTHER = exports.REPLACE_PAADB_OTHER = "replacePaAdbOther";
var REPLACE_TOTALPREM_OTHER = exports.REPLACE_TOTALPREM_OTHER = "replaceTotalPremOther";

var REPLACE_POLICY_TYPE_AXA = exports.REPLACE_POLICY_TYPE_AXA = "replacePolTypeAXA";
var REPLACE_POLICY_TYPE_OTHER = exports.REPLACE_POLICY_TYPE_OTHER = "replacePolTypeOther";

var SHOW_EXIST_TABLE = exports.SHOW_EXIST_TABLE = "havExtPlans";
var SHOW_ACC_EXIST_TABLE = exports.SHOW_ACC_EXIST_TABLE = "havAccPlans";
var SHOW_PENDING_TABLE = exports.SHOW_PENDING_TABLE = "havPndinApp";
var SHOW_REPLACE_TABLE = exports.SHOW_REPLACE_TABLE = "isProslReplace";

var PERIOD = exports.PERIOD = {
  EXISTING: "exist",
  PENDING: "pending",
  REPLACE: "replace"
};

var SUMASSURED = exports.SUMASSURED = {
  LIFE: "LIFE",
  TPD: "TPD",
  CI: "CI",
  PAADB: "PAADB",
  TOTALPREM: "TOTALPREM"
};

var COMPANY = exports.COMPANY = {
  AXA: "AXA",
  OTHER: "OTHER",
  TOTAL: "TOTAL"
};

var EXITSTING_TABLE_MAPPING = exports.EXITSTING_TABLE_MAPPING = {
  AXA: {
    LIFE: EXIST_LIFE_AXA,
    TPD: EXIST_TPD_AXA,
    CI: EXIST_CI_AXA,
    PAADB: EXIST_PAADB_AXA,
    TOTALPREM: EXIST_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: EXIST_LIFE_OTHER,
    TPD: EXIST_TPD_OTHER,
    CI: EXIST_CI_OTHER,
    PAADB: EXIST_PAADB_OTHER,
    TOTALPREM: EXIST_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: EXIST_LIFE,
    TPD: EXIST_TPD,
    CI: EXIST_CI,
    PAADB: EXIST_PAADB,
    TOTALPREM: EXIST_TOTALPREM
  }
};

var PENDING_TABLE_MAPPING = exports.PENDING_TABLE_MAPPING = {
  AXA: {
    LIFE: PENDING_LIFE_AXA,
    TPD: PENDING_TPD_AXA,
    CI: PENDING_CI_AXA,
    PAADB: PENDING_PAADB_AXA,
    TOTALPREM: PENDING_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: PENDING_LIFE_OTHER,
    TPD: PENDING_TPD_OTHER,
    CI: PENDING_CI_OTHER,
    PAADB: PENDING_PAADB_OTHER,
    TOTALPREM: PENDING_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: PENDING_LIFE,
    TPD: PENDING_TPD,
    CI: PENDING_CI,
    PAADB: PENDING_PAADB,
    TOTALPREM: PENDING_TOTALPREM
  }
};

var REPLACE_TABLE_MAPPING = exports.REPLACE_TABLE_MAPPING = {
  AXA: {
    LIFE: REPLACE_LIFE_AXA,
    TPD: REPLACE_TPD_AXA,
    CI: REPLACE_CI_AXA,
    PAADB: REPLACE_PAADB_AXA,
    TOTALPREM: REPLACE_TOTALPREM_AXA
  },
  OTHER: {
    LIFE: REPLACE_LIFE_OTHER,
    TPD: REPLACE_TPD_OTHER,
    CI: REPLACE_CI_OTHER,
    PAADB: REPLACE_PAADB_OTHER,
    TOTALPREM: REPLACE_TOTALPREM_OTHER
  },
  TOTAL: {
    LIFE: REPLACE_LIFE,
    TPD: REPLACE_TPD,
    CI: REPLACE_CI,
    PAADB: REPLACE_PAADB,
    TOTALPREM: REPLACE_TOTALPREM
  }
};