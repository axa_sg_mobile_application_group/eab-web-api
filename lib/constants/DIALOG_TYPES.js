"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var CONFIRMATION = exports.CONFIRMATION = "CONFIRMATION";

// User can continue after clicking
var WARNING = exports.WARNING = "WARNING";

// User cannot continue after clicking
var ERROR = exports.ERROR = "ERROR";

var OTHER = exports.OTHER = "OTHER";