"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var TEXT_FIELD = exports.TEXT_FIELD = "TEXT_FIELD";
var TEXT_BOX = exports.TEXT_BOX = "TEXT_BOX";
var TEXT_SELECTION = exports.TEXT_SELECTION = "TEXT_SELECTION";
var TEXTSELECTION = exports.TEXTSELECTION = "TEXTSELECTION";
var TEXTAREA = exports.TEXTAREA = "TEXTAREA";
var TEXTFIELD = exports.TEXTFIELD = "TEXTFIELD";
var TEXT = exports.TEXT = "TEXT";
var CHECKBOX = exports.CHECKBOX = "CHECKBOX";
var QRADIOGROUP = exports.QRADIOGROUP = "QRADIOGROUP";
var DATEPICKER = exports.DATEPICKER = "DATEPICKER";
var PICKER = exports.PICKER = "PICKER";